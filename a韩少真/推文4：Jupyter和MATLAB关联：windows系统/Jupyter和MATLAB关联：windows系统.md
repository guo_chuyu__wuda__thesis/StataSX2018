# Jupyter和MATLAB关联：windows系统
> 作者：韩少真(西北大学) || 展金永（对外经济贸易大学）
## 1. 准备工作-关联前提
首先，请按助教提供教程安装Anaconda和MATLAB，并检验Anaconda和MATLAB是否已经成功安装在本机电脑上。
## 2. 安装matlab的python拓展
# 2.1 获取扩展的路径
打开matlab的安装路径（如果忘记可以看下matlab安装教程的第六步），依次打开extern、engines、python文件夹，进入python文件夹。然后点击地址栏，复制python文件夹的路径地址，如下图所示：
![](https://upload-images.jianshu.io/upload_images/17516282-1e17d2010b694ada.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

我的电脑为`D:\Polyspace\R2019a\extern\engines\python`，根据matlab安装的地址不同，`D:\Polyspace\R2019a\`可能有所差异，请根据个人安装状况更改。

# 2.2 安装MATLAB的Python扩展
- 通过`开始-Anaconda3（64-bit）-Anaconda Powershell prompt`，打开Anaconda Powershell prompt。如果直接打开执行下面的操作无法正常关联python和matlab，则需要右击选择以管理员身份运行，重新进行接下来的操作。
![](https://upload-images.jianshu.io/upload_images/17516282-2a2eb394d8c94047.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
- 在powershell中依次输入以下命令并按回车键执行
```
cd "D:\Polyspace\R2019a\extern\engines\python"
python setup.py install
```
-执行结果如下所示，表明扩展安装成功
```python
(base) PS C:\Users\mailshaozhen> cd D:\Polyspace\R2019a\extern\engines\python
(base) PS D:\Polyspace\R2019a\extern\engines\python> python setup.py install
running install
running build
running build_py
creating build
creating build\lib
creating build\lib\matlab
copying dist\matlab\mlarray.py -> build\lib\matlab
copying dist\matlab\mlexceptions.py -> build\lib\matlab
copying dist\matlab\__init__.py -> build\lib\matlab
creating build\lib\matlab\engine
copying dist\matlab\engine\basefuture.py -> build\lib\matlab\engine
copying dist\matlab\engine\engineerror.py -> build\lib\matlab\engine
copying dist\matlab\engine\enginehelper.py -> build\lib\matlab\engine
copying dist\matlab\engine\enginesession.py -> build\lib\matlab\engine
copying dist\matlab\engine\fevalfuture.py -> build\lib\matlab\engine
copying dist\matlab\engine\futureresult.py -> build\lib\matlab\engine
copying dist\matlab\engine\matlabengine.py -> build\lib\matlab\engine
copying dist\matlab\engine\matlabfuture.py -> build\lib\matlab\engine
copying dist\matlab\engine\__init__.py -> build\lib\matlab\engine
creating build\lib\matlab\_internal
copying dist\matlab\_internal\mlarray_sequence.py -> build\lib\matlab\_internal
copying dist\matlab\_internal\mlarray_utils.py -> build\lib\matlab\_internal
copying dist\matlab\_internal\__init__.py -> build\lib\matlab\_internal
running install_lib
creating D:\Anaconda3\Lib\site-packages\matlab
creating D:\Anaconda3\Lib\site-packages\matlab\engine
copying build\lib\matlab\engine\basefuture.py -> D:\Anaconda3\Lib\site-packages\matlab\engine
copying build\lib\matlab\engine\engineerror.py -> D:\Anaconda3\Lib\site-packages\matlab\engine
copying build\lib\matlab\engine\enginehelper.py -> D:\Anaconda3\Lib\site-packages\matlab\engine
copying build\lib\matlab\engine\enginesession.py -> D:\Anaconda3\Lib\site-packages\matlab\engine
copying build\lib\matlab\engine\fevalfuture.py -> D:\Anaconda3\Lib\site-packages\matlab\engine
copying build\lib\matlab\engine\futureresult.py -> D:\Anaconda3\Lib\site-packages\matlab\engine
copying build\lib\matlab\engine\matlabengine.py -> D:\Anaconda3\Lib\site-packages\matlab\engine
copying build\lib\matlab\engine\matlabfuture.py -> D:\Anaconda3\Lib\site-packages\matlab\engine
copying build\lib\matlab\engine\_arch.txt -> D:\Anaconda3\Lib\site-packages\matlab\engine
copying build\lib\matlab\engine\__init__.py -> D:\Anaconda3\Lib\site-packages\matlab\engine
copying build\lib\matlab\mlarray.py -> D:\Anaconda3\Lib\site-packages\matlab
copying build\lib\matlab\mlexceptions.py -> D:\Anaconda3\Lib\site-packages\matlab
creating D:\Anaconda3\Lib\site-packages\matlab\_internal
copying build\lib\matlab\_internal\mlarray_sequence.py -> D:\Anaconda3\Lib\site-packages\matlab\_internal
copying build\lib\matlab\_internal\mlarray_utils.py -> D:\Anaconda3\Lib\site-packages\matlab\_internal
copying build\lib\matlab\_internal\__init__.py -> D:\Anaconda3\Lib\site-packages\matlab\_internal
copying build\lib\matlab\__init__.py -> D:\Anaconda3\Lib\site-packages\matlab
byte-compiling D:\Anaconda3\Lib\site-packages\matlab\engine\basefuture.py to basefuture.cpython-37.pyc
byte-compiling D:\Anaconda3\Lib\site-packages\matlab\engine\engineerror.py to engineerror.cpython-37.pyc
byte-compiling D:\Anaconda3\Lib\site-packages\matlab\engine\enginehelper.py to enginehelper.cpython-37.pyc
byte-compiling D:\Anaconda3\Lib\site-packages\matlab\engine\enginesession.py to enginesession.cpython-37.pyc
byte-compiling D:\Anaconda3\Lib\site-packages\matlab\engine\fevalfuture.py to fevalfuture.cpython-37.pyc
byte-compiling D:\Anaconda3\Lib\site-packages\matlab\engine\futureresult.py to futureresult.cpython-37.pyc
byte-compiling D:\Anaconda3\Lib\site-packages\matlab\engine\matlabengine.py to matlabengine.cpython-37.pyc
byte-compiling D:\Anaconda3\Lib\site-packages\matlab\engine\matlabfuture.py to matlabfuture.cpython-37.pyc
byte-compiling D:\Anaconda3\Lib\site-packages\matlab\engine\__init__.py to __init__.cpython-37.pyc
byte-compiling D:\Anaconda3\Lib\site-packages\matlab\mlarray.py to mlarray.cpython-37.pyc
byte-compiling D:\Anaconda3\Lib\site-packages\matlab\mlexceptions.py to mlexceptions.cpython-37.pyc
byte-compiling D:\Anaconda3\Lib\site-packages\matlab\_internal\mlarray_sequence.py to mlarray_sequence.cpython-37.pyc
byte-compiling D:\Anaconda3\Lib\site-packages\matlab\_internal\mlarray_utils.py to mlarray_utils.cpython-37.pyc
byte-compiling D:\Anaconda3\Lib\site-packages\matlab\_internal\__init__.py to __init__.cpython-37.pyc
byte-compiling D:\Anaconda3\Lib\site-packages\matlab\__init__.py to __init__.cpython-37.pyc
running install_egg_info
Writing D:\Anaconda3\Lib\site-packages\matlabengineforpython-R2018a-py3.7.egg-info
(base) PS D:\Polyspace\R2019a\extern\engines\python>
```

## 3.在Python中安装matlab_kernel包
- 根据下图，打开`Anaconda prompt`
![](https://upload-images.jianshu.io/upload_images/17516282-2879e4f9fdeab2cc.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
- 在`Anaconda prompt`中运行以下代码
```python
pip install matlab_kernel
```
- anaconda 
会自动安装matlab_kernel包，具体如下所示，则安装成功
```
(base) C:\Users\mailshaozhen>pip install matlab_kernel
Collecting matlab_kernel
  Downloading https://files.pythonhosted.org/packages/64/ad/5f471160ec33e2f0f3586d285cd5e3b6dff51027849f28369d43d1d57fd1/matlab_kernel-0.16.7-py3-none-any.whl
Requirement already satisfied: ipython>=4.0.0 in d:\anaconda3\lib\site-packages (from matlab_kernel) (7.4.0)
Collecting metakernel>=0.23.0 (from matlab_kernel)
  Downloading https://files.pythonhosted.org/packages/bb/bd/658103e652c2c12a791c83539496541e8f8d755c1cc3293861983f0a6742/metakernel-0.24.2-py2.py3-none-any.whl (208kB)
    100% |████████████████████████████████| 215kB 13kB/s
Requirement already satisfied: jupyter-client>=4.4.0 in d:\anaconda3\lib\site-packages (from matlab_kernel) (5.2.4)
Requirement already satisfied: jedi>=0.10 in d:\anaconda3\lib\site-packages (from ipython>=4.0.0->matlab_kernel) (0.13.3)
Requirement already satisfied: backcall in d:\anaconda3\lib\site-packages (from ipython>=4.0.0->matlab_kernel) (0.1.0)
Requirement already satisfied: prompt-toolkit<2.1.0,>=2.0.0 in d:\anaconda3\lib\site-packages (from ipython>=4.0.0->matlab_kernel) (2.0.9)
Requirement already satisfied: traitlets>=4.2 in d:\anaconda3\lib\site-packages (from ipython>=4.0.0->matlab_kernel) (4.3.2)
Requirement already satisfied: colorama; sys_platform == "win32" in d:\anaconda3\lib\site-packages (from ipython>=4.0.0->matlab_kernel) (0.4.1)
Requirement already satisfied: setuptools>=18.5 in d:\anaconda3\lib\site-packages (from ipython>=4.0.0->matlab_kernel) (40.8.0)
Requirement already satisfied: decorator in d:\anaconda3\lib\site-packages (from ipython>=4.0.0->matlab_kernel) (4.4.0)
Requirement already satisfied: pygments in d:\anaconda3\lib\site-packages (from ipython>=4.0.0->matlab_kernel) (2.3.1)
Requirement already satisfied: pickleshare in d:\anaconda3\lib\site-packages (from ipython>=4.0.0->matlab_kernel) (0.7.5)
Collecting ipyparallel (from metakernel>=0.23.0->matlab_kernel)
  Downloading https://files.pythonhosted.org/packages/3f/82/aaa7a357845a98d4028f27c799f0d3bb2fe55fc1247c73dc712b4ae2344c/ipyparallel-6.2.4-py2.py3-none-any.whl (198kB)
    100% |████████████████████████████████| 204kB 7.5kB/s
Requirement already satisfied: ipykernel in d:\anaconda3\lib\site-packages (from metakernel>=0.23.0->matlab_kernel) (5.1.0)
Collecting pexpect>=4.2 (from metakernel>=0.23.0->matlab_kernel)
  Downloading https://files.pythonhosted.org/packages/0e/3e/377007e3f36ec42f1b84ec322ee12141a9e10d808312e5738f52f80a232c/pexpect-4.7.0-py2.py3-none-any.whl (58kB)
    100% |████████████████████████████████| 61kB 10kB/s
Requirement already satisfied: tornado>=4.1 in d:\anaconda3\lib\site-packages (from jupyter-client>=4.4.0->matlab_kernel) (6.0.2)
Requirement already satisfied: jupyter-core in d:\anaconda3\lib\site-packages (from jupyter-client>=4.4.0->matlab_kernel) (4.4.0)
Requirement already satisfied: pyzmq>=13 in d:\anaconda3\lib\site-packages (from jupyter-client>=4.4.0->matlab_kernel) (18.0.0)
Requirement already satisfied: python-dateutil>=2.1 in d:\anaconda3\lib\site-packages (from jupyter-client>=4.4.0->matlab_kernel) (2.8.0)
Requirement already satisfied: parso>=0.3.0 in d:\anaconda3\lib\site-packages (from jedi>=0.10->ipython>=4.0.0->matlab_kernel) (0.3.4)
Requirement already satisfied: wcwidth in d:\anaconda3\lib\site-packages (from prompt-toolkit<2.1.0,>=2.0.0->ipython>=4.0.0->matlab_kernel) (0.1.7)
Requirement already satisfied: six>=1.9.0 in d:\anaconda3\lib\site-packages (from prompt-toolkit<2.1.0,>=2.0.0->ipython>=4.0.0->matlab_kernel) (1.12.0)
Requirement already satisfied: ipython-genutils in d:\anaconda3\lib\site-packages (from traitlets>=4.2->ipython>=4.0.0->matlab_kernel) (0.2.0)
Collecting ptyprocess>=0.5 (from pexpect>=4.2->metakernel>=0.23.0->matlab_kernel)
  Downloading https://files.pythonhosted.org/packages/d1/29/605c2cc68a9992d18dada28206eeada56ea4bd07a239669da41674648b6f/ptyprocess-0.6.0-py2.py3-none-any.whl
Installing collected packages: ipyparallel, ptyprocess, pexpect, metakernel, matlab-kernel
Successfully installed ipyparallel-6.2.4 matlab-kernel-0.16.7 metakernel-0.24.2 pexpect-4.7.0 ptyprocess-0.6.0
```
- 然后在`Anaconda prompt`中输入`jupyter notebook`，回车执行就会直接打开`jupyter notebook`。打开`jupyter notebook`的方法有很多，可以在开始程序中找到`jupyter notebook`点击打开，也可以到Anaconda的主界面打开`jupyter notebook`。根据下图步骤新建Matlab的文档。
![](https://upload-images.jianshu.io/upload_images/17516282-8b4b025c0ea30edf.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- 在打开的notebook中输入matlab语句，例如a=3，表示生成新变量a，并给其赋值为3。点击运行，出现运行结果，表明关联成功。具体如下图所示：
- **注意：**执行第一条指令的时候要耐心等一等，matlab r2019a程序比较大，启动比较慢，执行完第一行执行，接下来执行就会快很多，可以继续执行b=a，测试发现基本不用等待就会出结果。
![image.png](https://upload-images.jianshu.io/upload_images/17516282-0340f981b1901a31.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)