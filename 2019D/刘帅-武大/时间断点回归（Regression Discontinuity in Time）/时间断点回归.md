
&emsp;

> 作者：刘帅 (武汉大学)      
>     
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)

&emsp;
  


## 1. 简介

断点回归已经在经济学领域得到广泛应用，主要有两个原因：①与其他非实验方法相比，断点回归需要更松的假设。②断点回归不仅仅是一种评估策略，更重要的是由断点回归得到的因果推断比其他自然实验方法更可信。（Lee & Lemieux，2010）

越来越多的断点回归设计（RDD）把时间作为配置变量（running variable），我们称这种断点回归设计为Regression Discontinuity in Time（RDiT）。RDiT在很多Top期刊都有出现，下表所示。

Publication | Journal
---|---
Anderson (2014) | American Economic Review
Auffhammer & Kellogg (2011) |American Economic Review
Bento et al. (2014) | American Economic Journal: Economic Policy
Busse et al. (2006) |American Economic Review
Chen & Whalley (2012) | American Economic Journal: Economic Policy
Davis (2008) | Journal of Political Economics
Davis & Kahn (2010) | American Economic Journal: Economic Policy
Gallego et al. (2013) | Journal of Public Economics
Grainger & Costello (2014) | Journal of Environmental Economics and Management
Lang & Siler (2013) | Energy Policy

RDiT多运用于环境与能源政策领域，可能的原因在于：相对于其他领域的研究，能源经济学和环境经济学更能获得高频时变污染数据。现以环境经济学为例，举一个RDiT的例子。假如美国政府要求燃煤火电厂在同一时刻安装排污控制装置，那么研究者可以用RDiT，测算这个时刻前后污染情况以衡量此政策的影响。这个典型的RDiT具有以下特点：①无法构造面板数据，所以无法使用DID。②环境质量数据容易获得，可以是日数据，甚至是小时数据，且时间跨度长。③其他随时间变化的因素，在政策前后是平稳的。

仍以此举例，在传统的RD中，燃煤火电厂装机容量达到1000兆瓦才需要安装排污控制装置，研究者需要对比刚好超过或低于1000兆瓦火电厂（如990兆到1010兆之间的火电厂）周围的空气质量。

本文主要是对比RDiT与传统RD的区别，并对进行RDiT提出建议。

## 2. 传统RDD与RDiT的区别
传统RD 已有很多文献进行介绍，本文不再赘述。两者的关键区别在于是否把某一时刻作为配置变量。在RDiT中，尤其是在环境经济学的应用中，观测数据很难形成一个面板数据，所以无法使用DID进行估计；另外，气温等其他因素可能也随着时间进行变化，难以消除其他因素的影响。两者主要三点不同：(这里主要是分析截面数据RD (cross-sectional RD)与RDiT的区别)

①在解释方面的不同。传统的断点回归设计被看做是一个在门槛周围的局部随机试验。Lee(2008)，Lee & Lemieux (2010)强调RD的随机性，而Hahn et al. (2001)更强调RD的断点特性。就RDiT来说，它更接近与断点特性，但却不能看作是一个局部随机试验。原因在于，时间作为配置变量，不能被看作是随机施加的政策冲击。RDiT更接近于一个利用断点的准试验框架（quasi-experimental framework），但不能看作是一个随机试验（randomized trial）。

②传统RD和RDiT在扩大估计样本的方式不同。数据有时间维度（T dimension）和截面维度（N dimension）。传统RD是基于截面维度定义的，即使随着带宽的缩小，研究者也可以通过增加N保持足够样本数。但在RDiT中，要想获得比较充足的样本，只能扩大时间断点的前后时间跨度，这样的话，可能有些观测值离断点就比较远，从而带来了估计偏误。时间跨越大，就增加了不能观测因素也随时间断点变化的可能性。

③传统RD和RDiT是否需要控制变量。正如Lee & Lemieux（2010）所说的，RD在带宽范围内可以看作是一个随机试验，并不需要控制变量，添加控制变量的作用则是降低噪音和提高估计精确度。然而，在RDiT中，不可观测因素可能会与配置变量（即时间）有关。在前表所列的文章中，几乎都包含了控制变量，如时间趋势项。因此，在RDiT中，加入控制变量以降低偏误是必须的，而不是仅仅提高估计准确性。

另外有两点需要说明，第一，在RDiT框架下，局部回归经常会碰到小样本，而在传统RD中，往往是大样本；第二，许多时间序列存在单位根问题，这也是与传统RD不同的地方。

## 3. RDiT偏误可能的来源

①时变处理效应（Time-Varying Treatment Effects）。指的是其他可能随时间变化同时也会影响结果的其他因素。这个问题在传统RD也会存在，但在RDiT中可能更为明显和突出。RDiT有两个假设必须满足：一是模型设定要准确，其他潜在可能影响结果的因素必须控制，如前述例子中的气候、天气等因素；二是必须正确识别处理效应，特别是要排除其他潜在政策的影响，必须明确处理效应在断点前后是平稳稳定的。

②自回归问题（Autoregression）。时间序列往往存在自相关问题。如果残差存在序列相关性，可以使用聚类稳健标准误加以纠正。如果控制变量存在自相关，那将影响对长期/短期效应的估计。举例说明，以前述例子来说，安装装置能够是排放减少β。研究者观测的是日数据。假设当天的污染在次日存留比例为α. 那么在政策是时候第一天，处理效应应该是β；第二天，处理效应就是减少的污染加上存留的污染：β + β · α. 这种长期效应就是 β/(1-α)。为了揭示这种动态过程，往往在回归中添加滞后项。值得一提的是，序列相关可能是残差序列相关，也可能是因变量序列相关（如AR(1)过程），添加滞后项只能解决后者。

③分类和预期效应（Sorting and Anticipation Effects）。传统RD通常采用DCdensity（McCrary, 2008）对配置变量的跳跃性进行检验。这是一种密度函数的检验方法，对于时间（均匀分布）这种配置变量无效。假设有一家发电厂提前安装了减排装置，（这类似于达不到一本分数线的同学通过改变自己的行为，如补习等，使自己成为处理组，即达到一本线。）RDiT 就很难识别这种行为，传统RD可尚可进行McCrary test。这就导致RDiT得到结果是处理效应+其他观测不到的分类效应等的综合效应。

综上，RDiT的偏误来自： time-varying treatment effects, autoregression, and selection into or out of
treatment. 


## 4. 使用RDiT的建议

基于以上分析，本文对RDiT提出一些建议，目的在于减少由上述原因导致的估计偏误。

我们首先对RDiT中可能遇到的问题及结果进行汇总：

问题 | 后果
---|---
与时间有关的不可观测因素（Unobservables correlated with time） | 回归中要尽量包括控制变量
 time-varying treatment effects | 容易导致处理效应的错误识别
Autoregressive properties | 短期效应和长期效应结果不一致；
Selection and strategic behavior | 时间变量分布是均匀分布，无法使用McCrary test

结合在顶刊上的RDiT，以及本文提出的存在问题，对进行RDiT时，提出如下建议：

①对原始数据作图。用不同阶多项式或线性模型进行拟合，如果结果随着不同时间趋势项发生变化，则可能存在 time-varying treatment effects。

②进行多种稳健性检验。展示不同阶多项式、不同带宽的回归结果。

③安慰剂检验。比如变换时间断点。

④画控制变量图（如气温等），观测在断点前后是否是平稳。

⑤甜甜圈RD。（Barreca et al. 2011）
 
⑥自回归检验。如果存在自回归，则加入滞后项。

⑦可考虑使用局部线性回归而不是多项式回归。

## 5. 个人学习心得

**心得1**：事实上，有学者建议尽量不要用"时间"当作 running variable （Stata专版 - 经管之家(原人大经济论坛)  https://bbs.pinggu.org/thread-7104041-1-1.html ）。但在实际的文章中，也看到了一些使用时间作为断点的例子，除了上文介绍的顶刊论文外，中文也有一些，如：

项后军, 何康等, 自贸区设立、贸易发展与资本流动——基于上海自贸区的研究[J]. 金融研究, 2016(10):48-63.

曹静,王鑫,钟笑寒.限行政策是否改善了北京市的空气质量?[J].经济学(季刊),2014,13(03):1091-1126.

显然，是否可以使用时间断点是一个有待进一步讨论的学术问题。

**心得2：** 在研读复制一些顶刊的RD结果发现，多数学者并未采用rdrobust命令，更多的是使用ivreg2命令。
 
使用rdbwselect命令比较方便地进行带宽选择，但这个命令2017年更新以后，cross-validation method（Ludwig and Miller,2007）被抛弃，但提供了更多的方法选择。具体可以比较一下：

[1]Calonico S, Cattaneo M D, Farrell M H, et al. rdrobust: Software for regression-discontinuity designs[J]. The Stata Journal, 2017, 17(2): 372-404.

[2]Calonico S, Cattaneo M D, Titiunik R. Robust data-driven inference in the regression-discontinuity design[J]. The Stata Journal, 2014, 14(4): 909-946.

## 6. 参考文献

[1]Hausman C, Rapson D S. Regression discontinuity in time: Considerations for empirical applications[J]. Annual Review of Resource Economics, 2018, 10: 533-552.

[2]Imbens G W, Lemieux T. Regression discontinuity designs: A guide to practice[J]. Journal of econometrics, 2008, 142(2): 615-635.

[3]Lemieux L T . Regression Discontinuity Designs in Economics[J]. Journal of Economic Literature, 2010, 48(2):281-355.

[4]Baskaran T, Hessami Z. Does the election of a female leader clear the way for more women in politics?[J]. American Economic Journal: Economic Policy, 2018, 10(3): 95-121.




