
## 提纲和参考资料

### 提纲
- 受限数据：截断和截堵
  用一个简单的例子说明这两类数据类型，参见 DM2004，以及双栏模型推文中我新加的那部分内容。
- Tobit 模型设定
- 估计
- 假设检验
- 边际效应
- Stata 范例
- 结论

### 各部分参考资料 
这是我看到的资料，你可以根据自己掌握的资料进行调整

- 理论部分参考：
  - 主要参考 DM2004 对 **截断** 和 **截堵** 的介绍。Dividson and MacKinnon (2004, [[PDF]](https://quqi.gblhgk.com/s/880197/t8EZJgtFyp23kfHc)), Sction 11.6 Models for Censored and Truncated Data.
  - 理论部分的对 Tobit 模型的详细推导和边际效应解释，参见 Wd2010。直观的解释参见 Wd2012.
  - Stata 实操部分参见 CT2009，chatper 16.
- Stata 应用部分：如下推文可以参考，但范例数据要换掉，这个例子不好。
  - **TOBIT ANALYSIS | STATA DATA ANALYSIS EXAMPLES** https://stats.idre.ucla.edu/stata/dae/tobit-analysis/

## 参考文献
  - **DM2004**. Davidson R, MacKinnon J G. Econometric theory and methods[M]. New York: Oxford University Press, 2004. [[PDF]](https://quqi.gblhgk.com/s/880197/t8EZJgtFyp23kfHc)
  - **Wd2010**. Wooldridge J M. Econometric analysis of cross section and panel data[M]. MIT press, 2010. [[PDF]](https://quqi.gblhgk.com/s/880197/1y6HA8CnmKjxbZxh)
  - **Wd2012** Wooldridge J M. Introductory econometrics: A modern approach[M]. Nelson Education, 2016. [[PDF]](https://quqi.gblhgk.com/s/880197/9yh5m8OtHdEcHnKh)
  - **CT2009**. Cameron A C, Trivedi P K. Microeconometrics Using Stata[J]. Stata Press books, 2010. [[PDF]](https://quqi.gblhgk.com/s/880197/Tts1JToBS6HPW4kh)