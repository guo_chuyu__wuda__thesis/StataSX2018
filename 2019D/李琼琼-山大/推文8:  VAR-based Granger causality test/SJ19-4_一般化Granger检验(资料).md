> **任务：** 根据如下文章写一篇推文，介绍这种新方法的原理和 Stata 实现方法。

> Anatolyev, S., A. Skolkova, **2019**, Many instruments: Implementation in stata, **The Stata Journal: Promoting communications on statistics and Stata**, 19 (4): 849-866. [[PDF]](https://sci-hub.tw/10.1177/1536867x19893627)

### 相关程序和作者提供的文件

stata command: 在命令窗口中输入如下命令即可：

`net describe st0581, from(http://www.stata-journal.com/software/sj19-4)`


原文查看：https://www.lianxh.cn/news/12ffe67d8d8fb.html


```

TITLE
      SJ19-4 st0581. Run the VAR-based Granger causality ...

DESCRIPTION/AUTHOR(S)
      Run the VAR-based Granger causality test in the
        presence of instabilities
      by Barbara Rossi, University of Pompeu Fabra,
           Barcelona Graduate School of Economics,
           and CREI, Barcelona, Spain
         Yiru Wang, University Pompeu Fabra,
           Barcelona, Spain
      Support:  barbara.rossi@upf.edu,
                yiru.wang@upf.edu
      After installation, type help gcrobustvar
      DOI:  10.1177/1536867X19893631
      gcrobustvar requires the matsqrt package
        and the gmm package; the matsqrt package
        can be found and installed in Stata by typing
        findit matsqrt in the Command window.

INSTALLATION FILES                                  (click here to install)
      st0581/gcrobustvar.ado
      st0581/chowgmmstar3.ado
      st0581/nyblomstar3.ado
      st0581/pvcalc.ado
      st0581/gcrobustvar.sthlp

ANCILLARY FILES                                     (click here to get)
      st0581/main_implement.do
      st0581/gcdata.xlsx
      st0581/pvtable.mmat
```

&emsp;

&emsp;

### 参考文献：

- Andrews, D. W. K. 1993. Tests for parameter instability and structural change with unknown change point. Econometrica 61: 821–856. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=1993&pages=821-856&author=D.+W.+K.+Andrews&title=Tests+for+parameter+instability+and+structural+change+with+unknown+change+point) \| [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr1-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.2307%2F2951764)
- Andrews, D. W. K., Lee, I., Ploberger, W. 1996. Optimal changepoint tests for normal linear regression. Journal of Econometrics 70: 9–38. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=1996&pages=9-38&author=D.+W.+K.+Andrews&author=I.+Lee&author=W.+Ploberger&title=Optimal+changepoint+tests+for+normal+linear+regression) \| [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr2-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.1016%2F0304-4076(94)01682-8)
- Andrews, D. W. K., Ploberger, W. 1994. Optimal tests when a nuisance parameter is present only under the alternative. Econometrica 62: 1383–1414. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=1994&pages=1383-1414&author=D.+W.+K.+Andrews&author=W.+Ploberger&title=Optimal+tests+when+a+nuisance+parameter+is+present+only+under+the+alternative) \| [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr3-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.2307%2F2951753)
- Boivin, J., Giannoni, M. P. 2006. Has monetary policy become more effective? Review of Economics and Statistics 88: 445–462. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=2006&pages=445-462&author=J.+Boivin&author=M.+P.+Giannoni&title=Has+monetary+policy+become+more+effective%3F) \| [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr4-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.1162%2Frest.88.3.445)
- Brown, R. L., Durbin, J., Evans, J. M. 1975. Techniques for testing the constancy of regression relationships over time. Journal of the Royal Statistical Society, Series B 37: 149–192. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=1975&pages=149-192&author=R.+L.+Brown&author=J.+Durbin&author=J.+M.+Evans&title=Techniques+for+testing+the+constancy+of+regression+relationships+over+time)
- Clark, T. E., McCracken, M. W. 2006. The predictive content of the output gap for inflation: Resolving in-sample and out-of-sample evidence. Journal of Money, Credit and Banking 38: 1127–1148. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=2006&pages=1127-1148&author=T.+E.+Clark&author=M.+W.+McCracken&title=The+predictive+content+of+the+output+gap+for+inflation%3A+Resolving+in-sample+and+out-of-sample+evidence) \| [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr6-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.1353%2Fmcb.2006.0068)
- Cogley, T., Sargent, T. J. 2001. Evolving post-World War II U.S. inflation dynamics. NBER Macroeconomics Annual 16: 331–373. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=2001&pages=331-373&author=T.+Cogley&author=T.+J.+Sargent&title=Evolving+post-World+War+II+U.S.+inflation+dynamics) \| [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr7-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.1086%2F654451) - Cogley, T. 2005. Drifts and volatilities: Monetary policies and outcomes in the post WWII U.S. Review of Economic Dynamics 8: 262–302. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=2005&pages=262-302&author=T.+Cogley&title=Drifts+and+volatilities%3A+Monetary+policies+and+outcomes+in+the+post+WWII+U.S) \| [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr8-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.1016%2Fj.red.2004.10.009)
- Granger, C. W. J. 1969. Investigating causal relations by econometric models and cross-spectral methods. Econometrica 37: 424–438. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=1969&pages=424-438&author=C.+W.+J.+Granger&title=Investigating+causal+relations+by+econometric+models+and+cross-spectral+methods) \| [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr9-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.2307%2F1912791)
- Jordá, Ó. 2005. Estimation and inference of impulse responses by local projections. American Economic Review 95: 161–182. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=2005&pages=161-182&author=Ó.+Jordá&title=Estimation+and+inference+of+impulse+responses+by+local+projections) \| [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr10-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.1257%2F0002828053828518)
- Kozicki, S., Tinsley, P. A. 2001. Shifting endpoints in the term structure of interest rates. Journal of Monetary Economics 47: 613–652. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=2001&pages=613-652&author=S.+Kozicki&author=P.+A.+Tinsley&title=Shifting+endpoints+in+the+term+structure+of+interest+rates) \| [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr11-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.1016%2FS0304-3932(01)00054-X)
- Newey, W. K., West, K. D. 1994. Automatic lag selection in covariance matrix estimation. Review of Economic Studies 61: 631–653. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=1994&pages=631-653&author=W.+K.+Newey&author=K.+D.+West&title=Automatic+lag+selection+in+covariance+matrix+estimation) \| [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr12-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.2307%2F2297912)
- Nyblom, J. 1989. Testing for the constancy of parameters over time. Journal of the American Statistical Association 84: 223–230. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=1989&pages=223-230&author=J.+Nyblom&title=Testing+for+the+constancy+of+parameters+over+time) \| [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr13-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.1080%2F01621459.1989.10478759)
- Quandt, R. E. 1960. Tests of the hypothesis that a linear regression system obeys two separate regimes. Journal of the American Statistical Association 55: 324–330. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=1960&pages=324-330&author=R.+E.+Quandt&title=Tests+of+the+hypothesis+that+a+linear+regression+system+obeys+two+separate+regimes) \| [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr14-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.1080%2F01621459.1960.10482067)
- Rossi, B. 2005. Optimal tests for nested model selection with underlying parameter instability. Econometric Theory 21: 962–990. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=2005&pages=962-990&author=B.+Rossi&title=Optimal+tests+for+nested+model+selection+with+underlying+parameter+instability) \| [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr15-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.1017%2FS0266466605050486)
- Rossi, B. 2013. Advances in forecasting under instability. In Handbook of Economic Forecasting, vol. 2B, ed. Elliott, G., Timmermann, A., 1203–1324. Amsterdam: Elsevier. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=2013&pages=1203-1324&author=B.+Rossi&title=Advances+in+forecasting+under+instability) \| [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr16-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.1016%2FB978-0-444-62731-5.00021-X)
- Sims, C. A. 1980. Macroeconomics and reality. Econometrica 48: 1–48. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=1980&pages=1-48&author=C.+A.+Sims&title=Macroeconomics+and+reality) \| [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr17-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.2307%2F1912017)
- Stock, J. H., Watson, M. W. 1996. Evidence on structural instability in macroeconomic time series relations. Journal of Business & Economic Statistics 14: 11–30. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=1996&pages=11-30&author=J.+H.+Stock&author=M.+W.+Watson&title=Evidence+on+structural+instability+in+macroeconomic+time+series+relations)
- Stock, J. H. 1999. Forecasting inflation. Journal of Monetary Economics 44: 293–335. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=1999&pages=293-335&author=J.+H.+Stock&title=Forecasting+inflation) \| [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr19-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.1016%2FS0304-3932(99)00027-6)
- Stock, J. H. 2001. Vector autoregressions. Journal of Economic Perspectives 15: 101–115. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=2001&pages=101-115&author=J.+H.+Stock&title=Vector+autoregressions) \| [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr20-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.1257%2Fjep.15.4.101)
- Stock, J. H. 2003. Forecasting output and inflation: The role of asset prices. Journal of Economic Literature 41: 788–829. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=2003&pages=788-829&author=J.+H.+Stock&title=Forecasting+output+and+inflation%3A+The+role+of+asset+prices) \| [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr21-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.1257%2Fjel.41.3.788)
- Stock, J. H. 2006. Forecasting with many predictors. In Handbook of Economic Forecasting, vol. 1, ed. Elliott, G., Granger, C. W. J., Timmermann, A., 515–554. Amsterdam: Elsevier. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=2006&pages=515-554&author=J.+H.+Stock&title=Forecasting+with+many+predictors) \| [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr22-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.1016%2FS1574-0706(05)01010-4)
- Welch, I., Goyal, A. 2008. A comprehensive look at the empirical performance of equity premium prediction. Review of Financial Studies 21: 1455–1508. [Google Scholar](http://scholar.google.com/scholar_lookup?hl=en&publication_year=2008&pages=1455-1508&author=I.+Welch&author=A.+Goyal&title=A+comprehensive+look+at+the+empirical+performance+of+equity+premium+prediction) \| [Crossref](https://journals.sagepub.com/servlet/linkout?suffix=bibr23-1536867X19893631&dbid=16&doi=10.1177%2F1536867X19893631&key=10.1093%2Frfs%2Fhhm014) 
