DPD(dynamic panel-data)模型各类实操的陷阱

在做基于动态面板数据模型 (dynamic panel data, DPD) 的实证研究时, 我们经常产生一些疑惑, 比如: 我该使用一阶差分 GMM 还是 系统 GMM 呢? 想做系统 GMM，但是不知道怎么用实操命令。如果用长面板数据, GMM 估计是否依旧可行呢? 内生解释变量该怎么处理? 另外, 我们还会踏入各种各样的 "陷阱", 像: 序列相关检验一直通不过, 感觉无法再继续我的研究了。Sargan 检验一直拒绝原假设, 究竟是哪个工具变量出了问题?

这些问题不解决, 实证真的很难做下去。为此, 我们整理了 6 个问题和相应的解决方法, 希望为陷入困惑的以及即将做 DPD 的读者提供一些参考方法和避坑指南。讨论的问题有:

- 1. 应该选择 FD-GMM 还是 SYS-GMM?
- 2. 该用哪个 Stata 命令做模型的估计？
- 3. 当解释变量中含有内生变量，应该如何做模型估计？
- 4. 干扰项序列相关检验无法通过怎么办？
- 5. 过度识别检验无法通过怎么办？
- 6. 大 T 小 N 型面板能用 GMM 进行估计吗？



### 1. 应该选择 FD-GMM 还是 SYS-GMM？

首先，无论是 FD-GMM 还是 SYS-GMM，我们做实证设定的方程(模型)是不受影响的，二者只是计量估计方法不一样。在设定好动态面板模型之后，选择一阶差分 GMM 和系统 GMM 估计的区别在于利用的矩条件不同，系统 GMM 使用的矩条件多，利用的信息更多，估计更有效率，但是受到的限制也更多，需要假定被解释变量的一阶差分项与个体效应变量不相关。关于 FD-GMM 和 SYS-GMM 的区别可以看上一期的推文 []() (2.2节)。

那么，在估计模型时该选择哪种估计方法呢？Bond (2002)曾经做过模拟, 结果表明，当被解释变量一阶滞后项的系数 rho 不是很大时，例如 rho<0.8，FD-GMM 估计结果很好，但当 rho >0.8 时，SYS-GMM 比较好。

![Bond (2002), Table 1. 模拟结果表明，当 rho 不是很大时，例如 rho<0.8，FD-GMM 估计结果很好，但当 rho >0.8 时，SYS-GMM 比较好](https://images.gitee.com/uploads/images/2020/0309/155240_218e00f7_5320297.png)

此外, 如果方程中有个体效应的变量是感兴趣的解释变量, 比如在研究就业工资的影响因素时, 想要估计性别、户口、所属行业等不随时间变化(代表个体效应)变量对工资的影响时, 必须使用 SYS-GMM 估计。


### 2. 该用哪个 Stata 命令做模型的估计？

目前估计动态面板数据模型主要有 4 个 Stata 命令，其中官方命令为 `xtabond` `xtdpdsys` `xtdpd`,  非官方命令为 `xtabond2`。具体来说
- `xtabond` 做 FD-GMM 估计
- `xtdpdsys`做 SYS-GMM 估计
- `xtdpd` 既可做  FD-GMM 估计也可以做 SYS-GMM 估计;
- `xtabond2` 既可做 FD-GMM 和 SYS-GMM 估计。

推荐使用官方命令做模型的估计, 官方命令一般更加成熟，但有时候非官方命令有特别的优点, `xtabond2` 可以提供异方差稳健的 Hansen 统计量。下面通过举例来介绍 4 种命令使用方法。

* **动态面板模型范例**
使用 Arellano and Bond (1991) 关于英国就业的数据 abdata.dta, 研究企业雇佣员工数量的影响因素。动态就业模型的设定如下：
$$
n_{i t}=\alpha_{1} n_{i(t - 1)}+\alpha_{2} n_{i(t-2)}+\beta^{\prime}(L) x_{i t}+\lambda_{t}+\eta_{i}+v_{i t}
$$
- $n_{i t}$ 企业 $i$ 在第 $t$ 年末雇佣员工数量的对数值
- $n_{i(t - 1)}$ 企业 $i$ 在第 $t-1$ 年末雇佣员工数量的对数值
- $(L) x_{i t}$ 表示解释变量 $x$ 及其滞后项, 这里包括 $w_{i,t}$ $w_{i,t-1}$ (即企业 $i$ 在第 $t$ 和 $t-1$ 年实际工资的对数), $k_{i,t}$ 、$k_{i,t-1}$ 和 $k_{i,t-2}$ (即企业 $i$ 在第 $t$ $t-1$ 和 $t-2$ 年资本存量的对数),  $ys_{i,t}$、$ys_{i,t-1}$ 和 $ys_{i,t-2}$ (即企业 $i$ 所在行业在第 $t$、$t-1$ 和 $t-2$ 年总产出的对数)
- $\lambda_{t}$ 包括年份虚拟变量, 表示总需求每年受到的外部冲击, 可表示为 i.yr1979-yr1984
<br>

#### 2.1 模型的实现 (FD-GMM)

**采用 xtabond 命令 进行 FD-GMM 估计**

 **xtabond** 命令语法格式:
```Stata
xtabond depvar [indepvars] [if] [in] [,options]
```
主要选项的含义为：
- `depvar`: 被解释变量
- `indepvars`: 严格外生的解释变量
- `noconstant`: 无常数项
- `lags(p)`: 表示使用被解释变量 p 阶滞后值作为解释变量, 默认一阶滞后 lags(1)
- `maxldep(q)`: 表示最多使用 q 阶被解释变量的滞后值作为工具变量, 默认使用所有可能的滞后值
- `endogenous()`: 内生解释变量, 可使用多次
- `twostep`:  两阶段估计，可修正 Sargan 统计量
- `inst()`: 其他的工具变量 (除解释变量滞后项以外)
-  `vce()`:  默认为 vce(gmm), 计算得到普通 GMM 标准误, vce(robust) 异方差稳健稳健标准误

 **xtabond** 估计:
```Stata
webuse abdata, clear   // 调用数据
xtabond n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, noconstant lags(2) twostep vce(robust) // L(0/1).w 表示 w 的当期值与一阶滞后
est store FdGMMxtabond
```
<br>
**采用 xtdpd 命令 进行 FD-GMM 估计**

 **xtdpd** 命令语法格式:
```Stata
 xtdpd depvar [indepvars] [if] [in] , dgmmiv(varlist [...]) [options]
```
主要选项的含义为：
- `dgmmiv(varlist [, lagrange(flag [llag])])`:  指定被解释变量或者内生解释变量的滞后项作为差分方程的 GMM 式工具变量, 默认 flag 为 2, 比如 dgmmiv(y , lagrange(2 4)) 将变量的 y 的 2-4 阶滞后项作为工具变量, dgmmiv(y) 将变量的 y 的 2-n 阶滞后项作为工具变量
- `lgmmiv(varlist [, lag(#)])`： 将被解释变量或者内生解释变量的一阶差分的 # 阶滞后作为水平方程的 GMM 型工具变量，默认阶数为1
- `iv(varlist [, nodifference])`: 指定外生变量的一阶差分项作为差分方程的标准型的工具变量, 加上 nodifference 后作为水平方程的标准型的工具变量
- `div(varlist)`： 为差分方程设定额外的标准型工具变量
- `liv(varlist)`:  为水平方程设定额外的标准型工具变量
- `twostep` 和 `vce()` 含义与 xtabond 命令相同

 **xtdpd** 估计:
```Stata
xtdpd n l(1/2).n L(0/1).w L(0/2).(k ys) year yr1980-yr1984, dgmmiv(n) div(L(0/1).w L(0/2).(k ys)) ///
div(year yr1980-yr1984) nocons  twostep vce(robust)
est store FdGMMxtdpd
```
<br>
**采用 xtabond2 命令 进行 FD-GMM 估计**

**xtabond2** 命令语法格式:
```Stata
xtabond2 depvar varlist [if] [in] [, gmm(varlist [, laglimits(a b) collapse)])  ///
iv(varlist [, equation()]) nolevel  nocons twostep robust
```
主要选项的含义为：
- `gmm()`:  指定 GMM 式工具变量, laglimits(a b) 限定具变量滞后的阶数, 默认为1到n,  collapse 系统自动删减工具变量地个数,
- `iv(varlist [, equation()]) ` 指定 IV 式工具变量, equation({diff | level | both}) 对差分方程和水平方程进行选择
- `nolevel`: 表示不估计水平方程, 即为 FD-GMM 估计
-  `robust`: 异方差稳健标准误

 **xtabond2** 估计:
```Stata
* ssc install xtabond2, replace // xtabond2 是外部命令需要下载
xtabond2 n l(1/2).n l(0/1).w l(0/2).(k ys) year yr1980-yr1984, ///
gmm(l.n) iv(l(0/1).w l(0/2).(k ys)) iv(year yr1980-yr1984) nolevel nocons twostep robust
est store FdGMMxtabond2
```

#### 2.2 模型的实现 (SYS-GMM)

**采用 xtdpdsys 命令 进行 SYS-GMM 估计**

**xtdpdsys** 命令语法格式:

```Stata
xtdpdsys depvar [indepvars] [if] [in] [,options]
```
主要选项的含义为：
- `noconstant`: 无常数项
- `lags(p)`: 表示使用被解释变量 p 阶滞后值作为解释变量, 默认一阶滞后 lags(1)
- `maxldep(q)`: 表示最多使用 q 阶被解释变量的滞后值作为工具变量, 默认使用所有可能的滞后值
-  `pre()`: 指定前定变量
- `endogenous ()`: 指定内生解释变量, 可使用多次
-  `twostep` 和 `vce()` 含义与 xtabond 命令相同

 **xtdpdsys** 估计:
```Stata
xtdpdsys n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, ///
noconstant lags(2) twostep vce(robust)
est store Sys-GMM-xtdpdsys
```

**采用 xtdpd 命令 进行 SYS-GMM 估计**

**xtdpd** 估计:
```Stata
xtdpd n l(1/2).n L(0/1).w L(0/2).(k ys) year yr1980-yr1984, ///
dgmmiv(n) lgmmiv(n) div(L(0/1).w L(0/2).(k ys)) ///
div(year yr1980-yr1984) nocons twostep vce(robust)
est store SysGMMxtdpd
```

**采用 xtabond2  命令 进行 SYS-GMM 估计**

* **xtabond2** 估计:
```Stata
xtabond2 n l(1/2).n l(0/1).w l(0/2).(k ys) year yr1980-yr1984, ///
gmm(l(1/2).) iv(l(0/1).w l(0/2).(k ys),eq(diff)) iv(year yr1980-yr1984,eq(diff)) nocons twostep robust
est store SysGMMxtabond2
```

#### 2.3 模型估计结果
```stata
*- 结果输出
local mm "FdGMMxtabond FdGMMxtdpd FdGMMxtabond2 SysGMMxtdpdsys SysGMMxtdpd SysGMMxtabond2"
esttab `mm', mtitle(`mm') compress star(* 0.1 ** 0.05 *** 0.01) b(%10.3f)

*---------- table2：不同的 Stata 命令对于 FD-GMM 与 SYS-GMM 估计结果对比----------------

----------------------------------------------------------------------------------------
                 (1)          (2)          (3)          (4)          (5)          (6)   
           FdGMMxt~d    FdGMMxt~d    FdGMMxt~2    SysGMMx~s    SysGMMx~d    SysGMMx~2   
----------------------------------------------------------------------------------------
L.n            0.629***     0.629***     0.629***     0.942***     0.942***     1.061***
              (3.25)       (3.25)       (3.25)       (6.04)       (6.04)      (11.50)   

L2.n          -0.065       -0.065       -0.065       -0.074*      -0.074*      -0.091**
             (-1.45)      (-1.45)      (-1.45)      (-1.67)      (-1.67)      (-2.09)   

w             -0.526***    -0.526***    -0.526***    -0.587***    -0.587***    -0.548***
             (-3.40)      (-3.40)      (-3.40)      (-3.62)      (-3.62)      (-2.94)   

L.w            0.311        0.311        0.311        0.540***     0.540***     0.563**
              (1.53)       (1.53)       (1.53)       (2.60)       (2.60)       (2.35)   

k              0.278***     0.278***     0.278***     0.282***     0.282***     0.269***
              (3.82)       (3.82)       (3.82)       (3.69)       (3.69)       (3.94)   

L.k            0.014        0.014        0.014       -0.076       -0.076       -0.095   
              (0.15)       (0.15)       (0.15)      (-0.85)      (-0.85)      (-1.00)   

L2.k          -0.040       -0.040       -0.040       -0.088*      -0.088*      -0.118***
             (-0.93)      (-0.93)      (-0.93)      (-1.90)      (-1.90)      (-2.69)   

ys             0.592***     0.592***     0.592***     0.551***     0.551***     0.594***
              (3.42)       (3.42)       (3.42)       (3.05)       (3.05)       (3.51)   

L.ys          -0.566**     -0.566**     -0.566**     -0.740***    -0.740***    -0.777***
             (-2.17)      (-2.17)      (-2.17)      (-3.04)      (-3.04)      (-2.90)   

L2.ys          0.101        0.101        0.101        0.109        0.109        0.127   
              (0.62)       (0.62)       (0.62)       (0.58)       (0.58)       (0.64)   

yr1980         0.001        0.001        0.001        0.013        0.013        0.023   
              (0.04)       (0.04)       (0.04)       (0.74)       (0.74)       (1.52)   

yr1981        -0.055*      -0.055*      -0.055*      -0.027       -0.027       -0.014   
             (-1.76)      (-1.76)      (-1.76)      (-0.85)      (-0.85)      (-0.49)   

yr1982        -0.076*      -0.076*      -0.076*      -0.031       -0.031       -0.017   
             (-1.81)      (-1.81)      (-1.81)      (-0.92)      (-0.92)      (-0.61)   

yr1983        -0.074       -0.074       -0.074       -0.016       -0.016        0.002   
             (-1.40)      (-1.40)      (-1.40)      (-0.42)      (-0.42)       (0.06)   

yr1984        -0.091       -0.091       -0.091       -0.020       -0.020       -0.004   
             (-1.41)      (-1.41)      (-1.41)      (-0.53)      (-0.53)      (-0.12)   

year           0.011        0.011        0.011        0.000        0.000        0.000   
              (0.96)       (0.96)       (0.96)       (1.02)       (1.02)       (0.36)   
----------------------------------------------------------------------------------------
N                611          611          611          751          751          751   
----------------------------------------------------------------------------------------
t statistics in parentheses
* p<0.1, ** p<0.05, *** p<0.01

```
<br>
解读: 据表 2 显示,  雇佣员工数量对数的一阶滞后项的系数在 FD-GMM 估计在0.6左右, 而 SYS-GMM 估计的系数为 0.9 左右,  SYS-GMM 估计结果整体偏高, 且各个系数显著性水平更高。对于 FD-GMM 估计, 三种命令估计结果相同, 故在做差分 GMM 时, 选择三者之中任意的工具对结果没有影响。而对于 SYS-GMM 估计, `xtdpdsys` 和  `xtdpd` 结果一致, `xtabond2` 估计结果与前面两个命令有差异。
<br>

### 3. 当解释变量中含有内生变量，应该如何做模型估计？

对于模型

$y_{it} = a_0 y_{it} + a_1 x_{it} + a_2 w_{it} + u_i + v_{i t-1}$

$y_{i t-1}$ 是一个典型的内生变量, 我们用 L.(2/.)$y_{it}$ 作为 D.$y_{it-1}$ 的工具变量。因此, 如果假设 $w_{i t}$ 为内生变量, 我们用 L.(2/.)$w_{it}$ 作为 $w_{i t}$ 的工具变量。

在 Stata 中,
- 如果用 `xtabond` 命令就使用 `endogenous()` 选项来设定内生变量
- 如果用 `xtdpd` 命令则使用 `dgmmiv()` 选项来设定
- 如果用 `xtabond2` 命令就使用 `gmm()` 选项来设定
- 如果用 `xtdpdsys` 命令同样使用`endogenous()` 选项来设定


```Stata
* - 依旧以  abdata.dta (1991) 数据为例, 假设 资本存量 k 为内生变量,  k 的设定如下
xtabond n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, nocons lags(2) endog(k) twostep vce(robust)

xtdpd n l(1/2).n L(0/1).w L(0/2).(k ys) year yr1980-yr1984, dgmmiv(n k) div(L(0/1).w L(0/2).ys) ///
div(year yr1980-yr1984) nocons  twostep vce(robust)

xtabond2 n l(1/2).n l(0/1).w l(0/2).(k ys) year yr1980-yr1984, ///
gmm(l.n l.k) iv(l(0/1).w l(0/2).ys) iv(year yr1980-yr1984) nolevel nocons twostep robust

xtdpdsys n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, ///
nocons endog(k) lags(2) twostep vce(robust)
```
<br>

### 4. 干扰项序列相关检验无法通过怎么办？

 Arellano–Bond 估计工具变量的设定的关键点在于
$$
E\left(\Delta y_{i(t-j)} \Delta \varepsilon_{i t}\right)=0 \quad j \geq 2
$$
即 FD-GMM 与 SYS-GMM 能够成立的前提是, 扰动项 {${\varepsilon_{i,t}}$} 不存在序列相关。如果存在序列相关, 就会导致内生性问题，所以要进行检验。原假设是 "扰动项 {${\varepsilon_{i,t}}$} 不存在序列相关" 。

首先 "扰动项的一阶差分" 存在自相关因为
$$
\operatorname{Cov}\left(\Delta \varepsilon_{i t}, \Delta \varepsilon_{i, t-1}\right) =\operatorname{Cov}\left(\varepsilon_{i t}-\varepsilon_{i, t-1}, \varepsilon_{i, t-1}-\varepsilon_{i, t-2}\right) = \operatorname{Cov}\left(\varepsilon_{i, t-1}, - \varepsilon_{i, t-1}\right)=-\operatorname{Var}\left(\varepsilon_{i, t-1}\right) \neq 0$$

但是扰动项的差分不存在二阶或更高的自相关, 即$$\operatorname{Cov}\left(\Delta \varepsilon_{i t}, \Delta \varepsilon_{i, t-k}\right)=0, k \geqslant 2$$  故可以通过检验扰动项的差分是否存在二阶(或更高阶)的自相关来检验原假设, 使用的 Stata 命令为 `estat abond`。
<br>
```stata
xtabond n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, noconstant lags(2) twostep vce(robust)
 estat abond
*----------使用 estat abond 命令 table3 ----------------------
Arellano-Bond test for zero autocorrelation in first-differenced errors
  +-----------------------+
  |Order |  z     Prob > z|
  |------+----------------|
  |   1  |-2.1255  0.0335 |
  |   2  |-.35166  0.7251 |
  +-----------------------+
   H0: no autocorrelation
```
解读: 结果显示, 在 5% 的显著性水平下, 扰动项的差分存在一阶自相关, 但不存在二阶自相关, 故接受原假设 "{$\varepsilon_{i,t}$} 不存在序列相关", 通过了序列相关检验。
<br>

如果出现无法通过序列相关检验的现象, 主要的原因是: 干扰项中可能包含了一些序列相关特征较为明显的变量。我们可以增加一些解释变量，以便让干扰项变得尽可能**干净**一些。常用的设定方法如下：
- 加入时间虚拟变量，如年度虚拟变量；
- 进一步加入部分解释变量的滞后项 (要有一定的理论依据)
- 加入被解释变量的滞后项，可以通过 `xtabond` 命令的 `lag(#)` 选项来设定

需要特别说明的是，序列相关检验和过度识别检验往往要同步进行。因为，如果选择的工具变量不妥 (表现为过度识别检验无法通过)，序列相关检验也就没有了根基。

### 5. 过度识别检验无法通过怎么办？

在使用动态面板模型做估计时, 一般会使用较多的工具变量, 远超过了内生变量的个数, 需要进行过度识别检验。原理是检验工具变量是否是与干扰项相关，即工具变量是否为外生变量。原假设是：所有工具变量都是外生, 在执行过 `xtabond` 命令以后, 使用 `estat sargan` 命令。
```stata
quietly xtabond n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, ///
  noconstant lags(2)   // 不使用 VCE(robust)
estat sargan  // 过度识别检验

Sargan test of overidentifying restrictions
        H0: overidentifying restrictions are valid

        chi2(25)     =  65.81806
        Prob > chi2  =    0.0000
```
解读：结果拒绝原假设，认为存在工具变量和干扰项是相关的，没有通过过度识别检验。 但是 Arellano and Bond (1991) 指出，当干扰项存在异方差时， Sargan 检验倾向于过度拒绝原假设，因此得到的结论并不可信。建议采用两阶段估计，再执行 Sargan 检验比较稳妥。

```Stata
. quietly xtabond n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, twostep ///
noconstant lags(2)   // 不使用 VCE(robust)

. estat sargan  // 过度识别检验
Sargan test of overidentifying restrictions
        H0: overidentifying restrictions are valid

        chi2(25)     =  31.38143
        Prob > chi2  =    0.1767
```
解读: 结果没有拒绝 "所有变量都是外生" 的原假设, 即意味着工具变量与扰动项不相关, 是有效工具变量。
<br>

* Sargan 检验是联合检验: (1) 模型的设定正确 (2) 工具变量合理。

因此若拒绝原假设，即 p < 0.1, 说明前面两个假设至少有一个存在问题，此时应该先考虑模型的设定是否有问题，进而分析工具变量的设定是否合理。本质上我们选择的工具变量与干扰项相关。这与模型设定 (包含哪些解释变量？使用哪些滞后项作为工具变量) 是密切相关的。


使用 $y_{t-s} (s\geq 2)$ 作为 $\Delta y_{it-1}$ 的工具变量，但当 $T$ 较大时，就会有很多个工具变量。显然，随着 $s$ 的增加，$\rho_s = corr(\Delta y_{it-1}, y_{t-s})$ 会越来越小。通常而言，当 $s \geq 6$ 以后，对应的工具变量非常有可能是弱工具变量，即 $y_{t-7}$  与 $\Delta y_{it-1}$ 的相关性很低。

减少工具变量的使用，可以避免弱工具变量问题。就 Stata 而言，在 `xtabond` 命令中，可以使用 `maxldep(#)` 来限制工具变量的最大滞后阶数，进而达到限制工具变量总数的目的。设定 `xtabond y x, maxldep(5)` 意味着最多只用五个滞后项 (注意：不是五阶滞后)，即用 $y_{t-2}, \cdots y_{t-6}$ 作为 $\Delta y_{it-1}$ 的工具变量。此外, 表 4 也表明只用两个工具变量与用多个工具变量得到的结果非常相近。所以我们不用太担心, 工具变量减少, 会对回归结果造成很大影响。此外, 如果存在其他内生变量, 也可以限制其工具变量的滞后阶数。


![Bond (2002) Table 4. 只用两个工具变量与用多个工具变量得到的结果非常相近](https://gitee.com/uploads/images/2019/0429/180458_b0fd6dc2_1522177.png)

使用 maxldep() 选项前 GMM 式工具变量为L(2/.).n

```Stata
. xtabond n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, twostep ///
noconstant lags(2)
--------------------------------------------------------------
Instruments for differenced equation
        GMM-type: L(2/.).n
        Standard: D.w LD.w D.k LD.k L2D.k D.ys LD.ys L2D.ys D.yr1980
                  D.yr1981 D.yr1982 D.yr1983 D.yr1984 D.year
--------------------------------------------------------------
```

使用 maxldep() 选项 GMM 式工具变量为 L(2/6).n

```Stata
. xtabond n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, twostep ///
noconstant lags(2)  maxldep(5)

--------------------------------------------------------------
Instruments for differenced equation
        GMM-type: L(2/6).n
        Standard: D.w LD.w D.k LD.k L2D.k D.ys LD.ys L2D.ys D.yr1980
                  D.yr1981 D.yr1982 D.yr1983 D.yr1984 D.year
-------------------------------------------------------------
```

* 使用其他动态面板回归命令的方法是:
  - `xtdpd` 命令, 使用 `dgmmiv(n , lagrange(2 5))`,  将被解释变量的 n 的 2-5 阶滞后项作为 $\Delta n_{it-1}$ 的工具变量
  - `xtabond2` 命令, 使用 `gmm(l.n , laglimits(2 5) collapse)) `, laglimits() 限制滞后项的阶数, collapse 系统自动删减工具变量
  - `xtdpdsys` 命令, 使用 `maxldep(5)`: 最多使用 5 阶被解释变量的滞后值作为工具变量

<br>

### 6. 大 T 小 N 型面板能用 GMM 进行估计吗？

上面介绍的 FD-GMM 和 SYS-GMM 适用于 大 N 小 T 的短面板数据，当我们研究长面板数据 (大 T 小 N) 时，比如省级面板数据，GMM 估计可能不是最有效的。根据 Bruno(2005) 的做法, 基于 Bootstrap 偏差纠正的 LSDV 估计可以在一定程度上克服 n 较小的问题。下面通过数据模拟 (DGP) 的例子, 来对 FD-GMM,  SYS-GMM 和 bias corrected LSDV 的估计效果进行比较。

* **数据生成过程 (DPG)**
$y_{it} = 0.4y_{it}+0.9x_{i,t}+u_i+u_t+e_{i t-1}$
$corr(x_{it}, u_i) \neq 0$
$x_{it} = 0.2x_{i t-1} +  v_{i t}$
面板的数据设定个体为 10, 一共 60 期, 即 $ N = 10$, $T = 60$

```stata
clear all
help xtarsim //  xtarsim 是模拟生成面板数据的命令
xtarsim y x eta, n(10) t(60) gamma(0.9) beta(0.8) rho(0.2) one(corr 1) sn(9) seed(1234)
```

* **模型估计**
用 `xtabond` 和 `xtdpdsys`命令进行 FD-GMM 和 SYS-GMM 估计：
```stata
xtabond y x
est store FdGMM
xtdpdsys y x
est store SysGMM
```

用 `xtlsdvc` 命令进行 bias corrected LSDV 估计：
```stata
help xtlsdvc // 外部命令, 需要下载安装词命令
xtlsdvc y x, initial(ab)  vcov(50)  // vcov(50) 计算标准误、显著性，需要较长等待时间
* initial(ab) 表示选择 Arellano 和 Bond (1991) 提出的 FD-GMM 估计为偏差纠正的初始量
est store LSDVab
xtlsdvc y x, initial(bb)  vcov(50)  // vcov(50) 计算标准误、显著性，需要较长等待时间
* initial(ab) 表示选择 Blundell 和 Bond (1998) 提出的 Sys-GMM 估计为偏差纠正的初始量
est store LSDVbbs
```

最终结果为:
```Stata
local mm "FdGMM SysGMM LSDVab LSDVbb"
esttab `mm', mtitle(`mm') compress star(* 0.1 ** 0.05 *** 0.01)

*---------- table5：关于长面板数据模型的估计结果对比 1----------------
--------------------------------------------------------------
                 (1)          (2)          (3)          (4)   
               FdGMM       SysGMM       LSDVab       LSDVbb   
--------------------------------------------------------------
L.y            0.874***     0.870***     0.894***     0.896***
             (50.50)      (55.79)      (54.83)      (55.32)   

x              0.842***     0.863***     0.836***     0.837***
             (19.20)      (20.95)      (21.71)      (21.75)   

_cons          0.105**      0.109**                           
              (2.33)       (2.42)                             
--------------------------------------------------------------
N                580          590          590          590   
--------------------------------------------------------------
t statistics in parentheses
* p<0.1, ** p<0.05, *** p<0.01

```

把数据模拟中的 $y_{i t-1}$ 的系数由 0.9 变为 0.4, 重复上述过程，再对估计结果进行比较

```stata
clear all
xtarsim y x eta, n(10) t(60) gamma(0.4) beta(0.8) rho(0.2) one(corr 1) sn(9) seed(1234)
xtabond y x
est store FdGMM
xtdpdsys y x
est store SysGMM
xtlsdvc y x, initial(ab)  vcov(50)  
est store LSDVab
xtlsdvc y x, initial(bb)  vcov(50)
est store LSDVbbs
local mm "FdGMM SysGMM LSDVab LSDVbb"
esttab `mm', mtitle(`mm') compress star(* 0.1 ** 0.05 *** 0.01)
```

最终结果为:
```Stata
*---------- table6：关于长面板数据模型的估计结果对比 2----------------
--------------------------------------------------------------
                 (1)          (2)          (3)          (4)   
               FdGMM       SysGMM       LSDVab       LSDVbb   
--------------------------------------------------------------
L.y            0.393***     0.390***     0.396***     0.396***
             (27.72)      (32.09)      (26.65)      (26.17)   

x              0.812***     0.812***     0.812***     0.812***
             (58.10)      (61.98)      (67.61)      (66.06)   

_cons          0.589***     0.593***                          
             (13.03)      (13.28)                             
--------------------------------------------------------------
N                580          590          590          590   
--------------------------------------------------------------
t statistics in parentheses
* p<0.1, ** p<0.05, *** p<0.01
```
结论: (1) 当 $y_{i t-1}$ 的系数为 0.4, XTLSDVC 的优势非常有限; (2) 当 $y_{i t-1}$ 的系数为 0.9 时, XTLSDVC 的优势比较明显。



## 参考文献

- Arellano M., S. Bond, 1991, Some tests of specification for panel data: monte carlo evidence and an application to employment equations. The Review of Economic Studies, 58(2), 277-297. [[PDF]](http://www.econ.uiuc.edu/~econ508/Papers/arellanobond91.pdf)

- Blundell R.W., and S.R. Bond, 1998, Initial Conditions and Moment Restrictions in Dynamic Panel Data Models, Journal of Econometrics,87,115-143

- Bond, S., 2002, Dynamic panel data models: A guide to micro data methods and practice, Portuguese Economic Journal, 1 (2): 141-162. [[PDF]](https://pdfs.semanticscholar.org/35ff/6451f2b539f436924c8ebfe6a5398ca3cedf.pdf)

- Bruno, G. S. F., 2005), Approximating the bias of the LSDV estimator for dynamic unbalanced panel data models. Economics Letters, 87(3), 361–366.[[PDF]](https://sci-hub.tw/10.1016/j.econlet.2005.01.005#)



## Appendix 本文涉及的 Stata 代码
```Stata
webuse abdata, clear   // 调用数据
*- 准备工作
ssc install xtabond2, replace // 下载外部命令 xtabond2
ssc install xtarsim, replace  // 下载外部命令 xtarsim
ssc install xtlsdvc, replace  // 下载外部命令 xtlsdvc


*- DPD 估计
xtabond n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, noconstant lags(2) twostep vce(robust) // L(0/1).w 表示 w 的当期值与一阶滞后
est store FdGMMxtabond

xtdpd n l(1/2).n L(0/1).w L(0/2).(k ys) year yr1980-yr1984, dgmmiv(n) div(L(0/1).w L(0/2).(k ys)) ///
div(year yr1980-yr1984) nocons  twostep vce(robust)
est store FdGMMxtdpd

xtabond2 n l(1/2).n l(0/1).w l(0/2).(k ys) year yr1980-yr1984, ///
gmm(l(1/2).n) iv(l(0/1).w l(0/2).(k ys)) iv(year yr1980-yr1984) nolevel nocons twostep robust
est store FdGMMxtabond2

xtdpdsys n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, ///
noconstant lags(2) twostep vce(robust)
est store SysGMMxtdpdsys

xtdpd n l(1/2).n L(0/1).w L(0/2).(k ys) year yr1980-yr1984, ///
dgmmiv(n) lgmmiv(n) div(L(0/1).w L(0/2).(k ys)) ///
div(year yr1980-yr1984) nocons twostep vce(robust)
est store SysGMMxtdpd

xtabond2 n l(1/2).n l(0/1).w l(0/2).(k ys) year yr1980-yr1984, ///
gmm(l.n) iv(l(0/1).w l(0/2).(k ys),eq(diff)) iv(year yr1980-yr1984,eq(diff)) nocons twostep robust
est store SysGMMxtabond2

*- 结果输出
local mm "FdGMMxtabond FdGMMxtdpd FdGMMxtabond2 SysGMMxtdpdsys SysGMMxtdpd SysGMMxtabond2"
esttab `mm', mtitle(`mm') compress star(* 0.1 ** 0.05 *** 0.01) b(%10.3f)

*- 存在内生变量 k
xtabond n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, nocons lags(2) endog(k) twostep vce(robust)

xtdpd n l(1/2).n L(0/1).w L(0/2).(k ys) year yr1980-yr1984, dgmmiv(n k) div(L(0/1).w L(0/2).ys) ///
div(year yr1980-yr1984) nocons  twostep vce(robust)

xtabond2 n l(1/2).n l(0/1).w l(0/2).(k ys) year yr1980-yr1984, ///
gmm(l.n l.k) iv(l(0/1).w l(0/2).ys) iv(year yr1980-yr1984) nolevel nocons twostep robust

xtdpdsys n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, ///
nocons endog(k) lags(2) twostep vce(robust)

*- 序列相关检验
xtabond n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, noconstant lags(2) vce(robust)
 estat abond

*- 过度识别检验
quietly xtabond n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, ///
  noconstant lags(2)   // 不使用 VCE(robust) 一步估计
estat sargan  
quietly xtabond n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, twostep ///
  noconstant lags(2)      // 两部估计
estat sargan  

*- 限制工具变量的数量

xtabond n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, twostep ///
noconstant lags(2)  maxldep(5)

*- 面板数据生成
clear all
xtarsim y x eta, n(10) t(60) gamma(0.9) beta(0.8) rho(0.2) one(corr 1) sn(9) seed(1234)

*- 模型估计
xtabond y x
est store FdGMM
xtdpdsys y x
est store SysGMM

xtlsdvc y x, initial(ab)  vcov(50)  // vcov(50) 计算标准误、显著性，需要较长的等待时间
* initial(ab) 表示选择 Arellano 和 Bond (1991) 提出的 FD-GMM 估计为偏差纠正的初始量
est store LSDVab
xtlsdvc y x, initial(bb)  vcov(50)  // vcov(50) 计算标准误、显著性，需要较长的等待时间
* initial(ab) 表示选择 Blundell 和 Bond (1998) 提出的 Sys-GMM 估计为偏差纠正的初始量
est store LSDVbb

*- 估计结果
local mm "FdGMM SysGMM LSDVab LSDVbb"
esttab `mm', mtitle(`mm') compress star(* 0.1 ** 0.05 *** 0.01)

*- 面板数据生成
clear all
xtarsim y x eta, n(10) t(60) gamma(0.4) beta(0.8) rho(0.2) one(corr 1) sn(9) seed(1234)

*- 模型估计
xtabond y x
est store FdGMM
xtdpdsys y x
est store SysGMM
xtlsdvc y x, initial(ab)  vcov(50)  
est store LSDVab
xtlsdvc y x, initial(bb)  vcov(50)
est store LSDVbb

*- 估计结果
local mm "FdGMM SysGMM LSDVab LSDVbb"
esttab `mm', mtitle(`mm') compress star(* 0.1 ** 0.05 *** 0.01)
```
