动面板数据模型与一阶差分 GMM 估计
&emsp;


&emsp;

> 作者：陆苏怡 (中山大学)，李琼琼(山东大学)
>
> Stata 连享会：  [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn) | [StataChina公众号](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

本文主要内容安排如下：
- 1. 简介
- 2. 动态面板数据模型的设定与类型
   *  2.1 模型基本设定
   *  2.2 模型的类型
       2.2.1  固定效应模型
       2.2.2  随机效应模型
   *  2.3 思考
- 3.  估计方法
   * 3.1 IV
   * 3.2 GMM
   * 3.3 FD - GMM
- 4. 动态面板模型范例及 Stata 命令
   * 4.1 FD-GMM 估计的 Stata 命令
   * 4.2 动态面板模型范例及 Stata 的实现
- 5. 总结
- 参考文献
- Appendix 本文涉及的 Stata 代码


---
> ## 1.  简介
动态面板数据模型 (dynamic panel data model)，是指通过在静态面板数据模型中引入滞后被解释变量以反映动态滞后效应的模型。这种模型的特殊性在于被解释变量的动态滞后项与随机误差组成部分中的个体效应相关，从而造成估计的内生性。

也就是说在动态面板模型允许使用过去的可观测值，考虑了过去结果对当前结果的影响。而实际上许多经济学问题本质上都是动态的，其中一些经济模型表明，当前的行为依赖于过去的行为，例如 **就业模型** 和 **公司投资问题**。

<br>

---

> ## 2.动态面板数据模型的设定与类型

### 2.1 模型基本设定
* **AR(1) 模型**
  $$ y_{i,t}=\delta y_{i,t-1}+x_{i,t}^{\prime} \beta+\alpha_{i}+\varepsilon_{i,t} \quad (1)$$  $$ i=1,...,N \qquad t=1,...,T$$

<br>
  - $y_{i,t}$ 是个体 $i$ 在时间 $t$ 上的被解释变量
   - $x_{i,t}$ 是个体的外生解释变量矩阵，$\beta$ 为其响应参数向量
  - $\alpha_{i}$ 是特定效应，且有 $\alpha_{i} \sim (0,\sigma_{\alpha}^2)$
  - $\varepsilon_{i,t}$是服从 $i.i.d$ 分布的干扰项，且有$\varepsilon_{i,t} \sim_{iid} (0,\sigma_{\epsilon}^2)$

### 2.2  模型的类型

**2.2.1  固定效应模型 (Fixed effect)**

通过消去特定效应项$\alpha_{i}$得到

$$y_{i,t}- \overline{y}_{i}=\delta (y_{i,t-1}- \overline{y}_{i.-1})+(x_{i,t}-\overline{x}_{i})\beta+(\varepsilon_{i,t}- \overline{\varepsilon}_{i})\quad (2)$$ $$ \overline{y}_{i,-1}=\sum_{t=2}^{T}\frac{y_{i,t-1}}{T-1}$$

但是此时 $(y_{i,t-1}- \overline{y}_{i.-1})$ 与 $(\varepsilon_{i,t}- \overline{\varepsilon}_{i})$ 相关，尽管是在 $\varepsilon_{i,t}$ 是序列不相关的的情况下。这是因为 $y_{i,-1}$ 与 $\overline{\varepsilon}_{i}$ 相关，后一项的均值包含了$\varepsilon_{i,t-1}$ 这一项，明显与 $y_{i,t-1}$ 是相关的。这可以看出，若使用广义最小二乘法 (GLS)，固定效应模型的估计是有偏的。

<br>

**2.2.2 随机效应模型 (Random effect)**

若使用广义最小二乘法 (GLS)，同理可知，$(y_{i,t-1}-\theta \overline{y}_{i.-1})$ 与$(\varepsilon_{i,t}-\theta \overline{\varepsilon}_{i})$是相关的。

因此，可以看出，对于一个动态面板数据模型来说，若使用 GLS 估计方法，不管是固定效应模型还是随机效应模型，它的估计都是有偏且不一致连续的。且偏误 $(bias)$ 的阶数是 $\frac{1}{T}$，仅当$T\rightarrow \infty$时$bias\rightarrow0$。但是当 $T$ 很小，$N\rightarrow \infty$ 时可能会引起很大的偏误。
<br>
### 2.3 思考
提出问题：为什么当 $T$ 固定，$N\rightarrow \infty $时会出现偏误和不一致连续的情况？

  - 首先考虑没有外生向量的模型：

$$y_{i,t}=\delta y_{i,t-1}+\alpha_{i}+\varepsilon_{i,t} \quad \left| \delta\right|<1 \quad (3)$$

  - 假设，对于 $y_{i,t}$ 我们在 $t=0,1,...,T$ 时有观测值，
则固定效应模型的估计量为

 $$\hat{\delta}_{FE}=\frac{\sum_{i=1}^{N}\sum_{t=1}^{T}(y_{i,t}- \overline{y}_{i})(y_{i,t-1}- \overline{y}_{i,-1})}{\sum_{i=1}^{N}\sum_{t=1}^{T}(y_{i,t-1}- \overline{y}_{i,-1})^2 }\quad (4)$$
其中
$$ \overline{y}_{i,t}=\frac{1}{T}\sum_{t=1}^{T}y_{i,t} \  \quad  \overline{y}_{i,-1}=\frac{1}{T}\sum_{t=1}^{T}y_{i,t-1}   $$

 - 将等式 (3) 代入等式 (4) 中，可以得到估计量的特征

$$\hat{\delta }_{FE}=\delta+\frac{\frac{1}{NT}\sum_{i=1}^{N}\sum_{t=1}^{T}(\varepsilon_{i,t}- \overline{\varepsilon}_{i})(y_{i,t-1}- \overline{y}_{i,-1})}{\frac{1}{NT}\sum_{i=1}^{N}\sum_{t=1}^{T}(y_{i,t-1}- \overline{y}_{i,-1})^2}\quad (5)$$


 - 等式 (5) 中，当 $T$ 固定，$N\rightarrow \infty $的情况下，FE模型的估计值有偏且不一致。因为等式 (5) 右边最后一项的期望不为零，且在$N\rightarrow \infty $的情况下不收敛到零。Nickell (1981) 和 Hsia (2003) 曾提出过：$$plim_{n\rightarrow \infty}\frac{1}{NT}\sum_{i=1}^{N}\sum_{t=1}^{T}(\varepsilon_{i,t}- \overline{\varepsilon}_{i})(y_{i,t-1}- \overline{y}_{i,-1})=-\frac{\delta_{\varepsilon}^2}{T^2}\times\frac{(T-1)-T_{\delta+\delta^T}}{(1-\delta)^2}\neq0 \quad (6)$$

<br>
因此，对于固定的 $T$，我们有不一致的估计量，不过这与 $\alpha_{i}$ 无关，$\alpha_{i}$ 已经在估计过程过被去掉了。这里的问题是被解释变量的动态滞后项与随机误差组成部分中的个体效应相关，如在等式 (2) 与 (5) 我们所遇到的问题，即 $y_{i,-1}$ 与 $\overline{\varepsilon}_{i}$相关。但当 $T\rightarrow \infty$ 时，等式 (6) 收敛到零，此时当 $T\rightarrow \infty $ 和 $N\rightarrow \infty$ 时，$\delta_{FE}$的估计是一致的。


&emsp;

---

>## 3. 估计方法
### 3.1   IV

为了解决估计量不一致的问题，Anderson 和 Hsio (1981) 提出了**工具变量估计 (IV)**。首先，我们考虑消去个体效应$\alpha_{i}$，对此，做差分得：

$$y_{i,t}-y_{i,t-1}=\delta(y_{i,t-1}-y_{i,t-2})+(\varepsilon_{i,t}-\varepsilon_{i,t-1})  \quad t=2,...,T\quad (7)$$

<br>

- 此处若使用**最小二乘法 (OLS)** 得到等式 (7) 的估计量是不一致的，尽管是在 $T\rightarrow \infty$ 的情况下，这是因为 $y_{i,t-1}$ 和 $\varepsilon_{i,t-1}$ 相关。

<br>

- 转换规范等式 (7) 给出一个 **IV 估计**的方法，例如，$y_{i,t-2}$与$(y_{i,t-1}-y_{i,t-2})$ 相关，但与 $\varepsilon_{i,t-1}$ 不相关，所以选取 $y_{i,t-2}$ 作为工具变量，给出了 $\delta$ 的一个 IV 估计：$$ \hat{\delta}_{IV}=\frac{\sum_{i=1}^{N}\sum_{t=2}^{T}y_{i,t-2}(y_{i,t}-y_{i,t-1})}{\sum_{i=1}^{N}\sum_{t=2}^{T}y_{i,t-2}(y_{i,t-1}-y_{i,t-2})} \quad (8)$$
对应的关于等式 (8) 的一致性条件为 $$plim_{N \rightarrow \infty}\frac{1}{N(T-1)}\sum_{i=1}^{N}\sum_{t=2}^{T}(\varepsilon_{i,t}-\varepsilon_{i,t-1})y_{i,t-2}=0  \quad (9) $$ $$T\rightarrow \infty \ or \  N\rightarrow \infty \ or \ T\rightarrow \infty \ and \ N \rightarrow \infty $$注意这里 $(\varepsilon_{i,t}-\varepsilon_{i,t-1})$ 是 MA(1)

<br>

- Anderson 和 Hsio (1981) 又给出了另一个  **IV 估计** 的方案，这里选择将 $(y_{i,t-2}-y_{i,t-3})$ 作为工具变量。
$$ \hat{\delta}_{IV}^{(2)}=\frac{\sum_{i=1}^{N}\sum_{t=3}^{T}(y_{i,t-2}-y_{i,t-3})(y_{i,t}-y_{i,t-1})}{\sum_{i=1}^{N}\sum_{t=3}^{T}(y_{i,t-2}-y_{i,t-3})(y_{i,t-1}-y_{i,t-2})} \quad (10)$$
同理，得到等式 (10) 的一致性条件为
$$plim_{N \rightarrow \infty}\frac{1}{N(T-2)}\sum_{i=1}^{N}\sum_{t=3}^{T}(\varepsilon_{i,t}-\varepsilon_{i,t-1})(y_{i,t-2}-y_{i,t-3})=0  \quad (11) $$
且只要 $\varepsilon_{i,t}$ 不是自相关的，等式 (8) 和 (11) 的一致性都得到了保障。

<br>

 可以看出，等式 (10) 构建工具变量时比等式 (8) 多引入了一个滞后项，这导致缺失了一期的样本数据。这就又提出了一个问题，我们究竟是选择等式 (8) 还是等式 (10) 的估计比较好？
而矩估计 (MM) 法则不需要考虑这个问题，MM 估计可以统一所有的估计量且消去样本容量减少的缺点。

### 3.2  GMM
可得等式 (9) 的矩条件为 $$plim_{N \rightarrow \infty}\frac{1}{N(T-1)}\sum_{i=1}^{N}\sum_{t=2}^{T}(\varepsilon_{i,t}-\varepsilon_{i,t-1})y_{i,t-2}=E[(\varepsilon_{i,t}-\varepsilon_{i,t-1})y_{i,t-2} ]=0  \quad (12) $$
同理，等式 (11) 的矩条件为
$$plim_{N \rightarrow \infty}\frac{1}{N(T-2)}\sum_{i=1}^{N}\sum_{t=3}^{T}(\varepsilon_{i,t}-\varepsilon_{i,t-1})(y_{i,t-2}-y_{i,t-3})=E[(\varepsilon_{i,t}-\varepsilon_{i,t-1})(y_{i,t-2}-y_{i,t-3})]=0  \ (13) $$
这两个 IV 估计值都在估计中附加了一个矩条件。但是，据我们所知，附加更多的矩条件则能增加估计值的效率。


据此，Arellano 和 Bond (1991) 提出可以通过开发附加矩来扩大工具的列表并且是工具的数量随着 t 变化。首先，固定 T，这里考虑 T = 4。
- t = 2 时，矩条件变为：$E[(\varepsilon_{i,2}-\varepsilon_{i,1})y_{i,0}]=0$
这意味着变量$y_{i,0}$是有效的工具，因为它与$(y_{i,2}-y_{i,1})$高度相关而与$(\varepsilon_{i,2}-\varepsilon_{i,1})$不相关。
- t = 3 时，同理可得，矩条件为：$E[(\varepsilon_{i,3}-\varepsilon_{i,2})y_{i,1}]=0$ 同时也满足$E[(\varepsilon_{i,2}-\varepsilon_{i,1})y_{i,0}]=0$
其中，$y_{i,0}\ y_{i,1}$与$(y_{i,2}-y_{i,1})$相关，而与$(\varepsilon_{i,3}-\varepsilon_{i,2})$不相关。
- t = 4 时，我们将得到三组矩条件
$$E[(\varepsilon_{i,2}-\varepsilon_{i,1})y_{i,0}]=0$$  $$E[(\varepsilon_{i,3}-\varepsilon_{i,2})y_{i,1}]=0$$  $$E[(\varepsilon_{i,4}-\varepsilon_{i, 3})y_{i,2}]=0$$

<br>

一直继续这么添加矩条件之后，有效的工具集合变为 $(y_{i0}, y_{i2}... y_{i,T-2})$
而所有的这些矩条件可以作为 **广义矩估计(GMM)** 的一个框架。对于一般的大 N 小 T，所有差分后的误差项可排成一个向量：
$$
\Delta \varepsilon_{i}=\left( \begin{array}{c}{\varepsilon_{i 2}-\varepsilon_{i 1}} \\\ {\cdots} \\\ {\varepsilon_{i, T}-\varepsilon_{i, T-1}}\end{array}\right)
$$
由工具变量排成的矩阵为：
$$
Z_{i} =\left( \begin{array}{ccccccc}{y_{i0}} & {0} & {0} & {\ldots} & {0} & {\ldots} & {0}\\ {0} & {y_{i0}} & {y_{i1}} & {\ldots} & {0} & {\ldots} & {0} \\ {\vdots} & {\vdots} & {\vdots} & {\ldots} & {\vdots} & {\ddots} & {\vdots}  \\ {0} & {0} & {0} & {\ldots} & {y_{i0}} & {\ldots} & {y_{i, T-2}} \end{array}\right) \ (15)
$$

<br>

矩阵 $Z_{i}$ 的每一行都包含了给定时段所有有效工具。因此，所有矩条件的集合可写为：
$$
E[Z_{i}^{\prime} \Delta \varepsilon_{i}]=0 \qquad (16)
$$
为了得到GMM估计，将等式  (16) 改写成
$$
E[Z_{i}^{\prime}\left(\Delta y_{i}-\gamma \Delta y_{i,-1}\right)]=0 \qquad (17)
$$

显然，矩条件的数量会增加未知参数的数量。
$\gamma$ 这里我们通过最小化由等式 (17) 所对应的条件表达的二次型来估计参数 $\gamma$：

$$
\min_{\gamma}\left[\frac{1}{N} \sum_{i=1}^{N} Z_{i}^{\prime}\left(\Delta y_{i}-\gamma \Delta y_{i,-1}\right)\right]^{\prime} W_{N}\left[\frac{1}{N} \sum_{i=1}^{N} Z_{i}^{\prime}\left(\Delta y_{i}-\gamma \Delta y_{i,-1}\right)\right]
 \quad (18)
$$

<br>

其中 $W_{i}$ 是定义的对称正定的权重矩阵。
然后，通过对等式 (18) 关于 $\gamma$ 求微分并求解 $\gamma$ 得到 GMM 估计量：
$$
\hat{\gamma}_{G M M}=\left(\left(\sum_{i=1}^{N} \Delta y_{i,-1}^{\prime} Z_{i}\right) W_{N}\left(\sum_{i=1}^{N} Z_{i}^{\prime} \Delta y_{i,-1}\right)\right)^{-1} \times\left(\sum_{i=1}^{N} \Delta y_{i,-1}^{\prime} Z_{i}\right) W_{N}\left(\sum_{i=1}^{N} Z_{i}^{\prime} \Delta y_{i}\right)
\ (19)$$

<br>

GMM 估计并不强制要求 $\varepsilon_{it}$ 关于个体与时间服从独立分布，但是需要注意的是，需要 **不存在自相关** 以保证矩条件有效。因此，在一个短面板（大 N 小 T），建议强制 $\varepsilon_{it}$ 不存在自相关，并结合同方差性的假设。

Alvarez 和 Arellano (2003) 表示，通常，GMM 估计在 $T \rightarrow \infty , N \rightarrow \infty$ 的情况下仍然保持一致性。但是，对于$T \rightarrow \infty $的情况下，GMM 估计和 FE 估计会很接近，这给我们估计方法提供了一个更具有吸引力的选择。

<br>

### 3.3 FD-GMM
Arellano 和 Bond ( 1991) 提出了 **一阶差分 GMM** (FD-GMM) 估计方法，主要做法是用变量的水平滞后值作为其一阶差分项的工具变量。具体来说：

-  由我们上文所考虑的没有外生向量的模型(3)的 **水平方程**  (level equation) $y_{i,t}=\delta y_{i,t-1}+\alpha_{i}+\varepsilon_{i,t} $，考虑它的 **差分方程** (differenced equation)
$$\Delta y_{it}=\delta\Delta y_{i,t-1} + \Delta \varepsilon_{it} \ (20)$$  因为 $\Delta y_{i,t-1}$ 与 $\Delta \varepsilon_{it}$ 相关联，所以将 $y_{i,t-2}$ 作为 $\Delta y_{i,t-1}$ 的工具变量, 并且 $y_{it}$ 两阶及以上滞后项都可以作为 $\Delta y_{i,t-1}$ 的工具变量。这种估计方法就是 FD-GMM。


&emsp;

---

## 4. 动态面板模型范例及 Stata 命令

这一章节, 我们介绍用 FD-GMM 估计动态面板模型的 Stata 命令, 然后使用 Arellano and Bond (1991) 研究英国就业的数据 abdata.dta 来做案例分析。
<br>
### 4.1 FD-GMM 估计的 Stata 命令
`xtabond` 命令用于估计线性动态面板数据模型, 被解释变量的滞后项对被解释变量有影响, 其影响效应被称为 Arellano–Bond 估计量。此命令将被解释变量的水平滞后项作为一阶差分项的工具变量, 是实现 FD-GMM 估计的 Stata 官方命令。

要求数据为大 N 小 T 的短面板数据, 并且模型的扰动项不存在序列相关。 `xtabond` 命令的主要语法格式为:

```Stata
xtabond depvar [indepvars] [if] [in] [,options]
```
主要选项的含义为：
- `depvar`: 被解释变量
- `indepvars`: 严格外生的解释变量
- `noconstant`: 无常数项
- `lags(p)`: 表示使用被解释变量 p 阶滞后值作为解释变量, 默认一阶滞后 lags(1)
- `maxldep(q)`: 表示最多使用 q 阶被解释变量的滞后值作为工具变量, 默认使用所有可能的滞后值
- `endogenous()`: 内生解释变量, 可使用多次
- `twostep`: 两阶段估计，可修正 Sargan 统计量
- `inst()`: 其他的工具变量 (除解释变量滞后项以外)
-  `vce()`:  默认为 vce(gmm), 计算得到普通 GMM 标准误, vce(robust) 异方差稳健稳健标准误

<br>

### 4.2 动态面板模型范例及 Stata 的实现
* **动态就业模型 (Arellano and Bond, 1991) 的设定**
$$
n_{i t}=\alpha_{1} n_{i(t - 1)}+\alpha_{2} n_{i(t-2)}+\beta^{\prime}(L) x_{i t}+\lambda_{t}+\eta_{i}+v_{i t}
$$
- $n_{i t}$ 企业 $i$ 在第 $t$ 年末雇佣员工数量的对数值
- $n_{i(t - 1)}$ 企业 $i$ 在第 $t-1$ 年末雇佣员工数量的对数值
- $(L) x_{i t}$ 表示解释变量 $x$ 及其滞后项, 这里包括 $w_{i,t}$ $w_{i,t-1}$(即企业 $i$ 在第 $t$ 和 $t-1$ 年实际工资的对数), $k_{i,t}$ 、$k_{i,t-1}$ 和 $k_{i,t-2}$ (即企业 $i$ 在第 $t$ $t-1$ 和 $t-2$ 年资本存量的对数),  $ys_{i,t}$、$ys_{i,t-1}$ 和 $ys_{i,t-2}$ (即企业所在行业在第 $t$、$t-1$ 和 $t-2$ 年总产出的对数)
- $\lambda_{t}$ 包括年份虚拟变量, i.yr1979-yr1984, 表示总需求受到的外部冲击
<br>
* **模型的实现**
```Stata
webuse abdata, clear   // 调用数据
xtabond n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, noconstant lags(2) twostep vce(robust) // L(0/1).w 表示 w 的当期值与一阶滞后

*----------使用 xtabond 命令 table1 ----------------------

. xtabond n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, noconstant lags(2) twostep vce(robust)

Arellano-Bond dynamic panel-data estimation     Number of obs     =        611
Group variable: id                              Number of groups  =        140
Time variable: year
                                                Obs per group:
                                                              min =          4
                                                              avg =   4.364286
                                                              max =          6

Number of instruments =     41                  Wald chi2(16)     =    1104.72
                                                Prob > chi2       =     0.0000
Two-step results
                                     (Std. Err. adjusted for clustering on id)
------------------------------------------------------------------------------
             |              WC-Robust
           n |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
           n |
         L1. |   .6287089   .1934138     3.25   0.001     .2496248    1.007793
         L2. |  -.0651882   .0450501    -1.45   0.148    -.1534847    .0231084
             |
           w |
         --. |  -.5257597   .1546107    -3.40   0.001     -.828791   -.2227284
         L1. |   .3112899   .2030006     1.53   0.125     -.086584    .7091638
             |
           k |
         --. |   .2783619   .0728019     3.82   0.000     .1356728    .4210511
         L1. |   .0140994   .0924575     0.15   0.879     -.167114    .1953129
         L2. |  -.0402484   .0432745    -0.93   0.352    -.1250649    .0445681
             |
          ys |
         --. |   .5919243   .1730916     3.42   0.001      .252671    .9311776
         L1. |  -.5659863   .2611008    -2.17   0.030    -1.077734   -.0542381
         L2. |   .1005433   .1610987     0.62   0.533    -.2152043    .4162908
             |
      yr1980 |   .0006378   .0168042     0.04   0.970    -.0322978    .0335734
      yr1981 |  -.0550044   .0313389    -1.76   0.079    -.1164275    .0064187
      yr1982 |   -.075978   .0419276    -1.81   0.070    -.1581545    .0061986
      yr1983 |  -.0740708   .0528381    -1.40   0.161    -.1776315      .02949
      yr1984 |  -.0906606   .0642615    -1.41   0.158    -.2166108    .0352896
        year |   .0112155   .0116783     0.96   0.337    -.0116735    .0341045
------------------------------------------------------------------------------
Instruments for differenced equation
        GMM-type: L(2/.).n
        Standard: D.w LD.w D.k LD.k L2D.k D.ys LD.ys L2D.ys D.yr1980
                  D.yr1981 D.yr1982 D.yr1983 D.yr1984 D.year

```
<br>
解读: 从回归结果看, (1) 上一年雇佣员工的数量对企业今年雇佣员工的数量有显著正向影响, 而过去两年雇佣员工的数量的影响则不显著;
(2) 实际工资对员工的数量有显著的负向效应, 而去年的实际工资却对员工的数量有显著正向效应;
(3) 资本存量会导致员工的数量增加, 但往年的资本存量不产生影响;
(4) 行业总需求对员工数量有显著正向影响, 而上一年的行业总需求有负向影响;
(5) 各年份对总需求的外部冲击对劳动力需求方面的作用并不显著。

表 1 的左上方显示, 所使用的全部工具变量一共 41 个，表的最下方显示具体的工具变量, 比如 "L(2/.).n" 为被解释变量 n 的二阶至尽可能多阶的滞后项, 即 $n_{i, t-2}$ $n_{i, t-3}$ $n_{i, t-4} $等；“L2D.k” 为变量 k 的二阶差分。
<br>
* **序列相关检验**
可以看出 Arellano–Bond 估计工具变量的设定的关键点在于
$$
E\left(\Delta y_{i(t-j)} \Delta \varepsilon_{i t}\right)=0 \quad j \geq 2
$$
故 FD-GMM 能够成立的前提是, 扰动项 {${\varepsilon_{i,t}}$} 不存在序列相关。如果存在序列相关, 就会导致内生性问题，所以要进行检验。假设"扰动项 {${\varepsilon_{i,t}}$} 不存在序列相关" 的原假设成立。 "扰动项的一阶差分" 存在自相关因为
$$
\operatorname{Cov}\left(\Delta \varepsilon_{i t}, \Delta \varepsilon_{i, t-1}\right) =\operatorname{Cov}\left(\varepsilon_{i t}-\varepsilon_{i, t-1}, \varepsilon_{i, t-1}-\varepsilon_{i, t-2}\right) = \operatorname{Cov}\left(\varepsilon_{i, t-1}, - \varepsilon_{i, t-1}\right)=-\operatorname{Var}\left(\varepsilon_{i, t-1}\right) \neq 0$$

但是扰动项的差分不存在二阶或更高的自相关, 即$$\operatorname{Cov}\left(\Delta \varepsilon_{i t}, \Delta \varepsilon_{i, t-k}\right)=0, k \geqslant 2$$  故可以通过检验扰动项的差分是否存在二阶(或更高阶)的自相关来检验原假设, 使用的 Stata 命令为 `estat abond`。
<br>

```stata
*----------使用 estat abond 命令 table2 ----------------------
. estat abond
Arellano-Bond test for zero autocorrelation in first-differenced errors
  +-----------------------+
  |Order |  z     Prob > z|
  |------+----------------|
  |   1  |-2.1255  0.0335 |
  |   2  |-.35166  0.7251 |
  +-----------------------+
   H0: no autocorrelation
```
解读: 结果显示, 在 5% 的显著性水平下, 扰动项的差分存在一阶自相关, 但不存在二阶自相关, 故接受 "{$\varepsilon_{i,t}$}不存在序列相关" 的原假设。
<br>
还可以做更高的自相关检验:
 ```stata
.estat abond, artests(4)
*----------使用 estat abond 命令 table3 ----------------------
Arellano-Bond test for zero autocorrelation in first-differenced errors
  +-----------------------+
  |Order |  z     Prob > z|
  |------+----------------|
  |   1  |-2.1255  0.0335 |
  |   2  |-.35166  0.7251 |
  |   3  | .26307  0.7925 |
  |   4  |-.05951  0.9525 |
  +-----------------------+
   H0: no autocorrelation
 ```
<br>

* **过度识别检验** (overidentification test)

关于动态就业模型使用了 41 个工具变量, 远超过了内生变量的个数, 需要进行过度识别检验。原理是检验工具变量是否是与干扰项相关，即工具变量是否为外生变量。原假设是：所有工具变量都是外生, 在执行过 `xtabond` 命令以后, 使用 `estat sargan`命令。

```Stata
. quietly xtabond n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, ///
  noconstant lags(2)   // 不使用 VCE(robust)
. estat sargan  // 过度识别检验
Sargan test of overidentifying restrictions
        H0: overidentifying restrictions are valid

        chi2(25)     =  65.81806
        Prob > chi2  =    0.0000
```
解读：结果拒绝原假设，认为存在工具变量和干扰项是相关的，没有通过过度识别检验。 但是 Arellano and Bond (1991) 指出，当干扰项存在异方差时， Sargan 检验倾向于过度拒绝原假设，因此得到的结论并不可信。建议采用两阶段估计，再执行 Sargan 检验比较稳妥。

- 使用两阶段估计 (twostep) 后的 Sargan Test 结果如下
```Stata
. quietly xtabond n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, ///
  noconstant lags(2)    // 不使用 VCE(robust)
. estat sargan
Sargan test of overidentifying restrictions
        H0: overidentifying restrictions are valid

        chi2(25)     =  31.38143
        Prob > chi2  =    0.1767
```

解读: 结果没有拒绝 "所有变量都是外生" 的原假设, 即意味着工具变量与扰动项不相关, 是有效工具变量。

*- Note: Sargan 检验是联合检验，包括 （1) 模型设定正确 , (2) 工具变量合理; 因此拒绝原假设时，即 p<0.10, 意味着上述两个假设至少有一个存在问题；此时应首先考虑模型设定是否有问题，进而分析工具变量的设定是否合理。关于工具变量的调整，后续推文会有更详细的介绍。
<br>

---

> ## 5.  总结

本文介绍了动态面板模型的基本设定，动态面板模型的固定效应和随机效应，以及模型的估计方法，重点对 FD-GMM 估计及 Stata 的实现进行了详细的说明。 以动态就业模型为例, 使用 `xtabond` 命令对模型中的系数进行 FD-GMM 估计, 并通过`estat abond` 和 `estat sargan` 命令做序列相关检验和过度识别检验。因此，做实证研究的过程中，当个体的当前行为取决于过去的行为时, 可以使用动态面板模型, 通过引入被解释变量的滞后值来可以实现对个体的动态行为进行建模。
<br>

---
## 参考文献

- Bond, S., 2002, Dynamic panel data models: A guide to micro data methods and practice, Portuguese Economic Journal, 1 (2): 141-162. [[PDF]](https://pdfs.semanticscholar.org/35ff/6451f2b539f436924c8ebfe6a5398ca3cedf.pdf)
http://www.econ.uiuc.edu/~econ508/Papers/arellanobond91.pdf
- Arellano M., S. Bond, 1991, Some tests of specification for panel data: monte carlo evidence and an application to employment equations. The Review of Economic Studies, 58(2), 277-297. [[PDF]](http://www.econ.uiuc.edu/~econ508/Papers/arellanobond91.pdf)
- [Dynamic panel data analysis | Stata](https://www.stata.com/features/overview/dynamic-panel-data/)
- [Dynamic Panel Data : IV and GMM Estimation with Stata (Panel)]([http://rizaudinsahlan.blogspot.com/2017/11/dynamic-panel-data-iv-and-gmm.html](http://rizaudinsahlan.blogspot.com/2017/11/dynamic-panel-data-iv-and-gmm.html))

## Appendix 本文涉及的 Stata 代码
```stata
webuse abdata, clear  // 调用数据
*----------使用 xtabond 命令 table1 ----------------------
xtabond n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, noconstant lags(2) twostep vce(robust) // L(0/1).w 表示 w 的当期值与一阶滞后
*----------使用 estat abond 命令 table2 ----------------------
estat abond // 序列相关检验
*----------使用 estat abond 命令 table3 ----------------------
estat abond, artests(4)
qui xtabond n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, noconstant lags(2)
estat sargan // 过度识别检验
qui xtabond n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, noconstant lags(2) twostep
estat sargan // 过度识别检验
```
