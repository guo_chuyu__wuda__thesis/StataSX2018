
&emsp;

动态面板数据模型与系统 GMM 方法

&emsp;

> 作者：陆苏怡 (中山大学)，第二作者(学校)
>
> Stata 连享会：  [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn) | [StataChina公众号](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

本文主要内容安排如下：
- 1. 简介
- 2. 系统 GMM 估计
   *  2.1 系统 GMM 估计原理
   *  2.2 与一阶差分 GMM 估计比较
- 3. 系统 GMM 估计的案例及 Stata 的实现
   * 3.1 SYS-GMM 估计的 Stata 命令
   * 3.2 动态面板模型范例及 Stata 的实现
- 4. 结语
-  参考文献
-  Appendix 本文涉及的 Stata 代码


## 1. 简介
在上一篇推文 []()，我们介绍了动态面板模型的基本设定，模型的类型-固定效应模型与随机效应模型, 以及模型的估计包括 IV 和 GMM 估计, 重点对一阶差分 GMM (FD-GMM) 估计及相应模型的实现进行了说明。一阶差分 GMM 将变量的水平滞后值作为其一阶差分项的工具变量, 存在一定的缺陷，Blundell 和 Bond (1998) 曾指出, 一阶差分 GMM 估计方法容易受到 **弱工具变量** 的影响而得到有偏的估计结果。即：
$$\Delta y_{i,t-1}=(\delta-1)y_{i,t-2}+\alpha_{i}+\varepsilon_{i,t-1}$$
当 $\delta$ 接近 1 的时候，工具变量和外生变量的关系就会变的很弱，这就会产生 “弱工具变量问题”。在实际回归时，常出现 Sargan 检验显著拒绝的情形。
&emsp;
为了克服弱工具变量的影响, Arellano 和 Bover (1995) 以及 Blundell和 Bond (1998) 提出了另外一种更加有效的方法, **在 FD-GMM 估计量的基础上进一步使用了水平方程的矩条件，将滞后变量的一阶差分作为水平方程的工具变量**,  这种方法被称为系统 GMM 估计(System GMM, SYS-GMM)。本文主要对系统 GMM 的原理与使用进行介绍。

&emsp;
## 2. 系统 GMM 估计

### 2.1 系统 GMM 估计原理

动态面板模型的基本设定： **AR(1) 模型**

 $$ y_{i,t}=\delta y_{i,t-1}+x_{i,t}^{\prime} \beta+\alpha_{i}+\varepsilon_{i,t} \quad (1)$$  $$ i=1,...,N \qquad t=1,...,T$$

  * **FD-GMM 估计**

 **无外生解释变量的 AR(1) 模型:**
   $$ y_{i,t}=\delta y_{i,t-1}+\alpha_{i}+\varepsilon_{i,t} \quad \left| \delta\right|<1\quad (2)$$  $$ i=1,...,N \qquad t=2,...,T$$
 **通过一阶差分，消去个体效应变量**
 $$\Delta y_{it}=\delta\Delta y_{i,t-1} + \Delta \varepsilon_{it} \ (3)$$
 因为 $\Delta y_{i,t-1}$ 与 $\Delta \varepsilon_{it}$ 相关联具有内生性,需要工具变量, 而 $ y_{i,t-1}$的滞后项比如 $y_{i,t-2}$与$(y_{i,t-1}-y_{i,t-2})$ 相关，但与 $\varepsilon_{i,t}$ 和 $\varepsilon_{i,t-1}$ 不相关所以将 $y_{i,t-2}$ 、$y_{i,t-3} ,..., $作为 $\Delta y_{i,t-1}$ 的工具变量，并且假定 **扰动项 {$\varepsilon_{it}$} 无序列相关**, 矩条件为:

 $$E[(\varepsilon_{i,t}-\varepsilon_{i,t-1})y_{i,t-s}]=0 \ (4)$$ $$ i=1,...,N \qquad t=3,...,T, \qquad s \geqslant 2$$
 令 $Z_{i}$  为工具变量的矩阵形式：
 $$
 Z_{i} =\left( \begin{array}{ccccccc}{y_{i1}} & {0} & {0} & {\ldots} & {0} & {\ldots} & {0}\\ {0} & {y_{i1}} & {y_{i2}} & {\ldots} & {0} & {\ldots} & {0} \\ {\vdots} & {\vdots} & {\vdots} & {\ldots} & {\vdots} & {\ddots} & {\vdots}  \\ {0} & {0} & {0} & {\ldots} & {y_{i1}} & {\ldots} & {y_{i, T-2}} \end{array}\right) \ (5)
 $$      每行的元素对应每期水平方程式的工具变量, $ t = 3,...,T$

 **FD - GMM 矩条件还可以表示为为**:
 $$
 E[Z_{i}^{\prime} \Delta \varepsilon_{i}]=0 \qquad (6)
 $$  $$ i=1,...,N$$

  * **SYS - GMM 估计**
  **水平 GMM** 的矩条件
  回到差分之前的水平方程等式 (2), 使用 $\Delta y_{i, t-1}$ 作为 $y_{i, t-1}$ 的工具变量, 显然二者相关。依旧 **假定$\left\{ \varepsilon_{i,t} \right\}$ 不存在自相关**, 同时 **假定 $\Delta y_{i, t-1}$ 与个体效应变量 $\alpha_{i}$ 不相关**, 故保证了工具变量与复合扰动项 $\left\{\alpha_{i}+\varepsilon_{i,t}\right\}$ 不相关, 水平 GMM 的矩条件可表示为:

  $$
  E\left(\Delta y_{i, t-1}\left(\alpha_{i}+\varepsilon_{i t}\right)\right)=E\left(\Delta y_{i, t-1}\left(y_{i t}- \delta y_{i, t-1}\right)\right)=0 \quad (7)
  $$ $$ t = 2,...,T$$


 **系统 GMM 兼顾了 FD - GMM 和水平 GMM 的矩条件**, 既包括将水平方程中变量的滞后项作为一阶差分的工具变量, 同时也将滞后变量的一阶差分作为水平方程的工具变量, 故其矩条件包括了等式 (4) 和等式 (7):

 令 $Z_{i}^{+}$ 为系统 GMM 工具变量的矩阵, 可表示为：
 $$
 Z_{i}^{+}=\left[\begin{array}{ccccc}
 {Z_{i}} & {0} & {0} & {\cdots} & {0} \\
 {0} & {\Delta y_{i 2}} & {0} & {\cdots} & {0} \\
 {0} & {0} & {\Delta y_{i 3}} & {\cdots} & {0} \\
 {\cdot} & {\cdot} & {\cdot} & {\cdots} & {0} \\
 {0} & {0} & {0} & {\cdots} & {\Delta y_{i, T-1}}
 \end{array}\right]  \qquad (8)
 $$
<br>
### 2.2 与一阶差分 GMM 估计比较

* **矩条件**
FD - GMM 估计: 变量的水平滞后值作为其一阶差分项的工具变量, 矩条件变现为，
 $$E[(\varepsilon_{i,t}-\varepsilon_{i,t-1})y_{i,t-s}]=0 $$  $$ i=1,...,N \qquad t=3,...,T, \qquad s \geqslant 2$$

SYS - GMM 估计: 除了变量的水平滞后值作为其一阶差分项的工具变量, 还包括将滞后变量的一阶差分作为水平方程的工具变量，矩条件表现为，
 $$E[(\varepsilon_{i,t}-\varepsilon_{i,t-1})y_{i,t-s}]=0 $$   $$ i=1,...,N \qquad t=3,...,T, \qquad s \geqslant 2$$  $$ E\left(\Delta y_{i, t-1}\left(\alpha_{i}+\varepsilon_{i t}\right)\right)=E\left(\Delta y_{i, t-1}\left(y_{i t}- \delta y_{i, t-1}\right)\right)=0 $$   $$ t = 3,...,T$$

* **模型的假设**
一阶差分 GMM 需要假定 $\left\{ \varepsilon_{i,t} \right\}$ 不存在自相关, 而对于系统 GMM, 既需要满足扰动项无自相关的条件, 又要假定 $\left\{\Delta y_{i, t-1}, \Delta y_{i, t-2}, \cdots\right\}$ 与个体效应变量 $\alpha_{i}$ 不相关, 假定条件更严格。


* **弱工具变量的问题***

在使用一阶差分 GMM 估计时, 如果 T 很大，则会出现很多工具变量, 容易出现弱工具变量的问题, 通常滞后期越多相关性越弱。当 $\left\{y_{it}\right\}$ 出现很强的相关性, 即自回归系数 $\delta$ 接近 1 时，也会导致弱工具变量问题。由水平方程式 (2)可得式 (10), 此时 $\delta-1$ 接近 0，工具变量与内生变量的关系很弱。
$\Delta y_{i,t-1}=(\delta-1)y_{i,t-2}+\alpha_{i}+\varepsilon_{i,t-1}   \qquad (10)$

由于系统 GMM 进一步将滞后变量的一阶差分作为水平方程中相应的水平变量的工具，显然 $\Delta y_{i,t-1}$ 与 $y_{i,t-1}$ 相关性很强, 可以增加工具变量的有效性。

* **个体效应的估计**

在一阶差分 GMM 估计中,  表示个体效应的变量 $\alpha_{i}$ 在差分时被消掉了, 无法估计 $\alpha_{i}$ 的系数。而系统 GMM 因为包含水平方程 GMM, 可以估计不随时间变化的变量 $\alpha_{i}$ 的系数。
&emsp;

## 3. 系统 GMM 估计的案例及 Stata 的实现

这一章节, 我们介绍用 SYS-GMM 估计动态面板模型的 Stata 官方命令, 然后和上一篇推文一样, 使用 Arellano and Bond (1991) 研究英国就业的数据 abdata.dta 来做案例分析。
<br>
### 3.1 系统 GMM 估计的 Stata 命令
`xtdpdsys` 命令用于估计变量受到其滞后项影响的线性动态面板数据模型, 此命令将被解释变量的水平滞后项作为一阶差分项的工具变量,并将滞后变量的一阶差分作为水平方程中相应的水平变量的工具变量, 是实现 SYS-GMM 估计的 Stata 官方命令。 适用于大 N 小 T 的短面板数据, 要求模型的扰动项不存在序列相关以及滞后变量的一阶差分与个体效应变量不相关。 `xtdpdsys` 命令的主要语法格式为:

```Stata
xtdpdsys depvar [indepvars] [if] [in] [,options]
```
主要选项的含义为：
- `depvar`: 被解释变量
- `indepvars`: 严格外生的解释变量
- `noconstant`: 无常数项
- `lags(p)`: 表示使用被解释变量 p 阶滞后值作为解释变量, 默认一阶滞后 lags(1)
- `maxldep(q)`: 表示最多使用 q 阶被解释变量的滞后值作为工具变量, 默认使用所有可能的滞后值
- `twostep`: 两阶段估计，可修正 Sargan 统计量
-  `pre()`: 指定前定变量
- `endogenous ()`: 指定内生解释变量, 可使用多次
-  `vce()`: 默认为 vce(gmm), 计算得到普通 GMM 标准误, vce(robust) 为异方差稳健稳健标准误

<br>

### 3.2 动态面板模型范例及 Stata 的实现
* **动态就业模型 (Arellano and Bond, 1991) 的设定**
$$
n_{i t}=\alpha_{1} n_{i(t - 1)}+\alpha_{2} n_{i(t-2)}+\beta^{\prime}(L) x_{i t}+\lambda_{t}+\eta_{i}+v_{i t}
$$
- $n_{i t}$ 企业 $i$ 在第 $t$ 年末雇佣员工数量的对数值
- $n_{i(t - 1)}$ 企业 $i$ 在第 $t-1$ 年末雇佣员工数量的对数值
- $(L) x_{i t}$ 表示解释变量 $x$ 及其滞后项, 这里包括 $w_{i,t}$ $w_{i,t-1}$ (即企业 $i$ 在第 $t$ 和 $t-1$ 年实际工资的对数), $k_{i,t}$ 、$k_{i,t-1}$ 和 $k_{i,t-2}$ (即企业 $i$ 在第 $t$ $t-1$ 和 $t-2$ 年资本存量的对数),  $k_{ys,t}$、$k_{ys,t-1}$ 和 $k_{ys,t-2}$ (即企业 $i$ 所在行业在第 $t$、$t-1$ 和 $t-2$ 年总产出的对数)
- $\lambda_{t}$ 包括年份虚拟变量, 表示总需求每年受到的外部冲击, 可表示为 i.yr1979-yr1984
<br>
* **模型的实现**
```Stata
webuse abdata, clear //调用数据
xtdpdsys n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, ///
noconstant lags(2) twostep vce(robust) // L(0/1).w 表示 w 的当期值与一阶滞后

*----------使用 xtdpdsys 命令 table1 ----------------------
. xtdpdsys n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, noconstant lags(2) twostep vce(robust)

System dynamic panel-data estimation            Number of obs     =        751
Group variable: id                              Number of groups  =        140
Time variable: year
                                                Obs per group:
                                                              min =          5
                                                              avg =   5.364286
                                                              max =          7

Number of instruments =     48                  Wald chi2(16)     =    8656.66
                                                Prob > chi2       =     0.0000
Two-step results
------------------------------------------------------------------------------
             |              WC-Robust
           n |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
           n |
         L1. |   .9420272   .1560463     6.04   0.000     .6361821    1.247872
         L2. |   -.074401   .0445357    -1.67   0.095    -.1616894    .0128873
             |
           w |
         --. |  -.5866148   .1622365    -3.62   0.000    -.9045926    -.268637
         L1. |   .5399676   .2075262     2.60   0.009     .1332238    .9467115
             |
           k |
         --. |   .2823063   .0765443     3.69   0.000     .1322822    .4323303
         L1. |  -.0760417   .0893716    -0.85   0.395    -.2512069    .0991234
         L2. |    -.08819   .0463753    -1.90   0.057     -.179084     .002704
             |
          ys |
         --. |    .550633   .1802835     3.05   0.002     .1972838    .9039822
         L1. |  -.7398895   .2436071    -3.04   0.002    -1.217351   -.2624284
         L2. |   .1088334   .1862667     0.58   0.559    -.2562426    .4739095
             |
      yr1980 |    .012991   .0176083     0.74   0.461    -.0215206    .0475025
      yr1981 |  -.0270849    .031772    -0.85   0.394    -.0893568    .0351871
      yr1982 |  -.0314368   .0342856    -0.92   0.359    -.0986354    .0357618
      yr1983 |  -.0156729    .037375    -0.42   0.675    -.0889265    .0575806
      yr1984 |   -.019776   .0375809    -0.53   0.599    -.0934332    .0538811
        year |   .0003632   .0003568     1.02   0.309     -.000336    .0010625
------------------------------------------------------------------------------
Instruments for differenced equation
        GMM-type: L(2/.).n
        Standard: D.w LD.w D.k LD.k L2D.k D.ys LD.ys L2D.ys D.yr1980
                  D.yr1981 D.yr1982 D.yr1983 D.yr1984 D.year
Instruments for level equation
        GMM-type: LD.n

```
<br>
解读:
表 1 的左上方显示, 所使用的全部工具变量一共 48 个(其中 FD-GMM
 有 41 个)，表的最下方显示具体的工具变量, 分别为差分方程式的工具变量 (和 FD-GMM 相同) 和水平方程式的工具变量, 其中水平方程式的工具变量是滞后被解释变量的一阶差分 (7 个工具变量分别是1978-1984 各年 $y_{it}$ 滞后项的一阶差分项)。

从回归结果看, (1) 上一年雇佣员工的数量对企业今年雇佣员工的数量有显著地正向影响, 而过去两年雇佣员工的数量有微弱的负向影响, 在 0.1 水平显著;
(2) 实际工资对员工的数量有显著的负向效应, 而去年的实际工资对员工的数量有显著的正向效应，从数值上看二者影响程度相当;
(3) 资本存量增加会导致员工的数量显著增加, 过去的资本存量会导致员工的数量减少但不是非常显著;
(4) 行业总需求对员工数量有显著正向影响;
(5) 各年份的总需求的外部冲击对劳动力需求方面的作用并不显著。

* **序列相关检验**
SYS-GMM 能够成立的前提之一是, 扰动项 {${\varepsilon_{i,t}}$} 不存在序列相关。如果存在序列相关, 就会存在内生性问题，导致估计的不一致。假设原假设"扰动项 {${\varepsilon_{i,t}}$} 不存在序列相关" 成立, "扰动项的一阶差分" 存在自相关但是扰动项的差分不存在二阶或更高的自相关, 即$\operatorname{Cov}\left(\Delta \varepsilon_{i t}, \Delta \varepsilon_{i, t-k}\right)=0, k \geqslant 2$，故可以通过检验扰动项的差分是否存在一阶与二阶(或更高阶)的自相关来检验原假设, 使用的 Stata 命令为 `estat abond`。
<br>

```stata
*----------使用 estat abond 命令 table2 ----------------------
. estat abond

Arellano-Bond test for zero autocorrelation in first-differenced errors
  +-----------------------+
  |Order |  z     Prob > z|
  |------+----------------|
  |   1  |-3.1914  0.0014 |
  |   2  |-.81025  0.4178 |
  +-----------------------+
   H0: no autocorrelation

```
解读: 结果显示, 在 1% 的显著性水平下, 扰动项的差分存在一阶自相关, 但不存在二阶自相关, 故接受原假设 "{$\varepsilon_{i,t}$} 不存在序列相关", 通过了序列相关检验。
<br>
做更高的自相关检验:
 ```stata
 *----------使用 estat abond 命令 table3 ----------------------
estat abond, artests(4)

Arellano-Bond test for zero autocorrelation in first-differenced errors
  +-----------------------+
  |Order |  z     Prob > z|
  |------+----------------|
  |   1  |-3.1914  0.0014 |
  |   2  |-.81025  0.4178 |
  |   3  | .13879  0.8896 |
  |   4  | .09616  0.9234 |
  +-----------------------+
   H0: no autocorrelation

 ```
<br>
* **过度识别检验** (overidentification test)

关于动态就业模型使用了 48 个工具变量, 远超过了内生变量的个数, 需要进行过度识别检验。原理是 **检验工具变量是否与干扰项相关，即工具变量是否为外生变量** 。原假设是：所有工具变量都是外生, 在执行过 `xtdpdsys` 命令以后, 使用 `estat sargan`命令可以得到 Sargan 统计量, 但是`xtdpdsys` 命令无法提供异方差稳健的 Hansen 统计量。

```Stata
. quietly xtdpdsys n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, ///
 noconstant lags(2) twostep

. estat sargan
Sargan test of overidentifying restrictions
        H0: overidentifying restrictions are valid

        chi2(32)     =   43.6452
        Prob > chi2  =    0.0822
```

解读: p 值为 0.082 即在 5% 的显著性水平下, 没有拒绝 "所有变量都是外生" 的原假设, 意味着工具变量与扰动项不相关, 是有效工具变量。

<br>

* **FD - GMM 与 SYS - GMM 估计结果对比**
命令为:
```Stata
webuse abdata, clear
xtdpdsys n  L(0/1).(k ys) yr1980-yr1984 year, noconstant ///
 lags(1) maxldep(2) endogenous(w,lag(1,2)) twostep vce(gmm)
 // maxldep(2) 使用被解释变量的滞后两、三期的变量作为工具变量
 // endogenous(w,lag(1,2)) 将w和w滞后一期变量的值作为内生解释变量放入模型中, 将w滞后两期、三期的变量作为工具变量
 // vce(gmm)普通 GMM 标准误
est store SYSGMM_gmm

xtdpdsys n  L(0/1).(k ys) yr1980-yr1984 year, noconstant ///
 lags(1) maxldep(2) endogenous(w,lag(1,2)) twostep vce(robust)
est store SYS-GMM_robust
// vce(robust) 异方差稳健的 GMM 标准误


xtabond n L(0/1).w L(0/1).(k ys) yr1980-yr1984 year, noconstant  ///
lags(1) maxldep(2) endogenous(w,lag(1,2)) twostep vce(gmm)
est store FD-GMM_gmm

xtabond n L(0/1).w L(0/1).(k ys) yr1980-yr1984 year, noconstant  ///
lags(1) maxldep(2) endogenous(w,lag(1,2)) twostep vce(robust)
est store FD-GMM_robust

estimates table FD-GMM_gmm FD-GMM_robust SYSGMM_gmm SYS-GMM_robust ,///
b star(0.1 0.05 0.01) b(%10.3f) //  b(%10.3f)保留小数点后三位
```
<br>

结果为:
```Stata
*---------- table4：FD - GMM 与 SYS - GMM 估计结果对比----------------------
------------------------------------------------------------------------------
    Variable |   FdGMM_gmm     FdGMM_robust     SysGMM_gmm     SysGMM_robust
-------------+----------------------------------------------------------------
           n |
         L1. |      0.283**         0.283           0.818***        0.818***
             |
           w |
         L1. |      0.178**         0.178*          0.312***        0.312***
         --. |     -0.403***       -0.403***       -0.542***       -0.542***
             |
           k |
         --. |      0.283***        0.283***        0.225***        0.225***
         L1. |      0.064           0.064          -0.081**        -0.081
             |
          ys |
         --. |      0.443***        0.443***        0.509***        0.509***
         L1. |     -0.203*         -0.203          -0.495***       -0.495***
             |
      yr1980 |      0.005           0.005           0.005           0.005
      yr1981 |     -0.029          -0.029          -0.040*         -0.040
      yr1982 |     -0.055*         -0.055          -0.049**        -0.049
      yr1983 |     -0.050          -0.050          -0.031          -0.031
      yr1984 |     -0.041          -0.041          -0.029          -0.029
        year |     -0.005          -0.005           0.000**         0.000
------------------------------------------------------------------------------
                                           legend: * p<.1; ** p<.05; *** p<.01

```
<br>
解读: 据表 4 显示,  雇佣员工数量对数的一阶滞后项的系数在FD-GMM估计为 0.238, 而 SYS-GMM 估计的系数为 0.818, 相差较大, 体现了两种估计方法得出的结果有较大差异。普通标准误和异方差稳健的标准误对系数大小的估计没有影响, 但是会影响系数的显著性, 表中有些系数在普通标准误下显著但是在异方差稳健的标准误下却变得不显著, 而实际上选择异方差稳健的标准误的结果，才更为可靠, 因为个体个性不同, 其扰动项的分布往往也是有差异的。






## 4. 结语
本文主要了介绍了系统 GMM 估计的矩条件及其与一阶差分 GMM 估计的对比, 接着以动态就业模型为例, 使用官方命令 `xtdpdsys` 命令对模型中的系数进行系统 GMM 估计, 并通过`estat abond` 和 `estat sargan` 命令做序列相关检验和过度识别检验。最后对两种方法的估计结果进行比较。

## 参考文献
- Blundell R.W., and S.R. Bond, 1998, Initial Conditions and Moment Restrictions in Dynamic Panel Data Models, Journal of Econometrics,87,115-143
- Bond, S., 2002, Dynamic panel data models: A guide to micro data methods and practice, Portuguese Economic Journal, 1 (2): 141-162. [[PDF]](https://pdfs.semanticscholar.org/35ff/6451f2b539f436924c8ebfe6a5398ca3cedf.pdf)
http://www.econ.uiuc.edu/~econ508/Papers/arellanobond91.pdf)
- [Dynamic Panel Data : IV and GMM Estimation with Stata (Panel)]([http://rizaudinsahlan.blogspot.com/2017/11/dynamic-panel-data-iv-and-gmm.html](http://rizaudinsahlan.blogspot.com/2017/11/dynamic-panel-data-iv-and-gmm.html))

##  Appendix 本文涉及的 Stata 代码
```stata
webuse abdata, clear //调用数据
*----------使用 xtdpdsys 命令 table1 ----------------------
xtdpdsys n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, ///
noconstant lags(2) twostep vce(robust) // L(0/1).w 表示 w 的当期值与一阶滞后
*----------使用 estat abond 命令 table2 ----------------------
estat abond
*----------使用 estat abond 命令 table3 ----------------------
estat abond, artests(4)
quietly xtdpdsys n L(0/1).w L(0/2).(k ys) yr1980-yr1984 year, ///
noconstant lags(2) twostep
estat sargan  // 过度识别检验
*---------- 获得 table4： DF - GMM 与 SYS - GMM 估计结果对比----------------------
xtdpdsys n  L(0/1).(k ys) yr1980-yr1984 year, noconstant ///
 lags(1) maxldep(2) endogenous(w,lag(1,2)) twostep vce(gmm)
 // maxldep(2) 使用被解释变量的滞后两、三期的变量作为工具变量
 // endogenous(w,lag(1,2)) 将w和w滞后一期变量的值作为内生解释变量放入模型中, 将w滞后两期、三期的变量作为工具变量
 // vce(gmm)普通 GMM 标准误
est store SYSGMM_gmm

xtdpdsys n  L(0/1).(k ys) yr1980-yr1984 year, noconstant ///
 lags(1) maxldep(2) endogenous(w,lag(1,2)) twostep vce(robust)
est store SYS-GMM_robust
// vce(robust) 异方差稳健的 GMM 标准误

xtabond n L(0/1).w L(0/1).(k ys) yr1980-yr1984 year, noconstant  ///
lags(1) maxldep(2) endogenous(w,lag(1,2)) twostep vce(gmm)
est store FD-GMM_gmm

xtabond n L(0/1).w L(0/1).(k ys) yr1980-yr1984 year, noconstant  ///
lags(1) maxldep(2) endogenous(w,lag(1,2)) twostep vce(robust)
est store FD-GMM_robust

estimates table FD-GMM_gmm FD-GMM_robust SYSGMM_gmm SYS-GMM_robust ,///
b star(0.1 0.05 0.01) b(%10.3f) //  b(%10.3f)保留小数点后三位
```
