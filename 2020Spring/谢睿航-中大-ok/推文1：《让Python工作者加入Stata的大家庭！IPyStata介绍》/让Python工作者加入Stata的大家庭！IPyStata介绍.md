    作者：谢睿航  email: xierh8@mail2.sysu.edu.cn
# 让Python工作者加入Stata的大家庭！IPyStata介绍
## 1、关于Python和Stata
Python，易学易懂，其自然语言一般的语法行云流水，已经获得无数科学家、工程师的青睐。在2019年的世界编程语言中，Python已超过C++成为全世界第三大编程语言，仅在Java和C的后面。而且，在全球前20名的编程语言中，使用Python的比例的增速也是最快的。可以说，Python是极有前途的语言。
![编程语言排名](https://img-blog.csdnimg.cn/20200304214927850.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQyMTQ2OTQx,size_16,color_FFFFFF,t_70)

作为一种“胶水语言”，Python可以通过下载各种各样的“包（package）”获得各种奇怪强大的能力，比如强大的数据处理Pandas包、矩阵运算Numpy包、画图的Matplotlib包、机器学习的Scikit-learn包、深度学习的Tensorflow框架，还有用于爬虫的、用于自然语言处理的，等等。

然而，尽管Python也有专门用于计量经济学研究的statsmodel包，其功能却远不及这个领域的“老大”：Stata。作为经管类学生的科研宝剑，Stata凭借其丰富的、前沿的计量经济学支持，以及经得起考验的各类函数和命令，屹立于计量经济学的研究中。

不过，要让习惯于使用Python的科学家和工程师花时间去学习它并不是一件轻松的事情。主要问题在于，虽然Stata在计量上非常强大，但其对数据的爬取、处理等方面却不如Python的各类包来的方便——这是两种编程思想的碰撞。更何况，为了使用Stata，科学家和工程师们需要重新花时间重新学习数据爬取、处理的方法。

Ties de Kok开发出的IPyStata实现了在Python环境下调用Stata，解决了最让习惯于使用Python的科学家和工程师们头疼的问题。通过IPyStata，用户可以在数据处理、数据爬取、自然语言处理、机器学习、画图等其他方面使用Python编程；数据处理好以后，仅通过简单的命令，即可以在Python中调用Stata的命令，从而学习成本大大降低，不得不为是“神器”！

![](https://img-blog.csdnimg.cn/20200304221144776.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQyMTQ2OTQx,size_16,color_FFFFFF,t_70)

## 2、环境部署教程
要完整部署IPyStata包，主要有以下操作：

 1. 下载Anaconda，并部署Jupyter环境；
 2. 通过cmd/terminal安装IPyStata包；
 3. 将Stata的位置绑定到IPyStata；
 4. 在Jupyter中通过魔法命令 **`%%stata`** 调用Stata。

本节将详细地将以上的步骤诉诸读者。
### 2.1 快速了解Jupyter
为了在Python中使用Stata，我们需要用到一种关键的交互式编程工具：Jupyter Notebook或者Jupyter Lab。Jupyter Notebook（即过去的 IPython notebook）是一个交互式笔记本，支持运行 40 多种编程语言。

![](https://img-blog.csdnimg.cn/20200304173203654.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQyMTQ2OTQx,size_16,color_FFFFFF,t_70)

Jupyter Notebook 的本质是一个 Web 应用程序，便于创建和共享文学化程序文档，支持实时代码，数学方程，可视化和 markdown。 最主要的用途是，在进行数据清理和转换、数值模拟、统计建模、机器学习等操作时，Jupyter Notebook提供交互式编程：你可以在其中运行独立的代码块（cell），可以实时获得你所写的代码的结果。

Jupyter Lab可以认为是Jupyter Notebook的升级版，它可以实现在一个网页打开多个Notebook（Jupyter Notebook和Jupyter Lab使用的都是 `.ipynb` 文件， 下文使用“Notebook”代表Jupyter Lab或Jupyter Notebook打开的文件）。通过安装插件，其可以实现比如在窗口浏览Excel、PDF等文件，在窗口浏览目前Notebook的变量等强大的功能。

![Jupyter Lab多窗口显示](https://img-blog.csdnimg.cn/20200304172935672.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQyMTQ2OTQx,size_16,color_FFFFFF,t_70)

### 2.2 通过Aanaconda部署Jupyter
安装Jupyter Notebook或Jupyter Lab的方法多种多样，这里首先推荐通过Anaconda安装。

Anaconda是一个开源的Python发行版本，其包含了conda、Python等180多个科学包及其依赖项。也就是说，安装了Anaconda，就相当不仅安装好了Python，更安装好了一切常用的Python包（Package）。

安装Anaconda十分简单，只需要进入其官网：**`https://www.anaconda.com`**  下载对应系统的版本即可。

![Anaconda主页](https://img-blog.csdnimg.cn/20200304174132607.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQyMTQ2OTQx,size_16,color_FFFFFF,t_70)

Windows用户在安装Anaconda的时候，推荐勾选 **Add Anaconda to the system PATH** 按钮，这样用户就可以通过系统命令提示符cmd中打开Jupyter（macOS/Linux 用户请无视）。

![请勾选Add Anaconda to the system PATH](https://img-blog.csdnimg.cn/20200304174928358.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQyMTQ2OTQx,size_16,color_FFFFFF,t_70)

如果用户点选了 **Add Anaconda to the system PATH** 按钮，那么请Windos用户打开命令提示符**cmd**，请macOS/Linux用户打开**terminal**。输入 **`jupyter notebook`** 或 **`jupyter lab`** 。点击回车运行，即会弹出一个网页，打开对应的Jupyter环境。

![启动jupyterlab](https://img-blog.csdnimg.cn/20200304175323805.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQyMTQ2OTQx,size_16,color_FFFFFF,t_70)

如果在安装Anaconda的时候没有点选此按钮，在安装完成后，可以通过Windows的 **`开始`** 找到 **`Anaconda Navigator`**。打开该软件并通过点选 **`JupyterLab`** 或者 **`Notebook`** 打开Jupyter编译环境。
![Anaconda软件的主页](https://img-blog.csdnimg.cn/20200304174720554.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQyMTQ2OTQx,size_16,color_FFFFFF,t_70)


### 2.2 部署IPyStata

在确保Jupyter环境部署无误后，我们接下来部署IPyStata。

同样地，请Windos用户打开命令提示符 **cmd** ，请macOS/Linux用户打开 **terminal** ，并输入 **`pip install ipystata`** ，按回车执行。系统会自动从互联网中寻找iPyStata包并自动安装。

![安装IPyStata包](https://img-blog.csdnimg.cn/2020030417571522.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQyMTQ2OTQx,size_16,color_FFFFFF,t_70)

在出现 **Successfully installed ipystata-x.x.x** 的字样后，说明已经成功安装IPyStata包了。
![在这里插入图片描述](https://img-blog.csdnimg.cn/2020030417575867.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQyMTQ2OTQx,size_16,color_FFFFFF,t_70)

### 2.3 将Stata绑定到IPyStata：Windows用户请注意
IPyStata包可以通过两种方法与Stata软件进行沟通，分别是：

1. **Stata Automation**，这种方法仅在Windows上可用；
2. **Stata Batch mode**，这种方法在Windows、macOS和Linux均可用。

根据iPyStata的官方介绍，**Stata Automation** 能够提供更加丰富的细节，并且是Windows环境的默认沟通方式。要在Windows上使用**Stata Automation**，请注册你的Stata实例（_register your Stata instance_）：

 - 在命令提示符cmd中输入 **`cd C:\Program Files (x86)\Stata14`** 以将当前目录设置到Stata的目录。
 - 查看你Stata目录下对应的执行文件的文件名（如 **`StataMP-64.exe`** ）
 - 在命令提示符cmd窗口输入：**`StataMP-64.exe /Register`** 
 
 ### 2.4 将Stata绑定到IPyStata：macOS/Linux用户请注意
 
 要在Notebook中使用Stata，首先需要告诉IPyStata在哪里可以找到Stata软件，请用下面的代码：

```python
In[1]: import ipystata  
In[2]: from ipystata.config import config_stata  
In[3]: config_stata('Path to your Stata executable')  
```
上面的 **`Path to your Stata executable`** 请输入电脑里Stata执行文件的路径，比如：

```python
Mac OS X --> '/Applications/Stata/StataSE.app/Contents/MacOS/stataSE'
Linux    --> '/home/user/stata14/stata-se'
```

## 3、IPyStata的使用方法
在本节，笔者将介绍最常用的IPyStata命令，包括：

 1. 加载IPyStata包
 2. IPyStata的使用语法
 3. 将Stata的数据导入Notebook
 4. 将Notebook的数据导入Stata
 5. 在Notebook中画Stata的图
 6. 通过Notebook直接打开Stata
 7. 通过Notebook使用meta

如果想了解更多的细节，请访问第4节对应的链接。
### 3.1 在Notebook中加载IPyStata包
要使用IPyStata，首先要加载IPystata包。前面2.3、2.4节提到的操作只需要操作一次，以后调用IPyStata时只需要执行：

```python
In[1]: import ipystata
>>Out[1]: IPyStata is loaded in batch mode.
```
即可调用IPyStata。需要注意的是，如果你重新安装了Stata或者更新了Stata的版本，则需要重复2.3、2.4节的操作，以将IPyStata绑定到新的Stata目录。


### 3.2 基本使用语法
在加载IPyStata包以后，如果我们要使用Stata命令，则需要在程序所在的cell（**“cell”即“代码块”，下同**）的第一行输入魔法命令 **`%%stata`** 。或者说，如果程序所在的cell的第一行是 **`%%stata`** ，那么整个cell都会被“翻译”成do文件放在Stata中执行，并将结果输出到Notebook中。比如：

```python
In[1]: %%stata
     display "Hello, I am printed by Stata."
>>Out[1]: Hello, I am printed by Stata.
```
其中，**`Out[1]`** 是通过Stata输出的。

IPyStata为用户定义了多种通过Notebook调用Stata的“方法”（Argument）。每一个“方法”的使用格式都是在魔法命令 **`%%stata`** 后输入该方法的缩写或全称。下文将开始介绍一些主要的“方法”。

值得一提的是，Notebook实际上是通过Python的Pandas包里的 **`Dataframe`** 数据结构与Stata联动的。

### 3.3 将Stata的数据导出到Notebook：**`-o`** 或  **`-output`** 命令

方法 **`-o [variable]`** 或  **`-output [variable]`** 将会把当前Stata中的数据以 **`variable`** 的变量名和Dataframe的数据结构导入到Notebook中。

下面的代码单元将运行Stata命令 **`sysuse auto.dta`** ，并将Stata中现在的数据通过 **`-o car_df`** 方法导出到Notebook中。

```python
In[1]: %%stata -o car_df
       sysuse auto.dta
```
执行完上面的代码后，sysuse里的数据将以dataframe的数据结构，以 **`car_df`** 为变量名进入Notebook。我们可以在下一个cell中通过Python的 **`type()`** 函数查看变量 **`car_df`**  的数据类型：

```python
In[2]: type(car_df)
>> Out[2]: pandas.core.frame.DataFrame
```
如果直接“运行”命令**`car_df`**  ，我们将可以在Notebook中查看 **`car_df`** 的内容：
![car_df](https://img-blog.csdnimg.cn/20200304204253934.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQyMTQ2OTQx,size_16,color_FFFFFF,t_70)

### 3.4 将Notebook的数据导入到Stata：**`-d`** 或  **`-data`** 命令

方法 **`-d [variable]`** 或  **`-data [variable]`** 用于定义应该将notebook内存中以 **`variable`** 为变量名的Dataframe导入到Stata中。

第3.2节的代码已经执行完毕后，Notebook会有一个命名为 **`car_df`** 的变量。我们把这个变量传回Stata中，并执行Stata的 **`tabulate`** 函数：

![--data](https://img-blog.csdnimg.cn/20200304204955521.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQyMTQ2OTQx,size_16,color_FFFFFF,t_70)
 
 可以看出，运行cell后，结果将会以Stata的格式显示在Notebook中。

### 3.5 在Notebook中画Stata的图：**`-g`** 或  **`-graph`** 命令

IPyStata会自动判断Stata对应的cell中是否有新的图像生成。

如果您在Stata中画了多张图，那么你应该使用 **`name(..., replace)`** 命令。

注意：IPyStata不能保证画图的顺序与程序的顺序相同。在画多张图的时候建议使用 **`title()`** 命令！

如果使用 **`-nogr`** 或 **`--nograph`** 命令，可以阻止生成的Stata图像被展示出来。

以下是一个事例：

```python
In[1]: %%stata --data car_df --graph
	   graph twoway scatter price mpg, name(a, replace) title("Graph a")
	   graph twoway scatter price trunk, name(b, replace) title("Graph b")
```

需要特别注意的是，在编者使用的 macOS Catalina 10.15.3 环境中，Stata的图像无法显示在Notebook中，如图：
![macos系统中不能使用](https://img-blog.csdnimg.cn/2020030420573823.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQyMTQ2OTQx,size_16,color_FFFFFF,t_70)

而Windows系统是支持此功能的。

但是既然使用Python调用Stata，那么也可以用Python画图嘛：

```python
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
font = FontProperties(fname='/System/Library/Fonts/Supplemental/Songti.ttc')  # 选择字体方案
plt.rcParams['axes.unicode_minus'] = False # 用来正常显示负号
%matplotlib inline

fig = plt.figure(figsize=(9, 5))
axe = fig.add_subplot()

axe.scatter(car_df.loc[:,'mpg'], car_df.loc[:,'price'], label='mpg', marker='o', color='blue')
axe.scatter(car_df.loc[:,'trunk'], car_df.loc[:,'price'], label='mpg', marker='*', color='red')

axe.set_title('用python画图', fontProperties=font, fontsize=15)
axe.set_ylabel('价格', fontProperties=font, fontsize=13)
axe.legend(loc='upper right', fontsize=13)
plt.show()
```

运行上面的程序，可以得到：

![用matplotlib画图](https://img-blog.csdnimg.cn/20200304205943651.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQyMTQ2OTQx,size_16,color_FFFFFF,t_70)

使用Python的matplotlib包画图虽然看似繁琐，但matplotlib包的画图逻辑对Python的原生用户或Matlab等其他一些语言的用户是十分友好的。

### 3.6 打开Stata程序：**`-os`** 或  **`--openstata`** 命令

***注意：该命令仅在Windows和macOS系统下可用，Linux不可用！***

虽然3.5节表明，在macOS环境下不能在Notebook中直接显示Stata的图像，但这里有一个不是很完美的解决办法：

```python
In[1]: %%stata --data car_df -os
	   graph twoway scatter price mpg, name(a, replace) title("Graph a")
	   graph twoway scatter price trunk, name(b, replace) title("Graph b")
```

运行上面的cell，Notebook会直接打开Stata程序，如图：

![直接打开Stata软件](https://img-blog.csdnimg.cn/20200304210525308.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQyMTQ2OTQx,size_16,color_FFFFFF,t_70)

这时候，我们就可以直接在Stata执行剩下的操作了。

需要注意的是，在macOS系统中，如果不将此Stata程序彻底关闭，那么Notebook对应的cell是不会停止运行的（cell序号一直显示 **`[*]`**）：

![不关闭Stata对应的cell是不会停止运行的](https://img-blog.csdnimg.cn/20200304210645835.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQyMTQ2OTQx,size_16,color_FFFFFF,t_70)

这时候之后关闭Stata程序（系统快捷键 **`cmd+Q`** ），这个cell才会结束运行状态。

上面这段程序的原理是：

 1. 通过 **`--data car_df`** 命令将Notebook中的 **`car_df`** 变量导入Stata；
 2. 根据代码 **`graph twoway scatter price ...`** 形成对应的do文件（在macOS系统中，生成的do文件的路径是 **`/Users/[username]/.ipython/stata/code.do `**）；
 3. 打开Stata，并直接执行do文件。

### 3.7 使用Meta：**`-m`** 或  **`--meta`** 命令

***注意：该命令仅在 **`Automation`** 模型下可用，即Windows系统的默认状态下可用，但macOS/Linux 用户不可用。***

使用方法与前面的相同，只需要在cell的开头运行 **`%%stata --meta`** ，然后输入meta命令即可。
## 4、版权和引用
**Python和Stata的介绍：**[双剑合璧之Stata与Python：初识IPyStata](https://zhuanlan.zhihu.com/p/22645950)
**Jupyter Notebook的介绍：**[Jupyter notebook的使用](https://www.jianshu.com/p/a3d61b68ba41)
**Anaconda的介绍：**[百度百科：Anaconda](https://baike.baidu.com/item/anaconda/20407441?fr=aladdin)
**IPyStata的源：**[TiesdeKok/ipystata](https://github.com/TiesdeKok/ipystata#setupwindows)
**IPyStata官方示例：**[Example notebook for the %%stata cell magic by the IPyStata package](https://nbviewer.jupyter.org/github/TiesdeKok/ipystata/blob/master/ipystata/Example.ipynb)