***Data Generation
* 导入模拟数据
cd "E:\Data\GA__PTW_REP"
u "DATA/INPUT/MCDATA.dta", replace
	
* 生成不随时间变化的个体特征变量，即 b1, b2, b3
qui gen h1 = runiform(0,1) 
qui gen h2 = runiform(0,1)   
qui gen h3 = runiform(0,1) 
 
* 生成模型(2)中对应的Hi
qui gen H =  (-1.5+h1+h2+h3)	
	
* 生成两组treatment variable (连续型 DID)
qui gen T1 = runiform(0,1) 
qui gen T2 = runiform(0,1)  

* 将 TREATMENT x H 加入至 TREND变量中，从而生成模型(2)对应的个体变化趋势 OMEGA
qui replace TREND = TREND + (0.5 * T1 + 0.5 * T2) * H
		
* 生成抽样比率 Fi 与抽样权重 Si
* 生成 r1, r2, r3
gen RW = runiform(0,1)
* 生成抽样权重 Si
qui gen MODELW =  (RW[1]*h1+RW[2]*h2+(RW[3])*h3)
* 对抽样权重作标准化处理 
qui sum MODELW 
qui replace MODELW = MODELW /r(mean)
* 生成抽样权重的倒数：抽样比率 Fi
qui gen IVW = 1/MODELW

* 依据抽样比率 Fi 从总体 j = 1,...,J 中抽取样本 i = 1,...,I	
qui gsample `obs' [w=IVW]	
	 
* 将数据由宽型数据转换为长型数据
		qui foreach num of numlist 1/10 {
		preserve
		keep ID LE YE Y`num' h*  TREND T1 T2 MODELW IVW 
		gen PERIOD = `num'
		ren Y`num' Y
		gen panelID = _n
		save "TEMP/re_temp`num'.dta", replace
		restore
		}
		qui u "TEMP/re_temp1.dta", clear
		qui erase "TEMP/re_temp1.dta"
		qui foreach num of numlist 2/10 {
		append using "TEMP/re_temp`num'.dta"
		erase "TEMP/re_temp`num'.dta"
		}	

	
* 定义面板数据
qui xtset panelID PERIOD	
		
* 将f(T) x OMEGA 加入至结果变量中
qui replace Y = Y + TREND*PERIOD

* 将处理效应 TREATMENT 加入至结果变量中，以t=5为接受处理的时点 
qui replace  Y = Y + T1 if PERIOD >=6
qui replace  Y = Y + T2 if PERIOD >=6

* 生成 TREATMENT x POST DID变量，其随机系数即我们考察的处理效应
qui gen T1_POST = T1 * (PERIOD >=6)
qui gen T2_POST = T2 * (PERIOD >=6)

* 在结果变量中加入随机扰动项
qui replace Y = Y + rnormal(0,0.1)

* 存储数据以进行下一步运算
save "TEMP/temp", replace


***ESTIMATION
** 估计 OLS DD	
reg Y T1_POST T2_POST i.PERIOD, abs(panelID) 
* RECOVER ESTIMATED TREATMENT EFFECTS
scalar bT1_OLS = _b[T1_POST]
scalar bT2_OLS = _b[T2_POST]
	
** 采用抽样权重估计 WPT DD
reg Y T1_POST T2_POST i.PERIOD [w=MODELW], abs(panelID) 


** ESTIMATE WPT DD USING WEIGHTS FROM GRID SEARCH ALGORITHM
	
* 导入数据并调整数据格式	
qui u "TEMP/temp", clear
	keep if PERIOD ==2
	keep ID Y
	ren Y Y2
	save "TEMP/ptwtemp", replace
qui u "TEMP/temp", clear
	keep if PERIOD ==1	
	merge m:m ID using "TEMP/ptwtemp.dta"	
	drop _m
	ren Y Y1
	erase "TEMP/ptwtemp.dta"	
	
* 生成结果变量自第1期至第2期的变化值	
	gen DELTA = Y2- Y1

* 将个体特征变量标准化
	foreach num of numlist 1/3 {
	gen VAR`num'= h`num'
	sum VAR`num',d
	gen wa_VAR`num' = VAR`num'- r(min)
	replace wa_VAR`num' = wa_VAR`num'/r(sd)
	}
	
* 将TREATMENT VARIABLE标准化
	sum T1
	replace T1 = T1/ r(sd)
	sum T2
	replace T2 = T2 / r(sd)

* 生成网格搜索法中需要用到的暂时性变量
	gen wa = .
	gen OBJ = .
	gen OBJmin = .
	gen VAR1min = .
	gen VAR2min = .
	gen VAR3min = .
	gen wa_min = .

* 确定搜索范围与搜索步长	
	local min = 0
	local max = 1
	local step = 0.1
	
* 运行网格搜索	
	
* 开启对q1的循环
local om_VAR1 = `min'
while `om_VAR1' <= `max' {
	* 开启对q2的循环
	local om_VAR2 = `min'
	while `om_VAR2' <= `max' {
	    * 开启对q3的循环
		local om_VAR3 = `min'
		while `om_VAR3' <= `max' {
			
			* 生成权重Si
			qui replace wa = `om_VAR1'*wa_VAR1+`om_VAR2'*wa_VAR2+`om_VAR3'*wa_VAR3
			* 确保抽样权重Si>0
			qui sum wa
			if r(mean) > 0 {
			* 对Si作标准化处理
			qui replace wa = wa / r(mean)	
			* 依照模型(4)进行加权回归估计处理效应
			qui reg DELTA T1 T2 [iw=wa]
			* 计算目标方程
			qui replace OBJ = _b[T1]^2+_b[T2]^2
			* 若 qm 使目标方程较上一轮循环更小，则存储参数
			qui replace VAR1min = `om_VAR1' if OBJ < OBJmin
			qui replace VAR2min = `om_VAR2' if OBJ < OBJmin
			qui replace VAR3min = `om_VAR3' if OBJ < OBJmin
			* 若目标方程较上一轮循环更小，存储计算所得的 WPT 权重
			qui replace wa_min = wa if OBJ < OBJmin
			qui replace OBJmin  = OBJ if OBJ < OBJmin
			display "VAR1 "`om_VAR1' " VAR2 " `om_VAR2' " 'VAR3 " `om_VAR3'
					}
					else {
					}		
			* 结束对q3的循环
			local om_VAR3 = `om_VAR3'+`step'
			}
		* 结束对q2的循环
		local om_VAR2 = `om_VAR2'+`step'
		}
	* 结束对q1的循环	
	local om_VAR1 = `om_VAR1'+`step'
	}

* 结束循环并存储样本权重 
	ren wa_min WA_MC
	keep ID WA_MC
	save "TEMP/WA_MC.dta", replace
			
* 将计算所得的样本权重与原模拟数据进行合并	
	qui u "TEMP/temp", clear
	qui merge m:m ID using "TEMP/WA_MC.dta"	
	erase "TEMP/WA_MC.dta"	
	qui tab _m
	qui drop _m 
	
* 估计 WPT DD
	reg Y T1_POST T2_POST i.PERIOD [w=WA_MC], abs(panelID) 

* 去除权重变量
	qui drop WA_MC  			
			

***ESTIMATE WPT DD USING WEIGHTS FROM ITERATIVE ALGORITHM
* 存储数据
save "TEMP/temp", replace
	
* 清除内存
set more off
estimates drop _all 
program drop _all

* 设定迭代次数为100 
local MAX = 100
* 设定目标方程的目标值为0.005	
local TARGET = 0.005
	
* 导入数据并整理数据格式	
u "TEMP/temp.dta", clear
keep if PERIOD ==2
keep ID Y
ren Y Y2
save "TEMP/ptwtemp.dta", replace
u "TEMP/temp.dta", clear
keep if PERIOD ==1	
merge m:m ID using "TEMP/ptwtemp.dta"	
drop _m
ren Y Y1
erase "TEMP/ptwtemp.dta"
	
* 生成结果变量自第1期至第2期的变化值	
	gen DELTA = Y2- Y1

* 对结果变量和处理变量作标准化处理
	foreach var of varlist DELTA T1 T2 {
		qui sum `var'
		qui gen S_`var' = `var'/r(sd)
		}
* 生成处理变量T1 T2与个体特征变量bm的交乘项
	qui foreach var1 of varlist S_T1 S_T2 {
		foreach var2 of varlist h1 h2 h3 {
		gen `var1'_`var2' = `var1'*`var2'
		}
		}
* 生成迭代所需要的暂时性变量		
	gen W = 1
	gen W1 = 1
	gen W2 = 1
	gen WOLD = 1
	qui gen ME_T1 = .
	qui gen ME_T2 = .
	gen WBEST = 1
	gen bBEST = 100
	scalar TH = 0.00
	gen R = .

* 定义迭代法所使用的程序 ************************************************
	program ALGO 
		* 估计模型(5) 
			qui replace WOLD = W
			qui reg S_DELTA S_T1 S_T2 [w=W]
		* 存储估计所得的处理效应 (模型(5)中的c_s^n)
			qui scalar bT1 = _b[S_T1]
			qui scalar bT2 = _b[S_T2]
		* 存储目标方程的函数值
			qui scalar bsum = abs(bT1)+abs(bT2)
				
		* 估计模型(6)
			qui reg S_DELTA S_T1 S_T2 S_T1_h* S_T2_h* [w=W]
		* 存储估计所得的处理效应 (模型(6)中的b_s^n,m)
				qui replace ME_T1 = _b[S_T1]
				qui replace ME_T2 = _b[S_T2]
				qui foreach num of numlist 1/3 {
				qui 	replace ME_T1 = ME_T1 + _b[S_T1_h`num']*h`num'
				qui 	replace ME_T2 = ME_T2 + _b[S_T2_h`num']*h`num'
				}
		
		* 生成调整后的权重Si
		
			* TREATMENT 1 (第一类处理效应)
				* 若模型(5)中估计所得的处理效应为负
				qui if bT1 < -1*TH {
				display "neg"
				* 增加处理效应为正的个体在样本中的权重
				replace W1 = W + W*ME_T1 if ME_T1 > 0
				}
				else {
				* 若模型(5)中估计所得的处理效应为正
				if bT1 > TH {
						display "pos"
 				* 增加处理效应为负的个体在样本中的权重
						 replace W1 = W - W*ME_T1 if ME_T1 < 0
						}
						else {
						}			
					}
						else {
						}
					
			* TREATMENT 2 (第二类处理效应)
				* 若模型(5)中估计所得的处理效应为负
				qui if bT2 < -1*TH {
				display "neg"
				* 增加处理效应为正的个体在样本中的权重
				replace W2 = W + W*ME_T2 if ME_T2 > 0
				}
				else {
				* 若模型(5)中估计所得的处理效应为正 
				if bT2 > TH {
						display "pos"
		 		* 增加处理效应为负的个体在样本中的权重
						replace W2 = W - W*ME_T2 if ME_T2 < 0
						}
						else {
						}			
					}
						else {
						}
					
		* 根据两类处理效应的估计结果对权重进行调整 
		* 对于绝对值较大的处理效应给予更大幅度的调整
			qui replace W =[exp(abs(bT1))* W1+exp(abs(bT2))*W2] / [exp(abs(bT1))+exp(abs(bT2))]
		* 基于新生成的权重估计模型(5)
			qui reg S_DELTA S_T1 S_T2 [w=W]
		* 将处理效应存储为单值
		    qui scalar bT1new = _b[S_T1]
			qui scalar bT2new = _b[S_T2]
		* 计算新的目标方程函数值
			qui scalar bsumnew = abs(_b[S_T1])+abs(_b[S_T2])
		* 基于目标方程的函数值判定是否对权重进行更新
			qui replace bBEST = bsumnew if  bsumnew<bBEST
			qui replace WBEST = W if  bsumnew<bBEST

		* 若上一轮循环得到的权重更优，则结合本次循环和上一轮循环得到的权重进行调整
			if bsumnew > bBEST + 0.025 {
				replace W = 0.8*WBEST+0.2*W
				}
				else {
				}
	
	end	
* 程序ALGO定义结束	****************************************************
	
	
* 迭代循环过程 *************************************************************	
	local it = 1
	
	while `it' < `MAX'  {
		* 运行以上定义的程序 ALGO
		qui ALGO	
		* 基于本轮循环得到的权重估计处理效应
		qui reg S_DELTA S_T1 S_T2 [w=W]
		* 存储估计所得处理效应
		qui local T1CORR = _b[S_T1]
		qui local T2CORR = _b[S_T2]		

		* 展示迭代结果 
		display "Iteration "`it' " T1 CORR " `T1CORR' " T2 CORR " `T2CORR'
		
		* 检验是否达到目标方程函数值小于0.005的目标
		if abs(`T1CORR') < `TARGET' & abs(`T2CORR') < `TARGET' {
			local it = 1000 
			}
			else {
			local it = `it'+1
			}
		}
* 存储最终得到的pre-treatment period的处理效应		
	reg  S_DELTA S_T1 S_T2 [w=W]
	scalar T1CORR = _b[S_T1]
	scalar T2CORR = _b[S_T2]		

* 存储最终得到的最优权重	
	qui sum W
	gen WA_MC = W / r(mean)
	keep ID WA_MC
	save "TEMP/WA_MC.dta", replace
			
* 将原模拟数据与权重数据合并	
	qui u "TEMP/temp", clear
	qui merge m:m ID using "TEMP/WA_MC.dta"	
	erase  "TEMP/WA_MC.dta"	
	qui tab _m
	qui drop _m 
* 估计 WPT DD
	reg Y T1_POST T2_POST i.PERIOD [w=WA_MC], abs(panelID) 

* 去除权重变量
	qui drop WA_MC
	erase "TEMP/temp.dta"			
			
			



