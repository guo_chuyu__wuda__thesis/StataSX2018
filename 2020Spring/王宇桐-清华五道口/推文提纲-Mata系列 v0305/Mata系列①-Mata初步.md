## Mata系列① | Mata 初步

### 一. Mata 是什么？



### 二. Mata 的强大/好处





### 三. Mata 基础语法

- 调用 Mata 环境
- 变量类型
- 运算符号
- 循环语句
- 条件语句

### 四、举个例子：计算分位数回归的边际效应

通过一个简单例子，帮助大家说明如何利用Mata来解决科研中面临的问题。

来源：

Baum_An Introduction to Stata Programming 

Chapter 14






