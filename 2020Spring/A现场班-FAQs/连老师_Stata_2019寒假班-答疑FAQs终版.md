注：此文档为初级班及高级班的所有答疑内容。文档中的链接可直接点击，一些链接也已经插入词句中~ 

初级班答疑：[连老师_Stata_2019寒假初级班-答疑FAQ](http://note.youdao.com/noteshare?id=ae8760353f8ae10eb8c69cd3ffc21112&sub=6439155130574F0BAFB2D7756657AD6B) 

高级班答疑：[连老师_Stata_2019寒假高级班-答疑FAQ](http://note.youdao.com/noteshare?id=bbbf8637801f6386a78862bb99331c3f&sub=45E527731A8F4C4789353AB7676CB226) 





### 问题整理
---
#### 安装及资料下载

1. **如何安装Stata15?**  
    请查看“A-课件下载和使用说明”文件，下载地址为：  
    百度云盘链接：<https://pan.baidu.com/share/init?surl=rZ5l16jmcz1-gkdj8pkKKg>  
    密码：k3qw  

1. **Mac系统如何安装?给出的链接中只有exe文件?**  
    （注：Windows系统请使用问题1中的链接安装）  
    百度云盘链接：<https://pan.baidu.com/s/1xQygOIHDWsYcw8R6bEIduw#list/path=%2F>  
    密码：2dtb

1. **本次培训的软件及数据资料多久可以下载好？下载太慢怎么办？**  
    正常为一小时左右，具体时间和网速有关系，可以使用下方两种加速器的任意一个。  
    下载地址：①<http://pandownload.com/>   
    ② <https://faq.speedpan.com/chapter2/install.html>

1. **Mac系统安装后中的stata/ado里面没有personal这个文件夹怎么办？**  
    自己新建一个名为“personal”的文件夹，然后把PX A 2019a放进去就可以了。

1. **如果Stata15软件装在C盘可以使用吗？**  
    可以使用的，只是老师给的do文档中部分路径需要简单修改。

注：安装和下载有问题可以找助教~


&nbsp;
---
#### 命令执行

6. **在Stata15主界面的执行某一命令行，如第一个命令`global...`提示invalid syntax错误？**  
    将语令后的中文注释删除后再在命令窗口执行即可。
    推荐在do文件中选中命令行后执行，具体可见老师给的课件（页码标号第5页,PDF的第7页）。

1. **如何操作do文档中的命令？**  
    选中之后点击do文档中的`Execute（do）`按钮执行选中部分，也可以选中后用快捷键`Ctrl+D`或`Ctrl+R`（静默执行），注意不进行选择直接点击do文档中的`Execute（do）`按钮会执行整个do文件，中途停止执行可以点击结果窗口上面的`叉号×（Break）`，执行中为红色。

1. **语句执行报错，文件无法打开？比如`shellout "Stata2019.xlsx"`报告“Cannot find...”**   
    &emsp;多半是文档不在现有的执行路径内所导致的，可以先执行`pwd`查看现在的工作路径，如果文件不在路径内那么Stata也就无法找到文件了。  
    &emsp;可以通过`cd`来修改命令。
    如果路径没有问题，可能是文件名输错，再核对一下命令~

1. **命令无法识别或无法执行？**  
    &emsp;外部命令统一存放于“D:\stata15\ado\plus”文件夹中，正确设定 Stata 的文件路径就不会出现命令无法识别的问题。  
    &emsp;连老师自己编写的命令统一存放于 “D:\stata15\ado\personal\PX_A_2019a\adofiles”文件夹中，要使这些命令正确执行，需要使用` adopath + `命令将上述路径加入 stata15 的搜索范围内。  
    &emsp;对于外部命令，使用`ssc install (命令)` 就可以将命令下载于路径中，如
    `ssc install esttab`。之后命令就可正常运行（只需执行一次命令）。

1. **如何设定Stata打开时自动执行的 profile.do 的文件？**  
    [Stata: 聊聊 profile.do 文档](https://www.jianshu.com/p/387118287099)  

        ------------以下增加于2019.1.18------------
        
1. **`shellout` 命令无法使用？**  
    - Mac用户：[Stata: 苹果(MAC)用户无法使用 shellout 命令？](https://www.jianshu.com/p/dfd034d1f00e)
    - Windows用户如果无法使用有两种情况：① 路径设置错误，文件不在当前路径内 ② 可以在`shellout`后加`using`之后进行尝试，例如`shellout using "Acock_2014.pdf"`

1. **什么时候需要使用`adopath`命令？**  
    &emsp;ado文件的创建是用户基于现有的Stata命令，根据自己的需求编写脚本和程序来添加一些新的特性或功能以自动实现可重复分析。  
    &emsp;相应的adopath就是执行这些文件的路径，当用户将自己写的ado文件保存到除Stata自动设定搜索的路径之外时就需要在这些路径中加入自己保存文件的路径。命令如`adopath + "E:\"`  
    &emsp;可见推送[Stata编程——我的第一个Stata程序](https://mp.weixin.qq.com/s/8ehxJMxbWwLcwZKa9DH4Tg)

&nbsp;
---
#### 命令与结果解答
13. **如何更好理解`merge`命令同维度和不同维度数据的合并？**  
    - [具体图解](http://blog.sina.com.cn/s/blog_629bb758010123av.html)  
    - [连老师简书-数据拆分与合并](https://www.jianshu.com/p/8d193ffdde05)
    - 多对多合并可用`joinby`  [组内交叉合并——joinby](https://mp.weixin.qq.com/s/G4qJrUkCRMTwk_8Eld9-Cg)
    - [A004. 为什么Stata中merge m:m容易出问题](https://www.jianshu.com/p/4509291531e0)

1. **如何更好理解`global`与`local`？**  
    - [认识 Stata 的暂元（Macro）](https://mp.weixin.qq.com/s/GK87N27oBD7eYXTuny3BHQ)
    - 应用如 [Stata绘图: 用暂元统一改变图形中的字号]( https://www.jianshu.com/p/fdbc6b3074f6)
    - 最好的方式依然是官方文档~ 可以执行`help macro`及`help quotes`（里面有说到single quotes和double quotes）进行深入的理解
    
1. **Stata中`destring`与`encode`将字符型转换成数值型直接的区别？**   
    [stata文本变量的处理](https://www.jianshu.com/p/d8045fde6c15)

1. **`logout` 和 `esttab` 输出作用的区别？**  
    `logout`可用于输出所有命令结果；`esttab`一般用于输出回归表格，也可以输出描述统计。  
    可见连享会推文 [君生我未生！Stata - 论文四表一键出](https://www.jianshu.com/p/97c4f291ee1e)  
    另外推荐一个功能强大的输出结果命令：`asdoc` (附[使用指南](https://mp.weixin.qq.com/s/gPc26DJTu8PAEvbIPBGLEg))  

        ------------以下增加于2019.1.18------------
            
1. **`tabstat wage hours married, by(race) s(mean) f(%3.2f)`中命令选项的`s()`是什么意思？**    
    &emsp;s即statistics，表示统计量，如最大值、最小值、平均数等。  
    &emsp;通过`s()`可以输出特定的统计信息，如`s(mean count n sum max min rang sd cv p1 p5 p10)`等等，具体可以通过执行`help tabstat`语句后在对应的帮助文档中进行详细的查看。

1. **日期从Excel导入后的格式为字符串格式（str7），如2013-08，应该如何进行设定为Stata中的日期格式？**  
   &emsp;以下的解决方法仅供参考~ 大家可以多参考一些官方文档（如[Working with dates and times]( https://www.stata.com/manuals13/u24.pdf)）  

    &emsp;一种解决方案：
    ```stata
    split time,p(-)
    destring time1 time2,replace force
    gen ymdata = ym(time1,time2)
    format ymdata %tm
    ```
    &emsp;注：如有报错可能是内存中存在time1或time2变量，可以先删去或者`rename`变量；也可以在Excel中先进行分列后分为年份和月份变量后导入Stata再执行后两行语句。
    
    &emsp;推荐连享会推文：[Stata：文字型日期格式的转换](https://www.jianshu.com/p/1db2500b02bb)；[如何处理时间序列中的日期间隔 (with gaps) 问题？](https://www.jianshu.com/p/1c8423ee6c13)
    
1. **执行命令+ `if code = 111`无法识别？**  
    Stata中一个=代表赋值，两个=表示判断，用`if`等条件语句时都是使用`==`，命令应该修改为`if code == 111`。

1. **日期形式的变量如何比大小？**  
    比如设定变量 **ymdate** 为`%tm`形式，运行`if ymdate > 2001m1`后程序报错，应该将以上命令改为`if ymdate > tm(2001m1)`后即可运行。

        ------------以下增加于2019.1.19------------
        
1. **`xtreg`和`reg`命令的区别？**         
     &emsp;混合回归没有区别，如果要用固定效应就可以用`xtreg`或`reg`后加入`i.id`进行固定效应的控制。另外推荐`reghdfe`与`areg`来进行面板数据的回归，大家可以通过help文件进行进一步详细的查看。  
     &emsp;推荐连享会推文 - [Stata: 面板数据模型-一文读懂](https://www.jianshu.com/p/e103270ce674)
       
        ------------以下增加于2019.1.21------------

1. **B2_TPanel.do文件中的第396行命令`dis "x" _c`中的`_c`是什么意思？**  
    c代表continue，即命令输出不换行，通过查看 help display可知，_c代表让display命令不自动产生新的一行。

1. **如何查看VAR脉冲图的标题 irf of a to b？**  
    &emsp;横轴表示冲击作用的滞后期间数，纵轴表示被解释变量的变化，中间那条实线表示脉冲 响应函数，两侧虚线表示正负两倍标准差偏离带。主要看中间的线，表示给解释变量一个冲击后，被解释变量如何变化。也就是of a to b中的b如果变动一个标准差，a会如何变化。  
    &emsp;如果要进一步确认，通过`viewsource pvar2.ado`打开连老师的程序看看，或者搜索Eviews user guide，里面图形标注和连老师的一样。

1. **为什么在生成随机数前要设置种子数？**  
    计算机并不能产生真正的随机数，如果不设种子，计算机会自动设置种子。如果你要模拟什么的话，每次的随机数都是不一样的，这样就不方便你研究，如果你事先设置了种子，这样每次的随机数都是一样的，便于重现你的研究，也便于其他人检验你的分析结果。

1. **如何查看鱼骨图？**   
    连享会简书 - [Stata：图示边际效应
](https://mp.weixin.qq.com/s/knLWqN6Cfes8PiRdHy3C6w)

1. **双层循环怎么来设定？**  
    [双重嵌套循环语句](https://bbs.pinggu.org/thread-2153987-1-1.html)  
    ```Stata
    *--- 一个例子 ----
    forv i=1/9{
        forv j=1/9{  
            di   'x'+'y'
        }
    }
    ```
    
1. **`reg`、`xtreg`、`areg`、`reghdfe`之间的区别是什么？**  
    `reg`、`xtreg`、`areg`、`reghdfe`均是基于普通最小二乘法（OLS）开发的回归模型，在一定程度上四者可以相互替代。`reg`是简单的回归，可以通过`i.id`方式来添加固定效应；`xtreg`是面板回归，在使用前需要先设定面板（`xtset`）。当模型中有很多虚拟变量时用`areg`运行效率更高；当数据是高维数据时用`reghdfe`更好。

1. **`xtabond`命令中为什么要加`vce(robust)`选项？**  
    该选项为计算稳健的标准误，当存在异方差就加，一般都会认为有异方差，所以都会计算稳健的标准误。这样计算出的标准误考虑了异方差和序列相关，加了这个后的标准误一般比不加时大一点，显著性相应的低一点。

1. **在回归前如何做数据清洗？**  
[在应用Stata在进行计量回归之前，你真的会进行数据清理么？](https://mp.weixin.qq.com/s/qjjLMS9gUX8ZiqmWnP21aA)

        ------------以下增加于2019.1.23------------

1. **如何识别数据中0实际上为缺省值？**  
    具体需要看变量的含义，看为0时有没有意义，没有意义就是缺省，有意义了再查数据来源。对于一些社会调查数据，会有用户手册，其中对于缺省值有明确的定义。另外在进行数据分析之前应当熟悉数据，对数据的平均值、最大最小值等统计量都有较好的了解，也可以从以往文献中查看具体的处理方式。

1. **VAR模型中若同时存在平稳和非平稳变量该怎么解决？**  
    可以看连老师课件中的xtgcause命令，这个命令可以解决这个问题，不足之处是没有脉冲响应函数，其重点是做格兰杰因果分析。

&nbsp;
---
#### 画图与其他
32. **`%v.df`的含义?**  
    该语句用来规定输出形式。可以通过`help format`进行查看，其中v为输出结果的总位数，d为小数点后的输出位数。

        ------------以下增加于2019.1.18------------
 

1. **如何打开连老师推荐的Baum和Acock两本电子书？**  
    - 在Stata15中执行下述程序：  
  
   ```Stata
    global path "`c(sysdir_personal)'\PX_A_2019a\A1_intro"
    global R "$path\refs" 
    shellout using "$R\Baum_2006.pdf" 
    shellout using "$R\Acock_2014.pdf" 
   ```
    &emsp;注：如果路径正确而文件无法打开可能是系统的问题，可以在`sheelout`后加`using`再尝试
    - 可以直接在连老师发的资料("D:\stata15\ado\personal\PX_A_2019a\A1_intro\refs")中查找。
    
1. **屏幕涂鸦与缩放软件如何使用？**  
   [屏幕涂鸦和缩放：ZoomIt (教师利器)](https://www.jianshu.com/p/76b78af068f5)

1. **正则表达式是什么？如何利用stata对文本进行分析？**  
    连老师简书 - [Stata: 正则表达式和文本分析](https://www.jianshu.com/p/38315707ef6c)

        ------------以下增加于2019.1.19------------
 
26. **如何让Stata输出的图形更加专业简洁？**  
    可见连享会推文：[Stata绘图: 一个干净整洁的 Stata 图形模板qlean](https://www.jianshu.com/p/3e6d4f5c0f28)

1. **Stata作图如何导出，相关的教程？**  
    利用`graph export "table 1-1.png", as(png) replace`命令即可导出图形。  
    可见连享会推文：[普林斯顿Stata教程 - Stata做图](https://www.jianshu.com/p/069bb26ad842)

&nbsp;
---
#### 计量与模型
        ------------以下增加于2019.1.18------------
 

38. **Stata如何检验分组回归后的组间系数差异？**  
    - [Stata: 如何检验分组回归后的组间系数差异？](https://www.jianshu.com/p/38315707ef6c)
    - [[旧版]Stata: 如何检验分组回归后的组间系数差异？](https://www.jianshu.com/p/f27f8716dd2d)

1. **如何理解内生变量和外生变量？**  
    &emsp;在模型中，如果一个变量能够被该模型中的其他变量所决定或被影响，那么就称这个变量为**内生变量**。如果一个变量独立于系统中其他所有变量，其他变量的变化不对该变量造成影响，那么就称该变量为**外生变量**。  
    &emsp;通常我们将外生变量作为自变量，内生变量作为因变量，如果自变量中存在内生变量，就会产生共线性问题，随机干扰项对自变量X的条件期望为0也是为了保证随机扰动项是外生的。
    
    &emsp;变量一般分为外生变量，前定变量和内生变量。可以根据模型去说明，比如连老师的解释为：
    ```
    模型： y[it] = a0*y[it-1] + a1*x[it] + a2*w[it] + u_i + e[it]
    
    特征：解释变量中包含了被解释变量的一阶滞后项
            x[it]  —— 严格外生变量   E[x_it,e_is] =0  for all t and s
                        即，所有干扰项与x都不相关
            w[it]  —— 先决变量       E[w_it,e_is]!=0  for s<t, but E[x_it,v_is]=0 for all s>=t
                        即，前期干扰项与当期x相关，但当期和未来期干扰项与x不相关。
            y[it-1]—— 内生变量       E[x_it,e_is]!=0  for s<=t
                        即，前期和当期，尤其是当期干扰项与x相关
    
    动态面板里面的y_i,t-1就是前定变量
    ```
    
    &emsp;另附上有关内生性的一些讨论链接：[Stata - 内生性问题：处理方法与进展](https://www.jianshu.com/p/27352f4d89ef)

1. **过度投资如何计算？**  
    [盈余管理、过度投资怎么算？分组回归获取残差](https://mp.weixin.qq.com/s/J2rRp7N4QGIC09h4lenH7A)

        ------------以下增加于2019.1.19------------
 


1. **面板数据中的固定效应怎么理解？**  
    推荐连享会推文 - [Stata: 面板数据模型-一文读懂](https://www.jianshu.com/p/e103270ce674)

        ------------以下增加于2019.1.21------------

1. **IV，2SLS，GMM之间的关系？**  
    - [知乎-如何用简单的例子解释什么是 Generalized Method of Moments (GMM)？
](https://www.zhihu.com/question/41312883)  
    - [IV和GMM相关估计步骤，内生性、异方差性等检验方法](https://mp.weixin.qq.com/s/sDQYei-wxVI9m0nQDL9NMQ)
    - [GMM是IV、2SLS、GLS、ML的统领，待我慢慢道来](https://mp.weixin.qq.com/s/Jh5UNFlAmVWZqLu0L6vGxA)

1. **差分gmm和系统gmm的区别?**  
[动态面板模型的王冠—系统GMM什么鬼？](https://mp.weixin.qq.com/s/mi_b90Dfc5G5kIKhndNMKQ)

        ------------以下增加于2019.1.23------------
1. **在RD模型中是否需要加入控制变量？**  
    &emsp;在选定一个较小带宽h时，认为两侧样本在协变量方面是同质的，有理由相信分配变量包括结果变量的足够信息，所以可以不加入其它控制变量。断点回归如果在选择的带宽内有足够多的观测可以加控制变量，让估计更加精确。  
    &emsp;参考文献（高级版讲义P105-106）
    [Nichols, Austin. 2011.
rd 2.0: Revised Stata module for regression discontinuity estimation](http://ideas.repec.org/c/boc/bocode/s456888.html)

1. **PVAR模型里变量的顺序如何确定？**  
    &emsp;PVAR模型中变量的顺序会影响脉冲反应的结果，建议是把最内生的变量放在最前面。  
    &emsp;感兴趣的同学也可以了解一下全球向量自回归模型(Global VAR；GVAR)，GVAR是将经典VAR模型加以拓展，使其能够用于分析世界各国或各地区之间经济联系的一种模型，相关的参考文献可以在Google学术里搜索，如：[张延群.全球向量自回归模型的理论、方法及其应用[J].数量经济技术经济研究,2012,29(04):136-149](http://m.wenku365.com/p-5271012.html)

1. **R²多大合适？**  
    对于R²，时间序列数据模型一般较大，面板数据次之，截面数据最小。不用过于迷恋R²，很多时候R²是没有意义的，比如工具变量估计法R²可能为负数，模型不包括常数项时也可能为负数。此外，使用微观数据时R²通常较小，宏观数据较大。关于R²的大小，可以看一下发在top期刊上相似主题文章的R²，和其大小相近即可。

1. **门槛模型与断点回归的区别？**  
    &emsp;断点回归模型多用于政策评价上，使用时对数据有比较高的要求，断点需要明确。门限模型的适用范围比较广，只要解释变量x对被解释变量y的影响存在门限效应，就可以使用该模型验证。  
    &emsp;连老师在课程大纲中指出：“现在的实证分析中经常要处理结构变化问题，目前主要使用交叉项和分组回归等方式，但这种设定方法都需要预先知道或假设结构变化点，使其合理性颇受质疑。*面板门槛模型则基于'让数据说话'的原则，自动搜索结构变化点，从而克服了上述方法的局限。*”  
    &emsp;关于断点回归可以见下方的内生性专题。  
    &emsp;门槛回归详细可参考：[重磅！面板门槛回归学习手册](https://mp.weixin.qq.com/s/RDw-7HZFap4Hdybo-Uym5g)

1. **Heckman两步法与处理效应模型之间的异同，如何选择？**  
    &emsp;都是关于纠正存在选择内生性问题的方法，思想上大体相似。  
    &emsp;**Heckman两步法**主要解决样本自选择导致y非随机缺失。比如，当我们关注管理层业绩预测的精准度时，能够观测的样本仅局限于发布了业绩预测的公司，而非样本总体。  
    &emsp;**处理效应**解决0/1变量的x的内生性（自选择），自变量是一个内生的虚拟变量。比如，检验管理层发布盈余预测是否影响资本成本这个问题时，管理层是否发布盈余预测就是一个内生解释变量（D）。




&nbsp;
---
### 内生性专题
        ------------以下增加于2019.1.23------------
        
&emsp;&emsp;内生性是规范实证研究中无法逃避的问题，内生性问题无法的到有效的解决就无法保证论文结论的准确性和有效性。连老师在本次高级班中通过三个专题全面深入地介绍了内生性问题。 

&emsp;&emsp;分别为：第3讲的 ++处理效应模型++ 和 ++Heckman选择模型++，解决自我选择偏误导致的内生性问题；第4讲和第5讲的 ++断点回归设计方法（RDD）++ 和 ++合成控制法（SCM）++ 是近年来被公认为“最为干净”的准实验方法，主要用来解决政策评价领域的内生性问题。

> #### **内生性相关**
- **什么是内生性？**   
&emsp;&emsp;在引入具体的计量模型前，我们需要了解什么是内生性问题，以及它会为我们的估计带来什么后果。  
&emsp;&emsp;在计量经济学中“内生性”的定义为：解释变量`$x$`与扰动项`$ε$` 相关（相关系数或协方差不为0）。以一元回归`$y_i=α+βx_i+ε_i$`为例，内生性意味着`$Cov(x_i,ε_i)\not=0$`。  

- **内生性的后果**   
&emsp;&emsp;如果存在内生性，则称解释变量`$x$`为“内生变量”；反之称`$x$`为“外生变量”。如果`$x$`外生，则OLS估计量在样本容量`$n$`趋向无穷大时会依概率收敛至真实的参数值。
&emsp;&emsp;内生性的主要后果是使得OLS变得不一致，即无论样本容量多大，其估计的偏差不会消失。

- **内生性的主要来源**  
    ① 遗漏变量   
    &emsp;&emsp;遗漏了解释变量`$z$`，`$z$`被纳入了扰动项`$ε$`，如果`$x$`与`$z$`相关，就会产生扰动项与解释变量相关。也就是会使得OLS不一致，其偏差称为 “遗漏变量偏差”（omitted variable bias）。  

    ② 双向因果  
    &emsp;&emsp;如果`$x$`影响`$y$`，而`$y$` 也影响`$x$`，则存在 “逆向因果”（reverse causality），也称 “双向因果” 或 “互为因果”。内生性产生的机制为：扰动项`$ε$`增加后，导致`$y$`增加，而`$y$`增加又会影响`$x$`，从而扰动项`$ε$`与解释变量`$x$`相关。

    ③ 自选择偏误  
    &emsp;&emsp;在评估某些政策或项目的效应时，由于个体是否参加项目存在自我选择（self selection），故参加项目者（treated group）与未参加项目者（control group）可能存在系统差异，导致OLS估计不一致，因为主解释结果/因变量是在某种程度上被个人选择所决定。  
    &emsp;&emsp;比如，我们想看就业中的是否参加培训（`$x$`）是否会影响找到工作的概率（`$y$`），结果可能会发现没有参加培训的找到工作的概率更大。但这不一定意味着培训无效，因为能力强的人自己可能不会选择去参加培训，能力差的人反而会选择去参加培训。  
    &emsp;&emsp;这里的内生性来源为：扰动项`$ε$`中存在着同时影响 是否参加培训（`$x$`） 与 找到工作的概率（`$y$`） 的变量`$x_2$`（能力）。即，`$ε$`中存在着`$x_2$`与`$x$`相关，而`$x_2$`同时也会影响`$y$`（故被纳入了模型中），实质上也就是遗漏变量的问题。

    ④ 样本选择偏误  
    &emsp;&emsp;因变量的观察仅仅局限于某个有限的非随机样本时，样本的选择偏误就容易产生。Heckman选择模型就可以解决该类问题。  
    &emsp;&emsp;比如，当我们为研究人们的幸福感而发放问卷时，大部分问卷来源都是我们身边的同学和老师，最终得到的参数就无法估计所有人的幸福感只能估计出我们身边人的幸福感。因为我们身边的老师和同学不是所有人的随机代表，那么估计就是有偏的。  
    &emsp;&emsp;这里的内生性可以这样理解，相当于对整个模型的每个变量及扰动项都做了个变化（或者是只选取了部分，非随机），那么扰动项`$ε$`与解释变量`$ε$`是否无关，就无从保证了。

    ⑤ 变量测量误差  
    &emsp;&emsp;如果解释变量测量得不准确，则其测量误差也被纳入到扰动项中。可以证明，纳入扰动项中的测量误差一定会与解释变量相关，导致 OLS 不一致，称为 “测量误差偏差”（measurement error bias）。   
    
- **解决内生性的方法**   
&emsp;&emsp;针对以上不同来源的内生性，常见的解决方法包括工具变量法、随机实验与自然实验、断点回归（regression discontinuity design）、拐点回归（regression kink design）、双重差分法（双向固定效应模型）、倾向得分匹配、合成控制法等。



> #### **Heckman选择模型**  

&emsp;&emsp;当被解释变量观察值非随机缺失的问题时，可以用Heckman选择模型来解决内生性问题（样本选择偏误）。  

&emsp;&emsp;典型的例子为：我们想研究工资收入（`$y$`）的影响因素，只能观测到有工作的人的工资收入，没有工作的人就没有工资收入。但是，即使他们没有工作，我们也认为这些人应该有一个“保留工资”（即愿意工作的最低工资），只不过他们不愿意去工作。尽管这些缺少`$y$`的样本的解释变量能被观测到，但我们通常会将他们从样本中剔除，也就产生了样本选择的问题。   

&emsp;&emsp;连老师在讲义中列举的例子就是妇女工资的例子。


> #### **处理效应模型**

&emsp;&emsp;Heckman于1979年提出Heckman样本选择模型后，Maddala于1983年提出了处理效应模型，可用于内生的自选择问题。  

&emsp;&emsp;该模型是指当解释变量`$x$`是虚拟变量且存在自我选择问题的时候，可以参照传统Heckman两步法的思想解决这个问题，但称为“处理效应模型”。

&emsp;&emsp;连老师在讲义中列举的例子为大学毕业是否有助于获得更高的薪水。在这个例子中，能力更强的学生可能更容易进入大学，而能力更高也会更容易帮助他们获得更高的薪水，那么到底更高的薪水是大学带来的还是能力带来的，就无从得知。也就导致了自我选择的内生性问题，从而使估计带来偏误。

&emsp;&emsp;王德文等人在[《农村迁移劳动力就业与工资决定:教育与培训的重要性》](http://xueshu.baidu.com/usercenter/paper/show?paperid=7309377d9d43131730f2a275a31df53f&site=xueshu_se)中使用了处理效应模型来考虑农村迁移劳动力是否接受培训存在个人选择的问题，并在利用工资收入者的资料估计教育回报率时，利用Heckman两步法来考虑样本选择问题。
    
**补充：Heckman两步法与处理效应模型的区别**   
- 在Heckman两步法中，被解释变量`$y$`是只有部分观察值，在处理效应模型中`$y$`是全部都可以被观察到的
- 处理变量`$D$`在Heckman两步法中可能存在，而在处理效应模型中是必然存在的
- Heckman两步法中的Probit估计是为了解决样本选择的问题，而在处理效应模型中是为了解决自选择问题

    
    
    
> #### **断点回归设计（RDD）**   
&emsp;&emsp;如果想要解决内生性问题，最好的是随机实验，而在生活中随机实验是很难发生的，但生活中一些规则的随意性却为我们提供了良好的准实验。

&emsp;&emsp;断点回归设计就是一种准自然实验, 其基本思想是存在一个连续变量, 该变量能决定个体在某一临界点两侧接受政策干预的概率, 由于`$x$`在该临界点两侧是连续的，因此个体针对`$x$`的取值落入该临界点任意一侧是随机发生的, 即不存在人为操控使得个体落入某一侧的概率更大, 则在临界值附近构成了一个准自然实验。一般将该连续变量`$x$`称为分组变量 (assignment variable) 。
    
&emsp;&emsp;典型特征：随机分配原则，即实验对象无法知道自己及是否能够进入Treat组，如高考分数与录取结果。

&emsp;&emsp;典型例子：高考分数分别为642分与644分的同学本质上没有什么区别，而中山岭院的录取分为643分，此时这两个同学的薪水差异就可以较好地归结为岭院教育产生的结果。
 
> #### **合成控制法（SCM）**

&emsp;&emsp;政策评价领域一直存在空白地带，有些政策只针对某一个省或某一个州实施，此时DID和PSM都不再适用。

&emsp;&emsp;合成控制法适用于当实验对象只有一个时，利用潜在控制组就可以进行合成。

&emsp;&emsp;典型的例子为评价「加州」于1989年实施的禁烟法案政策效果。

&emsp;&emsp;基本思想是使用其他38个未实施禁烟法的州的加权平均来虚拟合成一个「合成加州」，进而对比分析「加州」和「合成加州」在1989年之后香烟消费量的差异，这就是政策效果。
    
    



> **参考资料**
- 连老师讲义  
- 陈强 - 高级计量经济学及Stata应用（第二版）

++内生性相关++
- [Back to Basics: OLS与内生性](https://mp.weixin.qq.com/s/9xU0-eDuh5lXWip23gzYug)
- [社会学定量分析中的内生性问题——测估社会互动的因果效应研究综述](http://www.society.shu.edu.cn/CN/abstract/abstract7511.shtml)
- [选择性偏误（selection bias）与样本选择偏差（sample self-selction）一样吗](https://bbs.pinggu.org/thread-3823334-1-1.html)

++工具变量相关++
- [工具变量法（一）: 2SLS](https://mp.weixin.qq.com/s/CxzYLg3kNzp_WGCZhWtUYg)
- [工具变量法（二）: 弱工具变量](https://mp.weixin.qq.com/s/IHmBMrGBHyEygydW6GgzLQ)
- [工具变量法（三）：IV真的外生吗？](https://mp.weixin.qq.com/s/CZnkcRc_c9uZlqMjlvH4UQ)
- [工具变量法（四）：GMM](https://mp.weixin.qq.com/s/aSbHlvAlcWiT6CnCpW1L6Q)
- [工具变量法（五）: 为何第一阶段回归应包括所有外生解释变量](https://mp.weixin.qq.com/s/pw5lRQxkj20iUntSGltLSw)

++选择效应模型++  
- [诡异的Heckman两步法](https://mp.weixin.qq.com/s/7AeuA_UooLFL644cWA_qSw)
- [选择模型（Selection models）在会计研究中的规范使用](https://mp.weixin.qq.com/s/taxqS3i8JbXBX55O3D60Lw)
- [王德文, 蔡昉, 张国庆. 农村迁移劳动力就业与工资决定:教育与培训的重要性\[J\]. 经济学(季刊), 2008, 7(4):1131-1148.](http://xueshu.baidu.com/usercenter/paper/show?paperid=7309377d9d43131730f2a275a31df53f&site=xueshu_se)
    
++断点回归++
- [为什么断点回归可以解决内生性问题？](https://www.zhihu.com/question/46752436?utm_source=wechat_search&utm_medium=organic)
- [断点回归（RD）学习手册（包含设计前提条件内生分组等显著性检验、精确断点&模糊断点等全套标准操作）](https://mp.weixin.qq.com/s/vm5gxGYiuTMKm7s0SLzPAA)
- [断点回归设计:基本逻辑、方法、应用述评](https://mp.weixin.qq.com/s/McwkfTjFf8Go_J_qlctByQ)
- [【文献笔记】You jump, I will not jump!断点回归的连续性假设](https://mp.weixin.qq.com/s/7DVrv-p86VIZR3_l4VbkQg)
- [RDD与政策评估（附Stata实现）](https://mp.weixin.qq.com/s/7DVrv-p86VIZR3_l4VbkQg)
- [【文献笔记】The closer you look, the less you’ll see. 断点回归的带宽](https://mp.weixin.qq.com/s/taW1ipUJXtnfL-K7MfH4fg)  

++合成控制法++
- [合成控制法（一）](https://mp.weixin.qq.com/s/ux0pMN5hn8axqb029yBYWg)
- [合成控制法（二）](https://mp.weixin.qq.com/s/JW6dyMNpCWiAU8XZQzNT-g)
- [合成控制法（三）](https://mp.weixin.qq.com/s/N4XYVUy5jN2qHua2-NCtyg)

++其他++ 
- [【香樟推文0620】运用断点回归设计做研究的规定动作](https://mp.weixin.qq.com/s/mcQLpW91cJWqeEnnB45qRA)
- [【经典回顾】如何正确理解和运用matching](https://mp.weixin.qq.com/s/XA1-1D9F0nHX3jKS0rp8fQ)

&nbsp;
---
#### 去哪里找答案？  
- 先`help`一下~最好查Stata的manual（用户手册）
- [Stata连享会推文列表](https://www.jianshu.com/p/de82fdc2c18a)
- [经管之家：Stata专区 | 连玉君老师Stata-VIP专区 | Stata常见问题](https://bbs.pinggu.org/thread-272681-1-1.html)
- 微信“搜一搜”

&nbsp;
---
#### 如果想进一步了解？ 
- 连老师讲义中给出了许许多多的参考资料，包括了该方法的起源文献及经典应用
- 看top期刊中文献的原理、例子及解决方法


&nbsp;
---
### 答疑须知
由于各种限制，Stata 现场培训只能采用集中授课的方式进行，无论对于老师还是学生，这种高强度的互动都是非常辛苦的。因此，各位在开课前需要做充分的准备，以便提高听课的效率。

>#### 预习资料
- **Stata视频**
   - 作为 Stata 入门资料，建议看看连玉君老师的视频公开课：[连玉君优酷视频](http://i.youku.com/arlion)。
   - 对于现场班已经缴费的学员，可以看看免费赠送的连玉君老师制作的 [Stata初级视频](http://www.peixun.net/view/240.html) 和 [Stata高级视频](http://www.peixun.net/view/5.html)。
-  **教科书**
   - Baum, C. 2006. An Introduction to Modern Econometrics Using Stata: Stata Press. 
   - Acock, AC, 2014. A gentle introduction to stata: Stata Press.
   - 完整书单参见： [Stata连享会推文-Stata 书库来袭](https://www.jianshu.com/p/f1c4b8762709)

>#### 答疑方式

[[Stata初级班 FAQs]](http://note.youdao.com/noteshare?id=ae8760353f8ae10eb8c69cd3ffc21112&sub=6439155130574F0BAFB2D7756657AD6B) 汇总了往期学员的常见问题。烦请您先行浏览，如果未能找到合适的解答，可以进一步通过如下三种方式参与答疑：

1. **微信群答疑** (时段：`8:00-9:00`; `18:00-22:30`)：在课程微信里，助教们的群名片都带有 **「助教」** 字样，因此，你可以在群里 **@助教**，然后提出你的问题。建议尽可能清晰地描述你的问题，可以配合截图，或说明对应的 dofile 行号等方式让助教第一时间确认你的问题。(微信截图快捷键：**Alt+A**)
2. **课前和课后答疑**(时段：`8:30-9:00`; `17:30-18:00`)：你也可以提前半小时到教室，现场咨询助教们。 
3. **主讲教师集体答**(时段：`17:00-17:30`)：在每天课程结束后，主讲教师会安排半小时的集体答疑时间。学员举手，按先后顺序答疑。这样，大家都可以共同分享答案。（温馨提示：我们优先回答当天授课所涉及的内容；请在提问前认真梳理你的问题，以便提高互动效率）。


**温馨提示：** 建议各位在课间休息 (10分钟)，以及午休期间 (12:00-2:00) 不要答疑，以便您自己和主讲老师、助教们都能够得到适当的休息。


&nbsp;
---
> **初高级班助教 共同整理**  


> **更新时间**  
2019年1月24日