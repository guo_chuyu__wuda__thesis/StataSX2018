## 沙莎分工(解决“怎么办”问题)
建议主要参考连老师发在主教群里的论文Section 4, 5 & 6 (已将链接放在参考文献)；当然也可以参考以前你做的笔记

### 1 弱工具变量的检验方法
#### 1.1 同方差标准误的弱工具变量检验
#### 1.2 异方差标准误的弱工具变量检验
#### 1.3 以第一阶段 (First-Stage) 回归的F统计量为筛选依据

### 2 弱工具变量的推理
#### 2.1 对于恰好识别 (Just-identified) 的模型进行 Anderson-Rubin 检验 
#### 2.2 对于过度识别 (Overidentified) 模型的弱工具变量处理
- 同方差情形
- 异方差情形

### 3 文献中尚未解决的问题

### 主要参考文献
- [Andrews I, Stock JH, Sun L. Weak Instruments in IV Regression: Theory and Practice. *Annual Review of Economics*. Forthcoming.](https://quqi.gblhgk.com/s/3829824/2FzSawHqWHDLPSoR)