# LaTeX小白入门


> 作者：周璐（清华大学）      
> 邮箱：zhouluthu@126.com


## 1. LaTeX 是什么？

### 1.1 LaTeX 简介

介绍 LaTex 之前必须先介绍 $\TeX$：TeX  是一种计算机排版软件，用于排版包含有数学公式的书籍。LaTeX 是用 TeX 编写的宏代码，拥有更规范的命令和一套预定义的格式，对书籍作者更加友好。国际上数学、物理、计算机等学科的学术期刊接受  LaTeX  作为投稿格式，在其他专业的论文和著作撰写中也有大量应用。

> LaTeX 读作「Lei taik」，Logo 长这样：$\LaTeX$。


### 1.2 LaTeX  的优势

- 形式与内容分离，让写作者可以心无旁骛地专注于内容的创作，不用考虑字体、行距、边距、编号等问题，可以在  LaTeX 中完成统一设置。
- 数学公式、图片、表格可以自动编号和交叉引用
- 文献管理条理清晰，引用便捷，修改引用格式非常方便
- 代码完成后编译可以直接生成 PDF 预览，所见即所得。

### 1.3 常用编辑环境

LaTeX 系统包括复杂的一套软件，如果要分开安装各种排版引擎、编译脚本、配置文件等等会非常麻烦，我们可以直接安装打包好的发行版。

最常用的一套中文套装是来自中国科学院的 CTeX ($C\TeX$) ，它对中文支持良好，安装简单方便。CTeX 的主要编辑器是 WinEdt ，PDF 预览器是  SumatraPDF。

CTeX 最大的缺点是它只能在 Windows 系统中使用，因此本文将用多平台通用的 TeXLive 详细说明安装过程。对 CTeX 感兴趣的同学可以自行下载安装。

CTeX 下载地址： <http://www.ctex.org/CTeXDownload/>

本文将详细介绍的是 TeXLive，它可以在 Windows、Mac OS、Linux 等不同操作系统平台下使用。

## 2.TeXLive 的安装

可以使用官方网站，选择 texlive2019.iso 进行下载：<http://www.tug.org/texlive/>

如果希望下载更快，可以使用国内的镜像网站：
<https://mirrors.tuna.tsinghua.edu.cn/CTAN/systems/texlive/Images/>

点开镜像站后，选择 texlive2019.iso 进行下载。

![TeXLive开源镜像站](https://images.gitee.com/uploads/images/2019/1212/233734_b7b10984_1522177.png)

本地安装后，找到 readme-html.dir 的文件夹。

![](https://images.gitee.com/uploads/images/2019/1212/233734_5eb5bda4_1522177.png)

打开文件夹后，找到 readme.zh-cn.html 文件，在浏览器中打开。

![](https://images.gitee.com/uploads/images/2019/1212/233734_58a81bd3_1522177.png)

- Mac 平台
 Mac 平台的安装可以参照上文帮助文件中的说明进行安装，本文主要以 Windows 为例。

- Windows 平台
运行 Windows 批处理文件 install-tl-advanced.bat 
![](https://images.gitee.com/uploads/images/2019/1212/233734_4c3202d1_1522177.png)

在弹出的各个窗口不断点击“安装”即可，如果需要修改安装目录可以在 Directories 中修改。

![](https://images.gitee.com/uploads/images/2019/1212/233734_42f5ec2d_1522177.png)

![](https://images.gitee.com/uploads/images/2019/1212/233734_eef437d3_1522177.png)

等候约半小时后安装完成，点击 TeXworks Editor 启动编辑器，开始使用 LaTeX 编辑文章。

## 3.第一次使用 LaTeX 编辑文章

下面我们将使用 LaTeX 完成一篇简单文章的写作。

### 3.1 中英文兼容问题

LaTeX 中的基本文档类有三个： article, report, book， 分别对应短篇幅的文章、中篇幅的报告、长篇幅的书籍。为了更好地支持中文编码，我们使用与其分别对应的中文文档类：ctexart, ctexrep, ctexbook，并使用 UTF-8 编码。

基础代码与运行效果如下图所示：

![](https://images.gitee.com/uploads/images/2019/1212/233734_fbc1853b_1522177.png)


### 3.2 正文文字

在文章写作中，我们经常需要将文字分段并编号（如第几章，第几部分等等）。在短文 ctexart 中，我们使用 **section** 为段落编号，更细一层的分段使用 **subsection**。

![](https://images.gitee.com/uploads/images/2019/1212/233734_b1cf56e7_1522177.png)

如上图所示，我们只需要关心文字层次结构，不需要手动为章节编号， LaTeX 会自动完成这一过程。

### 3.3 插入公式

数学公式的简洁美观是 LaTeX 最重要的特色之一。LaTeX 中插入公式有两种方式：一种是行内公式，直接用美元符号 `$` 将公式首末包围起来即可；另一种是单行公式，要用 **equation** 「环境」来定义。代码如下所示：

```LaTeX
\section{公式示例}
第一种是行内数学公式，比如在这里直接插入质能公式 $E=mc^2$。
第二种是单独的公式段
\begin{equation}
S_{circle}=\pi r^2
\end{equation}
```

完整代码与运行结果如图所示。

![](https://images.gitee.com/uploads/images/2019/1212/233734_0965e4a9_1522177.png)

### 3.4 插入表格
表格插入非常直观，将文段类型指定为 table 类型，*&* 进行分栏，*\\*进行换行， *hline* 绘制表格线。代码如下所示：

```
\section{表格示例}
上课时间：2020年1月8-10,12-14,15-17日（9天）
上午9:00至12:00; 下午2:00至5:00; 答疑5:00至5:30
上课地点：北京市海淀区
\begin{table}[ht]
\centering
\begin{tabular}{ll}
\hline
课程类型 & 培训时间     \\
\hline                
初级班  & 2020年1月8-10日             \\
高级班  & 2020年1月12-14日            \\
论文班  & 2020年1月15-17日            \\
全程班  & 2020年1月8-10,12-14,15-17日 \\
\hline
\end{tabular}
\end{table}
```

运行效果如下图所示。

![](https://images.gitee.com/uploads/images/2019/1212/233734_420eaee7_1522177.png)


### 3.5 插入图片
首先需要将图片存放在 tex 文件同文件夹或记录图片指定路径。在 tex 文件文首加入下面一行代码，引用图片包。

```
\usepackage{graphicx}
```

然后将文段类型指定为 **figure** ，并使用 `includegraphics` 插入图片，**scale** 指定图片缩放倍数，代码如下。

```
\section{图片示例}
\begin{figure}[b]
\centering
\includegraphics[scale=0.25]{qrcode.png}
\caption{连享会二维码}
\end{figure}
```

运行效果如下图所示。

![](https://images.gitee.com/uploads/images/2019/1212/233734_b81f32dd_1522177.png)


## 4. 使用 LaTeX 的必备工具与网络资源

### 公式识别神器 Mathpix Snipping Tool

> **功能：** 截图识别公式，一键转换成 LaTeX 代码。     
> **官网地址：** <https://mathpix.com/>

![](https://images.gitee.com/uploads/images/2019/1212/233734_b0f917df_1522177.png)

### 表格识别神器 Tables Generator

> **功能：** 像在 Excel 里一样使用表格，一键转换成 LaTeX 代码。    
> **官网地址：**  <http://www.tablesgenerator.com/>

![](https://images.gitee.com/uploads/images/2019/1212/233734_cb3e7f14_1522177.png)

### 常用 LaTeX 资源网站

- TeX 资料大全 *CTAN*： <https://www.ctan.org/>
- TeX 用户组织 *TUG*： <https://www.tug.org/>
- 中文 CTeX 第一站： <http://www.ctex.org/HomePage>


