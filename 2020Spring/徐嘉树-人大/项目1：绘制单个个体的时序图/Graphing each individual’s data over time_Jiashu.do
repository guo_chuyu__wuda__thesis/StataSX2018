
/* 

Title: 
Graphing each individual’s data over time

Authors: 
Mark D. Chatfield University of Queensland 
Brisbane, Australia m.chatfield@uq.edu.au

The Stata Journal (2018) 18, Number 3, pp. 503–516


TA: Jiashu Xu

*/

cd "/Users/jiashu/Desktop/2020 winter/JX"

* 1.1 Dataset used in examples
* use http://www.stata-press.com/data/r15/nlswork.dta
 use "nlswork.dta",clear
      xtset
      xtdescribe


* To reproduce figures 1 and 2, you will need to set the Stata Journal graph scheme: 

 set scheme sj

* In Stata for Windows to create multiple graphs as tabs within one window (rather than as separate windows), you might want to use the setting
**  only available on windows
* set autotabgraphs on

* 2 Simple example—Graphing a single variable over time for each individual
* 2.1 Preparatory work
 generate wage = exp(ln_wage)
      summarize wage, detail
      generate wage_trunc = min(wage, 20) if !missing(wage)
      label variable wage_trunc "Wage ($/hour)"
 keep if idcode <= 6

 
* 2.2 Code for graphing a single variable over time for each individual
putdocx clear
putdocx begin, landscape
putdocx paragraph
levelsof idcode, local(idcodes) clean
foreach id of local idcodes {
   scatter wage_trunc year if idcode == `id' , connect(l) sort ///
     ylabel(0 2 4 6 8 10 12 14 16 18 20 "≥20", angle(horizontal))  /// 
     yscale(range(0 20.5))  /// 
     xlabel(68 "1968" 73 "1973" 78 "1978" 83 "1983" 88 "1988")  /// 
     xmtick(68 69 70 71 72 73 75 77 78 80 82 83 85 87 88, grid notick)  /// 
     xscale(range(67.5 88.5))  /// 
     title("id `id'")  /// 
     graphregion(color(white))  /// 
     name(g`id', replace)
     graph export "temp_graph.png", replace
     putdocx image "temp_graph.png"
}
putdocx save "Example 1. Individual Graphs.docx", replace
erase "temp_graph.png"

************************
* 2.3 Comments on code *
************************

* basic code for the first individual:
 scatter wage_trunc year if idcode == 1, connect(l) sort

* to have the same labels and scaling of axes for each graph
/* y axis: the values to reflect the truncation of wages at $20 per hour, and for 
major ticks, (horizontal) labels and grid lines to appear from 0 to ≥ 20 in steps of 2. */

ylabel(0 2 4 6 8 10 12 14 16 18 20 "≥20", angle(horizontal)) /// 
yscale(range(0 20.5)) ///

/* x axis: major ticks and labels every 5 years from 1968 to 1988 and grid lines 
to appear only for the 15 years of data collection. */

xlabel(68 "1968" 73 "1973" 78 "1978" 83 "1983" 88 "1988") /// 
xmtick(68 69 70 71 72 73 75 77 78 80 82 83 85 87 88, grid notick) /// 
xscale(range(67.5 88.5)) ///
* Note that the range of the axes needed to be expanded so that grid lines appeared at y = 20, x = 68, and x = 88.
 
 
* get printed: changed the line and fill color and opacity of the outer graph region (which is blue in scheme s2color) to white.

graphregion(color(white)) ///
 
/* To produce a graph for each individual, we need to create a local macro 
containing all idcodes, used a loop to run the same code for each individual, 
and generalized the code (to refer no longer to idcode == 1 but to idcode == ‘id’)*/

      levelsof idcode, local(idcodes) clean
      foreach id of local idcodes {
(commands referring to `id') }

* insert an individual’s ID into the title of his or her graph: 
title("id `id'")
 
* use the name() option to see multiple graphs within Stata as they are created (and re-created)
name(g`id', replace)

* To put all graphs into a .docx file, use the following code:
      putdocx clear
      putdocx begin, landscape
      putdocx paragraph
(inside foreach loop)
graph export "temp_graph.png", replace putdocx image "temp_graph.png"
      putdocx save "Example 1. Individual Graphs.docx", replace

* Finally, having served its purpose, the .png file was erased from disk:	  
erase "temp_graph.png"

* 3 A more complicated example—Graphing multiple variables over time for each individual
* 3.1 Preparatory work 

      * use http://www.stata-press.com/data/r15/nlswork, clear
	  use "nlswork.dta",clear
      generate wage = exp(ln_wage)
      keep idcode tenure hours union msp race grade age year wage
      generate death_yr = 83 if idcode==5 // variable created for illustration
	  describe
 
* to discover which variables vary with year within an individual and which do not. 
* More generally, one can use the community-contributed command distinct (Cox and Longton 2008) to check whether variables are constant within an individual. 
      * findit distinct
      distinct idcode
      distinct idcode grade, joint missing

* consider the four time-varying continuous variables: wage, tenure, hours, and age.
      summarize wage, detail
      generate wage_trunc = min(wage, 20) if !missing(wage)
      label variable wage_trunc "Wage ($/hour)"
	  
      summarize tenure, detail
	  generate tenure_trunc = min(tenure, 20) if !missing(tenure)
      label variable tenure_trunc "Tenure (years)"
	  
****** mistake in the paper
      summarize hours, detail
      generate hours_trunc = min(hours, 60) if !missing(hours)
      label var hours_trunc "Usual hours worked"
	  
* consider the two time-varying categorical variables union and msp.
      tabulate union, missing
      generate lab_union = "U" if union==1
      replace lab_union = "-" if union==0
      tabulate msp, missing
      generate lab_married = "M" if msp == 1
      replace lab_married = "-" if msp == 0
 
* consider the nontime-varying variables. 
      decode race, generate(race_str)
* As in the simple example, the dataset is now restricted to the first 6 individuals.	  
	  keep if idcode <= 6
 
* 3.2 Code for a graph of multiple variables over time for each individual

generate y_union = 23
generate y_married = 26
generate y_age = 0

putdocx clear
putdocx begin, landscape
putdocx paragraph

levelsof idcode, local(idcodes) clean
foreach id of local idcodes {
     local xline ""
     local xlinetext ""
     summarize death_yr if idcode == `id'
if r(N) > 0 {
     local death_year = r(min)
     if r(min) <= 88 {
          local xline "xline(`death_year')"
          local xlinetext `"text(21 `death_year' "Died")"'
     }
}
levelsof race_str if idcode == `id', local(race_info) clean
levelsof grade if idcode == `id', local(grade_info)

twoway (scatter wage_trunc tenure_trunc year, connect(l l) sort /// 
        clwidth(thick) msymbol(O T)) /// 
    (scatter y_union y_married year, msymbol(i i) /// 
        mlabel(lab_union lab_married) mlabposition(0 0)) /// 
    (scatter y_age year, msymbol(i) mlabel(age) mlabposition(6) /// 
        mlabgap(*0.1) mlabcolor(gs8)) /// 
    (scatter hours_trunc year, yaxis(2) connect(l) sort msymbol(Sh) /// 
        clpattern(dash)) /// 
     if idcode == `id', /// 
     xlabel(68 "1968" 73 "1973" 78 "1978" 83 "1983" 88 "1988", notick /// 
        labsize(small) labgap(*6)) /// 
     xmtick(68 69 70 71 72 73 75 77 78 80 82 83 85 87 88, grid notick) ///
     `xline' `xlinetext'   ///
     xscale(range(67.5 88.5)) ///
     plotregion(margin(zero)) ///
     xtitle("") ///
     text(-1 66 "Age", size(small) color(gs8)) ///
     text(-2.7 66 "Year", size(small)) ///
     graphregion(color(white)) ///
     legend(order(1 - "             " 6 2 ) span cols(3) ) ///
     ytitle("● Wage ($/hour)   " "􏰀 ▲ Tenure (years)    ") ///
     ytitle("􏰁☐ Usual hours worked        ", axis(2)) ///
     title("id `id'") subtitle("(`race_info', grade `grade_info')") /// 
     ylabel(0 2 4 6 8 10 12 14 16 18 20 "≥20" 23 "Union?" 26 "Married?", ///
          angle(horizontal)) /// 
     ylabel(0 6 12 18 24 30 36 42 48 54 60 "60+", axis(2) /// 
          angle(horizontal)) /// 
     yscale(range(0 27)) yscale(range(0 81) axis(2)) ///
     name(g`id', replace)
	 
       graph export "graph.png", replace
       putdocx image "graph.png"
  }
  putdocx save "Example 2. Individual Graphs.docx", replace
  erase "graph.png"

************************
* 3.3 Comments on code *
************************
 
* plot the variables wage trunc and tenure trunc on the same y axis:
      (scatter wage_trunc tenure_trunc year, connect(l l) sort         ///
            clwidth(thick) msymbol(O T))                               ///
/*
Using the same y axis, I added invisible markers with visible marker labels for 
the two time-varying categorical variables union and msp. I specified particular 
heights on the y axis for this information so that it appeared above the presentation 
of the continuous variables wage trunc and tenure trunc (which range from 0 to 20). I substituted text for labels on the y axis.
*/

 generate y_union = 23
      generate y_married = 26
(scatter y_union y_married year, msymbol(i i) /// 
mlabel(lab_union lab_married) mlabposition(0 0)) ///
, ylabel(0 2 4 6 8 10 12 14 16 18 20 "≥20" 23 "Union?" 26 "Married?", /// 
angle(horizontal)) ///
 
/*
In the same way, I added invisible markers with visible labels underneath for age 
(at y = 0), which gave the impression that two scales (age and year) were 
simultaneously used on the x axis. To make the graph tidy, I used some options 
that included eliminating the margin between the axes and the plot region.
*/ 
 
generate y_age = 0
(scatter y_age year, msymbol(i) mlabel(age) mlabposition(6) /// 
mlabgap(*0.1) mlabcolor(gs8)) /// 
, plotregion(margin(zero)) /// 
xtitle("") /// 
text(-1 66 "Age", size(small) color(gs8)) /// 
text(-2.7 66 "Year", size(small)) /// 
 
/*
 I plotted the variable hours trunc on the y axis on the right by specifying 
 yaxis(2). This axis was labeled and scaled in such a way that the grid lines 
 for the first y axis nicely corresponded with labels on the second y axis. 
 (Note that unless axis(2) is specified, Stata thinks y-axis options apply to 
 the first axis.)
*/

(scatter hours_trunc year, yaxis(2) connect(l) sort msymbol(Sh) clpattern(dash)) ///
 , ylabel(0 6 12 18 24 30 36 42 48 54 60 "60+", axis(2) ///
 angle(horizontal)) ///
 yscale(range(0 27)) yscale(range(0 81) axis(2)) ///
 
 
 legend(order(1 - " " 6 2 ) span cols(3) ) /// 
 ytitle("● Wage ($/hour)   " "􏰀 ▲ Tenure (years)    ") ///
 ytitle("􏰁☐ Usual hours worked        ", axis(2)) ///
 
 
  local xline ""
      local xlinetext ""
      summarize death_yr if idcode == `id'
      if r(N) > 0 {
           local death_year = r(min)
           if r(min) <= 88 {
                local xline "xline(`death_year')"
                local xlinetext `"text(21 `death_year' "Died")"'
           }
      }
      twoway ..., `xline' `xlinetext'

	  
      levelsof race_str if idcode == `id', local(race_info) clean
      levelsof grade if idcode == `id'́, local(grade_info)
      twoway ..., subtitle("(`race_info', `grade grade_info')")

	  
/* References

Center for Human Resource Research. 1989. National Longitudinal Survey of Labor Market Experience, Young Women 14–26 years of age in 1968. Ohio State University.
Cox, N. J. 2010. Speaking Stata: Graphing subsets. Stata Journal 10: 670–681.
Cox, N. J., and G. M. Longton. 2008. Speaking Stata: Distinct observations. Stata Journal 8: 557–568.

*/


* END





