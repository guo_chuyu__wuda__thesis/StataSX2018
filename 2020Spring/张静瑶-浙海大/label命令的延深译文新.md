>  label命令的延深

>作者：张静瑶（浙江海洋大学）
>邮箱：1132358871@qq.com

>**[编者按]**：本文内容摘译自以下文章。
>**[source]**:Daniel Klein. Extensions to the label commands[J]. The stata journal, 2019, 19(4):867-882.

**摘要**：数据管理任务包括操作变量、变量标签、和值标签。当使用stata通用的命令和函数来处理第一个任务时，管理变量和价值标签就不太方便了。在本文中，我会介绍一个新的命令：elabel，它增强了stata中label命令的功能。我会用不同的例子来讨论些优化之处，并演示如何在elabel中添加新的命令。

## 1  背景

在stata中操作变量很方便，我们可以使用通配符（参见 [U] 11.4 varname and varlists）在同一时间内缩写变量名或引用多个变量。重命名命令(参见[D] rename group)系统地更改组变量名。此外，我们可以应用算术和关系表达式以及许多函数(参考[D] generate and [U] 13 Functions and expressions) 和转换规则(参考[D] recode)来创建或更改变量的内容。

与操作变量不同，管理变量和价值标签并没有那么便捷。label命令（参见 [D] label）不支持value-label名称中的通配符。而且没有用于更改value-label名称的专用命令。并且，Stata的表达式和函数并不是很适用于label。例如，我们不能更改labels中的特定单词；我们必须定义或重新定义完整的 label。同样的，除了一次指定一个整数到文本的对应之外，没有其他更加容易的方法来定义或修改值标签。

对于数据而言，有许多community-contributed(外部命令) 用于操作变量和值标签，这些命令超出了stata的本地标签命令（stata自带的label命令） (例如, Blumenberg 2012, 2016; Cox 2000; Jann 2007, Joly 2002; Klein 2011; Newson 2007, 2009, 2018; Nichols 2011; Weesie 2005a,b)。许多用户提供的命令都是为了解决一个特定的问题而定制的。尽管它们具有功能性，但由于不同的作者对于命令名和语法没有一致的协商，因此查找和使用适当的命令来解决特定问题有时不方便。

在本文中，我会介绍另一个用于操作变量和值标签的命令：elabel。在这里，我所使用的方法与这一领域的大多数用户提供命令的方法有些不同。

本文简要概括如下；在第3节中，我会对elabel命令进行简要的技术概述，并展示基本的应用示例。在第4节中，我会进一步解释elabel命令解决特定问题的基本思想。在第5节中，我将展示如何在elabel中添加新命令。

## 2  elabel 命令

### 2.1  语法

```
Elabel  subcommand [elblnamelist]  [mappings] [iff eexp] [, options]
```

### 2.2 描述

elabel操作变量标签和值标签。接下来的部分，我将简要描述其各自的语法。

**子命令**

elabel的子命令与label的子命令相同，通常情况下，我们可以在能输入label的任何地方输入elabel，如果elabel没有在子命令中添加任何内容，其作用与stata的label命令相同，elabel还提供了额外的子命令，我会在4.3、4.4和第5节中讨论。

**值标签名称列表**

elblnamelist是一个值标签名称列表，其中可能包含通配符*、~和?；这些字符具有与varlist相同的含义(参见[U] 11.4 varname and varlists)。elblnamelist中的名称也可以间接引用值标签名称；(varname)是指在当前标签语言中附加到varname的value-label名称(如果有的话)(参见[D]] label language)。此时可以指定多个变量名。

**对应关系**

对应关系是特定于子命令的，并与相应的label命令一起使用。通常，用标签对应替代值(或变量名),一般形式是

```
{#|varname} "label"[ {#|varname} "label" ....]
```

使用elabel，我们可以使用括号分别对整数值(或变量名)和标签进行分组。一般形式是

```
(numlist|=eexp|varlist) ("label"[...]|=eexp)
```

eexp将在下面进行解释。我们将在第4节中进一步讨论。

**iﬀ eexp**

iff(注意双f)限定符类似于Stata的标准if限定符；然而，eexp通常不引用数据集中的观察值，也不包含变量名。

eexp是一个Stata表达式，通常包含字符#和@。#字符充当(整数)值的占位符，而@字符表示值标签或变量标签中的文本。

当跟随iff限定符时，eexp必须计算为true(!=0)或false (==0)，它从值标签中选择整数到文本相对应的子集。例如，iff(# < .)从值标签中选择所有未丢失的整数和相关文本。同样，iff (@ == "Foreign")只从值标签中选择文本Foreign和相关的整数值。注意@字符不能用双引号括起来。

 当eexp跟随等号时，#将被值标签中的整数值替换；同样，@也被相应的文本所代替。两个字符可以组合，但计算的eexp必须是数值型或字符串型。

## 3 elabel命令的基本例子

### 3.1 值标签名称的通配符

在第一个示例中，假设我们对auto.dta中的值标签origin的内容感兴趣。要使用elabel查看内容，我们使用list子命令。

```
. sysuse auto
 (1978 Automobile Data)
. elabel  list origin  origin: 
origin: (1978 Automobile Data)
 0 Domestic
 1 Foreign
```

我们得到的输出看起来很熟悉；它与我们使用Stata的标准label命令得到的输出是相同的。但是，使用Stata的label命令时，我们不能在值标签名称中使用通配符。例如，输入

```
. Label  list  ori~ 
value  label  ori  not  found 
r(111); 
```

导致错误消息。通过elabel，我们仍然得到了想要的结果:

```
. elabel  list  ori~
 origin: 
          0 Domestic
          1 Foreign
```

我们还可以方便的使用通配符同时引用多个价值标签名称。 

## 3.2间接指定值-标签名称

在上面的例子中，我们输入了value-label名称来源，或者用通配符替换了名称的部分。通常，我记不住附加到变量上的值标签名称。但我记得变量名。假如我们要列出附加到变量foreign的值标签的内容。使用elabel，我们不需要查找相应的value-label名称；我们可以简单地将变量名括在括号中，输入

```
 elabel  list (foreign) 
origin:
         0 Domestic 
         1 Foreign
```

并得到期望的结果。有经验的Stata用户能够识别括在括号内的变量名的语法；宏函数label(参见 [P] macro)与这个语法相同。利用elabel，我们可以在任何允许elblnamelist的地方使用这种语法。

## 3.3附加返回结果

尽管elabel的输出列表与我们从 Stata的label列表中得到的输出完全相同，但在幕后还是有一些差异的。让我们看看返回的结果。

```
. return list
 scalars:
 r(min) = 0
 r(max) = 1
 r(hasemiss) = 0
 r(nemiss) = 0
 r(k) = 2
macros:
r(name) : "origin" 
r(values) : "0 1" 
r(labels) : ""Domestic" "Foreign""
```

elabel列表返回标签列表将返回的所有标量。elabel list可能更有用，它还返回值标签名称、整数值和相关的文本。

## 3.4整数-文本对应的子集

为了简单起见，暂时忽略动机方面的原因，假设我们希望只列出整数到文本的对应，其中整数的值大于0。我们通过输入来实现这一点

```
. elabel list origin iff (# > 0)
 origin: 
         1 Foreign
```

这里的iff限定词类似于Stata中的if限定词(参见[U] 11.1.3 if exp)，大多数用于操作变量的命令都允许使用这个限定词。#字符充当源中的整数值的占位符。我们还可以使用@字符引用整数值-文本的对应。假设我们希望只列出整数到文本的对应，其中的文本包含大写的D:

```
. Elabel  list  origin  iff  strpos(@, "D") 
origin: 
0 Domestic
```

strpos()函数是一个任意的选择；我们可以选择strmatch()(参见[FN]String functions)。通常，我们可以在iff限定符后面的表达式中使用Stata任何形式的函数，只要它对任何值#和字符串@求值为true(!=0)或false(==0)。

# 4具体问题的一般解决方案

## 4.1定义数值到数值对应的值标签

与其他许多由用户提供的用于操作变量和值标签的命令相比，elabel采用了一种更通用的方法，我会用labutil包中的两个示例来说明(Cox 2000)，数据可公开获得。尽管labutil包中的大多数命令可以追溯到本世纪初，但该包仍然是用于管理变量和值标签下载频率最高的命令包。

labutil可以解决的一个具体问题是定义“包含反对数值的以10为底的对数的值标签”(Cox 2000)。解决这个问题的命令是lablog。可以举例为

```
. lablog logs, values(1/4) 
label def logs 1 "10" 2 "100" 3 "1000" 4 "10000", modify
```

其中回显lablog创建的label define命令。在值标签名称、对数值之后，我们指定了values()选项，列出了希望与标签关联的(整数)值。

labutil解决的第二个更普遍的问题是定义“一个数值-数值标签的值标签”(参见labmap. hlp in Cox[2000])。解决这个问题的相应命令是labmap。例如，我们定义了一个值标签，它将午夜后的分钟定为小时。

```
 Labmap  time, values(0(60)240) first(12) max(12) step(1) postfix(" am") .
 label list time
 time: 
0 12 am 
60 1 am 
120 2 am 
180 3 am 
240 4 am
```

在value-label名称、时间之后，我们指定values()选项；我们还包括first()、max()、step()和postfix()选项，它们分别是指第一个(数字)标签、最大标签、标签之间的步骤和其他文本。

当我们比较这两个例子时，似乎解决一般问题的命令labmap也具有更复杂的语法。复杂的语法可能更难记住，也更难理解。此外，请注意，第一个问题(将值对应到其底数为10的反对数)实际上是第二个问题(将数值对应到数字标签)的特殊情况。然而，更通用的命令labmap并不适用于第一个问题。

使用elabel，我们可以更一般地处理这两个问题：将(整数)值对应它们自己任意的函数。下面是我们如何用elabel解决以上两个问题。

```
Elabel  define  logs2 (1/4)  (= strofreal(10^#)) . 
elabel  define  time2 (0(60)240)  (= strofreal(cond(#, #/60, 12)) + " am") .
 elabel  list  logs2  time2 
logs2: 
1 10 
2 100
3 1000 
4 10000 
time2: 
0 12 am
 60 1 am
120 2 am 
180 3 am
 240 4 am
```

我们来检查一下代码。Stata用于定义值标签的命令是label define；因为我们希望定义值标签，所以我们使用相应的elabel命令。在分别使用值标签名称logs2和time2之后，我们指定整数—文本的对应、对整数值和标签进行分组(请参见第2.2节)。在第一对括号内，我们指定一个整数值的数字列表。在第二对括号中，我们指定整数值—文本的对应关系列表。

我们使用包含#字符的表达式来指定文本，#字符充当第一对括号里值的占位符。因为表达式必须输入为字符串，所以我们使用Stata的strofreal()函数；对于第二个问题，我们还使用了cond()函数(有关这两个函数的更多信息，请参见[FN] String functions 和[FN]Programming functions )。

将第一个elabel命令与相应的lablog命令进行比较，我们可以清楚地看到后者的语法更方便。然而，elabel的语法对于实际执行的命令可能更明确，因此仅通过查看代码可能更容易理解；但是Stata用户已经知道了大多数语法。而且，一旦我们理解了代码，它就很容易应用于相关的问题。

转到第二个例子，我们使用相同的命令elabel define，并像前面一样指定整数—文本的对应关系。我们只需要修改表达式和函数，就可以按照所需的方式转换整数值。然而，找出合适的表达方式比较困难；若给出一个合适的表达式，elabel define基本上可以简化为一个方便的foreach语句(参见[P] foreach)。可以说，elabel在系统地修改现有的值标签方面更为方便。

## 4.2  系统地修改价值标签

下一个例子中，假如我们用以下值标签来表示吸烟的频率：

```
. elabel define smoke 
>    1 "never" 
>    2 "once a week or less" 
>    3 "every few days" 
>    4 "most days"
>    5 "every day"
```

进一步假设，我们希望更改整数—文本的对应关系，我们将从不定义为0，一周一次或更少定义为1，依此类推。如果烟是一个变量，我们可以简单地编码

```
. replace smoke = smoke-1
```

来更改整数值。
使用elabel，我们可以对值标签做类似的命令。

```
. elabel define smoke (= #-1) (= @), replace
```

我们来检查一下代码。Stata改变值标签的命令是label define；因为我们希望更改值标签，所以我们使用相应的elabel命令。在smoke value-label名称之后，我们指定整数—文本的对应关系，对整数值和标签进行分组(参见2.2节)。

在第一对括号内，我们给整数值指定一个表达式；在这里从每个整数值中减去1。我们还给第二个括号内的文本指定了一个表达式；这里只是复制现有的文本。这样就有了5个新的整数到文本的对应关系：0是“从不”  1是“一周一次或更少”…4是“每天”。因为我们希望替换现有的值标签，所以我们指定了替换选项。验证一下结果:

```
. Elabel list smoke
 smoke: 
0 never 
1 once a week or less 
2 every few days 
3 most days 
4 every day
```

## 4.3  继续系统地修改值标签

正如在介绍中提到的，Stata提供了方便的命令来操作变量，比如recode(参见[D] recode)。继续进行价值标签smoke的例子。如果smoke是一个变量，并且我们想要反转它的编码，我们可以输入

```
. recode smoke
>    (0 = 4 "never") 
>     (1 = 3 "once a week or less") 
>     (2 = 2 "every few days") 
>     (3 = 1 "most days") 
>     (4 = 0 "every day"), generate(smoke2)
```

同时定义一个适当的值标签。虽然recode允许我们定义一个新的值标签，但当所有标签已经存在时，重新输入它们可能会很不方便。
使用elabel，我们可以指定与recode命令相似的转换规则。下面是elabel各自的recode子命令的语句。

```
. elabel recode smoke (0/4 = 4/0), define(smoke2) dryrun
smoke:
0 never 
1 once a week or less 
2 every few days 
3 most days 
4 every day
smoke2:
 0 every day 
 1 most days
 2 every few days
 3 once a week or less 
 4 never
```

与用于变量的recode命令相比，elabel的recode子命令非常简短，因为elabel允许等号两边都有一个数字列表。此外，我们不需要重新输入任何标签；我们只需要改变整数值。define()选项要求定义一个新的值标签smoke2，而不是替换值标签smoke。

但是，因为我们也指定了dryrun选项，所以elabel没有定义smoke2；相反，它列出了原始的和转换后的值标签，因此我们可以先验证结果。如果满意，可以删除dryrun选项，并定义值标签smoke2。还有一个更方便的特性:elabel recode以变量recode命令可以接受的格式返回r()中的转换规则。

```
. return list
 macros:
r(rules) : "(0=4) (1=3) (2=2) (3=1) (4=0)"
```

现在，我们可以将这些转换规则传递给Stata的recode命令，并相应地修改任意数量的变量。

## 4.4改变value-label名字

Stata的label命令不能轻易地更改value-label名称。原则上，更改值标签名称需要三个步骤：首先，使用新名称复制旧的值标签；第二，将这个新的值标签附加到所有以前附加了旧值标签的变量上；第三，从内存中删除旧值标签。

Weesie (2005b)讨论了重命名值标签的问题，并引入了labelrename命令来做到这一点。labelrename类似于Stata以前的变量重命名命令(参见[D] rename)，每次只更改一个值标签的名称。elabel借鉴了Weesie的工作，但是类似于Stata的新的重命名命令(参见[D] rename group)，它可以更改值标签组的名称。

我将用nlsw88演示elabel的重命名子命令，nlsw88是Stata系统自带的数据。数据集中的所有值标签名称都以lbl结尾；这里将举两个例子。

```
. sysuse nlsw88, clear
 (NLSW, 1988 extract) .
 describe married collgrad  
storage display value
 variable name    type      format      label       variable label
------------------------------------------------------------------------------------
Married         byte      %8.0g        marlbl      married 
Collgrad         byte      %16.0g       gradlbl      college graduate
```

假设现在我们想要更改所有的value-label名称，而不是以VL结尾。下面是我们要如何通过elabel rename来做到这一点。

```
. elabel rename (*lbl) (*VL) .
 describe married collgrad 
storage display value
 variable name    type    format    label    variable label
------------------------------------------------------------------------------------
married          byte    %8.0g    marVL    married 
collgrad          byte   %16.0g    gradVL    college graduate
```

## 4.5最后一个更改变量标签的例子

Stata的label命令还可以管理变量标签。我们将接着讲在第4.4节没讲完的部分。假设我们想在nlsw88中更改变量collgrad的标签。这样每个单词都以大写字母开头。使用elabel，我们可以像更改值标签一样更改当前变量标签。

```
. elabel variable (collgrad) (= strproper(@)) . 
describe collgrad 
storage display value
 variable name   type   format    label    variable label
------------------------------------------------------------------------------------
collgrad        byte    %16.0g   gradVL   College Graduate
```

在第一对括号中，我们指定要更改其标签的变量collgrad。在第二对括号中，我们指定了一个表达式，该表达式将strproper()函数(参见[U] 13 Functions and expressions)与@字符组合在一起，@字符充当当前变量标签的占位符。

# 5 给elabel添加命令

在上面的例子中，我们已经看到了elabel如何增强Stata的label命令；elabel还包含用于帮助实现新命令的编程命令(和Mata函数)。

## 5.1问题：组合值标签

作为一个新命令的例子，我们将使用labvalcombine，它是labutil安装包(Cox 2000)的一部分。labvalcombine命令“将两个或多个值标签集组合为一个”。以下是来自帮助文件的一个例子:

```
. label define lbl1 1 "one" 2 "two" 3 "three" .
 label define lbl2 2 "deux" 3 "three" 4 "four" .
 labvalcombine lbl1 lbl2, lblname(both)
 both: 
            1 one 
            2 deux
            3 three
            4 four
```

没有相应的elabel命令执行labvalcombine所执行的操作，因为我们了解labvalcombine，所以不需要这样的命令。但是，假设没有labvalcombine命令，并且我们希望向elabel添加这样的命令。在本节的其余部分，我将演示如何做到这一点。

## 5.2如何组合值标签

我们的目标是实现一个新命令elabel combine，它基本上完成了labvalcombine的功能。首先，我们需要找出如何组合值标签集。使用elabel的copy子命令使这变得相当简单。假设value label还不存在，我们将需要两行代码。

```
. elabel copy lbl1 both . 
. elabel copy lbl2 both, modify
```

第一行代码将值标签lbl1的内容复制到新的值标签。第二行将值标签lbl2的内容复制到现在已经存在的值标签both中，修改它们的内容。虽然我们可以在第一行中使用Stata的label copy命令，但是不能在第二行中使用它，因为Stata的label copy命令不允许修改选项。无论如何，我们现在知道了如何组合两组值标签。对于两个以上的标签，我们将简单地循环其余的值标签。

## 5.3实现elabel结合

在找出组合值标签集的代码之后，我们就可以实现新的命令了。假设我们想要这样的语法

```
elabel combine elblnamelist, {define(newlblname)|replace} 
```

该语法指示调用者必须指定elblnamelist和下面讨论的两个选项之一。

为了让elabel调用我们的新子命令combine，我们需要编写一个程序并将其命名为 elabel cmd combine。因为我们希望允许elblnamelist，所以我们将使用elabel的parse命令，它类似于Stata语法命令的一个基本版本(参见[P]syntax])，我将在下面简要解释它。我们还将允许两种选择。我们的项目是这样开始的。

```
*! version 1.0.0 17jun2019 
program elabel_cmd_combine
 version 15
// parse syntax elabel parse elblnamelist [ , DEfine(name) REPLACE ] : `0´
...
End
```

我们将重点放在elabel parse命令上，首先在冒号之前列出允许的语法。这里我们允许一个elblnamelist和两个选项。在冒号之后，我们清楚地将本地宏“0”(参见[P]macro)的内容传递给elabel解析。提醒您，本地宏“0”包含调用者输入的任何内容(参见[U]  18.4 Program arguments)。

elabel解析结束后，本地宏“lblnamelist”将包含调用者传递给elabel combine的值标签名称列表。此外，如果调用者指定了define()选项，则本地宏“define”将包含指定的名称。如果调用者指定了替换选项，本地宏“替换”将包含替换一词。

让我们就elabel combine中选项的以下规则达成一致：如果调用者没有为define()中的组合值指定值标签名称，我们将使用他们在“lblnamelist”中提到的第一个值标签名称。如果已经定义了组合值的value-label名称，则调用者必须指定replace。下面是我们如何实现它。

```
*! version 1.0.0 17jun2019
 program elabel_cmd_combine
 version 15

// parse syntax 
elabel parse elblnamelist [ , DEfine(name) REPLACE ] : `0´

// no name specified; use the first one mentioned
 if ("`define´" == "") gettoken define : lblnamelist

// either new name or explicit permission to overwrite
 if ("`replace´" == "") elabel confirm new lblname `define´
...
end
```

你在stata文件中识别不到的唯一命令是elabel confirm，它跟stata中的confirm命令(参见[P] conﬁrm)是一样的，我们已经完成了初始解析。让我们结合值标签来完成这些代码。

```
*! version 1.0.0 17jun2019 
program elabel_cmd_combine 
version 15
...

// start with a copy of the first value label 
gettoken firstlbl lblnamelist : lblnamelist 
elabel copy `firstlbl´ `define´, replace

// now combine the remaining value labels 
foreach lbl of local lblnamelist {
 elabel copy `lbl´ `define´, modify 
}
End
```

在最后一步中，我们需要将程序存储在Stata可以找到它的地方。如果我们打算经常使用我们的命令，我们将把代码存储为 elabel_cmd_combine.ado在 ado-ﬁle路径的某个地方(参见[P] sysdir)。在这里的例子，只要将代码放入do-file中并执行它，在内存中定义程序就足够了。

```
. do elabel_cmd_combine.do 
(output omitted)
```

我们在本节开始处复制labvalcombine的例子，以验证elabel combine是否按预期工作。在此之前，删除所有的值标签，但lbl1和lbl2除外，它们需要合并。我们注意到elabel有一个keep子命令，它补充了label drop命令。

```
. elabel keep lbl1 lbl2
```

这里是我们如何获得labvalcombine与elabel combine的结果。

```
. elabel combine lbl1 lbl2, define(both) . 
.elabel list both
both: 
                     1 one 
                     2 deux 
                     3 three 
                     4 four
```

# 6 结论

数据管理中的一个常见任务是定义和操作变量和值标签。elabel通过增强Stata的label命令来简化这项任务。该命令支持值标签名称中的通配符，并通过变量名间接引用值标签。此外，elabel可以从值标签中选择整数—文本的对应关系，并将Stata的任何表达式和函数应用于变量和值标签。因此，elabel有助于系统地更改变量和值标签。打算编写自己的命令来管理变量和值标签的Stata程序员可以很容易地在他们的代码中包含elabel的特性。

# 7 参考文献

[1] [Blumenberg, J. N. 2012. valtovar: Stata module to rename value labels to match variable names. Statistical Software Components S457443, Department of Economics, Boston College. ](https://ideas.repec.org/c/boc/bocode/s457443.html.)

[2]  [2016. trimlabs: Stata module to trim variable labels. Statistical Software Components S458148, Department of Economics, Boston College. ](https://ideas.repec.org/c/boc/bocode/s458148.html.)

[3] [Cox, N. J. 2000. labutil: Stata modules for managing value and variable labels. Statistical Software Components S402501, Department of Economics, Boston College. ](https://ideas.repec.org/c/boc/bocode/s402501.html.)

[4] [Jann, B. 2007. labelsof: Stata module to obtain a list of labeled values. Statistical Software Components S456834, Department of Economics, Boston College.](https://ideas.repec.org/c/boc/bocode/s456834.html.)

[5] [Joly, P. 2002. varlab: Stata module to save and load variable labels. Statistical Software Components S425001, Department of Economics, Boston College.](https://ideas.repec.org/c/boc/bocode/s425001.html.)

[6] [Klein, D. 2011. labutil2: Stata module to manage value and variable labels. Statistical Software Components S457320, Department of Economics, Boston College.](https://ideas.repec.org/c/boc/bocode/s457320.html.)

[7] [Newson, R. 2007. lablist: Stata module to list value labels (if present) for one or more variables. Statistical Software Components S456855, Department of Economics, Boston College. ](https://ideas.repec.org/c/boc/bocode/s456855.html.)

[8] [2009. varlabdef: Stata module to deﬁne a value label with values corresponding to variables. Statistical Software Components S457026, Department of Economics, Boston College. ](https://ideas.repec.org/c/boc/bocode/s457026.html.)

[9] [2018. vallabdef: Stata module to deﬁne value labels from label name, value and label variables. Statistical Software Components S458451, Department of Economics, Boston College. ](https://ideas.repec.org/c/boc/bocode/s458451.html.)

[10] [Nichols, A. 2011. labmatch: Stata module to ﬁnd observations by label values. Statistical Software Components S457263, Department of Economics, Boston College.](https://ideas.repec.org/c/boc/bocode/s457263.html.)

[11] [Weesie, J. 2005a. Multilingual datasets. Stata Journal 5: 162–187.]

[12] [2005b. Value label utilities: labeldup and labelrename. Stata Journal 5: 154–161.]
