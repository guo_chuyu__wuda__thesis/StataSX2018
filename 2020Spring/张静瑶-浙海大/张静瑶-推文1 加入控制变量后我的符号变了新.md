> 张静瑶(浙江海洋大学)      
> E-mail: 1132358871@qq.com

---

# 加入控制变量后我的变量符号变了

---
# 1 背景

## 1.1 问题的引入
>两小儿辩车
> - 王小儿：我发现车越长越贵，比如，奥迪 A6L 就比 A4L 贵多；
> - 李小儿：那也未必，奥迪 R8 只有 4 米 4，但可以买两辆 A6L了。 还有，我二爸开的那个公交车，12 米，才 30 万！ 

在大家做实证分析的最初阶段，经常会有一个困扰：原本主效应很符合预期目标，但加入了一个或几个控制变量后，主效应要么符号变了，要么不显著了。可是，关键控制变量不加入的话，审稿人必然会提出质疑。这是怎么回事了？
要回答这个问题，让我们先从条件期望说起。

## 1.2 什么是条件期望

### 1.2.1 举例说明

春节临近，如何应对亲戚的“问候杀”，是一个亟待解决的现实问题。试想，举国欢庆的日子，大家齐坐一堂，面对读硕读博归家的你，七大姑问到：“大闺女，老大不小了，该干点正事谈个对象什么的呢，读那么多书干嘛呀”，你咽了咽口水“读书好啊，以后可以多挣钱孝敬您老人家”，话音未落，八大姑道：“哎呀瞎说，你看隔壁二狗子，高中没读完去做生意，现在赚老多钱了”….气氛顿时尴尬了，除了咽口水外，该怎么“杠”回去…

那就要了解条件期望的概念了：条件期望函数记为 $E(Y_i|X_i)$，是关于 $X_i$ 的函数，考虑到 $X_i$ 是随机的，所以条件期望函数也是随机的。比如给定 $X$ 为受教育水平的一个定值，如 $X=12$，那么 $E(Y_i|X_i=12)$ 就是表示所有读 12 年书的个体，其收入水平的期望值。

### 1.2.2 用图说明

![图片1](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E5%9B%BE%E7%89%872_20191213104808.png)

图中，横轴表示受教育水平，纵轴则为收入，在每一个给定的受教育水平下（如：受教育年限 12 年），收入服从一个近似正态的随机分布。可以看到，由于存在着无法忽视的个体差异，使得某些低教育者的收入要高于某些高教育者的收入，但通常而言，教育水平高的人赚的更多。

看到这里，学习过初级计量经济学的同学肯定会想到，对，是“个人能力”的影响，是“个人能力”让二狗子和大闺女产生了收入的差异。确实如此，那么“个人能力”便是一个需要控制的重要变量。

# 2 Stata 实操

## 2.1 回到原例

### 2.1.1 加入控制变量和不加控制变量的回归分析结果做对比

让我们回到“小儿辩车”的引子。调入官方自带的汽车数据，研究汽车长度 **length** 对汽车价格 **price** 的影响。在多元回归中，加入的控制变量分别为里程数 **mpg** 和汽车重量 **weight**

```Stata
. sysuse "auto.dta", clear
. cap eststo clear
. eststo: reg price length
. eststo: reg price length mpg weight
. esttab, nogap 
. reganat price length mpg weight, dis(length) ///
.              biscat biline scheme(s2color)  
```

### 2.1.2 回归结果

```Stata
--------------------------------------------
                      (1)             (2)   
                    price           price   
--------------------------------------------
length              57.20***       -104.9** 
                   (4.06)         (-2.64)   
mpg                                -86.79   
                                  (-1.03)   
weight                              4.365***
                                   (3.74)   
_cons             -4584.9*        14542.4** 
                  (-1.72)          (2.47)   
--------------------------------------------
N                      74              74   
--------------------------------------------
t statistics in parentheses
* p<0.1, ** p<0.05, *** p<0.01

```
对比 **length** 的系数可见，在一元回归时该系数显著为正（57.2），而加入控制变量后，系数为负（-104.9）并在 10% 的水平下显著。

### 2.1.3 利用 `reganat` 命令对多元回归模型进行图解检验。

![图片2](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E5%9B%BE%E7%89%871_20191213104808.png)

虚线拟合线表示了不加入控制变量时 **length** 的影响，而实线是加入控制变量之后的。由之初的右上倾斜变为右下倾斜，这是一个本质性地改变。

### 2.2.1 原因分析

事实上，在多元回归 $Y_i=\alpha+\beta_1x_i+\beta_2x_2+e_i$ 中，系数$\beta_1$  更准确的应该被称为偏回归系数，表示在剔除掉 $x_2$ 的“贡献”后，$x_1$ 对 $y$ 的影响。若 $x_1$ 与 $x_2$ 并不独立，那么错误的使用一元回归 $Y_i=\alpha+\beta_1x_i+e_i$，系数 $\beta_1$ 中就包含了 $x_2$ 的一部分“贡献”。

### 2.2.2 通过实例演示

先进行正常的多元回归，加入主效应变量 **length** 和控制变量 **mpg**  **weight**

> - sysuse auto.dta, clear
> - reg price length mpg weight
> - est store m1

接着，我们利用解构回归（regression anatomy）来“解读”正常的多元回归：1.先用 **length** 对控制变量 **mpg** **weight** 进行回归，然后可以得到一个残差项。2. 再用被解释变量对上一步的残差项进行回归。

```Stata
. reg length mpg weight
. predict e, res
. reg price e
. est store m2
. esttab m1 m2 , nogap.  
```
### 2.2.3 估计结果

```Stata
--------------------------------------------
                      (1)             (2)   
                    price           price   
--------------------------------------------
length         -104.9***       
                 (-2.64)        
mpg             -86.79                 
                  (-1.03)   
weight           4.365***
                  (3.74)   
e                                        -104.9*
                                        (-2.22)  
_cons           14542.4*          6165.3***
                    (2.47)            (18.46)   
--------------------------------------------
N                      74              74   
--------------------------------------------
t statistics in parentheses
* p<0.1, ** p<0.05, *** p<0.01

```
可以看到，最后得到的主效应估计结果一致，均为 -104.9 。在用 **length**  对控制变量  **mpg**  **weight**  回归后得到的残差项，表示  **length**  剔除了其他解释变量对自己的影响的“结果”，将其再与被解释变量的回归就是一个“净”的效应。

# 3 结语

可见，加入控制变量后，我们关心的估计系数是否会产生变化，取决于与控制变量之间的独立性。

我们列出将会出现的四种情形

- 与控制变量之间完全独立，则加入控制变量对估计系数无影响（情形 1）

![图片3](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E5%9B%BE%E7%89%873_20191213104808.png)

- 与控制变量之间高度相关，则加入控制变量与的估计系数都不显著（情形 2）

![图片4](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E5%9B%BE%E7%89%874_20191213104808.png)

- 与控制变量之间相关，且完全通过控制变量的“途径”来影响被解释变量，则估计系数不显著（情形 3）

![图片5](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E5%9B%BE%E7%89%875_20191213104808.png)

- 与控制变量之间相关，则加入控制变量后，主要解释变量的估计系数会出现大小和符号变化。具体变化取决于与控制变量间的正负相关性。（情形4）

![图片6](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E5%9B%BE%E7%89%876_20191213104808.png)

