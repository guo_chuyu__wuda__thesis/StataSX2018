
&emsp;
> 编译： Benjamin Ferland.  2018.  "The Alternative Specification of Interaction Models With a Discrete Modifying Variable".  The Political Methodologist
> 
> 作者： 曾颖娴 (中山大学)
>     
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)

&emsp;
  
**Source:** [The Alternative Specification of Interaction Models With a Discrete Modifying Variable](https://thepoliticalmethodologist.com/2018/02/12/the-alternative-specification-of-interaction-models-with-a-discrete-modifying-variable/)
&emsp;
##  离散型调节变量——该如何设定模型？

自从 Brambor，Clark 和 Golder（2006）（以下简称 BCG ）在《Political Analysis》中发表文章以来，我们对交互模型有了更深的理解，现在大多数实证学者也已经整合了正确执行和解释交互模型的工具。其中，BCG 的主要建议之一是在进行模型设定的时候，将交互作用的所有构成项都包含在模型中。

然而，BCG 也在其论文的公式 7 的文本中指出，当一个调节变量是离散型变量时，进行模型设定时可以从交互模型中排除某些构成项，并且这个模型设定的替代方法和原有的设定方法在数学上是等价的。最近的一次审查经验也使我意识到，这种特殊的模型设定方式并没有得到学术界的广泛认可。因此，向学术界简要说明这种特殊的设定方法，可能有利于宣传这种模型设定的替代方法。在下一部分，我将展示关于模型设定的BCG 标准方法和替代方法的等价性。然后，我提供了在实际情况下应用这两种方法的简短示例—— Adams 等人（2006）的研究《小众政党与主流政党有本质区别吗？》,通过示例的结果分析可以发现，进行模型设定时，无论是使用 BCG提出的标准方法还是替代方法，都可以得到相同的结果。

总的来说，虽然两种模型设定的方法的效果是相同的，但是在解释回归结果方面，这两种模型设定的方法各有一些优势。一方面，标准方法的优点是可以在回归结果中直接显示变量 $X$ 对变量 $Y$ 的边际效应的差异在调节变量 $Z$ 的类别之间是否具有统计显著性。另一方面，替代方法的主要优点是可以在回归结果中直接显示变量 $X$ 在调节变量 $Z$ 的每个类别下的边际效应。因此，研究人员可以根据他们想要展示和强调的结果在两个等价的模型设定的方法之间进行选择。

### 1. 模型设定的标准方法和替代方法之间的等价性

当进行模型设定时，当其中一个调节变量是离散型变量时，使用 BCG 标准方法和替代方法的效果是等价的，为了展示这种等价性，我以一个因变量 $Y$ 为例，它受到连续变量 $X$ 和虚拟变量 $D$ 交互作用的影响。根据BCG文中提到的标准方法进行交互模型设定时，必须将变量 $X$ 和变量 $D$ 相乘，并在回归模型中分别包含$X$  、 $D$ 和交乘项$XD$。具体来说，常见的模型设定的标准方法如下式：

$$ Y = b_0 + b_1D + b_2X + b_3XD + ϵ  \quad (1)$$

其中，$X$ 是连续变量，$D$是虚拟变量（0,1）。当 $D$ =0 时 $X$ 的边际效应等于 $b_2$ ，而当 $D$ =1 时 $X$ 的边际效应则等于 $b_2$ + $b_3$。

 BCG （参见等式 7 ）和 Wright（1976）提到的模型设定的替代方法是将虚拟变量 $D$ 视为两个虚拟变量：原始变量 $D$ (等于 0 和 1)以及 $D$ 的反面 $D_0$（当 $D$ =0 时为 1，当 $D$ =1 时为 0）。例如，如果 $D$ 是一个虚拟变量，其中 1 代表民主国家，0 代表专制国家，那么 $D_0$ 则是一个相反的虚拟变量，其中 1 代表专制国家， 0 代表民主国家。因此，$D$ + $D_0$ =1 并且 $D_0$ = 1- $D$ 。

模型设定的替代方法如下：将 $X$  分别与 $D$ 和 $D_0$ 相乘得到新的交乘项 $XD$ 和 $XD_0$，其次将这两个交乘项和虚拟变量 $D$ 或 $D_0$  中的其中一个虚拟变量纳入模型中。由于 $D$ 和 $D_0$ 这两个变量是完全共线性的，因此在模型中只包含 $D$ 或 $D_0$ 的其中一个，而且模型中也不可能包含 $X$  。

因此，替代方法如下式：

$$ Y = a_0 + a_1D + a_2XD + a_3XD_0 + ϵ \quad (2)$$

等式(2) 式可以改写为：

$$ Y = a_0 + a_1D + a_2XD + a_3X(1−D) + ϵ   \quad (3)  $$

由等式(3)式可以发现，进行模型设定时也可以不创建 $D_0$ ，而只需将$X$  乘以（1-$D$）。在等式 (2) 式和等式 (3) 式中，当 $D$ =0 时（即当 $D_0$ =1 时） $X$ 的边际效应等于 $a_3$ ，  而当 $D$ =1 时 $X$ 的边际效应由 $a_2$ 给出。

这种模型设定的替代方法的主要优点是可以从回归结果中直接得到离散型调节变量 $D$ 的每个类别对 $X$  的边际效应及其相关的标准差。而使用常见的模型设定标准方法则无法直接得到这些结果，标准方法仅能直接提供这些结果中的一个（即 $b_2$，当 $D$ =0 时 $X$  的边际效应）。因此，使用标准方法进行模型设定时，当 $D$ =1 时，我们需要将 $b_2$ 和 $b_3$  相加以获得 $X$ 的边际效应。在 Stata 中，可以使用命令 `lincom`（`lincom _b [coef] + _b [coef]`）得到这个结果。

然而，替代方法有一个缺点，其回归结果没有表明当 $D$ =0 时和 $D$ =1 时 $X$  的边际效应之间的差异是否具有统计显著性。而这则是模型设定的标准方法的优点，它可以在回归结果中提供此信息。如果在等式(1)中，系数 $b_3$  具有统计显著性，则表明当 $D$ =1 时， $X$  的边际效应在统计上与 $D$ =0 时 $X$  的边际效应是不同的。如果要用替代方法设定的模型的结果来回答这个问题，我们必须检验 $a_2$  和 $a_3$  的等价性。在 Stata 中，使用命令 `test`（`test _b [coef] = _b [coef]`）或`lincom`（`lincom _b [coef] - _b [coef]`）可以达到这个检验目的。

无论离散型变量是名义变量还是有序变量，这种模型设定的替代方法都可以推广到具有多个类别的离散型变量，方法步骤是一样的。首先，我们需要为离散型调节变量的每个类别创建一个虚拟变量。然后，我们将 $X$ 与每一个虚拟变量中的每一个相乘得到相应的交互项，最后，在模型中包含虚拟变量 $D$ 或 $D_0$、得到的所有交互项项和其他所有构成项（除了 $X$ ）。通过这种方法进行模型设定，研究人员可以直接估计 $X$  在离散型调节变量的不同值上的实质影响的大小。

### 2. 文献重现：小众政党与主流政党有本质区别吗?

在本节中，我通过重现Adams 等人 2006 年发表在《American Journal of Political Science》上的文章《小众政党与主流政党有本质区别吗?》的实证结果，比较了交互模型设定的标准方法和替代方法的结果。

这篇文章主要研究了两个问题。首先，作者研究了主流政党在调整政策方案方面是否比小众政党更能响应公共舆论的转移。其次，基于这一预测，他们研究了在调节其政策立场时，小众政党是否会比主流政党在选举中受到更多惩罚。这里，我只重现了他们与第一个问题相关的模型。

Adams 等人(2006) 对七个发达民主国家(意大利、英国、希腊、卢森堡、丹麦、荷兰和西班牙) 在 1976-1998 年间的数据进行了假设检验，并且使用 Comparative Manifesto Project ( CMP) 的数据衡量政党在左右意识形态层面上的政策立场。欧盟民意调查 (Eurobarometer) 被用于根据相应的左右意识形态维度来定位受访者。舆论是所有受访者自我定义的平均水平。最后，作者将共产主义、绿党和民族主义政党编码为一个虚拟变量——小众政党。

在表 1 中，我研究了政党对舆论的反应，并介绍了模型设定的两种方法的结果。Adams 等人(2006)采用常见的标准方法进行模型设定，将变量舆论转移（ $public opinion shift$）与虚拟变量小众政党（$niche party$）进行交互得到交乘项。因变量是一方政党左右立场的变化。因此，Adams 等人(2006)提出了一个动态模型，在该模型中，他们评估舆论的变化是否会影响两次选举之间政党立场的变化。模型里包括了对国家的固定影响和一些控制变量(详细解释见原文)。由此，得到的模型设定如下:

$$∆party position = b_0 + b_1∆public opinion + b_2niche party + b_3(∆public opinionXniche party) + controls$$

在表 1 的第(1)列中，我展示了与 Adams  等人(2006)文章中表 1 中公布的结果相同的结果。其中，第(1)列的结果支持了作者的观点，即小众政党对舆论转移的反应不如主流政党。舆论转移（$public opinion shift$）的系数(0.97)在统计上显著为正，表明当舆论向左转变时，主流政党(小众政党 $niche party$ =0)会相应向左调整政策立场。舆论转移与小众政党的交互项（$public opinion shift X niche party$）的系数（-1.52）表明，面对舆论转移，小众政党调整其政策立场的反应低于主流政党1.52个百分点，并且该差异具有统计学意义(p<0.01)。

在表 1 的第（2）列中，我展示了使用替代方法进行模型设定的结果。生成主流政党（$mainstream party$）的变量后分别将小众政党($niche party$)、主流政党（$mainstream party$）与舆论转移（$public opinion shift$）进行交互得到交乘项，纳入模型后，模型展示如下：

 $$∆party position=b_0+b_1niche party+b_2(∆public opinionXniche party)+b_3(∆public opinionXmainstream party)+controls$$

这里，主流政党（$mainstream party$）等于(1 -小众政党($niche party$))。

我们可以发现，其实第（1）和（2）列中的结果在数学上是等价的，两列中的控制变量的系数完全相同。然而，两个模型的结果在对交互效应的解释效果上却是存在差异的。
一方面，在第（2）列中，主流政党-舆论转移（$public opinion shift – mainstream party$）系数为 0.97，等于第（1）列中舆论转移（$public opinion shift$）的系数。这是因为第（2）列中主流政党-舆论转移（$public opinion shift – mainstream party$）的系数表明舆论转移对主流政党立场的影响，正如第（1）栏中舆论转移（$public opinion shift$）的系数代表的含义是一样的。

另一方面，第（2）列中小众政党-舆论转移系数（$public opinion shift – niche party$）等于-0.55，在5%水平上具有统计显著性。这表明当舆论向左转变时，小众政党向右调整其政策立场。当使用标准方法进行模型设定时，则未能直接显示这个结果，正如第（1）列所示。当使用标准方法进行模型设定时，要想得到小众政党与舆论转移的交互作用的系数，必须
将舆论转移（$public opinion shift$）的系数和舆论转移与小众政党交乘项（$public opinion shift X niche party$）系数相加以获得边际效应。如表中结果可知，第（1）列中舆论转移（$public opinion shift$）的系数和舆论转移与小众政党交乘项（$public opinion shift X niche party$）系数之和，即0.97 +（-1.52）= -0.55，这与第（2）列中小众政党-舆论转移（$public opinion shift – niche party$）的系数相等。

在第（2）栏中，Wald 检验表明，小众政党-舆论转移（$public opinion shift – niche party$）和主流政党-舆论转移（$public opinion shift – mainstream party$）的边际效应的差异在1%水平上具有统计显著性，正如第（1）列中舆论转移与小众政党交乘项（$public opinion shift X niche party$）的系数显示的那样。

![image](https://upload-images.jianshu.io/upload_images/20314270-b7b5c6d8a77acf8a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

总体而言，当交互模型中的调节变量是离散型变量时，研究人员可以使用两种不同的方法进行模型设定：BCG 标准方法(包含交互的所有构成项)和替代方法(不明确包含交互的所有构成项)。这两种方法在解释交互效应方面都有其优势。其中，替代方法的优点是对于离散型调节变量 $Z$ 的每个类别，可以直接显示自变量 $X$ 对 $Y$ 的边际效应。另一方面， BCG 标准方法的优点是直接呈现 $X$ 对 $Y$ 在离散型调节变量Z每个类别下的边际效应的差异是否在统计上显著)。但是，研究人员在两种模型中均需要进行额外的检验，使用替代方法时需要验证边际效应的差异是否在统计上显著，使用常见的标准方法时则需要分别计算离散型调节变量每个类别下的实际边际效应。

### 3. Stata 范例

使用 数据 **c10interaction.dta** (http://www.stata-press.com/data/agis4/c10interaction.dta)， 研究收入 **inc** 与性别 **male** 和受教育年限 **educ** 之间的关系，其中，关于性别 **male** 的衡量，当为女性时，**male** =1，当为男性时，**male** =0。

#### 模型设定的标准方法：

```Stata
use http://www.stata-press.com/data/agis4/c10interaction.dta
egen mis = rowmiss(_all)
drop if mis
gen educXmale= educ*male   
reg inc male educ educXmale


      Source |       SS           df       MS      Number of obs   =       120
-------------+----------------------------------   F(3, 116)       =     34.89
       Model |  122604.719         3  40868.2397   Prob > F        =    0.0000
    Residual |  135875.281       116  1171.33863   R-squared       =    0.4743
-------------+----------------------------------   Adj R-squared   =    0.4607
       Total |      258480       119  2172.10084   Root MSE        =    34.225

------------------------------------------------------------------------------
         inc |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
        male |  -91.88539   26.27242    -3.50   0.001    -143.9212   -39.84954
        educ |   3.602369   1.388076     2.60   0.011     .8531092    6.351628
   educXmale |   8.196446   1.885263     4.35   0.000     4.462445    11.93045
       _cons |   16.84834   19.07279     0.88   0.379    -20.92773     54.6244
------------------------------------------------------------------------------
regfit
inc = 16.848 - 91.885*male + 3.602*educ + 8.196*educXmale
      (0.88)    (-3.50)      (2.60)       (4.35)
N  = 120, R2 = 0.4743, adj-R2 = 0.4607

lincom educ + educXmale

 ( 1)  educ + educXmale = 0

------------------------------------------------------------------------------
         inc |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
         (1) |   11.79881    1.27572     9.25   0.000      9.27209    14.32554
------------------------------------------------------------------------------
```
由上述标准方法的回归结果可知，当是男性( **male** =0)时，受教育年限对收入的边际效应为 3.602（即为 **educ** 的系数）；当是女性( **male** =1)时，受教育年限对收入的边际效应为 11.799（即为 **educ** 的系数 3.602 加上 **educXmale** 的系数8.196， 3.602+8.196=11.799）,并且边际效应具有显著性。



#### 模型设定的替代方法：

```Stata
use http://www.stata-press.com/data/agis4/c10interaction.dta
egen mis = rowmiss(_all)
drop if mis
gen Male = 1-male
gen educXmale= educ*male      
gen educXMale= educ*Male  
reg inc male educXmale educXMale

      Source |       SS           df       MS      Number of obs   =       120
-------------+----------------------------------   F(3, 116)       =     34.89
       Model |  122604.719         3  40868.2397   Prob > F        =    0.0000
    Residual |  135875.281       116  1171.33863   R-squared       =    0.4743
-------------+----------------------------------   Adj R-squared   =    0.4607
       Total |      258480       119  2172.10084   Root MSE        =    34.225

------------------------------------------------------------------------------
         inc |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
        male |  -91.88539   26.27242    -3.50   0.001    -143.9212   -39.84954
   educXmale |   11.79881    1.27572     9.25   0.000      9.27209    14.32554
   educXMale |   3.602369   1.388076     2.60   0.011     .8531092    6.351628
       _cons |   16.84834   19.07279     0.88   0.379    -20.92773     54.6244
------------------------------------------------------------------------------
regfit
inc = 16.848 - 91.885*male +11.799*educXmale + 3.602*educXMale
     （0.88）  （-3.50）        （9.25）          （2.60）
N  = 120, R2 =0.4743, adj-R2 = 0.4607

test educXmale=educXMale

 ( 1)  educXmale - educXMale = 0

       F(  1,   116) =   18.90
            Prob > F =    0.0000
```
由上述替代方法的回归结果可知，当是男性( **male** =0，**Male** =1)时，受教育年限对收入的边际效应为3.602（即为 **educXMale** 的系数）；当是女性( **male** =1，**Male** =0)时，受教育年限对收入的边际效应为 11.799（即为 **educXmale** 的系数 ），且两者边际效应的差异在1%水平上具有统计显著性。
 

由此范例可知，当交互模型中的调节变量是离散型变量时，模型设定的标准方法和替代方法均可用以检验分析其交互作用，其中，常见的标准设定方法可以直接体现 **educ** 对 **inc** 在 调节变量 **male** 下的边际效应是否具有显著性，（从范例的标准模型设定的结果表格中可以直接获取 **educXmale** 的系数8.196 在1%水平上具有统计显著性），而替代设定方法则可以直接显示对于调节变量的各个类别 **male**  和 **Male** ，**educ** 对 **inc** 的边际效应（即是范例中的 **educXmale** 和 **educXMale** 的系数）。


&emsp;

### 参考文献

- Adams, James, Michael Clark, Lawrence Ezrow and Garrett Glasgow. 2006. “Are Niche Parties Fundamentally Different from Mainstream Parties? The Causes and the Electoral Consequences of Western European Parties’ Policy Shifts, 1976-1998.” American Journal of Political Science 50(3):513–529.
**Source:** [Are Niche Parties Fundamentally Different from Mainstream Parties? The Causes and the Electoral Consequences of Western European Parties’ Policy Shifts, 1976-1998](https://onlinelibrary.wiley.xilesou.top/doi/abs/10.1111/j.1540-5907.2006.00199.x)

- Brambor, Thomas, William Roberts Clark and Matt Golder. 2006. “Understanding Interaction Models: Improving Empirical Analyses.” Political Analysis 14:63–82.
**Source:** [Understanding Interaction Models: Improving Empirical Analyses.” Political Analysis](https://www.researchgate.net/publication/228362270_Understanding_interaction_models_Improving_empirical_analyses)


- Wright, Gerald C. 1976. “Linear Models for Evaluating Conditional Relationships.” American Journal of Political Science 2:349–373.
**Source:** [Linear Models for Evaluating Conditional Relationships.](https://www.jstor.org/stable/2110649?seq=1#metadata_info_tab_contents)