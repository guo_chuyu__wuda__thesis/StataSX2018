> Cattaneo, M. D., R. Titiunik, G. Vazquezbare, 2020, Analysis of regression discontinuity designs with multiple cutoffs or multiple scores, working Paper, https://arxiv.org/abs/1912.07346, PDF: https://arxiv.org/pdf/1912.07346.pdf.

里面附带了 Stata 命令 `rdmulti` 的相关介绍。