> **任务：** 写一篇推文，介绍新命令 `speccheck` 的主要功能。

> Date: `2019/12/27 10:54`

## 引言

## speccheck 简介

`speccheck` generates a standardized graphical output from a regression specification. The command will individually regress depvar
against all possible indepvars combinations. The command will generate a figure with four subfigures. The effect curve is a histogram
of the regression coefficients estimated for the first indepvar. The t-curve is a histogram of the absolute value of the t-statistics
of the first indepvar.

## 相关命令

可以在 Stata 里执行 `findit specification` 等关键词，搜索一下是否有相关的其他命令，如果功能上可以互补，可以放到此处一并介绍。

- `SPECC` : module to create specification curves
