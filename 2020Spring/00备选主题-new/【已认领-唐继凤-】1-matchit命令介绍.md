> **任务：** 介绍 `matchit`, `reclink`，`reclink2` 命令

> 补充说明：通过查阅资料，发现或许可以不仅限于介绍 `matchit` 这一个命令，而是介绍一类命令，最终都指向同一个目标：**如何进行模糊匹配/模糊合并**。

- 推文中使用的数据最好用 `dataex` 直接产生，不需要太多观察值，能说明问题就行；参见 [Stata list - Matchit: new .ado to merge datasets using different string similarity methods (i.e. fuzzy match)](https://www.statalist.org/forums/forum/general-stata-discussion/general/1040086-matchit-new-ado-to-merge-datasets-using-different-string-similarity-methods-i-e-fuzzy-match)
- 作者提供的 PPT：[Raffo - 2016, Data consolidation and cleaning using fuzzy string
comparisons with -matchit- command, 2016 Swiss Stata Users Group meeting](https://www.stata.com/meeting/switzerland16/slides/raffo-switzerland16.pdf)

## 1. 问题背景
目前来看，`matchit` 和 `reclink` 以及其升级版本 `reclink2` 可能是最靠谱的两个命令。

## 2. matchit 命令

### 2.1 安装

### 2.2 范例
#### 2.2.1 Stata 范例 1
#### 2.2.2 Stata 范例 2
#### 2.2.3 Stata 范例 3


## 3. 附录
### 附1：matchit 的原理

- 有关模糊匹配/合并的一般原理性介绍：[Fuzzy Matching Algorithms To Help Data Scientists Match Similar Data](https://www.datasciencecentral.com/profiles/blogs/fuzzy-matching-algorithms-to-help-data-scientists-match-similar)
- Ansolabehere & Eitan (2017) 原理性介绍，参考文献列于文末

### 附2：推文中的完整 dofile 和数据下载链接


&emsp;

---



### 4. 参考资料



- `matchit` 作者提供的 PPT：[Raffo - 2016, Data consolidation and cleaning using fuzzy string comparisons with -matchit- command, 2016 Swiss Stata Users Group meeting](https://www.stata.com/meeting/switzerland16/slides/raffo-switzerland16.pdf)
- `help reclink` | [Stata: fuzzy matching with reclink - YouTube](https://www.youtube.com/playlist?list=PL_rBtSh1CLjh0RZtjq7FbauYUJNeC_ZGm)，可以进一步搜索一下该命令及相关应用；找到了一个应用  [Do CEOs Affect Employees' Political Choices? - Gies College of ...](https://business.illinois.edu/finance/wp-content/uploads/sites/46/2017/01/CEOandEmployeePoliticalChoices_v22_doublespaced.pdf)
- `help reclink2` Stata Journal || [Record Linkage using STATA: Pre-processing, Linking ... - Aaron Flaaen](http://www.aaronflaaen.com/uploads/3/1/2/4/31243277/wasi_flaaen_statarecordlinkageutilities_20140401.pdf)，Stata Journal 15-3，[View this article (PDF)](https://www.stata-journal.com/sjpdf.html?articlenum=dm0082)
-  [Fuzzy Matching - Andrew Johnston | Economics, University of California](https://sites.google.com/site/andrewjohnstoneconomics/my-advice/fuzzy-matching) || 作者写了一个命令 `merge3`
- PDF，写的很清楚 || [Merging two datasets on approximate values - aldo-benini.org](http://aldo-benini.org/Level2/HumanitData/Benini_NearMergesByGroup_120326.pdf)
- `help fuzzy` 作者提供的 adofile，但尚未封装发布 || [Stata Fuzzy match command](http://www.econometricsbysimulation.com/2012/06/stata-fuzzy-match-command.html)
- One option I've found useful is the user-written program [strgroup](https://ideas.repec.org/c/boc/bocode/s457151.html). You can install it from SSC: in Stata, type `ssc install strgroup`.
- 其他命令 (尚未测试，需要对比后决定去留)
  - `help nearmrg`  // match merging of two datasets on the values of the numeric variable nearvar.  只能基于数值型变量进行模糊合并

- Stephen Ansolabehere & Eitan D. Hersh (2017) ADGN: An Algorithm for Record Linkage Using Address, Date of Birth, Gender, and Name, Statistics and Public Policy, 4:1, 1-10, DOI: [10.1080/2330443X.2017.1389620](https://doi.org/10.1080/2330443X.2017.1389620)




