> **任务：** 翻译一篇文章。

> **Source：** [Using the lasso for inference in high-dimensional models](https://blog.stata.com/2019/09/09/using-the-lasso-for-inference-in-high-dimensional-models/)

### 加工方法
- 复制网页中的内容，贴入简书 Markdown 编辑器；
- 删除头部网页源码信息；
- 替换代码块的头尾表示语句
- 用 mathpix 识别文中的数学公式和包含数学公式的段落，贴回简书 Markdown 编辑器；
- 其他细节修订