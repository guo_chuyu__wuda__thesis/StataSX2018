> **任务：** 写一篇推文介绍 `ebalance` 命令的原理和用法。

### 参考资料

- Hainmueller J, Xu Y. ebalance: A Stata Package for Entropy Balancing[J]. Journal of Statistical Software, 2013, 54(7). [[PDF]](https://web.stanford.edu/~jhain/Paper/JSS2013.pdf)
  - 该文被引用了 [-208-次](https://scholar.google.com.hk/scholar?hl=zh-CN&newwindow=1&as_sdt=0,5&sciodt=0,5&cites=17004933150312485841&scipsc=)，若有时间，可以浏览一下，选一些有意思的在推文引言中建议介绍，以便读者了解这个命令的用途。
  - 文中的 Stata 实现 [[程序和数据文件]](http://yiqingxu.org/papers/english/2013_Hain_Xu_ebalance/ebalance1.5.1.rar) ， [[dofile]](http://yiqingxu.org/papers/english/2013_Hain_Xu_ebalance/replication_script.do)
- [Stata 手册命令 tebalance](https://www.stata.com/stata14/treatment-effects-balance/)


## Stata 实现

> Source: 文中的 Stata 实现  [[dofile]](http://yiqingxu.org/papers/english/2013_Hain_Xu_ebalance/replication_script.do)

```stata

/* Code to run examples */
/* JH and Xu 10/23/2012 */

***************************
/* installation */
***************************
// ssc install ebalance, all replace

***************************
/* Data */
***************************
sysuse cps1re74, clear
reg re78 treat age-u75

***************************
/* Basic Syntax */
***************************
ebalance treat age black educ, targets(1) 
tabstat age [aweight=_webal], by(treat) s(N me v) nototal

ebalance treat age black educ, targets(3)
ebalance treat age black educ, targets(3 1 2)

***************************
/* Interactions */
***************************

gen ageXblack = age*black
ebalance treat age educ black ageXblack, targets(1) 
bys black: tabstat age [aweight=_webal], by(treat) s(N me v) nototal

// factor variables or xi approach
ebalance treat i.black##c.educ, targets(1)
xi: ebalance treat i.black*age, targets(1) 
// ploynomials
gen age2 = age*age
xi: ebalance treat age age2, tar(1)

// full expansion
sysuse cps1re74, clear

***************************
/* Graphing */
***************************
ebalance treat educ black age, targets(2) 

***************************
/* Lalonde Example */
***************************
sysuse cps1re74, clear
foreach v in age educ black hispan married nodegree re74 re75 u74 u75 {
  foreach m in age educ black hispan married nodegree re74 re75 u74 u75 {
    gen `v'X`m'=`v'*`m'
  }
}

foreach v in age educ re74 re75 {
    gen `v'X`v'X`v' = `v'^3
  }
 
ebalance treat age educ black hispan married nodegree re74 re75 u74 u75 ///
         ageXage ageXeduc ageXblack ageXhispan ageXmarried ageXnodegree ///
		 ageXre74 ageXre75 ageXu74 ageXu75 educXeduc educXblack educXhispan ///
		 educXmarried educXnodegree educXre74 educXre75 educXu74 educXu75 ///
		 blackXmarried blackXnodegree blackXre74 blackXre75 blackXu74 ///
		 blackXu75 hispanXmarried hispanXnodegree hispanXre74 hispanXre75 ///
		 hispanXu74 hispanXu75  marriedXnodegree marriedXre74 marriedXre75 ///
		 marriedXu74 marriedXu75  nodegreeXre74 nodegreeXre75 nodegreeXu74 ///
		 nodegreeXu75 re74Xre74 re74Xre75 re74Xu75  re75Xre75 re75Xu74 u74Xu75 ///
		 re75Xre75Xre75 re74Xre74Xre74 ageXageXage educXeducXeduc,  keep(baltable) replace

svyset [pweight= _webal]
svy: reg re78 treat  

***************************
/* Survey reweighting */
***************************
ebalance age educ black hispan, manual(28 10 0.1 0.1)
```