
- **title** 里的文字处理问题。
  - 我是希望用 `title` 里的文字作为保存图片时图片的标题。但如果 `title=Stata: xxxx`，即标题文字中包含了诸如 `:`，`：` 或双引号之类的特殊符号时，Stata 是无法正确保存图片的。因此，我们在程序内部，需要把 `title` 中的特殊字符都用 `_` 代替，以保证图片可以正常保存。 
  