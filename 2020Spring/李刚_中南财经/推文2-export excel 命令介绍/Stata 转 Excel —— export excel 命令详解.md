> 作者：李刚 (中南财经政法大学)         
> 邮箱：gang.li.0814@gmail.com           

> Stata连享会   [计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q) || [简书推文](https://www.jianshu.com/p/de82fdc2c18a)  || [公众号合集](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

[![点击查看完整推文列表](https://images.gitee.com/uploads/images/2019/1208/003400_421b977a_1522177.png)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

 

> #### [2020寒假Stata现场班](http://www.peixun.net/view/1224.html)       
>  北京, 1月8-17日，连玉君-江艇主讲

[![2020寒假Stata现场班](https://images.gitee.com/uploads/images/2019/1208/003400_e471efb5_1522177.jpeg "2020寒假Stata现场班-扫码了解详情")](https://mp.weixin.qq.com/s/-dFPrGQMhH3JzA4tEc35kQ)

 

> #### [2020连享会-文本分析与爬虫-现场班](https://zhuanlan.zhihu.com/p/93858899)    
> 西安, 3月26-29日，司继春-游万海 主讲；  (附助教招聘)

![连享会-文本分析与爬虫专题班，西北工业大学，2020.3.26-29](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会2020.3文本分析海报.png "连享会-文本分析现场班，西北工业大学，2020.3.26-29")








 

## 1. 应用背景

数据处理过程中，通常面临数据集格式转换的难题，相比 Stata 的 `.dta` 数据文件，使用更广泛的是 Excel 数据。那么如何将 `.dta` 数据转换成以 `.xls` 或 `.xlsx` 后缀的 Excel 数据呢？

本文介绍的 `export excel` 命令便是主要解决此问题。下面将围绕**用法**和**应用案例**两方面展开，主要介绍命令的使用。

若采用窗口操作，相应的流程为：**File → Export → Data to Excel spreadsheet(_.xls; _.xlsx)**。

## 2. 用法介绍

下面，我们分两个小节，分别介绍 `export excel` 的基本语法和各个选项。

## 2.1 基本语法

`export excel` 的基本语法如下：

`export excel [using] filename [if] [in][, export_excel_options]`

- 其中，`export excel` 为命令主体，不可省略；
- `using` 为指定使用的文件，导出整个数据集时可省略；
- `filename` 为导出的 Excel 文件名；
- `if` 和 `in` 可限定要导出的数据的范围；
- `export_excel_options` 为在基础命令上添加的选项，**详细介绍见 2.2 小节。**

若只需导出部分变量名，则可在`excel export` 后面添加相应的变量名，**需要注意，此时的 `using` 不可省略**，对应的语法如下：

`export excel [varlist] using filename [if] [in][, export_excel_options]`

## 2.2 选项介绍

`export excel` 包含了丰富的选项，下面从主要选项 (Main Options) 和其他选项 (Advanced Options) 两方面进行介绍，并重点选取数据处理常用的进行详细说明。

### 主要选项 (Main Options)

| 选项                             | 用途                                                                   |
| :------------------------------- | :--------------------------------------------------------------------- |
| `replace`                        | 覆盖已有文件                                                           |
| `firstrow(variables or varlabels)` | 设置导出数据第一行为**变量名**还是**变量标签**                         |
| `sheet("sheetname")`             | 指定 sheetname                                                         |
| `cell(start)`                    | 从 `start` (upper-left) 开始写入数据                                   |
| `sheetmodify`                    | 不修改导出范围之外的数据，不能和 `sheetreplace` `replace` 选项连用     |
| `sheetreplace`                   | 导出之前先将 sheet 数据清除 ，不能和 `sheetreplace` `replace` 选项连用 |
| `nolabel`                        | 导出变量值，而不是变量的值标签                                         |
| `keepcellfmt`                    | 保留已有表格的单元格格式                                               |

值得说明的是：常用的是 `sheet("sheetname")` `firstrow(variables|varlabels)` `replace` 选项。

- `sheet()` 选项可以指定导出到 Excel 时数据的 sheet 名称，可是实现按需求修改 sheet 名的功能，套用在循环中使用比较方便，可以参见下文 3.1 小节 Task2 中的用法。
- `firstrow(variables|varlabels)`选项可以指定导出到 Excel 时表头为变量名还是变量标签，**当不添加 `firstrow` 选项时，默认时导出数据，不包含变量名**。
- `replace` 选项如同其他命令中的用法，覆盖已有数据，一般必选，否则报错 `file already exists` 。**但当使用`sheet()` 选项时`repalce` 可以省略**，表示在一份 Excel 表格中写入多张 sheet \*\*。

下面为大家演示 `sheet("sheetname")` 和 `firstrow(variables|varlabels)` 的使用效果，注意仔细观察导出的 Excel 文件 **sheet 名** 和 **表头** 的区别。

```Stata
sysuse auto, clear

* 设定 Excel 文件的子表 (Sheet) 名
export excel using "auto.xls", replace sheet("auto")

//结果：注意观察 Excel 文件的 Sheet 名

* 导出Excel的表头为变量名
preserve
  keep make price mpg rep78
  export excel using "auto_varname.xls", firstrow(variable) replace
restore

/*结果：auto_varname.xls 的前三行
make		price	mpg	rep78
AMC Concord	4,099	22	3
AMC Pacer	4,749	17	3
*/

*设定导出Excel的表头为变量名
preserve
  keep make price mpg rep78
  export excel using "auto_varlabel.xls", firstrow(varlabel) replace
restore

/*结果：auto_varlabel.xls 的前三行
Make and Model	Price	Mileage (mpg)	Repair Record 1978
AMC Concord	4,099	22	        3
AMC Pacer	4,749	17	        3
*/
```

### 其他选项 (Advanced Options)

| 选项                          | 用途                                                                                     |
| :---------------------------- | :--------------------------------------------------------------------------------------- |
| `datestring(datetime_format)` | 将时间格式数据导出为字符型                                                               |
| `missing(repval)`             | 将缺失值导出为指定的缺失值标记(`repval`)，字符型或数值型均可，<br> 如无此选项，默认导出为空格 |
| `locale(locale)`              | 当使用扩展的 ASCII 字符集可能需要此选项。默认的环境为 UTF-8                              |

## 3. 应用案例

### 3.1 案例一

按照 `rep78` 变量(汽车 1978 年维修次数) 将 **auto.dta** 拆分成 **Excel** 格式的子集 。

- Task1: 按照 `auto_rep78_i_.xls` 命名文件，其中 `i` 表示相应维修次数；
- Task2: 生成 `auto_rep78_all.xls`数据集，其中一个 Sheet 对应一个子集。

_提示：解决这两个问题的关键在于 2.2 部分重点介绍的 `sheet("sheetname")`和`firstrow(variables|varlabels)` 两个选项的使用。_

```Stata
sysuse auto, clear

tabulate rep78  //列表呈现 rep78 的类别和频数分布

levelsof rep78,local(rep)
foreach i in `rep'{
	preserve
	keep if rep78 == `i'
	export excel using "auto_rep78_`i'.xls", firstrow(variable) replace
	export excel using "auto_rep78_all.xls", firstrow(variable) sheet(`i')
	restore
	}
```

### 3.2 案例二

使用 `export excel` 导出 `nlsw88.dta`数据集的 **变量名** 和 **变量标签** ，存在`name`和 `varlabel`两列，文件名为 `nlsw88_varname_varlab.xls` 。

> **处理思路：** 导入 **nlsw88.dta**  数据 --> 使用 firstrow(variable) 选项获取变量名 (Data1) --> 使用 `firstrow(varlabel)` 选项获取变量标签(Data2) --> 合并  **Data1**  和  **Data2**  --> 由行转置为列 。

```Stata
sysuse nlsw88, clear

*获取变量名
preserve
  export excel using "nlsw88_varname.xls" in 1,firstrow(variable) replace
  import excel using  "nlsw88_varname.xls", clear
  keep in 1
  save "nlsw88_varname.dta", replace
restore

*获取变量标签
export excel using "nlsw88_varlab.xls" in 1, firstrow(varlabels) replace
import excel using  "nlsw88_varlab.xls", clear
keep in 1
save "nlsw88_varlab.dta", replace

*合并、转置
use "nlsw88_varname.dta", clear
append using "nlsw88_varlab.dta"
sxpose, clear
rename _var1 varname
rename _var2 varlabel
list varname  varlab, noobs
export excel using "nlsw88_varname_varlab.xls",firstrow(variable) replace

/*
  +-----------------------------------------+
  |       varname                  varlabel |
  |-----------------------------------------|
  |        idcode                    NLS id |
  |           age       age in current year |
  |          race                      race |
  |       married                   married |
  | never_married             never married |
  |-----------------------------------------|
  |         grade   current grade completed |
  |      collgrad          college graduate |
  |         south            lives in south |
  |          smsa             lives in SMSA |
  |        c_city     lives in central city |
  |-----------------------------------------|
  |      industry                  industry |
  |    occupation                occupation |
  |         union              union worker |
  |          wage               hourly wage |
  |         hours        usual hours worked |
  |-----------------------------------------|
  |       ttl_exp     total work experience |
  |        tenure        job tenure (years) |
  +-----------------------------------------+
*/
```

小彩蛋：`describe, replace` 命令可以快速实现上述需求，将数据集的 position, name, type, isnumeric, format, vallab, varlab 导出到一个新的数据集。

```Stata
sysuse nlsw88, clear
describe, replace
describe
list name varlab, noobs
keep name varlab
export excel using "nlsw88_varname_varlab.xls",firstrow(variable) replace
```

 

## 4. 附：文中所有代码

```Stata
* ##2.2 介绍 firstrow 和 sheet 的使用
sysuse auto, clear
*设定Excel文件的Sheet名
export excel using “auto.xls", replace sheet("auto")

*导出Excel的表头为变量名
preserve
  keep make price mpg rep78
  export excel using "auto_varname.xls", firstrow(variable) replace
restore

*设定导出Excel的表头为变量名
preserve
  keep make price mpg rep78
  export excel using "auto_varlabel.xls", firstrow(varlabel) replace
restore

* ## 3.1 案例一
sysuse auto, clear
levelsof rep78,local(rep)
foreach i in `rep'{
	preserve
	keep if rep78 == `i'
	export excel using "auto_rep78_`i'.xls", firstrow(variable) replace
	export excel using "auto_rep78.xls", firstrow(variable) sheet(`i')
	restore
	}

* ## 3.2 案例二
sysuse nlsw88, clear

*获取变量名
preserve
  export excel using "nlsw88_varname.xls" in 1,firstrow(variable) replace
  import excel using  "nlsw88_varname.xls",clear
  keep in 1
  save "nlsw88_varname.dta", replace
restore

*获取变量标签
export excel using "nlsw88_varlab.xls" in 1, firstrow(varlabels) replace
import excel using  "nlsw88_varlab.xls",clear
keep in 1
save "nlsw88_varlab.dta", replace

*合并、转置
use "nlsw88_varname.dta", clear
append using "nlsw88_varlab.dta"
sxpose, clear
rename _var1 varname
rename _var2 varlabel
list varname  varlab, noobs
export excel using "nlsw88_varname_varlab.xls",firstrow(variable) replace
```


 

>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。
- **欢迎赐稿：** 欢迎赐稿至StataChina@163.com。录用稿件达 **三篇** 以上，即可 **免费** 获得一期 Stata 现场培训资格。
- **往期精彩推文：**
 [Stata绘图](https://mp.weixin.qq.com/s/xao8knOk0ulGfNc7vasfew) | [时间序列+面板数据](https://mp.weixin.qq.com/s/8yP1Dijylgreg59QIkqnMg) | [Stata资源](https://mp.weixin.qq.com/s/Kdeoi5uJyNtwwwptdQDQDQ) | [数据处理+程序](https://mp.weixin.qq.com/s/_3DQacFyy7juRjgFedp9WQ) |  [回归分析-交乘项-内生性](https://mp.weixin.qq.com/s/61qJNWnL4KRp0fbLxuDGww)

---

![欢迎加入Stata连享会(公众号: StataChina)](https://images.gitee.com/uploads/images/2019/1208/003400_c9e11a3f_1522177.png)

