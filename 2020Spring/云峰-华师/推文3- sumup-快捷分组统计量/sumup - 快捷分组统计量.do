*基本统计
sysuse nlsw88.dta
sumup wage, by(race)

*全指标统计
sysuse nlsw88.dta
sumup wage, by(race) detail

*双限定分组统计
sysuse nlsw88.dta
sumup wage,by(union married)

*选取特定指标分组统计
sysuse nlsw88.dta
sumup wage, by(industry) statistics(mean p80)

*sum vs sumup
sysuse nlsw88.dta

sum wage

sumup wage, by(race)

*sumup vs tabulate
sysuse nlsw88.dta

sumup wage, by(race)

tab race,sum(wage)

*sumup vs tabstat
sysuse nlsw88.dta

sumup wage, by(race)

tabstat wage, by(race) stat(n mean sd min max) nototal long col(stat)
