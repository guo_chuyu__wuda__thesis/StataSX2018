> 唐继凤（中山大学）     
> 邮箱：18177471572@163.com

&emsp;

## 1. 问题背景

对于要使用数据进行研究的学术研究者来说，经常要面对数据处理问题，一个常见的场景是市场营销部门、运营部门或业务部门给你提供两组具有不同变量的相似数据，要求分析团队将这两个数据集标准化，以便形成一个用于建模的主记录。同时由于数据驱动的决策越来越重要，拥有强大的模糊匹配工具是等式的重要组成部分，并且将是改变业务未来的关键因素之一。在金融服务和医疗保健等各个行业，有大量的数据匹配工具正在进行着大量的工作。如何匹配两个不同但相似的数据集以获得建模的主记录?除了手工操作外，最常用的方法是模糊匹配。

那么，什么是模糊匹配呢?以下是来自维基百科的简短描述:模糊匹配是一种作为记录链接的特殊计算机辅助翻译技术。当查找文本片段与之前翻译数据库中的条目之间的对应关系时，它处理的匹配可能不是100%完美的。它通常在句子层面上运作，但一些翻译技术允许在短语层面上进行匹配。通常当译者在处理翻译记录时使用。

我们在处理数据时如何进行模糊集匹配呢？`matchit` 是处理模糊集匹配的工具之一，可以根据字符串变量将两个数据集的观察数据连接起来，不一定要完全相同。它执行许多不同的基于字符串的匹配技术，允许两个不同文本变量之间的模糊相似性。本文将详细介绍 `matchit` 命令。

## 2. matchit 命令

`matchit`基于相似的文本模式匹配两个列或两个数据集。`matchit` 通过执行许多不同的基于字符串的匹配技术，在两个不同的文本字符串之间提供相似度评分。它返回一个新的数值变量(*similscore*)，其中包含从0到1的相似性分数。根据所选择的字符串匹配技术，相似分数为1意味着具有完美的相似性，而当匹配不太相似时，相似度会降低。*similscore*是一个相对的度量方法，它可以(并且经常)根据所选择的技术而变化。`matchit` 在以下两种情况下特别有用：（1）当两个数据集对于相同的字符串数据有不同的模式时（例如，个人或公司名称、地址等）；（2）当其中一个数据集相当大，并由不同的来源提供，使其格式不统一（例如，名称或地址来自不同来源，顺序可能不同）。在使用`merge` 或类似命令时，在这种情况下连接数据可能会导致一些错误。因此模糊匹配的这两个变量可以来自同一个数据集，也可以来自两个不同的数据集。所使用的命令如下：

```
Data in two columns in the same dataset ///两个变量在同一数据集时

     matchit varname1 varname2 [, options]

Data in two different datasets (with indexation)///两个变量在两个不同数据集时

     matchit idmaster txtmaster using filename.dta , idusing(varname) txtusing(varname) [options]

```


以下是两种匹配方式通用的重要选项：

选项|解析

---|---
`similmethod (simfcn)`|指字符串匹配方法
`score (scrfcn)`|指定的相似性得分
`weights (wgtfcn)`|加权变换，默认是 `noweights`
`generate (varname)`|指定相似度评分变量的名称, 默认是 `similscore`

以下是匹配来自两个数据集的变量的必须选项：
匹配命令:

```
matchit idmaster txtmaster using filename.dta , idusing(varname) txtusing(varname) [options]
```
选项|解析
---|---
 `idmaster`|指当前文件(`masterfile`)中的数值变量，不需要唯一标识来自主文件的观察值(尽管推荐)。
 `txtmaster` |当前文件(`masterfile`)中的字符串变量，将被匹配到 `txtusing`。
 `using filename`  |要匹配的`Stata`文件（`usingfile`）的名称(和路径)。
`idusing (varname)`|`usingfile`中的数值变量。不需要从`usingfile`中唯一地标识观察值(尽管推荐)。
`txtusing (varname)`|来自`usingfile`的字符串`varname`将被匹配到`txtmaster`。

仅适用于匹配来自两个数据集的数据的高级选项：

选项|解析
---|---
 `threshold (num)`|在最终结果中保留最低的相似度分数。默认为`num `= 0.5。
 `override `|忽略未保存的数据警告。
 `diagnose`|报告对指数化的初步分析。用于通过清洗原始数据和尝试不同的`simfcn`来优化索引。
 `stopwordsauto`|自动生成停止词列表。它提高了索引速度，但忽略了潜在的匹配。
` swthreshold (grams-per-observation)`|只对`stopwordsauto`有效。它设置了每次观测的阈值，并将其包含在停止词列表中。默认情况下，每个观察值对应的语法值为0.2。

### 2.1 安装
`Stata` 中 `matchit`的安装命令：
```
ssc install matchit
```

### 2.2 范例

#### 2.2.1 Stata 范例 1

本文的范例一是对两个不同数据集的数据进行模糊匹配，为了更好说明`Stata`操作过程，本文简单建立两个数据集文件：`file1.dta` 和 `file2.dta`。两个数据集数据结构如下：

![matchit命令详解-图1-唐继凤.png](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/matchit命令详解-图1-唐继凤.png.png)

![matchit命令详解-图2-唐继凤.png](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/matchit命令详解-图2-唐继凤.png.png)

数据集`file1.dta`显示的是企业名称（Name）和企业销售量（Sales）两个变量，数据集`file2.dta`显示的企业名称（Name2）和企业顾客数（Customers）两个变量，两个数据集缺乏统一的唯一性识别变量，且企业名称变量具有相似性，因此可以使用`matchit`进行匹配。

`Stata`命令为：

```
.use file1.dta
.matchit Sales Name using file2.dta, idusing(Customers) txtusing(Name2) 
```
窗口运行结果为：
```
matchit Sales Name using file2.dta, idusing(Customers) txtusing(Name2) 
Matching current dataset with file2.dta
Similarity function: bigram
Loading USING file: file2.dta
Indexing USING file.
0%
40%
60%
100%
Done!
Computing results
        Percent completed ...   (search space saved by index so far)
        40%               ...   (67%)
        60%               ...   (50%)
        100%              ...   (34%)
        Done!
Total search space saved by index: 34%

```

匹配结果如下图：

![matchit命令详解-图3-唐继凤.png](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/matchit命令详解-图3-唐继凤.png.png)


注：最后一列`simiscore`为两个数据集的相似度评分，可以删除相似度评分较低的数据，比如使用`drop if similscore<.7`删除相似度评分低于70%以下的，另外可以根据数据结构和需要添加其他选项。

#### 2.2.2 Stata 范例 2

本文的范例二是对同一数据集里的两个变量进行模糊匹配，下图所示的为`file3.dta`的数据，该数据集中的 **Name** 和 **Name2** 两个变量具有一定相似性，可使用`matchit`进行相似度评分来选取合适的数据。（注：对同一数据集中的两个变量进行匹配仅适用于字符型变量。）

![matchit命令详解-图4-唐继凤.png](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/matchit命令详解-图4-唐继凤.png.png)

`Stata`命令为:
```
.use file3.dta
.matchit Name Name2
```
窗口运行结果为：
```
. matchit Name Name2
Matching columns Name and Name2
Similarity function: bigram
0%
40%
60%
100%
Done!
```
匹配结果如下：

![matchit命令详解-图5-唐继凤.png](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/matchit命令详解-图5-唐继凤.png.png)

#### 2.2.3 `matchit`总结

需要注意的是，matchit是区分大小写的。它还考虑了所有其他符号(就`Stata`而言)。虽然使用matchit不需要数据清理，但它通常意味着相似性分数的提高，从而提高了匹配练习的整体质量。然而，过多的数据清理可能会删除相关信息，导致误报对质量产生负面影响。其他需要注意的总结如下：
- 虽然`matchit`复制了`merge`命令的最标准用法(即交集`_merge == 3`)，但是当没有错误否定的风险时，它的效率就会降低。
- 使用`matchit`后，结果数据集可以很容易地与主数据集合并，也可以使用`merge`或`joinby`命令与数据集合并。
- 在本质上，使用哪个文件作为主文件或使用哪个文件并不重要。它只与结果数据集中列的顺序有关。计算时间可能不同，但这些差异并不是实质性的(在5%之内)。
- 结果数据集中的观察值没有排序。这可以通过使用排序命令(如`sort`或`gsort`)在运行`matchit`后轻松完成。通常，将结果数据集从较高的相似度评分排序到较低的相似度评分是很有用的，以便手动建立最佳阈值来保持。
- 使用者可以通过添加自己的相似度、权重或评分功能来定制`matchit`，并从其索引和其他功能中获益。

## 3. 其他模糊匹配命令：`reclink` 以及其升级版本`reclink2`

### 3.1 `reclink`介绍

`reclink`使用记录链接方法来匹配两个数据集之间的观察值，而这两个数据集之间没有完美的关键字段——本质上是一个模糊合并。

#### 3.1.1  `reclink`简介

`reclink`允许用户定义每个变量的匹配和非匹配权值，并使用一个二元字符串比较器来评估不完全的字符串匹配。主数据集和使用数据集的数据集必须各有一个惟一标识观测值的变量。创建了两个新变量，一个用于保存匹配的分数(比例为0-1)，另一个用于合并变量。此外，来自`using`数据集的所有匹配变量都被引入主数据集(带有新前缀的名称)，以便手动检查匹配。为了提高这个通常很慢的过程的速度，可以使用`or-blocking`，它需要至少一个变量来在数据集之间完美匹配。如果指定了4个或多个变量，则默认为`or -blocking`。

### 3.1.2 `reclink`命令

```
 reclink varlist using filename , idmaster(varname) idusing(varname) gen(newvarname) [ wmatch(match weight list) wnomatch(non-match weight list) orblock(varlist)
                     required(varlist) exactstr(varlist) exclude(filename) _merge(newvarname) uvarlist(varlist) uprefix(text) minscore(#) minbigram(#)

```
在使用`reclink`时需要注意：
- `idmaster (varname)`是必需的，它指定主数据集中唯一标识观察值的变量的名称。这个变量用于跟踪观测。如果不存在唯一标识符，则可以简单地将其创建为`gen idmaster=_n`。
- `idusing (varname)`是必需的，它指定了`using`数据集中唯一标识类似于`idmaster`的观察值的变量的名称。
- `gen (newvarname)`是必需的，它指定了一个新变量的名称，该变量由`reclink` 创建，用于存储关联观察的匹配分数(范围为0-1)。
- 一般情况下，记录连接方法是不完善的，结果应该手动检查，特别是对于匹配分数较低的观察。使用不同的权值、块选项和派生变量尝试多次运行以增加链接的准确性是很常见的。在`exclude`选项的帮助下，可以使用一系列的`reclink`命令。

### 3.2 `reclink2`介绍

`reclink2`一个记录连接程序，是`reclink`的升级版。

#### 3.2.1 `reclink2`简介

`reclink2`在两个数据集之间执行概率记录链接，这两个数据集没有标准合并所需的联合标识符。该命令是由**Blasnik**(2010)编写的`reclink`命令的扩展。这两个数据集被称为"主数据集"和"使用数据集"，其中主数据集是当前使用的数据集。对于主数据集中的每个观察值，程序尝试根据指定的变量列表、它们的关联匹配和不匹配权重以及它们的`bigram`评分，从正在使用的数据集中找到最佳匹配记录。`reclink2`命令引入了两个新选项，`manytoone`和`npair()`。
#### 3.2.2 `reclink2`命令
```
reclink2 varlist using filename, idmaster(varname) idusing(varname) gen(newvarname) [wmatch(match_weight_list) wnomatch(nonmatch_weight_list) orblock(varlist)
                required(varlist) exactstr(varlist) exclude(filename) _merge(newvarname) uvarlist(varlist) uprefix(text) minscore(#) minbigram(#) manytoone npairs(#)]

```
`reclink2`的大部分选项与`reclink`相同，本文主要介绍它新增的选项`manytoone`和`npair()`。
- `manytoone`指定`reclink2`将允许来自使用数据集的记录与来自主数据集的多条记录匹配(多对一链接过程)。在`reclink`的基本版本中，第一步从两个数据集中查找和删除完全匹配的对。因此，正在使用的数据集中与主数据集中的一条记录完全匹配的一条记录随后不能链接到主数据集中的另一条记录，因为它与主数据集中的另一条记录虽然不完全匹配，但已经足够了。此选项有效地允许使用数据集进行替换后的采样。
- `npair(#)`指定程序保留使用数据集中与主数据集中的给定记录相对应的潜在匹配项的顶部#(高于最低得分阈值)。在`reclink`的基本版本中，只有匹配分数最高的候选人被保留为匹配——除非匹配分数最高的候选人是相同的。因为近似字符串比较器是不完美的，一个不正确的记录有时会比一个正确的记录获得更高的分数，并被`reclink`选为最佳匹配。通常情况下，在文书审查期间必须删除这些匹配；然后，在随后的传递中，修改`varlist`或权值以找到更合适的匹配。`npair()`允许用户检查并找到额外的匹配项，否则需要多次"通过"，因此需要多次文书检查。因为`npair()`的计算时间没有增加，所以它应该有助于提高大规模匹配问题的效率，这些问题通常依赖于多次遍历来获得最佳的精度和覆盖率。但是，请注意，虽然`npair(#)`可以捕获不会产生最高分数的正确匹配，但是超过最低分数阈值的错误匹配也会包含在输出中。因此，我们建议用户保持`#`尽量小(通常为2或3)，并将`npair()`与`minscore()`结合使用。


## 4. 参考资料

- Fuzzy Matching Algorithms To Help Data Scientists Match Similar Data - Data Science Central. [[Link]](https://www.datasciencecentral.com/profiles/blogs/fuzzy-matching-algorithms-to-help-data-scientists-match-similar)
- `help reclink2` Stata Journal || Record Linkage using STATA: Pre-processing, Linking ... - Aaron Flaaen，Stata Journal 15-3. (连玉君建议：要写出标准的参考文献信息)
- Blasnik, M.  2010. reclink: Stata module to probabilistically match records.  Statistical Software Components S456876, Department of Economics, Boston College. [[Link]](https://ideas.repec.org/c/boc/bocode/s456876.html)
