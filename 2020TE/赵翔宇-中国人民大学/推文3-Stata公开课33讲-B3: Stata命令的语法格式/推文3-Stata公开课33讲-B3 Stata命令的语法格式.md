# Stata 命令的语法格式

## 1. 几个示例
- `if`
- `in`
- `, options`
- `bysort, `


## 2. Stata 的一般语法格式
- `help language` 具体介绍

## 3. 变量的引用
- 通配符

## 4. 因子变量：变量的前缀
- `i.`
- `c.`

## 5. 时间序列变量的简写
