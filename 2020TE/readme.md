
## 连享会助教团队写作指南

&emsp;

## 通知
- ${\color{red}{NEW!}}$ Stata 33 讲 认领
  - 已经选择要做 「Stata 33 讲」的同学，请尽快到如下页面认领选题：[「Stata-33讲认领」](https://gitee.com/Stata002/StataSX2018/wikis/Stata%E5%85%AC%E5%BC%80%E8%AF%BE%E6%96%87%E6%A1%A3-%E4%B8%80%E7%BB%84%E4%BB%BB%E5%8A%A1.md?sort_id=2195157)。对应的 dofiles和数据可以在 [「连享会公开课-Stata101」](https://gitee.com/arlionn/stata101) 主页下载。
  - **特别提醒：** 请尽量用 Jupyter Notebook 来写这一组推文，以便随后在 [「连享会公开课-Stata101」](https://gitee.com/arlionn/stata101) 主页发布。
  - 视频免费观看页面：[连享会短书 - Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0)
  - Stata dofiles 我这两天打包发给大家。 


## 任务记录

### 5.13 日：注册码云-点击邀请码 

1. 注册码云账号，详情参见 [码云使用说明](https://gitee.com/Stata002/StataSX2018/wikis/%E7%A0%81%E4%BA%91%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97.md?sort_id=937861)
2. 点击邀请码获取编辑权限：

### 助教小组分工 (`2020/5/13`)
- **基础**：冷  萱(组长)  冀云阳  赵翔宇  赵纯洁
- **TFP**： 汪  蕾(组长)  崔丽娟  秦利宾
- **DEA**： 谭睿鹏(组长)  周  涛  张翠燕 
- **SFA**： 秦利宾(组长)  欧  旭  李  丹  张翠燕  

### 推文工作安排

> 每位同学完成 2~3 篇推文，在 **7 月 1号** 之前完成 。 

`2020/5/19 14:29` 我们 5.13 日的腾讯会议上沟通完后，部分同学已经联系我确认了推文写作题目，也有一些同学已经在码云自己的主页下列出了写作提纲，比如 。请大家在 5.20 日前确定推文写作题目 (若暂时还无法确定三篇推文的写作题目，可以先确定一篇)，然后采用接龙的方式把题目报告给我，并在码云主页下上传初步写作提纲，我们 5.21 日开会讨论。

#### 初步选题意向 

> `2020/5/19 23:38`

1. 秦利宾，A. PSM 介绍；B. 聚类调整标准误
2. 冷萱：A. Stata33讲选择2讲 B. 其他待定
3. 谭睿鹏：A. 时间断点回归; B. Harvard Dataverse 找一篇论文重现；
4. 山东大学张翠燕 ：A. 使用python替代人工下载wind上市公司母子公司数据；B. 其他待定
5. 浙大_欧旭 A.Stata 33讲—缺漏值处理 B.CIC模型及Stata命令 C.SFA模型下的参数解读
6. 中国人民大学_赵翔宇： A.Stata33讲选择3讲；B.其他待定
7. 东南大学-李丹 A 基尼系数的空间分解 B其他待定
8. 华中师范大学-周涛 ：A did-实验组样本很少时如何处理？；B 其他待定
9. 浙江大学-崔丽娟 ：A.TFP测算过程中的问题总结；B.Stata33讲选2讲；C.EconPapers网站简介
10. 山东大学汪蕾 A.分类统计结果图示 B.会计领域论文的可重现问题
11. 广东财经—冀云阳：A.sargan和hansen J检验无法通过怎么办?B.stata 33讲选择2讲

> `2020/6/4 10:09`

#接龙：确认推文任务 - 【
格式：姓名，是/否】
【是/否】表示可以在 7.1 前完成 (若有困难可以说明理由和预期完工时间)

2. 西南财经大学_冷萱  ，是
3. 厦门大学 秦利宾,  是
4. 广东财经大—冀云阳，是
5. 东南大学-李丹 7月1日可以完成一篇，最近在大论文收尾工作，预计还要2周
6. 中国人民大学_赵翔宇，是
7. 山东大学张翠燕 ，是
8. 华中师范大学-周涛 ，是
9. 浙江大学-崔丽娟，是
10. 山东大学-汪蕾，是
11. 南京大学-谭睿鹏，是
12. 浙大_欧旭 ，是







&emsp;

&emsp;

&emsp;

---
**目录**
[TOC]
--- - --

&emsp;

大家好！写这份文档是为了各位更好地完成推文。主要介绍了推文的选题来源、写作规范和注意事项。我也分享了一些我平时使用的工具和软件。




## 0. 资源链接和推文选题

### 资源
大家写作时可以参考的书籍和文献：
- **连享会图书馆：** [计量教材](https://quqi.gblhgk.com/s/880197/wqZT9wv6IKpd4Wh1)  || [Stata Journal](https://quqi.gblhgk.com/s/880197/Uv7IUeASWJnvyjUd) || [SJ单篇PDF](https://www.lianxh.cn/news/12ffe67d8d8fb.html) 
- **常用工具和链接：** [连玉君的链接](https://www.lianxh.cn/news/9e917d856a654.html) || [五分钟 Markdown](https://gitee.com/arlionn/md) 
- **学术资源：**
  - 搜索引擎：[百度学术](http://xueshu.baidu.com/) | [微软学术](https://academic.microsoft.com/home) | [iData论文下载](https://www.cn-ki.net/)
  - 论文重现:  [Harvard dataverse](https://dataverse.harvard.edu/dataverse)  | [JFE](http://jfe.rochester.edu/data.htm)  | [github](https://github.com/search?utf8=%E2%9C%93&q=stata&type=)
  
  
### 推文选题		

我们写推文的目的一方面是分享经验，另一方面也是藉此提升我们对某些方法的理解。 

选题主要有几个来源：其一，研究过程中的经验总结，包括数据处理方法、模型含义和参数解释、方法汇总介绍、前沿方法介绍等；其二，浏览 Stata Journal，Stata Blogs，以及国外学者的主页时，看到了好的英文推文，可以进行翻译或编译；其三，从提供了期刊论文重现的论文中获取数据和程序，提炼方法部分的介绍。

我平时也想到了一些选题，统一放置于 [StataSX2018](https://gitee.com/Stata002/StataSX2018) &rarr; [【Wiki - 002_备选专题】](https://gitee.com/Stata002/StataSX2018/wikis/Home) 页面下，并不定期更新。

大家从中选题，并将选定的题目拖入【==已认领==】文件夹，推文完成后拖入【\~\~已完成~~】文件夹。

**切记：** 认领某个题目时，请标注上你的姓名。选题后请与连老师商议提纲后再开始正式撰写。

&emsp;

---

## 1. 推文整体要求和编辑器

> 推文写作流程 - 速览
- **S1：** 在 VScode 或 Typora 中写推文初稿 (建议两个同时用)。
  - 推文中的图片务必上传到连享会主页图床，参见第 **「2. 设定 PicGo 图床」** 小节。
  - 推文格式要求请务必严格遵守，参见 「4. 推文格式要求」，以及 「[简化版范例](https://gitee.com/Stata002/StataSX2018/wikis/01B-%E6%8E%A8%E6%96%87%E6%A8%A1%E6%9D%BF-%E7%AE%80%E5%8C%96%E7%89%88.md?sort_id=2169931)」和「[完整版范例](https://gitee.com/Stata002/StataSX2018/wikis/01A_%E6%8E%A8%E6%96%87%E6%A8%A1%E6%9D%BF-%E5%AE%8C%E6%95%B4%E7%89%88.md?sort_id=2169929)」(单击页面中的【编辑】按钮查看原始 Markdown 文档)。
- **S2：** 完成推文后务必贴入 [mdnice.com](https://mdnice.com/) 测试一下文中的数学公式和图片是否能正常显示
- **S3：** 将测试完的推文 Markdown 文档贴入你的码云主页下 (此时格式错乱，数学公式无法正常显示等都可以忽略)
> Note: 写作过程中，有任何问题，都可以在微信群里讨论。

---



我们的推文都用 Markdown 来写。相关介绍参见 [连玉君-五分钟 Markdown](https://gitee.com/arlionn/md)。

为了保证推文中的图片都能永久保存，请各位写推文时，最好使用我们自己的阿里云图床 (PicGo)。具体要求如下：
- **推文写作工具**。建议统一使用 [「VS Code」](https://code.visualstudio.com/) ([教程1](https://www.cnblogs.com/clwydjgs/p/10078065.html)；[教程2](https://www.cnblogs.com/qianguyihao/archive/2019/04/18/10732375.html)) 或 Typora 编辑器撰写 Markdown 文稿。它也可以作为自己个人写 Markdown 或其他软件代码的编辑器，很好用。     
  - (不推荐！仅在入门时使用) 当然，你也可以使用 [「简书」](https://www.jianshu.com/) 写推文初稿，可以拖拽上传图片，也可以实时保存。但简书的图床不能向外分享。因此，如用简书写推文，请务必将所有图片另存一份在你的电脑上。推文中的图片使用下面介绍的 PicGo 软件上传到阿里云图床。**特别注意：** 写完后不要发布，这样图片就不会被转存为简书的图床了。
- **测试和上传**。写好推文后，请贴入 mdnice.com 网站编辑，确保能正常显示。重点查看数学公式和图片是否能正常显示。测试后即可将 Markdown 原文贴入你的码云主页。码云只作为存储工具。因此，在 mdnice.com 测通过后的推文，即使码云中不能正常显示无所谓。
- **本地推文文件夹**。请在你的电脑中为每篇推文设定一个独立的文件夹，里面新建两个子文件夹：【Data】和【Figs】，分别用于存放推文中的范例数据和图片。推文完成后可以把这个文件夹传到码云上。**备份：** 可以在【推文1】文件夹中新建一个文本文件，然后把推文文字贴入其中。
- **图片要求**。图片要保证清晰，每幅图片内的字号尽可能一致。全文顺序编号。
  - <font color=red>图片名称</font>：【推文标题简称_Fig#_图片简称_作者姓名.png】 (或 jpg)。例如，【面板数据一文读懂_Fig1_常用Stata命令_游万海.png】。图片名称中不要包含空格。
  - **特别提示：** 由于我们的团队统一使用一个图床，每幅图片的名称就是这副图片的唯一识别码，因此图片名称尽可能有区分度，不要写为 **fig1** 或 **图1** 这样的过于简略的名称，要尽量按上述建议格式命名图片。 
- **其它建议。** 当然，你也可以使用其他 Markdown 编辑器，比如 Typora。这个软件可以配合 Pandoc，把写好的 Markdown 转换为 PDF，HTML，幻灯片，甚至是 Word 文档，效果非常棒。

![](https://images.gitee.com/uploads/images/2020/0511/145749_5786e2c9_1522177.png)


&emsp;

## 2. 设定 PicGo 图床 

- [图床工具的使用：PicGo 介绍](https://www.jianshu.com/p/9d91355e8418)

### 2.1 下载-安装 PicGo
百度云盘：https://pan.baidu.com/s/10N736oJVCPrTuVNaGrNOwQ

解压后按提示安装即可。

最新版下载地址：<https://github.com/Molunerfinn/PicGo/releases>

### 2.2 配置图床 (依次填入)

打开 PicGo，点击左侧「**图床设置**」下来菜单，选择「**阿里云OSS**」，填入如下信息：

  - **设定 KeyID：** xxxx参见PDF版手册
  - **设定 KeySecret：** xxxx参见PDF版手册
  - **设定存储空间名：** fig-lianxh  
  - **确认存储区域：** oss-cn-shenzhen   
  - **指定存储路径：** 不填
  - **设定自定义域名：** 不填

![PicGo-阿里云图床配](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/PicGo-阿里云图床配置.png)


### 2.3 上传图片
- **本地图片**：采用点击或拖拽方式上传完图片后，PicGo 会自动生成 Markdown 格式的图片链接：一份自动存于剪切板，另一份显示在屏幕右下角。我们只需到 Markdown 编辑窗口中，按下快捷键「**Ctrl+V**」即可贴入图片链接地址。
- **剪切板图片**：通过截图等方式得到的图片 (已经自动保存在了剪切板上)，可以直接点击图中右底角【剪切板图片上传】按钮上传图片 (有时候需要点击两次才能正确上传，可以点击左边栏的【相册】按钮查看是否成功上传)。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/PicGo-上传图片.png)

### 2.4 其他说明
- 点击左边栏【相册】按钮，可以查看已经上传的图片，并从图片下方的第一个菜单复制图片链接； 
- 点击【PicGo设置】可设定快捷键和各种参数，较少使用。


&emsp;

## 3. mdnice.cn - Markdown 文档测试

我们的推文最终要在 mdnice.com 测试，数学公式、图片以及其他格式方面的要求都以能在这网站的编辑器右侧预览框中正常显示为准。
- [mdnice.cn 官方介绍 1](https://docs.mdnice.com)，[介绍 2-详细](https://mp.weixin.qq.com/s/OeqUgCR5PtuhNEzeXbmyAQ) 或点击【功能】&rarr; 【重置】即可查看相关说明。
- [mdnice.cn 简介 - 连玉君](https://blog.csdn.net/arlionn/article/details/104113414) 对核心功能作了介绍。


&emsp;

## 4. 推文格式要求

> **特别注意：** 除非万不得已，尽量 **不要** 用图片形式展示 Stata 结果。推文中的 Stata 结果尽量采用结果窗口中呈现的文本信息来呈现，也就是采用代码块来呈现估计结果。这样就不会过度依赖图床，图床一旦出问题，就会导致推文中的所有图片都无法正常显示。另外，使用文本块显示结果可以获得更好的跨平台展示效果。
> &emsp;        
> **范本：**「[简化版范例](https://gitee.com/Stata002/StataSX2018/wikis/01B-%E6%8E%A8%E6%96%87%E6%A8%A1%E6%9D%BF-%E7%AE%80%E5%8C%96%E7%89%88.md?sort_id=2169931)」和「[完整版范例](https://gitee.com/Stata002/StataSX2018/wikis/01A_%E6%8E%A8%E6%96%87%E6%A8%A1%E6%9D%BF-%E5%AE%8C%E6%95%B4%E7%89%88.md?sort_id=2169929)」(单击页面中的【编辑】按钮查看原始 Markdown 文档)。

1. **署名**： 请在推文首行填入你的真实姓名 (单位信息)，格式为 `> 张三丰 (xxx大学)，邮箱`
1. **标题和层级：** 
- 推文的各个小节需要顺序编号
- 一级标题用 **「`## 1. 一级标题`」** (说明：`##` 后要添加一个空格)
- 二级标题用 **「`### 1.1 二级标题`」**，最多用到三级标题，即「`#### 1.1.1 三级标题`」。
- 如果后续还需细分，可以用 **条目(item)** 或独占一行的粗体文字代替。这主要是因为，多数 Markdown 编辑器的一级标题字号都太大了。
2. **段落：** 段落之间要空一行，这样网页上的显示效果比较好；每个段落不要太长 (最好不好超过 200 字，4 行以内比较好)，否则网页或手机阅读时会比较累；
3. **中英文混排**：
- **记得加空格！！** 英文字符和数字两侧要空一格，否则中英文混排时字符间距过小；例如：`我用Stata已经15年了(Cox,2019)，但Arlion(2018)说他刚用了14年。` 要修改为：`我用 Stata 已经 15 年了 (Cox, 2019)，但 Arlion (2018) 说他刚用了 14 年。`。
- **括号的使用** 文中所有圆括号都要在半角模式下输入，`(` 左侧和 `)` 右侧各添加一个空格。中英文混排格式参见上一行里的例子。
4. **Stata 相关**
  - Stata 要写为 「Stata」(首字母大写，无需加粗)，不要写成 「stata」或「STATA」。
  - 变量名用粗体 (如 \*\*varname\*\* &rarr; **varname**)；
  - Stata 命令用高亮显示 (如 \`regress\` &rarr; `regress`)；
  - 多行 Stata 命令和结果展示使用 **代码块样式**，即使用首尾 **\`\`\`** 包围，首行为 「\`\`\`stata」，尾行为「\`\`\`」。
  - **特别注意：** 除非万不得已，尽量不要用图片形式展示 Stata 结果。推文中的 Stata 结果尽量采用结果窗口中呈现的文本信息来呈现，也就是采用代码块来呈现估计结果。这样就不会过度依赖图床，图床一旦出问题，就会导致推文中的所有图片都无法正常显示。另外，使用文本块显示结果可以获得更好的跨平台展示效果。
  - 把如下语句加入你的 **profile.do** 文档中，以便保证结果显示格式比较规整：
  ```stata
   set cformat  %4.3f  //回归结果中系数的显示格式
   set pformat  %4.3f  //回归结果中 p 值的显示格式      
   set sformat  %4.2f  //回归结果中 se值的显示格式  
  ```
5. **数学公式：** 
- **(1) 单行公式：** 用 **\$\$**math**\$\$** 包围的单行数学公式上下各空一行，以保证公式和正文之间的段落间距合理。
- **(2) 行内公式：** 可以使用 **\$**math**\$** 包围。为了保证在知乎，简书和码云中都能正常显示公式，请把 $y=x\beta$ 写成 `$y=x\beta$`，而不要写成 `$ y=x\beta $` (内侧多加了两个空格)。
- **(3) mathpix 神器 - 惊喜：** 无论是网页还是 PDF 中的数学公式，都可以使用 mathpix (https://mathpix.com/) 软件扫描后识别成 LaTeX 数学公式，非常快捷。参见 [神器-数学公式识别工具 mathpix](https://www.jianshu.com/p/1f0506163694)。本页面顶端的「连享会成员工具包」中提供该软件的下载。
6. **图片**：
- (1) 务必使用 lianxh.cn 的图床，参见第 2 小节 - 「2. 设定 PicGo 图床」。
- (2) 图片的命令规则：**作者姓名_推文关键词_Figxx.png/jpg**，例如：**连玉君_面板数据一文读懂_Fig01.png**。由于我们所有人的图片都放置在同一个图床，若图片名称过于简单，会导致图传里面有很多重名的文件，引用的时候就会出错。 
7. **版权和引用：** 推文若为英文翻译稿，请务必在文首注明来源(独占一行)。Markdown 格式为：「`**Source：** [原英文文献完整信息](网址)`」，显示效果为「**Source：**[原英文文献完整信息](网址)」。文中若有从别处复制粘贴而来的内容，要标明出处。
8. **底部文本：** 推文底部的文字介绍无需添加，在修改完后会由总编统一添加。





&emsp;

## 5. 工具和软件

### 5.1 常用软件下载链接 

- 链接：https://pan.baidu.com/s/1tfXziwFDeSdyboLdpBLq8g 
- 提取码：qsvf

### 5.2 常用工具简介

- **VScode**: Markdown 编辑器，很好用，我们用这个编辑器写推文
- **Typora**: Markdown 编辑器 (将 Word, 网页文档贴入该编辑器，可以自动转换为 Markdown 文档) + Pandoc (可以把 Markdown 转换为 Word，PDF 文档，效果甚佳)。Note: Typora 可以自己去官网下载最新版： 
  - [Typora](https://www.typora.io/) &rarr; [使用说明](https://sspai.com/post/54912)
  - [VS Code](https://code.visualstudio.com/) &rarr; [教程1](https://www.cnblogs.com/clwydjgs/p/10078065.html)；[教程2](https://www.cnblogs.com/qianguyihao/archive/2019/04/18/10732375.html) 
- **mathpix**: 快速图片转公式软件，亦可以整页识别 PDF 文档。[Mathpix介绍](https://blog.csdn.net/qq_34243930/article/details/89158366)
- **Snipaste**: 截图-贴图软件，配合 PicGo 使用。[官网介绍](https://zh.snipaste.com/)；[Snipaste简介](https://baijiahao.baidu.com/s?id=1654041827952660862&wfr=spider&for=pc)
- **PicGo**: 图片上传到 lianxh.cn 图床的图片管理软件。[PicGo简介](https://www.jianshu.com/p/9d91355e8418)；[PicGo+Typora](https://blog.csdn.net/bruce_6/article/details/104821531)。图床使用参见我在群里分享的【连享会成员-PicGo图床设定和Markdown写作建议.pdf】。


#### 5.2.1 Snipaste 截图软件

它比微信自带的截图工具好用很多，还可以贴图。[这里](https://zhuanlan.zhihu.com/p/33013847) 和 [Here](https://zhuanlan.zhihu.com/p/99782135) 有介绍，还有 [哔哩哔哩-Picgo视频](https://www.bilibili.com/video/av60226929?from=search&seid=13187951187395386131)。大家如果有兴趣，可以在我的百度网盘下载：
  - 链接：https://pan.baidu.com/s/1FTC-Tvp4qnwR-gyscyDnJA ； 提取码：p5ci 

#### 5.2.2 Typora 和 VSCode 编辑器

我之前经常用这个编辑器，想爱你在主要用 VSCode，但 Typora 有一些独特的功能，知乎和简书上有很多相关介绍：
- **Word/网页 文字转换为 Markdown。** 写作过程中，直接复制网页或 Word 文档的内容，贴入 Typora 编辑器，即可瞬间转换为 Markdown 格式。
- 用 **Typora+Pandoc** 可以把 Markdown 转成 Word，PDF 等格式。
- [Typora 下载](https://www.typora.io/) &rarr; [使用说明](https://sspai.com/post/54912)
  - [VS Code](https://code.visualstudio.com/) &rarr; [教程1](https://www.cnblogs.com/clwydjgs/p/10078065.html)；[教程2](https://www.cnblogs.com/qianguyihao/archive/2019/04/18/10732375.html) 

#### 5.2.3 Mathpix 数学公式识别神器 

 **Mathpix**：将图片格式的数学公式快速识别为 LaTeX 格式。参见 [知乎-Mathpix](https://zhuanlan.zhihu.com/p/63918634)。

- 链接：https://pan.baidu.com/s/1cSDPmMfbBAxI6uUgOY7OoA ; 提取码：3fyd  

后文中可能提到的软件都已经放置在了我的百度云盘上，下载链接如下：

> 链接：https://pan.baidu.com/s/1sQZwAnfWCIX9HnjwEiBsYQ      
> 提取码：at5n

#### 5.2.4 手写批注

这部分功能大家可能暂时用不到，主要是讲课和网络讨论时使用。

[Word, PowerPoint, PDF 手写批注功能全解](https://www.lianxh.cn/news/8ec9b1b55d3ba.html) 文中提到的所有的软件都已经放置在了上述百度云盘【手写批注_软件】文件夹中，包括：
- **PDF Annotator**：PDF 浏览，手写批注等；
- **Sai2**：手绘和电子板书，参见 [Sai2：手写板书软件](https://www.lianxh.cn/news/d1b727a1abe15.html)
- **gInk**：可以在任何屏幕上随意手写板书，并保存为图片。参见 [gInk：一款好用的屏幕标注写画软件](https://www.lianxh.cn/news/3df0d01ba7fee.html)
- **ZoomIt**：屏幕缩放，适合讲解软件操作。参见 [ZoomIt 4.5 屏幕缩放标注小软件](https://www.lianxh.cn/news/fe39ea9816747.html)


&emsp;

> Stata连享会 &ensp;   [主页](https://www.lianxh.cn/news/46917f1076104.html)  || [视频](http://lianxh.duanshu.com) || [推文](https://www.zhihu.com/people/arlionn/) 

![](https://images.gitee.com/uploads/images/2020/0511/145749_fd9d1e40_1522177.png)
> 扫码查看连享会最新专题、公开课视频和 100 多个码云计量仓库链接。
