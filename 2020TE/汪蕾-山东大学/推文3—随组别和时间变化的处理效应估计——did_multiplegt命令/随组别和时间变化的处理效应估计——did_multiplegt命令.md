# 随组别和时间变化的处理效应估计——did_multiplegt命令

估计处理效应的常用方法是比较随着时间变化，不同组在接受处理前后的不同。体现在实际操作上就是控制组别和时间固定效应后进行回归。当处理效应不随组别和时间变化时，这样的回归操作确实可以用来估计处理效应。但处理效应很难保证不随组别和时间变化，使得前述估计方法并不稳健。例如，最低工资法对就业的影响会随所在州的不同而不同，也有可能随时间发生变化。基于此，本文介绍did_multiplegt命令，对组别和时间变化的处理效应估计。

命令语法如下：

> did_multiplegt Y G T D [if] [in] [, options]
> options：
> placebo(#)
> dynamic(#) 
> controls(varlist)
> trends_nonparam(varlist) 
> trends_lin(varlist) 
     weight(varlist) 
     breps(#)
     cluster(varname) 
     covariances 
     average_effect(string) 
     recat_treatment(varlist)
     threshold_stable_treatment(#)
     save_results(path) 



各个选项含义如下：

* Y，必选项，被解释变量

* G，必选项，组别变量

* T，必选项，时间变量

* D，必选项，处理变量

* placebo(#) ，可选项，指定安慰剂检验的估计量个数，最大个数等于时期数-2。安慰剂检验的是处理组和控制组在处理之前的不同。

* dynamic(#)，可选项，指定要估计的动态处理效应的个数，最大个数等于时期数-2。

* controls(varlist)，可选项，指定控制变量

* trends_nonparam(varlist)，可选项，使估计中包含时间固定效应

* trends_lin(varlist)，可选项，使估计中包含该项时间趋势，该项不能与trends_nonparam(varlist)同时进行设定。

* weight(varlist)，可选项，指定赋权变量。例如，当处理区域的面板数据时，如果想用每个区域*年份的人口数作为权重，则指定weight(population)。

* breps(#)，可选项，指定自举法重复次数。

* cluster(varname)，可选项，使用block bootstrap估计标准误。

* covariances，可选项，如果该项和 breps(#)被设定，stata会计算即时处理效应和动态处理效应之间的协方差。

* average_effect(string)，可选项，计算即时处理效应和动态处理效应的平均数，可选设定为average_effect(simple)或average_effect(prop_number_switchers)。该项被设定时，covariances要需要设定。

* recat_treatment(varlist) ，可选项，当处理变量的值较多的情况下使用该项。例如，当处理变量D取值为{0,1,2,3,4}，但接受处理2的个体很少。可能导致出现，在对比时有接受从处理2到处理3的处理组，但没有始终接受2的控制组。为了避免浪费样本，可以创建一个变量D_recat，令值在D=1或D=2时相同，然后设定recat_treatment(D_recat)。这样形成的结果是，用始终接受处理1的控制组，与接受处理2到处理3的处理组进行对比。

* threshold_stable_treatment(#)，可选项，当处理变量的值较多或处理变量是连续变量时。例如，处理区域面板数据时，将降雨量作为处理变量，但可能找不到哪一个区域的当年降雨量正好跟以前的相同。此时定义 threshold_stable_treatment(#)，#是正实数，stata会将小于#的作为控制组。

* save_results(path)，可选项，保存估计量、标准误、95%置信区间和样本数。

操作示例：

```stata
bcuse wagepan //*调用数据
did_multiplegt lwage nr year union, placebo(1) breps(50) cluster(nr)
```

结果如下：
```stata
This command does not produce a table, but all the estimators you have requested and their standard errors
are stored as eclass objects. Please type ereturn list to see them.
If the breps option was specified, the command also produces a graph, with all the point estimates
and their 95% confidence intervals. To change some features of the graph, open the adofile and search
for twoway. That will lead you to the line of code that produces the graph. You can modify  that line
to produce the graph you would like.
```
进一步操作
```stata 
ereturn list
```
结果如下：
```stata 
scalars:
           e(effect_0) =  .0261225951945861
        e(se_effect_0) =  .0194752588662298
         e(N_effect_0) =  3815
  e(N_switchers_effet_0) = 508
          e(placebo_1) =  .099321110943454
       e(se_placebo_1) =  .0315586535625052
        e(N_placebo_1) =  2842
```

