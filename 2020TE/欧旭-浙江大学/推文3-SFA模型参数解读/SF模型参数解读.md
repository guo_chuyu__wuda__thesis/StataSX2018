##### SFA 模型下的参数解读

噢，你就相当于说对于效率的那个估计我们有异质性的设定是吧，panel里边呢，也有不同的假设在里面，有一些是随着时间递增递减，还有一些加一堆时间虚拟变量的是吧？哎，对，这个你倒不用着急了，反正我上课的时候呢，我会对那些模型做一个非常完整的梳理，我自己之前也有写一些讲义啊，我有写一个书稿的一张就是这个随机边界的，那当然写着40多页还只是理论的部分，嗯，到时候我可以把那个书稿发给你，你可以看一下，然后呢，你的重点工作可能是在这个Stata实现这一块是吧，把这个相关的命令给补全了。

从 [Stata Journal list](https://www.lianxh.cn/news/12ffe67d8d8fb.html) 搜索 **Ctrl+F** 关键词 

- `help sfcross` //
- `help sfpanel` // 2015 Stata Journal
- `help sftfe` // Greene 2005 

1. 背景介绍

2. 模型介绍

### TE 测算方法
- `sfcross`， distribution(); 
- `help sfpanel` // model()
回头跟连老师要书稿，书稿里面有对这些模型的一个非常系统的介绍，然后再对着这个命令去做相应的私下实操的部分啊。

3. 参数梳理