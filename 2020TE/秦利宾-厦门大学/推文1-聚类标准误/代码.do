
*------------------------------------
*           聚类稳健标准误
*------------------------------------

*-调入数据
  *copy http://www.stata-press.com/data/r9/nlswork.dta nlswork.dta, replace
  use nlswork.dta, clear
  
*-定义全局暂元
  global x "age grade"
  
*------------一维聚类调整------------

/*
*-截面数据，在公司层面进行聚类，以下两种写法等价
  reg y x, cluster(id) 
  reg y x, vce(cluster id)  
  

*-面板数据，在公司层面进行聚类，以下三种写法等价
  xtset id year
  xtreg y x, fe cluster(id)  
  xtreg y x, fe vce(cluster id)
  xtreg y x, fe robust  // If you specify -xtreg, fe robust-, Stata will automatically, and without even telling you, use vce(cluster panel_variable) instead. (This is true since version 13.)
*/

*-回归结果
  reg ln_wage $x          //干扰项同方差
  est store m1
  reg ln_wage $x, robust  //干扰项异方差
  est store m2
  reg ln_wage $x, vce(cluster idcode)  
  est store m3

*------------二维聚类调整------------

*-cluster2 (Petersen-2009, RFS) 
  cluster2 ln_wage $x, fcluster(idcode) tcluster(year) 
  *该命令没有帮助文件，所有功能都可以用 cgmreg 和 vce2way 代替
  *因此，建议日后不必使用该命令

*-cgmreg (CGM2011, Mitchell Petersen's -cluster2.ado- 的升级版)
  *需手动下载：https://sites.google.com/site/judsoncaskey/data
  *help cgmreg  
  cgmreg ln_wage $x, cluster(idcode year)	
  est store m4

*-vce2way (CGM2011, 支持 Panel data, xtreg 等命令)
  *ssc install vce2way
  *help vce2way
  vce2way reg ln_wage $x, cluster(idcode year)	
  est store m5
  
*-vcemway (Gu and Yoo-2019, 该命令在 vce2way 的基础上扩展到多维)
  *ssc install vcemway
  *help vcemway
   vcemway reg ln_wage $x, cluster(idcode year)	
   est store m6
  
*-结果对比
  local m  "m1 m2 m3 m4 m5 m6"
  local mt "OLS Robust 1Clus 2_cgmreg 2_vce2way 2_vcemway"
  esttab `m', mtitle(`mt') nogap b(%4.3f) se(%6.4f) brackets ///
	          star(* 0.1 ** 0.05 *** 0.01) s(N r2) compress
			  
*---------手工计算二维聚类标准误----------

*-分别进行 idcode、year、id_year 一维聚类
  *在 idcode 维度进行一维聚类
  reg ln_wage $x, cluster(idcode)
  est store m1
  
  *在 year 维度进行一维聚类
  reg ln_wage $x, cluster(year)
  est store m2
  
  *在 idcode 和 year 交互维度进行一维聚类
  egen id_year = group(idcode year)
  reg ln_wage $x, cluster(id_year)
  est store m3

*-异方差调整
  reg ln_wage $x, robust
  est store m4

*-vcemway 计算
  vcemway reg ln_wage $x, cluster(idcode year)
  est store m5

*-结果对比
  local m  "m1 m2 m3 m4 m5"
  local mt "Clu_id Clu_year Clu_id_year Robust 2_vcemway"
  esttab `m', mtitle(`mt') nogap b(%4.3f) se(%6.4f) brackets ///
	          star(* 0.1 ** 0.05 *** 0.01) s(N r2) compress
			  
*-手工计算二维聚类标准误
  est restore m1
  scalar se_idcode  = _se[age]
  est restore m2
  scalar se_year    = _se[age]
  est restore m3
  scalar se_id_year = _se[age]
  est restore m4
  scalar se_robust = _se[age]
  scalar se_2way1 = sqrt(se_idcode^2+se_year^2-se_id_year^2)
  scalar se_2way2 = sqrt(se_idcode^2+se_year^2-se_robust^2)
  scalar list se_2way1 se_2way1
  