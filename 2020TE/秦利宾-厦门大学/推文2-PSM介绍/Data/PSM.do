
*----------PSM处理----------

*-调入数据
  use 数据.dta, clear

*-定义全局暂元
  global indepvar LNASSET LEV ROA GROWTH BM AGE 
  global fixvar   i.indcode i.year
  
*-样本匹配
  probit BIG4 $indepvar $fixvar, vce(cluster stkcd) 
  est store Probit
  predict pscore, p 
  psmatch2 BIG4, pscore(pscore) outcome(ABSACC RESTATE) ///
           common n(2)  cal(0.03)                 //详见help文件
  pstest $indepvar, both graph
  psgraph
  
*-回归结果
  
  /*
  PSM后，OLS回归权重设置，请参考以下链接：
  https://www.statalist.org/forums/forum/general-stata-discussion/general/487358-weights-included-in-regression-after-psmatch2
  https://www.statalist.org/forums/forum/general-stata-discussion/general/1445432-psmatch2-and-fweight-option-of-regress
  https://www.ssc.wisc.edu/sscc/pubs/stata_psmatch.htm
  其实，二者并没有本质区别，只不过 fweight 需要 _weight 为整数。
  在非 1:1 情况下，_weight 需要乘以相应匹配个数调整。如，在 1:2 情况下，_weight 要乘以 2。
  */
  
  *-Full Sample ABSACC
  reg ABSACC BIG4 $indepvar $fixvar, cluster(stkcd)
  est store ABSACC_F
  *-Matched Sample ABSACC
  reg ABSACC BIG4 $indepvar $fixvar [pweight=_weight], cluster(stkcd)
  est store ABSACC_M
  *-Full Sample RESTATE
  reg RESTATE BIG4 $indepvar $fixvar, cluster(stkcd)
  est store RESTATE_F
  *-Matched Sample RESTATE
  reg RESTATE BIG4 $indepvar $fixvar [pweight=_weight], cluster(stkcd)
  est store RESTATE_M

*-结果对比
  local m "Probit ABSACC_F ABSACC_M RESTATE_F RESTATE_M"
  esttab `m', mtitle(`m') b(%6.3f) nogap drop(*.indcode *.year)  ///
	     order(BIG4) s(N r2_p r2_a) star(* 0.1 ** 0.05 *** 0.01)


