&emsp; 

> **作者：** 张翠燕（山东大学）  
> **邮箱：** <zcy1992yan@163.com>

&emsp;

## 1. 问题背景

Wind 金融终端数据库中有很多相比其他数据库而言更为精细的数据指标，例如上市公司控股或参股公司信息，其中包含了各年各个参控公司对上市公司的 **直接持股比例**、**主营业务**、**成立时间** 等比较详细的数据。

遗憾的是，Wind 数据库的使用并不友好：深度数据库中的很多指标无法实现一次性批量下载，只能依据上市公司代码逐一下载，即一家公司的数据下载为一个 Excel 表格。如此一来，若需下载所有上市公司的数据，需要执行三千多次下载任务，耗时耗力。

虽然Python可以有效替代人工下载，但Wind数据库中存在反爬虫机制，对于很多初学者而言是不小的障碍。若是强制反爬，一不留神还可能面临被封号的风险。值得庆幸的是，Python中的pyautogui安装包可以帮我们轻松规避反爬虫问题，还能解决人工下载三千多次的繁琐问题。其原理是模拟人工鼠标操作行为，是一种「傻瓜机」式的操作。设置好后，就可轻松实现解放双手啦。本文旨在为科研者提供一种减少重复劳动、解放双手的思路。

## 2. 准备工作

- **Wind** 数据库账号
- **Anaconda3** 以上版本，利用其自带的 **Jupyter Notebook** 编辑器编写代码
- 一份完整的 **上市公司代码列表**：可直接从 Wind 数据库中下载

## 3. 简单介绍

#### Wind数据库中诸如控股或参股公司该类指标在哪？

打开wind终端**首页**，定位到**多维数据**，进而点击**深度数据**，深度数据下有关于公司资料、知识产权等详细目录，可根据研究需要下载，本文以**控股或参股公司数据**为例。注意：*将**时间范围**改为**上市以来**或是**10Y**，保证时间的统一性。若选择**上市以来**部分证券代码会存在没有数据的情况，则需要将证券代码进行逐步剔除过程，即下载成功的从excel表格中删除*。
   ![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/张翠燕Wind数据库基本信息.png)

#### 本文示例中用到的pyautogui包中相关函数的详细介绍 

```python
  pyautogui.position()  ## get the XY position of the mouse
  pyautogui.click(x,y)  ## move the mouse to XY coordinates and click it
  pyautogui.typewrite ((List["证券代码"][i])  ## typewrite the content of list
  pyautogui.press('enter')  ## press the Enter key
  pyautogui.hotkey('altleft','f4')  ## press the Altleft-F4 hotkey combination
```

#### 获取上市公司股票代码列表  

从wind中下载一份完整的上市公司代码列表数据，保存到一个文件夹中，注意存储路径，本示例中的路径为 **D:\company code.xlsx**。数据结构如图所示：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/张翠燕Wind数据库上市公司代码列表.png)

## 4. 代码实现过程  

```python
##导入模块
import time, sys, smtplib
import pandas as pd
import pyautogui
```

```python
##测试鼠标位置:pyautogui依据不同电脑屏幕分辨率获取屏幕上（x,y)坐标
print(pyautogui.position())
```

**测试鼠标位置**是实现整个过程的**核心**，基本原理是在Jupyter中执行完导入模块后，依据人工手动下载的过程确定鼠标位置，并记录位置2、3、4的坐标，为了减少bug整个过程中Wind数据库切记要保持最大化状态,详解如图所示：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/张翠燕Wind数据运行测试位置说明.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/张翠燕Wind数据运行测试位置4说明.png)

确认2、3、4位置后可将其坐标写入**过程实现**中的对应位置

```python
##导入证券代码
List=pd.read_excel(r"D:\company code.xlsx")
for i in range(0, len(List)):
    print(List["证券代码"][i])
```

```python
##过程实现
for i in range(0, len(List)):
    time.sleep(5)
    pyautogui.click(1157, 715) ##位置2的坐标
    time.sleep(1) ##停顿时间依据自己电脑反应速度修改
    pyautogui.typewrite(List["证券代码"][i])
    time.sleep(1)
    pyautogui.press('enter')
    time.sleep(10)
    pyautogui.click(1243, 104) ##位置3的坐标
    time.sleep(5)
    pyautogui.click(604, 477) ##位置4的坐标
    time.sleep(1)
    l1=List["证券代码"][i].replace('.','')
    pyautogui.typewrite(l1)
    time.sleep(1)
    pyautogui.press('enter')   
    time.sleep(3)
    pyautogui.hotkey('altleft','f4') ##由于Wind中下载的excel会自动打开，因而需利用Alt+F4快捷键将其关闭
```

最后，执行过程实现部分的方法如图所示：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/张翠燕Wind数据最后一部分运行说明.png)

完成上述操作后，最终将获取一份包含所有上市公司的控股或参股公司数据的excel文件夹，每一个excel里包含一家上市公司控股或参股公司详细的**名称**、**直接持股比例**、**表决权比例**、**被参控公司注册资本**、**投资额**等数据。结果如下：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Wind数据下载结果部分展示.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/张翠燕Wind控股参股公司数据详情.png)