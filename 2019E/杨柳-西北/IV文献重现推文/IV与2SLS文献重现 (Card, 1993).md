介绍一篇使用工具变量 (是否在大学附近成长) 与 2SLS 法研究教育回报率的文献案例 (Card, 1993) 。该文献的信息如下：
> Card (1993). Using geographic variation in college proximity to estimate the return to schooling, NBER Working Paper Series, Working Paper No.4483. [ [PDF 下载](https://www.nber.org/papers/w4483.pdf) ]

## 1. 描述性统计分析
该篇文献使用的数据来源于美国 1966 至 1981 年间对青年男性的纵向调查数据 (NLSYM)，可在 [连享会码云](****) 中下载 `Card1993_schooling.dta` 数据。对调查样本的描述性统计结果见下图所示。
![Card_1993_schooling_Fig01](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Card_1993_schooling_Fig01.bmp)

> 统计结果显示：
> - NLSYM 样本不是随机的对美国人口进行抽样的结果。NLSYM 样本中包含的位于南部地区的男性和黑人男性的比例较大。
1976 年的子样本的统计结果显示：
> - 41% 的男性居住在南部地区；64%-65% 的男性居住在标准都市区统计区域 (SMSA)；67%-68% 的男性在1966 年居住在 4 年制大学附近。
> - 在家庭组成方面，78%-79% 的被调查者在 14 岁时与父母同住在一起；10% 仅与母亲住在一起；其余 11%-12% 与亲戚或继父继母同住在一起。
> - 父亲与母亲的平均受教育年限为 10 年 ( 22% 与 11% 的样本分别缺少父亲与母亲的受教育年限，将缺少的数据用父母受教育水平的平均值进行替代)。
> - 1966 年的访问还调查了世界知识测试得分，这项测试在以前研究教育回报率的文献中被用来度量能力。
> - 被调查者在 1976 年的平均受教育年限是 13.2 年。其中，1/3 的样本报告了 12 年的受教育水平；23% 的样本报告有上大学；27% 的样本报告了大于 16 年的受教育水平。

由于缺少整个样本数据 (NLSYM)，我们仅统计了上图中第 2 列和第 3
 列在 1976 年的子样本数据。Stata 中的命令如下所示。
```Stata
 ********************************************************************
 *        文献案例
 /*card(1993), Using geographic variation in college proximity to estimate 
  the return to schooling, NBER Working Paper Series, Working Paper No.4483
 */
 *******************************************************************
 
 use "D:\stata15\ado\personal\IV_2SLS\Data\Card1993_schooling.dta", clear
  
 ****************************
 *  Table 1
 ****************************
 
 *-row 1
 tab age66 if ((age66 == 14) | (age66 == 15)) & (ed76 != .)
 scalar percent_age14_15 = r(N)/_N  //r(N):r-return，引用与模型估计无关命令的结果；_N是单值(scalar)，是样本量的数值
 dis percent_age14_15
 tab age66 if ((age66 == 16) | (age66 == 17)) & (ed76 != .)
 scalar percent_age16_17 = r(N)/_N
 dis percent_age16_17
 tab age66 if ((age66 == 18) | (age66 == 19) | (age66 == 20)) & (ed76 != .)
 scalar percent_age18_20 = r(N)/_N
 dis percent_age18_20
 tab age66 if ((age66 == 21) | (age66 == 22) | (age66 == 23) | (age66 == 24)) & (ed76 != .)
 scalar percent_age21_24 = r(N)/_N
 dis percent_age21_24
 
 mat percent_age66 = (percent_age14_15 \ percent_age16_17 \ percent_age18_20 \ percent_age21_24) //手动输入矩阵元素
 mat list percent_age66  //在屏幕上列出矩阵
 
 count if lwage76 != .
 tab age66 if ((age66 == 14) | (age66 == 15)) & (ed76 != .) & (lwage76 != .)
 scalar percent_age14_15_2 = r(N)/3010
 dis percent_age14_15_2
 tab age66 if ((age66 == 16) | (age66 == 17)) & (ed76 != .) & (lwage76 != .)
 scalar percent_age16_17_2 = r(N)/3010
 dis percent_age16_17_2
 tab age66 if ((age66 == 18) | (age66 == 19) | (age66 == 20)) & (ed76 != .) & (lwage76 != .)
 scalar percent_age18_20_2 = r(N)/3010
 dis percent_age18_20_2
 tab age66 if ((age66 == 21) | (age66 == 22) | (age66 == 23) | (age66 == 24)) & (ed76 != .) & (lwage76 != .)
 scalar percent_age21_24_2 = r(N)/3010
 dis percent_age21_24_2
 
 mat percent_age66_2 = (percent_age14_15_2 \ percent_age16_17_2 \ percent_age18_20_2 \ percent_age21_24_2)
 mat list percent_age66_2
  
 *-row 2 (注：只有south变量有标签，其余地区变量没有标签)
 count if (south66 == 1) & (ed76 != .)
 dis "percent_south66 =" r(N)/_N
 count if lwage76 != .
 count if (south66 == 1) & (ed76 != .) & (lwage76 != .)
 dis "percent_south66_2 =" r(N)/3010
 
 *-row 3
 count if (smsa66r == 1) & (ed76 != .)
 dis "percent_smsa66r =" r(N)/_N
 count if lwage76 != .
 count if (smsa66r == 1) & (ed76 != .) & (lwage76 != .)
 dis "percent_smsa66r_2 =" r(N)/3010
 
 *-row 4
 count if (nearc4 == 1) & (ed76 != .)
 dis "percent_nearc4 =" r(N)/_N
 count if lwage76 != .
 count if (nearc4 == 1) & (ed76 != .) & (lwage76 != .)
 dis "percent_nearc4_2 =" r(N)/3010 
 
 *-row 5
 count if (momdad14 == 1) & (ed76 != .)
 dis "percent_momdad14 =" r(N)/_N
 count if (sinmom14 == 1) & (ed76 != .)
 dis "percent_sinmom14 =" r(N)/_N
 count if lwage76 != .
 count if (momdad14 == 1) & (ed76 != .) & (lwage76 != .)
 dis "percent_momdad14_2 =" r(N)/3010
 count if (sinmom14 == 1) & (ed76 != .) & (lwage76 != .)
 dis "percent_sinmom14_2 =" r(N)/3010
 
 *-row 6
 sum momed if (ed76 != .)
 sum momed if (ed76 != .) & (lwage76 != .)
 sum daded if (ed76 != .)
 sum daded if (ed76 != .) & (lwage76 != .)
 
 *-row 7
 sum black if (ed76 != .)
 sum black if (ed76 != .) & (lwage76 != .)
 
 *-row 8
 sum kww if (ed76 != .)
 sum kww if (ed76 != .) & (lwage76 != .)
 
 *-row 9
 count if (ed76 != .)
 dis r(N)/_N
 count if (ed76 != .) & (lwage76 != .)
 dis r(N)/3010
 
 *-row 10
 sum ed76 if (ed76 != .)
 sum ed76 if (ed76 != .) & (lwage76 != .)
 
 *-row 11
 sum reg76r if (ed76 != .)
 sum reg76r if (ed76 != .) & (lwage76 != .)
 
 *-row 12
 count if (ed76 != .)
 count if (ed76 != .) & (lwage76 != .)
```
## 2. 使用 OLS 估计的结果
作者先使用了 OLS 估计方法研究教育回报率问题，建立了线性回归模型，在所有模型中都包括了线性的受教育水平连续型变量、工作年数及其二次项、种族哑变量、居住在南部地区与大都市区 (SMSA) 地区哑变量。回归结果如下图所示。

![Card_1993_schooling_Fig02](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Card_1993_schooling_Fig02.bmp)

> OLS 估计结果显示受教育年数的系数相当稳定，每增加一年教育，收入增加 7.3%-7.5%。

Stata 中的命令如下所示。
```Stata
 ****************************
 *  Table 2
 ****************************
 gen exp762_100 = exp762/100
 global dd "ed76 exp76 exp762_100 black reg76r smsa76r"
 reg lwage76 $dd if (ed76!=.) & (lwage76!=.)
 est store Table2_1
  
 global ee "reg661 reg662 reg663 reg664 reg665 reg666 reg667 reg668 reg669"
 reg lwage76 $dd $ee smsa66r if (ed76!=.) & (lwage76!=.)
 est store Table2_2
 
 global ff "momed daded nomomed nodaded"
 reg lwage76 $dd $ee smsa66r $ff if (ed76!=.) & (lwage76!=.)
 est store Table2_3
 
 global gg "f1 f2 f3 f4 f5 f6 f7 f8"
 reg lwage76 $dd $ee smsa66r $ff $gg if (ed76!=.) & (lwage76!=.)
 est store Table2_4
 
 reg lwage76 $dd $ee smsa66r $ff $gg momdad14 sinmom14 if (ed76!=.) & (lwage76!=.)
 est store Table2_5
 
 esttab Table2_1 Table2_2 Table2_3 Table2_4 Table2_5 ///
        using "D:\stata15\ado\personal\IV_2SLS\Results\card_1993_Table2.csv", ///
		nogap nobase noomit r2 se replace
```
> - 上述 OLS 结果可能会导致教育回报率的有偏估计，因为有许多原因会带来受教育年数与随机误差项相关。例如 **遗漏能力变量**、受教育年数存在 **测量误差** 。
> - 如果我们找到 **与受教育年数相关，但不与工资直接相关的变量** 作为受教育年数的 **工具变量**，就可以对 “真实” 的教育回报率进行 **一致估计** 。

## 3. 寻找 IV 
作者选取了 **是否在大学附近成长** 作为受教育年数的 **工具变量**。 **理由是：那些没有在大学附近成长的学生上大学的成本会较高。这些较高的成本会减少他们上大学，这个情况至少对于家庭收入低的学生是成立的** 。

为了证明这个说法，作者将整个样本按照 **受教育年数的预测值** 分成了四份（按照四分位数的值进行划分）。然后在这四份样本中，按照 **是否在大学附近成长** 进行 **分组计算**，分别计算这四份样本的 **受教育年数观察值的平均值**。计算结果如下图所示。

![Card_1993_schooling_Fig03](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Card_1993_schooling_Fig03.bmp)

其中，受教育年数预测值的计算过程是：使用未在大学附近成长的子样本建立线性回归方程，以 1976 年的受教育年数作为因变量，1966 年的地区和都市区哑变量、年龄、种族哑变量、表征家庭结构和父母教育水平的变量作为自变量。再根据上述回归方程中估计的系数值计算全样本的受教育年数的预测值。Stata 中的命令如下所示：

```Stata
 ****************************
 *  Figure 1
 ****************************
 
 *-仅使用当 nearc4==0 的子样本建立受教育年数回归方程
 preserve
 reg ed76 $ee age76 black momdad14 sinmom14 momed daded if nearc4 == 0
 restore
 
 *-按照上述回归方程的估计系数对全样本的受教育年数值进行预测
 predict ed76_hat, xb
 
 *-计算受教育年数预测值的四分位数，并分成四份子样本以quartile分类变量表示
 centile ed76_hat, centile(25,50,75,100)
 gen quartile = 0
 replace quartile = 1 if (ed76_hat <= 11.91084)
 replace quartile = 2 if (ed76_hat <= 12.96226) & (ed76_hat > 11.91084)
 replace quartile = 3 if (ed76_hat <= 13.89777) & (ed76_hat > 12.96226)
 replace quartile = 4 if (ed76_hat > 13.89777) & (ed76_hat != .)
 
 *-按照是否在大学附近成长分组计算四份子样本的受教育年数观察值的平均值
 sum ed76 if (nearc4 == 1) & (quartile == 1)  //bysort quartile: sum ed76 if (nearc4 == 1)
 scalar ed76_n_q1 = r(mean)
 sum ed76 if (nearc4 == 1) & (quartile == 2)
 scalar ed76_n_q2 = r(mean)
 sum ed76 if (nearc4 == 1) & (quartile == 3)
 scalar ed76_n_q3 = r(mean)
 sum ed76 if (nearc4 == 1) & (quartile == 4)
 scalar ed76_n_q4 = r(mean)
 
 sum ed76 if (nearc4 == 0) & (quartile == 1)  //bysort quartile: sum ed76 if (nearc4 == 0)
 scalar ed76_q1 = r(mean)
 sum ed76 if (nearc4 == 0) & (quartile == 2)
 scalar ed76_q2 = r(mean)
 sum ed76 if (nearc4 == 0) & (quartile == 3)
 scalar ed76_q3 = r(mean)
 sum ed76 if (nearc4 == 0) & (quartile == 4)
 scalar ed76_q4 = r(mean)
 
 *-将计算结果以矩阵的形式存储，列在屏幕上；
 *-矩阵第一列代表四份子样本的序号；
 *-第二列代表在大学附近成长样本的受教育年数平均值；
 *-第三列代表未在大学附近成长样本的受教育年数的平均值
 mat mean_ed76 = (1,ed76_n_q1,ed76_q1 \ 2,ed76_n_q2,ed76_q2 \ 3,ed76_n_q3,ed76_q3 \ 4,ed76_n_q4,ed76_q4)
 mat list mean_ed76
 
 *-将矩阵的第1至3列转换为变量，分别命名为mean_ed76_1，mean_ed76_2，mean_ed76_3
 svmat mean_ed76, names(mean_ed76_) 
 
 *-绘制折线图
 twoway line mean_ed76_2 mean_ed76_3 mean_ed76_1,      ///y1，y2与x的折线图：写的时候顺序为y1 y2 x
             ylabel(10(1)16) lpattern(solid dash)      ///y轴刻度；线型格式（第一种线型 第二种线型）
			 lcolor(black b)                           ///线的颜色（第一种线的颜色 第二种线的颜色）
			 xtitle("Quartile of Predicted Education") ///x轴标题
			 ytitle("Mean Years Education")            ///y轴标题 
			 title("Mean Years of Eduction By Quartile of Predicted Education")  ///图的标题
			 legend(label(1 "Colledge Nearby") label(2 "No Colledge Nearby"))    //两种线型的图例说明
```
> - 从上图的计算结果可以看到，在每一个受教育年数预测值的四分位数上，在大学附近成长的样本的受教育年数都大于未在大学附近成长的样本。
> - 在三个较高的受教育年数预测值分位数上，在是否与大学邻近的样本之间，受教育年数的差别较小，相差 0.2-0.4 年。
> - 在最低的受教育年数预测值分位数上，是否与大学邻近的受教育年数的差值为 1.1 年。
> - 如所预计的，在大学附近成长对那些不倾向于继续上学的男性（例如单亲家庭、父母受教育年数少、在南部农村地区）的影响是较大的。

## 4. 使用 2SLS 法估计结果
使用 **是否在大学附近成长** 作为受教育年数的 **工具变量**，并用 2SLS 法建立回归方程。下表中呈现了一系列约减形式的受教育年数和工资方程，以及工资的结构方程。
- 第 1 列与第 2 列显示了受教育年数对是否与大学邻近的回归结果。
- 第 3 列与第 4 列显示了去掉受教育年数之后的工资的约减方程的回归结果。
- 第 5 列与第 6 列报告了使用 IV 的教育回报率的估计值（可以用是否在大学附近成长在工资约减方程中的系数值除以在受教育年数方程中的系数值计算）。
- 下表中 Panel A 部分将工作年数及其平方项视为外生变量，Panel B 中将工作年数及其平方项视为内生变量，认为若受教育年数有测量误差，则工作年数也有测量误差，并使用年龄及其平方项作为工作年数及其平方项的工具变量。

![Card_1993_schooling_Fig04](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Card_1993_schooling_Fig04.bmp)

Stata 中的命令如下所示：
（注：在下述的 Stata 命令中，由于年龄及其平方项导致了严重共线性问题，Stata自动删除了年龄变量，造成方程缺少足够的工具变量、无法识别方程，因此用取对数之后的年龄值及其平方项作为工具变量）

```Stata
 ****************************
 *  Table 3
 ****************************
 *-Panel A
 global hh "black reg76r smsa76 smsa66r"
 global ii "daded momed famed nodaded nomomed f1 f2 f3 f4 f5 f6 f7 f8 momdad14 sinmom14 step14"
 
 reg ed76 nearc4 exp76 exp762 $hh $ee if (ed76!=.) & (lwage76!=.)
 est store Table3_A_1
 
 reg ed76 nearc4 exp76 exp762 $hh $ee $ii if (ed76!=.) & (lwage76!=.)
 est store Table3_A_2
 
 reg lwage76 nearc4 exp76 exp762 $hh $ee if (ed76!=.) & (lwage76!=.)
 est store Table3_A_3
 
 reg lwage76 nearc4 exp76 exp762 $hh $ee $ii if (ed76!=.) & (lwage76!=.)
 est store Table3_A_4
 
 ivregress 2sls lwage76 exp76 exp762 $hh $ee (ed76 = nearc4) if (ed76!=.) & (lwage76!=.)
 est store Table3_A_5
 
 ivregress 2sls lwage76 exp76 exp762 $hh $ee $ii (ed76 = nearc4) if (ed76!=.) & (lwage76!=.)
 est store Table3_A_6

 esttab Table3_A_1 Table3_A_2 Table3_A_3 Table3_A_4 Table3_A_5 Table3_A_6     ///
        using "D:\stata15\ado\personal\IV_2SLS\Results\card_1993_Table3_A.csv", ///
		nogap nobase noomit r2 se replace

 *-Panel B
 ivregress 2sls ed76 nearc4 $hh $ee (exp76 exp762 = age76 age76sq) if (ed76!=.) & (lwage76!=.)
 est store Table3_B_1
 
 ivregress 2sls ed76 nearc4 $hh $ee $ii (exp76 exp762 = age76 age76sq) if (ed76!=.) & (lwage76!=.)
 est store Table3_B_2
 
 ivregress 2sls lwage76 nearc4 $hh $ee (exp76 exp762 = age76 age76sq) if (ed76!=.) & (lwage76!=.)
 est store Table3_B_3
 
 ivregress 2sls lwage76 nearc4 $hh $ee $ii (exp76 exp762 = age76 age76sq) if (ed76!=.) & (lwage76!=.)
 est store Table3_B_4
 
 *-由于age76和age76sq的相关系数达到了0.9988
 *-用这两个变量作为exp76和exp762的工具变量时，会有高度共线性问题
 *-stata会默认删除age76变量，则工具变量的个数小于内生变量，方程无法识别，无法进行2SLS回归
 *-替代的用ln(age76)和[ln(age76)]^2作为工具变量，不会报错存在共线性问题，但它们的相关系数高达0.9999？
 
 gen lage76sq = ln(age76)*ln(age76)
 gen lage76 = ln(age76)
 corr age76 age76sq
 corr lage76 lage76sq
 
 ivregress 2sls lwage76 $hh $ee (ed76 exp76 exp762 = nearc4 lage76 lage76sq) if (ed76!=.) & (lwage76!=.)
 est store Table3_B_5
 
 ivregress 2sls lwage76 $hh $ee $ii (ed76 exp76 exp762 = nearc4 lage76 lage76sq) if (ed76!=.) & (lwage76!=.)
 est store Table3_B_6

 esttab Table3_B_1 Table3_B_2 Table3_B_3 Table3_B_4 Table3_B_5 Table3_B_6     ///
        using "D:\stata15\ado\personal\IV_2SLS\Results\card_1993_Table3_B.csv", ///
		nogap nobase noomit r2 se replace
```

> - 从上表的结果中可以看到，不论模型中是否包括家庭背景方面的变量，不论将工作年数及其平方项视为外生或内生变量，教育回报率的结果是十分相近的，估计值介于 0.12-0.14 之间。这些值比 OLS 估计结果高出 50-60%，与已有的一些文献结果是类似的。但是，IV 估计的标准误较大，**？？？我们不能拒绝这个假设：IV 与 OLS 估计值的差别是由抽样误差带来的。？？？**
> - 在大学附近成长对受教育年数具有正效应（增加 0.32-0.38 年），**？？？对工资也具有正效应（增加 4.2%-4.8%）？？？是因为模型中没有受教育年数变量？？？**。

接下来，作者对上述结果的稳健性进行了一些列检验。结果显示在下表中。Stata 中的命令如下所示：

![Card_1993_schooling_Fig05](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Card_1993_schooling_Fig05.bmp)

```Stata
 ****************************
 *  Table 4
 ****************************
 *-row 1
 reg lwage76 ed76 exp76 exp762 $hh $ee $ii if (ed76!=.) & (lwage76!=.)
 est store Table4_OLS_1
 
 ivregress 2sls lwage76 $hh $ee $ii (ed76 exp76 exp762 = nearc4 lage76 lage76sq) if (ed76!=.) & (lwage76!=.)
 est store Table4_IV_1
 
 *-row 2 系数与文献不一致，原因可能是没有用78年的教育数据，此数据表中没有该数据
 reg lwage78 ed76 exp76 exp762 $hh $ee $ii if (ed76!=.) & (lwage78!=.)
 est store Table4_OLS_2
 
 ivregress 2sls lwage78 $hh $ee $ii (ed76 exp76 exp762 = nearc4 lage76 lage76sq)  if (ed76!=.) & (lwage78!=.)
 est store Table4_IV_2
 
 *-row 3
 reg lwage76 ed76 exp76 exp762 $hh $ee $ii kww if (ed76!=.) & (lwage76!=.) & (kww!=.)
 est store Table4_OLS_3
 
 ivregress 2sls lwage76 $hh $ee $ii kww (ed76 exp76 exp762 = nearc4 lage76 lage76sq) if (ed76!=.) & (lwage76!=.) & (kww!=.)
 est store Table4_IV_3
 
 *-row 4 
 reg lwage76 ed76 exp76 exp762 $hh $ee $ii kww if (ed76!=.) & (lwage76!=.) & (kww!=.) & (iq!=.)
 est store Table4_OLS_4
 
 ivregress 2sls lwage76 $hh $ee $ii (ed76 exp76 exp762 kww = nearc4 lage76 lage76sq iq) if (ed76!=.) & (lwage76!=.) & (kww!=.) & (iq!=.)
 est store Table4_IV_4
 
 *-row 5
 reg lwage76 ed76 exp76 exp762 $hh $ee $ii if (ed76!=.) & (lwage76!=.)
 est store Table4_OLS_5
 
 /*
 reg ed76 nearc4a exp76 exp762 $hh $ee $ii if (ed76!=.) & (lwage76!=.)    // nearc4a对ed76的边际效应为0.309年
 reg ed76 nearc4 exp76 exp762 $hh $ee $ii if (ed76!=.) & (lwage76!=.)     // nearc4对ed76的边际效应为0.322年
 reg lwage76 nearc4a exp76 exp762 $hh $ee $ii if (ed76!=.) & (lwage76!=.) // nearc4a对wage76的边际效应为6.16%，6.16%/0.309=1.994
 reg lwage76 nearc4 exp76 exp762 $hh $ee $ii if (ed76!=.) & (lwage76!=.)  // nearc4对wage76的边际效应为4.52%，4.52%/0.322=1.404
 */
 
 ivregress 2sls lwage76 $hh $ee $ii (ed76 exp76 exp762 = nearc4a lage76 lage76sq) if (ed76!=.) & (lwage76!=.)
 est store Table4_IV_5
 
 *-row 6
 ivregress 2sls lwage76 exp76 exp762 $hh $ee $ii (ed76 = nearc4 nearc2 ) if (ed76!=.) & (lwage76!=.)
 est store Table4_OLS_6
 
 ivregress 2sls lwage76 $hh $ee $ii (ed76 exp76 exp762 = nearc4 nearc2 lage76 lage76sq) if (ed76!=.) & (lwage76!=.)
 est store Table4_IV_6
 
 *-row 7 
 reg lwage76 ed76 exp76 exp762 $hh $ee $ii if (ed76!=.) & (lwage76!=.) & (age66 <=19) & (age66 >= 14)
 est store Table4_OLS_7
 
 gen lage66 = ln(age66)
 gen lage66sq = ln(age66)*ln(age66)
 ivregress 2sls lwage76 $hh $ee $ii (ed76 exp76 exp762 = nearc4 lage66 lage66sq) if (ed76!=.) & (lwage76!=.) & (age66 <=19) & (age66 >= 14)
 est store Table4_IV_7
 
 
 esttab Table4_OLS_1 Table4_OLS_2 Table4_OLS_3 Table4_OLS_4 Table4_OLS_5 Table4_OLS_6 Table4_OLS_7    ///
        using "D:\stata15\ado\personal\IV_2SLS\Results\card_1993_Table4_OLS.csv", ///
		nogap nobase noomit r2 se replace
 
 esttab Table4_IV_1 Table4_IV_2 Table4_IV_3 Table4_IV_4 Table4_IV_5 Table4_IV_6 Table4_IV_7    ///
        using "D:\stata15\ado\personal\IV_2SLS\Results\card_1993_Table4_IV.csv", ///
		nogap nobase noomit r2 se replace
```

> - 上表中第 1 行的结果来自于之前进行了 **基本设定** 的 **OLS** 与 **IV** 估计结果。
> - 第 2 行报告了 **使用 1978 年工资数据** 进行 OLS 与 IV 估计的结果。
> - 第 3 行报告了在回归模型中 **加入反映能力的变量——KWW 测试得分** 之后的 OLS 与 IV 估计结果。与第 1 行的基本结果相比较，加入 KWW 变量后，OLS 估计的教育回报率减少了 25%。一部分原因是由于受教育年数与 KWW 变量高度相关，然而，还有一些原因是由于受教育年数的测量误差。
> - 由于 KWW 变量也会存在测量误差，因此在第 4 行 IV 估计中，作者
 **将 KWW 与受教育年数都视为具有测量误差的内生变量**，选取 IQ 变量作为 KWW 的工具变量，进行 IV 估计。结果显示教育回报率下降并且不显著。
> - 第 5 行与第 6 行中，作者选取了 **不同的工具变量来度量是否在大学附近成长**。第 5 行使用是否在公办 4 年制大学附近成长作为 IV，结果显示教育回报率比进行基本设定时略大。第 6 行使用两个工具变量，一个为是否在 2 年制大学附近成长，另一个为是否在 4 年制大学附近成长。结果显示教育回报率为 12%，相比基本设定模型，标准误略微变小。
> - 是否在大学附近成长可能存在测量误差的地方在于 1966 年调查时，被调查者并不一定是做出上大学决策时的 18 岁或 19 岁。因此，作者在第 7 行 **使用 1966 年调查的子样本（14-19 岁）** 进行 OLS
 与 IV 估计。结果显示 OLS 回归结果与基本设定模型相近似，IV 估计结果比基本设定模型小一些。
> - 上述这些设定检验了之前表 3 中的 **两个主要结论**：(1). 使用是否在大学附近成长的工具变量进行 **IV 估计结果普遍高于 OLS 估计结果**；(2). **高出 OLS 估计结果的范围介于 25-60% 之间** 。

文献中的图 1 可以证明，是否在大学附近成长 **对于家庭受教育背景较差的学生** 的教育回报率的影响是较大的。鉴于此，作者选取 **在大学附近成长与家庭受教育背景较差的交乘项** 作为受教育年数的工具变量，并检验是否在大学附近成长与工资的相关性，建立受教育年数与工资的约减方程、工资的结构方程进行 OLS 与 IV 估计，结果如下表 5 所示。家庭受教育背景较差变量的定义是：父亲和母亲都没有高中毕业。Stata 中的命令如下所示：

![Card_1993_schooling_Fig06](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Card_1993_schooling_Fig06.bmp)

```Stata
 ****************************
 *   Table 5 
 ****************************
 
 *-colum 1  ？？系数与文献不一致？？
  gen md_less12 = 0  //生成父母教育年数都小于12年的变量
  replace md_less12 = 1 if (momed < 12) & (daded < 12)  //给变量赋值
  gen n_mdless12 = nearc4*md_less12  //生成交乘项：在大学附近成长与父母教育年数都小于12年
 
  ivregress 2sls ed76 nearc4 i.n_mdless12 $hh $ee $ff $ii (exp76 exp762 = lage76 lage76sq) 
  est store Table5_1
 
 *上一条的等价命令
  ivregress 2sls ed76 i.nearc4##md_less12 $hh $ee $ff $ii (exp76 exp762 = lage76 lage76sq) 
  ereturn list     //列出模型所有的估计结果
  matrix list e(b) //列出所有估计系数值，包括基准值与忽略组
 
 *-colum 2 
  ivregress 2sls lwage76 nearc4 i.n_mdless12 $hh $ee $ff $ii (exp76 exp762 = lage76 lage76sq)  
  est store Table5_2
 
 *上一条的等价命令
  ivregress 2sls lwage76 i.nearc4##i.md_less12 $hh $ee $ff $ii (exp76 exp762 = lage76 lage76sq) 
  matrix list e(b) //列出所有估计系数值，包括基准值与忽略组
 
 *-colum 3  ？？系数与文献不一致？？
 *-工具变量不能用因子变量的形式
 *-因此需要先生成新的交乘项变量
  ivregress 2sls lwage76 nearc4 $hh $ee $ff $ii (ed76 exp76 exp762 = n_mdless12 lage76 lage76sq)
  est store Table5_3
 
 
 *-colum 4
 *-工具变量不能用因子变量的形式
 *-因此需要先生成新的交乘项变量
  gen n_f1 = nearc4*f1  //生成交乘项：8类父母教育水平变量与在大学附近成长
  gen n_f2 = nearc4*f2
  gen n_f3 = nearc4*f3
  gen n_f4 = nearc4*f4
  gen n_f5 = nearc4*f5
  gen n_f6 = nearc4*f6
  gen n_f7 = nearc4*f7
  gen n_f8 = nearc4*f8
   
  ivregress 2sls lwage76 nearc4 $hh $ee $ff $ii (ed76 exp76 exp762 = n_f* lage76 lage76sq)
  est store Table5_4
 
  esttab Table5_1 Table5_2 Table5_3 Table5_4                                     ///
        using "D:\stata15\ado\personal\IV_2SLS\Results\card_1993_Table5.csv", ///
		nogap nobase noomit r2 se replace
```

> - 上表中第 1 列与第 2 列结果显示了 **在大学附近成长** 对受教育年数与工资的影响效果在 **家庭受教育背景较差的男性** 的群体中更大 。
> - 第 3 列报告了使用 **在大学附近成长与家庭受教育背景较差的交乘项** 作为工具变量的估计结果。与表 3 中的结果相比要小一些，并且标准误要略大一些。结果还显示了 **在大学附近成长对工资的影响很小，并且不显著，表明是否在大学附近成长这个工具变量的外生性** 。
> - 在第 1 列至第 3 列的回归模型中，表征家庭受教育背景较差的变量为哑变量，仅有 2 类，因此在第 4 列回归模型中，作者使用 **8 类表示家庭受教育背景的哑变量与是否在大学附近成长的交乘项** 作为工具变量进行 IV 估计，结果显示标准误有所减小，系数值略微增加。
> - 上述结果显示，不论如何将家庭受教育背景进行分类，不论使用 **交乘项** 作为工具变量还是 **仅使用是否在大学附近成长** 作为工具变量，估计结果是 **相近的** 。**并且，是否在大学附近成长对工资的影响效果是很小的并且不显著** 。**是否在大学附近成长** 或 **是否在大学附近成长与家庭受教育背景的交乘项** 都可以作为受教育年数的 **工具变量** 。

在文献的最后部分，作者对结果进行总结并讨论：
- **在大学附近成长的男性** 的受教育年数与工资显著 **高于** 未在大学附近成长的男性。并且，对于那些 **家庭教育背景较差的男性** 来说，**在大学附近成长** 对受教育年数与工资的 **正效应更大**。
- 使用 **是否在大学附近成长** 作为 IV 、使用 2SLS 法对受教育年数的边际影响的估计结果介于 10-14% ，与 OLS 估计结果相比（7.3%），OLS 估计结果是 **下偏的** 。当对模型中变量的设定进行稍许改变时，IV 估计结果是稳健的。这些结果与已有的一些文献的结论是相似的，但却与 OLS会 **高估** 受教育年数的边际效应的普遍认知是 **相反的** 。
- **一方面可能的原因** 是由于受教育年数存在测量误差导致 OLS 估计结果下偏，**另一方面可能的原因是** “真实” 的教育回报率对于不同人群来说会不一样。如果在大学附近成长仅仅影响家庭教育背景较差的男性做出是否继续接受教育的决策，那么 IV 估计值仅仅是对这部分人群计算的受教育年数的边际影响。
- 这些 IV 估计结果的成立都是 **建立在 “是否在大学附近成长与工资不直接相关 (除了是否在大学附近成长会通过受教育年数影响工资的情况) ” 的假设之上** 。对此，作者进行了检验 ：作者利用在大学附近成长对那些家庭教育背景较差的男性的正效应更大的特征，使用 **是否在大学附近成长与家庭教育背景较差的交乘项** 作为工具变量，这样 **工资的结构方程中可以直接包括是否在大学附近成长这个变量，以检验它的显著性** 。使用 **交乘项** 作为工具变量的结果与 **仅使用是否在大学附近成长** 作为工具变量的估计结果是相近的，并且，**是否在大学附近成长对工资的影响很小且不显著** 。