> 作者：连玉君 | 吴思锐

## 1. 引言
在实证研究中，我们经常会遇到被解释变量是部分观测数据的情形。例如：
- 在研究什么因素更易导致公司财务欺诈时，我们仅能观测到从事欺诈活动且被证券监管机构调查公告的公司，而不能观测到发生财务欺诈但未被稽查的公司，从而低估相关因素对欺诈概率的影响。
- 在研究贷款人信贷需求和信贷机构信贷决策问题时，仅能观测到“有需求和有供给”的情形，而无法观测到“有需求无供给”，“无需求有供给”和“无需求和无供给”这三种情形。

在这些情况下，采用二元 Logit 模型研究被解释变量的影响因素是 **不合适的** ，需要使用基于部分可观测的Bivariate Probit 模型。Poirier（1980）最早针对部分可观测问题提出该方法， 近些年 Bivariate Probit 估计在公司违规及证券监管领域逐渐普及。本次推文主要介绍 `biprobit` 命令及其在 Stata 中的应用实例。Bivariate Probit 模型是两个二元变量结果的联合模型。如果这两个二元变量的结果是**不相关**的，我们可以估计两个独立的 probit 模型，否则则需要使用 Bivariate Probit 模型。Bivariate Probit 模型边际效应和预测值的估计和二元 probit 模型类似，但其是两种不同结果的联合概率。以公司违规为例，公司违规数据由公司自身违规行为和证券监管机构违规稽查共同决定。由于未被稽查出的公司违规行为是不可观测数据，公司的违规行为属于部分可观测数据。也就是说，我们只能观测到发生了违规并且被稽查出的公司样本。因此若直接采用Probit 模型进行估计，则会低估违规影响因素（如舞弊的动机、压力、机会）对公司违规的影响程度。在 Bivariate Probit 模型中，不可观测到的潜变量可以表述为
$$y^*_1=x_1\beta_1+e_1$$
$$y^*_2=x_2\beta_2+e_2$$
Bivariate Probit 模型将结果指定为
$$y_1={ 1 \quad if  \ y^*_1>0 \choose 0 \quad if  \ y^*_1\leq0}$$
$$y_2={ 1 \quad if  \ y^*_2>0 \choose 0 \quad if  \ y^*_2\leq0}$$
由于不能直接观测到$y_1$和$y_2$的取值，而是观察到$y_1*y_2$的取值，即

P(Observe=1)=P($y_1*y_2$=1)=P($y_2=1$ | $y_1=1$) * P($y_1=1$)= $\psi$($x_1\beta_1$,$x_2\beta_2$,$
\rho$)

P(Observe=0)=P($y_1*y_2$=0)=P($y_2=0$ | $y_1=1$) * P($y_1=1$) + P($y_1=0$)=1 - $\psi$($x_1\beta_1$,$x_2\beta_2$,$
\rho$)

其中，$\rho$为$e_1$和$e_2$之间的相关系数，$\psi$为二元正态分布的分布函数，根据以上公式可求得对数似然函数，再通过最大似然法进行模型的参数估计。需要特别注意的一点是，**$x_1$和$x_2$这两组解释变量不能完全相同**。


## 2. `biprobit` 命令

```Stata

  biprobit depvar1 depvar2 [indepvars] [if] [in] [weight] [, options]

```
其中， `depvar` 是被解释变量； `indepvars` 是解释变量。

`_options` 的选项：  partial  是拟合部分观测模型，offset1(varname) 是抵消第一个方程中的变量，offset2(varname) 是抵消第二个方程中的变量； vce(vcetype) 是指稳健型标准误估计的类别选项，可以是异方差稳健型标准误 robust，聚类调整的稳健型标准误 cluster，自体抽样法稳健型标准误 bootstrap 或刀切法稳健型标准误 jackknife。

## 3. Stata实例
本次推文以 Pindyck and Rubinfeld (1998) school 数据集为例，对 Bivariate Probit 模型的使用进行简要介绍。Pindyck and Rubinfeld (1998) school 数据集的变量包括：子女是否上私立学校（private），家庭在目前所在地的居住年限（years），财产税的对数（logpta），收入的对数（loginc）以及家庭业主是否对增加财产税投赞成票（vote）。我们希望根据其他协变量对子女是否上私立学校和家庭业主是否对增加财产税投赞成票的双变量结果进行建模。
```stata

webuse school

biprobit private vote logptax loginc years
------------------
Fitting comparison equation 1:

Iteration 0:   log likelihood = -31.967097  
Iteration 1:   log likelihood = -31.452424  
Iteration 2:   log likelihood = -31.448958  
Iteration 3:   log likelihood = -31.448958  

Fitting comparison equation 2:

Iteration 0:   log likelihood = -63.036914  
Iteration 1:   log likelihood = -58.534843  
Iteration 2:   log likelihood = -58.497292  
Iteration 3:   log likelihood = -58.497288  

Comparison:    log likelihood = -89.946246

Fitting full model:

Iteration 0:   log likelihood = -89.946246  
Iteration 1:   log likelihood = -89.258897  
Iteration 2:   log likelihood = -89.254028  
Iteration 3:   log likelihood = -89.254028  

Bivariate probit regression                     Number of obs     =         95
                                                Wald chi2(6)      =       9.59
Log likelihood = -89.254028                     Prob > chi2       =     0.1431

------------------------------------------------------------------------------
             |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
private      |
     logptax |  -.1066962   .6669782    -0.16   0.873    -1.413949    1.200557
      loginc |   .3762037   .5306484     0.71   0.478     -.663848    1.416255
       years |  -.0118884   .0256778    -0.46   0.643    -.0622159    .0384391
       _cons |  -4.184694   4.837817    -0.86   0.387    -13.66664    5.297253
-------------+----------------------------------------------------------------
vote         |
     logptax |  -1.288707   .5752266    -2.24   0.025    -2.416131   -.1612839
      loginc |    .998286   .4403565     2.27   0.023     .1352031    1.861369
       years |  -.0168561   .0147834    -1.14   0.254    -.0458309    .0121188
       _cons |  -.5360573   4.068509    -0.13   0.895    -8.510188    7.438073
-------------+----------------------------------------------------------------
     /athrho |  -.2764525   .2412099    -1.15   0.252    -.7492153    .1963102
-------------+----------------------------------------------------------------
         rho |  -.2696186   .2236753                     -.6346806    .1938267
------------------------------------------------------------------------------
LR test of rho=0: chi2(1) = 1.38444                       Prob > chi2 = 0.2393

```
上图展示了多个迭代日志的结果，第一个迭代日志展示了运行第一个方程的单变量 probit 模型结果，第二个迭代日志展示了运行第二个方程的单变量 probit 模型结果。如果$\rho=0$，这两个模型的对数似然之和等于 Bivariate Probit 模型的对数似然函数值。最后一个迭代日志用来拟合完整的 Bivariate Probit 模型，结果后列示了该模型最大似然估计的似然比及其检验。但如果指定了vce（robust）选项，则该测试将作为Wald测试而非似然比检验。

注：本次推文主要介绍了 Bivariate probit model 的应用，如果是要做似不相关 Bivariate Probit 回归（Seemingly unrelated bivariate probit regression），则可使用如下命令：
```stata
   gen y_copy=y
   biprobit equation1 equation2 [if] [in] [weight] [, su_options]
```
`su_options` 的选项：partial 用于部分观测模型，collinear 保留共线的变量。以公司违规为例，假设 equation 1 和 equation 2 中的被解释变量分别是公司自身违规行为和违规被稽查变量，则equation 1 中的解释变量为单独影响公司违规行为的因素（如资产负债率和资产收益率等）和同时影响公司违规行为和被稽查公告的因素（如公司规模等），equation 2 中的解释变量为单独影响违规稽查的因素（如公司的股价波动率和超额收益率等）和同时影响公司违规行为和被稽查公告的因素。

## 参考文献
[[1] 陆瑶, 胡江燕. CEO与董事间“老乡”关系对公司违规行为的影响研究[J]. 南开管理评论, 2016, 019(002): 52-62](http://www.cnki.com.cn/Article/CJFDTotal-LKGP201602006.htm)
[[2] Khanna, V., Kim, E. H., & Lu, Y. (2015). CEO connectedness and corporate fraud. The Journal of Finance, 70(3), 1203-1252](http://xueshu.baidu.com/usercenter/paper/show?paperid=a6f00885912272a898ad7e9d9df1a56a&site=xueshu_se)
[[3] Poirier, D. J. 1980. Partial observability in bivariate probit models. Journal of Econometrics 12: 209–217](http://xueshu.baidu.com/usercenter/paper/show?paperid=4da0aad59aa146f3f08e842680c66635&site=xueshu_se)
[[4] Ani Katchova, Bivariate Probit and Logit Models](http://sites.google.com/site/econometricsacademy/econometrics-models/bivariate-probit-and-logit-models)