
# 学术前沿工具：拐点回归设计 

> 这篇文章摘译    
> 源自: [DAVID MCKENZIE](https://blogs.worldbank.org/team/david-mckenzie), Tools of the Trade: The Regression Kink Design, [Link](https://blogs.worldbank.org/impactevaluations/tools-trade-regression-kink-design)

断点回归设计已经成为评估政策影响的一个流行工具
[visually appealing](https://blogs.worldbank.org/impactevaluations/regression-discontinuity-porn) ，
并提供了一种直观的方式来演示政策对断点附近的影响。这种方法的一个扩展是**拐点回归（RKD）**，它的应用正在不断增长。我从没有估计过，也不是专家，我尝试提供一些这种方法介绍和相关的链接，希望能对想用种方法的人有用，如果人们想要实现这种方法，可以关注下面的内容：

## 基本理念 

断点回归设计利用在某个阈值处被处理的可能性的跳跃或不连续性。而在RKD设计中，在拐点处的处理点可能发生变化，导致赋值函数的一阶导数不连续。这些类型的问题出现在一些政府政策中。
例如，[Simonsen et al. (2015)](http://onlinelibrary.wiley.com/doi/10.1002/jae.2436/abstract;jsessionid=0C061B1217904090DBAFE0FD3A16D26C.f04t01?userIsAuthenticated=false&deniedAccessCustomisedMessage=) 
用拐点来阐释丹麦政府的处方药报销计划中：补贴是基于个人一年中支付的处方费总额来计算的。初始500丹麦克朗的费用有0%的补贴，之后会有50%的补贴。一旦超过500丹麦克朗，在500到1200丹麦克朗支出，可以获得75%的补贴。超过2800丹麦克朗以上的费用，最终可获得80%的补贴。自付费部分出现拐点的结果如图1所示： 

![Figure 1](https://images.gitee.com/uploads/images/2020/0321/100314_cad1ccd2_6569481.png)

>图1:Y轴是自付费的份额。当接近500丹麦克朗时，补贴会下降。如果你已经花费480丹麦克朗，如再有50丹麦克朗购买支出， 那么，其中20丹麦克朗支出部分会得到0%补贴，超过500临界点的30丹麦克朗支出部分会得到50%补贴。
第二个例子是政府的失业保险金，它通常涵盖你以前工资部分比例，有个最高达到的上限，有时还有一个最低下限。例如图2来自最近[Landais](https://www.aeaweb.org/articles.php?doi=10.1257/pol.20130248),发表的一篇论文，显示了路易斯安那州失业救济金与最高季度收入的函数关系：

![Figure 2: ](https://images.gitee.com/uploads/images/2020/0321/100314_4e82a6e8_6569481.png)
>表2.  美国路易斯安那州: 用户周福利金额明细表,1979.1-1983.12. 表2.美国路易斯安那州：用户周福利金额明细表，1979.1-1983.12
接下来的思路是观察在同一点上，我们关心的结果是否与其相关的运行变量也呈现出斜率拐点/变化。例如，Landais感兴趣的是，人们在失业中花费的时间与他们所获得的福利金额的函数关系。下面的图3显示了在拐点处，失业持续时间和最高季度收入之间关系强烈变化。
面板A.福利水平影响 

![Figure 3: Kink in the outcome at the same place as kink in the treatment](https://images.gitee.com/uploads/images/2020/0321/100314_29ff3851_6569481.png)
>图3：得出的拐点结果与处理的拐点处于同样位置图3：得出的拐点结果与处理的拐点处于同样位置 -随机效应通过处理变量斜率的变化除以结果变量斜率的变化观测得到的。当政策在执行时出现误差时，可以使用模糊拐点方法(see [Card et al. (2015)](http://onlinelibrary.wiley.com/doi/10.3982/ECTA11224/abstract))

 ## 估计

可以用多项式回归来估计：
设x为运行变量（如上一季度最高收益），Y为结果变量（如失业持续时间）；k为拐点，D为拐点右侧的虚拟变量。然后做如下估计：
Y=a0+a1*（x-k）+b1D（x-k）+a2*（x-k）^2+a3D（x-k）^2+…
b1为拐点处结果斜率变化的结果。然后进行相同的回归，得到处理变量在运行变量转折点处的斜率变化，并取两个系数的比值。然后可以使用Delta方法做重复抽样或修正标准误。也可以使用局部多项式进行非参数估计。
## 关键假设和检验 

1. 关键假设与回归不连续性的假设相似，并以类似的方式进行检验： 其他协变量在转折点处的斜率不应发生变化（例如，Landai显示在转折点处的年龄、教育、资本收入或受抚养人数量的斜率没有转折）。
2. 在临界点不存在对分配的操纵——例如，在失业情况下，人们不会战略性地操纵他们过去的收入，以期望这样对他们未来的失业福利产生影响。这可以通过Mcclary检验来验证，检验表明赋值变量在拐点处的分布是连续的，且该Pdf的一阶导数也是平滑的。[Card et al. (2015)](http://onlinelibrary.wiley.com/doi/10.3982/ECTA11224/abstract) 表明可以允许代理变量围绕拐点做一些排序，只要代理变量出现优化误差或错误，排序就不是完全确定的。
3. 这篇[文章](http://ftp.iza.org/dp8282.pdf) 建议你在非拐点处做一些安慰剂测试。 在[Econometrica paper by Card et al. (2015).](http://onlinelibrary.wiley.com/doi/10.3982/ECTA11224/abstract)中可以找到Card等人(2015)提供技术细节。
## 注意事项

- RKD设计在小样本中做得很差，通常需要比RDD更宽的带宽或更大的样本。
- 有一些优化选择带宽的方法，而那些无法证明这种选择的合理性和/或对不同带宽选择表现出稳健性的论文则面临批评。例如，请参阅对本[AEJ经济政策文件的评论](https://www.aeaweb.org/articles.php?doi=10.1257/pol.6.1.167)。

## 参考文献

Andais的[comments on this AEJ Economic Policy paper](https://www.aeaweb.org/articles.php?doi=10.1257/pol.6.1.167)提供了方便的[在线数据](https://www.aeaweb.org/articles.php?doi=10.1257/pol.20130248)。这篇由Calonico等人撰写的Stata期刊文章，有一些研发代码也涵盖了RKD的情况。


##文献原文

Regression Discontinuity designs have become a popular addition to the impact evaluation toolkit, and offer a [visually appealing](https://blogs.worldbank.org/impactevaluations/regression-discontinuity-porn) way of demonstrating the impact of a program around a cutoff. An extension of this approach which is growing in usage is the **regression kink design(RKD)**. I've never estimated one of these, and am not an expert, but thought it might be useful to try to provide an introduction to this approach along with some links that people can then follow-up on if they want to implement it.

## Basic Idea
The regression discontinuity design exploits a jump or discontinuity in the likelihood of being treated at some threshold point. In the RKD design, there is instead a change in slope at the likelihood of being treated at a kink point, resulting in a discontinuity in the first-derivative of the assignment function. These types of kinks arise in a number of government policies. For example, [Simonsen et al. (2015)](http://onlinelibrary.wiley.com/doi/10.1002/jae.2436/abstract;jsessionid=0C061B1217904090DBAFE0FD3A16D26C.f04t01?userIsAuthenticated=false&deniedAccessCustomisedMessage=) use a kink in the Danish government's prescription drug reimbursement schedule: the subsidy is based on the total prescription costs the individual has paid during the year – there is 0% subsidy for the first 500 DKK in expenses, then 50% subsidy once you have paid 500 up until you have paid 1200, then 75% subsidy, and eventually an 80% subsidy for expenses above 2800 DKK. The result is that the share of the price paid out of pocket kinks as shown in Figure 1:

![Figure 1](https://images.gitee.com/uploads/images/2020/0321/100314_cad1ccd2_6569481.png)

> Figure 1: Y-axis is the share of the price paid out of pocket. It falls as one approaches 500 DKK since if you have spent 480 and buy something for 50, you get 0% subsidy on 20 DKK and then 50% subsidy on the 30 DKK that is expenditure past the threshold.

A second example is government unemployment insurance payments, which often cover up to some percentage of your previous wage, up to some maximum, and sometimes with a floor. For example, Figure 2, from a [recent paper by Landais](https://www.aeaweb.org/articles.php?doi=10.1257/pol.20130248), shows the unemployment benefit in Louisiana as a function of the highest quarter earnings:

![Figure 2: ](https://images.gitee.com/uploads/images/2020/0321/100314_4e82a6e8_6569481.png)

Then the idea is to see whether the outcome of interest also exhibits a kink/change of slope in its relation to the running variable at this same point. For example, Landais is interested in how long people spend in unemployment as function of the amount of benefit they receive. Figure 3 below shows a strong change in the shape of the relationship between unemployment duration and highest quarter earnings at this kink-point.

![Figure 3: Kink in the outcome at the same place as kink in the treatment](https://images.gitee.com/uploads/images/2020/0321/100314_29ff3851_6569481.png)

> Figure 3: Kink in the outcome at the same place as kink in the treatment

Then the causal impact is found by dividing the change in slope for the outcome by the change in slope for the treatment.
When the policy rule is implemented with some error, a fuzzy RKD design can be used (see [Card et al. (2015)](http://onlinelibrary.wiley.com/doi/10.3982/ECTA11224/abstract))

## Estimation

This can be estimated by using a polynomial regression. Let x be the running variable (e.g. highest quarter previous earnings), Y the outcome variable (e.g. duration of unemployment); k the kink point, and D a dummy variable for being to the right of the kink. Then estimate:
Y = a0 + a1*(x-k) + b1*D*(x-k) + a2*(x-k)^2 + a3*D*(x-k)^2 + …
Then b1 tells you the change in the slope of the outcome at the kink point.
Then run the same regression to get the change in the slope of the treatment variable at the kink point of the running variable, and take the ratio of the two coefficients. The standard error can then be bootstrapped or recovered using the Delta method.
Estimation can also be done non-parametrically using local polynomials.

## Key assumptions and checks 
The key assumptions are similar to those for regression discontinuity and are checked in similar ways:

1. There should be no change in slope at the kink point for other covariates (e.g. Landais shows no kinks in slope for age, education, capital income, or number of dependents at the kink point)
2. There is no manipulation of the assignment variable at the kink point – e.g. in the unemployment case, people don't strategically manipulate their past earnings in expectation of how this will affect their future unemployment benefits. This can be tested by McCrary-type tests that show the distribution of the assignment variable is continuous at the kink point, and also that the first derivative of this pdf is also smooth. [Card et al. (2015)](http://onlinelibrary.wiley.com/doi/10.3982/ECTA11224/abstract) show that you can allow agents to do some sorting around the kink points, so long as they make small optimization errors or mistakes so that this sorting is not completely deterministic.
3. This [paper](http://ftp.iza.org/dp8282.pdf) suggests some placebo tests you can do at non-kink points.

Technical details can be found in this [Econometrica paper by Card et al. (2015).](http://onlinelibrary.wiley.com/doi/10.3982/ECTA11224/abstract)

## Caveats

- The RKD design can do pretty poorly in small samples, and typically will require a wider bandwidth or larger sample than you would need with a RDD.
- There are methods for optimally choosing the bandwidth, and papers that aren't able to justify this choice and/or show robustness to different bandwidth choices face criticism. See the [comments on this AEJ Economic Policy paper](https://www.aeaweb.org/articles.php?doi=10.1257/pol.6.1.167) for an example.


## Resources
The AEJ paper of Landais has data and replication Stata code [available online](https://www.aeaweb.org/articles.php?doi=10.1257/pol.20130248).
[This Stata Journal article](http://www-personal.umich.edu/~cattaneo/papers/Calonico-Cattaneo-Titiunik_2014_Stata.pdf) by Calonico et al. has some RD code that also covers the RKD case.