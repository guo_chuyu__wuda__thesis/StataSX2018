


&emsp;

> 作者：王文韬 (山东大学)    
>     
> E-mail: <ssss@163.com>

## 1. 简介

## 2. 理论说明

$y_{it} = x_{it}\beta + \varepsilon_{it}$



### 2.1 基本原理

### 2.2 推导


## Stata 实操
  
使用 `regress` 命令来实现：

```stata
. sysuse "auto.dta", clear
. regress price wei, robust

``` 