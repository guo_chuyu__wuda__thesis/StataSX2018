
```
作者：王文韬（山东大学）
Email：190138445@qq.com
```


## 一、版本与工具

Python⽬前有 **不兼容** 的两个版本：`Python2.7+` 和 `Python3`，Python2 和 Python3 之间语法很⼤程度上不能互通。

由于Python官⽅将在不久的将来停⽌对 Python2 的⽀持和更新，⽽且绝⼤多数的库都已经迁移到了 Python3 中，因⽽我们强烈建议未来不要继续学习和使⽤ Python2，⽽是**使⽤`Python3`。** 

初学者可以考虑**直接安装** `Anaconda`。Anaconda 和 Python 解释器⼀般只安装⼀个就可以，如⽆特殊需求，不要重复安装。

 **前面我们安装的 Anaconda 中包含的是 Python3，因此不需要再次下载。如果电脑上原来安装了python2，请卸载掉。** 


## 二、相关安装

### 2.1 Anaconda 和 Python 的安装

在前面游老师课程上，我们已经安装了 Anaconda ，不需要重复安装。大家可以简单操作一下，以验证自己的 Python 版本正确与否：

根据 **司老师 "Pdf_version.zip" → "T0_begin.pdf"** ，打开  **Anaconda Prompt**，分别输入 `python` 和 `import this` 进行检验，如下所示，最后点击 ctrl+d 退出 Python Shell：

![输入 python 后回车结果](https://images.gitee.com/uploads/images/2020/0331/115853_3403ea84_7349907.png "1.png")

![输入 import this 后回车结果](https://images.gitee.com/uploads/images/2020/0331/115953_734160cc_7349907.png "微信截图_20200331113241.png")



### 2.2 Vscode下载
点击 `Anaconda Navigation` （与点击 "Anaconda Prompt" 类似），可以出现下图所示界面，上课时我们会用到 `Jupyter Notebook` `IPython` `Spyder` 以及` VS Code`，可以发现 VScode 没有 launch ，需要下载安装：

![Anaconda Navigation 界面](https://images.gitee.com/uploads/images/2020/0331/120742_8c384c94_7349907.png "nabigator.png")

`VScode` 安装的具体操作为：

1）打开 **https://code.visualstudio.com** 链接，选择对应的版本进行下载，然后找到 ".exe" 双击进行安装：

![选择适合的版本进行下载](https://images.gitee.com/uploads/images/2020/0331/123246_11abfe9e_7349907.png "微信截图_20200331123203.png")

![应用程序](https://images.gitee.com/uploads/images/2020/0331/123656_908bf622_7349907.png "微信截图_20200331123616.png")

2）安装成功后打开，选择左边一列最下面的图标（`Extension`），输入 `python` ，点击 install 即可：

![下载 Python 插件](https://images.gitee.com/uploads/images/2020/0331/123952_81b07337_7349907.png "微信截图_20200331123911.png")

### 2.3 Git下载

在开始下载前，请先登录 https://github.com/ 注册一个账号。

 **2.3.1 Windows 版本下载** 

打开网页 https://git-scm.com/downloads 下载安装， **注意** ：此次安装需要链接外网，打开`Vpn`才可正常下载安装程序。
安装完成后，在电脑桌面空白处右击选择 `git`→ `git bash` 即可。

安装完成后，需要配置个⼈的名字和Email：

·git config --global user.name "your name"

·git config --global user.email "sombody@somesite.com"

![安装完成后打开设置个人姓名和Email](https://images.gitee.com/uploads/images/2020/0331/141032_055a845f_7349907.png "111.png")

 **2.3.2 Mac版本下载** 

 **方法一** ，通过brew安装，⾸先安装 `homebrew`：

/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/
install/master/install)"

brew install git

 **方法二** ，通过 `Xcode` 安装：

直接从 AppStore 安装 Xcode，运⾏ Xcode，选择菜单 `Xcode` → `Preferences`，在弹出窗⼝中找 到 `Downloads` ，选择 `Command Line Tools`，点击  **Install** 。

## 2.4 Numpy等下载

本课程所需要的包：

pip install `numpy` `scipy` `pandas` `scikit-learn` `matplotlib` `tushare` `akdata` `jieba` `seaborn` `nltk` `selenium` `requests` `flask` `gensim` `bs4` --upgrade

### 安装方法一：

共有15个安装包，可以在 Notebook 中依次输入 `“pip install xxx --upgrade”` 进行安装（xxx 为安装包名称，例如 numpy）

### 安装方法二——快速安装方法：

1、将上述安装包名字粘贴复制到 txt 文件中，并将其上传至 jupyter notebook 中（操作方法与上传 .ipynb 文件相同)，txt的名字大家可自行定义，在这里我使用的是 “r.txt”，从下面的代码中可以看到。另外，`gensim`这一安装包比较特殊，请以 "-i https://pypi.tuna.tsinghua.edu.cn/simple gensim" 的形式粘贴进去。如下图所示：

![创建 r.txt 并上传至 Notebook](https://images.gitee.com/uploads/images/2020/0331/183346_f43221d0_7349907.png "123.png")

2、在命令行中输入：

```
pip install -r r.txt
```

注： `gensim` 安装可能会遇到问题，还可以尝试单独下载，在 Anaconda Prompt 中输入以下代码：

`pip install -i https://pypi.tuna.tsinghua.edu.cn/simple gensim --user` 

为检验是否安装成功，可在 jupyter notebook 命令行中输入 `import gensim`，如果正常运行即表明安装成功，如下图所示：

![import gensim](https://images.gitee.com/uploads/images/2020/0331/170702_8a5ed6de_7349907.png "微信截图_20200331170649.png")


 
**如果是mac或者Linux，需要将 pip 换成 `sudo pip3`** 

**如果是Windows，后面可能还要加 `--user`** 


### 注：

司老师为大家准备了一些小惊喜，例如 `OCRmypdf` 的使用说明，这个安装包在 Mac 和 Linux 系统下安装都很方便，但是在 Windows 系统下安装非常复杂。由于这一部分不是老师的授课内容，使用 Windows 的老师想要安装的话，可以尝试先安装 `WSL`，然后再 `Linux`环境下安装。

有关 WSL 的安装方法可参考司老师文章：
`https://mp.weixin.qq.com/s?__biz=MjM5NTE1NjQ0NA==&mid=2654631297&idx=1&sn=32f22d851acd5819dc5334d8c265b05a&chksm=bd320d328a458424473a819ae8b82d3bb63356300b28770f316a4a1ace43be9d4ba5579e8711&scene=0&xtrack=1&key=868c3c6d20df59e092dd4c0eda0e5664f864f3c0f2e9fe971b589879ee8d7a16402008354586b5f6181558d001f6e4d27064ab8a2e4de3f4f3196a03d282c4b926a763373c1bba566fcff03afd2b9413&ascene=1&uin=MTQwMjc5MTQ0Mw%3D%3D&devicetype=Windows+8.1&version=62080079&lang=zh_CN&exportkey=AcqF2JWiyLnTWZCu8puT738%3D&pass_ticket=C04xww7LaD%2F9MG3WNOSYYiBa3YEmpxg6s0r1SDLP5u%2B1h7G8A2K2KJIY7kS0JJxI`

























