> 作者：王文韬（山东大学）
>
> E-mail：190138445@qq.com 

**目 录**
- 一、引言
- 二、熵平衡介绍
    - 2.1 基本含义
    - 2.2 总体平均处理效应与熵平衡
- 三、Stata 实现
    - 3.1 指令安装与变量说明
    - 3.2 ebalance 的基本语法
    - 3.3 利用 ebalance 实现 NSW 实验效果的无偏估计
- 四、结论
- 五、参考文献


## 一、引言
在进行随机实验时，我们需要对处理组和对照组的协变量进行平衡性检验，以保证实验组满足外生性，从而增加回归结果的有效性。Hainmueller 于 2012年构建了`熵平衡方法（Entropy Balancing）` ，使研究者能够同时控制处理组与对照组样本协变量进行多维度调整，如**同时考虑协变量的一阶矩、二阶矩、三阶矩和交叉矩**等，最大程度上使两组样本在其协变量满足的约束条件下实现精确匹配。

根据Google学术镜像，Hainmueller 在 2013 年撰写的介绍熵平衡 ebalance 指令的Stata 操作文献已被国内外学者广泛使用，引用次数达 **200** 余次。近期，国内学者（张海峰、梁若冰、林细细，2019；马恩、王有强，2019）陆续使用这一方法开展学术研究。

**需要强调的是**，熵平衡方法虽然能在一定程度上降低实验组的内生性，但不可能完全解决回归模型中的内生性问题。在实际研究中，研究者在科学使用这一方法时，要注意搭配其他计量工具进行系统检验，以保证回归结果的稳健性和合理性。

本文主要向大家介绍熵平衡方法在 Stata 中实现时需要用到的 `ebalance`  指令及具体操作步骤。

## 二、熵平衡介绍
### 2.1 基本含义
在高中物理课堂上，我们曾学习过“熵”的概念。它是热力学中表征物质状态的参量之一，能够度量一个体系的混乱程度。在物理热力学中，熵越大表示该体系中的热量越高。在计量经济学中，**“熵平衡”**则是一个基于最大化熵的权重计算方法。所谓**最大化熵，指的是研究者进行随机性实验研究时，在对数据进行预处理的过程中，最大化处理组与对照组的协变量匹配效率**。
### 2.2  总体平均处理效应与熵平衡
研究者在进行随机性实验时，主要关注的结果是**总体平均处理效应（the Population Average Treament Effect on the Treated）**，可以由以下公式表示：
$$\tau= E[Y(1)|D=1] - E[Y(0)|D=1] $$其中，**$E[Y(1)|D=1]$** 表示处理组实验发生后的效应大小，**$E[Y(0)|D=1]$** 表示处理组在没有发生实验后的效应大小。

显然，**$E[Y(0)|D=1]$** 是一个反事实指标，在现实中不可能存在对应的数据。为了估计这一项，学者一般通过调整对照组的协变量分布，使其能够近似处理组的协变量分布，从而令是否为处理组可以满足外生性需求。

目前，我们熟知的协变量匹配方法包括`最近邻匹配（NNM）`、`粗化精确匹配（CEM）`、`倾向得分匹配（PSM）`等。以**倾向得分匹配为例**，$E[Y(0)|D=1]$可以通过以下表达式计算得到：
$$ E[Y(0)|D=1] =\frac {\sum_{i|D=0}Y_id_i }{\sum_{i|D=0}d_i }$$ 其中，$d_i = \frac {p(x_i)}{1-p(x_i)}$, $p_i$ 为倾向得分值。

**这些匹配方法面临的一个共同问题是：研究者无法保证联合并平衡所有协变量，难以避免倾向得分模型被错误指定的可能。** 为了解决这一问题，学者通常利用`Logit 回归`挑选可以通过平衡性检验的协变量组合。但是，这样做费时费力，同时无法完全保证最终得到高水平的协变量组合。

**在上述基础上，Hainmueller 推导出熵平衡方法**，以熵权重 $\omega_i$ 代替 $d_i$ , 得到反事实指标 $E[Y(0)|D=1]$ 的表达式如下所示：
$$ E[Y(0)|D=1] =\frac {\sum_{i|D=0}Y_i\omega_i }{\sum_{i|D=0}\omega_i }$$ 权重 $\omega_i$ 通过以下公式决定，**表示最小化的熵距离度量**：
$$ min  H(\omega_i)= \sum_{i|D=0}\omega_i log(\omega_i/q_i)$$ 其中 $q_i = 1/n$ , $n$ 表示对照组样本数量。 **$\omega_i$ 具有一定约束条件，其核心思想是，给予对照组协变量增加一组矩约束，使其与处理组的协变量相平衡。矩约束包括均值（一阶矩）、方差（二阶矩）和偏度（三阶矩）**。有关最小化熵距离和 $\omega_i$ 的相关数理验证这里就不展开描述了，对此感兴趣的读者可以自行阅读 Hainmueller 的相关文献。

综上所述，Hainmueller 创造的熵平衡方法为研究者在平衡处理组与对照组的协变量分布上提供了新的科学方法。接下来，我们使用 Hainmueller(2013) 提供的案例及其数据来说明 ebalance 在Stata 中的具体应用。

## 三、Stata 实现
### 3.1 指令安装与变量说明
#### （1）指令安装
我们下载 `ebalance `指令及所需数据包 `cps1re74.dta `，Stata 代码如下所示（本文所有实证操作均在 Stata15 软件中进行）：
```Stata
ssc install ebalance, all replace
```
简单介绍一下 cps1re74.dta 数据包的基本情况。这一数据包最早由 Dehejia 和 Wahba (1999) 两位学者使用，进行相关研究。它包括 185 名参与 NSW （the National Supported Work Demonstration，一个关于补贴资助工作的随机评估）项目的成员和 15,992 名未参与项目的成员（来自美国社会保障管理档案 CPS-1）。
#### （2）变量说明
下面实证部分将使用到的变量及其含义如下表所示：

| 变量     | 含义                     |
| -------- | ------------------------ |
| re78    | 1978年实际收入（单位：美元） | 
| treat   | 处理组 = 1；对照组  = 0   |
| age     | 年龄                      |
| educ    | 受教育年限                 |
|  black  | 黑人  = 1，否则为0          |
|  hisp   | 西班牙裔 = 1；非西班牙裔 = 0 |
| married | 已婚 = 1；未婚 = 0          |
| nodegree | 未获得高中文凭 = 1；获得高中文凭 = 0 | 
| re74    | 1974年实际收入（单位：美元）             | 
| re75    | 1975年实际收入（单位：美元）              | 
| u74     | 1974年未就业 = 1（re74为0） ，否则为0     | 
| u75     | 1975年未就业 = 1（re75为0） ，否则为0      | 

本文衡量政策效果的变量为 **re78**，根据 Dehejia 和 Wahba (1999) 的研究分析，NSW 这一补贴资助计划平均提高了 **1,794** 美元的收入水平，95% 的置信区间（单位：美元）为 [551: 3,038] 。这一结果便是 Hainmueller 想利用熵平衡方法预处理数据之后得到的政策效应的无偏估计。

如果我们直接使用 OLS 回归进行检验，发现此时的平均处理效应为 1,016 美元。具体结果如下所示：
```Stata
use cps1re74.dta, clear
reg re78 treat age-u75

      Source |       SS           df       MS      Number of obs   =    16,177
-------------+----------------------------------   F(11, 16165)    =   1343.88
       Model |  7.2418e+11        11  6.5835e+10   Prob > F        =    0.0000
    Residual |  7.9190e+11    16,165  48988567.3   R-squared       =    0.4777
-------------+----------------------------------   Adj R-squared   =    0.4773
       Total |  1.5161e+12    16,176  93724175.2   Root MSE        =    6999.2

------------------------------------------------------------------------------
        re78 |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
       treat |   1067.546   554.0595     1.93   0.054    -18.47193    2153.564
         age |  -94.54102   6.000283   -15.76   0.000    -106.3022    -82.7798
        educ |   175.2255   28.69658     6.11   0.000      118.977     231.474
       black |  -811.0888   212.8488    -3.81   0.000    -1228.296   -393.8815
      hispan |  -230.5349   218.6098    -1.05   0.292    -659.0344    197.9646
     married |   153.2284   142.7748     1.07   0.283     -126.626    433.0828
    nodegree |   342.9265   177.8778     1.93   0.054    -5.733561    691.5866
        re74 |   .2914332   .0127311    22.89   0.000     .2664789    .3163875
        re75 |   .4426945   .0128868    34.35   0.000      .417435     .467954
         u74 |   355.5564   231.6004     1.54   0.125    -98.40599    809.5189
         u75 |  -1612.758    239.803    -6.73   0.000    -2082.798   -1142.717
       _cons |    5762.18   445.6145    12.93   0.000     4888.726    6635.634
------------------------------------------------------------------------------
```
上述结果表明，OLS 估计值大大低于真正的政策实施效果。 于是我们考虑利用熵平衡对数据进行预处理，看看是否能使我们更准确地评估实际情况。

### 3.2 ebalance 的基本语法
#### （1）基本语法
 `ebalance `的基本语法如下所示：
```Stata
ebalance [treat] [covar] , targets(numlist)
```
其中，**[treat]** 代表是否为处理组的代理变量，**[covar]** 代表需要在熵平衡中进行调整的协变量组合，**targets(numlist)** 是最为关键的选择指标，**numlist** 对应协变量组合需要调整的矩阶数。下面进行举例说明：
```Stata
ebalance treat age black educ , targets(3) #表示对 age、black 和 educ 三个协变量的一阶、二阶和三阶矩进行调整

Data Setup
Treatment variable:   treat
Covariate adjustment: age black educ (1st order). age black educ (2nd order). age black educ (3rd order).

Optimizing...
Iteration 1: Max Difference = 580799.347
Iteration 2: Max Difference = 213665.688
......    #为节省篇幅，省略部分结果
Iteration 16: Max Difference = .037151116
Iteration 17: Max Difference = .008791339
maximum difference smaller than the tolerance level; convergence achieved

Treated units: 185     total of weights: 185
Control units: 15992   total of weights: 185

Before: without weighting

             |              Treat              |             Control             
             |      mean   variance   skewness |      mean   variance   skewness 
-------------+---------------------------------+---------------------------------
         age |     25.82      51.19      1.115 |     33.23        122      .3478 
       black |     .8432      .1329     -1.888 |    .07354     .06813      3.268 
        educ |     10.35      4.043     -.7212 |     12.03      8.242     -.4233 

After:  _webal as the weighting variable

             |              Treat              |             Control             
             |      mean   variance   skewness |      mean   variance   skewness 
-------------+---------------------------------+---------------------------------
         age |     25.82      51.19      1.115 |     25.75      51.22       1.14 
       black |     .8432      .1329     -1.888 |     .8423      .1328     -1.879 
        educ |     10.35      4.043     -.7212 |     10.35      4.039     -.7224 

ebalance treat age black educ , targets(3 1 2) #表示对 age 的一阶、二阶和三阶矩进行调整，对 black 的一阶矩进行调整，对 educ 的一阶和二阶矩进行调整

Data Setup
Treatment variable:   treat
Covariate adjustment: age black educ (1st order). age educ (2nd order). age (3rd order).

Optimizing...
Iteration 1: Max Difference = 573647.601
Iteration 2: Max Difference = 211032.083
......  #为节省篇幅，省略部分结果
Iteration 15: Max Difference = .082623597
Iteration 16: Max Difference = .0011605
maximum difference smaller than the tolerance level; convergence achieved

Treated units: 185     total of weights: 185
Control units: 15992   total of weights: 185

Before: without weighting

             |              Treat              |             Control             
             |      mean   variance   skewness |      mean   variance   skewness 
-------------+---------------------------------+---------------------------------
         age |     25.82      51.19      1.115 |     33.23        122      .3478 
       black |     .8432      .1329     -1.888 |    .07354     .06813      3.268 
        educ |     10.35      4.043     -.7212 |     12.03      8.242     -.4233 

After:  _webal as the weighting variable

             |              Treat              |             Control             
             |      mean   variance   skewness |      mean   variance   skewness 
-------------+---------------------------------+---------------------------------
         age |     25.82      51.19      1.115 |     25.82       51.2      1.115 
       black |     .8432      .1329     -1.888 |     .8432      .1322     -1.888 
        educ |     10.35      4.043     -.7212 |     10.35      4.043     -.7193 

```
从结果来看：1）调整后，处理组和加权调整后的对照组的年龄均值、方差和偏度相同。 2）对于虚拟变量，如 black，调整一阶足以匹配到最精确的水平。3）对于教育，调整均值和方差（numlist = 2）便会使得处理组和对照组之间达到几乎相同的偏态。

需要指出的是，`ebalance treat age, targets(2)` 与 `ebalance treat age age2, targets(1)` 表示的执行指令相同，age2 代表年龄变量 age 的平方项。
#### （2）平衡性检验
**为了验证某一个协变量是否匹配，可以使用** `tabstat` **进行验证**，以年龄 age 为例：
```Stata
tabstat age [aweight=_webal], by(treat) s(N me v) nototal #检验是否平衡

Summary for variables: age
     by categories of: treat (1 if treated, 0 control)

   treat |         N      mean  variance
---------+------------------------------
       0 |     15992  25.81641   51.1981
       1 |       185  25.81622   51.1943
----------------------------------------
```
**默认情况下，ebalance命令执行后，Stata 会自动将匹配权重储存在** `_webal ` 。从上面的检验结果看，经过熵平衡调整之后，age 变量在对照组的分布几乎与处理组一致。
#### （3）变量交互项的匹配
在实际研究中，我们还需要**考虑协变量之间的交互项**。例如，为了调整协变量组，我们应保证黑人和非黑人的年龄均值相似，这里便要引入 black 和 age 的交互项：
```Stata
gen ageXblack = age*black  #设置 age 和 black 的交互项 ageXblack
 ebalance treat age educ black ageXblack, targets(3 2 1 1)  #进行熵平衡

#为节省篇幅，这里仅显示熵平衡调整之后的结果
After:  _webal as the weighting variable

             |              Treat              |             Control             
             |      mean   variance   skewness |      mean   variance   skewness 
-------------+---------------------------------+---------------------------------
         age |     25.82      51.19      1.115 |     25.82      51.19      1.115 
        educ |     10.35      4.043     -.7212 |     10.35      4.043     -.7231 
       black |     .8432      .1329     -1.888 |     .8432      .1322     -1.888 
   ageXblack |     21.91      134.6     -.4435 |     21.91      133.2     -.4572 

bysort black: tabstat age [aweight=_webal], by(treat) s(N me v) nototal #根据 _webal 验证年龄均值在黑人和非黑人之间是平衡的
-------------------------------------------------------------------------------------------------------------------------
-> black = 0
Summary for variables: age
     by categories of: treat (1 if treated, 0 control)
   treat |         N      mean  variance
---------+------------------------------
       0 |     14816  24.93109  45.28087
       1 |        29  24.93103  40.49507
----------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-> black = 1
Summary for variables: age
     by categories of: treat (1 if treated, 0 control)
   treat |         N      mean  variance
---------+------------------------------
       0 |      1176  25.98077  52.16224
       1 |       156  25.98077   53.2835
----------------------------------------
#由于 black 为虚拟变量，因此 ageXblack 只需要满足一阶矩平衡即可，从结果来看加入 ageXblack 之后进行熵平衡的匹配结果是平衡的
```


### 3.3 利用 eblance 实现 NSW 实验效果的无偏估计
为最大程度的平衡样本，我们使用所有协变量的一阶、二阶、三阶以及所有一阶交互项进行 ebalance 匹配。具体如下所示：
```Stata
sysuse cps1re74, clear

#协变量组合生成二阶矩及一阶交互项
foreach v in age educ black hispan married nodegree re74 re75 u74 u75 {
  foreach m in age educ black hispan married nodegree re74 re75 u74 u75 {
    gen `v'X`m'=`v'*`m'
  }
}

#age、educ、re74 和 re75 作为连续变量，还需设置他们的三阶矩
foreach v in age educ re74 re75 {
    gen `v'X`v'X`v' = `v'^3
  }

#进行熵平衡，并将结果保存到 baltable.dta 
ebalance treat age educ black hispan married nodegree re74 re75 u74 u75 ageXage ageXeduc ageXblack ageXhispan ageXmarried ageXnodegree ageXre74 ageXre75 ageXu74 ageXu75 educXeduc educXblack educXhispan  educXmarried educXnodegree educXre74 educXre75 educXu74 educXu75 blackXmarried blackXnodegree blackXre74 blackXre75 blackXu74  blackXu75 hispanXmarried hispanXnodegree hispanXre74 hispanXre75  hispanXu74 hispanXu75  marriedXnodegree marriedXre74 marriedXre75  marriedXu74 marriedXu75  nodegreeXre74 nodegreeXre75 nodegreeXu74 nodegreeXu75 re74Xre74 re74Xre75 re74Xu75  re75Xre75 re75Xu74 u74Xu75 re75Xre75Xre75 re74Xre74Xre74 ageXageXage educXeducXeduc, keep(baltable) replace

After:  _webal as the weighting variable  #熵平衡处理后一阶协变量的结果

             |              Treat              |             Control             
             |      mean   variance   skewness |      mean   variance   skewness 
-------------+---------------------------------+---------------------------------
         age |     25.82      51.19      1.115 |     25.82      50.92      1.116 
        educ |     10.35      4.043     -.7212 |     10.35      4.022     -.7206 
       black |     .8432      .1329     -1.888 |     .8432      .1322     -1.888 
      hispan |    .05946     .05623      3.726 |    .05946     .05593      3.726 
     married |     .1892      .1542      1.587 |     .1892      .1534      1.587 
    nodegree |     .7081      .2078     -.9155 |     .7081      .2067     -.9154 
        re74 |      2096   2.39e+07      3.387 |      2097   2.38e+07      3.385 
        re75 |      1532   1.04e+07       3.78 |      1533   1.03e+07      3.781 
         u74 |     .7081      .2078     -.9155 |      .708      .2067     -.9151 
         u75 |        .6      .2413     -.4082 |     .5999        .24     -.4079 

```
根据 `baltable.dta` 中的平衡结果进行简单整理，得到如下所示的一阶协变量的平衡检验表格：
![一阶协变量的平衡检验结果](https://images.gitee.com/uploads/images/2020/0523/001910_7e7e98a2_7349907.png)

下面，对调整加权之后的样本组重新进行回归：
```Stata
svyset [pweight=_webal] 
svy: reg re78 treat

Survey: Linear regression

Number of strata   =         1                  Number of obs     =     16,177
Number of PSUs     =    16,177                  Population size   =        370
                                                Design df         =     16,176
                                                F(   1,  16176)   =       5.58
                                                Prob > F          =     0.0182
                                                R-squared         =     0.0161

------------------------------------------------------------------------------
             |             Linearized
        re78 |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
       treat |   1761.344   745.5099     2.36   0.018      300.062    3222.626
       _cons |     4587.8   472.2286     9.72   0.000     3662.179     5513.42
------------------------------------------------------------------------------

```
可以发现，上述回归结果显示的处理效果为 1,761 美元，这表明**熵平衡预处理步骤能够让我们非常接近随机实验的实际评估结果**，95% 置信区间为 [300，3,223] 美元。

## 四、结论
**第一，熵平衡可以与回归方法相结合，研究者首先通过调整协变量对协变量进行重新加权，然后将加权后的数据应用于回归模型。这一过程可应用于回归前对处理组和对照组数据的预处理工作以及后续的稳健性检验。**

**第二，在实证回归中，ebalance指令的关键点是如何设置阶数（numlist）**：
（1）对于虚拟变量及包含虚拟变量的交互项，只需进行一阶矩加权调整即可实现平衡；
（2）对于年龄等连续变量，通常选择其一阶、二阶、三阶矩以及一阶与其他协变量一阶矩的交互项进行加权调整；
（3）在进行熵平衡之后，权重值自动储存在 `_webal ` 指标下。

**第三，熵平衡不是万能的。 实际研究中，为保证检验结果的稳健性，研究者最好搭配 NNM、PSM、CEM 等其他匹配方法一起进行实证分析。** Hainmueller 也提出，未来会考虑如何将熵平衡与 Stata 中其他匹配方法相结合，进一步提高匹配效率。


## 五、参考文献
[1] Hainmueller J.  Entropy Balancing for Causal Effects: A Multivariate Reweighting Method to Produce Balanced Samples in  
    Observational Studies[J]. Political Analysis, 2012, 20(1):25–46.

[2] Hainmueller J, Xu Y. ebalance: A Stata Package for Entropy Balancing[J]. Journal of Statistical Software, 2013, 54(7).

[3] 张海峰,梁若冰,林细细.子女数量对农村家庭经济决策的影响——兼谈对“二孩政策”的启示[J].中国经济问题,2019(03):68-80.

[4] 马恩,王有强.区位导向性政策是否促进了企业创新?——以我国开发区政策为例[J].科技管理研究,2019,39(11):35-42.

[5] Dehejia R, Wahba S. Causal Effects in Nonexperimental Studies: Reevaluating the Evaluation of Training Programs[J]. 
    Journal of the American Statistical Association, 1999,94:1053–1062.

