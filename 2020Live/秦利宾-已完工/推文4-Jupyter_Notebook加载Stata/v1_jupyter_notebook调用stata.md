> **作者：** 秦利宾 (厦门大学)  
> **邮箱：** <qlb150@163.com>


---

**目录**

[[TOC]]

---


## 背景介绍

`Jupyter Notebook` 是一个开源的 `Web` 应用程序，旨在方便开发者创建和共享代码文档。用户可以在里面写代码、运行代码、查看结果，并在其中可视化数据。同时，`Jupyter  Notebook` 也具有较高的灵活性和交互性，可支持 40 多种编程语言。鉴于以上优点，我们可以通过加载 `Stata`，可以更好与 `Python` 进行交互，也可以更好展示 `Stata` 代码和结果，方便与他人共享。为此，本文将简要介绍两种 `Jupyter Notebook` 加载 `Stata` 方法。

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/jupyter_notebook调用Stata-图1-秦利宾.gif"
    width="700">
    <div><font size=2>Jupyter Notebook 加载 Stata 官方演示图 </font></div>
</center>


## 1. 注册 Stata

首先，以`管理员身份`打开 `CMD` 命令窗口。然后，在 `CMD` 中输入 `cd /d D:\stata` 定位到 `Stata` 安装路径。最后输入 `StataMP-64.exe /Register` 注册 `Stata`。这里 `Stata` 的安装路径和可执行文件需要根据自己电脑设置。

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图2-秦利宾.png"
    width="700">
    <div><font size=2>注册 Stata </font></div>
</center>

## 2. 使用 ipystata 模块调用 Stata 

### 2.1 安装 ipystata 模块

打开 `Anaconda Prompt`，输入 `pip install ipystata`，出现下图表示安装成功。需要注意的是，成功安装 `ipystata` 模块后，需要在 `Jupyter Notebook` 中 `Restart the Kernel`。

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图3-秦利宾.png"
    width="700">
    <div><font size=2> 安装 ipystata 模块 </font></div>
</center>

### 2.2 导入 ipystata 模块

输入以下命令，导入 `ipystata` 模块。

```python
import ipystata  #导入ipystata模块
```

或则，输入以下命令，导入 `ipystata` 模块。

```python
import ipystata  #导入ipystata模块
from ipystata.config import config_stata  #告诉ipystata哪里可以找到stata
config_stata(r"D:\stata\StataMP-64.exe", force_batch=True)   #Stata可执行文件路径，根据自己电脑设置
```

调用魔法函数 `%%stata`，输入 `Stata` 命令，检验是否可以成功调用 `Stata`。

```python 
%%stata  
display "Hello, I am printed in Stata."  
```

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图4-秦利宾.png"
    width="700">
    <div><font size=2> 导入 ipystata 模块 </font></div>
</center>

### 2.3 基本语法

在加载 `ipystata` 模块后，若要运行 `Stata` 命令，需要在 `Cell` 中第一行输入魔法函数 `%%stata`，该命令会将整个 `cell` 中代码在 `Stata` 中执行，并将结果输出到 `Jupyter Notebook`。

通过 `-o` 或 `--output` 将 `Stata` 中数据导入到 `Jupyter Notebook` 中。之后，可以调用 `Python` 对数据进行处理。

```python
%%stata -o car_df
sysuse auto.dta  //需要注意的是，第一行不能写注释 
```

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图5-秦利宾.png"
    width="700">
    <div><font size=2> 导入 auto 数据 </font></div>
</center>

```python
car_df.head()  #查看car_df前几行信息
car_df = car_df[["price", "mpg", "rep78", "weight", "length"]]  #提取某几列
car_df.head()
```

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图6-秦利宾.png"
    width="700">
    <div><font size=2>用 Python 处理数据</font></div>
</center>

通过 `-d` 或 `--data` 将 `DataFrame` 数据导入到 `Stata` 中，并调用 `Stata` 进行处理。

```python
%%stata -d car_df
sum 
pwcorr price weight length  //可以运行Stata其他命令，如reg price weight length
```

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图7-秦利宾.png"
    width="700">
    <div><font size=2>用 Stata 处理数据</font></div>
</center>

同时，也可以使用以下命令，将 `Stata` 处理后的数据导入到 `Jupyter Notebook`。

```python
%%stata -o car_df
sysuse auto.dta, clear
keep price mpg rep78 weight length  //保留几列
gen lnprice = log(price)  //生成price的对数lnprice
```
```python
car_df.head()
```

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图8-秦利宾.png"
    width="700">
    <div><font size=2> Jupyter Notebook 导入Stata 处理后数据 </font></div>
</center>

通过 `-gr` 或 `--graph`，在 `Jupyter Notebook` 中画 `Stata` 图。

```python
%%stata  -gr -d car_df
graph twoway scatter price weight
```

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图9-秦利宾.png"
    width="700">
    <div><font size=2> 在 Jupyter Notebook 中画 Stata 图 </font></div>
</center>

若 `Jupyter Notebook` 不能正常显示图形，可通过加 `-os` 或 `--openstata` 的方法打开 `Stata` 程序作图。需要注意，`Stata` 作完图后，需将 `Stata` 终端关掉，否则 `Jupyter Notebook` 程序将一直运行，即 `cell` 前一直为 `[*]` 状态。

```python
%%stata -gr -d car_df -os
graph twoway scatter price weight
```

## 3. 使用 stata_kernel 调用 Stata

### 3.1 stata_kernel 安装

在 `Anaconda Prompt`，输入以下命令安装 `stata_kernel`。同时，也需要在 `Jupyter Notebook` 中 `Restart the Kernel`。若 `stata_kernel` 长时间不能安装成功，可以参考 [Python 配置 pip 镜像地址](https://blog.csdn.net/weixin_44489823/article/details/90486951)。

```python
pip install stata_kernel 
python -m stata_kernel.install
```

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图10-秦利宾.png"
    width="700">
    <div><font size=2> 安装 stata_kernel </font></div>
</center>

安装完 `stata_kernel` 后，打开 `Jupyter Notebook`，通过 `Kernel` -> `Change kernel` -> `Stata` 可以新建一个用 `Stata` 编写的 `.ipynb` 文档。

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图11-秦利宾.png"
    width="700">
    <div><font size=2> 新建一个 Stata 的 .ipynb 文档 </font></div>
</center>

### 3.2 基本语法

与 `ipystata` 用法不同，`stata_kernel` 并不需要特殊命令，我们可以像在 `Stata` 中一样写代码，正如文章最开始那副图的展示。

不过，`stata_kernel` 也提供了特殊的命令，可以提供一些特殊功能，如 `%head`、`%tail`、`%browser`、`%help`、`%locals`、`%globals`等，我们可以通过 `%magic_name --help` 查看这些命令的帮助文档。

其中，`%head (%tail, %browser)` 是以 `DataFrame` 在 `Jupyter Notebook` 中展现数据。

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图12-秦利宾.png"
    width="700">
    <div><font size=2> %head 结果展示 </font></div>
</center>

`%local` 和 `%globals` 可以展示当前文件暂元中的内容。`%help` 可以将相关命令的帮助文档展示在 `Jupyter Notebook` 中。

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图13-秦利宾.png"
    width="700">
    <div><font size=2> %help 结果展示 </font></div>
</center>

## 4. 总结

以上是两种加载 `Stata` 的方法，即 `ipystata` 和 `stata_kernel`，并且两种方法都要注册 `Stata`。在这个过程中，若不能正常使用，可重新安装 `Stata` 或其他版本进行尝试。

## 4. 参考资料

* [IPyStata 官方文档](https://github.com/TiesdeKok/ipystata/tree/master/)

* [stata_kernal 官方文档](https://kylebarron.dev/stata_kernel/)

* [在Python中使用Stata：IPyStata介绍](https://blog.csdn.net/qq_42146941/article/details/104657784)

* [双剑合璧之 Stata 与 Python：初识 IPyStata](https://zhuanlan.zhihu.com/p/22645950)
