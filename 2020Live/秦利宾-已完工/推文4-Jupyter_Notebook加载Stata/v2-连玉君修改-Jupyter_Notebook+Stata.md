> **作者：** 秦利宾 (厦门大学)  
> **邮箱：** <qlb150@163.com>


---

**目录**

[[TOC]]

---


## 背景介绍

### 为什么要用 Jupyter Notebook + Stata 模式 ？

无论是老师写讲义，还是学生向老师汇报研究进展，我们经常需要不停地在写作软件 (如 Word, Vscode, Typora) 和统计软件 (如，Stata, Python, R) 之间切换，以便将文字说明和实证结果「人为融合」在一份文档中，增加其可读性。

这种模式的最大局限在于：一旦需要更新文稿中的实证结果，就需要重新复制粘贴一次，很是繁琐。 

今天要介绍的 **Jupyter Notebook** 可以完美解决这一困惑 —— 它是「文字说明 + 代码 + 结果展示」的有机结合体。由于所有的文档都采用 Markdown 来写，所以很好地实现了 **内容** 与 **格式** 的分离，让我们可以专注于写作。对于导师而言，采用「Jupyter Notebook + Stata」模式最大的好处莫过于可以实时了解学生的进展，杜绝了学生由于「赶工」或「过度追星」而进行的各种冒险行为。

遗憾的是，此前，我们想把 **Jupyter Notebook** 和 **Stata** 融合起来，并非易事。前段时间，在担任 [【连享会-文本分析与爬虫专题】](https://www.lianxh.cn/news/88426b2faeea8.html) 课程助教的过程中，大家集思广益，终于测试出了一些相对稳定的关联方法，分享于此，希望能将各位从恼人的的「**Ctrl+C/V**」中解放出来。ps，由于这门直播课支持回放，不断有新的学员加入，因此，我们助教团队仍然能够结识不少新同仁，日后必然会有更好的解决方案。


> #### [连享会 - 文本分析与爬虫 - 专题视频](https://www.lianxh.cn/news/88426b2faeea8.html)    
> 主讲嘉宾：司继春 || 游万海

![连享会-文本分析与爬虫-专题视频教程](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lanNew-文本分析-海报002.png "连享会-文本分析与爬虫-专题视频，四天课程，随随时学")

### Jupyter Notebook 是个啥东东？

**Jupyter Notebook** 是一个开源的 Web 应用程序，旨在方便开发者创建和共享代码文档。用户可以在里面写代码、运行代码、查看结果，并在其中可视化数据。同时，**Jupyter Notebook** 也具有较高的灵活性和交互性，可支持 40 多种编程语言。鉴于以上优点，我们可以通过加载 **Stata**，可以更好与 `Python` 进行交互，也可以更好展示 **Stata** 代码和结果，方便与他人共享。为此，本文将简要介绍两种 **Jupyter Notebook** 加载 **Stata** 方法。

[连享会-码云主页](https://gitee.com/arlionn) 上放置了几个访问量非常高的项目，都是采用 **Jupyter Notebook** 来展示的，比如：
- [Introduction to Statistics With Python](https://gitee.com/arlionn/statsintro_python)
- [Combine Python with Stata using IPyStata](https://gitee.com/arlionn/ipystata)
- [Intro to Python using Jupyter](https://gitee.com/arlionn/Intro-to-Python-using-Jupyter)

下面这幅 gif 图形很好地展示了「Jupyter Notebook + Stata」模式的工作过程和便利之处：

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/jupyter_notebook调用Stata-图1-秦利宾.gif"
    width="700">
    <div><font size=2>Jupyter Notebook 加载 Stata 官方演示图 </font></div>
</center>

言归正传，我们说说如何让 **Jupyter Notebook** 和 **Stata** 关联起来。 

&emsp;


## 1. 注册 Stata

首先，以 `管理员身份` 打开 `CMD` 命令窗口。然后，在 `CMD` 中输入 `cd /d D:\stata` 定位到 **Stata** 安装路径。最后输入 `StataMP-64.exe /Register` 注册 **Stata**。这里 **Stata** 的安装路径和可执行文件需要根据自己电脑设置。

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图2-秦利宾.png"
    width="700">
    <div><font size=2>注册 Stata </font></div>
</center>

## 2. 使用 ipystata 模块调用 Stata 

### 2.1 安装 ipystata 模块

打开 `Anaconda Prompt`，输入 `pip install ipystata`，出现下图表示安装成功。需要注意的是，成功安装 `ipystata` 模块后，需要在 **Jupyter Notebook** 中 `Restart the Kernel`。

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图3-秦利宾.png"
    width="700">
    <div><font size=2> 安装 ipystata 模块 </font></div>
</center>

### 2.2 导入 ipystata 模块

输入以下命令，导入 `ipystata` 模块。

```python
import ipystata  #导入ipystata模块
```

或则，输入以下命令，导入 `ipystata` 模块。

```python
import ipystata  #导入ipystata模块
from ipystata.config import config_stata  #告诉ipystata哪里可以找到stata
config_stata(r"D:\stata\StataMP-64.exe", force_batch=True)   #Stata可执行文件路径，根据自己电脑设置
```

调用魔法函数 `%%stata`，输入 **Stata** 命令，检验是否可以成功调用 **Stata**。

```python 
%%stata  
display "Hello, I am printed in Stata."  
```

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图4-秦利宾.png"
    width="700">
    <div><font size=2> 导入 ipystata 模块 </font></div>
</center>

### 2.3 基本语法

在加载 `ipystata` 模块后，若要运行 **Stata** 命令，需要在 `Cell` 中第一行输入魔法函数 `%%stata`，该命令会将整个 `cell` 中代码在 **Stata** 中执行，并将结果输出到 **Jupyter Notebook**。

通过 `-o` 或 `--output` 将 **Stata** 中数据导入到 **Jupyter Notebook** 中。之后，可以调用 `Python` 对数据进行处理。

```python
%%stata -o car_df
sysuse auto.dta  //需要注意的是，第一行不能写注释 
```

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图5-秦利宾.png"
    width="700">
    <div><font size=2> 导入 auto 数据 </font></div>
</center>

```python
car_df.head()  #查看car_df前几行信息
car_df = car_df[["price", "mpg", "rep78", "weight", "length"]]  #提取某几列
car_df.head()
```

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图6-秦利宾.png"
    width="700">
    <div><font size=2>用 Python 处理数据</font></div>
</center>

通过 `-d` 或 `--data` 将 `DataFrame` 数据导入到 **Stata** 中，并调用 **Stata** 进行处理。

```python
%%stata -d car_df
sum 
pwcorr price weight length  //可以运行Stata其他命令，如reg price weight length
```

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图7-秦利宾.png"
    width="700">
    <div><font size=2>用 Stata 处理数据</font></div>
</center>

同时，也可以使用以下命令，将 **Stata** 处理后的数据导入到 **Jupyter Notebook**。

```python
%%stata -o car_df
sysuse auto.dta, clear
keep price mpg rep78 weight length  //保留几列
gen lnprice = log(price)  //生成price的对数lnprice
```
```python
car_df.head()
```

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图8-秦利宾.png"
    width="700">
    <div><font size=2> Jupyter Notebook 导入Stata 处理后数据 </font></div>
</center>

通过 `-gr` 或 `--graph`，在 **Jupyter Notebook** 中画 **Stata** 图。

```python
%%stata  -gr -d car_df
graph twoway scatter price weight
```

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图9-秦利宾.png"
    width="700">
    <div><font size=2> 在 Jupyter Notebook 中画 Stata 图 </font></div>
</center>

若 **Jupyter Notebook** 不能正常显示图形，可通过加 `-os` 或 `--openstata` 的方法打开 **Stata** 程序作图。需要注意，**Stata** 作完图后，需将 **Stata** 终端关掉，否则 **Jupyter Notebook** 程序将一直运行，即 `cell` 前一直为 `[*]` 状态。

```python
%%stata -gr -d car_df -os
graph twoway scatter price weight
```

## 3. 使用 stata_kernel 调用 Stata

### 3.1 stata_kernel 安装

在 `Anaconda Prompt`，输入以下命令安装 `stata_kernel`。同时，也需要在 **Jupyter Notebook** 中 `Restart the Kernel`。若 `stata_kernel` 长时间不能安装成功，可以参考 [Python 配置 pip 镜像地址](https://blog.csdn.net/weixin_44489823/article/details/90486951)。

```python
pip install stata_kernel 
python -m stata_kernel.install
```

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图10-秦利宾.png"
    width="700">
    <div><font size=2> 安装 stata_kernel </font></div>
</center>

安装完 `stata_kernel` 后，打开 **Jupyter Notebook**，通过 `Kernel` -> `Change kernel` -> **Stata** 可以新建一个用 **Stata** 编写的 `.ipynb` 文档。

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图11-秦利宾.png"
    width="700">
    <div><font size=2> 新建一个 Stata 的 .ipynb 文档 </font></div>
</center>

### 3.2 基本语法

与 `ipystata` 用法不同，`stata_kernel` 并不需要特殊命令，我们可以像在 **Stata** 中一样写代码，正如文章最开始那副图的展示。

不过，`stata_kernel` 也提供了特殊的命令，可以提供一些特殊功能，如 `%head`、`%tail`、`%browser`、`%help`、`%locals`、`%globals`等，我们可以通过 `%magic_name --help` 查看这些命令的帮助文档。

其中，`%head (%tail, %browser)` 是以 `DataFrame` 在 **Jupyter Notebook** 中展现数据。

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图12-秦利宾.png"
    width="700">
    <div><font size=2> %head 结果展示 </font></div>
</center>

`%local` 和 `%globals` 可以展示当前文件暂元中的内容。`%help` 可以将相关命令的帮助文档展示在 **Jupyter Notebook** 中。

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/juypter_notebook调用Stata-图13-秦利宾.png"
    width="700">
    <div><font size=2> %help 结果展示 </font></div>
</center>

&emsp;

## 4. 总结

以上是两种加载 **Stata** 的方法，即 `ipystata` 和 `stata_kernel`，并且两种方法都要注册 **Stata**。在这个过程中，若不能正常使用，可重新安装 **Stata** 或其他版本进行尝试。

&emsp;

## 5. 参考资料

* [IPyStata 官方文档](https://github.com/TiesdeKok/ipystata/tree/master/)
* [stata_kernal 官方文档](https://kylebarron.dev/stata_kernel/)
* [在Python中使用Stata：IPyStata介绍](https://blog.csdn.net/qq_42146941/article/details/104657784)
* [双剑合璧之 Stata 与 Python：初识 IPyStata](https://zhuanlan.zhihu.com/p/22645950)


&emsp; 

> **连享会-直播课** 上线了！         
>  <http://lianxh.duanshu.com>  

> **免费公开课：**
> - [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟
> - [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. 
> - 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等)

---
## 课程一览   

&emsp;

> #### NEW！[连享会 - 效率分析专题](https://www.lianxh.cn/news/a94e5f6b8df01.html)， 2020年5月29-31日   
> 主讲嘉宾：连玉君 | 鲁晓东 | 张宁      
> [课程主页](https://www.lianxh.cn/news/a94e5f6b8df01.html)，[微信版](https://mp.weixin.qq.com/s/zRotMGebIQqaMih8B71fdg)，[码云版](https://gitee.com/arlionn/TE)，[PDF版](https://quqi.gblhgk.com/s/880197/g3ne5HdXdrSoo8iL)

![连享会-效率分析专题视频](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/TE专题_连享会_lianxh.cn500.png "连享会-效率分析专题视频，三天课程，随时学")

&emsp;


> 支持回看，所有课程可以随时购买观看。

| 专题 | 嘉宾    | 直播/回看视频    |
| --- | --- | --- |
| **[效率分析专题](https://gitee.com/arlionn/TE)** | 连玉君<br>鲁晓东<br>张&emsp;宁 | [视频-TFP-SFA-DEA](https://www.lianxh.cn/news/88426b2faeea8.html) <br> 2020.5.29-31, 热招中 |
| **文本/爬虫** | 游万海<br>司继春 | [视频-文本分析与爬虫](https://www.lianxh.cn/news/88426b2faeea8.html) <br> 已上线，可随时购买 |
| **分位数回归** | 游万海 | [视频，2小时，88元](https://lianxh.duanshu.com/#/brief/course/f0bfb3102ada48969966c92123a7ebf0) |
| **[空间计量系列](https://lianxh.duanshu.com/#/brief/course/958fd224da8548e1ba7ff0740b536143)** | 范巧    | [空间全局模型](https://efves.duanshu.com/#/brief/course/ed1bc8fc5e7748c5aca7e2c39d28e20e), [空间权重矩阵](https://lianxh.duanshu.com/#/brief/course/94a5361647384a18852d28d1b9246362) <br> [空间动态面板](https://lianxh.duanshu.com/#/brief/course/f4e4b6b1e77c4ff88cecb685bbde07c3), [空间DID](https://lianxh.duanshu.com/#/brief/course/ff7dc9e0b82b40dab2047af0d01e96d0) |
| 论文重现 | 连玉君    | [经典论文精讲](https://lianxh.duanshu.com/#/brief/course/c3f79a0395a84d2f868d3502c348eafc)，[-课程资料-](https://gitee.com/arlionn/Paper101), [-幻灯片-](https://quqi.com/s/880197/nz9LvbzzEEpWBNrD)   |
| 研究设计 | 连玉君    | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[-幻灯片-](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)|
| 面板模型 | 连玉君    | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[-幻灯片-](https://quqi.gblhgk.com/s/880197/o7tDK5tHd0YOlYJl)   |
|     |     | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，2小时]  |
| 大数据 | 李兵 | [经济学中的大数据应用](https://lianxh.duanshu.com/#/brief/course/da1a75bc3acc4e238f489af3367efa26) |
| R 语言 | 游万海 | [R 语言初识](https://lianxh.duanshu.com/#/brief/course/a719037536de4812a630a599f8cd7b43), 9.9元
| Python+Github | 司继春 | [Python和Github入门](https://lianxh.duanshu.com/#/brief/course/132a12b2e5ef45b795bfa897c037a6f4) <br> 2小时, 9.9元

> Note: 部分课程的资料，PPT 等可以前往 [连享会-直播课](https://gitee.com/arlionn/Live) 主页查看，下载。

 <img style="width: 180px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/行走的人小.jpg">


&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- **你的颈椎还好吗？** 您将 [::连享会-主页::](https://www.lianxh.cn) 和 [::连享会-知乎专栏::](https://www.zhihu.com/people/arlionn/) 收藏起来，以便随时在电脑上查看往期推文。
- **公众号推文分类：** [计量专题](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=4&sn=0c34b12da7762c5cabc5527fa5a1ff7b) | [分类推文](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=2&sn=07017b31da626e2beab0332f5aa5f9e2) | [资源工具](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=3&sn=10c2cf37e172289644f03a4c3b5bd506)。推文分成  **内生性** | **空间计量** | **时序面板** | **结果输出** | **交乘调节** 五类，主流方法介绍一目了然：DID, RDD, IV, GMM, FE, Probit 等。
- **公众号关键词搜索/回复** 功能已经上线。大家可以在公众号左下角点击键盘图标，输入简要关键词，以便快速呈现历史推文，获取工具软件和数据下载。常见关键词：
  - `课程, 直播, 视频, 客服, 模型设定, 研究设计, `
  - `stata, plus，Profile, 手册, SJ, 外部命令, profile, mata, 绘图, 编程, 数据, 可视化`
  - `DID，RDD, PSM，IV，DID, DDD, 合成控制法，内生性, 事件研究` 
  - `交乘, 平方项, 缺失值, 离群值, 缩尾, R2, 乱码, 结果`
  - `Probit, Logit, tobit, MLE, GMM, DEA, Bootstrap, bs, MC, TFP`
  - `面板, 直击面板数据, 动态面板, VAR, 生存分析, 分位数`
  - `空间, 空间计量, 连老师, 直播, 爬虫, 文本, 正则, python`
  - `Markdown, Markdown幻灯片, marp, 工具, 软件, Sai2, gInk, Annotator, 手写批注`
  - `盈余管理, 特斯拉, 甲壳虫, 论文重现`
  - `易懂教程, 码云, 教程, 知乎`


---

![连享会主页  lianxh.cn](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会跑起来就有风400.png "连享会主页：lianxh.cn")


> 扫码加入连享会微信群，提问交流更方便

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-学习交流微信群001-150.jpg)

