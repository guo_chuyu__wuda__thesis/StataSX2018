

&emsp;

> **作者：** 秦利宾 (厦门大学)  
> **邮箱：** <qlb150@163.com>

&emsp;
 
> Stata 连享会 <https://www.lianxh.cn>  
> 扫码查看最新推文和分享

![](https://file.lianxh.cn/images/20191111/3ed0c73d48c0046f04c502458b4e1c0b.png)

&emsp;


> 连享会小程序：扫一扫，看推文，看视频……

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会小程序二维码180.png)


&emsp;


> **编者按：**

自连享会 [**<font color=green>「文本分析与爬虫 - 视频专题课程(4天)」</font>**](https://mp.weixin.qq.com/s/U-QlnkXOfAuFyeyXkjv5_A) 上线以来，小伙伴们根据课程所学，掀起了一波爬虫热潮，先后完成了一系列非常实用的推文，且多数内容都已应用于自己的论文中：
- [**<font color=blue>「Python: 6 小时爬完上交所和深交所的年报问询函」</font>**](https://www.lianxh.cn/news/0e57c635cd225.html) 
- [**<font color=blue>「Python爬虫: 《经济研究》研究热点和主题分析」</font>**](https://www.lianxh.cn/news/2fb619662956e.html) 
- [**<font color=blue>「Python+微信: 如何优雅地管理微信数据库？」</font>**](https://www.lianxh.cn/news/d34f09cb214e0.html) 
- 其他相关推文参见连享会主页-Python专题：<https://www.lianxh.cn/blogs/37.html>

今天，我们的文本分析专题班助教秦利宾老师为大家分享如何使用 Python 爬取上市公司公告。



&emsp;

> #### [连享会 - 文本分析与爬虫 - 专题视频](https://www.lianxh.cn/news/88426b2faeea8.html)    
> 主讲嘉宾：司继春 || 游万海

![连享会-文本分析与爬虫-专题视频教程](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lanNew-文本分析-海报002.png "连享会-文本分析与爬虫-专题视频，四天课程，随随时学")





---

**目录**

[[TOC]]

---

&emsp;


## 1. 背景介绍

目前，上市公司公告主要从巨潮网、上交所以及深交所等网站获取。爬取的步骤分为以下两步，一是获取公告地址，二是通过公告地址下载 `PDF` 文件。通常来说，获取公告地址比较麻烦。本文将根据 `Wind` 和 `CSMAR` 数据库的公告信息简化获取公告地址过程，并下载 `PDF` 文件，这里以年报为例。

## 2. 爬取 Wind 上市公司年报

### 2.1 获取年报地址

首先，打开 Wind 终端，按照`公司公告`->`年度报告`->`高级检索`的步骤检索上市公司年报信息。

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/爬取上市公司公告-图1-秦利宾.png"
    width="700">
    <div><font size=2>年报检索</font></div>
</center>

其次，点击导出列表，将检索到的年报信息导出到 `Excel` 文件。需要注意的是，`Wind` 最多导出 `9999` 条记录。实际操作中，可以通过在检索中设置时间区间分批导出。

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/爬取上市公司公告-图2-秦利宾.png"
    width="700">
    <div><font size=2>下载年报信息</font></div>
</center>

最后，公告标题在 `Excel` 中是以超链接形式存在，通过 Excel 中的函数 `FORMULATEXT()` 可以将超链接分解成链接和文本两部分。进一步，可以通过调用 `Python` 进行拆分，提取公告地址链接。

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/爬取上市公司公告-图3-秦利宾.png"
    width="500" height="200">
    <div><font size=2>获取年报地址</font></div>
</center>

```Python
def address(str):    #定义提取公告地址函数
    return str.split('"')[1]
data["公告地址"] = data["公告地址"].apply(address)
```

&emsp;

### 2.2 下载年报 PDF 文件
打开年报地址，可以看到年报页面是由年报 `PDF` 超链接和年报内容两部分组成。其中，`PDF` 超链接可以通过 `Xpath` 或 `正则表达式` 获取。

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/爬取上市公司公告-图4-秦利宾.png"
    width="700">
    <div><font size=2>年报地址内容</font></div>
</center>

 ```Python
 #利用Xpath提取年报PDF链接
def pdf_url(url):
    html = requests.get(url).text #获取网页源代码
    tree = etree.HTML(html)  #解析网页
    url = tree.xpath("//div[2]/a/@href") #获取PDF链接
    return "http://news.windin.com/ns/" + url[0]
data["PDF地址"] = data["公告地址"].apply(pdf_url)
 ```

```Python
#利用正则表达式提取年报PDF链接
def pdf_url(url):
    html = requests.get(url).text
    url = re.findall("(?<=href=).*(?=class)", html) #?<= 正则肯定式向后看 ?= 正则肯定式向前看   
    return "http://news.windin.com/ns/" + url[0] 
data["PDF地址"] = data["公告地址"].apply(pdf_url)
```

根据提取到的年报 PDF 链接下载文件。

```Python
for index, row in data.iterrows():
    name = row["证券代码"][:6] + "_" + row["公告日期"] + ".pdf" #文件名称
    url = row["PDF地址"]      #pdf地址
    times = 1                 #失败后，重新获取次数
    while times <= 3:         #3次都失败后跳出循环
        try:
            urlretrieve(url, filename =  r"./PDF/" + name) #下载pdf
            print(f"成功下载{name}！")
            break
        except:
            times += 1 
            print(f"休息5秒！再试第{times}次!")
            time.sleep(5)
print("成功下载所有PDF文件！")
```

&emsp;

## 3. 爬取 CSMAR 上市公司年报

首先，打开 `CSMAR` 数据库，按照`市场咨询系列`->`公告`->`公告分类关联表和公告证券关联表`步骤，下载上市公司公告信息。其中，年报正文分类编码为 `01030101`。

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/爬取上市公司公告-图5-秦利宾.png"
    width="700">
    <div><font size=2> CSMAR 公告信息</font></div>
</center>

其次，根据公告分类关联表和公告证券关联表共同字段，合并两个表。

```Python
data1 = pd.read_excel("ANN_Classify.xlsx")[2:] #剔除前两行中文标题和单位
data2 = pd.read_excel("ANN_Security.xlsx")[2:]
data = pd.merge(data1, data2, on = ["AnnouncementID", "DeclareDate", "FullDeclareDate", "Title"])
```

<center>
    <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/爬取上市公司公告-图6-秦利宾.png"
    width="700">
    <div><font size=2>分类和证券关联表合并</font></div>
</center>

最后，生成年报 PDF 链接，并下载。

```Python
#巨潮咨询网年报链接形式
#http://static.cninfo.com.cn/finalpage/2020-04-14/1207489969.PDF
#生成年报PDF链接
data["pdf_url"] = "http://static.cninfo.com.cn/finalpage/" + data["DeclareDate"] + "/" + data["AnnouncementID"] + ".PDF"
#根据PDF链接下载
for index, row in data.iterrows(): 
    name = row["Symbol"] + "_" + row["DeclareDate"] + ".pdf" #文件名称
    print(name)
    url = row["pdf_url"]       #pdf地址
    times = 1                  #失败后，重新获取次数
    while times <= 3:         #3次都失败后跳出循环
        try:
            urlretrieve(url, filename =  r"./PDF/" + name)
            print(f"成功下载{name}！")
            break
        except:
            times += 1 
            print(f"休息5秒！再试第{times}次!")
            time.sleep(5)
print("成功下载所有PDF文件！")
```

&emsp;

## 4. 总结

通过数据库提供的公告信息，可以大大简化爬取公告难度。同时，相较于 `CSMAR` 而言，`Wind` 提供的公告信息更加详尽。

当然，爬取公告只是文本分析的开始，后续还有很多工作要做，如提取文本、分词、词频统计、关键词提取、情感分析、以及文本相似度计算等。

其中，从 `PDF` 中提取信息就是一项繁杂的工作。我们应该尽力去找那些以 `网页形式` 展示公告内容的网站，如 [Wind](http://news.windin.com/ns/bulletin.php?code=F5DEDA747AFD&id=113296374&type=1)、[网易财经](http://quotes.money.163.com/f10/ggmx_600006_6184563.html)等，其次可以考虑用专门的 `PDF转换软件` 或 `API` 接口将 `PDF` 转换为`TXT`，

最后，可考虑 `Python` 的 `pdfminer3k` 和 `pdfplumber` 等包，并且 `pdfplumber` 可以很好提取表格数据。

关于图片文字识别，可以参考 `百度AI` 提供的[文字识别](https://ai.baidu.com/tech/ocr_others/webimage) `API` 接口。





&emsp;

> #### [连享会 - 效率分析专题](https://www.lianxh.cn/news/a94e5f6b8df01.html)，2020年5月29-31日   
> 主讲嘉宾：连玉君 | 鲁晓东 | 张宁      
> [课程主页](https://www.lianxh.cn/news/a94e5f6b8df01.html)，[微信版](https://mp.weixin.qq.com/s/zRotMGebIQqaMih8B71fdg)，[码云版](https://gitee.com/arlionn/TE)，[PDF版](https://quqi.gblhgk.com/s/880197/g3ne5HdXdrSoo8iL)

![连享会-效率分析专题视频](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-效率专题海报-500.png "连享会-效率分析专题视频，三天课程，随时学")

&emsp; 

&emsp;




## 5. 附：文中涉及的主要 Python 代码

完整 `.py` 文件​，可以到百度云盘下载：
- 链 接：`https://pan.baidu.com/s/1Vr_-YCSJAmyoKvfayTQqpw`   
- 提取码：`u7ku`

如下是文中使用的主要代码：


```Python
#导入相应的包
import os      
import pandas as pd 
import requests
import re
from lxml import etree #解析网页
from urllib.request import urlretrieve #下载网络文件到本地
import time 

#设置地址
os.chdir(r"D:\爬取上市公告") #修改为自己文件路径
os.getcwd()

#爬取上市公司年报_Wind
data = pd.read_excel("公司公告.xlsx")[:-1] #读入数据，并删除最后一行（最后一行为空值）
def address(str):            #定义提取公告地址函数
    return str.split('"')[1]
data["公告地址"] = data["公告地址"].apply(address)
def pdf_url(url): #利用Xpath提取年报PDF链接
    html = requests.get(url).text
    tree = etree.HTML(html)  #解析网页
    url = tree.xpath("//div[2]/a/@href") #获取PDF链接
    return "http://news.windin.com/ns/" + url[0]
data["PDF地址"] = data["公告地址"].apply(pdf_url)
for index, row in data.iterrows(): #下载年报
    name = row["证券代码"][:6] + "_" + row["公告日期"] + ".pdf" #文件名称
    url = row["PDF地址"] #pdf地址
    times = 1                  #失败后，重新获取次数
    while times <= 3:          #3次都失败后跳出循环
        try:
            urlretrieve(url, filename =  r"./PDF/" + name)
            print(f"成功下载{name}！")
            break
        except:
            times += 1 
            print(f"休息5秒！再试第{times}次!")
            time.sleep(5)
print("成功下载所有PDF文件！")


#爬取上市公司年报_CSMAR

data1 = pd.read_excel("ANN_Classify.xlsx")[2:] #剔除前两行中文标题和单位
data2 = pd.read_excel("ANN_Security.xlsx")[2:]
data = pd.merge(data1, data2, on = ["AnnouncementID", "DeclareDate", "FullDeclareDate", "Title"])
data["pdf_url"] = "http://static.cninfo.com.cn/finalpage/" + data["DeclareDate"] + "/" + data["AnnouncementID"] + ".PDF" #根据巨潮资讯网年报链接形式，生产PDF年报链接
for index, row in data.iterrows():  #下载前年报
    name = row["Symbol"] + "_" + row["DeclareDate"] + ".pdf" #文件名称
    print(name)
    url = row["pdf_url"]       #pdf地址
    times = 1                  #失败后，重新获取次数
    while times <= 3:          #3次都失败后跳出循环
        try:
            urlretrieve(url, filename =  r"./PDF/" + name)
            print(f"成功下载{name}！")
            break
        except:
            times += 1 
            print(f"休息5秒！再试第{times}次!")
            time.sleep(5)
print("成功下载所有PDF文件！")  
```



&emsp; 

> **连享会-直播课** 上线了！         
>  <http://lianxh.duanshu.com>  

> **免费公开课：**
> - [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟
> - [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. 
> - 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等)




&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- **你的颈椎还好吗？** 您将 [::连享会-主页::](https://www.lianxh.cn) 和 [::连享会-知乎专栏::](https://www.zhihu.com/people/arlionn/) 收藏起来，以便随时在电脑上查看往期推文。
- **公众号推文分类：** [计量专题](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=4&sn=0c34b12da7762c5cabc5527fa5a1ff7b) | [分类推文](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=2&sn=07017b31da626e2beab0332f5aa5f9e2) | [资源工具](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=3&sn=10c2cf37e172289644f03a4c3b5bd506)。推文分成  **内生性** | **空间计量** | **时序面板** | **结果输出** | **交乘调节** 五类，主流方法介绍一目了然：DID, RDD, IV, GMM, FE, Probit 等。
- **公众号关键词搜索/回复** 功能已经上线。大家可以在公众号左下角点击键盘图标，输入简要关键词，以便快速呈现历史推文，获取工具软件和数据下载。常见关键词：
  - `课程, 直播, 视频, 客服, 模型设定, 研究设计, `
  - `stata, plus，Profile, 手册, SJ, 外部命令, profile, mata, 绘图, 编程, 数据, 可视化`
  - `DID，RDD, PSM，IV，DID, DDD, 合成控制法，内生性, 事件研究` 
  - `交乘, 平方项, 缺失值, 离群值, 缩尾, R2, 乱码, 结果`
  - `Probit, Logit, tobit, MLE, GMM, DEA, Bootstrap, bs, MC, TFP`
  - `面板, 直击面板数据, 动态面板, VAR, 生存分析, 分位数`
  - `空间, 空间计量, 连老师, 直播, 爬虫, 文本, 正则, python`
  - `Markdown, Markdown幻灯片, marp, 工具, 软件, Sai2, gInk, Annotator, 手写批注`
  - `盈余管理, 特斯拉, 甲壳虫, 论文重现`
  - `易懂教程, 码云, 教程, 知乎`


---

![连享会主页  lianxh.cn](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会跑起来就有风400.png "连享会主页：lianxh.cn")


> 扫码加入连享会微信群，提问交流更方便

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-学习交流微信群001-150.jpg)

