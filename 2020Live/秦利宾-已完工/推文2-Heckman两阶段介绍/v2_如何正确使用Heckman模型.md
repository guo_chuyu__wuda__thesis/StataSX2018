作者：秦利宾 (厦门大学)  
邮箱：qlb150@163.com

**[编者按]:** 本文主要内容摘译自以下文章。同时，结合网上资料和个人经验整理了 **Stata** 命令 **Heckman**、**etregress**、以及手动计算两步法的使用方法，以供大家参考。

**[Source]:**  [Lennox C S, Francis J R, Wang Z. Selection models in accounting research[J]. The accounting review, 2012, 87(2): 589-616.](https://doi.org/10.2308/accr-10195)

[toc]

## 1. 背景

社会科学数据大部分都是非随机分配，使得选择模型在研究中的作用越来越重要，编辑和审稿人也通常会要求作者控制内生性和选择偏差。但是，学者在使用中也存在一些问题，如忽视了排他性约束变量的重要性。选择模型通常很脆弱，任意或不设排他性约束变量，会使得结论非常不稳健。接下来，将针对该问题予以说明，并提供一些有益的建议。

## 2. 选择性偏差校正

### 2.1 选择模型

**选择模型**有两种主要应用，一是**处理效应 (treatment effect model)**，即回归模型中包含一个内生的指示变量 (*D*)。例如，管理层是否发布盈余预测 (*D* = 1 或 0) 对资本成本影响。二是**样本选择 (sample selection model)**，即回归样本为一个子样本。例如，在管理层发布盈余预测子样本中 (*D* = 1)，对管理层盈余预测精确度的影响因素进行分析。在上述模型中，内生的 *D* 都会导致有偏估计。

处理效应模型 (treatment effect model) 如下：

$$ Y=\beta^{\prime} X+\theta D+u \tag1 $$

其中，*X* 包含截距项和一组影响 *Y* 的外生解释变量。指示性变量 *D* 可以通过以下二元选择模型进行回归得到：

$$ D^{*}=a_{0}^{\prime} Z+a_{1}^{\prime} X+v \tag2 $$

如果 $D^{*}\geq 0$，则 $D = 1$，反之 $D^{*} < 0$，则 $D = 0$。

通常，式 (1) 和式 (2) 的随机误差项 $u$ 和 $v$ 服从二元正态分布，其均值为 0，协方差矩阵为： 

$$ \left[\begin{array}{cc}
\sigma^{2} & \rho \sigma \\
\rho \sigma & 1
\end{array}\right] $$

如果随机误差项 $u$ 和 $v$ 相关 ($\rho \neq 0$)，则 $E(u|D) \neq 0$，使得式 (1) 中的 **OLS** 估计量 $\theta$ 有偏。**Heckman** 方法就是通过式 (2) 构造**逆米尔斯比率** (*MILLS*) 控制这个偏差。*MILLS* 计算如下：

<span id="jump">

$$ MILLS=E(u | D)=\left\{\begin{array}{cc}
\varphi\left(\hat{\alpha}_{0}^{\prime} Z+\hat{\alpha}_{1}^{\prime} X\right) / \Phi\left(\hat{\alpha}_{0}^{\prime} Z+\hat{\alpha}_{1}^{\prime} X\right) & \text { if } D=1 \\
-\varphi\left(\hat{\alpha}_{0}^{\prime} Z+\hat{\alpha}_{1}^{\prime} X\right) /\left(1-\Phi\left(\hat{\alpha}_{0}^{\prime} Z+\hat{\alpha}_{1}^{\prime} X\right)\right) & \text { if } D=0
\end{array}\right. $$ 

</span>

其中，$\varphi(.)$ 和 $\Phi(.)$ 分别为密度函数和累积分布函数。然后，将计算得到 *MILLS* 加入式 (1) 进行回归：

$$ Y=\beta^{\prime} X+\theta D+\rho \sigma MILLS+\varepsilon \tag3 $$

此时，式 (3) 中随机误差项 $\varepsilon$ 与 *D* 不相关，$\theta$ 为无偏估计量。并且，可以通过 *MILLS* 显著性和系数判断选择偏差是否存在以及方向。当然，式 (2) 和 式 (3) 可以用传统两步法或最大似然估计法。 

上述为处理效应模型 (treatment effect model)，除式 (3) 是以子样本回归外，样本选择模型与处理效应模型 (sample selection model)类似。例如，对管理层预测精确度 (*Y*) 分析中，回归样本为发布盈余预测的公司 (*D* = 1)。此时，式 (3) 为 $Y=\beta^{\prime}X+\theta+\rho\sigma MILLS_{1}+\varepsilon$，$MILLS_{1}$ 计算如下： 
 
$$ MILLS_{1}=\varphi\left(\hat{\alpha}_{0}^{\prime} Z+\hat{\alpha}_{1}^{\prime} X\right) / \Phi\left(\hat{\alpha}_{0}^{\prime} Z+\hat{\alpha}_{1}^{\prime} X\right) $$

或者，对于 *D* = 0 的子样本回归中，式 (3) 为 $Y=\beta^{\prime}X+\rho\sigma MILLS_{0}+\varepsilon$，$MILL_{0}$ 计算如下：
$$ MILLS_{0}=-\varphi\left(\hat{\alpha}_{0}^{\prime} Z+\hat{\alpha}_{1}^{\prime} X\right) /\left(1-\Phi\left(\hat{\alpha}_{0}^{\prime} Z+\hat{\alpha}_{1}^{\prime} X\right)\right) $$

### 2.2 应用中存在的问题

选择模型式 (3) 与 OLS 模型式 (1) 的区别在于前者加入了逆米尔斯比率 (*MILLS*)。*MILLS* 可以识别选择性偏差主要是以下原因：

- *MILLS* 是 *X* 和 *Z* 变量的非线性函数
- *Z* 变量从式 (3) 中已经排除

其中，*Z* 变量被假定为不会对 *Y* 变量产生直接的影响，只能通过 *MILLS* 产生间接影响，因此也被称为**排他性约束 (exclusion restrictions)**。

排他性约束变量，也就是 *Z* 变量，在控制选择模型内生性问题过程中具有非常重要的作用。首先，*Z* 变量必须是外生的，否则选择模型第一阶段回归 (式 2) 系数以及 *MILLS* 会产生偏误；其次，*Z* 变量对 *D* 具有较强的解释能力，使得 *MILLS* 能够更好的检测和控制选择偏差；最后，式 (3) 中必须有效排除 *Z* 变量的影响，即 *Z* 变量只能通过 *MILLS* 对 *Y* 产生间接影响，否则 *MILLS* 与随机误差项相关，使得 *MILLS* 系数估计有偏，不能有效控制 *D* 变量的内生性。

找到一个好的 *Z* 变量是困难的。在没有排他性约束情况下，即没有 *Z* 变量，仍可以通过 *MILLS* 的非线性去识别偏差。但可能面临以下两个问题：

- 由于只能通过 *MILLS* 的非线性识别偏差，非线性模型被错误设定为线性模型会被 *MILLS* 吸收。

- 在式 (3) 中，*MILLS* 与 *X* 和 *D* 相关，这种相关性在没有排他性约束变量 (*Z*) 情况下更加严重。

进一步，高的共线性会产生以下两个问题：

- 高共线性会使得系数的标准差变大，降低了系数的显著性。*MILLS* 系数可能会变得不显著，进而得出错误结论。

- 在模型被正确设定下，即使存在高的共线性问题，系数也可以被无偏的估计。但是，事实上，选择模型被错误设定概率是很高的。若模型被错误设定，共线性会吸收这种偏差，导致系数估计有偏。

## 3. 选择模型在研究中应用情况

接下来，以审计机构是否是前 5 大对审计费用的研究为例，说明**没有排他性约束变量**和**任意排他性约束变量**对结果的影响。为了说明前者，将第一阶段回归中所有自变量都包括在第二阶段回归中。为了说明后者，将第一阶段中企业规模从第二阶段排除。

下表为选择模型第一阶段回归结果，其中被解释变量为是否是前五大审计机构，**Model 1** 是以总资产自然对数衡量公司规模，**Model 2** 是以营业收入自然对数衡量公司规模。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Heckman介绍-图1-秦利宾.png)

下表 (1)-(4)为选择模型第二阶段回归结果，(5) 和 (6) 普通 OLS 回归结果，其中，被解释变量为审计费用自然对数，(1) 和 (2) 中的 *MILLS* 是根据上表 **Model 1** 计算，(3) 和 (4) 中的 *MILLS* 是根据上表 **Model 2** 计算，同时，(1)、(4)、(5) 是以总资产自然对数控制公司规模，(2)、(3)、(6) 则是以营业收入自然对数衡量公司规模。由于(1) 和 (3) 在第一阶段和第二阶段都是用相同方式控制公司规模，所以没有排他性约束变量，(2) 和 (4) 排除约束变量分别为总资产自然对数和营业收入自然对数。

从结果可以看出，在选择模型第二阶段，$Big_{it}$ 系数对排他性约束的不同设置较为敏感，同时共线性问题也比较严重，相反，OLS 的回归结果更为稳健和共线性也更小。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Heckman介绍-图2-秦利宾.png)

下表为 2000-2009 年会计顶级期刊使用选择模型情况的描述统计。在选择模型第二阶段，有 54 篇具有一个或多个排除约束变量，有 6 篇同时报告了具有和不具有排他性约束变量结果；有 3 篇报告了使用不同排除约束变量的稳健性结果；有3篇报告了排除约束变量经济机制；有3篇报告了内生性变量和 *MILLS* 的多重共线性。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Heckman介绍-图3-秦利宾.png)

## 4. 应用案例

### 4.1 样本选择模型

以工作和教育经历对女性工资影响研究为例，只有参加工作的女性工资数据才能被观察到，而未参加的工作女性工资数据缺失，因此在研究女性工资影响因素时存在偏差。为克服该问题，采用选择模型进行处理。第一阶段，选择女性是否结婚 (*married*) 和孩子数量 (*children*) 作为排他性约束 *Z* 变量，同时控制影响女性工资 (*wage*) 的工作经历 (*age*) 和教育程度 (*educ*) 变量。第二阶段，加入逆米尔斯比率 (*MILLS*) 进行回归。接下来，将分别以 **OLS**、**Heckman Maximum Likelihood**、**Heckman Two-Step**、以及手工计算两步法进行回归对比。

```Stata
use womenwk.dta, clear

*ols
reg wage educ age
est store OLS

*heckman maximum likelihood
heckman wage educ age, select(married children educ age) //默认最大似然估计
est store HeckMLE

*heckman two-step  all-in-one 不可以进行cluster调整
heckman wage educ age, select(married children educ age) twostep
est store Heck2s

*heckman two-step  step-by-step 可以进行cluster调整
probit work married children educ age
est store First
predict y_hat, xb
gen pdf = normalden(y_hat)  //概率密度函数
gen cdf = normal(y_hat)     //累积分布函数
gen imr = pdf/cdf           //计算逆米尔斯比率
reg  wage educ age imr if work == 1  //女性工作子样本
est store Second
vif  //方差膨胀因子

*对比结果
local m "OLS HeckMLE Heck2s First Second"
esttab `m', mtitle(`m') nogap compress pr2 ar2
```

```
---------------------------------------------------------------------------
                 (1)          (2)          (3)          (4)          (5)   
                 OLS      HeckMLE       Heck2s        First       Second   
---------------------------------------------------------------------------
main                                                                       
education      0.897***     0.990***     0.983***    0.0584***     0.983***
             (18.00)      (18.59)      (18.23)       (5.32)      (19.46)   
age            0.147***     0.213***     0.212***    0.0347***     0.212***
              (7.83)      (10.34)       (9.61)       (8.21)      (10.25)   
married                                               0.431***             
                                                     (5.81)                
children                                              0.447***             
                                                    (15.56)                
imr                                                                4.002***
                                                                  (6.93)   
_cons          6.085***     0.486        0.734       -2.467***     0.734   
              (6.84)       (0.45)       (0.59)     (-12.81)       (0.63)   
---------------------------------------------------------------------------
select                                                                     
married                     0.445***     0.431***                          
                           (6.61)       (5.81)                             
children                    0.439***     0.447***                          
                          (15.79)      (15.56)                             
education                  0.0557***    0.0584***                          
                           (5.19)       (5.32)                             
age                        0.0365***    0.0347***                          
                           (8.79)       (8.21)                             
_cons                      -2.491***    -2.467***                          
                         (-13.16)     (-12.81)                             
---------------------------------------------------------------------------
/                                                                          
athrho                      0.874***                                       
                           (8.62)                                          
lnsigma                     1.793***                                       
                          (64.95)                                          
---------------------------------------------------------------------------
/mills                                                                     
lambda                                   4.002***                          
                                        (6.60)                             
---------------------------------------------------------------------------
N               1343         2000         2000         2000         1343   
adj. R-sq      0.252                                               0.278   
pseudo R~q                                            0.189                
---------------------------------------------------------------------------
t statistics in parentheses
* p<0.05, ** p<0.01, *** p<0.001

. vif  //方差膨胀因子
    Variable |       VIF       1/VIF  
-------------+----------------------
         imr |      1.39    0.719868
         age |      1.31    0.764000
   education |      1.10    0.906166
-------------+----------------------
    Mean VIF |      1.27

```

### 4.2 处理效应模型

以女性工会成员身份对工资影响研究为例。女性是否加入工会是一种自选择行为，会受到许多因素影响。为克服该问题，采用选择模型进行处理。第一阶段，选择女性是否生活在南方州 (*south*)  作为排他性约束 *Z* 变量，同时控制是否非洲裔 (*black*) 和工作年限 (*tenure*)。第二阶段，加入逆米尔斯比率 (*MILLS*) 进行回归。接下来，将分别以 **OLS**、**etregress Maximum Likelihood**、**etregress Two-Step**、以及手工计算两步法进行回归对比。值得注意的是，逆米尔斯比率要在 $D = 1$ 和 $D = 0$ 两种情况下分别计算，正如本文在 [**2.1**](#jump) 部分所列示的 *MILLS* 计算过程一样。具体可参考 [Mills’ ratios and censoring direction in the Heckman selection model](https://www.stata.com/support/faqs/statistics/inverse-mills-ratio/)。

```Stata
*--------------处理效应模型-------------
use union3.dta, clear

*ols
reg wage age grade smsa black tenure union
est store OLS

*etregress maximum likelihood
etregress wage age grade smsa black tenure,  ///
          treat(union = south black tenure) twostep 
est store etrMLE

*etregress two-step all-in-one  不可以进行cluster调整
etregress wage age grade smsa black tenure,  ///
          treat(union = south black tenure) twostep 
est store etr2s	

*etregress two-step  step-by-step 可以进行cluster调整
probit union south black tenure
est store First
predict y_hat, xb
gen pdf = normalden(y_hat)
gen cdf = normal(y_hat)
gen imr1 = pdf/cdf            //union = 1算法
gen imr2 = (-pdf) / (1 - cdf) //union = 0算法
gen imr = imr1 
replace imr = imr2 if union == 0
reg wage age grade smsa black tenure union imr
est store Second
vif

*对比结果
local m "OLS etrMLE etr2s First Second"
esttab `m', mtitle(`m') nogap compress pr2 ar2
```

```stata

---------------------------------------------------------------------------
                 (1)          (2)          (3)          (4)          (5)   
                 OLS       etrMLE        etr2s        First       Second   
---------------------------------------------------------------------------
main                                                                       
age            0.148***     0.154***     0.154***                  0.154***
              (7.56)       (7.92)       (7.92)                    (7.94)   
grade          0.437***     0.423***     0.423***                  0.423***
             (14.82)      (14.56)      (14.56)                   (14.35)   
smsa           0.975***     0.863***     0.863***                  0.863***
              (7.79)       (6.71)       (6.71)                    (6.79)   
black         -0.618***    -0.921***    -0.921***     0.440***    -0.921***
             (-4.94)      (-5.19)      (-5.19)       (4.52)      (-6.45)   
tenure         0.212***     0.100        0.100       0.0998***     0.100*  
              (6.26)       (1.93)       (1.93)       (4.22)       (2.37)   
union          1.004***     4.564***     4.564***                  4.564***
              (7.50)       (4.53)       (4.53)                    (5.45)   
south                                                -0.490***             
                                                    (-5.24)                
imr                                                               -2.093***
                                                                 (-4.31)   
_cons         -4.326***    -4.670***    -4.670***    -0.968***    -4.670***
             (-8.14)      (-8.65)      (-8.65)     (-12.97)      (-8.75)   
---------------------------------------------------------------------------
union                                                                      
south                      -0.490***    -0.490***                          
                          (-5.24)      (-5.24)                             
black                       0.440***     0.440***                          
                           (4.52)       (4.52)                             
tenure                     0.0998***    0.0998***                          
                           (4.22)       (4.22)                             
_cons                      -0.968***    -0.968***                          
                         (-12.97)     (-12.97)                             
---------------------------------------------------------------------------
hazard                                                                     
lambda                     -2.093***    -2.093***                          
                          (-3.61)      (-3.61)                             
---------------------------------------------------------------------------
N               1210         1210         1210         1210         1210   
adj. R-sq      0.337                                               0.346   
pseudo R~q                                            0.046                
---------------------------------------------------------------------------
t statistics in parentheses
* p<0.05, ** p<0.01, *** p<0.001

. vif
    Variable |       VIF       1/VIF  
-------------+----------------------
       union |     40.76    0.024532
         imr |     39.72    0.025173
      tenure |      1.79    0.558305
       black |      1.37    0.730327
         age |      1.14    0.878075
       grade |      1.08    0.928859
        smsa |      1.06    0.939479
-------------+----------------------
    Mean VIF |     12.42
```

关于选择模型 all-in-one 和 step-by-step 两种方法差别，参考 [Heckman Two-Step Model](https://www.statalist.org/forums/forum/general-stata-discussion/general/1484793-heckman-two-step-model) 和 [Stata commands to do Heckman two steps](http://kaichen.work/?p=1198)。关于选择模型第一阶段是否要包含第二阶段全部外生解释变量，请参考 [工具变量法（五）: 为何第一阶段回归应包括所有外生解释变量](https://mp.weixin.qq.com/s/pw5lRQxkj20iUntSGltLSw)，值得注意的是，选择模型中 *MILLS* 是一种非线性，因此不包含第二阶段全部外生解释变量引起的内生性问题可能并没有工具变量法那么严重。

## 5. 选择模型使用建议

- 选择模型要有排他性约束变量，即在第一阶段回归中包含，而在第二阶段回归中排除。

- 选择模型要报告第一阶段回归结果，否则不清楚哪个变量是排他性约束变量或评估排他性约束变量的解释能力。

- 需要证明为什么排他性约束变量，即 *Z* 变量，可以从第二阶段回归中有效排除。

- 由于选择模型是脆弱的，敏感性分析是必要的，如与不同排他性约束变量结果和OLS 结果对比、以及报告内生性变量和逆米尔斯比率的 VIF 值。

- 在使用过程中，可以综合借鉴 Lennox and Francis(2012)[<sup>1</sup>](#refer-anchor-1)，Kim and Zhang(2016)[<sup>2</sup>](#refer-anchor-2)，李小荣和刘行 (2012)[<sup>3</sup>](#refer-anchor-3)。

## 6. 结论

最后，借用文章作者原话予以总结。

>In short, researchers are faced with the following trade-off. On the one hand, selection models can be fragile and suffer from multicollinearity problems, which hinder their reliability. On the other hand, the selection model potentially provides more reliable inferences by controlling for endogeneity bias if the researcher can find good exclusion restrictions, and if the models are found to be robust to minor specification changes. The importance of these advantages and disadvantages depends on the specific empirical setting, so it would be inappropriate for us to make a general statement about when the selection model should be used. Instead, researchers need to critically appraise the quality of their exclusion restrictions and assess whether there are problems of fragility and multicollinearity in their specific empirical setting that might limit the effectiveness of selection models relative to OLS. 

## 7. 参考文献
<div id="refer-anchor-1"></div>

[1] [Lennox C S, Francis J R, Wang Z. Selection models in accounting research[J]. The accounting review, 2012, 87(2): 589-616.](https://doi.org/10.2308/accr-10195)

<div id="refer-anchor-2"></div>

[2] [Kim C, Zhang L. Corporate political connections and tax aggressiveness[J]. Contemporary Accounting Research, 2016, 33(1): 78-114.](https://doi.org/10.1111/1911-3846.12150)

<div id="refer-anchor-3"></div>

[3] [李小荣, 刘行. CEO vs CFO: 性别与股价崩盘风险[J]. 世界经济, 2012, 12: 102-129.](http://www.cnki.com.cn/Article/CJFDTotal-SJJJ201212009.htm)
