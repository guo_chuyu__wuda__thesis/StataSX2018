## 1 fillmissing用法简介

### 一些常见的数据缺失情形

- 缺前面的
- 缺后面的
- 文字型缺失值：如性别，行业归属等

### 补缺方法

均值插值 (补充参考文献或推文链接)、多重补漏分析 (不是本文的内容，但提供 Stata 命令或资料链接)

本文介绍的方法适用于 …… 情形：

- xxx
- xxx
- 
>特别说明：This program does not imply that filling missing values is justified by theory. Users should make their own decisions and follow appropriate theory while filling missing values.

##  2 基本用法
`fillmissing varname [if] [in]`

## 3 进阶用法
`[bysort varlist]: fillmissing varname [if] [in], [with(with_options)]`

选项说明：
>- with(any)
>- with(previous)
>- with(next)
>- with(first)
>- with(last)	
>- with(mean)
>- with(median)
> -with(min)	
> -with(max)
>- bysort varlist

## 4 示例
按照上面的10个选项，分别举例说明

## 参考资料和文献
