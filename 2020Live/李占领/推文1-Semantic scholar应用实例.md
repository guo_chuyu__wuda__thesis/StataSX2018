## Semantic scholar应用实例
### ——一款基于机器学习的学术搜索引擎简介
&emsp;
> **作者：** 李占领 (中南财经政法大学)      
> **E-mail:**lizhanling888@163.com     
&emsp;
## 1 Semantic scholar简介
2015年11月2日，微软联合创始人Paul Allen旗下的艾伦人工智能研究所 （Allen Institute for Artificial Intelligence，AI2）发布了一款名为 Semantic Scholar的免费学术搜索引擎，目标是“破除混沌（cut through the clutter）”，帮助科研用户从浩如烟海的文献中快速筛选有用信息，减少检索时间，提升工作效率。

Semantic Scholar 利用机器学习技术，可以从文献文本中挑选出最重要的关键词或短语，确定文献的研究主题，也可以从文献中提取图表，呈现在文献检索页面，能够帮助使用者快速理解文献的主要内容。对于科学研究人员来说，Semantic Scholar 的较大用处是可以帮助他们快速获得重要文献，因为该引擎可以辨别一篇文章引用的参考文献是否具有重要的参考价值.

Semantic scholar从最初收录的计算机科学领域的300万篇文献，经过4年多的发展，截止2020年5月8日，已收录文献1.87亿篇，涵盖经济、管理等19个领域的文献。
下面，本文以检索美国著名经济学家，全世界引用率较高的经济学家之一，尤金·法玛（Eugene F. Fama）教授发表的学术论文为例，介绍Semantic scholar的常规信息检索功能和特色功能。

## 2 常规信息检索功能
Semantic scholar搜索引擎网址：https://www.semanticscholar.org/

检索页面如下图：

 <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/李占领-Semantic Scholar搜索引擎简介-图1搜索界面.png"/>

在搜索框内，输入“Eugene F. Fama”，点击“More Filters”，可以看到如下页面：

 <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/李占领-Semantic Scholar搜索引擎简介-图2搜索结果.png"/>

同时把“Eugene F. Fama”分别放入微软学术和谷歌学术进行检索，以比较三者之间检索结果的区别，具体检索结果如下：

<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/李占领-Semantic Scholar搜索引擎简介-图3搜索对比1.png"/>

<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/李占领-Semantic Scholar搜索引擎简介-图3搜索对比2.png"/>

>在微软学术的检索结果页面出现了136篇文献，显示的被引用次数为252802，而在谷歌学术中得到了约40900条文献，数量虽然很多，但大量的文献不是学术论文类型，假若使用者利用该引擎进行检索作者发文情况时，稍显不便。在Semantic scholar与前两个学术搜索引擎相比，其提供了丰富的二次检索功能，初步检索出的相关文献是183篇。

以《Risk, Return, and Equilibrium: Empirical Tests》为例，看看Semantic scholar提供了哪些信息。
 
 <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/李占领-Semantic Scholar搜索引擎简介-图4论文显示.png"/>

>- View PDF（本文提供了pdf版本可以下载，但不是所有的文献都能下载pdf版本）
>- Abstract（摘要）
>- Tables（图表和主题）
>- Citations（被引用情况）
>- References（参考文献）
>- Related Papers（相关文献）
>- Cite（提供了BibTex、EndNote、MLA、APA、Chicago等格式）

## 3 特色功能
### （1）被引用情况分类
Semantic Scholar提前为使用者对引用的文献进行了分类，引用情况共分为<span style="color:rgb(248,57,41);font-weight:bold;">高影响力引用次数，引用方法，引用背景和引用结果四类。
>该分类为我们追踪文献的后续引用情况提供了极大的便利。
> - 若想模仿作者的研究话题进行后续深入研究，可以点击“Highly Influential Citations”链接，选择其中提供的文献
> - 如果仅想模仿作者的研究方法，可以点击“Cite Methods”链接中的文献进行阅读。
> - Semantic Scholar还提供了相关主题的文献，能够节约检索者的时间。 

### （2）补充资料
部分文献还提供了补充资料，为了介绍该部分的功能，本文选取了《A Convolutional Encoder Model for Neural Machine Translation》单独进行示例:
> - Supplemental Code模块，可以下载Github代码，方便使用者进行结果的重现；
> - Explore Further模块，提供了更多与本文所讨论主题相关的论文，同Related Papers模块的功能相似。
>- 检索页面还提供了推特Twitter Mentions的数据。

<img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/李占领-Semantic Scholar搜索引擎简介-图5补充资料.png"/>

 
### (3)学者影响力评价
点击“Influence”就可以找到Fama教授学术影响力评价图，具体如下：
 
 <img src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/李占领-Semantic Scholar搜索引擎简介-图6影响力分布.png"/>
 
学者影响力图主要包括学者所发表的文献数量、H指数、总被引次数、高影响力引用次数、学者本人所受其他学者影响情况、受学者本人影响的情况等内容，
>其中“Highly Influential Citations（高影响力引用次数）”，该指标是Semantic Scholar基于对引用文献的分析，包括被引文献在施引文献中的被引次数、每次被引时的位置，以及所引用内容与上下文之间的关系等，利用深度学习技术，建立模型，据此判定施引者对所引用文献的态度，确定引用的价值，该指标具有重要的参考价值。

## 4 结束语
更多关于Semantic schola的功能、使用技巧或问题详见链接https://www.semanticscholar.org/faq，或者点击百度云盘分享链接下载已整理好的Pdf版本，链接: https://pan.baidu.com/s/1RhjzwfOSB-6prbBXYwmjkg 提取码: 6ju2 

## 参考文献
Miwa, M., & Bansal, M. (2016). End-to-End Relation Extraction using LSTMs on Sequences and Tree Structures. ArXiv, abs/1601.00770.