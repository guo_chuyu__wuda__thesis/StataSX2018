
&emsp;

> **杨冬：** (重庆大学)    
> **E-Mail:** yang.dong@cqu.edu.cn     

> `2020/6/15 13:01`

本文主要内容来自如下论文，有兴趣的小伙伴可自行下载了解详细内容

**[Source]:**  [Ghysels E, Kvedaras V, Zemlys V. Mixed Frequency Data Sampling Regression Models: The R Package midasr[J]. Journal of Statistical Software, 2016, 72(4).](https://10.18637/jss.v072.i04)  


&emsp;


## 混频数据抽样（MIDAS）模型

自[Ghysels(2002)等](https://escholarship.org/uc/item/9mf223rs)提出MIDAS模型以来，由于其处理混频数据的便利性而被广泛应用于金融波动率和宏观经济预测等领域。本文将从如下三个方面对如何基于R语言的MIDAS模型在宏观经济预测方面的应用做简要的介绍。

- <span style="color:orangered;font-weight:bold;">MIDAS 模型设定</span>
- <span style="color:#773098;font-weight:bold;">MIDAS 模型估计</span>
- <span style="color:#35b378;font-weight:bold;">MIDAS 模型应用</span>

## 1 MIDAS 模型设定
假定{$y_t, t \in Z$}为低频可观测的一元时间序列，其滞后算子用$B$表示，即$By_t=y_{t-1}$。MIDAS回归涉及到对随机过程{$x_{\tau}^{(i)}, \tau \in Z$}，$i=0,\dots,k$的线性投影。其中$x_{\tau}^{(i)}$为高频可观测序列，即在对应的每一个低频时期$t=t_0$，我们观测到的$x_{\tau}^{(i)}$的时期$\tau=(t_0-1)m_i+1,\dots,t_0m_i$，其中$m_i \in N$为高频解释变量相对于低频解释变量抽样频率的倍差，当$m_i=1$时，MIDAS模型退化为同频模型。高频解释变量$x_{\tau}^{(i)}$滞后算子用$L$表示，即$Lx_{\tau}^{(i)}=x_{\tau-1}^{(i)}$。基于上述设定，MIDAS模型可表述为如下形式：
$$y_t-\alpha_1y_{t-1}-\cdots-\alpha_py_{t-p}=\sum_{i=0}^{k}\sum_{j=0}^{l_i}\beta_j^{(i)}x_{tm_{i}-j}^{i}+\varepsilon_t$$

上述模型设定形式可采用通常的时间序列回归或贝叶斯方法进行估计。但随着模型中滞后阶数的增加，待估参数$d=p+\sum_{i}^{k}l_i$也会快速增加。为了解决这一问题，[Ghysels(2002)等](https://escholarship.org/uc/item/9mf223rs)建议采用一个充分灵活的函数形式对原参数进行约束，即

$$\beta_j^{(i)}=f_i(\gamma_i,j),j=0,\dots,l_i,\gamma_i=(\gamma_1^{(i)},\dots,\gamma_{q_i}^{i}),q_i\in N$$

通过函数化约束可以大大减少待估参数个数，从$d$减少到$q=\sum_{i=0}^{h_i}q_i$。函数化约束虽然可以有效减少待估参数的个数，但也导致模型不再线性化，因此需要采用NLS或极大似然进行估计。为了说明函数化约束的优势，图1分别给出了无约束、正确约束和不正确约束三种条件下均方误差和估计值与真实参数差异的结果。

![](https://images.gitee.com/uploads/images/2020/0615/112926_e7097bbe_7372353.jpeg)

由图1结果可知，随着样本容量的增加，即使不正确的约束也表现出了较为理想的效果。

更为常见的，上述MIDAS模型是以一个紧集的形式表述：

$$\alpha(B)y_t=\boldsymbol{\beta(L)^{\text{T}}x_{t,0}}+\varepsilon_t$$

其中$\alpha(z)=1-\sum_{j=1}^{p}\alpha_jz^j$，且

$$\boldsymbol{x_{t,0}}:=(x_{tm_0}^{0},\dots,x_{tm_i}^{i},\dots,x_{tm_l}^{l})^\text{T},$$

$$\boldsymbol{\beta(z)}=\sum_{j=0}^l\beta_jz^j,\boldsymbol{\beta_j}=(\beta_{j}^{0},\dots,\beta_{j}^{i},\dots,\beta_{j}^{l})^\text{T},$$

$$\boldsymbol{L^jx_{t,0}}:=\boldsymbol{x_{t,j}}=(L^jx_{tm_0}^{0},\dots,L^jx_{tm_i}^{i},\dots,L^jx_{tm_l}^{l})^\text{T}.$$

## 2 MIDAS 模型估计
上述紧集的表述形式可以直接采用OLS估计而无需对参数进行约束，因而该模型也被[Foroni, et al. (2015)](https://rss.onlinelibrary.wiley.com/doi/abs/10.1111/rssa.12043)称为无约束的MIDAS（U-MIDAS）模型。对于存在约束的MIDAS（R-MIDAS）模型，我们可以采用NLS估计：
$$\hat{\gamma}=\mathop{\arg\min}_{\gamma \in \mathbb{R^q}} \sum_{[(l+1)/m]}^n (\alpha(B)y_t-\boldsymbol{f_{\gamma}}(L)^T \boldsymbol{x_{t,0}})^2 $$

其中，有约束参数的滞后分布多项式定义为如下形式：

$$\boldsymbol{f_{\gamma}}(z)=\sum_{j=0}^{l} \boldsymbol{f_{\gamma,j}}z^j$$
$$\boldsymbol{f_{\gamma,j}}=(f_0(\gamma_0;j),\dots,f_i(\gamma_i;j),\dots,f_k(\gamma_k;j))^\text{T}$$

## 3 MIDAS 模型应用
R中用于建立MIDAS模型的程序包名为**midasr**，在使用该模型前，我们需要对该程序包进行安装

```R
install.packages("midasr")
```

我们使用该程序包中含有的数据对美国的季度GDP预测为例，其预测方程形式如下：
$$y_{t+1}=\alpha+\rho y_t + \sum_{j=0}^{8} \theta_jx_{3t-j}+\varepsilon_t$$

其中，$y_t$是经过季节调整后的美国季度GDP的对数差分值，$x_t$是月度非农就业总人数的对数差分。在进行建模前，我们对数据做必要的变换和处理。

```R
# input data
data("USqgdp", package = "midasr")
data("USpayems", package = "midasr")
# define the date
y <- window(USqgdp, end = c(2011, 2))
x <- window(USpayems, end = c(2011, 7))
# take log difference 
yg <- diff(log(y)) * 100
xg <- diff(log(x)) * 100
# fill the missing value
nx <- ts(c(NA, xg, NA, NA), start = start(x), frequency = 12)
ny <- ts(c(rep(NA, 33), yg, NA), start = start(x), frequency = 4)
```

上面程序的最后两行是为了保证样本量相对应。进一步地，我们来看一下调整后的数据格式

```R
# check the data
head(nx,9)
            Jan        Feb        Mar        Apr        May        Jun        Jul        Aug        Sep
1939         NA  0.5930978  0.5929035 -0.6161612  0.6821896  0.6644543 -0.2724842  0.7989303  1.1962214

> head(ny,36)
         Qtr1     Qtr2     Qtr3     Qtr4
1939       NA       NA       NA       NA
1940       NA       NA       NA       NA
1941       NA       NA       NA       NA
1942       NA       NA       NA       NA
1943       NA       NA       NA       NA
1944       NA       NA       NA       NA
1945       NA       NA       NA       NA
1946       NA       NA       NA       NA
1947       NA 1.307742 1.531053 3.997397
```
由上述数据格式可知，MIDAS模型需要解释变量和被解释变量对应时期的样本均需列入，没有的观测可用**NA**代替。经过处理后的时间序列图如下所示

![](https://images.gitee.com/uploads/images/2020/0615/112951_48195dc8_7372353.jpeg)

与[Ghysels(2013)](https://escholarship.org/uc/item/9mf223rs)设定形式一致，我们限制估计样本时间从1985年第一季度至2009年第一季度，并分别采用有约束的**Beta**分布多项式MIDAS模型、有约束的非零**Beta**分布多项式MIDAS模型和U-MIDAS模型进行建模。

```R
xx <- window(nx, start = c(1985, 1), end = c(2009, 3))
yy <- window(ny, start = c(1985, 1), end = c(2009, 1))
beta0 <- midas_r(yy ~ mls(yy, 1, 1) + mls(xx, 3:11, 3, nbeta), start = list(xx = c(1.7, 1, 5)))
coef(beta0)
(Intercept) yy xx1 xx2 xx3
0.8315274 0.1058910 2.5887103 1.0201202 13.6867809
betan <- midas_r(yy ~ mls(yy, 1, 1) + mls(xx, 3:11, 3, nbetaMT),
+ start = list(xx = c(2, 1, 5, 0)))
coef(betan)
(Intercept) yy xx1 xx2 xx3 xx4
0.93778705 0.06748141 2.26970646 0.98659174 1.49616336 -0.09184983
um <- midas_r(yy ~ mls(yy, 1, 1) + mls(xx, 3:11, 3), start = NULL)
coef(um)
(Intercept) yy xx1 xx2 xx3 xx4
0.92989757 0.08358393 2.00047205 0.88134597 0.42964662 -0.17596814
xx5 xx6 xx7 xx8 xx9
0.28351010 1.16285271 -0.53081967 -0.73391876 -1.18732001
```

基于这三个模型，我们可以利用2009年二季度至2011年二季度数据对预测表现进行评价。

```R
fulldata <- list(xx = window(nx, start = c(1985, 1), end = c(2011, 6)), yy = window(ny, start = c(1985, 1), end = c(2011, 2)))
insample <- 1:length(yy)
outsample <- (1:length(fulldata$yy))[-insample]
avgf <- average_forecast(list(beta0, betan, um), data = fulldata, insample = insample, outsample = outsample)
sqrt(avgf$accuracy$individual$MSE.out.of.sample)
[1] 0.5361953 0.4766972 0.4457144
```

由上述结果可知，U-MIDAS模型具有最小的样本外RMSE结果，为0.4457144。

---

**参考文献**

[1] Foroni C, Marcellino M, Schumacher C (2015). “Unrestricted Mixed Data Sampling (MIDAS):
MIDAS Regressions with Unrestricted Lag Polynomials.” Journal of the Royal Statistical Society A, 178(1), 57–82. [[Link]](https://rss.onlinelibrary.wiley.com/doi/abs/10.1111/rssa.12043)

[2] Ghysels E, Santa-Clara P, Valkanov R (2002). “The MIDAS Touch: Mixed Data Sampling Regression Models.” Working paper, UNC and UCLA. [[Link]](https://rady.ucsd.edu/faculty/directory/valkanov/pub/docs/midas-touch.pdf)

[3] Ghysels E (2013). “MATLAB Toolbox for Mixed Sampling Frequency Data Analysis Using MIDAS Regression Models.” Available on MATLAB Central at [[Link]](http://www.mathworks.com/matlabcentral/fileexchange/45150-midas-regression) 





