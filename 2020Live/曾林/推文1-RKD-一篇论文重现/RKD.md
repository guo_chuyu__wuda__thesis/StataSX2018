
# 学术前沿工具：拐点回归设计 

> 这篇文章摘译    
> 源自: [DAVID MCKENZIE](https://blogs.worldbank.org/team/david-mckenzie), Tools of the Trade: The Regression Kink Design, [Link](https://blogs.worldbank.org/impactevaluations/tools-trade-regression-kink-design)

断点回归设计已经成为评估政策影响的一个流行工具
[visually appealing](https://blogs.worldbank.org/impactevaluations/regression-discontinuity-porn) ，
并提供了一种直观的方式来演示政策对断点附近的影响。这种方法的一个扩展是**拐点回归（RKD）**，它的应用正在不断增长。我从没有估计过，也不是专家，我尝试提供一些这种方法介绍和相关的链接，希望能对想用种方法的人有用，如果人们想要实现这种方法，可以关注下面的内容：

## 基本理念 

断点回归设计利用在某个阈值处被处理的可能性的跳跃或不连续性。而在RKD设计中，在拐点处的处理点可能发生变化，导致赋值函数的一阶导数不连续。拐点通常由一些政府政策产生，如补贴、税收政策。
例如，[Simonsen et al. (2015)](http://onlinelibrary.wiley.com/doi/10.1002/jae.2436/abstract;jsessionid=0C061B1217904090DBAFE0FD3A16D26C.f04t01?userIsAuthenticated=false&deniedAccessCustomisedMessage=) 
用拐点来阐释丹麦政府的处方药报销计划中：补贴是基于个人一年中支付的处方费总额来计算的。初始500丹麦克朗的费用有0%的补贴，之后会有50%的补贴。一旦超过500丹麦克朗，在500到1200丹麦克朗支出，可以获得75%的补贴。超过2800丹麦克朗以上的费用，最终可获得80%的补贴。自付费部分出现拐点的结果如图1所示： 

![Figure 1](https://images.gitee.com/uploads/images/2020/0321/100314_cad1ccd2_6569481.png)

>图1:Y轴是自付费的份额。当接近500丹麦克朗时，补贴会下降。如果你已经花费480丹麦克朗，如再有50丹麦克朗购买支出， 那么，其中20丹麦克朗支出部分会得到0%补贴，超过500临界点的30丹麦克朗支出部分会得到50%补贴。
<p>注意到上图结果变量处有一个拐点（kink point），在该点左侧和右侧分别写下变量关系式再联立求解，即可得到干预变量对于结果变量的弹性影响。接下来看一个详细的例子。


## 对弹性影响的推导——政府失业保险能鼓励失业者找到工作吗？

第二个例子来自[Landais，（2015）](https://www.aeaweb.org/articles.php?doi=10.1257/pol.20130248),其计算得出政府的失业保险金（干预变量）在过往最高季度收入（运行变量）拐点处的一阶导数变化，以及失业持续时间（结果变量）在相同拐点处的斜率变化，以此二者考察人们在失业中花费的时间与他们所获得的福利金额的函数关系。
图二显示了路易斯安那州失业保险金额与最高季度收入的函数关系：

![Figure 2: ](https://images.gitee.com/uploads/images/2020/0321/100314_4e82a6e8_6569481.png)
>图2.  美国路易斯安那州: 用户周福利金额明细表,1979.1-1983.12. 表2.美国路易斯安那州：用户周福利金额明细表，1979.1-1983.12。
<p>由图2作者发现，失业救济金（干预变量）的一阶导数确实在运行变量的某个阈值处产生了跳跃（jump），也即斜率的变化。接下来的思路是继续做图，观察在运行变量的同一点上，结果变量是否也呈现出斜率变化。在本例中，Landais感兴趣的结果变量是人们在失业中花费的时间，于是他查看了在拐点处，失业持续时间的斜率变化，见图3.


![Figure 3: Kink in the outcome at the same place as kink in the treatment](https://images.gitee.com/uploads/images/2020/0321/100314_29ff3851_6569481.png)
>图3：作者在运行变量的同一处找到了结果变量的拐点。
<p> 作者所感兴趣的关系为：
$$
\mathrm{y}=f(b, w,\varepsilon)
$$
 

其中 $y$ 是失业持续时间， $b$ 是失业者在失业期间所能领取的失业救济金，该金额由失业者过往最高季度收入 $w$ 决定， $\varepsilon$ 为扰动项。对该式求偏导数有：
$$
\frac{ {\rm d}y}{ {\rm d}w} = f^{'}_1\frac{\partial b}{\partial w} + f^{'}_2+f^{'}_3\frac{\partial {\varepsilon}}{\partial w}
$$
，共可以得到 $f^{'}_1$ 、$f^{'}_2$ 、 $f^{'}_3$ 三种影响。其中 $f^{'}_2$ 为运行变量（失业者的过往最高季度收入）对结果变量（失业者的失业时间）的直接影响（dirrect effect），该效应在拐点两侧被认为是固定不变的； $f^{'}_3$ 可以看做是失业者的年龄、受教育水平等等协变量对于失业期限的弹性影响，但由于在 RKD 设计中假设这些协变量在已有的拐点处并未产生拐点（这些协变量与运行变量的一阶导是光滑，未发生跳跃），也就是说 $\frac{\partial {\varepsilon}}{\partial w}$ 在拐点两侧是不变的，所以来自 $f^{'}_3$ 的弹性影响为0（理论上，在该假定下你根本无法估计出 $f^{'}_3$ 的值）。因此，作者感兴趣的仅仅是干预变量（失业救济金）对结果变量（失业持续时间）的弹性影响 $f^{'}_1$ ，文章将其标记为：
$$
\alpha_t=\frac{\partial s_t(Y| _W=w)}{\partial b}
$$
作者并未给出利用上述图 2 和图 3 的两处拐点求得 $\alpha_t$ 的详细步骤，本文尝试予以补充。
<p>首先依据图 3，写出结果变量与运行变量在拐点处左侧的关系式：
$$
\frac{ {\rm d}y_1}{ {\rm d}w_1} = \frac{\partial s_t(Y| _W=w)}{\partial b}\frac{\partial b_1}{\partial w_1} + \frac{\partial s_t(Y| _W=w)}{\partial w_1} + \frac{\partial s_t(Y| _W=w)}{\partial \varepsilon}\frac{\partial \varepsilon_1}{\partial w_1}
$$
<p>类似的，写出结果变量与运行变量在图3拐点右侧的关系式：
$$
\frac{ {\rm d}y_2}{ {\rm d}w_2} = \frac{\partial s_t(Y| _W=w)}{\partial b}\frac{\partial b_2}{\partial w_2} + \frac{\partial s_t(Y| _W=w)}{\partial w_2} + \frac{\partial s_t(Y| _W=w)}{\partial \varepsilon}\frac{\partial \varepsilon_2}{\partial w_2}
$$
<p>上述两式相减，先注意到根据前文分析，运行变量（失业者的过往最高季度收入）对结果变量（失业持续时间）的直接影响（dirrect effect）在拐点两侧被认为是固定不变，也即：
$$
\frac{\partial s_t(Y| _W=w)}{\partial w_2}-\frac{\partial s_t(Y| _W=w)}{\partial w_1}= \Delta{f^{'}_2} = 0
$$
此外，协变量在已有的拐点处并未产生拐点（这些协变量与运行变量的一阶导数是光滑的，未发生跳跃），也就是说 $\frac{\partial {\varepsilon}}{\partial w}$ 在拐点两侧是不变的，那么：
$$
\frac{\partial s_t(Y| _W=w)}{\partial \varepsilon}(\frac{\partial \varepsilon_2}{\partial w_2} - \frac{\partial \varepsilon_1}{\partial w_1}) = {f^{'}_3} \cdot 0 = 0
$$
因此消元后可得：
$$
\frac{ {\rm d}y_2}{ {\rm d}w_2} - \frac{ {\rm d}y_1}{ {\rm d}w_1} = \frac{\partial s_t(Y| _W=w)}{\partial b}(\frac{\partial b_2}{\partial w_2} - \frac{\partial b_1}{\partial w_1}) 
$$
化简后有：
$$
f^{'}_1 = \frac{\partial s_t(Y| _W=w)}{\partial b} =  \frac{\frac{ {\rm d}y_2}{ {\rm d}w_2} - \frac{ {\rm d}y_1}{ {\rm d}w_1}}{\frac{\partial b_2}{\partial w_2} - \frac{\partial b_1}{\partial w_1}}
$$
从上式可以清晰地看到，我们需要估计的干预变量 $b$ 对于结果变量 $y$ 的弹性影响 $f^{'}_1$ 由**分子**上的斜率变动 $\frac{ {\rm d}y_2}{ {\rm d}w_2} - \frac{ {\rm d}y_1}{ {\rm d}w_1}$，也即图 3 拐点左右两侧的斜率之差，以及**分母**上的斜率变动 $\frac{\partial b_2}{\partial w_2} - \frac{\partial b_1}{\partial w_1}$ ，也即图 2 干预变量与运行变量拐点左右两侧的斜率之差，二者的比值计算得出。

## 在 Stata 中估计弹性影响
<p> 实际操作中，我们用如下多项式来估计分子上的斜率变动：
$$
E[Y|W=w] = \mu_0 + \sum\limits_{p=1}^n \left[ \alpha_p (w-k)^p + \beta_p (w-k)^p \cdot D \right]
$$

其中， $w$ 为运行变量（过往最高季度收入）， $Y$ 为结果变量（失业持续时间）； $k$ 为拐点( kink point )， $D$ 为虚拟变量，它允许曲线的斜率在拐点两侧发生变化。系数 $\beta_1$ 即为我们关注的拐点两侧的斜率变化值。
<p> 然后进行相同的回归，得到干预变量在运行变量转折点处的斜率变化，此即分母上的斜率变动，并取两个系数的比值，就估计出了我们在上一小节推导出的干预变量对结果变量的弹性影响 $f^{'}_1$ 。
<p>然后可以使用Delta方法做重复抽样或修正标准误。也可以使用局部多项式进行非参数估计。

## 关键假设和检验 

1. 关键假设与回归不连续性的假设相似，并以类似的方式进行检验： 其他协变量在转折点处的斜率不应发生变化（例如，Landai显示在转折点处的年龄、教育、资本收入或受抚养人数量的斜率没有转折）。
2. 在临界点不存在对分配的操纵——例如，在失业情况下，人们不会战略性地操纵他们过去的收入，以期望这样对他们未来的失业福利产生影响。这可以通过Mcclary检验来验证，检验表明赋值变量在拐点处的分布是连续的，且该Pdf的一阶导数也是平滑的。[Card et al. (2015)](http://onlinelibrary.wiley.com/doi/10.3982/ECTA11224/abstract) 表明可以允许代理变量围绕拐点做一些排序，只要代理变量出现优化误差或错误，排序就不是完全确定的。
3. 这篇[文章](http://ftp.iza.org/dp8282.pdf) 建议你在非拐点处做一些安慰剂测试。 在[Econometrica paper by Card et al. (2015).](http://onlinelibrary.wiley.com/doi/10.3982/ECTA11224/abstract)中可以找到Card等人(2015)提供技术细节。
## 注意事项

- RKD设计在小样本中做得很差，通常需要比RDD更宽的带宽或更大的样本。
- 有一些优化选择带宽的方法，而那些无法证明这种选择的合理性和/或对不同带宽选择表现出稳健性的论文则面临批评。例如，请参阅对本[AEJ经济政策文件的评论](https://www.aeaweb.org/articles.php?doi=10.1257/pol.6.1.167)。

## 参考文献

Andais的[comments on this AEJ Economic Policy paper](https://www.aeaweb.org/articles.php?doi=10.1257/pol.6.1.167)提供了方便的[在线数据](https://www.aeaweb.org/articles.php?doi=10.1257/pol.20130248)。这篇由Calonico等人撰写的Stata期刊文章，有一些研发代码也涵盖了RKD的情况。


##文献原文

Regression Discontinuity designs have become a popular addition to the impact evaluation toolkit, and offer a [visually appealing](https://blogs.worldbank.org/impactevaluations/regression-discontinuity-porn) way of demonstrating the impact of a program around a cutoff. An extension of this approach which is growing in usage is the **regression kink design(RKD)**. I've never estimated one of these, and am not an expert, but thought it might be useful to try to provide an introduction to this approach along with some links that people can then follow-up on if they want to implement it.

## Basic Idea
The regression discontinuity design exploits a jump or discontinuity in the likelihood of being treated at some threshold point. In the RKD design, there is instead a change in slope at the likelihood of being treated at a kink point, resulting in a discontinuity in the first-derivative of the assignment function. These types of kinks arise in a number of government policies. For example, [Simonsen et al. (2015)](http://onlinelibrary.wiley.com/doi/10.1002/jae.2436/abstract;jsessionid=0C061B1217904090DBAFE0FD3A16D26C.f04t01?userIsAuthenticated=false&deniedAccessCustomisedMessage=) use a kink in the Danish government's prescription drug reimbursement schedule: the subsidy is based on the total prescription costs the individual has paid during the year – there is 0% subsidy for the first 500 DKK in expenses, then 50% subsidy once you have paid 500 up until you have paid 1200, then 75% subsidy, and eventually an 80% subsidy for expenses above 2800 DKK. The result is that the share of the price paid out of pocket kinks as shown in Figure 1:

![Figure 1](https://images.gitee.com/uploads/images/2020/0321/100314_cad1ccd2_6569481.png)

> Figure 1: Y-axis is the share of the price paid out of pocket. It falls as one approaches 500 DKK since if you have spent 480 and buy something for 50, you get 0% subsidy on 20 DKK and then 50% subsidy on the 30 DKK that is expenditure past the threshold.

A second example is government unemployment insurance payments, which often cover up to some percentage of your previous wage, up to some maximum, and sometimes with a floor. For example, Figure 2, from a [recent paper by Landais](https://www.aeaweb.org/articles.php?doi=10.1257/pol.20130248), shows the unemployment benefit in Louisiana as a function of the highest quarter earnings:

![Figure 2: ](https://images.gitee.com/uploads/images/2020/0321/100314_4e82a6e8_6569481.png)

Then the idea is to see whether the outcome of interest also exhibits a kink/change of slope in its relation to the running variable at this same point. For example, Landais is interested in how long people spend in unemployment as function of the amount of benefit they receive. Figure 3 below shows a strong change in the shape of the relationship between unemployment duration and highest quarter earnings at this kink-point.

![Figure 3: Kink in the outcome at the same place as kink in the treatment](https://images.gitee.com/uploads/images/2020/0321/100314_29ff3851_6569481.png)

> Figure 3: Kink in the outcome at the same place as kink in the treatment

Then the causal impact is found by dividing the change in slope for the outcome by the change in slope for the treatment.
When the policy rule is implemented with some error, a fuzzy RKD design can be used (see [Card et al. (2015)](http://onlinelibrary.wiley.com/doi/10.3982/ECTA11224/abstract))

## Estimation

This can be estimated by using a polynomial regression. Let x be the running variable (e.g. highest quarter previous earnings), Y the outcome variable (e.g. duration of unemployment); k the kink point, and D a dummy variable for being to the right of the kink. Then estimate:
Y = a0 + a1*(x-k) + b1*D*(x-k) + a2*(x-k)^2 + a3*D*(x-k)^2 + …
Then b1 tells you the change in the slope of the outcome at the kink point.
Then run the same regression to get the change in the slope of the treatment variable at the kink point of the running variable, and take the ratio of the two coefficients. The standard error can then be bootstrapped or recovered using the Delta method.
Estimation can also be done non-parametrically using local polynomials.

## Key assumptions and checks 
The key assumptions are similar to those for regression discontinuity and are checked in similar ways:

1. There should be no change in slope at the kink point for other covariates (e.g. Landais shows no kinks in slope for age, education, capital income, or number of dependents at the kink point)
2. There is no manipulation of the assignment variable at the kink point – e.g. in the unemployment case, people don't strategically manipulate their past earnings in expectation of how this will affect their future unemployment benefits. This can be tested by McCrary-type tests that show the distribution of the assignment variable is continuous at the kink point, and also that the first derivative of this pdf is also smooth. [Card et al. (2015)](http://onlinelibrary.wiley.com/doi/10.3982/ECTA11224/abstract) show that you can allow agents to do some sorting around the kink points, so long as they make small optimization errors or mistakes so that this sorting is not completely deterministic.
3. This [paper](http://ftp.iza.org/dp8282.pdf) suggests some placebo tests you can do at non-kink points.

Technical details can be found in this [Econometrica paper by Card et al. (2015).](http://onlinelibrary.wiley.com/doi/10.3982/ECTA11224/abstract)

## Caveats

- The RKD design can do pretty poorly in small samples, and typically will require a wider bandwidth or larger sample than you would need with a RDD.
- There are methods for optimally choosing the bandwidth, and papers that aren't able to justify this choice and/or show robustness to different bandwidth choices face criticism. See the [comments on this AEJ Economic Policy paper](https://www.aeaweb.org/articles.php?doi=10.1257/pol.6.1.167) for an example.


## Resources
The AEJ paper of Landais has data and replication Stata code [available online](https://www.aeaweb.org/articles.php?doi=10.1257/pol.20130248).
[This Stata Journal article](http://www-personal.umich.edu/~cattaneo/papers/Calonico-Cattaneo-Titiunik_2014_Stata.pdf) by Calonico et al. has some RD code that also covers the RKD case.