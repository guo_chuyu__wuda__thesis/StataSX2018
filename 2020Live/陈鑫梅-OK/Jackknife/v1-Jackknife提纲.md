## 1. 引入

- 围绕Jackknife 的一个简单应用场景、Jackknife 如何解决的问题的思路写

## 2. Jackknife介绍

### 2.1 Jackknife 简介

**2.2**  Jackknife 不适用的情形

### 2.3  Jackknife 的 Stata 实现

## 3. Jackknife 与其他方法的关系和异同

### 3.1 Jackknife 与交叉验证法

####   留一法（Leave One Out Cross Validation，LOOCV）

> 你理解的没错，留一法与 Jackknife 没有本质区别，只是名称不同而已     
> 广义上来讲，也可以有留二法，留三法，……。

#### K 折交叉验证（K-Fold Cross Validation）

#### 蒙特卡洛交叉验证（Monte Carlo Cross Validation）

### 3.2 Jackknife 与 Bootstrap 对比

#### 二者的抽样方法不同

Bootstrap 采用的是「**可重复抽样**」，或曰「**有放回抽样**」。假设手头有一组包含三个观察值的「抽样样本 (Sample)」 $S = {ABC}$，则通过「有放回抽样」可以得到 ${AAA, AAB, AAC, ABB, ACC, ABC, BBB, BBC, BCC, CCC}$ 共 10 组不同的「**经验样本** (empirical sample)」。

## 总结




 

 

 