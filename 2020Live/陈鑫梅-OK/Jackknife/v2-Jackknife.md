
&emsp;

> 连玉君 (中山大学，arlionn@163.com )     
> 陈鑫梅 (暨南大学，1562922593@qq.com )

## 1. 引入

当你在做研究时，不知道是否有过这样的经历：花了好长时间想出来一个好 idea，但却发现搜集数据存在困难，比方说进行一次抽样的成本较高，再进行一次抽样不太可能实现… 总之，你无法做到对总体进行重复多次的抽样，也就是说你无法通过统计样本对总体进行推断。本文为大家分享了一种重抽样方法 Jackknife 刀切法，并将刀切法与其他几种类似方法进行比较，旨在为大家解决上述问题提供一种思路。

Jackknife 解决问题的思路是，通过随机抽样取得样本，保证样本 $S$ 能够代表总体，在再取得样本存在困难的情况下，对已经取得的样本 $S$ 进行“每次删除一个数据”的操作，得到样本 $S1$, $S2$...，用样本$S1$，$S2$...分别计算对应的统计量，进而采用平均等方式计算我们感兴趣的统计量。

简言之，假设有一组包含五个观察值的「抽样样本 (Sample)」 $S = {12345}$，则通过“每次删除一个数据”的操作可以得到${2345, 1345, 1245, 1235, 1234}$ 共 5 组不同的「**经验样本** (empirical sample)」

## 2. Jackknife 介绍
### 2.1 Jackknife 简介
在统计学中， Jackknife 刀切法是一种重采样技术，原始动机是降低估计的偏差。刀切法比其他常见的重采样方法如 (Bootstrap) 先出现。一个参数的刀切估计是通过系统地从一个数据集中删除每个观测值并计算估计值，然后求出这些计算值的平均值而得到的。给定一个 $n$ 大小的样本，通过聚集每个 $n-1$ 大小的子样本的估计来找到刀切估计。

Jackknife 由 Maurice Quenouille(1949) 提出， Tukey(1958) 创造了这个术语，即通过移除数据后重新计算估计值的 Jackknife 估算方法，它提供了一种通用的统计工具，既易于实现，又能解决许多问题。

 **Jackknife 背后的机制：**
 如果 $\bar{x}$ 表示规模为 $n$ 的样本均值，我们同样可以计算在移除第  $j$ 个数据点时的样本均值，

$$\bar{x}_{-j}=\frac{1}{n-1} \sum_{i \neq j}^{n} x_{i}    (1)$$ 

也就是说，如果我们知道 $\bar{x}$ 和 $\bar{x}_{-j}$ ，我们可以计算第 $j$ 数据点的值为
$$
x_{j}=n \bar{x}-(n-1) \bar{x}_{-j}
 (2)$$ 

假设我们希望估计“伪数值” (pseudovalues) $\widehat{\theta}$（可能非常复杂）作为 $n$ 个数据点的统计量，
$$
\widehat{\theta}=\phi\left(x_{1}, x_{2}, \cdots, x_{i-1}, x_{i}, x_{i+1}, \cdots, x_{n}\right)
 (3)$$ 

受 (1) 式的启发，我们用 $\widehat{\theta}_{j}$ 表示去除数据点 $x_{j}$ 的估计量
$$
\widehat{\theta}_{j}=\phi\left(x_{1}, x_{2}, \cdots, x_{i-1}, x_{i+1}, \cdots, x_{n}\right)
 (4)$$

与 (2) 式类似，定义第 $j$ 个伪数值为，
$$
\widehat{\theta}_{j}^{*}=n \widehat{\theta}-(n-1) \widehat{\theta}_{j}
 (5)$$

假设这些伪数值与 $x_{j}$ 估计均值的作用相同，因此 Jackknife 的估计由这些值的平均值给出，
$$
\widehat{\theta^{*}}=\frac{1}{n} \sum_{i=1}^{n} \widehat{\theta}_{i}^{*}
 (6)$$

$\widehat{\theta^{*}}$ 近似的抽样误差可以通过计算 $\widehat{\theta}_{j}^{*}$ 的样本误差得到,
$$
\operatorname{Var}(\widehat{\theta^{*}})=\frac{\operatorname{Var}(\widehat{\theta_{j}^{*}})}{n}=\frac{\sum_{j=1}^{n}\left(\widehat{\theta}_{j}^{*}-\widehat{\theta}^{*}\right)^{2}}{n(n-1)}
 (7)$$

同样的， ( $1-\alpha$ )\% 的置信区间为
$$
\widehat{\theta^{*}} \pm t_{\alpha / 2, n-1} \sqrt{\frac{\sum_{j=1}^{n}\left(\widehat{\theta}_{j}^{*}-\widehat{\theta}^{*}\right)^{2}}{n(n-1)}}
 (8)$$

其中，$t_{\alpha, n}$ 满足 $\operatorname{Pr}\left(t_{n} \geq t_{\alpha / 2, n-1}\right)=\alpha$ ， $t_{n}$ 表示自由度为 $n$ 的 $t$ 分布随机变量。Jackknife 估计值的置信区间必须谨慎使用，因为它们可能会高估或低估真实的置信区间。

### 2.2  Jackknife 的 Stata 实现

#### 2.2.1 `Jackknife` 命令介绍
**基本语法**

```
 jackknife exp_list [, options eform_option] : command
```
**主要选项**
|options| Description|
|---|-----|
| cluster(varlist) | variables identifying sample clusters|
| idcluster(newvar) | create new cluster ID variable|
|       keep        |keep pseudovalues|
| mse               |use MSE formula for variance estimation|


#### 2.2.2`Jackknife2` 命令-- 快速刀切估计

`Jackknife2` 可用于刀切线性估计。此外，它允许用户计算交叉验证和诊断度量 (diagnostic measures)，这些措施目前在 `ivregress 2sls`、`xtreg` 和 `xtivreg` 之后是不能进行的。`jacknife2` 和 `jknife2` 都可表示 `jackknife2`。

`Jackknife` 和 `Jackknife2` 建立在 $n$ 次迭代的循环上，每一样本单元对应一次迭代，区别在于每次迭代的 L1O 估计的计算方式。

- `Jackknife` 在删除某一样本单位的基础上运行适当的估计命令，退出循环后，计算刀切估计的偏差。这种计算方式代价昂贵，因为要计算 $n$ 次  K x K 矩阵的逆运算。
- `Jackknife2` 在每次迭代中运用 L1O 公式计算了 L1O 估计值；在循环中，还积累了计算刀切估计的方差和偏差。这大大减轻了计算负担。


**基本语法**

```
jackknife2 [, options] : command
```
**Jackknife 和 Jackknife2 命令实例**
```
sysuse auto, clear
jackknife: regress mpg weight trunk

*输出结果
Jackknife replications (74)
----+--- 1 ---+--- 2 ---+--- 3 ---+--- 4 ---+--- 5 
..................................................    50
........................

Linear regression                               Number of obs     =         74
                                                Replications      =         74
                                                F(   2,     73)   =      78.10
                                                Prob > F          =     0.0000
                                                R-squared         =     0.6543
                                                Adj R-squared     =     0.6446
                                                Root MSE          =     3.4492

------------------------------------------------------------------------------
             |              Jackknife
         mpg |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
      weight |  -.0056527   .0010216    -5.53   0.000    -.0076887   -.0036167
       trunk |   -.096229   .1486236    -0.65   0.519    -.3924354    .1999773
       _cons |   39.68913   1.873324    21.19   0.000      35.9556    43.42266
------------------------------------------------------------------------------

sysuse auto, clear
jackknife2: regress mpg weight trunk
*输出结果
Jackknife replications (74)
----+--- 1 ---+--- 2 ---+--- 3 ---+--- 4 ---+--- 5 
.................................................. 50
........................

Linear regression                               Number of obs     =         74
                                                Replications      =         74
                                                F(   2,     71)   =      67.19
                                                Prob > F          =     0.0000
                                                R-squared         =     0.6543
                                                Adj R-squared     =     0.6446
                                                Root MSE          =     3.4492
Cross-validation criterion =    932.76
------------------------------------------------------------------------------
             |              Jackknife
         mpg |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
      weight |  -.0056527   .0010216    -5.53   0.000    -.0076896   -.0036157
       trunk |   -.096229   .1486236    -0.65   0.519     -.392576    .2001179
       _cons |   39.68913   1.873323    21.19   0.000     35.95383    43.42443
------------------------------------------------------------------------------
```

### 2.3 Jackknife 不适用的情形
 see Efron(1993, p.148) 

当统计函数不是平滑函数，数据的一个小变化 (删除其中一个观察值) 将导致统计量的大变化，如极值、中值。
- 如数据 $X =(10,27,31,40,46,50,52,104,146)$,这个数据集的中位数是 46，使用 Jackknife 估计中位数 (依次去掉其中一个样本，求剩下样本的中位数) 得到 $Y =(48,48,48,48,45,43,43,43,43)$，可以看到得出了 3 个不同的中位数估计。因此，只有在满足原始样本平滑的条件下，Jackknife 结果才会接近 Bootstrap，否则估计结果将是有偏的。

## 3. Jackknife 与其他方法的关系和异同
### 3.1 Jackknife 与 Bootstrap 
#### Bootstrap 基本思想
Bootstrap 由 Bradley Efron 于 1979 年提出，用于估计标准误差、置信区间和偏差。Bootstrap 利用计算机手段进行重采样。

Bootstrap 解决问题的思路是，首先随机抽样，保证样本 $X$ 能够代表总体，在再取得样本存在困难的情况下，对已经取得的样本 $X$ 进行再次抽样 (re-sample) 得到样本 $X_1$, $X_2$...，用再抽样取得的样本计算对应的统计量，进而采用平均等方式计算我们感兴趣的统计量。

#### 两者的关系
 **第一**，二者的抽样方法不同。

Bootstrap 采用的是「**可重复抽样**」，或曰「**有放回抽样**」。假设手头有一组包含三个观察值的「抽样样本 (Sample)」 $S = {ABC}$，则通过「有放回抽样」可以得到 ${AAA, AAB, AAC, ABB, ACC, ABC, BBB, BBC, BCC, CCC}$ 共 10 组不同的「**经验样本** (empirical sample)」。

在 Bootstrap 采样中，某些原始样本可能一次都没被采集到，某些样本可能被多次采样。重采样的样本集中，不包含某原始样本 $X_{i}$ 的概率为，
$$\mathbb{P} X_{j} \neq X_{i}, j=1, \ldots n=\left(1-\frac{1}{n}\right)^{n} \approx e^{-1} \approx 0.368$$

也就是说，重采样样本集包含大约 1-0.368=0.632 的原始样本集，大约有 0.368 的样本没有包括。

 **第二**，前面提到刀切法在解决不光滑 (Smooth) 参数估计时会失效，而自助法可以解决中位数的估计。

 **第三**，
- 1. 若统计量是线性的，二者的结果会非常接近。虽然从表面上看，Jackknife 似乎只利用了非常有限的样本信息。 
- 2. 对于非线性统计量而言，Jackknife 会有信息损失，此时 Bootstrap 较好。这是因为，Jackknife 可以视为 Bootstrap 的线性近似。换言之，Jackknife 的准确程度决定于统计量与其线性展开的接近程度。

### 3.2 Jackknife 与交叉验证法
机器建模中，数据被分为训练集和测试集，测试集与训练模型无关，用于模型的评估。在训练模型时，往往面临过拟合问题 (模型能够匹配训练数据，但预测训练集以外的数据效果欠佳)，一个解决的思路就是利用测试集的数据调整模型参数，但这将影响评估模型的准确性 (训练模型时已经利用测试集数据的信息)。通常，我们会选择将训练集再进行划分，留一部分数据作为验证集，用于评估模型训练效果。

交叉验证法是统计学上，将数据样本切割成较小样本的方法。将每个子集分别作为一次验证集，而其他子集作为训练集。交叉验证的目的是定义一个验证集在训练阶段测试模型，并得出未知数据集 （测试集）应用于该模型的结果。

#####  留一法（Leave One Out Cross Validation，LOOCV）
留一法是指只使用原样本中的一个样本作为验证集，其他数据作为训练集。本质上，留一法与 Jackknife 并无区别。广义上来讲，还存在留二法 (留出 2 个观察值作为验证集，其余作为训练集)，留三法，……。
```
Stata 用 Leave-one-out 交叉验证评估模型的 performance
ssc install loocv
loocv reg mpg weight trunk

 Leave-One-Out Cross-Validation Results 
-----------------------------------------
         Method          |    Value
-------------------------+---------------
Root Mean Squared Errors |   3.5503247
Mean Absolute Errors     |   2.4184582
Pseudo-R2                |   .61889574
-----------------------------------------
```


##### K 折交叉验证（K-Fold Cross Validation）
将训练集分成 $K$ 个子样本，其中的一个子样本被当作验证集，剩下 $K-1$ 个样本是训练集。交叉验证重复 $K$ 次，使每个子样本都作为验证集验证一次，将得到的 $K$ 次结果平均，或者采用其它方式，最终得出一个估计值，10 次交叉验证是最常用的。当数据总量较小，选择其他方法也无法提升性能可以考虑使用，K 折交叉验证 (当数据量大时，该法会使训练时间加长)。

##### 蒙特卡洛交叉验证（Monte Carlo Cross Validation）
蒙特卡洛交叉验证也叫作重复随机子抽样验证 (Repeated random sub-sampling validation)，数据集被随机分为训练集和验证集，用训练集训练模型，使用验证集评估预测是否准确。这种方法拆分训练集和验证集的比例不依赖“折叠”次数，但是，随机分解数据可能会使某些数据从未被选入验证集，也可能被多次选中。这些不确定因素使得实验结果不可复制。

- 关于交叉验证的更多资料 (`crossfold`、`kfoldclass`命令介绍及实例等)
https://mp.weixin.qq.com/s/PbjsYPKCwQlbvN9uM1BezQ

## 相关命令概览
得益于 Jackknife 的一些优势，以及「去一法」的应用日益广泛，Stata 中已经有不少相关的外部命令，这里列举一些，供大家参考：

- `help cv_regress` // estimate the leave-one-out error for linear regression models
- `help estrat` // module to perform Endogenous Stratification for Randomized Experiments
- `help loocv` // module to perform Leave-One-Out Cross-Validation
- `help looclass` // module for generating classification statistics of Leave-One-Out cross-validation for binary outcomes


## 参考资料
- (*CT09) Cameron, A., P. Trivedi. Microeconometrics using stata[M]. Stata Press, 2009. [[PDF]](https://quqi.gblhgk.com/s/880197/nnSoNLtKBztCsOUx), [[Data&Progs]](https://quqi.gblhgk.com/s/880197/4DwHh3wAwogxaQdm)
- Brian P. Poi, 2006, Jackknife Instrumental Variables Estimation in Stata, Stata Journal, 6(3): 364–376. [pdf]
- Efron, B., R. Tibshirani. An introduction to the bootstrap[M]. Chapman & Hall, 1993. [[PDF]](https://quqi.gblhgk.com/s/880197/N3xNRMDsmW0AdcOa)
- Walsh (2000), Resampling Methods: Randomization Tests, Jackknife and Bootstrap Estimators, Lecture Notes. [[PDF]](https://quqi.gblhgk.com/s/880197/xP1TSOJVV5uI83gZ)
