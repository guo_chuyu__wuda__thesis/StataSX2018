
*=======================
*  Jackknife法(刀切法)
*=======================

* ------------
*    简  介
* ------------
* 参见：Efron(1993, Chp11)； Stata Manual 10 [R]jackknife

*- 在四十年代末，五十年代初提出，见 Tukey(1958)
*
*- 主要性质和用途：
*
*  1. 是诸多统计量的一阶无偏估计量(first-order unbiased estimator)；
*  2. 以样本为基础，计算统计量的标准误和置信区间；
*  3. 可产生每个观察值对应的“伪数值”(pseudovalues)，
*     用于评估该观察值对统计量的影响，
*     多用于勘查“离群值”(outliter)；

*- 发展状况：
*  
** 目前应用较为广泛的是第三个性质，这是Bootstrap无法做到的，
** 之所以不用JK计算标准误和置信区间，
*  是因为采用Bootstrap计算标准误和置信区间更为有效和稳健。

*- 我们学习的目的是掌握一种抽样方法。


* ------------
*   基本思想
* ------------
* 给定样本(x1,x2,...,x100)，记为 x_i (i=1,2,...,100)
* 假设我们要计算的统计量是样本均值，
* 若采用Jackknife，则我们将每次删除一个观察值，
* 只计算剩下99个观察值的均值，得到 100 个均值， 
* 利用这 100 个均值，便可计算其标准误和置信区间了。
* 具体而言：
*    m1 = (   x2+x3+...+x100)/99
*    m2 = (x1   +x3+...+x100)/99
*    ...  ...
*  m100 = (x1+x2+x3+...+x99) /99  

* 当然，使用Jackknife也可以计算其他统计量，如标准误、比率等。
* 但基本思想都是一样的。


*-----------------------
*  伪数值(pseudovalues)
*-----------------------
* XM_a = (x1+x2+...+x100)/100 (样本均值)
* XM_2 = (x1   +...+x100)/99  (去除第二个观察值后的样本均值)
*
* 则 XM_a 和 XM_2 存在如下关系：
*
*          (N-1)XM_2 + x2
*  XM_a = -----------------    (本例，N=100)             [1]
*                N
*
* 一般化公式：
*
*          (N-1)XM_j + x_j
*  XM_a = -----------------    (x_j 表示第 j 个观察值)   [2]
*                 N
*
* 由 [2] 式中，可求得：
* 
*  x_j = N*XM_a - (N-1)*XM_j                             [3]
* 
*  该值在 Jackknife 检验中称为“伪数值”(pseudovalues)
*
* 一般化：
* 设 Q   为任意统计量(如标准差、方差等)，
*    Q_j 为删除第j个观察值后得到的该统计量；
* 则第j个观察值对应的“伪数值”为：
*
*    q_j = N*Q - (N-1)*Q_j                               [4]
* 
*-- 性质：
*  1. q_j 的 均值 是统计量 Q 的一阶无偏估计量；
*  2. q_j 的标准误是统计量 Q 的标准误；
*
*  q_mean = (q1+q2+...+qN)/N
*
*         {    1     N                 }1/2 
*  q_se = { ------- SUM (q_j-q_mean)^2 }
*         {  N(N-1) j=1                }


*- 例1：解析Jackknife的计算过程：

   clear 
   set obs 10
   gen id = _n  /*样本序号*/   
   gen x  = _n + 10 
   gen px = .   /*x的伪值*/

 *- 计算伪 均值
    forvalues j = 1/10{
      qui sum x            /* Q */
      local m_0 = r(mean)
      sum x if (id != `j') /* Q_j */
      local m_j = r(mean)
      replace px = 10*`m_0' - (10-1)*`m_j' in `j' /*计算伪值*/
    }
    sum x px

  *- Stata命令：jackknife
     jackknife m_jk = r(mean),rclass keep: sum x     
     
  *- 例1.1：   
  *- 计算伪 方差
    replace px = .
    forvalues j = 1/10{
      qui sum x            
      local m_0 = r(Var)
      sum x if (id != `j') 
      local m_j = r(Var)
      replace px = 10*`m_0' - (10-1)*`m_j' in `j' /*计算伪值*/
    }
    sum x px  
    * 可见，两端的数值对方差的影响较大
    
    *- stata命令：
    replace m_jk = .
    jackknife m_jk = r(Var),rclass keep: sum x       


*- 例2：JK 得到的标准误和置信区间与 ci 命令的结果相同(传统方法)
   
   sysuse auto, clear
   jackknife r(mean),double: sum price
   
   ci price  /*参见Bootstrap小节*/
   
   * 这里，double 用于设定数值的精度，即“双精度”
   
   
*- 例3：Jackknife 计算标准误
     clear
     input x
        0.1
        0.1
        0.1
        0.4
        0.5
        1.0
        1.1
        1.3
        1.9
        1.9
        4.7
     end
      
     jackknife sd=r(sd), rclass keep: sum x 
     * x 的“标准差”为 1.343
     * x 的“标准差”的 95% 置信区间为 [-0.04779，2.7347]
     * x 的“标准差”的 JK 标准误为 0.6244
     
     list  
     * 我们发现，最后一个观察值对应的“伪值”很大，明显不同于其他伪值。
     * 至于11个观察值是否为离群值，则视具体情况而定；
     * 本例中，这11个观察值来自于指数分布，因此它并非离群值。    
     histogram x
     

*- 例4：离群值的查验

   sysuse auto, clear
   jackknife sd=r(sd), rclass keep: sum price
   
   sum sd, detail
   list price make sd if sd>10000
   
   
*- 例5：同时计算多个统计量的标准误

   sysuse auto, clear
   jackknife sd=r(sd) skew=r(skewness) var=r(Var), rclass: sum mpg, detail
   
   
*- 例6：计算估计系数的标准误
 
 *-截面资料
   sysuse auto, clear
   jackknife: reg price wei len mpg foreign
   * 或
   reg price wei len mpg foreign, vce(jackknife)
     est store r_jk
   reg price wei len mpg foreign, vce(bootstrap, reps(200))
     est store r_bs
   reg price wei len mpg foreign
     est store r_ols
   local mm "r_ols r_bs r_jk"
   esttab `mm', mtitle(`mm')
   * 结论：相对于传统的t检验，BS和JK得到的标准误都较大，
   *       同时，BS和JK得到的标准误非常接近。
 
 *-面板数据
   use xtcs.dta, clear
   keep if code<1000
   xtdes
   jackknife,eclass cluster(code) idcluster(code_new): ///
             xtreg tl size ndts tang tobin, fe
     est store fe_jk
   xtreg tl size ndts tang tobin, fe
     est store fe
   
   local mm "fe_jk fe"
   esttab `mm', mtitle(`mm') se(%6.3f)   /*标准误*/
   esttab `mm', mtitle(`mm')  t(%6.3f)   /*tֵ*/
   * 结论：相对于传统的t检验，JK得到的标准误较大

   * 说明：类似于BS，在采用JK获取标准误时，
   *       对于面板模型，需要同时设定 cluster() 和 idcluster()选项
   * cluster() 选项的作用在于，每次删除一个截面(公司或国家)


*- 例7：Jackknife instrumental variables estimation regression
   * “刀切法”IV 估计
   * 对该方法的介绍和相关的Monte Carlo过程，参见Stata Journal 2006(3):304-376.
   * 主要特点：能较好地克服弱工具变量问题。
   
   use hsng2.dta, clear
   jive rent pcturban (hsngval = faminc reg2-reg4), ujive2
   
   ivreg rent pcturban (hsngval = faminc reg2-reg4)


* -------------------------------
*  Jackknife 与 Bootstrap 的异同
* -------------------------------
* see Efron(1993, Chp11, p.145)
* 1. 若统计量是线性的，二者的结果会非常接近。
*    虽然从表面上看，Jackknife似乎只利用了非常有限的样本信息。 
* 2. 对于非线性统计量而言，Jack会有信息损失，此时BS较好。
*    这是因为，Jack可以视为BS的线性近似。
*    换言之，Jack的准确程度决定于统计量与其线性展开的接近程度。

 *-- 例：比较BS和JK在计算非线性统计量时的表现
 *   参见 Efron(1993,chp11,Ex11.3,p.152)
 
 *------------------------------------------
   clear
   set obs 20
   local B = 100         /*模拟次数*/
   mat R = J(`B',2,.)    /*存储模拟结果的矩阵*/
   local statis "r(Var)"       /*统计量1：Var[mean(x)]*/
   *local statis "r(mean)^2" /*统计量2：[mean(x)]^2 */
   forvalues i = 1/`B'{
     tempvar x
     gen `x' = 1 + invnorm(uniform())  /* x - N(1,1)*/
     * Bootstrap
     preserve
       tempfile bsdata
       qui bs m_bs = r(mean),reps(20) nowarn saving("`bsdata'"): sum `x'
       qui use "`bsdata'",clear
       qui sum m_bs
       mat R[`i',1] = `statis'   
     restore
     * Jackknife
       cap drop m_jk
       qui jackknife m_jk = r(mean), rclass keep: sum `x'
       qui sum m_jk
       mat R[`i',2] = `statis'
   }
   svmat R, names(r)
   rename r1 r_bs
   rename r2 r_jk
   sum r*
   
   graph box r_bs r_jk
 *------------------------------------------
   clear
   set obs 100
   gen x  = .
   gen mx = .
 *local jk  "in 2/100"
   forvalues i = 1/100{
     qui replace x  = 1+ invnorm(uniform())
     qui sum x `jk'
     qui replace mx = r(mean) in `i'
   }
   sum mx
   dis r(Var)
   
   
   
* -------------------------
*   Jackknife不适用的情形
* -------------------------
* see Efron(1993, p.148)    
     
  *- Jackknife 的基本假设是：
  *  原始样本具有“平滑性”(smoothness)，
  *  即，数据的微小变动(如，删除一个观察值)，仅会导致统计量的微小变动。
  * 在满足这一假设的前提下，JK 结果会非常接近于 BS。 
  * 反之，JK 的结果将是有偏的(biased)。
  
  *- 例1：非平滑数据的中位数
   *--------------------
   *   x      JK中位数 
   *--------------------       
   *   10        48
   *   27        48
   *   31        48
   *   40        48
   *   46        45
   *   50        43
   *   52        43
   *   104       43
   *   146       43
     
*== 问题：变量x中位数的标准误是多少？
   
   use B9_jk_fail.dta, clear
   list
   
   * s.e. using JK
     jackknife med=r(p50), rclass keep: sum x,de
   * s.e. using BS  
     bootstrap med=r(p50), reps(100) nodots nowarn: sum x, de
  
   *- 我们发现，采用JK得到的标准误明显偏小，
   *  主要是因为 JK 中位数缺乏平滑性所致。
   
   *- 结论：若所有检验的统计量不具有平滑性，
   *  采用JK计算其标准误或置信区间得到的结果将是有偏的。
  
    