
> **作者：** 吕政 (中央财经大学)      
> **邮箱：** <1433722321@qq.com>

---

**目录**

[TOC]

---

## 1. GARCH 模型介绍

简单地说，多元 GARCH 指的是多个时间序列之间各自波动的交互影响，这里的波动具体指的是建立时间序列 ARIMA 或 VAR 模型后，提取到的各时间序列残差的波动。介绍多元 GARCH 中的 DCC（Dynamic Conditional Correlational）模型之前，我们先来介绍基础的 GARCH 模型。在对金融时间序列进行建模时，我们常常会碰到误差项的方差会随着时间的变化而变化的现象，直观的看，方差存在明显的波动聚集性。我们需要估计出很多个参数，才能尽可能的描述出这个随时间变化而改变的异方差函数的生成过程，但这是很难做到的。为此， Bollerslev（1986）提出 GARCH 模型，下面先来简单的介绍一下 GARCH（p, q）模型：

$\begin{array}{*{20}{l}}
{{y_t} = \alpha  + {\beta ^\prime }{{\bf{X}}_t} + {u_t},\quad {u_t}|{\Omega _t} \sim {\mathop{\rm iid}\nolimits} \left( {0,\sigma _t^2} \right)}\\
{\sigma _t^2 = {\gamma _0} + \sum\limits_{i = 1}^p {{\delta _i}} \sigma _{t - i}^2 + \sum\limits_{j = 1}^q {{\gamma _j}} u_{t - j}^2}
\end{array}$

在 GARCH 模型里，随时间变化而变化的方差   既依赖于方差自身的滞后期   ，又依赖于误差项平方的滞后期   。特别地，当 p = 0 时， GARCH（p, q）模型还可以进一步简化成 ARCH（q）模型的形式。 GARCH（1, 1）模型最简单的 GARCH 模型，它的方差方程可以表示为：

$\sigma _t^2 = {\gamma _0} + {\delta _1}\sigma _{t - 1}^2 + {\gamma _1}u_{t - 1}^2$

张世英和樊智（2014）发现，金融时间序列的相关性信息能够较好的被1阶的波动相关性所解释。因此在实际应用中， GARCH（1, 1）模型的估计效果往往较为理想，并且它只有三个未知参数需要估计，估计起来比较方便。

## 2. DCC-MGARCH 基本原理

当我们的研究对象从单个金融时间序列拓展到多个金融时间序列时，我们可能会对多个时间序列之间波动的动态相关性感兴趣，即一个时间序列的波动和另一个时间序列的波动之间的相关关系随着时间的变化会产生什么样的改变。针对这个问题，Engle（2002）提出了动态条件相关 GARCH 模型，简记为： DCC-MGARCH 模型，模型的设定如下：

${H_t} = {D_t}{R_t}{D_t}$

Rt 表示我们感兴趣的多个时间序列的动态相关性，通过指数平滑的方式对条件相关矩阵 Rt 表示为：

${\left( {{R_t}} \right)_{ij}} \equiv {\rho _{ij,t}} = \frac{{\sum\limits_{s = 1}^{t - 1} {{\lambda ^s}} {\varepsilon _{i,t - s}}{\varepsilon _{j,t - s}}}}{{\sqrt {\sum\limits_{s = 1}^{t - 1} {{\lambda ^s}} \varepsilon _{i,t - s}^2\sum\limits_{s = 1}^{t - 1} {{\lambda ^s}} \varepsilon _{j,t - s}^2} }}$

式中以 $\lambda $（$0 < \lambda  < 1$）为平滑系数。为此，可以构造指数平滑模型：

$\left\{ {\begin{array}{*{20}{l}}
{{\rho _{ij,t}} = \frac{{{q_{ij,t}}}}{{\sqrt {{q_{ii,t}}{q_{jj,t}}} }}}\\
{{q_{ij,t}} = (1 - \lambda ){\varepsilon _{i,t - 1}}{\varepsilon _{j,t - 1}} + \lambda {q_{ij,t - 1}}}
\end{array}} \right.$

也可以借鉴 GARCH（1, 1）模型的形式，将 qij,t 表示为：

${q_{ij,t}} = {\bar \rho _{ij}} + \alpha \left( {{\varepsilon _{i,t - 1}}{\varepsilon _{j,t - 1}} - {{\bar \rho }_{ij}}} \right) + \beta \left( {{q_{ij,t - 1}} - {{\bar \rho }_{ij}}} \right)$

式中，${\bar \rho _{ij}}$ 为 ${\varepsilon _{i,t}}{\varepsilon _{j,t}}$ 与 ${\varepsilon _{i,t - 1}}{\varepsilon _{j,t - 1}}$ 之间的非条件相关。也可以将上述过程用矩阵的形式表示成：

$\begin{array}{l}
{Q_t} = (1 - \lambda ){\varepsilon _{t - 1}}\varepsilon _{t - 1}^\prime  + \lambda {Q_{t - 1}}\\
{Q_t} = \bar Q(1 - \alpha  - \beta ) + \alpha \left( {{\varepsilon _{t - 1}}\varepsilon _{t - 1}^\prime } \right) + \beta {Q_{t - 1}}
\end{array}$

接下来，介绍在 R 语言中估计 DCC 模型的实现命令。

## 3. 软件实现
### 3.1 R 语言命令
```R
library(xts)   ###加载xts包

RDJI=read.csv("D:/R/RDJI.csv",header=T)   ###读取美国道琼斯指数残差的时间序列，记为RDJI
                                          ###csv表格中共有两列数据，第一列为时间，第二列为美国道琼斯指数的残差。后表类似
RDJI=xts(RDJI[,2],as.Date(RDJI[,1]))

RSH=read.csv("D:/R/RSH.csv",header=T)     ###读取上证综合指数残差的时间序列，记为RSH

RSH=xts(RSH[,2],as.Date(RSH[,1]))

RHIS=read.csv("D:/R/RHIS.csv",header=T)   ###读取香港恒生指数残差的时间序列，记为RHIS

RHIS=xts(RHIS[,2],as.Date(RHIS[,1]))

r.data3 <- merge(RDJI, RSH, join='inner') ###将三个残差序列根据时间轴合并在一起

r.data3 <- merge(r.data3, RHIS, join='inner')

R3 <- r.data3[!apply(is.na(r.data3), 1, any), ]

colnames(R3) <- c('RDJI', 'RSH', 'RHIS')

R3=as.matrix(R3)                          ###将读入的数据转化为矩阵

head(R3)

tail(R3)

a <- c(0.003, 0.005, 0.001)               ###赋初始值，初始值可以随意设定，不大于1即可

A <- diag(c(0.2, 0.3, 0.15))              ###赋初始值

B <- diag(c(0.75, 0.6, 0.8))              ###赋初始值

dcc.para <- c(0.01, 0.98)                 ###输入参数，两个参数的和小于1即可

library(ccgarch)                          ###调用包

results=dcc.estimation(inia=a,iniA=A,iniB=B,dvar=R3,ini.dcc=dcc.para,model="diagonal")   ###祥见包

results$DCC[,2]                           ###表示提取第1个资产和第2个资产之间的相关系数

plot(results$DCC[,2])

setwd("D:/R")

write.table(results$DCC[,2] ,"1-2.csv",sep="," ,row.names = FALSE)   ###输出文件1-2.csv，得到图1中美国道琼斯指数和上证综合指数的动态相关系数

results$DCC[,3]

plot(results$DCC[,3])

setwd("D:/R")

write.table(results$DCC[,3] ,"1-3.csv",sep="," ,row.names = FALSE)   ###输出文件1-3.csv，得到图1中美国道琼斯指数和香港恒生指数的动态相关系数

results$DCC[,6]

plot(results$DCC[,6])

setwd("D:/R")

write.table(results$DCC[,6] ,"2-3.csv",sep="," ,row.names = FALSE)   ###输出文件2-3.csv，得到图1中上证综合指数和香港恒生指数的动态相关系数

```
### 3.2 Stata 命令
```Stata
tsset date
mgarch dcc (rdji rsh rhis), arch(1) garch(1)
```
```Stata
Number of gaps in sample:  1106
(note: conditioning reset at each gap)

Calculating starting values....

Optimizing concentrated log likelihood

(setting technique to bhhh)
Iteration 0:   log likelihood =   40572.81  
Iteration 1:   log likelihood =  40819.929  
Iteration 2:   log likelihood =  40906.554  
Iteration 3:   log likelihood =  40941.674  
Iteration 4:   log likelihood =  40951.215  
Iteration 5:   log likelihood =  40953.377  
Iteration 6:   log likelihood =  40953.696  
Iteration 7:   log likelihood =  40953.779  
Iteration 8:   log likelihood =  40953.809  
Iteration 9:   log likelihood =  40953.819  
(switching technique to nr)
Iteration 10:  log likelihood =  40953.821  
Iteration 11:  log likelihood =  40953.822  

Optimizing unconcentrated log likelihood

Iteration 0:   log likelihood =  40953.826  
Iteration 1:   log likelihood =  40956.416  
Iteration 2:   log likelihood =  40956.422  
Iteration 3:   log likelihood =  40956.422  

Constant conditional correlation MGARCH model

Sample: 36531 - 43948, but with gaps              Number of obs   =      4,641
Distribution: Gaussian                            Wald chi2(.)    =          .
Log likelihood =  40956.42                        Prob > chi2     =          .

---------------------------------------------------------------------------------
                |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
----------------+----------------------------------------------------------------
rdji            |
          _cons |   .0005034   .0001367     3.68   0.000     .0002355    .0007714
----------------+----------------------------------------------------------------
ARCH_rdji       |
           arch |
            L1. |   .3697685   .0295315    12.52   0.000     .3118879    .4276492
                |
          garch |
            L1. |   .6178397   .0416798    14.82   0.000     .5361487    .6995307
                |
          _cons |   1.53e-06   3.84e-06     0.40   0.690    -5.99e-06    9.06e-06
----------------+----------------------------------------------------------------
rsh             |
          _cons |   .0000167   .0002013     0.08   0.934    -.0003779    .0004112
----------------+----------------------------------------------------------------
ARCH_rsh        |
           arch |
            L1. |   .1764467   .0212204     8.31   0.000     .1348554     .218038
                |
          garch |
            L1. |   .9021743   .0562433    16.04   0.000     .7919395    1.012409
                |
          _cons |  -.0000317     .00001    -3.16   0.002    -.0000513    -.000012
----------------+----------------------------------------------------------------
rhis            |
          _cons |   .0001917   .0001889     1.01   0.310    -.0001785     .000562
----------------+----------------------------------------------------------------
ARCH_rhis       |
           arch |
            L1. |   .1285255   .0158916     8.09   0.000     .0973784    .1596725
                |
          garch |
            L1. |    .979326   .0599907    16.32   0.000     .8617464    1.096906
                |
          _cons |  -.0000365     .00001    -3.64   0.000    -.0000561   -.0000168
----------------+----------------------------------------------------------------
  corr(rdji,rsh)|   .0674728   .0138013     4.89   0.000     .0404228    .0945229
 corr(rdji,rhis)|   .2255087     .01335    16.89   0.000     .1993431    .2516742
  corr(rsh,rhis)|   .4102547   .0112957    36.32   0.000     .3881156    .4323938
---------------------------------------------------------------------------------
```
需要注意，使用Stata建立DCC模型时，使用的数据是对数收益率，而不是残差，Stata会在输入的对数收益率数据的基础上建立ARIMA模型提取残差建模。

## 4. DCC-MGARCH 模型的应用

收集了 2000 年 1 月 3 日至 2019 年 11 月 25 日，美国道琼斯指数、上证综合指数和香港恒生指数的日度数据，经平稳性检验可知，三个时间序列均为一阶单整，因此利用对数收益率序列建模，经 VAR 判断滞后阶数为 7 阶，提取 VAR 模型的残差，经检验具有显著的 ARCH 效应，接下来利用残差时间序列建立 DCC 模型。为直观显示石油行业股指收益率间的动态条件相关性，将相关系数时间序列绘制成图 1，图 1 中的三组系数来源于上一部分得到的三个文件（1-2.csv、1-3.csv、2-3.csv）。从图 1 可知，自 2000 年以来，道琼斯指数的波动率和上证综指恒生指数的波动率之间波动较大，大致呈现逐年波动提高的趋势，上证综指和恒生指数的波动相关性之间明显具有先下降后上升的关系。

![](https://images.gitee.com/uploads/images/2020/0605/110653_3ad60924_1522177.png)

## 5. 参考文献

- Bollerslev T. Generalized autoregressive conditional heteroskedasticity[J]. Journal of econometrics, 1986, 31(3): 307-327.[[PDF]](http://www.u.arizona.edu/~rlo/readings/278762.pdf)
- 张世英, 樊智. 协整理论与波动模型: 金融时间序列分析及应用[M]. 清华大学出版社, 2014.
- Engle R. Dynamic conditional correlation: A simple class of multivariate generalized autoregressive conditional heteroskedasticity models[J]. Journal of Business & Economic Statistics, 2002, 20(3): 339-350.[[PDF]](https://archive.nyu.edu/bitstream/2451/26482/2/02-38.pdf)