
> **作者：** 吕政 (中央财经大学)      
> **邮箱：** <1433722321@qq.com>

---

**目录**

[TOC]

---

## 1. CCC-MGARCH 基本原理

当我们研究资产组合或风险管理时，往往会面对面两种及以上的资产，所以我们需要建立多个变量的 GARCH 模型，对方差协方差阵进行建模。多元 GARCH 的建模步骤，大致可以分为三步：第一步，建立均值方程，用于提取残差；第二步，检验残差是否存在 ARCH 效应，并对残差进行标准化处理；第三步，对得到的残差序列建立多元 GARCH 模型。

多元 GARCH 均值方程的设定主要有两种方法：(1) 对每一个资产的时间序列分别建立 ARMA (p, q) 模型；(2) 参考徐国祥和代吉慧 (2015) 的方法，利用向量自回归模型 (VAR) 对多个资产时间序列建模，VAR 的优势在于抛开了经济学意义的因果关系，单纯从统计学的角度寻找变量之间的因果关系，此外还可以根据 VAR 模型估计参数的显著性水平分析资产与资产之间是否存在均值溢出效应。通过以上两种方法得到残差序列后，我们要进行 ARCH 检验，检验残差序列是否存在 ARCH 效应，有无 ARCH 效应是接下来建立多元 GARCH 模型的依据。接下来，简要地对多元 GARCH 模型中的 CCC-MGARCH 模型进行介绍：

Bollerslev (1990) 提出了 CCC (Constant Conditional Correlational) 模型，Bollerslev (1990) 认为在某些场合条件相关系数并不随时间的变化而变化，因此对时变的协方差矩阵 Ht 的形式做了如下设定：

${H_t} = {D_t}R{D_t}$

上式中的 ${D_t} = diag\left( {{\sigma _{1t}},{\sigma _{2t}}, \cdots ,{\sigma _{Nt}}} \right){\rm{ = }}{\mathop{\rm diag}\nolimits} \left( {h_{11,t}^{1/2},h_{22,t}^{1/2}, \cdots ,h_{NN,t}^{1/2}} \right)$  是由各个资产的条件标准差构成的对角阵，$R = \left( {{\rho _{it}}\sqrt {{w_i}{w_j}} } \right)$ 是 Bollerslev (1990) 所设定的不变的条件相关系数矩阵。将相关系数矩阵设为常数，有两方面的好处：第一，这样做简化了模型，降低了 CCC-MGARCH 模型估计上的难度；第二，常系数矩阵的设定保证了条件协方差矩阵正定性的要求。具体地，Bollerslev (1990) 的处理过程如下：

记 rt 为 N×1 维资产对数收益率时间序列，满足：

$\left\{ {\begin{array}{*{20}{l}}
{{r_t} = E\left( {{r_t}|{\Omega _{t - 1}}} \right) + {\varepsilon _t}}\\
{{\mathop{\rm var}} \left( {{\varepsilon _t}|{\Omega _{t - 1}}} \right) = {H_t}}
\end{array}} \right.$

上半部分为均值方程，下半部分为方差方程。原本，${\rho _{ij,t}} = \frac{{{h_{ij,t}}}}{{\sqrt {{h_{ii,t}}{h_{jj,t}}} }}$  表示其中两个资产对数收益率序列 rit 与 rjt 之间波动的关联关系。但 Bollerslev (1990) 对条件方差做了以下两个改动：

$\begin{array}{l}
{h_{ij,t}} = {\rho _{ij}}\sqrt {{h_{ii,t}}{h_{jj,t}}} \\
{h_{ii,t}} = {w_i}\sigma _{it}^2
\end{array}$

其中，wi 为不随时间变化的正标量  。由此得到了 ${H_t} = {D_t}R{D_t}$。接下来，介绍在 R 语言中估计 DCC 模型的实现命令。

## 2. 软件实现
### 2.1 R 语言命令 
```R
library(xts)                                    ###调用xts包

RDJI=read.csv("D:/R/RDJI.csv",header=T)         ###读取美国道琼斯指数残差的时间序列，记为RDJI。csv表格中共有两列数据，第一列为时间，第二列为美国道琼斯指数的残差。后表类似

head(RDJI)

tail(RDJI)

RDJI=xts(RDJI[,2],as.Date(RDJI[,1]))

head(RDJI)

tail(RDJI)

RSH=read.csv("D:/R/RSH.csv",header=T)           ###读取上证综合指数残差的时间序列，记为RSH

head(RSH)

tail(RSH)

RSH=xts(RSH[,2],as.Date(RSH[,1]))

head(RSH)

tail(RSH)

RHIS=read.csv("D:/R/RHIS.csv",header=T)         ###读取香港恒生指数残差的时间序列，记为RHIS

head(RHIS)

tail(RHIS)

RHIS=xts(RHIS[,2],as.Date(RHIS[,1]))

head(RHIS)

tail(RHIS)

r.data3 <- merge(RDJI, RSH, join='inner')       ###将RDJI和RSH的残差序列根据时间轴合并在一起，记为r.data3

head(r.data3)

tail(r.data3)

r.data3 <- merge(r.data3, RHIS, join='inner')   ###在r.data3的基础上，根据时间轴再加入RHIS残差序列，同样命名为r.data3

head(r.data3)

tail(r.data3)

R3 <- r.data3[!apply(is.na(r.data3), 1, any), ]

head(R3)

tail(R3)

colnames(R3) <- c('RDJI', 'RSH', 'RHIS')

head(R3)

tail(R3)

R3=as.matrix(R3)                                ###将读入的数据转化为矩阵

head(R3)

tail(R3)

a <- c(0.003, 0.005, 0.001)                     ###赋初始值，初始值可以随意设定，不大于1即可

A <- diag(c(0.5, 0.5, 0.5))                     ###赋初始值

B <- diag(c(0.75, 0.6, 0.8))                    ###赋初始值

R <- diag(c(0.8, 0.8, 0.8))                     ###输入常系数矩阵的初始值，因为有例子里有三个时间序列，所以这里需要输入三个大于-1小于1的数字，初始值不影响最后的估计结果

library(ccgarch)                                ###调用ccgarch包

results=eccc.estimation(a, A, B, R, dvar=R3, model="diagonal", method="BFGS")

results                                         ###输出我们想要的CCC估计结果

```
### 2.2 Stata 命令
```Stata
tsset date
mgarch ccc (rdji rsh rhis), arch(1) garch(1)
```
```Stata
Number of gaps in sample:  1106
(note: conditioning reset at each gap)

Calculating starting values....

Optimizing concentrated log likelihood

(setting technique to bhhh)
Iteration 0:   log likelihood =   40572.81  
Iteration 1:   log likelihood =  40819.929  
Iteration 2:   log likelihood =  40906.554  
Iteration 3:   log likelihood =  40941.674  
Iteration 4:   log likelihood =  40951.215  
Iteration 5:   log likelihood =  40953.377  
Iteration 6:   log likelihood =  40953.696  
Iteration 7:   log likelihood =  40953.779  
Iteration 8:   log likelihood =  40953.809  
Iteration 9:   log likelihood =  40953.819  
(switching technique to nr)
Iteration 10:  log likelihood =  40953.821  
Iteration 11:  log likelihood =  40953.822  

Optimizing unconcentrated log likelihood

Iteration 0:   log likelihood =  40953.826  
Iteration 1:   log likelihood =  40956.416  
Iteration 2:   log likelihood =  40956.422  
Iteration 3:   log likelihood =  40956.422  

Constant conditional correlation MGARCH model

Sample: 36531 - 43948, but with gaps              Number of obs   =      4,641
Distribution: Gaussian                            Wald chi2(.)    =          .
Log likelihood =  40956.42                        Prob > chi2     =          .

---------------------------------------------------------------------------------
                |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
----------------+----------------------------------------------------------------
rdji            |
          _cons |   .0005034   .0001367     3.68   0.000     .0002355    .0007714
----------------+----------------------------------------------------------------
ARCH_rdji       |
           arch |
            L1. |   .3697685   .0295315    12.52   0.000     .3118879    .4276492
                |
          garch |
            L1. |   .6178397   .0416798    14.82   0.000     .5361487    .6995307
                |
          _cons |   1.53e-06   3.84e-06     0.40   0.690    -5.99e-06    9.06e-06
----------------+----------------------------------------------------------------
rsh             |
          _cons |   .0000167   .0002013     0.08   0.934    -.0003779    .0004112
----------------+----------------------------------------------------------------
ARCH_rsh        |
           arch |
            L1. |   .1764467   .0212204     8.31   0.000     .1348554     .218038
                |
          garch |
            L1. |   .9021743   .0562433    16.04   0.000     .7919395    1.012409
                |
          _cons |  -.0000317     .00001    -3.16   0.002    -.0000513    -.000012
----------------+----------------------------------------------------------------
rhis            |
          _cons |   .0001917   .0001889     1.01   0.310    -.0001785     .000562
----------------+----------------------------------------------------------------
ARCH_rhis       |
           arch |
            L1. |   .1285255   .0158916     8.09   0.000     .0973784    .1596725
                |
          garch |
            L1. |    .979326   .0599907    16.32   0.000     .8617464    1.096906
                |
          _cons |  -.0000365     .00001    -3.64   0.000    -.0000561   -.0000168
----------------+----------------------------------------------------------------
  corr(rdji,rsh)|   .0674728   .0138013     4.89   0.000     .0404228    .0945229
 corr(rdji,rhis)|   .2255087     .01335    16.89   0.000     .1993431    .2516742
  corr(rsh,rhis)|   .4102547   .0112957    36.32   0.000     .3881156    .4323938
---------------------------------------------------------------------------------
```
需要注意，使用Stata建立CCC模型时，使用的数据是对数收益率，而不是残差，Stata会在输入的对数收益率数据的基础上建立ARIMA模型提取残差建模。

## 3. CCC-MGARCH 模型的应用

收集了 2000 年 1 月 3 日至 2019 年 11 月 25 日，美国道琼斯指数、上证综合指数和香港恒生指数的日度数据，之所以选取日度的时间序列，是因为利用高频的金融时间序列 (日度) 建模得到的残差序列，比低频的金融时间序列 (月度) 更容易出现 ARCH 效应。经平稳性检验可知，三个时间序列均为一阶单整，记为 I (1)。因此利用对数收益率序列建模，经 VAR 判断滞后阶数为 7 阶，提取 VAR 模型的残差，经检验具有显著的 ARCH 效应，接下来利用残差时间序列建立 CCC 模型，常相关系数的估计结果见表 1。由表 1 可知，美国道琼斯指数和上证综合指数波动的常相关系数为 0.111，美国道琼斯指数和香港恒生指数波动的常相关系数为 0.306，上证综合指数和香港恒生指数波动的常相关系数为 0.421。说明从选取的三个股票指数看，上证综指和恒生指数波动的相关性最高，美国道琼斯指数和恒生指数波动的相关性次之，上证综指和美国道琼斯指数波动的相关性相对最低，较为符合经济现实。

![](https://images.gitee.com/uploads/images/2020/0612/233245_49bac8b5_5748246.png)

## 4. 参考文献

- 徐国祥, 代吉慧. 中国与国际大宗商品市场价格之间的关联性研究[J]. 统计研究, 2015, 32(6): 81-89.
- Bollerslev T. Modelling the coherence in short-run nominal exchange rates: a multivariate generalized ARCH model[J]. The review of economics and statistics, 1990: 498-505.[[PDF]](http://public.econ.duke.edu/~boller/Published_Papers/restat_90.pdf)