StataBlogs---美国新冠病毒数据

作者：刘亮（三峡大学）

邮件：jetinskyliu0@gmail.com

Source:

> 原文：原文作者Chuck Huber，Associate Director of Statistical Outreach

> 原文标题：Import COVID-19 data from Johns Hopkins University

# 1简介

 本文介绍Chuck Huber博士编写的Covid19 Stata命令，并将约翰霍普金斯大学公布的全球新型冠状病毒数据为例，依次介绍如何使用Stata命令进行数据获取、清洗、保存以及数据可视化。



 该命令能生成一个包含日期、确诊病例、新增病例、死亡人数，以及康复人数的表格。为了能够查看具体国家病毒感染人数，作者添加**country（）,**增加**graph**选项画出确诊人数图,添加保存选项saving,将数据保存为(covid19_usa)



其基本命令为：`covid19, country(“US”) graph saving(covid19_usa)`



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/1us_vars.png)



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/2graph.png)

​                                ** Confirmed cases of COVID-19 in the United States**



# 2 约翰霍普金斯大学GitHub数据

GitHub是一个流行的软件开发和发布平台。约翰霍普金斯大学系统科学与工程中心（School of Engineering Center for Systems Science and Engineering）拥有一个包括来自世界各地的定期更新的COVID-19GitHub数据存储库。

> 其原始数据网址为：https://github.com/CSSEGISandData/COVID19/tree/master/csse_covid_19_data/csse_covid_19_daily_reports

打开网站后得到如下界面，可以看到部分数据如下图所示：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/41.29号数据.png)



每天的相关数据存储在独立的.csv文件中。以2020年1月29日的数据为例，查看相关内容。

点击“Raw”按键可以看到原始的数据，原始数据如下所示:

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/5rawdata.png)



# 3 数据下载

我们能够将这些原始数据（Raw data）使用import delimited命令将其导入到Stata中，首先以2020年1月29日为例。Stata的基本命令为：`import delimited https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/01-29-2020.csv`

导入数据后先输入`describe`命令，结果显示如下：



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/6describe.png)



输入`list in 1/5`查看前5个观测值。



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/7list_in_5.png)

# 4 数据处理

## 4.1修改变量名

我们看到变量provincestate在一些文件中被当成变量ïprovincestate。由于导入的文件很多，我们避免手动逐个查看。我们使用confirm命令去逐个查看每个文件中的ïprovincestate。其相关命令如下：



```Stata
confirm variable ïprovincestate
display_rc
```

输出结果为：0

如果变量ïprovincestate存在，command命令会为_rc存一个“0”值，从而我们可以使用rename 和label命令修改变量名和变量标签。相关命令如下：



```Stata
if _rc == 0{
	rename ïprovincestate provincestate
	label variable provincestate "Province/State"
}
```



执行完了if命令块后，使用`describe provincestate`命令，可以看到现在的数据中包含的变量为provincestate的相关信息。



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/10describe_province.png)



为了检查数据是否还存在ïprovincestate变量，再使用`comfirm variable ïprovincestate`其结果如下：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/11after_confirm.png)



可以发现原始数据中的变量已经被我们清理完成了。



## 4.2数据导入

## 4.2.1使用宏导入不同的文件

宏可以分为局部宏和全局宏。全局宏（global macro）一旦定义将贯穿整个Stata程序，局部宏（local macro）仅存在被定义的程序或do文件中。我们做一个示例定义并引用一个局部宏today，如下所示：

`local today = "3-19-2020"`

我们可以通过在宏名称使用左右引号进行引用。左引号（在“esc”键下方）和右引号（在“回车”键的左侧）来引用宏。命令如下：

```stata
local today = "3-19-2020"
display "`today'"
```

3-19-2020

我们可以通过整合其他宏来创建宏，例如:

```stata
local month = "3"
local day = "19"
local year = "2020"
local today = "`month'-`day'-`year'"
display "`today'"
```

3-19-2020

  我们想导入名为03-19-2020.csv的文件。注意月份包含一个“前置0”。1至9月被指定为：“01”，“02”往后依次类推。10至12月指定为“10”,“11",”12”。我们需要一种方法值赋时给月份添加适当的包含前导零的局部宏。String()函数能够满足这一要求。相关代码如下所示：



```stata
local month = string(3, "%02.0f")
local day = string(19, "%02.0f")
local year = "2020"
local today = "`month'-`day'-`year'"
display "`today'"
```

03-19-2020

接下来将前面的内容进行整合，使用宏来导入文件。命令为：

```Stata
local URL = "https://raw.githubusercontent.com/CSSEGISandData/  > COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/"
local FileName = "`URL'`today'.csv"
import delimited "`FileName'"
```

(8 vars,284 obs)

## 4.2.2使用循环读入多个文件

作者思路非常清晰，首先介绍简单的月循环，将每个月展示出来，然后介绍嵌套循环，将年和月共同展示。

作者调用宏将月进行循环，代码如下：



```Stata
forvalues month = 1/12{
	display "month = `month'"
}
```



回归结果如下：



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/16month回归结果.png)



继续将月和日共同循环,相关代码如下：



```Stata
forvalues month = 1/12{
	forvalues day = 1/31{
		display "month = `month', day = `day'"
	}
}
```

结果展示如下：



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/17日和月循环结果.png)



有了前文的介绍，继续运用暂元和循环的相关知识，展现2020年所有的日期。相关代码如下：



```Stata
forvalues month = 1/12{
	forvalues day = 1/31{
		local month = string(`month', "%02.0f")
		local day   = string(`day', "%02.0f")
		local year  = "2020"
		local today = "`month'-`day'-`year'"
		display "`today'"
	}
}
```

结果展示：



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/18年和月结果展示.png)



接下来作者使用关于循环和宏的命令将网站的所有数据均导入到Stata中，但在执行命令的过程中需要注意2点：

**疫情的数据从1月23号开始公布，在此之前并没有数据，如果不加以提示，会出现报错，程序停止运行。**

**原始数据中有的文件的变量名ïprovincestate，有的是provincestate，全部统一修改为provincestate。为了避免程序出现错误，使用capture命令，即使程序出现错误也可以继续执行。**

本部分命令如下：



```Stata
local URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/"
forvalues month = 1/12 {
	forvalues day = 1/31 {
      	local month = string(`month', "%02.0f")
      	local day = string(`day', "%02.0f")
      	local year = "2020"
      	local today = "`month'-`day'-`year'"
      	local FileName = "`URL'`today'.csv"
      	clear
      	capture import delimited "`FileName'"
      	capture save "`today'"
   }
}
```



输入`ls`命令 可以查看Stata中下载到的数据。



 ![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/19汇总数据.png)



打开01-22-2020.dta文件，并且输入`describe`查看数据。相关结果如下：



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20.02-22-2020.png)



我们看到其中的文件包括 **ïprovincestate**变量名，需要将其全部统一为**provincestate**

代码如下：

```Stata
local URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/"
forvalues month = 1/12 {
   forvalues day = 1/31 {
      local month = string(`month', "%02.0f")
      local day = string(`day', "%02.0f")
      local year = "2020"
      local today = "`month'-`day'-`year'"
      local FileName = "`URL'`today'.csv"
      clear
      capture import delimited "`FileName'"
      capture confirm variable ïprovincestate
      if _rc == 0 {
         rename ïprovincestate provincestate
         label variable provincestate "Province/State"
      }
   capture save "`today'", replace  //需要添加replace,替换掉之间存在内存中的数据
   }
}
```

现在打开01-22-2020.dta数据，并且`describe`结果如下:



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/21修改后的描述.png)



继续使用循环命令将所有的数据依次添加并保存，代码如下：



```Stata
clear
forvalues month = 1/12 {
	forvalues day = 1/31 {
    	local month = string(`month', "%02.0f")
      	local day = string(`day', "%02.0f")
      	local year = "2020"
      	local today = "`month'-`day'-`year'"
      	capture append using "`today'"
      }
}
```

最后使用`describe`描述最后生成的数据。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/22合并后的所有数据.png)



# 5数据更新与保存

随着疫情的不断发展，相关的数据不断更新，因此本文的相关数据和变量会产生不一致。例如**provoncestate**,**province_state**,**countryregion**,**country_region**等。当我们追加数据时必须要保证变量名称一致，接下来继续使用capture。其完整代码如下：



```Stata
//收集数据
local URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/"
forvalues month = 1/12 {
	forvalues day = 1/31 {
		local month = string(`month', "%02.0f")
      	local day = string(`day', "%02.0f")
      	local year = "2020"
      	local today = "`month'-`day'-`year'"
      	local FileName = "`URL'`today'.csv"
      	clear
      	capture import delimited "`FileName'"
      	capture confirm variable ïprovincestate
      	if _rc == 0 {
         	rename ïprovincestate provincestate
         	label variable provincestate "Province/State"
      }
      capture rename province_state provincestate
      capture rename country_region countryregion
      capture rename last_update lastupdate
      capture rename lat latitude
      capture rename long longitude
      capture save "`today'", replace
      }
}
clear
forvalues month = 1/12 {
   forvalues day = 1/31 {
      local month = string(`month', "%02.0f")
      local day = string(`day', "%02.0f")
      local year = "2020"
      local today = "`month'-`day'-`year'"
      capture append using "`today'"
   }
}
//合并数据
clear
forvalues month = 1/12 {
	forvalues day = 1/31 {
    	local month = string(`month', "%02.0f")
      	local day = string(`day', "%02.0f")
      	local year = "2020"
      	local today = "`month'-`day'-`year'"
      	capture append using "`today'"
   }
}

```



`describe`数据可以看到数据结果为：



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/23最后保存数据.png)



更新数据已经处理完毕，所有变量均统一，最后保存，为以后所用。



```Stata
save covid19_raw
```

# 6数据可视化

本文收集到的疫情数据截止到2020年4月2日，随着疫情的不断发展，相关数据可以到[https://github.com/CSSEGISandData/COVID19/tree/master/csse_covid_19_data/csse_covid_19_daily_reportszi](https://github.com/CSSEGISandData/COVID-19/tree/master/csse_covid_19_data/csse_covid_19_daily_reportszi)自行下载。

## 6.1中国疫情发展情况可视化

湖北省是国内疫情重灾区，我们心系湖北，所以先画出湖北省现存确诊人数。相关代码如下：



```Stata
use China_covid_0403.dta,clear
gen current_confirmed = confirmed - deaths - recovered //计算现存确诊人数
replace current_confirmed = 0 if current_confirmed < 0
keep province date current_confirmed
drop if missing(current_confirmed)
replace province = subinstr(province, " ", "_", .)
spread province current_confirmed  //生成面板
tw conn Hubei date, xtitle("日期", size(*1.0)) /// 
	ytitle("现存确诊人数", size(*0.8))  ///
	title("湖北省现存确诊人数变化）", size(*1.3)) ///
	subtitle("") caption("数据来源：约翰·霍普金斯大学", size(*0.8)) 
graph save hubei.png, replace
```



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/湖北省疫情图新.png)



我们同时也关注湖北省周边省市的疫情情况，同时画出周边四省的现存确诊人数，从上往下依次为河南、安徽、江西、湖南。相关代码如下：



```Stata
tw conn Henan Anhui Jiangxi Hunan date, xtitle("日期", size(*0.8)) /// 
	ytitle("现存确诊人数", size(*0.8)) ///
	title("湖北周边四省现存确诊人数变化", size(*1.1)) ///
	subtitle("河南、安徽、江西和湖南四省") caption("数据来源：约翰·霍普金斯大学", 		size(*0.8)) xla(21937(16)22007) ///
	xsc(range(21937 22017) extend) ///
	leg(order(1 "河南" 2 "安徽" 3 "江西" 4 "湖南") pos(1) ring(0)) ///
	lc("100 200 165" "252 141 98" "141 160 203" "231 138 195") ///
	mc("102 194 165" "252 141 98" "141 160 203" "231 138 195")
graph save Fourprovince.png, replace
```



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/新周边湖北四省情况.png)



接下来我们观察累计治愈人数，累计死亡病例数、每日新增治愈人数、每日确诊人数。相关代码如下：



```Stata
use China_covid_0403.dta,clear
collapse (sum) confirmed (sum) deaths (sum) recovered, by(date)
tsset date
gen new_confirmed = confirmed - l.confirmed
tw ///
lpolyci new_confirmed date, bw(3) || ///
sc new_confirmed date, ms(circle) ||, ///
	leg(off) xlab(, format(%tdCY-N-D)) xla(21937(16)22007) ///
	xsc(range(21937 22007) extend) ///
	ti("每日新增确诊数", size(*1.2)) ///
	yti("人数") name(a, replace) nodraw

gen current_confirmed = confirmed - deaths - recovered
replace current_confirmed = 0 if current_confirmed < 0
tw ///
lpolyci current_confirmed date, bw(3) || ///
sc current_confirmed date, ms(circle) ||, ///
	leg(off) xlab(, format(%tdCY-N-D)) xla(21937(16)22007) ///
	xsc(range(21937 22007) extend) ///
	ti("现存确诊数", size(*1.2)) ///
	yti("人数") name(b, replace) nodraw

graph combine a b, caption("数据来源：约翰霍普金斯大学", size(*0.8)) xsize(20) ysize(12) ///
	graphr(margin(medlarge))
graph export ab.png, replace

* 死亡和治愈病例数量
use China_covid_0403.dta,clear
collapse (sum) confirmed (sum) deaths (sum) recovered, by(date)
tsset date
tw ///
lpolyci recovered date, bw(3) || ///
sc recovered date, ms(circle) ||, ///
	leg(off) xlab(, format(%tdCY-N-D)) xla(21937(16)22007) ///
	xsc(range(21937 22000) extend) ///
	ti("累计治愈人数", size(*1.2)) ///
	yti("人数") name(a, replace) nodraw

tw ///
lpolyci deaths date, bw(3) || ///
sc deaths date, ms(circle) ||, ///
	leg(off) xlab(, format(%tdCY-N-D)) xla(21937(16)22007) ///
	xsc(range(21937 22000) extend) ///
	ti("累计死亡病例数", size(*1.2)) ///
	yti("人数") name(b, replace) nodraw

* 每日新增治愈
gen new_recovered = recovered - l.recovered
tw ///
lpolyci new_recovered date, bw(3) || ///
sc new_recovered date, ms(circle) ||, ///
	leg(off) xlab(, format(%tdCY-N-D)) xla(21937(16)22007) ///
	xsc(range(21937 22007) extend) ///
	ti("每日新增治愈人数", size(*1.2)) ///
	yti("人数") name(c, replace) nodraw
graph combine a b, caption("数据来源：约翰霍普金斯大学", size(*0.8)) xsize(20) ysize(12) ///
	graphr(margin(medlarge))
* 每日新增死亡
gen new_deaths = deaths - l.deaths
tw ///
lpolyci new_deaths date, bw(3) || ///
sc new_deaths date, ms(circle) ||, ///
	leg(off) xlab(, format(%tdCY-N-D)) xla(21937(16)22007) ///
	xsc(range(21937 22007) extend) ///
	ti("每日新增", size(*1.2)) ///
	yti("人数") name(d, replace) nodraw

graph combine a b c d, r(2) caption("数据来源：约翰霍普金斯大学", size(*0.8)) xsize(20) ysize(12) ///
	graphr(margin(medlarge))
graph save fourtype.png, replace
```



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/四合一累计治愈、累计死亡病例、每日新增,每日治愈、.png)



接下来我们分析治愈人数，死亡人数，现存确诊的动态图。



```Stata
use China_covid_0403.dta, clear
collapse (sum) confirmed (sum) deaths (sum) recovered, by(date)
* 计算累积量
gen base = 0
gen deaths_recovered = deaths + recovered
* 绘图
tw ///
rarea recovered base date, fc("180 209 132") ///
	c("180 209 132") fintensity(inten80) lw(vvthin) || ///
rarea deaths_recovered recovered date, fc("122 118 123") ///
	c("122 118 123") fintensity(inten80) lw(vvthin) || ///
rarea confirmed deaths_recovered date, fc("95 144 161") ///
	c("95 144 161") fintensity(inten80) lw(vvthin) ||, ///
	leg(order(3 "现存确诊" 2 "死亡" 1 "治愈") pos(11) ring(0)) ///
	title("新冠肺炎疫情在中国的发展状况", size(*1.5) ///
		justification(left) bexpand) ///
	subtitle("中国新冠肺炎的确诊人数、治愈人数和死亡人数的发展趋势。", ///
		justification(left) bexpand) ///
	caption("数据来源：约翰霍普金斯大学", ///
		size(*0.8)ring(0)) ///
	xtitle("") xla(21937(8)22000) ///
	xsc(range(21937 22000) extend) ///
	graphr(margin(medlarge)) ///
	text(20000 21984 "治愈", color(black) size(*1.2)) ///
	text(63000 21983 "死亡", color(black) size(*1.2)) ///
	text(70000 21978 "现存确诊", color(black) size(*1.2)) || ///
pci 80000 21957 0 21957, lp(dash) text(56000 21952 "我国改变了" "确诊的标准", color(black) size(*0.8) justification(right) bexpand)
graph save dynamic.png,replace
```



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/新冠肺炎在中国发展图.png)



## 6.2世界疫情发展可视化

新冠肺炎在全球蔓延，约翰霍普金斯大学同时收集了各个国家的疫情数据。本部分内容将绘制新冠肺炎在世界蔓延情况。



首先绘制截止到4月2号全球疫情最严重的12个国家。本部分代码借鉴TidyFriday公众号《实时疫情与Stata地图绘制 》一文



```Stata
use global_covid_0403.dta,replace
* 全球疫情最严重的12个国家现存确诊人数
use global_covid_0403.dta,clear
gen current_confirmed = confirmed - deaths - recovered
gsort - current_confirmed
sencode country,gen(country_id) 
drop country
rename country_id country
keep in 1/12
twoway bar current_confirmed country,sort horizontal barwidth(0.5) ///
	fcolor(red) ylabel(1(1)12,valuelabel angle(0) labsize(*1.2)) ///
	ytitle("") xtitle("现存确诊人数") ysize(2) xsize(4) title(截止2020年4月2日确诊人数) ///
	caption("数据来源：约翰霍普金斯大学")
graph save global_12.png,replace
```



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/全球最严重的12个国家.png)



接下来绘制世界疫情图，本文部分代码借鉴TidyFriday公众号《使用Stata分析全球的新冠疫情数据  TidyFriday》



```Stata
use global_covid_0403.dta,clear
* 修改国家
replace country = "United States of America" if country == "US"
replace country = "The Bahamas" if country == "Bahamas"
replace country = "Republic of the Congo" if country == "Congo (Brazzaville)"
replace country = "Democratic Republic of the Congo" if country == "Congo (Kinshasa)"
replace country = "Ivory Coast" if country == "Cote d'Ivoire"
replace country = "Czech Republic" if country == "Czechia"
replace country = "Guinea Bissau" if country == "Guinea-Bissau"
replace country = "South Korea" if country == "Korea, South"
replace country = "Macedonia" if country == "North Macedonia"
replace country = "Republic of Serbia" if country == "Serbia"
replace country = "United Republic of Tanzania" if country == "Tanzania"
replace country = "East Timor" if country == "Timor-Leste"
rename country name
merge 1:1 name using worlddb
* 绘制地图
replace confirmed = 0 if missing(confirmed)
xtset, clear
grmap confirmed using worldcoord, id(ID) ///
	fcolor("252 255 164" "250 193 39" "245 125 21" "212 72 66" "159 42 99" "101 21 110" "40 11 84") ///
	ocolor("white" ...) ///
	clmethod(custom) clbreaks(0 0.9 9 99 999 9999 99999 999990) /// 
	title("新型冠状病毒肺炎疫情分布", size(*1.2) color(white)) ///
	graphr(margin(medium)) ///
	subtitle("截止 2020 年 4 月 2 日", color(white)) ///
	osize(vthin ...) ///
	legend(size(*1.5) ///
		order(2 "无" 3 "1～9 人" 4 "10～99 人" 5 "100~999 人" 6 "1000~9999人" 7 "10000~100000 人" 8 ">= 100000 人") ///
		ti(确诊, size(*0.5) pos(11) color(white)) color(white)) ///
	plotr(fcolor(24 24 24) lcolor(24 24 24)) ///
	graphr(fcolor(24 24 24) lcolor(24 24 24)) ///
	xsize(20) ysize(8.510638298) ///
	caption("数据来源：约翰·霍普金斯大学", size(*0.8) color(white))
graph save global_confirmed.png,replace
```



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/世界疫情图.png)





附录：

全文代码

```Stata
cd "D:\stata15\ado\personal\covid_date_chen"
****************************
*******收集数据*************
****************************
//收集数据//合并数据//保存数据

* 收集数据
local URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/"
forvalues month = 1/12 {
   forvalues day = 1/31 {
      local month = string(`month', "%02.0f")
      local day = string(`day', "%02.0f")
      local year = "2020"
      local today = "`month'-`day'-`year'"
      local FileName = "`URL'`today'.csv"
      clear
      capture import delimited "`FileName'"
      capture confirm variable ïprovincestate
      if _rc == 0 {
         rename ïprovincestate provincestate
         label variable provincestate "Province/State"
      }
      capture rename province_state provincestate
      capture rename country_region countryregion
      capture rename last_update lastupdate
      capture rename lat latitude
      capture rename long longitude
      capture save "`today'", replace
      }
}
clear
forvalues month = 1/12 {
   forvalues day = 1/31 {
      local month = string(`month', "%02.0f")
      local day = string(`day', "%02.0f")
      local year = "2020"
      local today = "`month'-`day'-`year'"
      capture append using "`today'"
   }
}
* 合并数据
clear
forvalues month = 1/12 {
   forvalues day = 1/31 {
      local month = string(`month', "%02.0f")
      local day = string(`day', "%02.0f")
      local year = "2020"
      local today = "`month'-`day'-`year'"
      capture append using "`today'"
   }
}

* 保存数据
save covid_0402_chen.dta,replace
***********************************
***********数据处理****************
***********************************
* 保存主要变量
use covid_0402_chen.dta,clear
keep provincestate countryregion lastupdate confirmed deaths recovered 

* 缩小字符串
format provincestate %15s
format countryregion %15s

* 将时间变量lastupdate分成6个类型
gen lastupdate_01 = lastupdate in 1/38
gen lastupdate_02 = lastupdate in 39/431
gen lastupdate_03 = lastupdate in 432/560
gen lastupdate_04 = lastupdate in 561/7617
gen lastupdate_05 = lastupdate in 7618/11034
gen lastupdate_06 = lastupdate in 11035/45927

rename lastupdate_01 type1
rename lastupdate_02 type2
rename lastupdate_03 type3
rename lastupdate_04 type4
rename lastupdate_05 type5
rename lastupdate_06 type6
//完成type1
gen y_01 = real(substr(type1,6,4))
gen m_01 = real(substr(type1,1,1))
gen d_01 = real(substr(type1,3,2))
gen new_type1 = string(y_01) + "-" + "0" + string(m_01) + "-" + string(d_01)

//完成type2
gen y_02 = real(substr(type2,6,2))
gen m_02 = real(substr(type2,1,1))
gen d_02 = real(substr(type2,3,2))
gen new_type2 = string(y_02) + "20" + "-" + "0" + string(m_02) + "-" + string(d_02) 

//完成type3
replace type3 = subinstr(type3,"2/1/2020","2/01/2020",.)
gen y_03 = real(substr(type3,6,4))
gen m_03 = real(substr(type3,1,1))
gen d_03 = real(substr(type3,3,2))
gen new_type3 = string(y_03) + "-" + "0" + string(m_03) + "-" + string(d_03) 
replace new_type3 = subinstr(new_type3,"2020-02-1","2020-02-01",.)


//完成type4
gen new_type4 = substr(type4,1,10)

//完成type5
gen y_05 = real(substr(type5,6,2))
gen m_05 = real(substr(type5,1,1))
gen d_05 = real(substr(type5,3,2))
gen new_type5 = string(y_05) + "20" + "-" + "0" +string(m_05) + "-" + string(d_05)

//完成type6
gen new_type6 = substr(type6,1,10)

gen time = new_type1 in 1/38 
gen time2 = new_type2 in 39/431
gen time3 = new_type3 in 432/560
gen time4 = new_type4 in 561/7617
gen time5 = new_type5 in 7618/11034
gen time6 = new_type6 in 11035/45927

//time2-time6手动合并到new_time 
gen new_time = time6 in 11035/45927 

//检查是否完整生成时间变量
gen date = date(new_time,"YMD")
gen v1 = "100" if date ==.   // 有缺失值
list v1 if v1 == "100"       //找到缺失值
replace new_time = "2020-03-08" in 10847
replace new_time = "2020-03-08" in 10848

gen date1 = date(new_time,"YMD")
format date1 %tdCY-N-D

keep provincestate countryregion confirmed deaths recovered date1 
rename date1 date

* 数据处理过程保留
save date_complete_covid_0403.dta,replace

* 将缺失值替换为0
replace confirmed = 0 if confirmed ==.
replace deaths = 0 if deaths ==.
replace recovered = 0 if recovered == .

//********画中国的图*******
* 保留中国的数据
keep if inlist(countryregion,"Mainland China","China","Hong Kong","Taiwan","Taiwan*","Macau") //如何判断一个变量中有这几个值

* 完善省份名称
replace provincestate = "Taiwan" if countryregion == "Taiwan*"
rename provincestate province
* 生成面板的中国数据
encode province,gen(province_id)
drop countryregion
xtset province_id date   

//处理重复值
duplicates list province_id date
duplicates tag province_id date,gen(isdup) 
edit if isdup
drop isdup
duplicates drop province_id date,force

xtset province_id date //生成面板数据
save China_covid_0403.dta,replace //保存中国疫情数据

******************************
*********可视化画图**************
*******************************
use China_covid_0403.dta,clear
gen current_confirmed = confirmed - deaths - recovered   //计算现存确诊人数计算图
replace current_confirmed = 0 if current_confirmed < 0
keep province date current_confirmed
drop if missing(current_confirmed)
replace province = subinstr(province, " ", "_", .)
spread province current_confirmed

*湖北省现存确诊人数图
tw conn Hubei date, xti("日期", size(*0.8)) /// 
	ytitle("现存确诊人数", size(*0.8)) ///
	title("湖北省现存确诊人数变化", size(*1.3)) ///
	subtitle("") caption("数据来源：约翰·霍普金斯大学", size(*0.8))
graph save hubei.png, replace

*湖北省周边四省现存确诊人数图
tw conn Henan Anhui Jiangxi Hunan date, xtitle("日期", size(*0.8)) /// 
	ytitle("现存确诊人数", size(*0.8)) ///
	title("湖北周边四省现存确诊人数变化", size(*1.1)) ///
	subtitle("河南、安徽、江西和湖南四省") caption("数据来源：约翰·霍普金斯大学", size(*0.8)) xla(21937(16)22000) ///
	xsc(range(21937 22010) extend) ///
	leg(order(1 "河南" 2 "安徽" 3 "江西" 4 "湖南") pos(1) ring(0)) ///
	lc("100 200 165" "252 141 98" "141 160 203" "231 138 195") ///
	mc("102 194 165" "252 141 98" "141 160 203" "231 138 195")
graph save Fourprovince.png, replace

*************************
**地图二
*死亡和治愈病例数
* 当前确诊人数与每日新增确诊人数
use China_covid_0403.dta,clear
collapse (sum) confirmed (sum) deaths (sum) recovered, by(date)
tsset date
gen new_confirmed = confirmed - l.confirmed
tw ///
lpolyci new_confirmed date, bw(3) || ///
sc new_confirmed date, ms(circle) ||, ///
	leg(off) xlab(, format(%tdCY-N-D)) xla(21937(16)22007) ///
	xsc(range(21937 22007) extend) ///
	ti("每日新增确诊数", size(*1.2)) ///
	yti("人数") name(a, replace) nodraw

gen current_confirmed = confirmed - deaths - recovered
replace current_confirmed = 0 if current_confirmed < 0
tw ///
lpolyci current_confirmed date, bw(3) || ///
sc current_confirmed date, ms(circle) ||, ///
	leg(off) xlab(, format(%tdCY-N-D)) xla(21937(16)22007) ///
	xsc(range(21937 22007) extend) ///
	ti("现存确诊数", size(*1.2)) ///
	yti("人数") name(b, replace) nodraw

graph combine a b, caption("数据来源：约翰霍普金斯大学", size(*0.8)) xsize(20) ysize(12) ///
	graphr(margin(medlarge))
graph export ab.png, replace

* 死亡和治愈病例数量
use China_covid_0403.dta,clear
collapse (sum) confirmed (sum) deaths (sum) recovered, by(date)
tsset date
tw ///
lpolyci recovered date, bw(3) || ///
sc recovered date, ms(circle) ||, ///
	leg(off) xlab(, format(%tdCY-N-D)) xla(21937(16)22007) ///
	xsc(range(21937 22000) extend) ///
	ti("累计治愈人数", size(*1.2)) ///
	yti("人数") name(a, replace) nodraw

tw ///
lpolyci deaths date, bw(3) || ///
sc deaths date, ms(circle) ||, ///
	leg(off) xlab(, format(%tdCY-N-D)) xla(21937(16)22007) ///
	xsc(range(21937 22000) extend) ///
	ti("累计死亡病例数", size(*1.2)) ///
	yti("人数") name(b, replace) nodraw

* 每日新增治愈
gen new_recovered = recovered - l.recovered
tw ///
lpolyci new_recovered date, bw(3) || ///
sc new_recovered date, ms(circle) ||, ///
	leg(off) xlab(, format(%tdCY-N-D)) xla(21937(16)22007) ///
	xsc(range(21937 22007) extend) ///
	ti("每日新增治愈人数", size(*1.2)) ///
	yti("人数") name(c, replace) nodraw
gr combine a b, caption("数据来源：约翰霍普金斯大学", size(*0.8)) xsize(20) ysize(12) ///
	graphr(margin(medlarge))
* 每日新增死亡
gen new_deaths = deaths - l.deaths
tw ///
lpolyci new_deaths date, bw(3) || ///
sc new_deaths date, ms(circle) ||, ///
	leg(off) xlab(, format(%tdCY-N-D)) xla(21937(16)22007) ///
	xsc(range(21937 22007) extend) ///
	ti("每日新增", size(*1.2)) ///
	yti("人数") name(d, replace) nodraw

gr combine a b c d, r(2) caption("数据来源：约翰霍普金斯大学", size(*0.8)) xsize(20) ysize(12) ///
	graphr(margin(medlarge))
graph save fourtype.png, replace

* 地图三  每天增长总数 //时间轴比较拥挤如何处理
use China_covid_0403.dta, clear
collapse (sum) confirmed (sum) deaths (sum) recovered, by(date)
* 计算累积量
gen base = 0
gen deaths_recovered = deaths + recovered
* 绘图
tw ///
rarea recovered base date, fc("180 209 132") ///
	c("180 209 132") fintensity(inten80) lw(vvthin) || ///
rarea deaths_recovered recovered date, fc("122 118 123") ///
	c("122 118 123") fintensity(inten80) lw(vvthin) || ///
rarea confirmed deaths_recovered date, fc("95 144 161") ///
	c("95 144 161") fintensity(inten80) lw(vvthin) ||, ///
	leg(order(3 "现存确诊" 2 "死亡" 1 "治愈") pos(11) ring(0)) ///
	title("新冠肺炎疫情在中国的发展状况", size(*1.5) ///
		justification(left) bexpand) ///
	subtitle("中国新冠肺炎的确诊人数、治愈人数和死亡人数的发展趋势。", ///
		justification(left) bexpand) ///
	caption("数据来源：约翰霍普金斯大学", ///
		size(*0.8)ring(0)) ///
	xti("") xla(21937(8)22000) ///
	xsc(range(21937 22000) extend) ///
	graphr(margin(medlarge)) ///
	text(20000 21984 "治愈", color(black) size(*1.2)) ///
	text(63000 21983 "死亡", color(black) size(*1.2)) ///
	text(70000 21978 "现存确诊", color(black) size(*1.2)) || ///
pci 80000 21957 0 21957, lp(dash) text(56000 21952 "我国改变了" "确诊的标准", color(black) size(*0.8) justification(right) bexpand)
graph save dynamic.png, replace

*************************************
*绘制世界地图
use date_complete_covid_0403.dta,clear
replace confirmed = 0 if confirmed ==.
replace deaths = 0 if deaths ==.
replace recovered = 0 if recovered ==.
* 保存变量*************************************************************
keep provincestate countryregion date confirmed deaths recovered

* 准备截面数据
keep if date == date("2020-04-02","YMD") 
replace countryregion = "China" if countryregion == "Taiwan*"

* 将各国数据汇总
bysort countryregion :egen confirmed_sum = sum(confirmed)
bysort countryregion :egen deaths_sum = sum(deaths)
bysort countryregion :egen recovered_sum = sum(recovered)

gsort -confirmed_sum //由大到小进行排序
drop in 2/2312 //保留美国
drop in 6/20  //保留中国
drop in 7/15 //法国
drop in 9/17 //英国
drop in 13/16 //荷兰
drop in 14/27 //加拿大
drop in 21/27 //澳大利亚
drop in 24/25 //保存丹麦

keep countryregion date confirmed_sum deaths_sum recovered_sum
rename confirmed_sum confirmed
rename deaths_sum deaths
rename recovered_sum recovered
rename countryregion country
save global_covid_0403.dta,replace  //全球数据保存完成

*绘制12个国家的疫情图
* 全球疫情最严重12个国家的现存确诊人数
use global_covid_0403.dta,clear
gen current_confirmed = confirmed - deaths - recovered
gsort - current_confirmed
sencode country,gen(country_id) 
drop country
rename country_id country
keep in 1/12
save global_12_covid_0403.dta,replace

twoway bar current_confirmed country,sort horizontal barwidth(0.5) ///
	fcolor(red) ylabel(1(1)12,valuelabel angle(0) labsize(*1.2)) ///
	ytitle("") xtitle("现存确诊人数") ysize(2) xsize(4) title(截止2020年4月2日确诊人数) ///
	caption("数据来源：约翰霍普金斯大学")
graph save global_12.png,replace

*******************************
*绘制世界疫情图
use global_covid_0403.dta,clear
* 修改国家
replace country = "United States of America" if country == "US"
replace country = "The Bahamas" if country == "Bahamas"
replace country = "Republic of the Congo" if country == "Congo (Brazzaville)"
replace country = "Democratic Republic of the Congo" if country == "Congo (Kinshasa)"
replace country = "Ivory Coast" if country == "Cote d'Ivoire"
replace country = "Czech Republic" if country == "Czechia"
replace country = "Guinea Bissau" if country == "Guinea-Bissau"
replace country = "South Korea" if country == "Korea, South"
replace country = "Macedonia" if country == "North Macedonia"
replace country = "Republic of Serbia" if country == "Serbia"
replace country = "United Republic of Tanzania" if country == "Tanzania"
replace country = "East Timor" if country == "Timor-Leste"
rename country name
merge 1:1 name using worlddb

* 绘制地图
replace confirmed = 0 if missing(confirmed)
xtset, clear
grmap confirmed using worldcoord, id(ID) ///
	fcolor("252 255 164" "250 193 39" "245 125 21" "212 72 66" "159 42 99" "101 21 110" "40 11 84") ///
	ocolor("white" ...) ///
	clmethod(custom) clbreaks(0 0.9 9 99 999 9999 99999 999990) /// 
	title("新型冠状病毒肺炎疫情分布", size(*1.2) color(white)) ///
	graphr(margin(medium)) ///
	subtitle("截止 2020 年 4 月 2 日", color(white)) ///
	osize(vthin ...) ///
	legend(size(*1.5) ///
		order(2 "无" 3 "1～9 人" 4 "10～99 人" 5 "100~999 人" 6 "1000~9999人" 7 "10000~100000 人" 8 ">= 100000 人") ///
		ti(确诊, size(*0.5) pos(11) color(white)) color(white)) ///
	plotr(fcolor(24 24 24) lcolor(24 24 24)) ///
	graphr(fcolor(24 24 24) lcolor(24 24 24)) ///
	xsize(20) ysize(8.510638298) ///
	caption("数据来源：约翰·霍普金斯大学", size(*0.8) color(white))
graph save global_confirmed.png,replace
```



# 参考资料

> 实时疫情与Stata地图绘制   TidyFriday
>
> 使用Stata分析全球的新冠疫情数据  TidyFriday
>
> 用Stata绘制地图    Stata之禅