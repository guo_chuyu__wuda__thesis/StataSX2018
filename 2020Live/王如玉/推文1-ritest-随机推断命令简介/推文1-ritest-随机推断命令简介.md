> **作者：** 王如玉（中国人民银行金融研究所）
>
> **E-mail:** 15242968@qq.com

> **Source:** 本文部分摘译自以下文章：
>
> [Finally, a way to do easy randomization inference in Stata!](https://blogs.worldbank.org/impactevaluations/finally-way-do-easy-randomization-inference-stata)
>
> [Randomization inference vs. bootstrapping for p-values.](https://jasonkerwin.com/nonparibus/2017/09/25/randomization-inference-vs-bootstrapping-p-values/)

# ritest-随机推断命令简介

随机推断（Randomization Inference）越来越多地成为分析随机实验数据的推荐方法，尤其是在样本量较少，具有聚类随机性或高杠杆点的样本中。但由于此前 Stata 中没有现成的命令来实现它，一直未能广泛应用于经济学中，直到 2017 年， Simon Heß，一位法兰克福大学的博士生编写了 `ritest` 命令。`ritest` 命令非常简单易用，它的主要理论依据和范例均已发表在 [Stata Journal](https://sci-hub.tw/10.1177/1536867X1701700306)，感兴趣的同学可以参考阅读。本文将对随机推断和 `ritest` 命令进行简介。

## 1. 什么是随机推断？

应用微观经济学中的一个普遍难题是，例如，在给定地区的所有学校中进行了一项实验，以求获得学校级别的结果。或者，试图研究一项在美国各州分别推行的政策，样本则是各州的执行结果。那么，这些结果的标准误差和 *p* 值到底意味着什么？毕竟，此处不存在采样误差，而我们通常在回归分析中使用的推断手段都是基于采样误差的。

答案是：要使用的正确 *p* 值必须能捕获不确定性，即将样品中的哪些数据分配给实验组（而不是对照组）（Kerwin, 2017）。这时我们就要用到随机推断。

### 1.1 随机推断的定义

随机推断是一种计算回归 *p* 值的方法。在随机对照实验中，随机推断不仅考虑在实验所采用的随机分配下会发生什么，还包括在所有可能的随机分配下会发生什么：结果是否都会成立？（The World Bank, 2020）可以说，随机推断就是对因果效应的推断过程进行物理上的随机化。

随机推断在样本较小的时候很有用。

### 1.2 随机推断的步骤

进行随机推断，首先需要确定：**严格零假设**（即实验对样本中的每个单元都没有作用）、采用的统计量、双边检验还是单边检验。然后实行如下步骤：

1. 保留样本的原始实验分配。
2. 根据原始实验组分配方式，重新随机分配实验组。
3. 用“假的”，或者说“安慰剂”实验组作为一个附加项，重新估计原始回归方程。
4. 重复以上 1-3 过程。
5. 随机推断 *p* 值是*安慰剂处理效果大于估计实验效果的次数的比例。*

随机推断的 *p* 值意义解释为 **“在选择的随机方法的不同假设实现下，观测到相似大小的实验效果的概率”**。

常规 *p* 值的意义为“在零假设成立时，得到比现有的样本观测结果更极端的结果的概率”。 *p* 值越小，拒绝零假设的理由越充分。

### 1.3 一个小例子
举一个简单的例子，我们想说明上重点中学与考 985 高校有无关系。我们随机选取 100 个中学生的样本，其中 30 人为重点中学学生，即实验组。70 人为普通中学学生，即对照组。零假设，即是否上重点中学对所有学生是否考上 985 高校都没有影响。选择一个统计量，例如高考成绩的 *z* 值（样本平均值差异）。假设我们获得的 *z* 值为 10 。重新从这 100 个中学生选取 30 人为实验组，70 人为对照组，并再次计算 *z* 值，并重复该过程 1000 次。这 1000 次中，有 170 次的 值大于等于 10，则我们得出随机推断的单边 *p* 值为 0.17 。在通常的 95% 置信区间下，则无法拒绝零假设。

更复杂一点，我们可以设计如下的回归方程：

$ y_i = \beta_0 + \beta_1 School + \theta y^{baseline}_i + \epsilon_i $

其中，$y^{baseline}$是末次模考成绩。*School* 代表是否在重点中学。

用原始样本得出 $\beta_1$ 的值，并按以上过程重复 1000 次，得出 1000 个 $\beta_1$ 的值，其中有 170 个值大于原始值，则随机推断的 *p* 值为 0.17 。

需注意，理论上要获得准确的 *p* 值，需构建所有可能的选择，例如从 7 个样本中选取 3 个为实验组，就有 35 种可能性。但当样本量较大时，如以上例子，构建所有可能的选择并不实际，可以随机重复一个较大的次数。

## 2. ritest命令的应用

### 2.1 使用说明

在 Stata 中输入`findit ritest`，即可获得该命令。这是初始版本，建议安装最新版本，它修正了初始版本的一些问题。输入以下命令：

```Stata
. net describe ritest, from(https://raw.githubusercontent.com/simonheb/ritest/master/)
```

如果因防火墙等原因导致更新版本安装失败，可以直接下载 [ritest.ado](https://raw.githubusercontent.com/simonheb/ritest/master/ritest.ado) 文件和 [ritest.sthlp](https://raw.githubusercontent.com/simonheb/ritest/master/ritest.sthlp) 文件放入 Stata 安装目录下的 `\ado\base\r` 目录中。

### 2.1.1 语法

`ritest` 命令的通用语法如下：

```stata
. ritest resampvar exp list [, options]: command
```

具体解释如下：

* `resampvar` 为需要重新抽样的变量名。例如，一个治疗指示变量 **treatment**
* `exp list` 为在每一步需要抽样并和主估计样本进行比较的表达式，例如，一个回归系数 **_b[treatment]** 
* `options` 定义了重新抽样过程的细节，对p值的计算，输出等。其中部分参数具体说明如下：
  * `reps(#)` 执行随机排练的次数，默认为100次
  * `left|right` 计算单边 *p* 值，默认为双边
  * `level(#)` 置信区间，默认为95
  * `fixlevels()` 将重新随机过程限制于部分实验变量，可被用于多重实验组成对测试的情况
  * `seed(#)` 设定随机数种子
* `command` 为每次重复时执行的回归程序，例如 **regress testscore treatment age**

 以上例子可写成：

```stata
. ritest treatment _b[treatment], cluster(class) strata(school): regress testscore treatment age
```

### 2.1.2 自定义重新抽样方法

`ritest` 命令允许使用者自定义更复杂的重新抽样方法，具体见下。

* **自动重新采样**

  使用者可以定义**分层（strata）**或**聚类（cluster）**。分层和聚类都是分组随机抽样的方式，其主要区别在于，分层是从每一组中随机抽样，例如上文例子，我们研究的是某市的 20 个中学（包括重点与普通中学）的学生，则根据就读的中学分组，在 20 个中学各随机抽取一定的样本人数。而聚类则是将分组整体视为一个单元，例如，仍然根据就读的中学分组，但随机抽取 2 个重点中学和 2 个普通中学的全部高三学生。

  语法如下：
  
  ```Stata
  . ritest resampvar exp_list, reps(#) strata(varlist) cluster(varlist): command
  ```
  
  主要参数说明：
  
  * `strata(varlist)` 在分层内重新排列 `resampvar`
  * `cluster(varlist)` 在聚类内保持`resampvar` 为常数
  
  该语句根据分层 `strata()` 和聚类 `cluster()`（可选），随机排列`resampvar` # 次，每次都执行语句并收集 `exp_list` 中变量的实现值。不设置分层即假设所有观测值在同一个单一分层中。不设置聚类即假设每个观测值为单一聚类。这是最简单但最受限的抽样方法，但在连续变量时可能会存在很大的局限，建议使用如下两种方法之一。
  
* **基于文件的重新抽样**

  使用者可以从外部文件读取对需要重新抽样的变量进行排列的方法。语法如下：

  ```stata
  . ritest resampvar exp_list, reps(#) samplingsourcefile(filename) samplingmatchvar(varlist): command
  ```

  主要参数说明：

  * `samplingsourcefile(filename)` 从 Stata 数据文件 `filename` 中获取 `resampvar` 的重新排列，文件包含命名为`resampvar1, resampvar2, resampvar3, ...` 的变量
  * `samplingmatchvar(varlist)` 将 `samplingsourcefile()`  中的排列与现有数据合并，用 `varlist` 中的变量 （1:1 或 m:1）

  该语句用 `samplingsourcefile()` 中的文件，根据 `samplingmatchvar()` 中列明的解释变量，将数据合并 # 次（1:1，如果解释变量非唯一，则 m:1）。每次都执行语句，并用 `resampvar1, resampvar2, resampvar3, ...` 逐次代换 `resampvar`。`samplingsourcefile()` 必须在执行语句前手动创建。

* **基于程序的重新抽样**

  使用者可以自行撰写重新抽样的程序。语法如下：

  ```Stata
  . ritest resampvar exp_list, reps(#) samplingprogram(progname) samplingprogramoptions(string): command
  ```

  主要参数说明：

  * `samplingprogram(programname)` 根据用户编写的程序 `programname` 对 `resampvar` 重新排列
  * `samplingprogramoptions(string)` 可选，将 `string` 作为选项传递给 `programname`

  该语句通过执行 `samplingprogram()` 并将 `samplingprogramoptions()`作为其可选项，重新排列 `resampvar`。这是最多样化，可应用最广的方法。如果实验的原始随机过程是由一个 do 文件生成的，原始程序可以用于 `samplingprogram()`。通过编写程序可以一次重新抽样多个变量。

### 2.2 应用范例

### 2.2.1 对分层内的个体随机分组

本例子来自 Mckenzie (2017) 的“寻找和激发高增长创业：来自一个商业计划竞赛的实验证据”，原文章可以从[这里](https://drive.google.com/open?id=0B9C9RwWKZrUNdzhGSm81UDFCMG8)下载，以下三个例子所使用的 do 文件和数据均可以从[这里](https://drive.google.com/open?id=0B9C9RwWKZrUNazdyVXFkSTlTNGc)下载。

该实验对尼日利亚的一个青年创业竞赛对就业和公司利润的影响进行了跟踪调研。实验随机选取该竞赛的获奖者，分配为实验组与对照组，奖金根据他们的商业计划的实施情况，分四个阶段支付，并进行了三轮跟踪问卷调查。

例如，为了研究第二轮调查时还在运行的公司的阶段利润，在 Stata 中使用如下命令：

```Stata
. use "Nigeriablogdata.dta", clear
. areg s_prof_trunc assigntreat, a(strata) robust
. ritest assigntreat _b[assigntreat], reps(5000) strata(strata) seed(125): areg s_prof_trunc assigntreat, a(strata) robust
```

首先进行回归。**s_prof_trunc** 为阶段利润，**assigntreat** 为实验组控制变量，同时控制随机分层。该回归的 *p* 值为 0.051 。

然后使用 `ritest`，告诉它控制变量是什么，想要什么系数的 *p* 值，在随机分层中进行随机推断，并重复 5000 次。输出结果如下：

```Stata
..................................................  5000

      command:  areg s_prof_trunc assigntreat, a(strata) robust
        _pm_1:  _b[assigntreat]
  res. var(s):  assigntreat
   Resampling:  Permuting assigntreat
Clust. var(s):  __000000
     Clusters:  541
Strata var(s):  strata
       Strata:  12

------------------------------------------------------------------------------
T            |     T(obs)       c       n   p=c/n   SE(p) [95% Conf. Interval]
-------------+----------------------------------------------------------------
       _pm_1 |   69.23354     256    5000  0.0512  0.0031  .0452543   .0576763
------------------------------------------------------------------------------
Note: Confidence interval is with respect to p=c/n.
Note: c = #{|T| >= |T(obs)|}
```

此处 *p* 值为 0.0512 ，这与原始回归的结果非常相近。这是个体级别的随机化，样本量为 497 ，因此我们可以认为随机推断的结果与回归相似。

**注意：** 此处原作者使用的原版 `ritest` 命令可能会忽略随机种子，即每次随机得出的结果不同。原作者使用 1000 次重复和种子 125 ，获得的 *p* 值为 0.47 。更新版本修正了这一问题，因此笔者根据原数据及 do 文件运行获得的 *p* 值有变化，为 0.37 。使用 5000 次重复获得的结果更接近于回归所得的 *p* 值。

### 2.2.2 对有多个时间段和聚类标准差的个体的随机分组

此处同样使用以上数据，但对其进行了重新调整，以便估计第二轮和第三轮跟踪随访数据的合并效应。使用回归得出的 *p* 值为 0.096 。然后，通过增加 `cluster` 选项（以 *uid* 为个体标识符）在进行重新排列时将聚类性质考虑在内：

```Stata
//合并第二、三轮数据
gen prof_trunc2=s_prof_trunc
gen prof_trunc3=t_prof_trunc
gen uid=_n
keep uid prof_trunc2 prof_trunc3 group existing assigntreat strata
reshape long prof_trunc ,i(uid) j(time) 

gen time3=time==3
areg prof_trunc assigntreat time3 if group<=2 & existing==1, a(strata) robust cluster(uid) //回归
ritest assigntreat _b[assigntreat], reps(1000) strata(strata) cluster(uid) seed(124): areg prof_trunc assigntreat time3 if group<=2 & existing==1, a(strata) robust cluster(uid)
```

得到的 *p* 值为 0.118（注：原作者在此处得到的结果为 0.100 ，原因同上），与原本估计的回归值非常相似。但如果没有指定 `cluster()` 选项，将得到一个人为降低的 *p* 值，0.068 。

### 2.2.3 对有分层的聚类进行随机分组

本例子来自 Mckenzie (2017)  的一篇关于缩短供应链的工作论文。这个例子中，公司所在市场被划分为 63 个区域，这些区域又被划分为 31 个分层（ 30 对和一个三元组）；在 63 个市场区域层级进行聚类随机化。我们想要知道，一周中有多少天公司在中央市场购物。在具有聚类标准差的回归中获得的 *p* 值是 0.024 。由于随机推断可以使聚类相对较少的情况下聚类随机产生更大的变化，此处又能得到什么不同呢？使用如下命令：

```Stata
. use "clusterColombia.dta", clear
. areg dayscorab b_treat b_dayscorab miss_b_dayscorab round2 round3, cluster(b_block) a(b_pair)
. ritest b_treat _b[b_treat], reps(5000) cluster(b_block) strata(b_pair) seed(546): areg dayscorab b_treat b_dayscorab miss_b_dayscorab round2 round3, cluster(b_block) a(b_pair)
```

获得结果如下：

```Stata
..................................................  5000

      command:  areg dayscorab b_treat b_dayscorab miss_b_dayscorab round2 round3, cluster(b_block) a(b_pair)
        _pm_1:  _b[b_treat]/_se[b_treat]
  res. var(s):  b_treat
   Resampling:  Permuting b_treat
Clust. var(s):  b_block
     Clusters:  63
Strata var(s):  b_pair
       Strata:  31

------------------------------------------------------------------------------
T            |     T(obs)       c       n   p=c/n   SE(p) [95% Conf. Interval]
-------------+----------------------------------------------------------------
       _pm_1 |  -2.312006     494    5000  0.0988  0.0042  .0906651   .1074066
------------------------------------------------------------------------------
Note: Confidence interval is with respect to p=c/n.
Note: c = #{|T| >= |T(obs)|}
```

可见，随机推断确实会产生很大的不同：将 *p* 值从 0.024 增加到 0.0988（注：原作者在此处得到的结果为 0.106，原因同上） 。因此，如果要进行聚类随机化，则使用随机推断特别重要。

## 2.3 注意事项

一些问题及思考：

1. 应该重复多少次？默认值为 100 次，但在许多情况下，这可能太低了。Alwyn Young 使用 10,000 次重复，但发现一旦超过 2000 次，得出的结果区别就很小。
3. 花费的时间：在本文例子的简单情况下，即从数据集中删除了所有其他变量后，使用笔记本电脑运行 5000 次大约需要 3 分钟。如果使用完整的数据集，时间将会大大加长。如果要加速运行，请从数据集中删除回归所需的所有其他数据。
4. 该命令不允许在回归中使用 `xi` 。因此，如果回归已使用 `xi`，请删除它。Jason Kerwin 指出，可以只使用例如 `i.groupvariable` 来添加组变量固定效果，而无需使用 `xi`。
4. 关于更复杂的随机过程，例如两级聚类随机。以原作者的[肯尼亚商业培训论文](https://drive.google.com/open?id=0B9C9RwWKZrUNdzhGSm81UDFCMG8)为例，包含双重随机化：首先在市场分层层面，在每个分层内，将市场随机分为实验组或对照组，然后在实验组市场内的每个分层内，将个体公司随机分配是否接受培训。这时就需要用到 2.1.2 节的后两种方法，通过调用自行编写的指定随机分配方式的程序，或使用自行创建的包括重新分配方式的文件来处理此问题。
5. 关于多重实验组。标准教科书中关于随机推断的解释通常只涉及一个二进制实验，一个单元不是属于实验组就是属于控制组。在严格零假设下，即所有实验效果均为零，那么无论将其分配给实验组还是对照组，每个单元的结果都应相同，因此我们可以仅估算所有可能的随机排列下的实验效果，并计算有多少种排列得到的系数至少与实际数据所得的系数一样大。但例如原作者[西非小企业培训论文](http://science.sciencemag.org/cgi/content/full/357/6357/1287?ijkey=yBDf.q3O79Kjs&keytype=ref&siteid=sci)中的实验，将公司分配给对照组（T = 0），传统商业培训（T = 1）或个人主动性培训（T = 2）。则我们至少要检验三个假设：传统培训没有效果，个人主动性培训没有效果，任何一种培训都没有效果。这可以通过更新版本的 `fixlevels()` 选项来达成，详情可参考 Simon Heß 在 GitHub 上关于 `ritest` 的[页面](https://github.com/simonheb/ritest)。

## 参考文献

[1] Mckenzie, David. Finally, a way to do easy randomization inference in Stata. October 02, 2017: [https://blogs.worldbank.org/impactevaluations/finally-way-do-easy-randomization-inference-stata](https://blogs.worldbank.org/impactevaluations/finally-way-do-easy-randomization-inference-stata)

[2] Kerwin, Jason. Randomization inference vs. bootstrapping for p-values. September 05, 2017: [https://jasonkerwin.com/nonparibus/2017/09/25/randomization-inference-vs-bootstrapping-p-values/](https://jasonkerwin.com/nonparibus/2017/09/25/randomization-inference-vs-bootstrapping-p-values/)

[3] Heß S. Randomization inference with Stata: A guide and software[J]. The Stata Journal, 2017, 17(3): 630-651. [[PDF]](https://sci-hub.tw/10.1177/1536867X1701700306)

[4] The World Bank. Randomization Inference. Retrieved on April 02, 2020: [https://dimewiki.worldbank.org/wiki/Randomization_Inference](https://dimewiki.worldbank.org/wiki/Randomization_Inference)