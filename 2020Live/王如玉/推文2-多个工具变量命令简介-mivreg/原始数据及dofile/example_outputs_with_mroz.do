// 11.03.19
// Reproduces the results and outputs presented in Section 7, "Example with real data"

clear
sjlog using mivreg1, replace
use http://www.stata.com/data/jwooldridge/eacsap/mroz.dta
// generating cross-products
unab vars:  nwifeinc educ age kidslt6 kidsge6 exper expersq fatheduc motheduc hushrs husage huseduc mtr // drop last 4, if in doubts
local nvar: word count `vars'
forval i = 1/`nvar' {
  forval j = 1/`=`i'-1' {
    local x : word `i' of `vars'
    local y : word `j' of `vars'
    generate `x'X`y' = `x' * `y'
  }
}
generate one = 1
mivreg hours nwifeinc educ age kidslt6 kidsge6 one (lwage = nwifeinc educ ///
age kidslt6 kidsge6 one exper expersq fatheduc motheduc hushrs husage ///
huseduc mtr *X*) if inlf==1 , hom
sjlog close, replace

sjlog using mivreg2, replace
mivreg hours nwifeinc educ age kidslt6 kidsge6 one (lwage = nwifeinc educ ///
age kidslt6 kidsge6 one exper expersq fatheduc motheduc hushrs husage ///
huseduc mtr *X*) if inlf==1 , hom robust fuller
sjlog close, replace

sjlog using mivreg3, replace
mivreg hours nwifeinc educ age kidslt6 kidsge6 one (lwage = nwifeinc educ ///
age kidslt6 kidsge6 one exper expersq fatheduc motheduc hushrs husage ///
huseduc mtr *X*) if inlf==1 , het robust fuller
sjlog close, replace

