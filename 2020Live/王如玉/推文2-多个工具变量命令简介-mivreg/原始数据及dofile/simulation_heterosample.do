// 11.03.19
// Reproduces simulations presented in Section 6, "Simulations" (heterosk. case)

/* Easiest way to run to get x simulations: set launch=0, portion =x/10, and run once (i.e. all simulations in one portion).

To do simulations portion by portion: set portion = smth smaller (say, 200) and launch=0, then run rows 11-171 repeatedly, and increase launch by +1 every time. After you run the code with launch=4, you will have 10000 simulations in total, for example.

*/

//NB: in heteroscedastic case, I will use robust under ivregress


clear
set trace off
set matsize 10000

local portion = 1000 // vary this parameter (how much iterations in one step)
local launch = 0 // and this one (how many steps to do)

local i_step = 9 
local i_start = `launch' * (`i_step' + 1) + 1

forvalues i = `i_start'/`=`i_start' + `i_step'' {
	clear mata
	//clear matrix
	//set maxvar 5000
	
	scalar l = 30
	set obs 400

	matrix repl = J(`portion', 7, 0)
	matrix pval_x = J(`portion', 9, 0)
	matrix pval_x_c = J(`portion', 9, 0)
	matrix pval_spec = J(`portion', 9, 0)

	qui gen z1 = .
	qui gen u2 = .
	qui gen v1 = .
	qui gen v2 = .
	qui gen ee = .
	qui gen x = .
	qui gen y = .
	qui gen one = 1

	di `i'
	local start_seed = `portion' * (`i' - 1) + 1
	local end_seed = `i' * `portion'

	forvalues k = `start_seed'/`end_seed' {
		set seed `k'

		scalar phi = 0.8
		scalar gam = sqrt(32/_N)
		tempname beta10 beta20
		scalar `beta10' = 1
		scalar `beta20' = 1

		qui replace z1 = rnormal()
		qui replace u2 = rnormal()

		qui replace v1 = z1 * rnormal()       // hetero
			//v1 = rndns(N,1,seed);           // homo
		qui replace v2 = 0.86 * rnormal()

		qui replace ee = 0.30 * u2 + sqrt((1 - 0.30^2) / (phi^2 + 0.86^4)) * (phi * v1 + 0.86 * v2)

		qui replace x = gam * z1 + u2
		qui replace y = `beta10' + `beta20' * x + ee

		forvalues t = 2/4 {
			if mod(`k', `portion') == 1 & mod(`i', `=`i_step' + 1') == 1 {
				gen z`t' = z1^`t' 
			}
			else {
				qui replace z`t' = z1^`t' // HERE IS a PROBLEM, FIX
			}
		}

		forvalues t = 5/29 {
			if mod(`k', `portion') == 1 & mod(`i', `=`i_step' + 1') == 1 {
				gen d`t' = round(runiform())
				gen z`t' = d`t' * z1
			}
			else {
				qui replace d`t' = round(runiform())
				qui replace z`t' = d`t' * z1
			}
		
		}

		qui ivregress 2sls y one (x = z*), nocons robust // 2SLS
		matrix beta = e(b)
		matrix repl[`=`k' - (`i' - 1)*`portion'' , 1] = beta[1,1]
		qui estat overid
		matrix pval_spec[`=`k' - (`i' - 1)*`portion'' , 1] = r(p_basmann) // p-value for Basmann χ2statistic
		qui test x = 1
		matrix pval_x[`=`k' - (`i' - 1)*`portion'', 1] =r(p) 
		qui test x = one = 1
		matrix pval_x_c[`=`k' - (`i' - 1)*`portion'', 1] = r(p)
		
		qui mivreg y one (x = z*), hom // non-robust LIML
		matrix beta = e(b)
		matrix repl[`=`k' - (`i' - 1)*`portion'' , 2] = beta[1,1]
		matrix pval_spec[`=`k' - (`i' - 1)*`portion'' , 2] = e(jpv) 
		qui test x = 1
		matrix pval_x[`=`k' - (`i' - 1)*`portion'', 2] = r(p)
		qui test x = one = 1
		matrix pval_x_c[`=`k' - (`i' - 1)*`portion'', 2] = r(p)
		
		qui mivreg y one (x = z*), hom fuller // non-robust FULL
		matrix beta = e(b)
		matrix repl[`=`k' - (`i' - 1)*`portion'' , 3] = beta[1,1]
		matrix pval_spec[`=`k' - (`i' - 1)*`portion'' , 3] = e(jpv) 
		qui test x = 1
		matrix pval_x[`=`k' - (`i' - 1)*`portion'', 3] = r(p)
		qui test x = one = 1
		matrix pval_x_c[`=`k' - (`i' - 1)*`portion'', 3] = r(p)
		
		qui mivreg y one (x = z*), hom robust // robust LIML
		matrix pval_spec[`=`k' - (`i' - 1)*`portion'' , 4] = e(jpv) 
		qui test x = 1
		matrix pval_x[`=`k' - (`i' - 1)*`portion'', 4] = r(p)
		qui test x = one = 1
		matrix pval_x_c[`=`k' - (`i' - 1)*`portion'', 4] = r(p)
		
		qui mivreg y one (x = z*), hom robust fuller // robust FULL
		matrix pval_spec[`=`k' - (`i' - 1)*`portion'' , 5] = e(jpv) 
		qui test x = 1
		matrix pval_x[`=`k' - (`i' - 1)*`portion'', 5] = r(p)
		qui test x = one = 1
		matrix pval_x_c[`=`k' - (`i' - 1)*`portion'', 5] = r(p)
		
		qui mivreg y one (x = z*), het robust // HLIM
		matrix beta = e(b)
		matrix repl[`=`k' - (`i' - 1)*`portion'' , 4] = beta[1,1]
		matrix pval_spec[`=`k' - (`i' - 1)*`portion'' , 6] = e(jpv) 
		qui test x = 1
		matrix pval_x[`=`k' - (`i' - 1)*`portion'', 6] = r(p)
		qui test x = one = 1
		matrix pval_x_c[`=`k' - (`i' - 1)*`portion'', 6] = r(p)
		
		qui mivreg y one (x = z*), het robust fuller // HFUL
		matrix beta = e(b)
		matrix repl[`=`k' - (`i' - 1)*`portion'' , 5] = beta[1,1]
		matrix pval_spec[`=`k' - (`i' - 1)*`portion'' , 7] = e(jpv) 
		qui test x = 1
		matrix pval_x[`=`k' - (`i' - 1)*`portion'', 7] = r(p)
		qui test x = one = 1
		matrix pval_x_c[`=`k' - (`i' - 1)*`portion'', 7] = r(p)
		
		qui ivregress liml y one (x = z*), nocons robust // LIML
		matrix beta = e(b)
		matrix repl[`=`k' - (`i' - 1)*`portion'' , 6] = beta[1,1]
		//qui estat overid
		//matrix pval_spec[`=`k' - (`i' - 1)*`portion'' , 8] = r(p_basmann) // p-value for score Chi2 stat
		qui test x = 1
		matrix pval_x[`=`k' - (`i' - 1)*`portion'', 8] =r(p) 
		qui test x = one = 1
		matrix pval_x_c[`=`k' - (`i' - 1)*`portion'', 8] = r(p)
		
		
		qui ivregress gmm y one (x = z*), nocons robust // GMM
		matrix beta = e(b)
		matrix repl[`=`k' - (`i' - 1)*`portion'' , 7] = beta[1,1]
		qui estat overid
		matrix pval_spec[`=`k' - (`i' - 1)*`portion'' , 9] = r(p_HansenJ) // p-value for score Chi2 stat
		qui test x = 1
		matrix pval_x[`=`k' - (`i' - 1)*`portion'', 9] =r(p) 
		qui test x = one = 1
		matrix pval_x_c[`=`k' - (`i' - 1)*`portion'', 9] = r(p)
		
		
	
		if mod(`k',10) == 0 {
		di `k'
		}
	}

	//drop d*
	
	
	
	
	if `i' == 1 {
		matrix many_repl = repl
		matrix many_pval_x = pval_x
		matrix many_pval_x_c = pval_x_c
		matrix many_pval_spec = pval_spec
	}
	else {
		matrix many_repl = many_repl \ repl
		matrix many_pval_x = many_pval_x \ pval_x
		matrix many_pval_x_c = many_pval_x_c \ pval_x_c
		matrix many_pval_spec = many_pval_spec \ pval_spec
	}
	
	drop z1 u2 v1 v2 ee x y one
}


svmat many_repl, name(est_vec)
rename est_vec1 est_TSLS
rename est_vec2 est_LIML
rename est_vec3 est_FULL
rename est_vec4 est_HLIM
rename est_vec5 est_HFUL
rename est_vec6 est_LIML_ivreg
rename est_vec7 est_GMM

svmat many_pval_x, name(test_x_pvalue)
rename test_x_pvalue1 test_x_pvalue_2SLS //1
rename test_x_pvalue2 test_x_pvalue_nonrob_LIML //2
rename test_x_pvalue3 test_x_pvalue_nonrob_FULL //3
rename test_x_pvalue4 test_x_pvalue_rob_LIML //4
rename test_x_pvalue5 test_x_pvalue_rob_FULL //5
rename test_x_pvalue6 test_x_pvalue_HLIM //6
rename test_x_pvalue7 test_x_pvalue_HFUL //7
rename test_x_pvalue8 test_x_pvalue_rob_LIML_ivreg //8
rename test_x_pvalue9 test_x_pvalue_GMM //9

svmat many_pval_x_c, name(test_x_c_pvalue)
rename test_x_c_pvalue1 test_x_c_pvalue_2SLS // 1
rename test_x_c_pvalue2 test_x_c_pvalue_nonrob_LIML // 2
rename test_x_c_pvalue3 test_x_c_pvalue_nonrob_FULL // 3 
rename test_x_c_pvalue4 test_x_c_pvalue_rob_LIML //4
rename test_x_c_pvalue5 test_x_c_pvalue_rob_FULL //5
rename test_x_c_pvalue6 test_x_c_pvalue_HLIM //6
rename test_x_c_pvalue7 test_x_c_pvalue_HFUL //7
rename test_x_c_pvalue8 test_x_c_pvalue_rob_LIML_ivreg //8
rename test_x_c_pvalue9 test_x_c_pvalue_GMM //8

svmat many_pval_spec, name(spec_test_p_value)
rename spec_test_p_value1 spec_test_p_value_2SLS
rename spec_test_p_value2 spec_test_p_value_nonrob_LIML // 2
rename spec_test_p_value3 spec_test_p_value_nonrob_FULL //3
rename spec_test_p_value4 spec_test_p_value_rob_LIML // 4
rename spec_test_p_value5 spec_test_p_value_rob_FULL // 5
rename spec_test_p_value6 spec_test_p_value_HLIM //6
rename spec_test_p_value7 spec_test_p_value_HFUL // 7
rename spec_test_p_value8 spec_test_p_value_rob_LIML_ivreg // 8
rename spec_test_p_value9 spec_test_p_value_GMM // 9

// Produce summary of the results
summ est_TSLS est_LIML est_FULL est_HLIM est_HFUL est_LIML_ivreg est_GMM, d

unab vars1 : test_x_pvalue*
local nvar1 : word count `vars1'
forval i = 1/`nvar1' {
	local x : word `i' of `vars1'
	qui count if `x' < 0.05
	di round(r(N)/10000, 0.001)
 
}


unab vars2 :  test_x_c_*
local nvar2 : word count `vars2'
forval i = 1/`nvar2' {
	local x : word `i' of `vars2'
	qui count if `x' < 0.05
	di round(r(N)/10000, 0.001)
 
}

unab vars3 :  spec_test_p_value_*
local nvar3 : word count `vars3'
forval i = 1/`nvar3' {
	local x : word `i' of `vars3'
	qui count if `x' < 0.05
	di round(r(N)/10000, 0.001)
 
}
