> **作者：** 王如玉（中国人民银行金融研究所）
>
> **E-mail:** 15242968@qq.com
>
> **Source:** 
> Stanislav Anatolyev, Alena Skolkova, 2019, Many instruments: Implementation in Stata, Stata Journal, 19(4): 849–866. [[PDF\]](https://sci-hub.tw/10.1177/1536867X19893627), [[Link\]](https://www.stata-journal.com/article.html?article=st0580)

# 多个工具变量命令简介-mivreg

## 1 多个工具变量回归简介及模型

计量经济学中，工具变量的使用是很常见的。在回归模型中，如果存在内生性问题，即解释变量和误差项存在相关性，则可以使用工具变量法来解决。找到一个仅和内生的解释变量 $x$ 相关，而和残差项 $u$ 不相关的变量 $z$ ，就是工具变量。

当工具变量和内生变量的相关性较弱时，则工具变量对内生变量的外生变动部分解释能力很小，则我们称其为弱工具变量。使用弱工具变量会导致使用 2SLS 的估计值产生较严重的偏差，且会偏向 OLS 的估计结果。一个典型的弱工具变量的例子是，考察教育收益率时，出生季度是一个弱工具变量，由于美国年满16岁之前禁止退学，出生在下半年的学生比出生在上半年的学生更晚一年才能退学，因此有更长的平均受教育年限，但出生季度对受教育年限的影响虽然存在却很小，只有微弱的相关性 ( Angrist and Kruege, 1991 ) 。

传统的工具变量法常常使用二阶段最小二乘法 ( 2SLS ) 。但随着理论的发展，人们发现 2SLS 在一些复杂情形下无法得出一致的估计，包括弱工具变量、大量工具变量等，于是提出了新的估计方法，例如有限信息最大似然法 ( LIML )，偏差校正二阶段最小二乘法 ( bias-corrected 2SLS ) 等。后来者又对LIML方法进行了延展和改进，并提出了解释变量为同方差（回归模型中，误差项的方差为常数）和异方差（回归模型中，误差项的方差非常数）不同情况下的模型。 

但实践中，Stata 尚无合适的应用包，直到 `mivreg` 的发布。`mivreg` 命令能对多个（可以是弱的）线性工具变量进行一致估计。

## 1.1 2SLS 方法简介

2SLS 方法分为两个阶段回归。

(1) 第一阶段回归：用内生解释变量 $x_i$ 对工具变量 $z_i$ 进行 OLS 回归：

$$
x_i=\pi_0+\pi_1z_i+u_i
$$

此回归的拟合值为 $\hat{x}_i$ ，其相应的残差值为 $\hat{u}_i=x_i-\hat{x}_i$ 。从而分离出了 $x_i$ 的外生部分 $\hat{x}_i$，即与扰动项不相关的那部分。

(2) 第二阶段回归：用被解释变量 $y_i$ 对第一阶段回归的拟合值 $\hat{x}_i$ 进行 OLS 回归：

$$
y_i=\beta_0+\beta_1\hat{x}_i+(\epsilon_i+\beta_1\hat{u}_i)
$$

此处 $\hat{x}_i$ 与扰动项 $(\epsilon_i+\beta_1\hat{u}_i)$ 不相关，故 2SLS 得到的结果是一致的。

## 1.2 LIML 方法简介

假定回归方程如下：

$$
y_i=x_i^{'}\beta_0+e_i
$$

其中$\beta_0$为 *k* × 1 阶向量。第一阶段回归式为：

$$
x_i=z_i^{'}\Gamma+u_i
$$

其中 $z_i$ 为 *l* × 1 阶向量，$\Gamma$ 为第一阶段系数的 *l × k* 阶矩阵。则残差表示如下：

$$
\left(\begin{array}{l}e_{i} \\ \mathbf{u}_{i}\end{array}\right) | \mathbf{z}_{i} \sim \mathcal{D}\left(\left(\begin{array}{l}0 \\ 0\end{array}\right),\left(\begin{array}{ll}\sigma_{i}^{2} & \Psi_{i}^{\prime} \\ \Psi_{i} & \Omega_{i}\end{array}\right)\right)
$$

其中 $\mathcal{D}$ 代表某种分布，它可以是正态分布。

### 1.2.1 同方差的情况

* 参数估计值

$\beta$ 的估计值为：

$$
\widehat{\boldsymbol{\beta}}_{\mathrm{LIML}}=\arg \min _{\beta} \frac{(\mathbf{Y}-\mathbf{X} \boldsymbol{\beta})^{\prime} \mathbf{P}(\mathbf{Y}-\mathbf{X} \boldsymbol{\beta})}{(\mathbf{Y}-\mathbf{X} \boldsymbol{\beta})^{\prime}(\mathbf{Y}-\mathbf{X} \boldsymbol{\beta})}
$$

其中，$\mathbf{P}$ 为工具变量的投影矩阵，$\mathbf{P}=\mathbf{Z}\left(\mathbf{Z}^{\prime} \mathbf{Z}\right)^{-1} \mathbf{Z}^{\prime}$ .

数学上，可以等价为解以下问题：
$$
\widehat{\beta}_{\mathrm{LIML}}=\overline{\mathbf{H}}^{-1} \mathbf{X}^{\prime} \hat{\mathbf{P}}\mathbf{Y}
$$
其中$\overline{\mathbf{H}}=\mathbf{X}^{\prime} \hat{\mathbf{P}} \mathbf{X}$ , 且 $\hat{\mathbf{P}}=\mathbf{P}-\bar{\alpha} \mathbf{I}_{n}$ , $\overline{\alpha}$ 为矩阵$\left(\hat{\mathbf{X}}^{\prime} \hat{\mathbf{X}}\right)^{-1} \hat{\mathbf{X}}^{\prime} \mathbf{P} \hat{\mathbf{X}}$的最小特征值，此处$\hat{\mathbf{X}}=(\mathbf{Y}, \mathbf{X})$。

但 LIML 估计值有一个缺陷，它的低阶炬也是不存在的。Fuller ( 1977 ) 的校准解决了这个问题，如下式定义 $\widetilde{\alpha}$ （通常令 $\zeta=1$ ），并将所有 $\overline{\alpha}$ 替换成 $\widetilde{\alpha}$。校准后的估计值称为 FULL 估计值。

$$
\widetilde{\alpha}=\frac{\bar{\alpha}-(1-\bar{\alpha}) \zeta / n}{1-(1-\bar{\alpha}) \zeta / n}
$$

令 LIML 或 FULL 的残差矢量为 $\hat{e}$ ，则残差的方差为：
$$
\widehat{\sigma}^{2}=\frac{\widehat{\mathbf{e}}^{\prime} \widehat{\mathbf{e}}}{n-k}
$$

* 特征检验

传统的 $J$ 检验如下：
$$
J=\frac{\widehat{\mathbf{e}}^{\prime} \mathbf{P} \widehat{\mathbf{e}}}{\widehat{\sigma}^{2}}=(n-k) \bar{\alpha}
$$
偏误校正的 $J$ 检验如下，其下标 R 代表稳健：
$$
J_{\mathrm{R}}=J-\frac{\ell}{n} \frac{\widehat{\mathrm{e}}^{\prime} \widehat{\mathrm{e}}}{\widehat{\sigma}^{2}}=(n-k)\left(\bar{\alpha}-\frac{\ell}{n}\right)
$$

在误差正态性或 $\mathbf{P}$ 为渐进常对角矩阵的情形下， Anatolyev and Gospodinov (2011) 的检验规定，当 $J$ 检验值超过 $q_{\phi^*}^{\chi^{2}(\ell-k)}$，即 $\chi^{2}(\ell-k)$ 分布的第 $(1-\phi^*)$ 分位数，则拒绝显著性水平为 $\phi$ 的正确的模型设定。

在误差非正态性以及 $\mathbf{P}$ 为渐进非常对角矩阵的情形下，Lee and Okui (2012) 的检验规定，当 $\frac{J_{\mathrm{R}}}{\sqrt{n \widehat{V}^{J}}}$ 的值超过 $q_{\phi}^{\mathcal{N}(0,1)}$，即标准正态分布的第 $(1-\phi)$ 分位数时，拒绝显著性水平为 $\phi$ 的正确的模型设定。


### 1.2.2 异方差的情况

* 参数估计值

$\beta$ 的估计值为：

$$
\widehat{\beta}_{\mathrm{HLIM}}=\arg \min _{\beta} \frac{(\mathbf{Y}-\mathbf{X} \beta)^{\prime}(\mathbf{P}-\mathbf{D})(\mathbf{Y}-\mathbf{X} \beta)}{(\mathbf{Y}-\mathbf{X} \beta)^{\prime}(\mathbf{Y}-\mathbf{X} \beta)}
$$

同上，Fuller ( 1977 ) 对异方差稳健的 LIML 估计值，即 HLIM 估计值，进行了校准，校准后的估计值称为 HFUL 。 

* 特征检验

Chao et al. (2014) 将 $J$ 检验类推至异方差的情形。他们的检验基于对 $J$ 检验的平方形式的刀切法 ( jackknife ) 改进：
$$
J=\frac{\widehat{\mathbf{e}}^{\prime}(\mathbf{P}-\mathbf{D}) \widehat{\mathbf{e}}}{\sqrt{\widehat{V}^{J}}}+\ell
$$
其中 $\widehat{V}^{J}$ 是该平方形式改进版的方差。

这个改进版的 $J$ 检验是单边的，其判定规则是，如果 $J$ 检验值超过 $q_{\phi}^{\chi^{2}(\ell-k)}$，即 $\chi^{2}(\ell-k)$ 分布的第 $(1-\phi)$ 分位数，则拒绝零假设。

## 2 mivreg命令

在 Stata 命令窗口中输入如下命令，即可安装 `mivreg` 命令，并获取下文提到的案例的完整 dofile 。

```Stata
net describe st0580, from(http://www.stata-journal.com/software/sj19-4)
net install st0580
net get st0580
```

`mivreg` 命令的语句和参数简单介绍如下：

```Stata
mivreg depvar [indepvars] (varlist1 = varlist2) [if] [in] [, hom het robust fuller level(#)]
```

* `depvar` 为被解释变量，`indepvars` 为解释变量，`varlist1` 为内生变量，`varlist2` 为工具变量。
* `hom` 为同方差的情况，使用 LIML（默认）或 FULL（与`fuller`选项同时使用）估计值。
* `het` 为异方差的情况，使用 HLIM（默认）或 HFUL（与`fuller`选项同时使用）估计值。
* `robust` 默认为非稳健方差。
* `fuller` 为 Fuller 校正。使用后，将得到 FULL（与 `hom` 选项同时使用）或 HFUL（与 `het` 选项同时使用）估计值。
* `level(#)` 为置信区间设置，默认为 95%。

## 3 模拟运行案例

## 3.1 数据

本节将用 Hausman et al. (2012) 文中两个人工生成的模拟数据集展示 `mivreg` 的使用。估计方程为：

$$
y=\beta_{1}+\beta_{2} x_{2}+e
$$

第一阶段方程为

$$
x_{2}=\gamma z_{1}+u_{2}
$$
其中 $z_{1} \sim N(0,1)$ , $z_{2} \sim N(0,1)$ 。工具变量为：

$$
\mathbf{z}=\left(1, z_{1}, z_{1}^{2}, z_{1}^{3}, z_{1}^{4}, z_{1} d_{1}, \ldots, z_{1} d_{\ell-5}\right)^{\prime}
$$
其中 $d_{j} \in\{0,1\}$ , $\operatorname{Pr}\left\{d_{j}=1\right\}=\frac{1}{2}$ 并独立于 $z_1$ 。其结构扰动为：

$$
e=0.30 u_{2}+\sqrt{\frac{1-0.30^{2}}{\phi^{2}+0.86^{4}}}\left(\phi v_{1}+0.86 v_{2}\right)
$$

其中同方差的情况下 $v_{1} \sim N(0,1)$ ，异方差的情况下 $v_{1} \sim N\left(0, z_{1}^{2}\right)$ ，而 $v_{2} \sim N\left(0,0.86^{2}\right)$ 。$v_1$ 和 $v_2$ 均独立于 $u_2$ 。样本大小为 400，生成 30 个工具变量。工具变量的强度设置为，令工具变量的集中度参数为 $\gamma^2$ ，则设 $n\gamma^2 = 32$。参数 $\phi$ 设为 0.8 ，这相当于方差回归中的 $R^{2} \approx 0.25$。$\beta_1$ 和 $\beta_2$ 的真实值设定为 1 。

注：集中度 ( concentration parameter) 是衡量工具变量强度的一种参数。设模型为 $y_t=\beta x_t+e_t$ , $x_t=\mathbf{Z}_t\pi+u_t$，则集中度定义为 $\gamma^{2}=\pi^{\prime}\mathbf{Z}^{\prime} \mathbf{Z} \pi / \sigma_{v}^{2}$ .

## 3.2 蒙特卡洛模拟

蒙特卡洛模拟是使用计算机进行重复的随机抽样来产生模拟概率分布的数据。本文使用的 Stata 的完整模拟程序见前文提及的 dofile 。例如，同方差情形下的蒙特卡洛模拟，以及使用 `mivreg` 与 `ivregress` 命令以不同方法对模拟数据进行回归的命令如下：

```Stata
clear
set trace off
set matsize 10000

local portion = 1000 // 此参数表示一个步骤进行多少次重复迭代，如果想进行x次模拟，则在此输入x/10
local launch = 0 // 此参数表示进行多少步骤，建议设为0

local i_step = 9 
local i_start = `launch' * (`i_step' + 1) + 1

forvalues i = `i_start'/`=`i_start' + `i_step'' {
	clear mata
	
	scalar l = 30
	set obs 400

	matrix repl = J(`portion', 7, 0)
	matrix pval_x = J(`portion', 9, 0)
	matrix pval_x_c = J(`portion', 9, 0)
	matrix pval_spec = J(`portion', 9, 0)

	qui gen z1 = .
	qui gen u2 = .
	qui gen v1 = .
	qui gen v2 = .
	qui gen ee = .
	qui gen x = .
	qui gen y = .
	qui gen one = 1

	di `i'
	local start_seed = `portion' * (`i' - 1) + 1
	local end_seed = `i' * `portion'

	forvalues k = `start_seed'/`end_seed' {
		set seed `k'

		scalar phi = 0.8
		scalar gam = sqrt(32/_N)
		tempname beta10 beta20
		scalar `beta10' = 1
		scalar `beta20' = 1

		qui replace z1 = rnormal()
		qui replace u2 = rnormal()
          
		qui replace v1 = rnormal()
		qui replace v2 = 0.86 * rnormal()

		qui replace ee = 0.30 * u2 + sqrt((1 - 0.30^2) / (phi^2 + 0.86^4)) * (phi * v1 + 0.86 * v2)

		qui replace x = gam * z1 + u2
		qui replace y = `beta10' + `beta20' * x + ee

		forvalues t = 2/4 {
			if mod(`k', `portion') == 1 & mod(`i', `=`i_step' + 1') == 1 {
				gen z`t' = z1^`t' 
			}
			else {
				qui replace z`t' = z1^`t'
			}
		}

		forvalues t = 5/29 {
			if mod(`k', `portion') == 1 & mod(`i', `=`i_step' + 1') == 1 {
				gen d`t' = round(runiform())
				gen z`t' = d`t' * z1
			}
			else {
				qui replace d`t' = round(runiform())
				qui replace z`t' = d`t' * z1
			}	
		}

		qui ivregress 2sls y one (x = z*), nocons // 2SLS
		matrix beta = e(b)
		matrix repl[`=`k' - (`i' - 1)*`portion'' , 1] = beta[1,1]
		qui estat overid
		matrix pval_spec[`=`k' - (`i' - 1)*`portion'' , 1] = r(p_basmann) // p-value for Basmann χ2 statistic
		qui test x = 1
		matrix pval_x[`=`k' - (`i' - 1)*`portion'', 1] =r(p) 
		qui test x = one = 1
		matrix pval_x_c[`=`k' - (`i' - 1)*`portion'', 1] = r(p)
		
		qui mivreg y one (x = z*), hom // non-robust LIML
		matrix beta = e(b)
		matrix repl[`=`k' - (`i' - 1)*`portion'' , 2] = beta[1,1]
		matrix pval_spec[`=`k' - (`i' - 1)*`portion'' , 2] = e(jpv) 
		qui test x = 1
		matrix pval_x[`=`k' - (`i' - 1)*`portion'', 2] = r(p)
		qui test x = one = 1
		matrix pval_x_c[`=`k' - (`i' - 1)*`portion'', 2] = r(p)
		
		qui mivreg y one (x = z*), hom fuller // non-robust FULL
		matrix beta = e(b)
		matrix repl[`=`k' - (`i' - 1)*`portion'' , 3] = beta[1,1]
		matrix pval_spec[`=`k' - (`i' - 1)*`portion'' , 3] = e(jpv) 
		qui test x = 1
		matrix pval_x[`=`k' - (`i' - 1)*`portion'', 3] = r(p)
		qui test x = one = 1
		matrix pval_x_c[`=`k' - (`i' - 1)*`portion'', 3] = r(p)
		
		qui mivreg y one (x = z*), hom robust // robust LIML
		matrix pval_spec[`=`k' - (`i' - 1)*`portion'' , 4] = e(jpv) 
		qui test x = 1
		matrix pval_x[`=`k' - (`i' - 1)*`portion'', 4] = r(p)
		qui test x = one = 1
		matrix pval_x_c[`=`k' - (`i' - 1)*`portion'', 4] = r(p)
		
		qui mivreg y one (x = z*), hom robust fuller // robust FULL
		matrix pval_spec[`=`k' - (`i' - 1)*`portion'' , 5] = e(jpv) 
		qui test x = 1
		matrix pval_x[`=`k' - (`i' - 1)*`portion'', 5] = r(p)
		qui test x = one = 1
		matrix pval_x_c[`=`k' - (`i' - 1)*`portion'', 5] = r(p)
		
		qui mivreg y one (x = z*), het robust // HLIM
		matrix beta = e(b)
		matrix repl[`=`k' - (`i' - 1)*`portion'' , 4] = beta[1,1]
		matrix pval_spec[`=`k' - (`i' - 1)*`portion'' , 6] = e(jpv) 
		qui test x = 1
		matrix pval_x[`=`k' - (`i' - 1)*`portion'', 6] = r(p)
		qui test x = one = 1
		matrix pval_x_c[`=`k' - (`i' - 1)*`portion'', 6] = r(p)
		
		qui mivreg y one (x = z*), het robust fuller // HFUL
		matrix beta = e(b)
		matrix repl[`=`k' - (`i' - 1)*`portion'' , 5] = beta[1,1]
		matrix pval_spec[`=`k' - (`i' - 1)*`portion'' , 7] = e(jpv) 
		qui test x = 1
		matrix pval_x[`=`k' - (`i' - 1)*`portion'', 7] = r(p)
		qui test x = one = 1
		matrix pval_x_c[`=`k' - (`i' - 1)*`portion'', 7] = r(p)
		
		qui ivregress liml y one (x = z*), nocons // LIML
		matrix beta = e(b)
		matrix repl[`=`k' - (`i' - 1)*`portion'' , 6] = beta[1,1]
		qui estat overid
		matrix pval_spec[`=`k' - (`i' - 1)*`portion'' , 8] = r(p_basmann)
		qui test x = 1
		matrix pval_x[`=`k' - (`i' - 1)*`portion'', 8] =r(p) 
		qui test x = one = 1
		matrix pval_x_c[`=`k' - (`i' - 1)*`portion'', 8] = r(p)
		
		qui ivregress gmm y one (x = z*), nocons // GMM
		matrix beta = e(b)
		matrix repl[`=`k' - (`i' - 1)*`portion'' , 7] = beta[1,1]
		qui estat overid
		matrix pval_spec[`=`k' - (`i' - 1)*`portion'' , 9] = r(p_HansenJ)
		qui test x = 1
		matrix pval_x[`=`k' - (`i' - 1)*`portion'', 9] =r(p) 
		qui test x = one = 1
		matrix pval_x_c[`=`k' - (`i' - 1)*`portion'', 9] = r(p)
					
		if mod(`k',10) == 0 {
		di `k'
		}
	}

		
	if `i' == 1 {
		matrix many_repl = repl
		matrix many_pval_x = pval_x
		matrix many_pval_x_c = pval_x_c
		matrix many_pval_spec = pval_spec
	}
	else {
		matrix many_repl = many_repl \ repl
		matrix many_pval_x = many_pval_x \ pval_x
		matrix many_pval_x_c = many_pval_x_c \ pval_x_c
		matrix many_pval_spec = many_pval_spec \ pval_spec
	}
	
	drop z1 u2 v1 v2 ee x y one
}
```

## 3.3 运行结果比较

本节将比较在同方差、异方差情形下各进行 10,000 次回归的结果。

首先来看参数估计值。表 1 统计了 `ivregress` 计算的 2SLS , LIML 和 GMM 的估计值的模拟分布百分比，以及 `mivreg` 计算的 LIML , FULL , HLIM 和 HFUL 估计值的模拟分布百分比。当然，计算出的 LIML 值是相等的。

![](https://images.gitee.com/uploads/images/2020/0614/222729_b83ba660_7361456.png)

```Stata
						表1   各回归模拟分布百分比比较
--------------------------------------------------------------------------------
Estimator |        Homoskedastic case        |      Heteroskedastic case
--------------------------------------------------------------------------------
          | 5%   | 25%  | 50%  | 75%  | 95%  | 5%   | 25%  | 50%  | 75%  | 95%
--------------------------------------------------------------------------------	                                command ivregress									
--------------------------------------------------------------------------------
2SLS      | 0.93 | 1.06 | 1.14 | 1.23 | 1.35 | 0.85 | 1.02 | 1.14 | 1.26 | 1.43 
GMM       | 0.91 | 1.05 | 1.14 | 1.23 | 1.37 | 0.85 | 1.02 | 1.14 | 1.26 | 1.42 
LIML      | 0.47 | 0.83 | 1.00 | 1.16 | 1.42 |-4.08 |-0.27 | 0.49 | 1.07 | 4.48 
--------------------------------------------------------------------------------
                               command mivreg  									
--------------------------------------------------------------------------------
LIML	  | 0.47 | 0.83 | 1.00 | 1.16 | 1.42 |-4.08 |-0.27 | 0.49 | 1.07 | 4.48 
FULL	  | 0.52 | 0.84 | 1.01 | 1.17 | 1.41 |-1.14 |-0.03 | 0.56 | 1.09 | 2.77 
HLIM	  | 0.43 | 0.82 | 1.00 | 1.17 | 1.43 | 0.15 | 0.76 | 1.01 | 1.22 | 1.62 
HFUL	  | 0.52 | 0.84 | 1.01 | 1.17 | 1.43 | 0.30 | 0.79 | 1.02 | 1.22 | 1.60 
```

2SLS 和 GMM 估计值相似，且总是向右偏误的。在同方差的情形下，其他估计值都能得出无偏误的结果。LIML 估计值比 HLIM 估计值略向中心集中，因此有较高的效率。Fuller 校准值都更向中心集中，说明他们更抵制离群值。在异方差的情形下，HLIM 和 HFUL 都是中位数无偏误的。 它们的 Fuller 校准值也更集中且对称。

原文也列出了对估计值的实际拒绝率和规范检验结果的比较，得出的结论是，传统的 2SLS , GMM 和 LIML 的规范检验值存在严重失真，而 `mivreg` 的检验值比较不失真。

表 2 展示了各种检验的实际拒绝率。$t_{\beta_2=1}$ 表示 5% 显著性水平下的双边 *t* 检验，零假设 $H_0 : \beta_2 = 1$ 。$W_{\beta_1=\beta_2=1}$ 表示 零假设 $H_0 : \beta_1=\beta_2 = 1$ 的 Wald 检验。$J_{E(ze)}=0$ 表示 J 检验。`ivregress` 生成的 2SLS 和 LIML 检验有两种形式：非稳健的，和异方差稳健的（这里的稳健实际对大量工具变量并不稳健）。在特征检验中，使用了 Basmann (1957) 方差估计值。 `mivreg` 生成的检验值则使用了如下的估计值和稳健选项：非稳健的 LIML ，非稳健的 FULL ，稳健的 LIML ，稳健的 FULL ， HLIM 和 HFUL .

![](https://images.gitee.com/uploads/images/2020/0614/222729_1edf9fa0_7361456.png)

可以预见的，2SLS , GMM 和 LIML 方法的传统的参数检验产生了较严重的偏误。这个例子的偏误还不是很大，但通常可能产生更大的偏误，详见 Anatolyev and Gospodinov (2011) 。在同方差的情况下，所有的 `mivreg` 检验产生的偏误都显著减小，但“异方差稳健”的版本看来更可靠。在异方差的情况下，理论上只有“异方差稳健”的版本是有效的，且其拒绝率近似于名义拒绝率。Fuller 校准版本没有对拒绝率产生显著影响。而如果我们依赖于“同方差”的特征检验，但实际上同方差假设并不成立，在此情形下特征检验结果也显示了巨大的偏误。因此我们在异方差的情形下不能使用同方差的特征检验，否则就会在工具变量有效的情形下，却得到无效的信号。

## 4 真实数据案例

本节使用经典的已婚妇女劳动力产出的真实数据 ( Mroz 1987 ) 展示。该数据可于 [http://www.stata.com/data/jwooldridge/eacsap/mroz.dta](http://www.stata.com/data/jwooldridge/eacsap/mroz.dta) 下载。本文仅使用了女性劳动力相关的数据。

被解释变量为工作时长 ( **hours** ) ，唯一的内生解释变量为工资的对数 ( **lwage** )，另外有 6 个外生控制变量：**nwifeinc , educ , age , kidslt6 , kidsge6** 以及常数 1 。此外还有 8 个基础工具变量： **exper , expersq , fatheduc , motheduc , hushrs , husage , huseduc** 以及 **mtr**，合计 14 个工具变量。作为一个整体，8 个基础工具变量的强度是高的：第一阶段回归的 F 统计值为 183.5 。为了提升估计的效率，充分利用工具变量的信息，我们还考虑了一套扩展的工具变量——基础工具变量加上所有它们的交互项，因此总共有 92 个工具变量。工具变量数量与样本量之比很大，约为 0.215 。虽然传统工具适用于基础的工具变量集，但扩展的工具变量集显然需要能够处理多个工具变量的系统。
表 2 展示了对工资对数的各模型的估计值，包括 OLS，异方差稳健 2SLS （使用基础和扩展工具变量集），以及 3 个多工具变量稳健的估计方法：LIML , FULL 和 HFUL （使用扩展工具变量集）。

```Stata
                 表2   各模型对已婚妇女劳动力产出的工资系数的估计值
---------------------------------------------------------------------------
Options           | Estimator | Instruments | Estimate | (Standard error)
---------------------------------------------------------------------------
                    command reg
---------------------------------------------------------------------------
robust            | OLS       | −           | −17.4    | (81.4)
---------------------------------------------------------------------------
                    command ivregress
---------------------------------------------------------------------------
robust            | 2SLS      | basic only  | 1179.1   | (185.2)
robust            | 2SLS      | extended    | 536.4    | (101.5)
---------------------------------------------------------------------------
                    command mivreg
---------------------------------------------------------------------------
hom               | LIML      | extended    | 1120.6   | (195.3)
hom robust fuller | FULL      | extended    | 1110.0   | (197.2)
het robust fuller | HFUL      | extended    | 1058.3   | (170.5)
---------------------------------------------------------------------------
```

显然，由于未考虑内生性，使用 `reg` 命令得到的 OLS 估计值是不一致的，其数值甚至是负的，具有极大的内生性偏误。两个 2SLS 的估计值之间高达超出两倍的差异，显示了传统工具及 `ivregress` 命令在多个工具变量的情况下不可行。而使用 `mivreg` 得到的 LIML , FULL 和 HFUL 估计值与 2SLS 仅使用基础工具变量时得到的估计值接近。在同方差情形下的 LIML 和 FULL 估计值，与异方差情形下的 HLIM 估计值之间有一点差异，虽然不大，但已使得 HFUL 估计值更加可靠。与 2SLS 相比，HLIM 的较小的标准差，可以理解为使用了扩展的工具变量集后获得的效率提升。

* 运行结果
  输入如下命令，使用 LIML 估计：

```Stata
use http://www.stata.com/data/jwooldridge/eacsap/mroz.dta
unab vars: nwifeinc educ age kidslt6 kidsge6 exper expersq fatheduc motheduc hushrs husage huseduc mtr
local nvar: word count `vars´
forval i = 1/`nvar´ {
forval j = 1/`=`i´-1´ {
local x : word `i´ of `vars´
local y : word `j´ of `vars´
generate `x´X`y´ = `x´ * `y´
}
}
generate one = 1
mivreg hours nwifeinc educ age kidslt6 kidsge6 one (lwage = nwifeinc educ age kidslt6 kidsge6 one exper expersq fatheduc motheduc hushrs husage huseduc mtr *X*) if inlf==1 , hom
```

运行结果为：

```Stata
MIVREG estimation (HOM)

First-stage summary                                  Number of obs   =     428
-------------------------                            F(   7,   421)  =   95.74
F(  86,   336)  =    2.07                            Prob > F        =  0.0000
Prob > F        =  0.0000                            R-squared       = -0.5157
R-squared       =  0.8471                            Adj R-squared   = -0.5373
-------------------------                            Root MSE        =  1.1e+03
LIML estimation
Bekker variance estimation

------------------------------------------------------------------------------
       hours |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
       lwage |   1120.595   195.3494     5.74   0.000     736.6134    1504.577
    nwifeinc |  -7.890468   5.261349    -1.50   0.134    -18.23225    2.451317
        educ |  -133.1851   31.79141    -4.19   0.000    -195.6748   -70.69543
         age |  -9.954741   7.918058    -1.26   0.209    -25.51859    5.609111
     kidslt6 |  -246.5892   143.8619    -1.71   0.087    -529.3663    36.18793
     kidsge6 |  -65.87682   44.77805    -1.47   0.142    -153.8932    22.13958
         one |    2345.98   487.9451     4.81   0.000     1386.868    3305.093
------------------------------------------------------------------------------
Instrumented:  lwage
Instruments:   nwifeinc educ age kidslt6 kidsge6 one exper expersq fatheduc
               motheduc hushrs husage huseduc mtr educXnwifeinc ageXnwifeinc
               ageXeduc kidslt6Xnwifeinc kidslt6Xeduc kidslt6Xage
               kidsge6Xnwifeinc kidsge6Xeduc kidsge6Xage kidsge6Xkidslt6
               experXnwifeinc experXeduc experXage experXkidslt6 experXkidsge6
               expersqXnwifeinc expersqXeduc expersqXage expersqXkidslt6
               expersqXkidsge6 expersqXexper fatheducXnwifeinc fatheducXeduc
               fatheducXage fatheducXkidslt6 fatheducXkidsge6 fatheducXexper
               fatheducXexpersq motheducXnwifeinc motheducXeduc motheducXage
               motheducXkidslt6 motheducXkidsge6 motheducXexper
               motheducXexpersq motheducXfatheduc hushrsXnwifeinc hushrsXeduc
               hushrsXage hushrsXkidslt6 hushrsXkidsge6 hushrsXexper
               hushrsXexpersq hushrsXfatheduc hushrsXmotheduc husageXnwifeinc
               husageXeduc husageXage husageXkidslt6 husageXkidsge6
               husageXexper husageXexpersq husageXfatheduc husageXmotheduc
               husageXhushrs huseducXnwifeinc huseducXeduc huseducXage
               huseducXkidslt6 huseducXkidsge6 huseducXexper huseducXexpersq
               huseducXfatheduc huseducXmotheduc huseducXhushrs huseducXhusage
               mtrXnwifeinc mtrXeduc mtrXage mtrXkidslt6 mtrXkidsge6 mtrXexper
               mtrXexpersq mtrXfatheduc mtrXmotheduc mtrXhushrs mtrXhusage
               mtrXhuseduc
------------------------------------------------------------------------------
AG specification test:
J statistic  =  0.1748
Prob > J =  0.8059
------------------------------------------------------------------------------
```

使用 FULL 估计，选项 `hom, robust, fuller`，命令为：

```Stata
mivreg hours nwifeinc educ age kidslt6 kidsge6 one (lwage = nwifeinc educ ///
age kidslt6 kidsge6 one exper expersq fatheduc motheduc hushrs husage ///
huseduc mtr *X*) if inlf==1 , hom robust fuller
```

使用 HFUL 估计，选项 `het, robust, fuller`，命令为：

```Stata
mivreg hours nwifeinc educ age kidslt6 kidsge6 one (lwage = nwifeinc educ ///
age kidslt6 kidsge6 one exper expersq fatheduc motheduc hushrs husage ///
huseduc mtr *X*) if inlf==1 , het robust fuller
```

## 参考文献

- Anatolyev, S. 2019. Many instruments and/or regressors: A friendly guide. Journal of Economic Surveys 33: 689–726.
- Anatolyev, S., and N. Gospodinov. 2011. Specification testing in models with many instruments. Econometric Theory 27: 427–441.
- Anatolyev, S., and P. Yaskov. 2017. Asymptotics of diagonal elements of projection matrices under many instruments/regressors. Econometric Theory 33: 717–738.
- Angrist, J. D., and A. B. Krueger. 2001. Instrumental variables and the search for identification: From supply and demand to natural experiments. Journal of Economic Perspectives 15: 69–85.
- Basmann, R. L. 1957. A generalized classical method of linear estimation of coefficients in a structural equation. Econometrica 25: 77–83.
- Bekker, P. A. 1994. Alternative approximations to the distributions of instrumental variable estimators. Econometrica 62: 657–681.
- Chao, J. C., J. A. Hausman, W. K. Newey, N. R. Swanson, and T. Woutersen. 2014. Testing overidentifying restrictions with many instruments and heteroskedasticity. Journal of Econometrics 178: 15–21.
- Fuller, W. A. 1977. Some properties of a modification of the limited information estimator. Econometrica 45: 939–954.
- Hansen, C., J. Hausman, and W. K. Newey. 2008. Estimation with many instrumental variables. Journal of Business & Economics Statistics 26: 398–422.
- Hausman, J. A., W. K. Newey, T. Woutersen, J. C. Chao, and N. R. Swanson. 2012. Instrumental variable estimation with heteroskedasticity and many instruments. Quantitative Economics 3: 211–255.
- Lee, Y., and R. Okui. 2012. Hahn–Hausman test as a specification test. Journal of Econometrics 167: 133–139.
- Mroz, T. A. 1987. The sensitivity of an empirical model of married women’s hours of work to economic and statistical assumptions. Econometrica 55: 765–799.