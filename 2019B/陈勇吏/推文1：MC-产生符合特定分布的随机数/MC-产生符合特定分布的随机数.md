
&emsp;

> 作者：陈勇吏 (上海交通大学安泰经济与管理学院)      
> &emsp;       
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)

&emsp;

- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/0722/231925_ae8d0b76_1522177.png)](https://gitee.com/arlionn/Course/blob/master/README.md)



&emsp;

 
####  [2019暑期“实证研究方法与经典论文”专题班-连玉君-江艇主讲](https://gitee.com/arlionn/Course/blob/master/2019Papers.md) 

[![2019暑期“实证研究方法与经典论文”专题班-连玉君-江艇主讲](https://images.gitee.com/uploads/images/2019/0722/231925_a6942f5f_1522177.png)](https://gitee.com/arlionn/Course/blob/master/2019Papers.md)

---



这一部分我们将讨论随机数的产生原理，以及随机数的代码实现方式。在介绍随机数之前，我们先简单介绍一下**蒙特卡洛模拟**和**随机数**的概念。

假设$\scriptsize X$是一个随机变量，其分布函数为$\scriptsize F(x)$，概率密度函数为$\scriptsize f(x)$。我们可能会对$\scriptsize E[g(X)]$感兴趣（$\scriptsize g$是一个给定的函数）。比如，若随机变量$\scriptsize X$代表产品的销量，则销售利润可以表示为$\scriptsize g(X)=(p-c)X$，这也是一个随机变量。在已知随机变量$\scriptsize X$概率分布的情况下，我们希望知道销售利润的**期望值**$\scriptsize E[g(X)]$是多少。带入随机变量的期望公式得到：$\scriptsize E[g(X)] = \int g(x)f(x)dx$，可以很容易求出积分结果。

但现实生活中，我们感兴趣的$\scriptsize E[g(\bf X)]$中的$\scriptsize \bf X$往往不是单一的随机变量，而是由多个随机变量组成的随机向量$\scriptsize {\bf X} = (X_1, X_2, ..., X_n)$。若已知随机向量$\scriptsize {\bf X}$的联合概率密度函数$\scriptsize f(x_1, x_2, ... x_n)$，则：$\scriptsize E[g({\bf X})] = \int\int\cdot\cdot\cdot\int g(x_1,x_2,...,x_n)f(x_1,x_2,...,x_n)dx_1dx_2\cdot\cdot\cdot dx_n$。多数情况下，这个多重积分很难求解。此时，我们考虑另一种方式来近似计算$\scriptsize E[g(\bf X)]$的值。

**近似计算方法：**
$\small \bf Step1$：产生一个联合概率密度函数为$\scriptsize f(x_1, x_2, ... x_n)$的随机向量$\scriptsize {\bf X}^{(1)} = (X_1^{(1)}, X_2^{(1)}, ..., X_n^{(1)})$，并计算$\scriptsize Y^{(1)} = g({\bf X}^{(1)})$。
$\small \bf Step2$：重复$\small r$次$\scriptsize Step1$的过程，得到独立同分布的$\scriptsize Y^{(i)} = g({\bf X}^{(i)}),\ i = 1,...,r$。
$\small \bf Step3$：使用$\scriptsize Y^{(i)}$的平均值（样本均值）来近似$\scriptsize E[g({\bf X})]$（总体均值）。
$$\scriptsize \lim_{n\to\infty} \frac{Y^{(1)} + Y^{(2)} + \cdot\cdot\cdot + Y^{(r)}}{r} = E[Y^{(i)}] = E[g({\bf X})]\ \textrm{（中心极限定理）}$$

上述近似计算方法称为**蒙特卡洛模拟**方法。即通过从相同的概率分布中多次抽取独立同分布的样本，使用 样本平均值 近似 总体平均值。将每一次抽取到的独立同分布的样本记为$\scriptsize {\bf X}^{(i)} = (X_1^{(i)}, X_2^{(i)}, ..., X_n^{(i)})$，则样本中的每一个元素$\scriptsize X_k^{(i)}\ (k = 1, 2, ..., n)$都是一个**随机变量**。我们将这种从给定的概率分布中抽取出来的独立的随机变量，称为**随机数**。

## 1.如何产生随机数
随机变量的分布形式多种多样，包括**连续性**随机变量、**离散型**随机变量。其中连续性随机变量包括均匀分布、指数分布等**可以写出分布函数形式**的随机变量，以及正态分布、卡方分布等**只存在概率密度函数形式**的随机变量。针对不同的概率分布形式，生成随机数的方式有所不同。但生成任意分布的随机数的基础，是生成服从$\scriptsize U(0,1)$均匀分布的随机数，其他分布的随机数是在$\scriptsize U(0,1)$随机数的基础上产生。

### 1.1 $U(0,1)$均匀分布的随机数
#### 1.1.1 产生过程
产生$\scriptsize U(0,1)$分布的一种方式是：将写有$\scriptsize 0,1,...,9$的完全相同的 10 个纸条放在一个盒子中，每次从中有放回地抽取一张纸条，连续抽取$\small n$次。将抽取到的$\small n$个数字按 抽取顺序 排列在小数点的后面，得到的数字即为服从$\scriptsize U(0,1)$均匀分布的随机数。例如：如果抽取的数字序列是$\scriptsize 3,8,7,2,1$，则得到的服从$\scriptsize U(0,1)$分布的随机数是$\scriptsize 0.38721$。

然而，数字计算机通常不使用这种方式来产生随机数，而是使用**伪随机数**来代替随机数。伪随机数的产生原理是**取模**。即给定一个初始值$\scriptsize X_0$，通过递归公式$$\scriptsize X_{n+1} = (aX_{n} + c)\ \textrm{modulo}\ m,\ n \geq 0$$可以得到一个序列$\scriptsize \{ X_{n} \}$，其中$\scriptsize a,c,m$是设定的正整数。该等式表示将$\scriptsize  (aX_{n} + c)$除以$\scriptsize m$的余数赋值给$\scriptsize X_{n+1}$，因此序列中的每个$\scriptsize X_{n}$都只能在$\scriptsize (0,1,...,m-1)$中取值。$\tiny \frac{X_{n}}{m}$是一个在$\scriptsize [0,1)$之间取值的数，可以看做近似服从$\scriptsize U(0,1)$均匀分布。可以看出，当设定合适的$\scriptsize a,c,m$值后，得到的伪随机数序列完全取决于给定的序列初始值$\scriptsize X_0$，这个初始值称为**种子**。设定相同的种子，可以得到完全相同的随机序列。这也是伪随机数的优点之一：可以重复相同的模拟过程。

#### 1.1.2 stata代码实现
Stata有很多用来产生随机数的内置函数，其中生成均匀分布随机数的函数为`runiform()`。在生成随机数前，可以通过`set seed #`命令设定随机数的初始值（种子）。具体代码如下：

- 生成 3 个$\scriptsize U(0,1)$的随机数序列，每个序列包含 10 个随机数。

```stata
*===== 代码 =====
clear
set obs 10
forvalues i = 1/3 {
	gen unif`i' = runiform()
}
*===== 生成结果 =====
unif1	unif2	unif3
.1387824	.56928813	.32682486
.92432766	.7060023	.30766002
.3676381	.81376687	.21982671
.11971952	.06189653	.69186391
.91902139	.27757641	.04663675
.60349285	.41005086	.60378652
.89752506	.22520046	.52267927
.48974351	.34788238	.23261151
.21933775	.17111017	.92006581
.88946164	.72442917	.39863705
/* 三次模拟，产生三个不同的随机数序列 */
```

- 生成 3 个$\scriptsize U(0,1)$的随机数序列，每个随机数序列都设定相同的种子。
```stata
*===== 代码 =====
clear
set obs 10
forvalues i = 1/3 {
	set seed 10
	gen unif`i' = runiform()
}
*===== 生成结果 =====
unif1	unif2	unif3
.60128311	.60128311	.60128311
.91370436	.91370436	.91370436
.26673195	.26673195	.26673195
.60736577	.60736577	.60736577
.03607367	.03607367	.03607367
.75318004	.75318004	.75318004
.45917266	.45917266	.45917266
.90569935	.90569935	.90569935
.31343658	.31343658	.31343658
.3755541	.3755541	.3755541
/* 设定相同的随机数，三次模拟都产生相同的随机数序列。*/
```

### 1.2 连续型随机变量的随机数
对于任意给定的连续型随机变量，若分布函数$\scriptsize F$已知，则该随机变量的随机数可以通过$\scriptsize F^{-1}(u)$获得，其中$\scriptsize u$为$\scriptsize U(0,1)$的随机数。换言之，连续型随机变量的随机数，可以通过对$\scriptsize u \sim U(0,1)$求分布函数的逆运算获得。
证明：记$\scriptsize X = F^{-1}(U),\ U \sim U(0,1)$，则$\scriptsize X$的分布函数$\scriptsize F_X(x)$满足
$$\scriptsize F_X(x) = P \{ X\leq x \} = P \{F^{-1}(U) \leq x\} = P \{ F(F^{-1}(U)) \leq F(x) \} = P \{ U \leq F(x) \} = F(x)$$ 证得，通过$\scriptsize X = F^{-1}(U)$得到的随机数服从$\scriptsize F$分布。

##### Stata代码实现
生成连续型随机变量的随机数函数包括：均匀分布 `runiform(a,b)`、正态分布 `rnormal(m,s)`、指数分布 `rexponential(b)`、卡方分布 `rchi2(df)`、t 分布 `rt(df)` 等。详细内容可以通过 `help random` 命令查看。

生成随机数的具体代码如下所示：

- **使用 Stata 的内置函数生成随机数**
```stata
*========== 代码：生成随机数序列 ==========
* 1.生成随机数序列
  clear
  set obs 1000
 *1.1 U(1,5)均匀分布的随机数序列
  gen runif = runiform()
 *1.2 指数分布 Exp(5) 的随机数序列
  gen exp = rexponential(5)
 *1.3 标准正态分布的随机数序列
  gen rnormal = rnormal()
 *1.4 N(3,2)正态分布的随机数序列
  gen rnormal_3_2 = rnormal(3, 2)
 *1.5 自由度为 3 的 t 分布的随机数序列
  gen rt = rt(3)
 *1.6 自由度为 43 的卡方分布的随机数序列
  gen rchi2 = rchi2(43)

* 2.随机数序列的分布图
  foreach dist in runif exp rnormal rnormal_3_2 rt rchi2 {
  	histogram `dist', name(`dist', replace) nodraw //kdensity
  }
  graph combine runif exp rnormal rnormal_3_2 rt rchi2
```
![随机数序列的分布图](https://images.gitee.com/uploads/images/2019/0705/005612_15ad524e_1333498.png)

- **使用分布函数求逆的方式生成随机数**
指数分布$\scriptsize Exp(\lambda) $的分布函数为：$\scriptsize F(x) = 1 - e^{-\frac{x}{\lambda}}$，反函数为$\scriptsize F^{-1}(u) = - \lambda\ ln(1-u)$
均匀分布$\scriptsize U(a, b)$的分布函数为：$\scriptsize F(x) = \frac{x-a}{b-a}$，反函数为$\scriptsize F^{-1}(u) = a + (b-a)u$

由于$\scriptsize u \sim U(0, 1)$，故$\scriptsize 1-u \sim U(0, 1)$。所以，指数分布$\scriptsize Exp(\lambda) $的随机数可以通过$\scriptsize - \frac{1}{\lambda}ln(u)$得到，$\scriptsize U(a, b)$均匀分布的随机数可以通过$\scriptsize a + (b-a)u$得到。

```stata
*========== 代码 ==========
clear
set obs 10
* 1.指数分布 Exp(5) 的随机数序列
set seed 20
gen exp_f = -5 * log(runiform()) //分布函数求逆的方式
set seed 20
gen exp = rexponential(5) //使用内置函数的方式
* 2.均匀分布 U(1,5) 的随机数序列
set seed 20
gen unif_f = 1 + (5-1)*runiform() //分布函数求逆的方式
set seed 20
gen unif = runiform(1,5) //使用内置函数的方式

*========== 生成结果 ==========
exp_f	exp	unif_f	unif
1.6727648	1.6727648	3.8626318	3.8626318
.47869168	.47869168	4.634807	4.634807
1.1330894	1.1330894	4.1889014	4.1889014
7.785189	7.785189	1.8430378	1.8430378
1.0036905	1.0036905	4.2725067	4.2725067
.02883674	.02883674	4.976997	4.976997
4.806301	4.806301	2.5296427	2.5296427
8.2741926	8.2741926	1.7644917	1.7644917
7.1020147	7.1020147	1.9664666	1.9664666
10.250074	10.250074	1.514932	1.514932
/* 可以看出，两种方式得到的随机数序列相同。 */
```
当感兴趣的随机变量在 Stata 中没有相应的内置函数来生成随机数时，可以通过**分布函数求逆**的方式计算。

### 1.3 离散型随机变量的随机数
对于任意给定的离散型随机变量，若概率分布为$\scriptsize P \{ X = x_{j} \} = P_{j}$，则该随机变量的随机数同样可以通过对$\scriptsize u \sim U(0,1)$求分布函数的逆运算获得。具体方式为：令$\scriptsize U \sim U(0,1)$，
$$\scriptsize X = \begin{cases}
                           x_1,\ & if\ U < P_1 \\
                           x_2,\ & if\ P_1 < U < P_1 + P_2 \\
                           \ \vdots \\
                           x_j,\ & if\ \sum_{1}^{j-1} P_j < U < \sum_{1}^{j} P_j  \\
                           \ \vdots  \end{cases}$$  可以看出$\scriptsize X$的概率分布为$\scriptsize P \{ X = x_{j} \} = P \{ \sum_{1}^{j-1} P_j < U < \sum_{1}^{j} P_j \} = P_{j}$。

##### Stata代码实现
生成离散型随机变量的随机数函数包括：二项分布 `rbinomial(n,p)`、超几何分布 `rhypergeometric(N,K,n)`、泊松分布 `rpoisson(m)`、负二项分布 `rnbinomial(n,p)` 等。详细内容可以通过 `help random` 命令查看。

生成随机数的具体代码如下所示：
```stata
*========== 代码：生成随机数序列 ==========
* 1.生成随机数序列
clear
set obs 1000
 *1.1 二项分布 B(10,0.5) 的随机数序列
gen rbinomial = rbinomial(10,0.5)
 *1.2 超几何分布 H(20,4,10) 的随机数序列
 /*
	其中：20 代表总体数量
		  4  代表总体中指定种类的数量
		  10 代表抽取的样本数量
 */
gen rhypergeometric = rhypergeometric(20,4,10)
 *1.3 泊松分布 Poisson(20) 的随机数序列
gen rpoisson = rpoisson(20)
 *1.4 负二项分布 NB(123,0.2) 的随机数序列
gen rnbinomial = rnbinomial(123,0.2)

* 2.随机数序列的分布图
foreach dist in rbinomial rhypergeometric rpoisson rnbinomial {
	histogram `dist', name(`dist', replace) nodraw //kdensity
}
graph combine rbinomial rhypergeometric rpoisson rnbinomial
```
![随机数序列的分布图](https://images.gitee.com/uploads/images/2019/0705/005612_53043a53_1333498.png)


## 2.如何产生具有相关性的随机数
Stata 提供了 `drawnorm` 命令，可以从多元正态分布中生成随机数序列。具体语法为
 `drawnorm newvarlist [, options]`，常用的 `option` 选项包括：
- `corr(matrix)`：设定多元正态分布的协方差矩阵
- `mean(vector)`：设定多元正态分布的均值向量，默认为 `mean(0)`。
具体规则可以查看 `help drawnorm`。

### 2.1 相关性
**Pearson** 相关系数和 **Spearman** 相关系数是衡量变量相关性最常用的两个指标。
- **Pearson**相关系数
用来衡量变量间的线性关系。计算公式为$$\scriptsize \rho_{X,Y} = \frac{Cov(X,Y)}{\sigma_X \sigma_Y} = \frac{E[XY]-E[X]E[Y]}{\sqrt{E[X^2]-E[X]^2}\sqrt{E[Y^2]-E[Y]^2}}$$
- **Spearman**相关系数
等级变量的 **Pearson** 相关系数。将两个变量分别排序以后生成相应的等级数据，使用两个变量对应的**等级数据**计算 **Pearson** 相关系数。

Stata 举例如下：
```stata
clear
set obs 1000
* 1.生成相关系数矩阵
/*
  相关系数矩阵是对称矩阵，矩阵“维数”与待生成的“随机序列个数”一致。
  其中：
  	（第i个）主对角元素：代表（第i个）随机变量的【方差】，
  	（第 i行j列 的）非主对角元素：代表（第i,j）两个随机变量间的【协方差】
*/
matrix C = (1, 0.5 \ 0.5, 1)
matrix list C
*======= Stata 结果 ========
symmetric C[2,2]
    c1  c2
r1   1
r2  .5   1
*===========================

* 2.生成二元正态分布样本（生成两个随机数序列：均值为 0，协方差矩阵为 C）
drawnorm x y, corr(C) //means(0,0)选项设定均值向量为(0,0)，可省略不写
* 3.查看两个随机序列的相关性
 *3.1 Pearson相关系数
corr x y
*========== Stata 结果 ===========
(obs=1,000)
             |        x        y
-------------+------------------
           x |   1.0000
           y |   0.5218   1.0000
*=================================

 *3.2 Spearman相关系数
  *3.2.1 使用 Pearson系数 计算 Spearman相关系数
sort x
gen rank_x = _n
sort y
gen rank_y = _n
corr rank_x rank_y
*========== Stata 结果 ===========
(obs=1,000)
             |   rank_x   rank_y
-------------+------------------
      rank_x |   1.0000
      rank_y |   0.5011   1.0000
*=================================
  *3.2.2 使用 内置函数 计算 Spearman相关系数
spearman x y
*========== Stata 结果 ===========
 Number of obs =    1000
Spearman's rho =       0.5011

Test of Ho: x and y are independent
    Prob > |t| =       0.0000
*=================================*
/* 两种方式得到的 Spearman相关系数 相等，都等于 0.5011。*/
```

从 Pearson 和 Spearman 两个相关系数的定义及计算公式可以看出：对变量做**正的线性变换**，变量间的 **Pearson** 相关系数不变；对变量做**非负的函数变换**， 变量间的 **Spearman** 相关系数保持不变。

### 2.2 产生具有相关性的随机序列
产生具有相关性的正态分布随机序列，可以使用 Stata 的 `drawnorm` 命令。产生具有相关性的其他分布的随机数序列没有现成的 Stata 命令，可以先将 `drawnorm` 得到的正态随机序列转换为均匀分布，然后通过**分布函数求逆**的方式得到想要的随机数序列。原理如下：

- **分布函数$\scriptsize Y=F_X(X)$服从$\scriptsize U(0,1)$均匀分布**
$$\scriptsize F(y) = \begin{cases}
0,\ y < 0 \\
P \{ Y \leq y\} = P \{ F_X(X) \leq y \} = P \{ X \leq F_X^{-1}(y) \} = F(F_X^{-1}(y)) = y,\ 0 \leq y < 1 \\
1,\ y \geq 1 \end{cases}$$
若$\scriptsize X$是正态分布的随机序列，$\scriptsize F_X$是正态分布的分布函数，则$\scriptsize F_X(X)$是均匀分布的随机序列。
```stata
* 1.生成均匀分布随机序列
clear
set obs 10000
 *1.1 生成两个相关的正态随机序列：x,y
matrix C = (1, 0.5 \ 0.5, 1)
drawnorm x y, corr(C)
 *1.2 根据正态分布函数，生成与 x,y 对应的均匀分布序列：x_unif,y_unif
gen x_unif = normal(x)
gen y_unif = normal(y)
 *1.3 作图展示生成的随机数序列的分布情况
foreach dist in x y x_unif y_unif {
	hist `dist', name(`dist', replace) nodraw
}
graph combine x y x_unif y_unif
/* 可以看出，生成的 x_unif y_unif 两个随机序列在 (0,1) 上均匀分布 */
```
![随机数序列的分布图](https://images.gitee.com/uploads/images/2019/0705/005612_90bc315e_1333498.png)


- **分布函数是单调不减函数，故$\scriptsize F_X(X)$与$\scriptsize X$具有相同的 **Spearman** 系数**
可以验证，上面代码中生成的 `x_unif` 和 `y_unif` 之间的 Spearman 相关系数，与正态序列 `x` 和 `y` 之间的 Spearman 相关系数是一样的。
```stata
* 1.1 正态序列的 Spearman值
spearman x y
/*============= Stata 结果 =============
 Number of obs =   10000
Spearman's rho =       0.4917
Test of Ho: x and y are independent
    Prob > |t| =       0.0000
*======================================*/
* 1.2 生成的均匀分布序列的 Spearman值
spearman x_unif y_unif
/*============= Stata 结果 =============
 Number of obs =   10000
Spearman's rho =       0.4917
Test of Ho: x_unif and y_unif are independent
    Prob > |t| =       0.0000
*======================================*/
```
但 Pearson 相关系数有所不同：
```stata
* 2.1 正态序列的 Pearson值
corr x y
/*============= Stata 结果 =============
(obs=10,000)
             |        x        y
-------------+------------------
           x |   1.0000
           y |   0.5046   1.0000
*======================================*/
* 2.2 生成的均匀分布序列的 Pearson值
corr x_unif y_unif
/*============= Stata 结果 =============
(obs=10,000)
             |   x_unif   y_unif
-------------+------------------
      x_unif |   1.0000
      y_unif |   0.4912   1.0000
*======================================*/

/* 可以看出，两组变量的 Spearson 系数值完全一样，
            两组变量的 Pearson 系数值有些微不同   */
```

- **任意分布$\scriptsize F$的随机序列都可以通过$\scriptsize F^{-1}(U) = F^{-1}(F_X(X))$的方式得到**
使用 Stata 内置的逆分布函数 `invgamma()` 和 `invexponential()`，带入正态分布产生的均匀分布$\scriptsize U = F_X(X)$，生成 `伽马 Gamma` 随机序列和 `指数 Exponential` 随机序列：
```stata
* 1.生成 Gamma 随机序列和 Exponential 随机序列
gen x_gamma = invgammap(4, x_unif)
gen y_exponential = invexponential(1, y_unif)
* 2.做序列的分布图
hist x_gamma, name(x_gamma, replace)
hist y_exponential , name(y_exponential , replace)
```

![随机序列的分布图](https://images.gitee.com/uploads/images/2019/0705/005612_c8a00eb4_1333498.png)


- **分布函数的反函数$\scriptsize F^{-1}(U)$是单调不减函数，故$\scriptsize F^{-1}(F_X(X))$与$\scriptsize X$具有相同的 **Spearman** 系数**
可以验证，上面代码中生成的伽马随机序列 `x_gamma` 和指数随机序列 `y_exponential` 之间的 Spearman 相关系数，与正态序列 `x` 和 `y` 之间的 Spearman 相关系数是一样的。

```stata
spearman x y
/*============= Stata 结果 =============
 Number of obs =   10000
Spearman's rho =       0.4758
Test of Ho: x and y are independent
    Prob > |t| =       0.0000
*======================================*/
spearman x_gamma y_exponential
/*============= Stata 结果 =============
 Number of obs =   10000
Spearman's rho =       0.4758
Test of Ho: x_gamma and y_exponential are independent
    Prob > |t| =       0.0000
*======================================*/
```
但 Pearson 相关系数有所不同：
```stata
spearman x y
/*============= Stata 结果 =============
(obs=10,000)
             |        x        y
-------------+------------------
           x |   1.0000
           y |   0.4894   1.0000
*======================================*/
spearman x_gamma y_exponential
/*============= Stata 结果 =============
(obs=10,000)
             |  x_gamma y_expo~l
-------------+------------------
     x_gamma |   1.0000
y_exponent~l |   0.4515   1.0000
*======================================*/
```
如此，`drawnorm` 命令不仅可以生成 `正态` 随机序列，也可以进一步通过分布函数求逆的方式得到相关性相同的 `其他分布` 的随机序列。使用 `drawnorm` 命令生成具有相关性的 `非正态分布` 随机序列的 **Stata 完整代码**如下所示：
```stata
clear
set obs 10000
* 1.定义相关系数矩阵 
matrix C = (  1,  0.7, -0.3,  0.2 \ ///
            0.7,    1,  0.2, -0.1 \ ///
		   -0.3,  0.2,    1,  0.3 \ ///
			0.2, -0.1,  0.3,    1)
* 2.生成 4 个相关系数矩阵为 C 的正态随机向量
drawnorm x1 x2 x3 x4, corr(C)
* 3.根据 逆分布函数 方法生成四个其他分布的随机序列
/* 
    四个分布包括：
		自由度为5的卡方分布：chi2(5),
		均值为5的泊松分布：poisson(5),
		自由度为 5,5 的F分布：F(5,5)
		在(-5,5)之间的均匀分布：uniform(-5,5) 
*/
gen x_chi2 = invchi2(5, normal(x1))
gen x_poisson = invpoisson(5, 1-normal(x2)) // normal(x2) 和 1-normal(x2) 都是U(0,1)均匀分布的随机序列
gen x_F = invF(5, 5, normal(x3))
gen x_uniform = -5 + normal(x4)*10
* 4.查看四个生成序列的分布图
foreach dist in x_chi2 x_poisson x_F x_uniform {
	hist `dist', name(`dist', replace) nodraw
}
graph combine x_chi2 x_poisson x_F x_uniform, imargin(large)
* 5.查看相关系数
mat list C
spearman x?
spearman x_*
corr x?
corr x_*
```
![随机序列的分布图](https://images.gitee.com/uploads/images/2019/0705/005612_bbbae3b1_1333498.png)

## 3.应用举例
在计量模型中，随机数可以很好的模拟扰动项的随机性，以及我们对数据特征的高质量要求。这有助于我们按照预计的分布特征去抽样，进而验证计量模型的有效性，比较计量模型的优劣。我们将引入一个简单的例子，介绍随机数如何应用于 **似不相关回归（SUR）** 和 **OLS** 的模型比较。
#### 例子
我们考虑应用 `似不相关回归（SUR）` 方法来决定是否开发小行星铂金矿物资源的例子，具体内容参见 [Pricing an Asteroid](http://blog.thisyoungeconomist.com/2012/04/pricing-asteroid.html)。在本例中，我们尝试估计三个存在相关性的模型。三个模型具有相同的解释变量： `市场现有铂金数量(X1)`，`投资者们的平均年龄(X2)`，`宇宙飞船的新闻报道数量(X3)`。被解释变量分别为：`铂金价格(Y1)`，`宇宙飞船的投资金额(Y2)`，`宇宙飞船的新闻报道数量(Y3)`。
$$\scriptsize \begin{cases}  
                Y_1 = \beta_{10} + \beta_{11}  X_1 + \beta_{12}  X_2 + \beta_{13} X_3 + \varepsilon_1 \\
                Y_2 = \beta_{20} + \beta_{21}  X_1 + \beta_{22}  X_2 + \beta_{23} X_3 + \varepsilon_2 \\
                Y_3 = \beta_{30} + \beta_{31}  X_1 + \beta_{32}  X_2 + \beta_{33} X_3 + \varepsilon_3
\end{cases}$$
- 先**生成三个解释变量**：
`clear`
`set obs 10000`
`gen x1 = 10 + rnormal()`
`gen x2 = 50 + rnormal()`
`gen x3 = rpoisson(10)`
- 生成三个**线性相关的扰动项**
`matrix C = (12, -5,  5 \ -5, 12, -3 \ 5, -3,  6 )`
`drawnorm u1 u2 u3, cov(C)`
- 生成三个**被解释变量**
若模型中的系数矩阵为
$$\scriptsize 
B = \left\{ \begin{matrix}  
                  20 & 3 & 0 & -0.1 \\  5 & 3 & 3.5 & 5.2 \\ 10 & 3 & 5.3 & -10
       \end{matrix} \right\} $$ 则生成：
`gen y1 = 20 + 3*x1 + 0*x2 - 0.1*x3 + u1`
`gen y2 = 5 + 3*x1 + 3.5*x2 + 5.2*x3 + u2`
`gen y3 = 10 + 3*x1 + 5.3*x2 - 10*x3 + u3`

- **OLS** 回归
`reg y1 x1 x2 x3`
`reg y2 x1 x2 x3`
`reg y3 x1 x2 x3`
- **SUR** 回归
`不加约束：` `sureg (y1 = x1 x2 x3) (y2 = x1 x2 x3) (y3 = x1 x2 x3)`
`加约束：三个模型的 x1 系数相等`
`constraint 1 [y1]x1 = [y2]x1`
`constraint 2 [y1]x1 = [y3]x1`
`sureg (y1 = x1 x2 x3) (y2 = x1 x2 x3) (y3 = x1 x2 x3), const(1 2)`

OLS 估计结果：
$$\scriptsize \begin{cases}  
                Y_1 = 22.224 + 3.043X_1 - 0.049X_2 - 0.116X_3 + \varepsilon_1 \\
                Y_2 = 2.264 + 2.944X_1 + 3.566X_2 + 5.203X_3 + \varepsilon_2 \\
                Y_3 = 11.207 + 2.997X_1 + 5.278X_2 - 10.006X_3 + \varepsilon_3
\end{cases}$$

SUR 估计结果（不加约束）：
$$\scriptsize \begin{cases}  
                Y_1 = 22.224 + 3.043X_1 - 0.049X_2 - 0.116X_3 + \varepsilon_1 \\
                Y_2 = 2.264 + 2.944X_1 + 3.566X_2 + 5.203X_3 + \varepsilon_2 \\
                Y_3 = 11.207 + 2.997X_1 + 5.278X_2 - 10.006X_3 + \varepsilon_3
\end{cases}$$

SUR 估计结果（加约束）：
$$\scriptsize \begin{cases}  
                Y_1 = 22.832 + 2.985X_1 - 0.050X_2 - 0.116X_3 + \varepsilon_1 \\
                Y_2 = 1.835 + 2.985X_1 + 3.566X_2 + 5.203X_3 + \varepsilon_2 \\
                Y_3 = 11.332 + 2.985X_1 + 5.278X_2 - 0.006X_3 + \varepsilon_3
\end{cases}$$

用于生成的数据：
$$\scriptsize \begin{cases}  
                Y_1 = 20 + 3X_1 + 0X_2 - 0.1X_3 + \varepsilon_1 \\
                Y_2 = 5 + 3X_1 + 3.5X_2 + 5.2X_3 + \varepsilon_2 \\
                Y_3 = 10 + 3X_1 + 5.3X_2 - 10X_3 + \varepsilon_3
\end{cases}$$

可以看出，当三个模型的解释变量完全相同时，若不加约束，则 SUR 与 OLS 的回归结果完全相同；若加约束，SUR 的回归结果比 OLS 更好一些。解释变量不同的情形可以参考 [Seemingly Unrelated Regression-with Correlated Errors](http://www.econometricsbysimulation.com/2012/04/seemingly-unrelated-regression-with.html)。

**完整的 Stata 代码及回归结果如下：**





```stata
clear
set obs 10000
gen x1 = 10 + rnormal()
gen x2 = 50 + rnormal()
gen x3 = rpoisson(10)
matrix C = (12, -5, 5 \ -5, 12, -3 \ 5, -3, 6 )
drawnorm u1 u2 u3, cov(C)
gen y1 = 20 + 3*x1 + 0*x2 - 0.1*x3 + u1
gen y2 = 5 + 3*x1 + 3.5*x2 + 5.2*x3 + u2
gen y3 = 10 + 3*x1 + 5.3*x2 - 10*x3 + u3

// OLS回归
reg y1 x1 x2 x3
reg y2 x1 x2 x3
reg y3 x1 x2 x3

// SUR回归
  *-不加约束
sureg (y1 = x1 x2 x3) (y2 = x1 x2 x3) (y3 = x1 x2 x3)
  *-加约束
constraint 1 [y1]x1 = [y2]x1
constraint 2 [y1]x1 = [y3]x1
sureg (y1 = x1 x2 x3) (y2 = x1 x2 x3) (y3 = x1 x2 x3), const(1 2)


*========================= OLS 回归结果 =======================
. reg y1 x1 x2 x3

  Source |       SS           df       MS      Number of obs =  10,000
---------+----------------------------------   F(3, 9996)    = 2627.73
   Model |  95478.2491         3   31826.083   Prob > F      =  0.0000
Residual |  121067.806     9,996  12.1116252   R-squared     =  0.4409
---------+----------------------------------   Adj R-squared =  0.4407
   Total |  216546.055     9,999  21.6567711   Root MSE      =  3.4802
----------------------------------------------------------------------
      y1 |      Coef.   Std. Err.     t   P>|t|   [95% Conf. Interval]
---------+------------------------------------------------------------
      x1 |   3.043426   .0346424   87.85  0.000    2.97552    3.111332
      x2 |  -.0491475    .034845   -1.41  0.158  -.1174508    .0191557
      x3 |  -.1155822    .010902  -10.60  0.000  -.1369523   -.0942122
   _cons |   22.22404   1.782452   12.47  0.000   18.73008    25.71801
----------------------------------------------------------------------

. reg y2 x1 x2 x3

  Source |       SS           df       MS      Number of obs =   10,000
---------+----------------------------------   F(3, 9996)    = 80558.04
   Model |  2948482.24         3  982827.414   Prob > F      =   0.0000
Residual |  121953.602     9,996  12.2002403   R-squared     =   0.9603
---------+----------------------------------   Adj R-squared =   0.9603
   Total |  3070435.85     9,999  307.074292   Root MSE      =   3.4929
-----------------------------------------------------------------------
      y2 |      Coef.   Std. Err.      t   P>|t|   [95% Conf. Interval]
---------+-------------------------------------------------------------
      x1 |   2.943839   .0347689    84.67  0.000   2.875685    3.011993
      x2 |   3.565666   .0349722   101.96  0.000   3.497114    3.634219
      x3 |   5.202808   .0109418   475.50  0.000    5.18136    5.224256
   _cons |   2.263563    1.78896     1.27  0.206  -1.243159    5.770286
-----------------------------------------------------------------------

. reg y3 x1 x2 x3

  Source |       SS           df       MS      Number of obs =   10,000
---------+----------------------------------   F(3, 9996)    > 99999.00
   Model |  10622883.6         3  3540961.19   Prob > F      =   0.0000
Residual |       61363     9,996  6.13875551   R-squared     =   0.9943
---------+----------------------------------   Adj R-squared =   0.9943
   Total |  10684246.6     9,999  1068.53151   Root MSE      =   2.4777
-----------------------------------------------------------------------
      y3 |     Coef.   Std. Err.      t    P>|t|   [95% Conf. Interval]
---------+-------------------------------------------------------------
      x1 |  2.996978   .0246631   121.52   0.000   2.948633    3.045322
      x2 |  5.278252   .0248073   212.77   0.000   5.229624    5.326879
      x3 | -10.00558   .0077615 -1289.13   0.000  -10.02079   -9.990367
   _cons |   11.2074   1.268986     8.83   0.000   8.719934    13.69487
-----------------------------------------------------------------------

*===================== SUR 回归结果：不加约束 ====================
. sureg (y1 = x1 x2 x3) (y2 = x1 x2 x3) (y3 = x1 x2 x3)

Seemingly unrelated regression
-------------------------------------------------------------------
Equation      Obs   Parms        RMSE    "R-sq"       chi2        P
-------------------------------------------------------------------
y1         10,000       3     3.47948    0.4409    7886.35   0.0000
y2         10,000       3    3.492186    0.9603  241770.82   0.0000
y3         10,000       3    2.477156    0.9943   1.73e+06   0.0000
-------------------------------------------------------------------

------------------------------------------------------------------------
          |     Coef.   Std. Err.      z    P>|z|   [95% Conf. Interval]
----------+-------------------------------------------------------------
y1        |
       x1 |  3.043426   .0346355    87.87   0.000   2.975541     3.11131
       x2 | -.0491475    .034838    -1.41   0.158  -.1174288    .0191337
       x3 | -.1155822   .0108998   -10.60   0.000  -.1369454    -.094219
    _cons |  22.22404   1.782095    12.47   0.000    18.7312    25.71688
----------+-------------------------------------------------------------
y2        |
       x1 |  2.943839    .034762    84.69   0.000   2.875707    3.011971
       x2 |  3.565666   .0349652   101.98   0.000   3.497136    3.634197
       x3 |  5.202808   .0109396   475.59   0.000   5.181366    5.224249
    _cons |  2.263563   1.788603     1.27   0.206  -1.242033     5.76916
----------+-------------------------------------------------------------
y3        |
       x1 |  2.996978   .0246581   121.54   0.000   2.948649    3.045307
       x2 |  5.278252   .0248023   212.81   0.000    5.22964    5.326863
       x3 | -10.00558   .0077599 -1289.39   0.000  -10.02079   -9.990372
    _cons |   11.2074   1.268732     8.83   0.000   8.720733    13.69407
------------------------------------------------------------------------

*===================== SUR 回归结果：加约束 ====================
. constraint 1 [y1]x1 = [y2]x1
. constraint 2 [y1]x1 = [y3]x1
. sureg (y1 = x1 x2 x3) (y2 = x1 x2 x3) (y3 = x1 x2 x3), const(1 2)

Seemingly unrelated regression
-------------------------------------------------------------------
Equation      Obs   Parms        RMSE    "R-sq"       chi2        P
-------------------------------------------------------------------
y1         10,000       3    3.479975    0.4408   36858.93   0.0000
y2         10,000       3    3.492431    0.9603  271242.65   0.0000
y3         10,000       3    2.477185    0.9943   1.75e+06   0.0000
-------------------------------------------------------------------

 ( 1)  [y1]x1 - [y2]x1 = 0
 ( 2)  [y1]x1 - [y3]x1 = 0
-------------------------------------------------------------------------
          |      Coef.   Std. Err.      z    P>|z|   [95% Conf. Interval]
----------+--------------------------------------------------------------
y1        |
       x1 |   2.985018    .015583   191.56   0.000   2.954476     3.01556
       x2 |  -.0495507   .0348408    -1.42   0.155  -.1178374     .018736
       x3 |  -.1160189   .0108984   -10.65   0.000  -.1373794   -.0946584
    _cons |   22.83197   1.752945    13.02   0.000   19.39626    26.26768
----------+--------------------------------------------------------------
y2        |
       x1 |   2.985018    .015583   191.56   0.000   2.954476     3.01556
       x2 |   3.565951   .0349683   101.98   0.000   3.497414    3.634487
       x3 |   5.203116   .0109383   475.68   0.000   5.181677    5.224554
    _cons |   1.834963   1.759306     1.04   0.297  -1.613212    5.283139
----------+--------------------------------------------------------------
y3        |
       x1 |   2.985018    .015583   191.56   0.000   2.954476     3.01556
       x2 |   5.278169    .024802   212.81   0.000   5.229558     5.32678
       x3 |  -10.00567   .0077586 -1289.62   0.000  -10.02088   -9.990464
    _cons |   11.33189   1.253044     9.04   0.000   8.875964    13.78781
-------------------------------------------------------------------------

```






### 参考资料

- [Introduction to Probability Models, Sheldon M.Ross 10th Edition.pdf](http://vdisk.weibo.com/s/d2MtH3GUNYbKZ)
- [Generating 'random' variables drawn from any distribution](http://www.econometricsbysimulation.com/2012/06/generating-random-variables-drawn-from.html)
- [Drawing jointly distributed non-normal random variables](http://www.econometricsbysimulation.com/2012/09/drawing-jointly-distributed-non-normal.html)
- [Generate rank correlated variables](http://www.econometricsbysimulation.com/2012/06/how-to-generate-variables-that-are-rank.html)
- [Seemingly Unrelated Regression-with Correlated Errors](http://www.econometricsbysimulation.com/2012/04/seemingly-unrelated-regression-with.html)
- [Write your own System OLS in Mata](http://www.econometricsbysimulation.com/2012/05/write-your-own-system-ols-in-mata.html)



&emsp;


>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。您也可以从 [连享会选题平台](https://gitee.com/Stata002/StataSX2018/wikis/Home) &rarr; [002_备选主题] 中选择感兴趣的题目来撰写推文。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表](https://www.jianshu.com/p/de82fdc2c18a) 
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/0722/232051_ebe9413f_1522177.png)](https://gitee.com/arlionn/Course/blob/master/README.md)





---
> [![image](https://images.gitee.com/uploads/images/2019/0722/232051_91cc3b32_1522177.jpeg)](https://gitee.com/arlionn/Course/blob/master/README.md)
