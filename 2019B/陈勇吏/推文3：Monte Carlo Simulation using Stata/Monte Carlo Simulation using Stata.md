> 作者：陈勇吏（上海交通大学）
> 连享会：([知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn))

&nbsp;


本篇推文将介绍 Stata 中，做蒙特卡洛模拟的两种常用方法。第一种方法是使用 `postfile` 命令，第二种方法是 `simulate` 命令，并举了两个具体的例子，说明如何在 Stata 中做蒙特卡洛模拟。

## 一、蒙特卡洛模拟（MC）
蒙特卡洛模拟方法（MC），即从总体中抽取大量随机样本的计算方法。当根据总体的分布函数 $F(\bf x)$ 很难求出想要的数字特征时，可以使用蒙特卡洛模拟的方法，从总体中抽取大量样本，使用样本的数字特征估计总体的数字特征。

比如，我们想知道 $E[g(\bf X)]$，其中 ${\bf X}$ 是随机向量，其概率密度函数为 $f(x_1, x_2, ... x_n)$。根据期望公式可以得到：$E[g({\bf X})] = \int\int\cdot\cdot\cdot\int g(x_1,x_2,...,x_n)f(x_1,x_2,...,x_n)dx_1dx_2\cdot\cdot\cdot dx_n。$

这是一个多重积分，大多时候很难求解。此时，我们可以使用蒙特卡洛模拟的方法，从总体中抽取大量的样本，通过样本来近似 $E[g(\bf X)]$。具体操作过程如下：
- 从总体的概率分布 $\scriptsize f(x_1, x_2, ... x_n)$ 中抽取一个随机样本 $\scriptsize {\bf X}^{(1)} = (X_1^{(1)}, X_2^{(1)}, ..., X_n^{(1)})$，并计算 $\scriptsize Y^{(1)} = g({\bf X}^{(1)})$。
- 重复 $\small r$ 次 上述过程，得到 $\small r$ 个独立同分布的样本 $\scriptsize Y^{(i)} = g({\bf X}^{(i)}),\ i = 1,...,r$。
- 使用$\scriptsize Y^{(i)}$的平均值（样本均值）来近似$\scriptsize E[g({\bf X})]$（总体均值）。

&nbsp;
## 二、两种常用的 MC 方法
MC 方法的原理是从总体中生成大量的样本，Stata 有两种常用的方法。第一种是使用 `postfile` 命令，另一种是使用 `simulate` 命令。
### 2.1 postfile 命令
`postfile` 命令需要与循环语句结合使用。使用 `foreach、while` 等循环语句逐次生成独立的样本，并基于样本计算感兴趣的统计值。使用 `postfile` 命令生成 dta 文件，并将每次循环得到的数据追加进来。蒙特卡洛模拟次数由循环次数决定，最后生成的 dta 文件中，每一行代表一次蒙特卡洛模拟，每一列代表一个基于样本计算出的统计值。

#### 2.1.1 `postfile` 命令语法

查看帮助文件 `help postfile`，可以看到三个配套使用的命令 `postfile、post、postclose`。

**postfile 命令**
`postfile` 命令的原理是：在内存中划出一个区域，在该区域中存放 MC 中生成的数据。在这个过程中，需要给区域起一个名字，来告诉程序需要把数据存入哪一块区域中。同样地，需要给存入的数据取一个名字，方便索引到这个数据。具体语法规则如下：
```stata
postfile postname newvarlist using filename [, every(#) replace]
```
其中，`postname` 表示内存中划出的区域的名字，`filename` 表示保存的数据的名字，`newvarlists` 表示数据中包含的变量名列表。即在 `postname` 内存区域中，生成一个 `filename` 数据文件，该数据包含的变量是 `newvarlists` 。

**post 命令**
`post` 命令的原理是，在 `postfile` 生成的数据文件中追加数据。具体语法规则如下：
```stata
post postname (exp) (exp) ... (exp)
```
其中，`postname` 告诉程序在哪一块内存区域追加数据（即追加到哪个内存区域对应的数据集中）。`(exp) (exp) ... (exp)` 为追加的数据内容，与 `postfile` 中的 `newvarlist` 变量名一一对应。

**postclose 命令**
`postclose` 表示结束追加数据，将所有蒙特卡洛模拟过程中 `post` 的数据写入硬盘中，可以使用 `use` 命令打开。

#### 2.1.2 `postfile` 的使用方式
通常情况下，使用 `postfile` 做蒙特卡洛模拟的 Stata 代码格式如下：
```stata
tempname memhold	//定义临时的内存区域的名字
tempfile results	//定义临时的保存文件的名字

postfile `memhold' ... using `results'	//在 `memhold' 内存区域中生成 `results' dta文件，包含 ... 中列出的变量。
forvalues i = 1/# {	    //循环语句，做 # 次蒙特卡洛模拟
	...					//根据总体分布，生成随机样本，进行相关运算
	post `memhold' ...	//追加感兴趣的计算值
}
postclose `memhold'		//结束追加数据，完成 # 次蒙特卡洛模拟

use "`results'", clear	//打开蒙特卡洛模拟生成的数据
```


### 2.2 simulate 命令
`simulate` 命令的语法格式如下：
```stata
simulate [exp_list] , reps(#) [options] : command
```
其中，`command` 为一次蒙特卡洛模拟执行的程序，可以是 Stata 自带的或者外部下载安装的命令，也可以是用户自己封装的程序。`[exp_list]` 表示将每一次 `command` 命令的执行结果按 `[exp_list]` 格式提取出来。

| `exp_list` 格式 |                  表达式               |       举例       |
|--------------------|-------------------------------------|------------------|
| (name: elist) |                 | (scale: sd=r(sd) iqr=(r(p75)-r(p25)) |
| elist | newvar = (exp) <br>  (exp) | mean=r(mean) <br> r(sd) |
| eexp | _b、_b[] <br> _se、_se[]  | _b |



选项 `reps(#)` 表示做 # 次蒙特卡洛模拟，即执行 # 次 `command` 中的命令。其他选项 `[options]` 和功能如下：
```sql
options                          作用
---------------------------------------------------------------------------
nodots                   MC 过程中不在屏幕上打印点
dots(#)                  每隔 # 次模拟，在屏幕上打印一个点
noisily                  将每次 MC 的结果都显示在屏幕上
trace                    追踪命令运行过程
saving(filename, ...)    将 MC 结果保存在数据中
nolegend                 不显示 MC 信息
verbose                  显示 MC 信息
seed(#)                  设定随机数种子为 #  
```






&nbsp;
## 三、例子
### 3.1 例一
从对数正态分布中，抽取样本量为 100 的样本，计算样本均值与方差。将上述过程重复 1000 次，查看 1000 次蒙特卡洛模拟的结果。

**使用 postfile 命令：**
```stata
clear
set obs 100
set seed 1234
gen b = .

tempname sim
tempfile results
postfile `sim' mean Var using "`results'", replace
quietly {
	forvalues i = 1/1000 {
		replace b = exp(rnormal())
		summarize
		scalar mean = r(mean)
		scalar Var = r(Var)
		post `sim' (mean) (Var)
	}
}
postclose `sim'
use "`results'", clear
sum
```

**使用 simulate 命令：**
```stata
//封装代码
cap program drop lnsim
program lnsim, rclass
	version 13
	drop _all
	set obs 100
	gen z = exp(rnormal())
	summarize z
	return scalar mean = r(mean)
	return scalar Var = r(Var)
end

set seed 1234
simulate mean=r(mean) var=r(Var), reps(1000) nodots: lnsim
describe *
sum
```

两个命令的结果是一样的：
```
. describe *
              storage   display    value
variable name   type    format     label      variable label
-----------------------------------------------------------------------
mean            float   %9.0g                 r(mean)
var             float   %9.0g                 r(Var)

. sum
    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
        mean |      1,000    1.630648    .2173062   1.106372   2.612052
         var |      1,000     4.60798    4.502166    .966087   70.55969
```


### 3.2 例二
实验拟合模型 $y_j = a + bx_j + u_j$。假设真实的模型为：$a = 1,  b = 2, u_j = z_j + cx_j, z_j \sim N(0,1)$。在实验过程中，我们将保存参数估计值和标准误差，并尝试不同的 $c$。$x_j$ 在实验中固定，且从 $N(0,1)$ 中生成。

在这之前，生成一个真实数据 truth.dta ：
```stata
drop _all
set obs 100
set seed 54321
gen x = rnormal()
gen true_y = 1 + 2*x
save truth.dta, replace 
```
根据 truth.dta 数据，做 10000 次蒙特卡洛模拟。在每一次模拟过程中：
- 生成随机序列 $Z_j \sim N(0,1)$
- 计算 $y_j = 1 + (2+c)x_j + z_j$
- 拟合模型 `reg y x`，提取参数估计值和标准误

**使用 postfile 命令：**
```stata
use truth.dta, clear
set seed 123
local c = 3

tempname sim
tempfile results
postfile `sim' b se using "`results'", replace
quietly {
	forvalues i = 1/10000 {
		capture drop y
		gen y = true_y + (rnormal() + `c'*x)
		reg y x
		post `sim' (_b[x]) (_se[x])
	}
}
postclose `sim'

use "`results'", clear
sum
```

**使用 simulate 命令：**
```stata
set seed 123
cap program drop hetero
program hetero
	version 13
	args c
	capture drop y
	gen y = true_y + (rnormal() + `c'*x)
	regress y x
end

simulate _b _se, reps(10000): hetero 3
```
两个命令的结果是一样的：
```
===== postfile 命令的结果 ====
. sum
    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
           b |     10,000     5.00097    .1004966    4.62909   5.379388
          se |     10,000    .0998357     .007169   .0754253   .1309434

===== simulate 命令的结果 ====
. sum
    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
        _b_x |     10,000     5.00097    .1004966    4.62909   5.379388
     _b_cons |     10,000    1.000281    .1002894   .6301054   1.409075
       _se_x |     10,000    .0998357     .007169   .0754253   .1309434
    _se_cons |     10,000    .0998682    .0071713   .0754499   .1309861

```