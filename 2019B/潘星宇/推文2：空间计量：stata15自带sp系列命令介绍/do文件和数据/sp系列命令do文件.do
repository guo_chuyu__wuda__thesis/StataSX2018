cd C:\Users\xypan\Desktop\连享会推文\空间计量sp系列命令\数据和do文件

unzipfile "Local_Authority_Districts_December_2016_Full_Clipped_Boundaries_in_Great_Britain.zip",replace

spshape2dta Local_Authority_Districts_December_2016_Full_Clipped_Boundaries_in_Great_Britain,replace


use Local_Authority_Districts_December_2016_Full_Clipped_Boundaries_in_Great_Britain.dta , clear
encode lad16cd, generate(lacode)
spset lacode, modify replace



merge 1:1 lad16cd using "LA-CO2-2014.dta"
keep if _merge==3
drop _merge
save "CO2-merged.dta", replace

import excel  "File_10_ID2015_Local_Authority_District_Summaries.xlsx"  , sheet("IMD") firstrow clear
keep LocalAuthorityDistrictcode2 IMDAveragescore
rename LocalAuthorityDistrictcode2 lad16cd
merge 1:1 lad16cd using "CO2-merged.dta"
keep if _merge==3
drop _merge
save co2_IMD_data

grmap domestictotal, clnumber(9) fcolor(PuRd) name(dommap, replace) title("Total CO2")

scatter regres domest, msymbol(Oh) name(regres, replace)
grmap regres, clnumber(9) fcolor(BuYlRd) ///
name(regresmap, replace)

spmatrix create contiguity W, rook
spmatrix create idistance W2


// take logarithms of domestictotal and population
generate logdom = log(domestictotal)
generate logpop = log(population)
// make per capita CO2
generate pc = logdom-logpop
grmap pc, clnumber(9) fcolor(PuRd) name(pcmap, replace) title("Per capita CO2")
regress pc IMDAveragescore
predict regres, residuals


spregress pc IMDAveragescore, gs2sls dvarlag(W)

spregress pc IMDAveragescore , ml errorlag(W) dvarlag(W)
estimates store normalreg
spregress pc IMDAveragescore, ml dvarlag(W)
lrtest normalreg

spregress pc IMDAveragescore ,ml dvarlag(W)
estimates store normalreg
spregress pc IMDAveragescore, ml errorlag(W) dvarlag(W)
lrtest normalreg

import excel using 面板数据.xlsx ,first clear
// take logarithms of domestictotal and population
generate logdom = log(domestictotal)
generate logpop = log(population)
// make per capita CO2
generate pc = logdom-logpop

xtset id year ,yearly

use co2_IMD_data,clear
spshape2dta Local_Authority_Districts_December_2016_Full_Clipped_Boundaries_in_Great_Britain,replace
spset lacode, modify replace
spmatrix create contiguity W, rook      
spgenerate WIMDAveragescore =W*IMDAveragescore
summarize pc IMDAveragescore WIMDAveragescore

