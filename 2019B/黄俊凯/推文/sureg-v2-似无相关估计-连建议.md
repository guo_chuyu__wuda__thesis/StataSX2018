

> **作者：** 黄俊凯（中国人民大学财政金融学院）  
> **E-mail：** <kopanswer@126.com>

&emsp;

> Stata连享会 &ensp;   [主页](https://www.lianxh.cn/news/46917f1076104.html)  || [视频](http://lianxh.duanshu.com) || [推文](https://www.zhihu.com/people/arlionn/) 

![](https://file.lianxh.cn/images/20191111/3ed0c73d48c0046f04c502458b4e1c0b.png)
> 扫码查看连享会最新专题、公开课视频和 100 多个码云计量仓库链接。

&emsp;

「**Source**: [Econometrics Academy: SUR ](https://sites.google.com/site/econometricsacademy/econometrics-models/seemingly-unrelated-regressions)」

&emsp;

&#x1F34E; 修改建议：by 连玉君 `2020/6/12 7:45`

- 基于「女士们的护肤品、化妆品、鞋帽服装需求体系 (通常是似不相关模型)」的例子说明 SUR 的基本思想和要点
- 增加一个小节，介绍 `help sureg postestimation` 中提到的拟合值的计算和 `margins` 分析，这在目前的多数资料里都没有呈现
- 增加一个小节重点说明 `suest` 命令的使用，比如组间系数差异检验，引用 [Stata: 如何检验分组回归后的组间系数差异？](https://www.lianxh.cn/news/051e3a01cdb19.html)，这反而是本文的重点所在。
  - 支持面板数据回归的 `suest` 命令：
  - `net describe suest, from(http://www.econometrics.it/stata)`  
- 增加一些 SUR 扩展命令 (简要说明即可)
  - `help nlsur` //
  - `help xtsur` // panel data SUR
    - `net describe xtsur, from(http://fmwww.bc.edu/RePEc/bocode/x)` 
  - `help bireprob` // SJ 16-1, bivariate random-effects probit models
  - `help sudcd` //seemingly unrelated discrete-choice duration models. 

- 增加联立方程的相关命令和简要说明
  - `help reg3` //


### 其他拓展或新的选题
- 参考这篇 [Fitting a seemingly unrelated regression (sureg) manually](http://statistics.ats.ucla.edu/stat/stata/code/sureg.htm)，可以考虑融入到本文中，也可以单独再写一篇新的推文。 
- 新命令介绍 `rifsureg`，作者提供了范例 dofiles  
  - `net describe st0588, from(http://www.stata-journal.com/software/sj20-1)` 
  - Fernando Rios-Avila, 2020, Recentered influence functions (RIFs) in Stata: RIF regression and RIF decomposition, Stata Journal, 20(1): 51–94. [[pdf\]](https://sci-hub.tw/10.1177/1536867X20909690)


&emsp;

&emsp;

&emsp;

&emsp;



## 1. Seemingly Unrelated Regressions (SUR)

### 1.1 方程组和系统估计

方程组有多个被解释变量，按照是否包含内生解释变量可以分为两类：

1. 联立方程模型 (simultaneous equation models)

   右端项包含被解释变量

2. 似不相关模型 (seemingly unrelated models)

   右端项不包含被解释变量

常见的方程组模型有：

- 一般均衡模型 (通常是联立方程模型)
- 居民们对各种食物的需求体系 (通常是似不相关模型)
- 女士们的护肤品、化妆品、鞋帽服装需求体系 (通常是似不相关模型)

系统估计的利弊：

- 如果多个方程之间有某种联系，对方程组进行联合估计可能提高估计的效率
- 如果方程组中某个方程误差较大，系统估计会将该方程的误差带入其他方程

### 1.2 SUR 模型

考虑一个有 $n$ 个观测值和 $m$ 个方程的线性方程组 

第 $i$ 个观测值的第 $j$ 个方程：
$$
y_{ij}=X_{ij}\beta_{ij}+\mu_{ij}
$$
第 $j$ 个方程：
$$
y_{j}=X_{j}\beta_{j}+\mu_{j}
$$

- 方程组：

$$
\left[\begin{array}{c}
y_{1} \\
y_{2} \\
\vdots \\
y_{m}
\end{array}\right]=
\left[\begin{array}{cccc}
X_{1}  & 0      & \cdots & 0      \\
0      & X_{2}  &        & 0      \\
\vdots & \vdots & \ddots & \vdots \\
0      & 0      & \cdots & X_{m}
\end{array}\right]
\left[\begin{array}{c}
\beta_{1} \\
\beta_{2} \\
\vdots    \\
\beta_{m}
\end{array}\right]
+
\left[\begin{array}{c}
u_{1}  \\
u_{2}  \\
\vdots \\
u_{m}
\end{array}\right]
$$

SUR 模型：每个观测值不同方程的误差项相关，但不同观测值的误差项无关的方程组

注：SUR 模型的每个方差都可以单独 OLS 估计，因此被称为"似不相关" (但 OLS 估计不是有效的)

### 1.3 SUR 模型的假设

- 任意一个观测值 $i$ 的任意两个方程 $j$ 和 $k$：

$$
E\left(\mu_{ij}\mu_{ik}|X_{i}\right) =\sigma_{jk} \neq 0
$$

- 任意一个观测值 $i$ 的方差-协方差矩阵 $\Sigma$：

$$
\begin{align}
\Sigma &= E\left(\mu_{i}\mu_{i}\right) \\
&= 
\left[\begin{array}{cccc}
\sigma_{1}  & \sigma_{12} & \cdots & \sigma_{1n} \\
\sigma_{12} & \sigma_{2}  &        & \sigma_{2n} \\
\vdots      & \vdots      & \ddots & \vdots \\
\sigma_{1n} & \sigma_{2n} & \cdots & \sigma_{n}
\end{array}\right]
\end{align}
$$

- 任意方程 $j$ 和 $k$：

$$
\begin{align}
E\left(\mu_{j}|X\right) &= 0 \\
E\left(\mu_{j}\mu_{j}|X\right) &= \sigma_{j}I_{n} \\
E\left(\mu_{j}\mu_{k}|X\right) &= \sigma_{jk}I_{n} 
\end{align}
$$

- 方程组的方差-协方差矩阵 $\Omega$：

$$
\begin{align}
\Omega &= E\left(uu^{\prime}|X\right) = \Sigma \otimes I_{n} \\
&=
\left[\begin{array}{cccc}
\sigma_{1}I_{n}  & \sigma_{12}I_{n} & \cdots & \sigma_{1n}I_{n} \\
\sigma_{12}I_{n} & \sigma_{2}I_{n}  &        & \sigma_{2n}I_{n} \\
\vdots           & \vdots           & \ddots & \vdots           \\
\sigma_{1n}I_{n} & \sigma_{2n}I_{n} & \cdots & \sigma_{n}I_{n}
\end{array}\right]
\end{align}
$$

- 方差-协方差矩阵 (以2个观测值2个方程为例)

$$
\begin{align}
\Sigma &= 
\left[\begin{array}{cc}
\sigma_{1}  & \sigma_{12} \\
\sigma_{12} & \sigma_{2}  
\end{array}\right] \\
\Omega = \Sigma \otimes I_{n} 
&=
\left[\begin{array}{cc}
\sigma_{1}I_{n}  & \sigma_{12}I_{n} \\
\sigma_{12}I_{n} & \sigma_{2}I_{n}  
\end{array}\right] 
= 
\left[\begin{array}{cccc}
\sigma_{1}  & 0          
& \sigma_{12} & 0           \\
0           & \sigma_{1}  & 0           & \sigma_{12} \\
\sigma_{12} & 0           & \sigma_{2} & 0           \\
0           & \sigma_{12} & 0          & \sigma_{2}
\end{array}\right]
\end{align}
$$

### 1.4 SUR 估计

由于满足外生性条件 $E\left(\mu_{j}|X\right) = 0$，所以 OLS 估计是一致估计

由于不满足同方差假定，所以 OLS 估计不是最优的

已知误差项方差-协方差矩阵的形式，FGLS 估计是最优的一致估计

SUR 估计本质是 FGLS 估计，具体步骤如下：

1. OLS 估计，得到误差项方差-协方差矩阵 $\Sigma(\Omega)$ 的一致估计 $\hat{\Sigma}(\hat{\Omega})$

$$
\hat{\Sigma}=\frac{1}{n}\hat{\mu}_{ols}\hat{\mu}_{ols}^{\prime} 
\qquad
\hat{\Omega}=\hat{\Sigma} \otimes I_{n}
$$

2. 用 $\hat{\Omega}$ 替换 $\Omega $，GLS 估计

$$
\hat{\beta}_{SUR}=\hat{\beta}_{gls}(\hat{\Omega})=
(X^{\prime}\hat{\Omega}^{-1} X)^{-1}
X^{\prime}\hat{\Omega}^{-1}  y
$$

### 1.5 SUR 模型对 OLS 的优势

- 可以跨方程检验系数是否联合为 $0$，$\mathbb{H} \text{: } \beta_{j}=\beta_{k}=0$
- 可以跨方程检验系数是否相等，$\mathbb{H} \text{: } \beta_{j}=\beta_{k}$
- 可以先施加 $\beta_{j}=\beta_{k}$ 的约束，再进行 SUR 估计

## 2. Stata 实例

该实例为 **Ani Katchova** 教授在 UCLA 网站上提供的实例 [What is seemingly unrelated reg. and how can I perform it in Stata?](https://stats.idre.ucla.edu/stata/faq/what-is-seemingly-unrelated-regression-and-how-can-i-perform-it-in-stata/)。

若由于网络原因而无法加载实例有基础的朋友也可以参考 Stata 自带的帮助文档中的实例。

### 2.1 模型设定

我们想要知道数学和阅读的成绩如何受其他因素影响，构建如下方程组：
$$
\begin{align}
math &= \alpha + \beta_{1}·female + \beta_{2}·prog + \beta_{3}·science + \mu_{1} \\
read &= \alpha + \gamma_{1}·female + \gamma_{2}·socst + \mu_{2}
\end{align}
$$
其中 **math** 是数学成绩，**read** 是阅读成绩，**female** 是性别的哑变量，**prog** 是教育项目，**science** 是自然科学成绩，**socst** 是社会科学成绩。

方程 1 的被解释变量是 **math**，解释变量是 **female**，**prog** 和 **science**。

方程 1 的被解释变量是 **read**，解释变量是 **female** 和 **scost**。

方程 1 和 2 共同的解释变量是 **female**。

### 2.2 比较 OLS 和 SUR

我们用 Stata 的 `sureg` 命令进行 SUR 估计，该命令的选项 `corr` 报告各方程误差项的相关系数矩阵，并对误差项之间的独立性执行 布伦斯-帕甘 (*Breusch*–*Pagan*) 检验，代码如下：

```Stata
clear all
set more off
set cformat %4.3f
set pformat %4.3f
set sformat %4.2f

use "https://stats.idre.ucla.edu/stat/stata/notes/hsb2", clear

* or

use "https://gitee.com/arlionn/data/raw/master/data01/hsb2.dta", clear


* 对方程 1 和 2 单独进行 OLS 估计
reg math female prog science
reg read female socst

* 对方程 1 和 2 组成的方程组用 SUR 估计
sureg (math female prog science) (read female socst), corr
```

OLS 估计回归结果如下：

```Stata
reg math female prog science

      Source |       SS           df       MS      Number of obs   =       200
-------------+----------------------------------   F(3, 196)       =     43.81
       Model |  7010.50752         3  2336.83584   Prob > F        =    0.0000
    Residual |  10455.2875       196  53.3433035   R-squared       =    0.4014
-------------+----------------------------------   Adj R-squared   =    0.3922
       Total |   17465.795       199  87.7678141   Root MSE        =    7.3037

------------------------------------------------------------------------------
        math |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
      female |      0.966      1.046     0.92   0.357       -1.097       3.028
        prog |     -0.413      0.764    -0.54   0.589       -1.919       1.094
     science |      0.598      0.054    11.13   0.000        0.492       0.703
       _cons |     21.971      3.582     6.13   0.000       14.906      29.035
------------------------------------------------------------------------------

reg read female socst

      Source |       SS           df       MS      Number of obs   =       200
-------------+----------------------------------   F(2, 197)       =     63.93
       Model |  8233.79028         2  4116.89514   Prob > F        =    0.0000
    Residual |  12685.6297       197  64.3940595   R-squared       =    0.3936
-------------+----------------------------------   Adj R-squared   =    0.3874
       Total |    20919.42       199  105.122714   Root MSE        =    8.0246

------------------------------------------------------------------------------
        read |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
      female |     -1.764      1.141    -1.55   0.124       -4.014       0.487
       socst |      0.598      0.053    11.27   0.000        0.493       0.702
       _cons |     21.862      2.874     7.61   0.000       16.195      27.529
------------------------------------------------------------------------------
```

SUR 估计回归结果如下：

```Stata
sureg (math female prog science) (read female socst), corr

Seemingly unrelated regression
--------------------------------------------------------------------------
Equation             Obs   Parms        RMSE    "R-sq"       chi2        P
--------------------------------------------------------------------------
math                 200       3    7.256297    0.3971     111.11   0.0000
read                 200       2    7.981054    0.3910     112.68   0.0000
--------------------------------------------------------------------------

------------------------------------------------------------------------------
             |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
math         |
      female |      0.805      1.035     0.78   0.437       -1.224       2.834
        prog |     -0.530      0.743    -0.71   0.476       -1.987       0.927
     science |      0.534      0.052    10.18   0.000        0.431       0.637
       _cons |     25.603      3.494     7.33   0.000       18.755      32.450
-------------+----------------------------------------------------------------
read         |
      female |     -1.709      1.132    -1.51   0.131       -3.928       0.511
       socst |      0.549      0.052    10.57   0.000        0.447       0.651
       _cons |     24.374      2.818     8.65   0.000       18.851      29.897
------------------------------------------------------------------------------
```

OLS 估计和 SUR 估计的结果总结：
$$
\begin{array}{|l|c|c|c|c|}
\hline & \text { OLS } & \text { OLS } & \text { SUR } & \text { SUR } \\
& \text { math score } & \text { reading score } & \text { math score } & \text { reading score } \\
\hline \text { Female } & 0.97 & -1.76 & 0.80 & -1.71 \\
\hline \text { Program } & -0.41 & - & -0.53 & - \\
\hline \text { Science score } & 0.59^{*} & - & 0.53^{*} & - \\
\hline \text { Social science setore } & - & 0.60^{*} & - & 0.55^{*} \\
\hline \text { Constant } & 21.97^{*} & 21.86^{*} & 25.60^{*} & 24.37^{*} \\
\hline
\end{array}
$$
本例中，OLS 估计的结果与 SUR 估计的结果定性一致，差异不大。

布伦斯-帕甘 (*Breusch*–*Pagan*) 检验的结果

```Stata
Correlation matrix of residuals:

        math    read
math  1.0000
read  0.1849  1.0000

Breusch-Pagan test of independence: chi2(1) = 6.835, Pr = 0.0089
```

方程 1 和 2 的误差项存在显著的相关性（chi2 值 6.385，P 值小于 0.01），但相关性并不强（相关系数仅 0.18）

### 2.3 检验与约束

方程 1 和 2 中 **female** 的系数都与 0 无显著差异，我们进一步检验两个方程中变量 **female** 的系数是否相等，代码如下：

```Stata
test [math]female = [read]female
```

检验结果如下：

```Stata
(1)  [math]female - [read]female = 0

             chi2(1) =    3.28
         Prob > chi2 =    0.0701
```

系数差异检验的 chi2 值为 3.28，P 值为 0.07，大于 0.05，无法拒绝原假设。因此，我们对方程 1 和 2 施加约束：$\beta_{1}=\gamma_{1}$，代码如下：

```Stata
constraint 1 [math]female = [read]female
sureg (math female prog science) (read female socst), constraints(1) 
```

回归结果如下：

```Stata
Seemingly unrelated regression
--------------------------------------------------------------------------
Equation             Obs   Parms        RMSE    "R-sq"       chi2        P
--------------------------------------------------------------------------
math                 200       3    7.284186    0.3924     109.08   0.0000
read                 200       2    8.013775    0.3860     109.60   0.0000
--------------------------------------------------------------------------

 ( 1)  [math]female - [read]female = 0
------------------------------------------------------------------------------
             |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
math         |
      female |     -0.315      0.833    -0.38   0.705       -1.948       1.317
        prog |     -0.548      0.747    -0.73   0.463       -2.012       0.916
     science |      0.525      0.052    10.02   0.000        0.423       0.628
       _cons |     26.686      3.454     7.73   0.000       19.917      33.455
-------------+----------------------------------------------------------------
read         |
      female |     -0.315      0.833    -0.38   0.705       -1.948       1.317
       socst |      0.545      0.052    10.47   0.000        0.443       0.648
       _cons |     23.818      2.814     8.46   0.000       18.302      29.334
------------------------------------------------------------------------------
```

此时方程 1 和 2 中 **female** 的系数都相等，仍然显著不等于 0。

### 2.4 sureg 命令

基础语法

```Stata
sureg (depvar1 varlist1) (depvar2 varlist2) ...  (depvarN varlistN) 
      [if] [in] [weight] [, options]
```

进阶语法

```Stata
 sureg ([eqname1:] depvar1a [depvar1b ... =] varlist1 [, noconstant])
       ([eqname2:] depvar2a [depvar2b ... =] varlist2 [, noconstant])
       ...
       ([eqnameN:] depvarNa [depvarNb ... =] varlistN [, noconstant]) 
       [if] [in] [weight] [, options]
```

特别说明：当被解释变量 **depvar** 有多个时，不能指定方程名 **eqname**

一些重要的参数和选项

```Stata
*	参数
eqname                    方程
depvar                    被解释变量
varlist                   控制变量

*	约束
constraints(constraints)  应用指定的线性约束

*	自由度的调整
small                     报告小样本统计量
dfk                       使用小样本调整方法
dfk2                      使用替代的调整方法

*	最优化选项
isure                     迭代直到收敛
iterate(#)                指定最大的迭代次数
nolog					  抑制迭代过程的输出
tolerance(#)              指定数值收敛条件
```

此外，`sureg` 还可以与 `bootstrap`, `by`, `fp`, `jackknife`, `rolling`, and `statsby` 等前缀连用。

### 2.5 完整数据和代码

需要复现的朋友可以下载 [sur_scores.dta 和 sur_source.do](https://pan.baidu.com/s/1t94o8Y8lClyoNbbOiDqMXw)，提取码：xbt4 ，链接永久有效

```Stata
cls
clear all
set more off
set cformat %4.3f
set pformat %4.3f
set sformat %4.2f

global path "C:\SUR\"		//	替换为 sur_scores.dta 的路径
use "$path\sur_scores", clear

global y1list math					//	方程 1 的被解释变量
global y2list read					//	方程 2 的被解释变量
global x1list female prog science	//	方程 1 的解释变量
global x2list female socst			//	方程 2 的解释变量
global x1 female					//	方程 1 和 2 共同的解释变量

describe $y1list $y2list $x1list $x2list
summarize $y1list $y2list $x1list $x2list

* 对方程 1 和 2 单独进行 OLS 估计
reg $y1list $x1list
reg $y2list $x2list

* 对方程 1 和 2 组成的方程组用 SUR 估计
sureg ($y1list $x1list) ($y2list $x2list), corr

* 检验跨方程约束，方程 1 和 2 中变量 female 的系数是否相等
test [$y1list]$x1 = [$y2list]$x1

* 对 SUR 估计施加跨方程约束
constraint 1 [$y1list]$x1 = [$y2list]$x1
sureg ($y1list $x1list)($y2list $x2list), constraints(1) 
```

#### 参考文献和资料

- What is seemingly unrelated reg. and how can I perform it in Stata? [Link](https://stats.idre.ucla.edu/stata/faq/what-is-seemingly-unrelated-regression-and-how-can-i-perform-it-in-stata/)
- [Econometrics Academy: SUR ](https://sites.google.com/site/econometricsacademy/econometrics-models/seemingly-unrelated-regressions)
- Zellner, Arnold. "An efficient method of estimating seemingly unrelated regressions and tests for aggregation bias." *Journal of the American statistical Association* 57.298 (1962): 348-368. [Link](https://www.tandfonline.com/doi/abs/10.1080/01621459.1962.10480664)
- Zellner, A., and D. S. Huang. 1962. Further properties of efficient estimators for seemingly unrelated regression equations. International Economic Review 3: 300–313. [Link}](https://www.jstor.org/stable/2525396?seq=1)
- Zellner, Arnold. "Estimators for seemingly unrelated regression equations: Some exact finite sample results." *Journal of the American Statistical Association* 58.304 (1963): 977-992. [Link](https://amstat.tandfonline.com/doi/abs/10.1080/01621459.1963.10480681#.XstP4zPiuUk)
- Breusch, T. S., and A. R. Pagan. 1980. The Lagrange multiplier test and its applications to model specification in econometrics. Review of Economic Studies 47: 239–253. [Link](https://www.jstor.org/stable/2297111?seq=1)
- J. Lloyd Blackwell, III, 2005, Estimation and Testing of Fixed-effect Panel-data Systems, Stata Journal, 5(2): 202–207. [[pdf\]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500205)
- Allen McDowell, 2004, From the Help Desk: Seemingly Unrelated Regression with Unbalanced Equations, Stata Journal, 4(4): 442–448. [[pdf\]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400407)