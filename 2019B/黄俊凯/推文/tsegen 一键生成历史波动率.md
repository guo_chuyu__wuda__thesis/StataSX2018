> 作者：黄俊凯       
> 单位：中国人民大学财政金融学院

<br />

tsegen 允许 Stata 用户在使用 `egen` 函数时方便的调用时间序列算子

### 1 下载安装
```stata
ssc install tsegen, replace
help tsegen 
```

<br />

### 2 语法结构
```stata
tsegen [type] newvar = fcn(tsvarlist[, min_obs]) [if] [in] [, options]
```
> - `fcn` 仅限于那些参数为 varlist 的函数，你可以 help egen 和 help egenmore 来确定哪些函数可以 tsegen
> - `min_obs` 当 tsvarlist 中 non-missing obs 的数量小于 min_obs 个时，newvar 为缺失值
> - `options` 选项是基于 egen 函数的
> - `limits` Stata 一般最多只支持 100 个时间序列算子，但 tsegen 命令突破了该限制，例如 L(1/200).return

<br />

### 3 使用要点
> 1. 使用 `tsegen` 命令前，必须先用 `tsset` 或 `xtset` 命令声明数据类型为 时间序列数据 或 面板数据，才能调用时间序列算子
> 2. `tsegen` 命令不支持分组计算（non-byable），但你可以参考 [runby](https://www.jianshu.com/p/68af54db9ecb?utm_campaign=maleskine&utm_content=note&utm_medium=reader_share&utm_source=weixin) 命令实现分组计算

<br />

### 4 实例
#### 4.1 滚动窗口统计量
计算五年滚动窗口(包含当年)内的投资额平均数
```stata
webuse grunfeld, clear
tsegen inv_m5 = rowmean(invest L(1/4).invest)
```
下面这段代码与上面等价
```stata
webuse grunfeld, clear
tsegen inv_m5b = rowmean(L(0/4).invest)
```
至少要求五年滚动窗口内有三个观测值
```stata
webuse grunfeld, clear
tsegen inv_m5m3 = rowmean(L(0/4).invest, 3)
```
计算五年滚动窗口内投资的标准差，且要求窗口内至少有三个观测值
```stata
webuse grunfeld, clear
tsegen inv_sd5 = rowsd(L(0/4).invest, 3)
```
#### 4.2 平滑数据
对数据做简单移动平均（+3到-2期）
```stata
webuse sales1, clear
tsegen sm = rowmean(L(0/2).sales F(1/3).sales)
```
它等价于
```stata
webuse sales1, clear
tssmooth ma sm1=sales, window(2 1 3)
```
#### 4.3 进一步扩展
你也可以调用 egenmore 中的函数
例如判断一组变量是否都大于0或非缺失值的 rall()
```stata
webuse grunfeld, clear
gen double diff = D.mvalue
tsegen pg = rall(L(0/2).diff,3) , c(@ > 0)
```
它等价于
```stata
webuse grunfeld, clear
tsegen pg2 = rall(L(0/2)D.mvalue,3) , c(@ > 0)
```