黄俊凯（中国人民大学财政金融学院）kopanswer@126.com

「**Source**: [Econometrics Academy: SUR ](https://sites.google.com/site/econometricsacademy/econometrics-models/seemingly-unrelated-regressions)」

## 1. Seemingly Unrelated Regressions (SUR)

### 1.1 方程组和系统估计

方程组有多个被解释变量，按照是否包含内生解释变量可以分为两类：

1. 联立方程模型 (simultaneous equation models)

   右端项包含被解释变量

2. 似不相关模型 (seemingly unrelated models)

   右端项不包含被解释变量

常见的方程组模型有：

- 一般均衡模型 (通常是联立方程模型)
- 居民们对各种食物的需求体系 (通常是似不相关模型)
- 女士们的护肤品、化妆品、鞋帽服装需求体系 (通常是似不相关模型)

系统估计的利弊：

- 如果多个方程之间有某种联系，对方程组进行联合估计可能提高估计的效率
- 如果方程组中某个方程误差较大，系统估计会将该方程的误差带入其他方程

### 1.2 SUR 模型

考虑一个有 $n$ 个观测值和 $m$ 个方程的线性方程组 

第 $i$ 个观测值的第 $j$ 个方程：
$$
y_{ij}=X_{ij}\beta_{ij}+\mu_{ij}
$$
第 $j$ 个方程：
$$
y_{j}=X_{j}\beta_{j}+\mu_{j}
$$

- 方程组：

$$
\left[\begin{array}{c}
y_{1} \\
y_{2} \\
\vdots \\
y_{m}
\end{array}\right]=
\left[\begin{array}{cccc}
X_{1}  & 0      & \cdots & 0      \\
0      & X_{2}  &        & 0      \\
\vdots & \vdots & \ddots & \vdots \\
0      & 0      & \cdots & X_{m}
\end{array}\right]
\left[\begin{array}{c}
\beta_{1} \\
\beta_{2} \\
\vdots    \\
\beta_{m}
\end{array}\right]
+
\left[\begin{array}{c}
u_{1}  \\
u_{2}  \\
\vdots \\
u_{m}
\end{array}\right]
$$

SUR 模型：每个观测值不同方程的误差项相关，但不同观测值的误差项无关的方程组

注：SUR 模型的每个方差都可以单独 OLS 估计，因此被称为"似不相关" (但 OLS 估计不是有效的)

### 1.3 SUR 模型的假设

- 任意一个观测值 $i$ 的任意两个方程 $j$ 和 $k$：

$$
E\left(\mu_{ij}\mu_{ik}|X_{i}\right) =\sigma_{jk} \neq 0
$$

- 任意一个观测值 $i$ 的方差-协方差矩阵 $\Sigma$：

$$
\begin{align}
\Sigma &= E\left(\mu_{i}\mu_{i}\right) \\
&= 
\left[\begin{array}{cccc}
\sigma_{1}  & \sigma_{12} & \cdots & \sigma_{1n} \\
\sigma_{12} & \sigma_{2}  &        & \sigma_{2n} \\
\vdots      & \vdots      & \ddots & \vdots \\
\sigma_{1n} & \sigma_{2n} & \cdots & \sigma_{n}
\end{array}\right]
\end{align}
$$

- 任意方程 $j$ 和 $k$：

$$
\begin{align}
E\left(\mu_{j}|X\right) &= 0 \\
E\left(\mu_{j}\mu_{j}|X\right) &= \sigma_{j}I_{n} \\
E\left(\mu_{j}\mu_{k}|X\right) &= \sigma_{jk}I_{n} 
\end{align}
$$

- 方程组的方差-协方差矩阵 $\Omega$：

$$
\begin{align}
\Omega &= E\left(uu^{\prime}|X\right) = \Sigma \otimes I_{n} \\
&=
\left[\begin{array}{cccc}
\sigma_{1}I_{n}  & \sigma_{12}I_{n} & \cdots & \sigma_{1n}I_{n} \\
\sigma_{12}I_{n} & \sigma_{2}I_{n}  &        & \sigma_{2n}I_{n} \\
\vdots           & \vdots           & \ddots & \vdots           \\
\sigma_{1n}I_{n} & \sigma_{2n}I_{n} & \cdots & \sigma_{n}I_{n}
\end{array}\right]
\end{align}
$$

- 方差-协方差矩阵 (以2个观测值2个方程为例)

$$
\begin{align}
\Sigma &= 
\left[\begin{array}{cc}
\sigma_{1}  & \sigma_{12} \\
\sigma_{12} & \sigma_{2}  
\end{array}\right] \\
\Omega = \Sigma \otimes I_{n} 
&=
\left[\begin{array}{cc}
\sigma_{1}I_{n}  & \sigma_{12}I_{n} \\
\sigma_{12}I_{n} & \sigma_{2}I_{n}  
\end{array}\right] 
= 
\left[\begin{array}{cccc}
\sigma_{1}  & 0          
& \sigma_{12} & 0           \\
0           & \sigma_{1}  & 0           & \sigma_{12} \\
\sigma_{12} & 0           & \sigma_{2} & 0           \\
0           & \sigma_{12} & 0          & \sigma_{2}
\end{array}\right]
\end{align}
$$

### 1.4 SUR 估计

由于满足外生性条件 $E\left(\mu_{j}|X\right) = 0$，所以 OLS 估计是一致估计

由于不满足同方差假定，所以 OLS 估计不是最优的

已知误差项方差-协方差矩阵的形式，FGLS 估计是最优的一致估计

SUR 估计本质是 FGLS 估计，具体步骤如下：

1. OLS 估计，得到误差项方差-协方差矩阵 $\Sigma(\Omega)$ 的一致估计 $\hat{\Sigma}(\hat{\Omega})$

$$
\hat{\Sigma}=\frac{1}{n}\hat{\mu}_{ols}\hat{\mu}_{ols}^{\prime} 
\qquad
\hat{\Omega}=\hat{\Sigma} \otimes I_{n}
$$

2. 用 $\hat{\Omega}$ 替换 $\Omega $，GLS 估计

$$
\hat{\beta}_{SUR}=\hat{\beta}_{gls}(\hat{\Omega})=
(X^{\prime}\hat{\Omega}^{-1} X)^{-1}
X^{\prime}\hat{\Omega}^{-1}  y
$$

### 1.5 SUR 模型对 OLS 的优势

- 可以跨方程检验系数是否联合为 $0$，$\mathbb{H} \text{: } \beta_{j}=\beta_{k}=0$
- 可以跨方程检验系数是否相等，$\mathbb{H} \text{: } \beta_{j}=\beta_{k}$
- 可以先施加 $\beta_{j}=\beta_{k}$ 的约束，再进行 SUR 估计

## 2. Stata 实例

该实例的原作者为 **Ani Katchova**，完整数据和代码见文末。有基础的朋友也可以参考 Stata 自带的帮助文档中的实例。

### 2.1 模型设定

我们想要知道数学和阅读的成绩如何受其他因素影响，构建如下方程组：
$$
\begin{align}
math &= \alpha + \beta_{1}·female + \beta_{2}·prog + \beta_{3}·science + \mu_{1} \\
read &= \alpha + \gamma_{1}·female + \gamma_{2}·socst + \mu_{2}
\end{align}
$$
其中 **math** 是数学成绩，**read** 是阅读成绩，**female** 是性别的哑变量，**prog** 是教育项目，**science** 是自然科学成绩，**socst** 是社会科学成绩。

方程 1 的被解释变量是 **math**，解释变量是 **female**，**prog** 和 **science**。

方程 1 的被解释变量是 **read**，解释变量是 **female** 和 **scost**。

方程 1 和 2 共同的解释变量是 **female**。

### 2.2 比较 OLS 和 SUR

我们用 Stata 的 `sureg` 命令进行 SUR 估计，该命令的选项 `corr` 报告各方程误差项的相关系数矩阵，并对误差项之间的独立性执行 布伦斯-帕甘 (*Breusch*–*Pagan*) 检验，代码如下：

```Stata
* 对方程 1 和 2 单独进行 OLS 估计
reg math female prog science
reg read female socst

* 对方程 1 和 2 组成的方程组用 SUR 估计
sureg (math female prog science) (read female socst), corr
```

OLS 估计回归结果如下：

```Stata
      Source |       SS           df       MS      Number of obs   =       200
-------------+----------------------------------   F(3, 196)       =     43.81
       Model |  7010.50752         3  2336.83584   Prob > F        =    0.0000
    Residual |  10455.2875       196  53.3433035   R-squared       =    0.4014
-------------+----------------------------------   Adj R-squared   =    0.3922
       Total |   17465.795       199  87.7678141   Root MSE        =    7.3037

------------------------------------------------------------------------------
        math |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
      female |   .9656828   1.045894     0.92   0.357    -1.096968    3.028333
        prog |  -.4128426   .7638838    -0.54   0.589    -1.919329    1.093644
     science |   .5975687    .053712    11.13   0.000      .491641    .7034964
       _cons |   21.97077   3.582234     6.13   0.000      14.9061    29.03544
------------------------------------------------------------------------------


      Source |       SS           df       MS      Number of obs   =       200
-------------+----------------------------------   F(2, 197)       =     63.93
       Model |  8233.79028         2  4116.89514   Prob > F        =    0.0000
    Residual |  12685.6297       197  64.3940595   R-squared       =    0.3936
-------------+----------------------------------   Adj R-squared   =    0.3874
       Total |    20919.42       199  105.122714   Root MSE        =    8.0246

------------------------------------------------------------------------------
        read |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
      female |  -1.763517   1.141039    -1.55   0.124    -4.013735    .4867015
       socst |   .5978268   .0530589    11.27   0.000     .4931903    .7024632
       _cons |   21.86201   2.873858     7.61   0.000     16.19453    27.52948
------------------------------------------------------------------------------
```

SUR 估计回归结果如下：

```Stata
Seemingly unrelated regression
--------------------------------------------------------------------------
Equation             Obs   Parms        RMSE    "R-sq"       chi2        P
--------------------------------------------------------------------------
math                 200       3    7.256297    0.3971     111.11   0.0000
read                 200       2    7.981054    0.3910     112.68   0.0000
--------------------------------------------------------------------------

------------------------------------------------------------------------------
             |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
math         |
      female |   .8047752   1.035145     0.78   0.437    -1.224072    2.833622
        prog |     -.5303   .7433182    -0.71   0.476    -1.987177    .9265769
     science |   .5338002   .0524355    10.18   0.000     .4310285    .6365718
       _cons |   25.60272   3.493717     7.33   0.000     18.75516    32.45028
-------------+----------------------------------------------------------------
read         |
      female |  -1.708889   1.132408    -1.51   0.131    -3.928367    .5105893
       socst |   .5493215   .0519639    10.57   0.000     .4474741    .6511688
       _cons |   24.37415   2.817793     8.65   0.000     18.85138    29.89693
------------------------------------------------------------------------------
```

OLS 估计和 SUR 估计的结果总结：
$$
\begin{array}{|l|c|c|c|c|}
\hline & \text { OLS } & \text { OLS } & \text { SUR } & \text { SUR } \\
& \text { math score } & \text { reading score } & \text { math score } & \text { reading score } \\
\hline \text { Female } & 0.97 & -1.76 & 0.80 & -1.71 \\
\hline \text { Program } & -0.41 & - & -0.53 & - \\
\hline \text { Science score } & 0.59^{*} & - & 0.53^{*} & - \\
\hline \text { Social science setore } & - & 0.60^{*} & - & 0.55^{*} \\
\hline \text { Constant } & 21.97^{*} & 21.86^{*} & 25.60^{*} & 24.37^{*} \\
\hline
\end{array}
$$
本例中，OLS 估计的结果与 SUR 估计的结果定性一致，差异不大。

布伦斯-帕甘 (*Breusch*–*Pagan*) 检验的结果

```Stata
Correlation matrix of residuals:

        math    read
math  1.0000
read  0.1849  1.0000

Breusch-Pagan test of independence: chi2(1) = 6.835, Pr = 0.0089
```

方程 1 和 2 的误差项存在显著的相关性（chi2 值 6.385，P 值小于 0.01），但相关性并不强（相关系数仅 0.18）

### 2.3 检验与约束

方程 1 和 2 中 **female** 的系数都与 0 无显著差异，我们进一步检验两个方程中变量 **female** 的系数是否相等，代码如下：

```Stata
test [math]female = [read]female
```

检验结果如下：

```Stata
(1)  [math]female - [read]female = 0

             chi2(1) =    3.28
         Prob > chi2 =    0.0701
```

系数差异检验的 chi2 值为 3.28，P 值为 0.07，大于 0.05，无法拒绝原假设。因此，我们对方程 1 和 2 施加约束：$\beta_{1}=\gamma_{1}$，代码如下：

```Stata
constraint 1 [math]female = [read]female
sureg (math female prog science) (read female socst), constraints(1) 
```

回归结果如下：

```Stata
Seemingly unrelated regression
--------------------------------------------------------------------------
Equation             Obs   Parms        RMSE    "R-sq"       chi2        P
--------------------------------------------------------------------------
math                 200       3    7.284186    0.3924     109.08   0.0000
read                 200       2    8.013775    0.3860     109.60   0.0000
--------------------------------------------------------------------------

 ( 1)  [math]female - [read]female = 0
------------------------------------------------------------------------------
             |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
math         |
      female |  -.3152625   .8328986    -0.38   0.705    -1.947714    1.317189
        prog |  -.5481211   .7468853    -0.73   0.463    -2.011989    .9157471
     science |    .525379   .0524311    10.02   0.000     .4226158    .6281422
       _cons |   26.68586   3.453638     7.73   0.000     19.91686    33.45487
-------------+----------------------------------------------------------------
read         |
      female |  -.3152625   .8328986    -0.38   0.705    -1.947714    1.317189
       socst |   .5454426   .0521048    10.47   0.000     .4433191    .6475661
       _cons |    23.8179   2.814497     8.46   0.000     18.30159    29.33421
------------------------------------------------------------------------------
```

此时方程 1 和 2 中 **female** 的系数都相等，仍然显著不等于 0。

### 2.4 sureg 命令

基础语法

```Stata
sureg (depvar1 varlist1) (depvar2 varlist2) ...  (depvarN varlistN) 
      [if] [in] [weight] [, options]
```

进阶语法

```Stata
 sureg ([eqname1:] depvar1a [depvar1b ... =] varlist1 [, noconstant])
       ([eqname2:] depvar2a [depvar2b ... =] varlist2 [, noconstant])
       ...
       ([eqnameN:] depvarNa [depvarNb ... =] varlistN [, noconstant]) 
       [if] [in] [weight] [, options]
```

特别说明：当被解释变量 **depvar** 有多个时，不能指定方程名 **eqname**

一些重要的参数和选项

```Stata
*	参数
eqname                    方程
depvar                    被解释变量
varlist                   控制变量

*	约束
constraints(constraints)  应用指定的线性约束

*	自由度的调整
small                     报告小样本统计量
dfk                       使用小样本调整方法
dfk2                      使用替代的调整方法

*	最优化选项
isure                     迭代直到收敛
iterate(#)                指定最大的迭代次数
nolog					  抑制迭代过程的输出
tolerance(#)              指定数值收敛条件
```

此外，`sureg` 还可以与 `bootstrap`, `by`, `fp`, `jackknife`, `rolling`, and `statsby` 等前缀连用。

### 2.5 完整数据和代码

需要复现的朋友可以下载 [sur_scores.dta](https://pan.baidu.com/s/1Pgs0a14KbO7Bz0bYPGPihw 
)，提取码：9cap，链接永久有效

```Stata
cls
clear all
set more off

global path "C:\SUR\"		//	请在此处填入 sur_scores.dta 的路径
use "$path\sur_scores", clear

global y1list math					//	方程 1 的被解释变量
global y2list read					//	方程 2 的被解释变量
global x1list female prog science	//	方程 1 的解释变量
global x2list female socst			//	方程 2 的解释变量
global x1 female					//	方程 1 和 2 共同的解释变量

describe $y1list $y2list $x1list $x2list
summarize $y1list $y2list $x1list $x2list

* 对方程 1 和 2 单独进行 OLS 估计
reg $y1list $x1list
reg $y2list $x2list

* 对方程 1 和 2 组成的方程组用 SUR 估计
sureg ($y1list $x1list) ($y2list $x2list), corr

* 检验跨方程约束，方程 1 和 2 中变量 female 的系数是否相等
test [$y1list]$x1 = [$y2list]$x1

* 对 SUR 估计施加跨方程约束
constraint 1 [$y1list]$x1 = [$y2list]$x1
sureg ($y1list $x1list)($y2list $x2list), constraints(1) 
```

### 引用

[Econometrics Academy: SUR ](https://sites.google.com/site/econometricsacademy/econometrics-models/seemingly-unrelated-regressions)

[Zellner, Arnold. "An efficient method of estimating seemingly unrelated regressions and tests for aggregation bias." *Journal of the American statistical Association* 57.298 (1962): 348-368.](https://www.tandfonline.com/doi/abs/10.1080/01621459.1962.10480664)

[Zellner, A., and D. S. Huang. 1962. Further properties of efficient estimators for seemingly unrelated regression equations. International Economic Review 3: 300–313.](https://www.jstor.org/stable/2525396?seq=1)

[Zellner, Arnold. "Estimators for seemingly unrelated regression equations: Some exact finite sample results." *Journal of the American Statistical Association* 58.304 (1963): 977-992.](https://amstat.tandfonline.com/doi/abs/10.1080/01621459.1963.10480681#.XstP4zPiuUk)

[Breusch, T. S., and A. R. Pagan. 1980. The Lagrange multiplier test and its applications to model specification in econometrics. Review of Economic Studies 47: 239–253.](https://www.jstor.org/stable/2297111?seq=1)<br />

陈强. 高级计量经济学及 Stata 应用[M]. 高等教育出版社, 2014.    
 
Greene, William H. (2005). Econometric Analysis. Fifth edition. Pearson Education.     
  
[Stata FAQ: What is seemingly unrelated regression and how can I perform it in Stata?](https://stats.idre.ucla.edu/stata/faq/what-is-seemingly-unrelated-regression-and-how-can-i-perform-it-in-stata/)  
  
[Stata Code Fragments: Fitting a seemingly unrelated regression(sureg)manually](https://stats.idre.ucla.edu/stata/code/fitting-a-seemingly-unrelated-regression-sureg-manually/)
