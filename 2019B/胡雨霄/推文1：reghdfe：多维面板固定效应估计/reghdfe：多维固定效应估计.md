> &emsp;          
> 作者：胡雨霄 (伦敦政治经济学院)         
> &emsp;          
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)
> &emsp;  


- [连享会推文集锦](https://www.jianshu.com/p/de82fdc2c18a) 
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)
[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/0802/174601_3b077969_1522177.png)](https://gitee.com/arlionn/Course/blob/master/README.md)



&emsp;
>  [连享会计量方法专题……](https://gitee.com/arlionn/Course/blob/master/README.md)
>  [连享会#空间计量专题 (成都)](https://gitee.com/arlionn/Course/blob/master/2019Spatial.md)

&emsp;

实证分析中，我们经常需要控制各个维度的个体效应，以便尽可能减轻 **遗漏变量** 导致的偏误。在最常用的二维面板数据中，我们通常会采用 `xtreg y x i.year, fe` 的形式来控制 **公司个体效应** 和 **年度效应**。然而，在有些情况下，我们需要对三维甚至更高维度的数据进行分析 (例如，**公司-年度-高管**，**省份-城市-行业-年度**)，此时，一方面要考虑估计的可行性，另一方面还需兼顾计算速度问题。

本文介绍的 `reghdfe` 命令可以很好地达成上述目的。`reghdfe`  主要用于实现多维固定效应线性回归。该命令类似于 `areg` 及 `xtreg,fe`，但允许引入多维固定效应。此外，该命令在运行速度方面远远优于 `areg` 及 `xtreg`， 因此倍受研究者青睐。

本文对该命令的介绍基于 [A Feasible Estimator for Linear Models with Multi-Way Fixed Effects (Correia, 2016 )](http://scorreia.com/research/hdfe.pdf)。

### **1. 命令的安装**

 ```
ssc install reghdfe, replace ///安装命令
```

### **2. 命令的语法**

该命令的具体语法如下：

```stata
reghdfe depvar [indepvars] [if] [in] [weight], absorb(absvars) [options]
```
其中，
- `depvar`: 因变量
- `indepvars`: 解释变量
- `absorb(absvars) `：引入固定效应
  - 可以包含多维固定效应，即 `absorb (var1,var2,var3,...)`。若想保存对某变量的固定效应，则运行命令`absorb (var1,var2,FE3=var3)` ， 变量 **FE3** 将保存对 **var3** 的固定效应估计结果。 
  - 可以包含不同效应间的交互影响，即`absorb(var1#var2)`。

值得注意的是，`reghdfe` 允许定类变量 (categorical variable) 与连续性变量 (continuous variable) 进行交互，即 `absorb(i.var1#c.var2)` 。实证中很少引入这样的交互项。但如果对该问题感兴趣，可参考 Duflo (2014) 。

## 3. 命令的操作

这一部分用两个实证的例子介绍如何运用 `reghdfe` 。

### 3.1 估计双重差分的固定效应模型（DID)

该命令可用于估计双重差分的固定效应模型（DID）。过去推文[Stata: 双重差分的固定效益模型](https://www.jianshu.com/p/e97c1dc05c2c?utm_campaign=hugo&utm_medium=reader_share&utm_content=note&utm_source=weixin-friends&from=singlemessage&isappinstalled=0)列举了用于估计 DID 模型的三个命令：`reg`,  `areg`, 以及`xtreg`。`reghdfe` 也可实现同样的估计结果，而且运行速度优于其他命令。

使用的数据请参考之前推文[Stata: 双重差分的固定效应模型](https://www.jianshu.com/p/e97c1dc05c2c?utm_campaign=hugo&utm_medium=reader_share&utm_content=note&utm_source=weixin-friends&from=singlemessage&isappinstalled=0)。该数据模拟的情况为，政策冲击发生在 $t = 14$ 时，对照组为 $i = 1$，控制组为 $ i = 0$。模型为 $ y = 0.3 + 0.19 \times i + 1.67 \times d + 0.56 \times i \times d + e $。

```
. set obs 400
. gen firm=_n  ///生成企业数量
. expand 24
. bysort firm: gen t=_n  ///时间跨度设定为24个季度（6年）
. gen d=(t>=14)
. label var d "=1 if post-treatment" ///设定事件冲击发生在第14期
. gen r=rnormal()
. qui sum r, d
. bysort firm: gen i=(r>=r(p50)) if _n==1
. bysort firm: replace i=i[_n-1] if i==. & _n!=1 ///设定处理组和对照组
. drop r
. label var i "=1 if treated group, =0 if untreated group" 
. gen e = rnormal() ///设定随机变量
. label var e "normal random variable"
. gen y = 0.3 + 0.19*i + 1.67*d + 0.56*i*d + e ///模型设置
```

首先，回顾双重差分模型的设定形式，

$$ 
y_{it} = \alpha + \beta (G_i \times D_i) + \mu_i + \lambda_t + \epsilon_{it} 
$$

其中，$G_i$ 为分组虚拟变量（处理组=1，控制组=0）；$D_i$ 为分期虚拟变量（政策实施后=1，政策实施前=0）；交互项 $G_i \times D_i $ 表示处理组在政策实施后的效应。$\mu_i$ 与 $\lambda_t$ 分别为个体固定效应和时间固定效应。

具体用于估计政策冲击对公司的影响的命令如下。

```stata
gen did = i*d ///生成交互项
reghdfe y did, absorb(firm t) vce(cluster firm)
```

变量 **did** 即为交互项，其系数为双重差分模型重点考察的处理效应。命令 `absorb(firm t)` 同时引入了公司固定效应以及时间固定效应。结果如下。

```
. reghdfe y did, absorb(firm t) vce(cluster firm)
(MWFE estimator converged in 2 iterations)

HDFE Linear regression                            Number of obs   =      9,600
Absorbing 2 HDFE groups                           F(   1,    399) =     175.80
Statistics robust to heteroskedasticity           Prob > F        =     0.0000
                                                  R-squared       =     0.5102
                                                  Adj R-squared   =     0.4875
                                                  Within R-sq.    =     0.0198
Number of clusters (firm)    =        400         Root MSE        =     1.0043

                                 (Std. Err. adjusted for 400 clusters in firm)
------------------------------------------------------------------------------
             |               Robust
           y |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
         did |   .5656247   .0426601    13.26   0.000     .4817581    .6494914
       _cons |   1.143579   .0084565   135.23   0.000     1.126954    1.160204
------------------------------------------------------------------------------

Absorbed degrees of freedom:
-----------------------------------------------------+

 Absorbed FE | Categories  - Redundant  = Num. Coefs |
-------------+---------------------------------------|
        firm |       400         400           0    *|
           t |        24           0          24     |
-----------------------------------------------------+
* = FE nested within cluster; treated as redundant for DoF computation

```


### 3.2 估计多维固定效应的线性模型（复制一篇 AER 论文）

这一小节将介绍如何运用 `reghdfe` 估计多维固定效应的线性模型。American Economic Review一篇文章，The Costs of Patronage: Evidence from the British Empire (Xu, 2018)， 提供的可供复制的[代码](https://www.aeaweb.org/articles?id=10.1257/aer.20171339)中出现了大量 `reghdfe` 命令。本小节介绍该作者如何用 `reghdfe` 命令输出其文章Table 2第六列的结果。


![Source: Xu, G. (2018). The Costs of Patronage: Evidence from the British Empire. American Economic Review, 108 (11): 3170-98.](https://images.gitee.com/uploads/images/2019/0624/203159_08d3a30c_5089004.png)


作者在这篇文章中想要探究 **任命制** (patronage) 对英国 **政治体系** 的影响。具体于 Table2，作者意图研究社会联系（social connections) 是否会影响政府官员的工资水平。Table 2中，第六列所估计的回归为：

$$log w_{ist} = \beta \times c_{it } + \theta_i + \omega_i + x_{it}'\gamma + \tau_t + \epsilon_{ist}$$

其中，
- $logw_{ist}$为政府官员 $i$ 于时间 $t$ 在 $j$ 州执政时的对数工资水平。Stata 命令中，该变量名为 **log_salary_governor_gbp**
- $c_{it}$ 为虚拟变量（Dummy Variable)，当政府官员与其上任官员存在社会联系时，该变量取1。如若不然，则取0。社会联系包括：共同祖先，贵族身份以及教育背景。Stata 命令中，该变量名为 **connected**
- $\theta_i$ 为政府官员固定效应。该部分的设置为了解决政府官员的异质性 (heterogeneity) 问题。例如，具有较强能力的政府官员更有可能建立更多的社会关系。Stata 命令中, **aid** 为不同官员的 unique ID 变量。
- $\omega_i$ 为政府官员执政时长固定效应。设置该部分是因为，执政时间的长短可能也会对社会关系产生影响。Stata 命令中, **duration** 为官员执政时长变量。
- $x_{it}'$ 为控制变量。作者选用了执政者在历史上执政过的州的数目。Stata 命令中，该变量名为 **no_colonies** 。
- $\gamma_t$ 为年份固定效应。该部分的设置是为了吸收执政者们在不同时期受到的共同时间冲击。Stata 命令中, **year** 为年份变量。
- $\epsilon_{ist}$ 为残差。作者使用了聚类标准误的方法。

该回归的原假设为，$H_0$: 社会联系 (**connected**) 与政府官员的工资水平 (**log_salary_governor_gbp**) 无关。若 **connected** 的系数 $\beta$ 不显著，则不拒绝原假设。若 $\beta$ 显著，则拒绝原假设，并可以判定社会联系对政府官员的工资水平显著相关。

用 Stata 实现该回归的命令如下。

```stata
reghdfe log_salary_governor_gbp no_colonies connected, ///
        absorb(aid year duration) vce(cluster bilateral)
```

其中，`absorb(aid year duration)` 同时引入了官员固定效应、时间固定效应以及执政时长固定效应。

命令运行后的结果如下所示。数据请于AER[下载](https://www.aeaweb.org/articles?id=10.1257/aer.20171339)。

```stata
. quietly use "analysis.dta", replace
. reghdfe log_salary_governor_gbp no_colonies connected, ///
          absorb(aid year duration) vce(cluster bilateral)
(MWFE estimator converged in 26 iterations)

HDFE Linear regression                            Number of obs   =      3,510
Absorbing 3 HDFE groups                           F(   2,   1517) =      25.45
Statistics robust to heteroskedasticity           Prob > F        =     0.0000
                                                  R-squared       =     0.9255
                                                  Adj R-squared   =     0.9109
                                                  Within R-sq.    =     0.0978
Number of clusters (bilateral) =      1,518       Root MSE        =     0.2374

                          (Std. Err. adjusted for 1,518 clusters in bilateral)
------------------------------------------------------------------------------
             |               Robust
log_salary~p |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
 no_colonies |   .2234767   .0347473     6.43   0.000     .1553189    .2916346
   connected |   .0972969   .0355508     2.74   0.006     .0275628    .1670309
       _cons |   7.485619    .065766   113.82   0.000     7.356617    7.614621
------------------------------------------------------------------------------

Absorbed degrees of freedom:
-----------------------------------------------------+
 Absorbed FE | Categories  - Redundant  = Num. Coefs |
-------------+---------------------------------------|
         aid |       456           0         456     |
        year |       110           1         109     |
    duration |         7           1           6    ?|
-----------------------------------------------------+
? = number of redundant parameters may be higher

```

上述结果表明，变量 **connected** 的系数为 0.097, 标准误为 0.036。这说明该变量在 1% 的水平上显著大于 0 。其经济学含义为，与上一任官员存在社会联系的官员，相较于无社会联系的官员，工资水平要高出 9.7%。也就是说，官员的工资水平和其社会关系显著相关。


### **4.结语**

这篇推文主要介绍了如何在实证中运用 `reghdfe`.具体而言，本推文列举了两个例子。其一，为运用该命令对 DID 模型进行估计。其二，为运用该命令进行多维固定效应线性模型的估计。

## **文献来源**

[1] Correia, S. (2016). Linear Models with High-Dimensional Fixed Effects: An Efficient and Feasible Estimator, Working Paper. [[PDF]](http://scorreia.com/research/hdfe.pdf)
[2] Duflo, E. (2004). The medium run effects of educational expansion: Evidence from a large school construction program in Indonesia. *Journal of Development Economics*, 74(1), 163-197. [[PDF]](https://www.nber.org/papers/w8710.pdf)
[3] Xu, G. (2018). The Costs of Patronage: Evidence from the British Empire. *American Economic Review*, 108 (11): 3170-98. [[PDF]](http://guoxu.org/docs/empireJMP_Xu.pdf)


&emsp;
> #### [连享会计量方法专题……](https://gitee.com/arlionn/Course/blob/master/README.md)

&emsp;


>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。您也可以从 [连享会选题平台](https://gitee.com/Stata002/StataSX2018/wikis/Home) &rarr; [002_备选主题] 中选择感兴趣的题目来撰写推文。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表](https://www.jianshu.com/p/de82fdc2c18a) 
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/0802/174601_beb1d1b2_1522177.png "连享会(公众号: StataChina)推文列表")](https://gitee.com/arlionn/Course/blob/master/README.md)





---
![欢迎加入Stata连享会(公众号: StataChina)](https://images.gitee.com/uploads/images/2019/0802/174601_0b5a936e_1522177.jpeg "扫码关注 Stata 连享会")
