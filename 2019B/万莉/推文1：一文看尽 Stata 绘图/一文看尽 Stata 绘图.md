> 作者：万莉 (北京航空航天大学)       
> 连享会：([知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn))  

&emsp;


> #### [2019暑期Stata现场班，7.17-26日，北京，连玉君+刘瑞明 主讲](https://gitee.com/arlionn/Course/blob/master/2019%E6%9A%91%E6%9C%9FStata%E7%8E%B0%E5%9C%BA%E7%8F%AD.md)


&emsp;

[![2019暑期Stata现场班，7.17-26日，北京，连玉君+刘瑞明 主讲](https://images.gitee.com/uploads/images/2019/0704/090623_05c4fd96_1522177.png)](https://gitee.com/arlionn/Course/blob/master/2019%E6%9A%91%E6%9C%9FStata%E7%8E%B0%E5%9C%BA%E7%8F%AD.md)
  

&emsp;


### 引言

数据可视化在数据分析中扮演着重要的角色。基于由浅入深的原则，本推文结合 Stata 范例，梳理了 Stata 的绘图命令，便于读者掌握其中规律。用图说话，就从这里开始！

### 1. 绘图简介

Stata 提供各种绘图命令，包括点 (scatter)、线 (line)、面 (area)，直方图 (histogram)、条形图 (bar)、饼图 (pie)、函数曲线 (function) 以及矩阵图 (matrix) 等。对时间序列数据有以 ts 开头的一系列特殊命令，如 tsline。对面板数据有以 xt 开头的特殊命令，如 xtline。还有一类是对双变量的回归拟合图 (lfit、qfit 、lowess 等)。具体内容可参考帮助命令 `help graph` 和  `help twoway`。

#### 1.1 绘图区域

在动手绘图之前，我们先了解 Stata 的绘图区域划分，借用官方的一幅图说明：

![region.png](https://images.gitee.com/uploads/images/2019/0704/090623_d087446e_1522177.png)

绘图区域分为： graph 区域（包括标题与副标题、图例、脚注等）和 plot 区域（包括曲线等）两大区域，而两大区域内又分为 inner 和 outer 两个隔开的区域，这四个区域都有相应的控制命令。（可参考 `help region_options`）

#### 1.2 语法结构

```stata
graph-command (plot-command, plot-options) (plot-command, plot-options) (...), graph-options
```

或者

```stata
graph-command plot-command, plot-options || plot-command, plot-options || ..., graph-options
```

具体说明如下：

- `graph-command` 定义图的类型
- `plot-command` 定义曲线类型（比如点、线、面等）
- 同一个图中如果有多条曲线，可以用 “()” 分开， 也可以用 “||” 分开。
- 不同的图类有其自身的选项，而整个图也有相应的选项。 

例如: `twoway` 为 `graph-command` 中的一个子命令，而 `scatter` 则为 `plot-command` 的子命令，则可写成 `graph twoway scatter mpg weight`，亦可简写为 `twoway scatter mpg weight` 或 `scatter mpg weight`。

**注意：** 以 `()` 或 `||` 分隔的 **plot-command**, **plot-options** 可看做一个图层。多个图层可进行叠加。

#### 1.3 一个简单的例子

本小节将借助一个简单的例子，介绍 Stata 绘图的常用设定和选项。

```stata
/* 一个简单的例子 */
cd "D:\推文：一文看尽Stata绘图\Prog" 
// 设置工作路径
sysuse sp500, clear // 导入软件自带数据文件
#d ;
twoway (line high date) (line low date), 
 title("这是图选项：标题", box) 
 subtitle("这是副标题""图1：股票最高价与最低价时序图")  
 xtitle("这是 x 轴标题：交易日期", margin(medsmall))
 ytitle("这是 y 轴标题：股票价格") 
 ylabel(900(200)1400) ymtick(##5)  
 legend(title("图例")label(1 "最高价") label(2 "最低价"))  
 note("这是注释：数据来源于 Stata 公司") 
 caption("这是说明：欢迎加入 Stata 连享会!") 
 saving(myfig.gph, replace); 
#d cr
/*一些解释：
line 是曲线选项，表示线图
设置副标题时，用 "" 达到换行效果
ylabel 设置 y 轴标签及刻度  
saving() 表示保存图像
#d ;  #d cr 表示断行，也可以用 /// 
*/
```

命令运行结果，如下图：

![这只是个栗子.png](https://images.gitee.com/uploads/images/2019/0704/114328_7f27f6dc_3042808.png)

#### 1.4 图形管理

本小节将简单介绍如何保存、合并、导出图形。

```stata
/* 图形管理 */

*- 图形保存
sysuse sp500, clear
twoway line high low date, ///
  saving(fig1.gph, replace) // 保存方式1
graph use fig1.gph // 重现图形

twoway line high low date, scheme(s1mono)
graph save fig2.gph, replace 
// 保存方式2，scheme 是设定绘图模板

*- 图形合并
graph combine fig1.gph fig2.gph, col(1) 
// 以单列形式合并

help graph combine

*- 图形导出
graph export "fig3.png", replace 
// 导出格式有 png，tif，wmf 等
graph export "fig4.png", ///
  width(3200) height(1800) replace 
/* 调整输出图片的分辨率，
仅适用于.png 和 .tif 格式的图片*/

help graph export
```

### 2. 绘图范例

本节将结合 [Intro to data visualization](https://dss.princeton.edu/training/Visual101.pdf) 提供的数据及代码，具体介绍几种常用图形。此外，每小节结构为：指出基本命令--重点强调某几个选项 (option)--Stata 范例（代码+注释）。

#### 2.1 初识数据

```stata
******* 让我们开始吧
use "http://www.princeton.edu/~otorres/wdipol.dta", clear  // 导入数据
browse

describe
summarize
/* 了解数据结构
year:    年份
country: 国家名称
gdppc:   人均GDP
unempf:  女性失业率（%）
unempm:  男性失业率（%）
unemp:   失业率（%）
export:  出口额
import:  进口额
polity:  Polity IV数据库中的政体类型变量（polity）,
         衡量政体的民主程度，
         数值越大表示政体民主程度越高。
polity2: Polity IV数据库中的政体类型变量（polity2）,
         衡量政体的民主程度，
         数值越大表示政体民主程度越高。
trade:   进出口总额（进口+出口） 
id:      可通过命令group(country)得到，
         将国家名称与数字相对应
*/
```

#### 2.2 线图

基本命令： `line` 或 `twoway line` 或 `twoway connected`

强调选项：
- `lpattern(...)` 改变线条类型(如实线、虚线、点线等)。
    - `lpattern(solid)` 表示将线条定义为实线。
    - 可以借助命令 `palette linepalette` 查看线型及对应代号。
- `msymbol(...)` 改变标记符号(如实心圆圈、实心菱形、实心正方形)。
    - `msymbol(o)` 表示将标记符号定义为实心小圆圈。
    - 可以借助命令 `palette symbolpalette` 查看标记符号及对应代号。

范例如下：

```stata
******* 线图
line unemp unempf unempm year 
     if country=="United States" 
// 利用 if 条件，只画美国失业率的时序图

summarize unemp unempf unempm
replace unemp=. if unemp==0
replace unempf=. if unempf==0
replace unempm=. if unempm==0 
// 将变量为0的值变成缺失值
summarize unemp unempf unempm
line unemp unempf unempm year ///
     if country=="United States"

twoway line unemp unempf unempm year ///
  if country=="United States", ///
  title("Unemployment rate in the US, 1980-2012") /// 
  legend(label(1 "Total") ///
         label(2 "Females") ///
         label(3 "Males")) ///
  lpattern(solid dash dot) ///
  ytitle("Percentage")
/*
命令依次为：指定画线形图、添加标题、
调整图例、改变线条类型、添加y轴标题
*/
	   	   
twoway connected unemp unempf unempm year 
  if country=="United States", ///
  title("Unemployment rate in the US, 1980-2012") ///
  legend(label(1 "Total") ///
         label(2 "Females") ///
         label(3 "Males")) ///
  msymbol(circle diamond square) ///
  ytitle("Percentage")
/*
命令依次为：指定点连线图、添加标题、调整图例、
改变点的标记符号（实心圆圈、实心菱形、实心正方形）、
添加y轴标题。
标记符号的各种代号可参考 help symbolstyle。 
*/	   
	   
twoway connected unemp year 
       if country=="United States" | ///
          country=="United Kingdom" | ///
          country=="Australia" | ///
          country=="Qatar", ///
       by(country, title("Unemployment")) ///
       msymbol(circle_hollow)
	   
twoway connected unemp year 
       if country=="United States" | ///
          country=="United Kingdom" | ///
          country=="Australia" | ///
          country=="Qatar", ///
       by(country) ///
       title("Unemployment") ///
       msymbol(circle_hollow)	   
/*
利用by(varname)同时做多个图。 
注意：by(varname)功能适用于 matrix 和 star 以外所有图形,
该选项使 graph 按照指定的分组变量分别绘制图形。
title("")放在by()里面，画出的图共用一个标题；
title("")放在by()外面，每个图一个标题
*/

twoway (connected unemp year 
          if country=="United States", ///
          msymbol(diamond_hollow)) ///
       (connected unemp year 
          if country=="United Kingdom", ///
          msymbol(triangle_hollow)) ///
       (connected unemp year 
          if country=="Australia", ///
          msymbol(square_hollow)) ///
       (connected unemp year ///
          if country=="Qatar", ///
       title("Unemployment") ///
       msymbol(circle_hollow) ///
       legend(label(1 "USA") label(2 "UK") ///
              label(3 "Australia") label(4 "Qatar")))
/*
将四个国家的失业率时序图放在同一张图里
*/	   

twoway connected gdppc year if gdppc>40000, ///
       by(country) msymbol(diamond)
/* 保留人均GDP高于40000美元的子样本，再按国家分组绘制时序图*/


bysort year: egen gdppc_mean=mean(gdppc)
bysort year: egen gdppc_median=median(gdppc)
// 利用bysort命令，计算出人均GDP每年的平均数和中位数
twoway connected gdppc gdppc_mean year ///
       if country=="United States" | ///
          country=="United Kingdom" | ///
          country=="Australia" | ///
          country=="Qatar", ///
       by(country, title("GDP pc (PPP, 2005=100)")) ///
       legend(label(1 "GDP-PC") ///
              label(2 "Mean GDP-PC")) ///
       msymbol(circle_hollow)
/*
将四个国家的人均GDP与平均水平进行比较
*/

help twoway line // 查看线图的帮助文件
help twoway connected // 查看点线图的帮助文件

palette symbolpalette // 图示标记符号及对应代号
palette linepalette // 图示线型及对应代号
palette color green // 图示颜色
help palett
```

命令运行的结果，部分见下图：

![线图-部分栗子.png](https://images.gitee.com/uploads/images/2019/0704/114419_744ea51d_3042808.png)

#### 2.3 条形图

基本命令： `graph hbar` 或 `graph bar`

强调选项：
- `over(...)` 表示按什么变量进行分组。
    - 有几个 `over`，就分几层。若有多个 `over`，出现顺序不一样，画出的图就不同。
    - 比如，`over(var, sort(#) bargap(#) bar(#, color(red)))` 表示按变量 var 进行分组，根据第 # 个变量的柱体高度进行升序排列。
      若要降序排列，命令则为 `over(var, sort(#) descending)`。
    - `bargap(#)` 设定组内条形之间的空隙大小；默认为 bargap(0)，表示组内条形之间紧密相连。
       若设置参数为 `bargap(20)`，则表示组内条形之间所留的空隙为条形宽度的20%。
    - `bar(#, color(red))` 单独设置第 # 个柱体的颜色。 
    - [可参考：教你如何一步一步绘制漂亮的分组条形图](http://www.sohu.com/a/218200922_697896)
 
范例如下：

```stata
*******  条形图
graph hbar (mean) gdppc 
// 绘制横向条形图，平均值mean选项是默认值
graph hbar (mean) gdppc, ///
      over(country, sort(1) descending)
graph hbar (mean) gdppc, ///
      over(country, sort(1) ///
      descending label(labsize(*0.5)))

graph hbar (mean) gdppc (median) gdppc ///
      if gdppc>40000, ///
      over(country, sort(1) descending ///
           label(labsize(*1))) ///
      legend(label(1 "GDPpc (mean)") ///
             label(2 "GDPpc (median)"))
/*
over() 设定分组变量，这里表示按国家分组
sort(1) 选项表示根据第一个变量，
        即 gdppc 的柱体高度进行升序排列
descending 表示降序排列
labsize(*0.5) 表示标签字体大小缩放 0.5 倍
*/

help graph bar // 查看条形图的帮助文件
```

命令运行的结果，部分见下图：

![条形图-部分栗子.png](https://images.gitee.com/uploads/images/2019/0704/114441_32d3d915_3042808.png)

#### 2.4 箱型图

基本命令： `graph hbox` 或 `graph box` 

强调选项：
- `marker(...)` 设定如何标记箱型图的异常值。
    - 比如 `marker(#, msymbol(Oh) msize(small))` 以较小的空心圆 (Oh)，标识出第 # 个变量的特异值。

范例如下：


```stata
******* 箱形图
sort id year  // 排序，规范样本
recode polity2 (-10/-6=1 "Autocracy") ///
               (-5/6=2 "Anocracy") ///
               (7/10=3 "Democracy") ///
               (else=.), ///
               gen(regime) label(polity_rec)
/*
利用recode命令，将 polity2 转换为类别变量regime:
1 "威权政体" if -10<=polity2<=-6
2 "中间政体" if -5<=polity2<=6
3 "民主政体" if 7<=polity2<=10
*/
			   
tab regime 
// 对类别变量 regime 列表统计，结果包括频数，频率
tab regime, nolabel 
// 列表统计，不显示类别变量的标签
tab country regime // 二维列表
tab country regime, row 
// row 选项表示行末增加 Total 统计量

help tab

graph hbox gdppc // 绘制横向箱型图
graph hbox gdppc if gdppc<40000
graph box gdppc, over(regime) yline(4517.94) ///
                 marker(1, msymbol(Oh) msize(small))
/*
over(regime) 表示按regime（政体类型）分组
yline(4517.94) 添加附加线，即 y=4517.94 的直线
marker(1, msymbol(Oh) msize(small)) 以较小的空心圆，标识出第1个变量的特异值
*/

help graph box // 查看箱型图的帮助文件
```

命令运行的结果，部分见下图：

![箱型图-部分栗子.png](https://images.gitee.com/uploads/images/2019/0711/224223_dcafcb65_3042808.png)

#### 2.5 散点图

基本命令： `scatter` 或 `twoway scatter` 

强调选项：
- `mlabel(varname)` 给散点添加文字标签。
    - 比如 `mlabel(country)` 表示标上国家名称，注意 country 是已设定好的文字变量。
- `msymbol(...)` 改变标记符号(如实心圆圈、实心菱形、实心正方形)。
    - 比如，`msymbol(o)` 表示将标记符号定义为实心小圆圈。
    - 可以借助命令 `palette symbolpalette` 查看标记符号及对应代号。

范例如下：

```stata
******* 散点图
scatter import export // 进口额与出口额的散点图

twoway (scatter import export ///
        if export>1000000, mlabel(country)) ///
       (scatter import export), legend(off)
	   
twoway (scatter import export) ///
       (scatter import export ///
       if export>1000000, mlabel(country)), legend(off)
/*
mlabel(country) 标上国家名称
legend(off) 不显示图例
注意： 理解图层的概念，图层2会覆盖图层1。
上述两个 twoway 命令由于图层顺序不一样，
画出的图的效果不一样。
*/			   

twoway (scatter import export, ///
        ytitle("Imports") xtitle("Exports")) ///
       (scatter import export ///
       if export>1000000, mlabel(country) legend(off)) ///
       (lfit import export, ///
       note("Constant values, 2005, millions US$"))
/*lfit 线性回归拟合图*/
	   
capture bysort year: egen gdppc_mean=mean(gdppc)
// 利用bysort命令，计算出人均GDP每年的平均数;
// capture避免程序因错误而中断

twoway (scatter gdppc year, jitter(13)) ///
       (connected gdppc_mean year, ///
        msymbol(diamond)) , ///
        xlabel(1980(1)2012, angle(90))
/*
jitter(#) 选项表示添加白噪声数据点，
数值越大，添加的的白噪声越多；
xlabel(1980(1)2012) 设定横坐标刻度标签，
x轴的刻度从1980开始，到2012结束，每隔1添加一个刻度；
angle(90) 设定刻度标签的角度
*/
	   	   
help twoway scatter // 查看散点图的帮助文件
```

命令运行的结果，部分见下图：

![散点图-部分栗子.png](https://images.gitee.com/uploads/images/2019/0704/114549_c28a6d76_3042808.png)

#### 2.6 矩阵图

基本命令： `graph matrix`

强调选项：
- `half` 表示只显示矩阵的一半，即左下角。
- `maxes(...)` 或 `maxis(...)`，两者等价，可用来控制 y 轴、x 轴刻度标签。
    - 比如 `maxes(ylabel(none) xlabel(none))` 表示不显示 y 轴、x 轴刻度及标签。
    - 比如 `maxes(ylabel(, nolabels))` 不显示 y 轴标签，但显示刻度。
    - 比如 `maxes(ylabel(,grid) xlabel(,grid))` y 轴、x 轴均增加网格线。

范例如下：

```stata
******* 矩阵图
graph matrix gdppc unemp unempf unempm ///
             export import trade polity2, ///
             maxis(ylabel(none) xlabel(none))

graph matrix gdppc unemp unempf unempm ///
             export import trade polity2, ///
             half maxis(ylabel(none) xlabel(none))
/*
graph matrix 命令用矩阵的形式
同时画出多个变量之间的相互关系。
比如，第 1 行第 1 列的图表示人均 GDP 和失业率的关系。
half 表示只显示矩阵的一半，即左下角。
maxis(ylabel(none) xlabel(none))
表示不显示 y 轴、x 轴刻度及标签。
*/
			 
help graph matrix // 查看矩阵图的帮助文件
```

命令运行的结果，部分见下图：

![矩阵图-部分栗子.png](https://images.gitee.com/uploads/images/2019/0704/114617_462414f9_3042808.png)

#### 2.7 直方图

基本命令： `histogram` 或 `hist` 或 `twoway histogram` 或 `twoway hist` 

强调选项：
- `bin(#)` 指定分为 # 个组别。
    - 比如 `bin(10)` 将连续变量分为10个组别。
- `barwidth(#)` 控制直方图柱子的宽度。
- `fcolor(...)` 设定柱子的填充颜色。
    - 比如 `fcolor(none)` 表示无填充颜色。
    - 可通过外部命令 `palette_all` 查看颜色代号。运行 `ssc install palette_all` 即可下载该外部命令。
    - 可通过外部命令 `full_palette` 查看 66 种颜色及其 RGB 代码。运行 `ssc install full_palette` 即可下载该外部命令。
- `lcolor(...)` 设定柱子的轮廓颜色。   
    - 比如 `lcolor(black)` 设定柱子的轮廓颜色为黑色。    
- `lwidth(...)` 设定外边缘线的宽度。
    - 比如 `lwidth(medium)` 设定外边缘线的宽度为适中。
    - 可通过命令 `graph query linewidthstyle` 列示线宽代号。
- `lpattern(...)` 设定外边缘线的类型。
    - 比如 `lpattern(dash)` 设定外边缘线为虚线。
    - 可通过命令 `palette linepalette` 图示线型代号。

范例如下：

```stata
******* 直方图
hist gdppc // 频率分布
hist gdppc, frequency // 频数分布 
hist gdppc, kdensity 
// 将直方图和核密度曲线绘制在一起
hist gdppc, kdensity normal 
// 同时显示频率直方图、核密度曲线和正态分布图
hist gdppc, kdensity normal bin(20) 
// bin(#)指定分为几个组别
hist gdppc if country=="United States" | ///
              country=="United Kingdom", ///
              bin(10) by(country)
// 利用by(varname)设定分组，同时画多个图。 

twoway hist gdppc ///
         if country=="United States", bin(10) || ///
       hist gdppc ///
         if country=="United Kingdom", bin(10) ///
       fcolor(none) lcolor(black) ///
       lwidth(medium) lpattern(dash) ///
       legend(label(1 "USA") label(2 "UK"))
/*
fcolor(none) 设定柱子的填充颜色，none表示无填充颜色
lcolor(black) 设定柱子的轮廓颜色
legend()设置图例
lwidth(medium) 设定外边缘线的宽度
lpattern(dash) 设定外边缘线的类型
*/

help linewidthstyle   
help linepatternstyle
help hist // 查看直方图的帮助文件
```

命令运行的结果，部分见下图：

![直方图-部分栗子.png](https://images.gitee.com/uploads/images/2019/0704/115020_f25b5f79_3042808.png)

#### 2.8 面板数据时间趋势图

基本命令： `xtline` 

范例如下：

```stata
******* 面板数据时间趋势图
*xtset country year 
// 会报错，'country' 为字符串变量
encode country, gen(country1) 
xtset country1 year // 声明数据是面板数据

xtline gdppc 
xtline gdppc if gdppc>39000, overlay 
// overlay 将所有国家放在同一图中

help xtline // 查看面板数据时间趋势图的帮助文件
```

#### 2.9 点图

基本命令： `graph dot` 

范例如下：

```stata
******* 点图
graph dot (mean) gdppc if gdppc>40000, ///
          over(country, sort(1) descending)

graph dot (mean) gdppc (median) gdppc ///
          if gdppc>40000, ///
  over(country, sort(1) descending label(labsize(*1))) ///
  legend(label(1 "GDPpc (mean)") label(2 "GDPpc (median)"))
// * 事实上是柱状图的另一种表示方法, 比较省墨	  

help graph dot // 查看点图的帮助文件
```

命令运行的结果，如下图：

![点图.png](https://images.gitee.com/uploads/images/2019/0704/114742_c850e0c7_3042808.png)

#### 2.10 饼图

基本命令： `graph pie` 

强调选项：
- `plabel(...)` 控制饼块的标签。
    - 比如 `plabel(_all percent,format("%5.2f"))` 为所有饼块按 %5.2f 格式显示百分比。
    - 比如 `plabel(_all sum)` 为所有饼块显示总数。
    - 比如 `plabel(_all name)` 为所有饼块显示类别名。
    - 比如 `plabel(1 percent,gap(20))` 为第一饼显示百分比,其中 gap(#) 控制标签距离圆心的相对距离。
- `pie(...)` 设置饼块的颜色，以及是否突出/分离。
    - 比如 `pie(#,explode)`  突出/分离第 # 饼块。
    - 比如 `pie(#, color(red))` 令第 # 饼块的颜色为红色。

范例如下：

```stata
******* 饼图
graph pie export if     ///
   (country=="Brazil" | ///
    country=="Russia" | ///
    country=="India"  | ///
    country=="China") & year == 2010, ///
    over(country) noclockwise 
// noclockwise 逆时针排序

graph pie export if     /// 
   (country=="Brazil" | ///
    country=="Russia" | ///
    country=="India"  | ///
    country=="China") & year == 2010, ///
    over(country) sort descending 			

graph pie export if     /// 
   (country=="Brazil" | ///
    country=="Russia" | ///
    country=="India"  | ///
    country=="China") & year == 2010,    ///
    over(country) sort descending        ///
    plabel(_all percent,format("%5.2f")) ///
    pie(1,explode)
					  					 
/*
sort descending 降序排列; sort 升序排列
plabel(_all percent,format("%7.2f")) 
为所有饼块按 %7.2f 格式显示百分比
pie(1,explode)  突出/分离第1块饼块
*/

help graph pie // 查看饼图的帮助文件
```

命令运行的结果，部分见下图：

![饼图-部分栗子.png](https://images.gitee.com/uploads/images/2019/0704/115021_2b831967_3042808.png)

### 3 结语

本推文较为详细地介绍了 Stata 的常用绘图命令，如何选取合适的图形应结合具体分析。同时应学会常用 Stata help 命令，灵活运用 option 选项进一步美化图形。

### **参考资料**

1. [Intro to data visualization](https://dss.princeton.edu/training/Visual101.pdf) 
2. 陈传波 《Stata十八讲》

**注：本推文相关数据，do file及资料点这里可获得-->**  [点这里，提取码：l9e4](https://pan.baidu.com/s/1LY11Q32G07DmXacurdwpQA) 

&emsp;


[2019暑期“实证研究方法与经典论文”专题班-连玉君-江艇主讲](https://gitee.com/arlionn/Course/blob/master/2019Papers.md)


[![2019暑期“实证研究方法与经典论文”专题班-连玉君-江艇主讲](https://images.gitee.com/uploads/images/2019/0703/220705_980133f3_1522177.png)](https://gitee.com/arlionn/Course/blob/master/2019Papers.md)



>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。您也可以从 [连享会选题平台](https://gitee.com/Stata002/StataSX2018/wikis/Home) &rarr; [002_备选主题] 中选择感兴趣的题目来撰写推文。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表](https://www.jianshu.com/p/de82fdc2c18a) 
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/0704/095207_4d1cfccf_3042808.png "连享会(公众号: StataChina)推文列表")](https://gitee.com/arlionn/Course/blob/master/README.md)





---
> [![image](http://upload-images.jianshu.io/upload_images/7692714-e52cb14e1928d3f1.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")](https://gitee.com/arlionn/Course/blob/master/README.md)