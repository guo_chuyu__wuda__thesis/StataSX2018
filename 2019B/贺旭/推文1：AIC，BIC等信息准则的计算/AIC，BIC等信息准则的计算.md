&emsp;

> 作者： 贺旭 （中央财经大学）      
>     
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)

&emsp;

[2019暑期“实证研究方法与经典论文”专题班-连玉君-江艇主讲](https://gitee.com/arlionn/Course/blob/master/2019Papers.md)

[![2019暑期“实证研究方法与经典论文”专题班-连玉君-江艇主讲](https://images.gitee.com/uploads/images/2019/0703/222613_f91e8bb0_1522177.png)](https://gitee.com/arlionn/Course/blob/master/2019Papers.md)
  
本篇推文将介绍 AIC,BIC 等信息准则和 MSE,MAE 等回归评价指标以及在它们在 Stata 中的相关命令。

# 1 信息准则

该小节将介绍 AIC,BIC 等信息准则以及在 Stata 中的相关命令。

##  1.1 简介
经常的，在建模过程中，会有一些备选解释变量，选择不同的变量组合会得到不同的模型，而信息准则就是刻画这些模型相对于 “ 真实模型 ” 的信息损失。AIC, BIC, HQIC 等信息准则的计算公式为：

赤池信息量 （akaike information criterion）：

$AIC=-2 ln(L) + 2 k $

贝叶斯信息量 （bayesian information criterion）：

$BIC=-2 ln(L) + ln(n)*k $

汉南 - 奎因信息量 （quinn criterion）：

$HQ=-2 ln(L) + ln(ln(n))*k$

其中 : $L$ 是该模型下的最大似然，$n$ 是数据数量，$k$ 是模型的变量个数。

在模型拟合时，增加参数可使得似然概率增大，但是却引入了额外的变量，因此 AIC 和 BIC 都在目标式中添加了模型参数个数的惩罚项，也就是第二项。当 $n≥8$ 时， $ln(n)*k≥2k$，所以，BIC 相比 AIC 在大数据量时对模型参数惩罚得更多，导致 BIC 更倾向于选择参数少的简单模型。

## 1.2 信息准则的Stata命令
在估计完模型后用，用命令 `estat ic` 来获得模型的 AIC,BIC , 例如 ： 

```stata
. sysuse auto ///载入数据
. regress price headroom trunk length mpg ///利用数据估计模型
. estat ic ///获得模型地AIC和BIC
``` 
结果为

```stata
Akaike's information criterion and Bayesian information criterion

-----------------------------------------------------------------------------
       Model |        Obs  ll(null)  ll(model)      df         AIC        BIC
-------------+---------------------------------------------------------------
           . |         74 -695.7129  -685.0518       5    1380.104   1391.624
-----------------------------------------------------------------------------
``` 
# 2 回归评价指标

该小节将介绍 MSE、MAE、MAPE、R2、Adjusted R2 等回归评价指标以及在 Stata 中的相关命令。

## 2.1 简介
MSE、MAE、MAPE、R2、Adjusted R2 等指标用来表示回归模型拟合数据的精确程度。以下是各个指标的具体公式：

均方误差 MSE（Mean Square Error）：

$M S E=\frac{1}{m} \sum_{i=1}^{m}\left(y_{i}-\hat{y}_{i}\right)^{2}$

平均绝对误差 MAE（Mean Absolute Error）：

$M A E=\frac{1}{m} \sum_{i=1}^{m}\left(y_{i}-\hat{y}_{i}\right)$

平均绝对百分比误差 MAPE（Mean Absolute Percentage Error）：

$M A P E=\frac{100 \%}{m} \sum_{i=1}^{m}\left|\frac{\hat{y}_{i}-y_{i}}{y_{i}}\right|$

决定系数 R2（R-Square）：

$R^{2}=1-\frac{\sum_{i=1}^{m}\left(\hat{y}_{i}-y_{i}\right)^{2}}{\sum_{i}^{m}\left(\overline{y}_{i}-y_{i}\right)^{2}}$

校正决定系数 adj-R2（Adjusted R-Square）：

$R^{2}-$dajust$=1-\frac{\left(1-R^{2}\right)(n-1)}{n-p-1}$

其中： $y_{i}$为解释变量，$\hat{y}_{i}$ 为拟合值

MSE、MAE、MAPE 与 R2 、Adjusted R2 都可以描述模型拟合的精确程度，但 R2和 Adjusted R2，大致可以分为一类。因为 R2 范围为 0 到 1，给了所有模型一个相同的比较标准，Adjusted R2 有可能小于 0，但也大致在 0 到 1 这个范围内。而 MSE、MAE、MAPE 的值与数据有关，范围没有限制。
## 2.2 回归评价指标的Stata命令
我们继续用 Stata 自带的 **auto** 数据来做演示。

（一） R2 和 Adjusted R2 的计算：

```stata
. sysuse auto ///载入数据
. regress price headroom trunk length mpg ///利用数据估计模型 ，R2 和 Adjusted R2也在结果中显示了///
``` 
结果为：

```stata
      Source |       SS           df       MS      Number of obs   =        74
-------------+----------------------------------   F(4, 69)        =      5.76
       Model |   158982705         4  39745676.1   Prob > F        =    0.0005
    Residual |   476082692        69  6899749.15   R-squared       =    0.2503
-------------+----------------------------------   Adj R-squared   =    0.2069
       Total |   635065396        73  8699525.97   Root MSE        =    2626.7

------------------------------------------------------------------------------
       price |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
    headroom |  -680.2898   486.0063    -1.40   0.166    -1649.846    289.2663
       trunk |    80.9094   119.8176     0.68   0.502    -158.1201    319.9389
      length |   23.28608   27.02926     0.86   0.392    -30.63583    77.20798
         mpg |  -173.9507   87.75118    -1.98   0.051    -349.0095    1.108165
       _cons |   6416.948   6036.935     1.06   0.292    -5626.407     18460.3
------------------------------------------------------------------------------
``` 

（二） MSE 的计算：

```stata
. sysuse auto ///载入数据
. regress price headroom trunk length mpg ///利用数据估计模型 ///
. predict e,xb ///获得模型拟合值保存为变量 e
. gen mse=(price-e)*(price-e) ///计算并创建变量 mse
. sum mse//得到结果栏中mean那一列为MSE
``` 
结果为：

```stata
    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
         mse |         74     6433550    1.26e+07   2230.343   8.80e+07
``` 
---
（三） MAE 的计算：

```stata
. sysuse auto ///载入数据
. regress price headroom trunk length mpg ///利用数据估计模型 ///
. predict e,xb ///获得模型拟合值保存为变量 e
. gen mae=abs(price-e) ///计算并创建变量 mae
. sum mae//mae的均值即为结果，也就是结果栏中mean那一列的值
``` 
结果为：

```stata
    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
         mae |         74    1958.684    1622.555   47.22651   9380.703
``` 
---
（三） MAE 的计算：
```stata
. sysuse auto ///载入数据
. regress price headroom trunk length mpg ///利用数据估计模型 ///
. predict e,xb ///获得模型拟合值保存为变量 e
. gen mape=abs(price-e)/price ///计算并创建变量 mae
. sum mape///mape的均值即为结果，也就是结果栏中mean那一列的值
``` 
结果为：

```stata
    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
        mape |         74    .3138812    .1892069   .0060338   .9669298
``` 
---

### **参考资料**

[1] [模型选择的几种方法：AIC，BIC，HQ准则](https://blog.csdn.net/xianlingmao/article/details/7891277)

[2] [理解赤池信息量（AIC）,贝叶斯信息量（BIC）](https://blog.csdn.net/CHIERYU/article/details/51746554)

[3] [STATA计算AIC、BIC、MSE、MAE、MAPE值](https://blog.csdn.net/sinat_37346717/article/details/82195134)



&emsp;

&emsp;


> #### [2019暑期Stata现场班，7.17-26日，北京，连玉君+刘瑞明 主讲](https://gitee.com/arlionn/Course/blob/master/2019%E6%9A%91%E6%9C%9FStata%E7%8E%B0%E5%9C%BA%E7%8F%AD.md)


&emsp;
[![2019暑期Stata现场班，7.17-26日，北京，连玉君+刘瑞明 主讲](https://images.gitee.com/uploads/images/2019/0703/222613_cdfe000f_1522177.png)](https://gitee.com/arlionn/Course/blob/master/2019%E6%9A%91%E6%9C%9FStata%E7%8E%B0%E5%9C%BA%E7%8F%AD.md)



>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。您也可以从 [连享会选题平台](https://gitee.com/Stata002/StataSX2018/wikis/Home) &rarr; [002_备选主题] 中选择感兴趣的题目来撰写推文。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表](https://www.jianshu.com/p/de82fdc2c18a) 
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/0703/222613_0965312d_1522177.png "连享会(公众号: StataChina)推文列表")](https://gitee.com/arlionn/Course/blob/master/README.md)



---
> [![image](http://upload-images.jianshu.io/upload_images/7692714-e52cb14e1928d3f1.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")](https://gitee.com/arlionn/Course/blob/master/README.md)
















