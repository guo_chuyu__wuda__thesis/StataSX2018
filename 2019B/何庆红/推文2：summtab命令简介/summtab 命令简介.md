> 作者：何庆红（北京大学中国卫生经济研究中心）     
> &emsp;
> 连享会：([知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn))
![输入图片说明](https://images.gitee.com/uploads/images/2019/0704/200528_cfe2d30a_5087133.png "微信图片_20190704200434.png")



&emsp;

- Stata连享会 [精品专题](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q)  || [精彩推文](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/0721/002409_29005080_1522177.png "连享会(公众号: StataChina)推文列表")](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)



&emsp;

[2019暑期“实证研究方法与经典论文”专题班-连玉君-江艇主讲](https://gitee.com/arlionn/Course/blob/master/2019Papers.md)

[![2019暑期“实证研究方法与经典论文”专题班-连玉君-江艇主讲](https://images.gitee.com/uploads/images/2019/0703/220705_980133f3_1522177.png)](https://gitee.com/arlionn/Course/blob/master/2019Papers.md)


&emsp;


今天我们介绍一个外部命令 `summtab`，用于连续变量和类别变量的基本描述性统计分析，一方面可用 `putdocx` 命令将结果输出到Word文档中，另一方面还可以使用 `putexcel` 将结果输出到 Excel 文档中。

它的优点是可以在 Word 或 Excel 里生成格式优美、发布质量良好的表格。

`summtab` 只能在  Stata 15 以上版本中运行。
 

## 1.下载安装

```stata
ssc install summtab, replace
```

## 2. 语法格式

```stata
summtab [if] [in], cont_vars(varlist) cat_vars(varlist) [by(varname) {other options}]
```
- 必选项
     - `by(varname) `定义统计描述的分组变量，若不限定变量，则默认整体统计量
     - `cat_vars(varlist) `表示类别变量
     - `cont_vars(varlist)`表示连续变量
   
- 统计量的设定
    - `mean`显示连续变量的均值和标准误
    - `median`显示连续变量的中位数和分位数，如 `25th`、`75th`等
    - `range`显示连续变量的最小值和最大值
    - `pnonmiss`在单独一行中显示连续变量的非缺失样本总数和%比例
    - `rowperc`表示为分类变量提供行百分比，而不是(默认的)列百分比
    - `incmiss`显示分类变量缺失值
- **P-Value 的设定**
    - `pval `表示 P 值
    - `cont_ptype(integer)`表示连续变量的P值
    - `cat_ptype(integer)` 表示分类变量的P值
- **显示格式的设定**
    - `mnfmt(integer)`表示均值和标准误的小数点位数，默认是两位，最大是八位
    - `medfmt(integer)`表示中位数和分位数的小数点位数，默认是一位，最大是八位
    - `rangefmt(integer)`表示最大值和最小值的小数点位数，默认是一位，最大是八位
    - `pnonmissfmt(integer)`表示非缺失值比例的小数点位数，默认是一位，最大是八位
    - `catfmt(integer) `表示分类变量比例的小数点位数，默认是一位，最大是八位
    - `pfmt(integer) `表示P值的小数点位数，默认是三位，最大是八位
- **结果输出** 
   - `directory(string)`表示输出结果存储的位置，默认是当前路径
   - `title(string)`表示表格标题
   - `word `表示结果输出到word格式，`wordname(string) `表示word名称，默认是table1
   - `excel `表示结果输出到excel格式，`excelname(string) `表示excel名称，默认是table1 
   - `replace  `表示输出结果在excel格式或word格式之间替换


# 3. 应用举例

下面以输出到 Excel 为例，展示效果：

```stata
webuse lbw3.dta  
summtab,cont_vars(price mpg weight length) cat_vars(foreign rep78) excel excelname(summary_table1) title(My Table 1) ///
mean median replace

其中，连续变量是`price mpg weight length`，统计量包括`mean`（均值和标准误）、`median`（中位数）；类别变量是`rep78`；统计量描述是针对所有样本

``` 
![输入图片说明](https://images.gitee.com/uploads/images/2019/0704/200553_8fe89fcc_5087133.png "My Table 1.png")

```stata
summtab, by(foreign) cont_vars(price mpg weight length)  cat_vars(rep78) mean median total title(My Table 2) excel ///
excelname(summary_table2) replace

其中，连续变量是`price mpg weight length`，统计量包括`mean`（均值和标准误）、`median`（中位数）；类别变量是`rep78`；根据`foreign`变量，将样本分为两类（`domestic`和`foreign`）进行描述统计
```  
![输入图片说明](https://images.gitee.com/uploads/images/2019/0704/200615_6615822a_5087133.png "My Table 2.png")

```stata
summtab, by(foreign) cont_vars(price mpg weight length) cat_vars(rep78) mean median range total medfmt(1) mnfmt(2) ///
excel excelname(summary_table3) title(My Table 3) replace

其中，连续变量是`price mpg weight length`，统计量包括`mean`（均值和标准误）、`median`（中位数）、`range`（最大最小值），`median`（中位数）保留一位小数点`medfmt(1)`，`mean`（均值和标准误）保留两位小数点`mnfmt(2)`；类别变量是`rep78`；根据`foreign`变量，将样本分为两类（`domestic`和`foreign`）进行描述统计
``` 
![输入图片说明](https://images.gitee.com/uploads/images/2019/0704/200634_7c13d7d1_5087133.png "My Table 3.png")

## 4. 其他相关命令

   - `table` 命令：主要是用来做列表统计，尤其对于类别变量的统计，优点是可用于汇报三维等以上维度表格，具体可参看[[Stata：今天你 “table” 了吗？]](https://www.jianshu.com/p/b0537619e19c)
   - `summarize` 命令：主要用于一维列表的相关统计量的计算
   - `fsum` 命令：主要用于一维列表的相关统计量的计算，优点是命令简洁，支持用户设置的变量标签输出，目前运用较多
   - `baselinetab` 命令：主要用于一维列表和二维列表的相关统计量的计算，优点是表格内容可以转换成数据、文本、网页等格式


## 5. 结果输出和呈现相关推文

- [Stata：今天你 “table” 了吗？](https://mp.weixin.qq.com/s/Pd-lo6mkRXR6DJWRdo19kg)
- [asdoc：Stata 结果输出又一利器！](https://mp.weixin.qq.com/s/gPc26DJTu8PAEvbIPBGLEg)
- [tabout: 用 Stata 输出高品质表格](https://mp.weixin.qq.com/s/3aWmgzG_a1w4gPtXlKlXuA)
- [君生我未生！Stata - 论文四表一键出](https://mp.weixin.qq.com/s/HNAdZLBoMIueID6W7mGrYA)
- [输出相关系数矩阵至 Word / Excel 文档中：pwcorr_a 命令简介](https://mp.weixin.qq.com/s/v60OaBGcMPiJAjt7dOCA2A)
- [Stata: 用esttab生成带组别名称的 LaTeX 回归表格](https://mp.weixin.qq.com/s/GraGtTBRl3pi6Uhw8m4hrw)
- [Stata新命令：Export tabulation results to Excel](https://www.jianshu.com/p/0e2e0d83e490)

## 6. 参考资料

- https://www.statalist.org/forums/forum/general-stata-discussion/general/1402412
- Nicholas J. Cox, 2003, Speaking Stata: Problems with Tables, Part I, Stata Journal, 3(3): 309–324. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300308)
- Nicholas J. Cox, 2003, Speaking Stata: Problems with Tables, Part II, Stata Journal, 3(4): 420–439. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400300410)
- Ben Jann, 2005, Making Regression Tables from Stored Estimates, Stata Journal, 5(3): 288–308. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500302)
- Ben Jann, 2007, Making Regression Tables Simplified, Stata Journal, 7(2): 227–244. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700207)
- Michael Lokshin,  Zurab Sajaia, 2008, Creating Print-ready Tables in Stata, Stata Journal, 8(3): 374–389. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800304)
- John Luke Gallup, 2012, A New System for Formatting Estimation Tables, Stata Journal, 12(1): 3–28. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200102)
- Lauren J. Scott,  Chris A. Rogers, 2015, Creating Summary Tables Using the Sumtable Command, Stata Journal, 15(3): 775–783. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500310)
- Susan Donath, 2018, Baselinetable: A Command for Creating one- and Two-way Tables of Summary Statistics, Stata Journal, 18(2): 327–344. [[pdf]](https://sci-hub.tw/10.1177/1536867X1801800202)
- Susan Donath, 2018, Baselinetable: A Command for Creating one- and Two-way Tables of Summary Statistics, Stata Journal, 18(2): 327–344. [[pdf]](https://sci-hub.tw/10.1177/1536867X1801800202)


&emsp;


>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。您也可以从 [连享会选题平台](https://gitee.com/Stata002/StataSX2018/wikis/Home) &rarr; [002_备选主题] 中选择感兴趣的题目来撰写推文。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- Stata连享会 [精品专题](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q)  || [精彩推文](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/0721/002409_29005080_1522177.png "连享会(公众号: StataChina)推文列表")](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)


---
> [![image](http://upload-images.jianshu.io/upload_images/7692714-e52cb14e1928d3f1.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")](https://gitee.com/arlionn/Course/blob/master/README.md)