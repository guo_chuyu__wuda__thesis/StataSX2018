这篇文章还没有写完。认领者完成后担任第一作者。 建议供参考：

> by 连玉君 `2020/6/15 9:15`
  
1. 我觉得理论部分介绍的还是太过粗糙，可以找一本教科书，用相对规范的方式来介绍，例如：  
- 基本无害那本书  
- [经典文献回顾：政策评价\因果推断的计量方法](https://www.jianshu.com/p/92ec3358c7f2)
- Angrist, J. D., & Krueger, A. B. (1999). Empirical Strategies in Labor Economics. Handbook of Labor Economics, 1277–1366. doi:10.1016/s1573-4463(99)03004-7, [[PDF]](https://sci-hub.tw/10.1016/S1573-4463(99)03004-7)  
- [Elsevier检索结果](https://www.sciencedirect.com/topics/economics-econometrics-and-finance/difference-in-differences)  
2. 文中的例子解释的可以更细致一些


&emsp;

&emsp;

&emsp;



> 第一作者 (单位，邮箱)  
> 何庆红 (北京大学，邮箱) 


> 此推文的目的是简要介绍两期和多期双重差分模型（DID）中的模型设定和系数含义。
## 1. 双重差分模型设定
### 1.1 **政策评估**
政策评估（PolicyEvaluation）是当前公共经济学和劳动经济学中最常用的实证方法。目的在于评价一项既有政策的效果，此类研究关注的是该政策的处理效应（TreatmentEffect）。由于政策往往是对一类特定的人群起作用，因此政策评估的基本思想是将这类人群与其他人群进行比较。目前，双重差分模型是用于政策评估的常用模型。
### 1.2 **政策评估的一般设定**
- 对特定个体**i**， **Di=1**表示个体受到政策影响（处理组），**Di=0**表示个体不受政策影响（控制组） 
- **Yi1**为个体i受政策影响时的结果（outcome），**Yi0**为不受政策 影响时的结果，注意到**Yi1**与**Yi0**的潜变量概念 
- 由于个体**i**不可能同时属于处理组和控制组，因此不可能同时观测到**Yi1**与**Yi0**，这是政策评估模型的出发点 
- 观测到的结果变量**Yi=DiYi1+(1−Di)Yi0**
### 1.3 **DID数据环境**
- DID方法在一定程度上可以看做政策评估的“标杆”方法 
- 其估计出的政策系数较其他方法有更高的可信度——假设条件更弱 
- 其对数据的要求更高：首先，数据分时间前后 ；其次，要有明确的处理组和控制组 
- 估计出的处理效应是ATT
- 前提假设是满足共同趋势
### 1.4 **截面数据vs面板数据**
- DID的数据结构：面板数据最佳，重复截面数据也可 
- 重复截面数据：数据组成必须是稳定的 
   - 不同时点上的数据都是总体的代表性样本 
   - 可以允许组别固定效应
- 面板数据：可以使用个体固定效应，更加稳健 
### 1.5 **模型设定**
##### 1.5.1 **两期DID**

![两期DID](https://images.gitee.com/uploads/images/2019/0812/190329_3d1d0429_5087133.png)
##### 1.5.2 **多期DID**
![多期DID](https://images.gitee.com/uploads/images/2019/0812/190329_6430604b_5087133.png)

## 2. Stata 操作
### 2.1 **数据导入**
```
clear
input 	///
  id	Year	Treat	After	Treat_x_After
  1		2001	0		0		0
  1		2002	0		0		0
  1		2003	0		1		0
  1		2004	0		1		0
  2		2001	0		0		0
  2		2002	0		0		0
  2		2003	0		1		0
  2		2004	0		1		0
  3		2001	1		0		0
  3		2002	1		0		0
  3		2003	1		1		1
  3		2004	1		1		1
end
. bysort id: gen fi = 0.5*id  // Firm Fixed effects
. bysort id: gen  y = _n + fi
. replace         y = y + 10 if Treat_x_After==1
. save "DIDm_simudata.dta", replace
```
### 2.2 两期DID
```
.   use "DIDm_simudata.dta", clear
.   keep if inlist(Year,2002,2003)  //(6 observations deleted)
```
* -DID-基本设定
```
.   reg y Treat##After
*-or
    reg y Treat After Treat_x_After
```
```
.   reg y Treat##After
      Source |       SS           df       MS      Number of obs   =         6
-------------+----------------------------------   F(3, 2)         =    281.56
       Model |  105.583333         3  35.1944444   Prob > F        =    0.0035
    Residual |         .25         2        .125   R-squared       =    0.9976
-------------+----------------------------------   Adj R-squared   =    0.9941
       Total |  105.833333         5  21.1666667   Root MSE        =    .35355

------------------------------------------------------------------------------
           y |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
     1.Treat |        .75   .4330127     1.73   0.225    -1.113103    2.613103
     1.After |          1   .3535534     2.83   0.106    -.5212175    2.521217
             |
 Treat#After |
        1 1  |         10   .6123724    16.33   0.004     7.365174    12.63483
             |
       _cons |       2.75        .25    11.00   0.008     1.674337    3.825663
------------------------------------------------------------------------------
```
### 2.3 多期DID
```
.   use "DIDm_simudata.dta", clear
.   list id Year Treat After y, sepby(id) noobs

  +----------------------------------+
  | id   Year   Treat   After      y |
  |----------------------------------|
  |  1   2001       0       0    1.5 |
  |  1   2002       0       0    2.5 |
  |  1   2003       0       1    3.5 |
  |  1   2004       0       1    4.5 |
  |----------------------------------|
  |  2   2001       0       0      2 |
  |  2   2002       0       0      3 |
  |  2   2003       0       1      4 |
  |  2   2004       0       1      5 |
  |----------------------------------|
  |  3   2001       1       0    2.5 |
  |  3   2002       1       0    3.5 |
  |  3   2003       1       1   14.5 |
  |  3   2004       1       1   15.5 |
  +----------------------------------+
```
* -DID-基本设定
```
混合OLS： 
reg y   i.id i.Year Treat_x_After 
面板数据：
xtset id Year
xtreg y i.Year Treat_x_After, fe
```
* -结果如下：
```
.   eststo m1: reg y  i.id i.Year Treat_x_After  //双向固定+交互项
.   *-同上(使用 xtreg 命令)
.   xtset id Year
       panel variable:  id (strongly balanced)
        time variable:  Year, 2001 to 2004
                delta:  1 unit

.   eststo m2: xtreg y i.Year Treat_x_After, fe
.   *-输出结果
.   local m "m1 m2"

.   esttab `m', nogap  replace not  b(%4.2f) s(N r2_a) 

--------------------------------------------
                      (1)             (2)   
                        y               y   
--------------------------------------------
1.id                 0.00                   
2.id                 0.50                   
3.id                 1.00                   
2001.Year            0.00            0.00   
2002.Year            1.00            1.00   
2003.Year            2.00            2.00   
2004.Year            3.00            3.00   
Treat_x_Af~r        10.00           10.00   
_cons                1.50            2.00   
--------------------------------------------
N                   12.00           12.00   
r2_a                 1.00            1.00   
--------------------------------------------
* p<0.05, ** p<0.01, *** p<0.001
```
>通过上例，我们发现，对于多期DID，模型设定不论是混合OLS还是面板固定效应，结果都是一致的。对此，读者可自行选择。
### 2.4 平行趋势检验
- 第一，构建数据：
>假定一项外生政策在78年实施，试点组由idcode 超过2604的观测值构成：
```
. clear all 
. set more off 
. webuse nlswork, clear 
. xtset idcode year, delta(1)  //设置成面板数据

. gen post = (year >= 78) 
. gen treated = (idcode > 2604)
. global controlx "c.age##c.age c.ttl_exp##c.ttl_exp i.south i.race i.year" 
. levelsof year, local(localyear) //判断年份的情况
. *68 69 70 71 72 73 75 77 78 80 82 83 85 87 88 
. *Get the minimum of variables “year”
. qui sum year 
. return list

scalars:
                  r(N) =  28534
              r(sum_w) =  28534
               r(mean) =  77.9586458260321
                r(Var) =  40.75390607628773
                 r(sd) =  6.383878607577664
                r(min) =  68
                r(max) =  88
                r(sum) =  2224472

.   local yearmin = r(min) 

.  foreach i of local localyear {   
  2.      local j = `i' - 78   
  3.      if `i' > `yearmin'{   
  4.          gen coeff`i' = (year == `i' & treated == 1)   
  5.          label var coeff`i' "`j'"   
  6.          if `i' <= 78 {   
  7.              local countsmall78 = `countsmall78' + 1   
  8.          }   
  9.      }  
 10.  } 
```
- 第二，动态估计回归结果：
```
. xtreg ln_w coeff* $controlx, fe robust 
note: 2.race omitted because of collinearity
note: 3.race omitted because of collinearity

Fixed-effects (within) regression               Number of obs     =     28,502
Group variable: idcode                          Number of groups  =      4,710

R-sq:                                           Obs per group:
     within  = 0.1619                                         min =          1
     between = 0.2932                                         avg =        6.1
     overall = 0.2174                                         max =         15

                                                F(33,4709)        =      62.36
corr(u_i, Xb)  = 0.1187                         Prob > F          =     0.0000

                                    (Std. Err. adjusted for 4,710 clusters in idcode)
-------------------------------------------------------------------------------------
                    |               Robust
            ln_wage |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
--------------------+----------------------------------------------------------------
            coeff69 |   .0055446   .0207995     0.27   0.790    -.0352321    .0463213
            coeff70 |   .0343507   .0208612     1.65   0.100     -.006547    .0752484
            coeff71 |   .0088947   .0218837     0.41   0.684    -.0340076    .0517971
            coeff72 |    .038176   .0232913     1.64   0.101    -.0074859    .0838379
            coeff73 |   .0450401    .023497     1.92   0.055     -.001025    .0911053
            coeff75 |   .0308804   .0237891     1.30   0.194    -.0157572    .0775181
            coeff77 |   .0583207   .0246613     2.36   0.018     .0099729    .1066684
            coeff78 |   .0598168   .0257568     2.32   0.020     .0093216    .1103121
            coeff80 |   .0476003   .0259926     1.83   0.067    -.0033574    .0985579
            coeff82 |   .0530631   .0259442     2.05   0.041     .0022002     .103926
            coeff83 |   .0363201   .0267828     1.36   0.175    -.0161867    .0888269
            coeff85 |   .0306318   .0258689     1.18   0.236    -.0200833    .0813468
            coeff87 |   .0469758   .0264587     1.78   0.076    -.0048955    .0988472
            coeff88 |   .0244436   .0290138     0.84   0.400    -.0324371    .0813243
                age |   .0673821   .0133884     5.03   0.000     .0411346    .0936295
                    |
        c.age#c.age |  -.0009288   .0001047    -8.87   0.000    -.0011341   -.0007235
                    |
            ttl_exp |   .0672428   .0037157    18.10   0.000     .0599582    .0745273
                    |
c.ttl_exp#c.ttl_exp |  -.0011855   .0001617    -7.33   0.000    -.0015025   -.0008684
                    |
            1.south |  -.0684098   .0169926    -4.03   0.000    -.1017233   -.0350964
                    |
               race |
             black  |          0  (omitted)
             other  |          0  (omitted)
                    |
               year |
                69  |   .0325533   .0173162     1.88   0.060    -.0013945    .0665012
                70  |  -.0606162   .0276927    -2.19   0.029    -.1149069   -.0063255
                71  |  -.0511246   .0390546    -1.31   0.191    -.1276898    .0254406
                72  |   -.101514    .050546    -2.01   0.045    -.2006079   -.0024201
                73  |  -.1498063   .0622991    -2.40   0.016    -.2719416   -.0276709
                75  |  -.2071723   .0855563    -2.42   0.015    -.3749027    -.039442
                77  |  -.2409533   .1092063    -2.21   0.027    -.4550487   -.0268578
                78  |  -.2588216   .1216995    -2.13   0.033    -.4974095   -.0202337
                80  |  -.3365212   .1454035    -2.31   0.021    -.6215801   -.0514623
                82  |  -.4019851   .1696162    -2.37   0.018    -.7345122    -.069458
                83  |  -.4101316   .1808849    -2.27   0.023    -.7647507   -.0555126
                85  |  -.4226428   .2046641    -2.07   0.039    -.8238802   -.0214053
                87  |  -.4690002   .2288801    -2.05   0.041    -.9177123    -.020288
                88  |  -.4320016   .2441946    -1.77   0.077    -.9107373    .0467342
                    |
              _cons |   .4604538   .2419374     1.90   0.057    -.0138567    .9347643
--------------------+----------------------------------------------------------------
            sigma_u |  .36355618
            sigma_e |  .29351858
                rho |  .60539259   (fraction of variance due to u_i)
-------------------------------------------------------------------------------------
. est store dynamic 
```
- 第三，平行趋势检验作图：
```
. coefplot dynamic, keep(coeff*) vertical recast(connect) yline(0) xline(`countsmall78') ciopts(recast( > rline) lpattern(dash))
```
![平行趋势检验](https://images.gitee.com/uploads/images/2019/0812/190329_6d486022_5087133.png)

### **3.相关推文**
- [Stata：双重差分的固定效应模型 (DID)](https://www.jianshu.com/p/e97c1dc05c2c)
- [Stata: 倍分法(DID)板书](https://www.jianshu.com/p/c3f6cde92d5e)
- [Stata：多期倍分法 (DID) 图示](https://www.jianshu.com/p/a45879ea884c)
- [DID 边际分析：让政策评价结果更加丰满](https://www.jianshu.com/p/4ff6124df225)
- [多期倍分法（DID）：平行趋势检验图示](https://www.jianshu.com/p/384776721340)
- [Fuzzy Differences-in-Differences （模糊倍分法）](https://www.jianshu.com/p/8918037d76a1)
- [Stata新命令快讯: 有向无环图、模糊倍分法等](https://www.jianshu.com/p/3b0f925a940d) 