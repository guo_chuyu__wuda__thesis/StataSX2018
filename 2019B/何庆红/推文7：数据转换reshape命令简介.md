> 作者：何庆红 
> 连享会：([知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn))

> 此推文的目的是简要介绍数据横纵变换的命令`reshape`。
## 1. 数据横纵转换

在实证分析之前，我们需要将数据转换成需要的格式，比如从宽型转换为长型，或者反过来。
```
           long
        +------------+                  wide
        | i  j  year |                 +----------------+
        |------------|                 | i  year1 year2 |
        | 1  1   4.1 |     reshape     |----------------|
        | 1  2   4.5 |   <--------->   | 1    4.1   4.5 |
        | 2  1   3.3 |                 | 2    3.3   3.0 |
        | 2  2   3.0 |                 +----------------+
        +------------+

        long --> wide:

                                            j 旧变量名称
                                           /
                reshape wide year, i(i) j(j)

        wide --> long:

                reshape long stub, i(i) j(j)
                                           \
                                            j 新变量名称
```

## 2. Stata 命令 `reshape`

### 2.1 命令语法

该命令的基本语法如下

```
reshape long stubnames, i(varlist)  j(varname)   //wide --> long
reshape wide stubnames, i(varlist)  j(varname)   //long --> wide
```
其中，

**long** ：表示将数据从宽型转为长型

**wide**：表示将数据从长型转为宽型

**i(varlist)**：表示用 **varlist**作为 **ID**变量

**j(varname)**：在 wide->long时， varname表示新的变量名称；在 long --> wide时， varname表示旧的变量名称。
## 2.2 实证运用：数据处理

### 2.2.1 **问题描述**

```
* 原始资料(可能来自网页等途径)  -wide-型数据
                       income                unemployee      
   -----------------------------------+----------------------
    id   sex    1980    1981    1982  |  1980   1981    1982 
   -----------------------------------+----------------------
     1     0    5000    5500    6000  |    0      1      0   
     2     1    2000    2200    3300  |    1      0      0   
     3     0    3000    2000    1000  |    0      0      1   
   -----------------------------------+----------------------

  * 整理后的资料     -wide-型数据
    id   sex   inc80   inc81   inc82   ue80   ue81   ue82 
     1     0    5000    5500    6000      0      1      0 
     2     1    2000    2200    3300      1      0      0 
     3     0    3000    2000    1000      0      0      1 

  * 最终需要的资料(panel data)    -long-型数据
    id   year   sex    inc   ue  
     1   1980     0   5000    0  
     1   1981     0   5500    1  
     1   1982     0   6000    0  
     2   1980     1   2000    1  
     2   1981     1   2200    0  
     2   1982     1   3300    0  
     3   1980     0   3000    0  
     3   1981     0   2000    0  
     3   1982     0   1000    1  
```
### 2.2.2 **数据转换**

- 第一种转换：wide --> long和 long --> wide
```
.  webuse reshape1, clear
.  list
     +-------------------------------------------------------+
     | id   sex   inc80   inc81   inc82   ue80   ue81   ue82 |
     |-------------------------------------------------------|
  1. |  1     0    5000    5500    6000      0      1      0 |
  2. |  2     1    2000    2200    3300      1      0      0 |
  3. |  3     0    3000    2000    1000      0      0      1 |
     +-------------------------------------------------------+
```
- 从wide 转为 long
>在通过 `reshape` 命令转换时，变量 **sex** 不发生变化，无需转换；变量 **inc** 和**ue**发生了变化，需要转换；**j()** 选项中填写新的变量名称。
```
.  reshape long inc ue, i(id) j(year)  //year为新的变量名称
.  replace year = 1900 + year
.  list, sepby(id)
     +-----------------------------+
     | id   year   sex    inc   ue |
     |-----------------------------|
  1. |  1   1980     0   5000    0 |
  2. |  1   1981     0   5500    1 |
  3. |  1   1982     0   6000    0 |
     |-----------------------------|
  4. |  2   1980     1   2000    1 |
  5. |  2   1981     1   2200    0 |
  6. |  2   1982     1   3300    0 |
     |-----------------------------|
  7. |  3   1980     0   3000    0 |
  8. |  3   1981     0   2000    0 |
  9. |  3   1982     0   1000    1 |
     +-----------------------------+
```
- 从long 转为 wide
```
.  reshape wide inc ue, i(id) j(year)  //或者采用快捷命令 **reshape wide**
.  list 
     +-------------------------------------------------------------------+
     | id   inc1980   ue1980   inc1981   ue1981   inc1982   ue1982   sex |
     |-------------------------------------------------------------------|
  1. |  1      5000        0      5500        1      6000        0     0 |
  2. |  2      2000        1      2200        0      3300        0     1 |
  3. |  3      3000        0      2000        0      1000        1     0 |
     +-------------------------------------------------------------------+
```
- 第二种转换：wide-wide -->long-long 和 long-long -->wide-wide 
```
.  webuse reshape5, clear
.  list
     +-------------------------+
     | hid   sex   year    inc |
     |-------------------------|
  1. |   1     f     90   3200 |
  2. |   1     f     91   4700 |
  3. |   1     m     90   4500 |
  4. |   1     m     91   4600 |
     +-------------------------+
```
*  从long-long 转为wide-wide
```
.  reshape wide @inc, i(hid year) j(sex) string
或者采用下面这种等价写法：  
.  reshape wide minc finc, i(hid) j(year)
.  list 
     +--------------------------+
     | hid   year   finc   minc |
     |--------------------------|
  1. |   1     90   3200   4500 |
  2. |   1     91   4700   4600 |
     +--------------------------+
```
*  从wide-wide转为long-long
```
 . reshape long minc finc, i(hid) j(year)
或者采用下面这种等价写法：  
 . reshape long @inc, i(hid year) j(sex) string
.  list 

     +--------------------------+
     | hid   year   finc   minc |
     |--------------------------|
  1. |   1     90   3200   4500 |
  2. |   1     91   4700   4600 |
     +--------------------------+
```
### 2.2.3 **注意事项**
- 第一：允许文本型变量的转换（注意：默认的是数值型变量）
```
.  webuse reshape4, clear
.  list
     +-------------------------+
     | id   kids   incm   incf |
     |-------------------------|
  1. |  1      0   5000   5500 |
  2. |  2      1   2000   2200 |
  3. |  3      2   3000   2000 |
     +-------------------------+
```
- 从wide 转为 long
```
. reshape long inc, i(id) j(sex) string 
. list, sepby(id)
     +------------------------+
     | id   sex   kids    inc |
     |------------------------|
  1. |  1     f      0   5500 |
  2. |  1     m      0   5000 |
     |------------------------|
  3. |  2     f      1   2200 |
  4. |  2     m      1   2000 |
     |------------------------|
  5. |  3     f      2   2000 |
  6. |  3     m      2   3000 |
     +------------------------+
```
- 从long 转为 wide
```
. reshape wide inc, i(id) j(sex) string
. list
     +-------------------------+
     | id   incf   incm   kids |
     |-------------------------|
  1. |  1   5500   5000      0 |
  2. |  2   2200   2000      1 |
  3. |  3   2000   3000      2 |
     +-------------------------+
```
- 第二：ID需要能够唯一识别，否则无法转换
```
. webuse reshape2, clear
. list

     +----------------------------------+
     | id   sex   inc80   inc81   inc82 |
     |----------------------------------|
  1. |  1     0    5000    5500    6000 |
  2. |  2     1    2000    2200    3300 |
  3. |  3     0    3000    2000    1000 |
  4. |  2     0    2400    2500    2400 |
     +----------------------------------+
. reshape long inc, i(id) j(year)
(note: j = 80 81 82)
variable id does not uniquely identify the observations
    Your data are currently wide.  You are performing a reshape long.  You specified i(id) and j(year).  In the current wide form,
    variable id should uniquely identify the observations.  Remember this picture:

         long                                wide
        +---------------+                   +------------------+
        | i   j   a   b |                   | i   a1 a2  b1 b2 |
        |---------------| <--- reshape ---> |------------------|
        | 1   1   1   2 |                   | 1   1   3   2  4 |
        | 1   2   3   4 |                   | 2   5   7   6  8 |
        | 2   1   5   6 |                   +------------------+
        | 2   2   7   8 |
        +---------------+
    Type reshape error for a list of the problem observations.
```
我们发现，stata结果显示错误信息，指出**ID**不能唯一识别。对此，我们可以通过`reshape error`命令，查看重复的**ID**。
```
.  reshape error
The data are currently in the wide form; there should be a single
observation per i.
2 of 4 observations have duplicate i values:
     +----+
     | id |
     |----|
  2. |  2 |
  3. |  2 |
     +----+
```
