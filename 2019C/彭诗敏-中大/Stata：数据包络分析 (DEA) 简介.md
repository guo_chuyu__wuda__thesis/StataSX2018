# Stata: 数据包络分析 (DEA) 简介
&emsp;

> 作者：彭诗敏 (中山大学)，pengshm7@mail2.sysu.edu.cn
>     
> Stata 连享会：  [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn) | [StataChina公众号](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw) 




## 1. 数据包络分析（DEA）简介

数据包络分析 (DEA) 是一种用于评估决策单元 (DMUs) 生产效率的线性规划方法。在不假定任何分布的情况下，它可以同时考虑多个投入和产出。面向投入的 DEA 模型是指在至少满足已有的产出水平的情况下最小化投入，而面向产出的 DEA 模型则是指在不需要更多的投入的情况下最大化产出。

DEA 模型具体又可细分为三种类型：（1）**CCR 模型**：该模型假定规模报酬不变，主要用来测量技术效率；（2） **BBC 模型**：该模型假定规模报酬可变，主要测算纯技术效率，即技术效率与规模效率的比值；（3）**DEA-Malmquist 指数模型**：该模型可以测算出决策单元 (DMUs) 的生产效率在不同时期的动态变化情况。


### 1.1 CCR 模型

CCR 模型可以计算规模报酬不变情况下的资源配置效率。首先，我们简单推倒一下 CCR 模型，以第 $j_{0}$ 个决策单元的效率指数为目标，以所有决策单元的效率为约束，我们可以得到以下模型：

$$
\max h_{j_{0}}=\frac{\sum_{r=1}^{s} u_{r} y_{rj_{0}}}{\sum_{i=1}^{m} v_{i} x_{i j_{0}}}
$$

$$
\text { s.t. } \ \frac{\sum_{r=1}^{s} u_{r} y_{r j}}{\sum_{i=1}^{m} v_{i} x_{i j}} \leq 1, j=1,2, \ldots n
$$

$$
u \geq 0, v \geq 0
$$

其中，$x_{ij}$ 表示第 $j$ 个决策单元对第 $i$ 种投入要素的投放总量，而 $y_{rj}$  则表示第 $j$ 个决策单元中第 $r$ 种产品的产出总量，$v_{i}$ 和 $u_{r}$ 分别指第 $i$ 种类型投入与第 $r$ 种类型产出的权重系数。经 **Charnes-Cooper** 变换，可变为如下线性规划模型：

$$
\max \  h_{j_{0}}=\mu^{T} y_{0}
$$

$$
\text {s.t. }\  w^{T} x_{j}-\mu^{T} y_{j} \geq 0, \ j=1,2, . . . n
$$

$$
w^{T} x_{0}=1
$$

$$
w \geq 0, \mu \geq 0
$$

其中，$\mathrm{t}=\frac{1}{v^{T} x_{0}}, w=t v, \mu=t u$。

在上述规划的对偶规划中引入松弛变量 $s^{+}$ 和剩余变量 $s^{-}$，把不等式约束变为等式约束，由此模型可以简化为：

$$
\min \theta
$$

$$
\text { s.t. }\sum_{j=1}^{n} \lambda_{j} y_{j}+s^{+}=\theta x_{0}
$$

$$
\sum_{j=1}^{n} \lambda_{j} y_{j}-s^{-}=\theta y_{0}
$$

$$
\lambda_{j} \geq 0, j=1,2, . . . n
$$

$$
s^{+} \geq 0, s^{-} \leq 0
$$

我们能够用 CCR 模型判定技术有效和规模有效是否同时成立：

1. 若满足 $\theta^{*}=1$且$s^{*+}=0, s^{*-}=0$，则决策单元为 DEA 有效，决策单元的经济活动同时为技术有效和规模有效；
2. 若满足 $\theta^{*}=1$，但至少某个投入或者产出大于 0，则决策单元为弱 DEA 有效，决策单元的经济活动不是同时为技术有效和规模有效；
3. 若满足 $\theta^{*}<1$，决策单元不是 DEA 有效，经济活动既不是技术有效，也不是规模有效。

### 1.2 BBC 模型

BBC 模型用以计算规模报酬可变情况下的资源配置效率，它对 CCR 模型的约束条件进行了修改：

$$
\min \theta
$$

$$
\text { s.t. }\sum_{j=1}^{n} \lambda_{j} y_{j}+s^{+}=\theta x_{0}
$$

$$
\sum_{j=1}^{n} \lambda_{j} y_{j}-s^{-}=\theta y_{0}
$$

$$
\sum{\lambda_{j}}=1, \ j=1,2, . . . n
$$

$$
s^{+} \geq 0, s^{-} \leq 0
$$

### 1.3 DEA-Malmquist 指数模型

DEA-Malmquist 指数模型被用来测度各个决策单元不同时期效率的变动情况，它可以用距离函数 (E) 表示为以下数学表现形式：

$$M P I_{I}^{t}=\frac{E_{I}^{t}\left(x^{t+1}, y^{t+1}\right)}{E_{I}^{t}\left(x^{t}, y^{t}\right)}, M P I_{I}^{t+1}=\frac{E_{I}^{t+1}\left(x^{t+1}, y^{t+1}\right)}{E_{I}^{t+1}\left(x^{t}, y^{t}\right)}$$

为了把两个时期的技术水平都纳入考虑，我们取它们的几何平均值：

$$M P I_{I}^{G}=\left(MPI_{I}^{\mathrm{t}} M P I_{I}^{t+1}\right)^{1 / 2}=\left[\left(\frac{E_{I}^{t}\left(x^{t+1}, y^{t+1}\right)}{E_{I}^{t}\left(x^{t}, y^{t}\right)}\right) \cdot\left(\frac{E_{I}^{t+1}\left(x^{t+1}, y^{t+1}\right)}{E_{I}^{t+1}\left(x^{t}, y^{t}\right)}\right)\right]^{1 / 2}$$

该生产率指数又可以分解为面向输入的效率变化 (EFFCH) 和技术效率(TECHCH) ，技术效率又可以分解为规模效率 (SECH) 和纯技术效率 (PECH) 两部分：

$$MPI_{I}^{G}=\left(EFFCH_{I}\right) \cdot \left(TECHCH_{I}^{G}\right)=\left(\frac{E_{I}^{t+1}\left(x^{t+1}, y^{t+1}\right)}{E_{I}^{t}\left(x^{t}, y^{t}\right)}\right) \cdot \left[\left(\frac{E_{I}^{t}\left(x^{t}, y^{t}\right)}{E_{I}^{t+1}\left(x^{t}, y^{t}\right)}\right) \cdot\left(\frac{E_{I}^{t}\left(x^{t+1}, y^{t+1}\right)}{E_{I}^{t+1}\left(x^{t+1}, y^{t+1}\right)}\right)\right]^{1/2}$$

$$SECH=\left[\frac{E_{v r s}^{t+1}\left(x^{t+1}, y^{t+1}\right) / E_{c r s}^{t+1}\left(x^{t+1}, y^{t+1}\right)}{E_{v r s}^{t+1}\left(x^{t}, y^{t}\right) / E_{c r s}^{t+1}\left(x^{t}, y^{t}\right)} \cdot \frac{E_{v r s}^{t}\left(x^{t+1}, y^{t+1}\right) / E_{c r s}^{t}\left(x^{t+1}, y^{t+1}\right)}{E_{v r s}^{t}\left(x^{t}, y^{t}\right) / E_{c r s}^{t}\left(x^{t}, y^{t}\right)}\right]^{1 / 2}$$

$$PECH=\frac{E_{\mathrm{vrs}}^{t+1}\left(x^{t+1}, y^{t+1}\right)}{E_{c r s}^{t}\left(x^{t}, y^{t}\right)}$$


## 2. DEA 和 MPI 的 Stata 实现

### 2.1 dea 命令

#### 2.1.1 dea 命令语法

`dea` 命令用于实现 xxxx (**---需要诗敏补充----**)，由 xxx 和 xxx 编写 (Author, year)。

在 Stata 命令窗口中输入 `ssc install dea, replace` 可以下载最新版的 `dea` 命令，进而输入 `help dea` 可以查看其帮助文件。

该命令的语法详情如下：

```stata
dea ivars = ovars [if] [in] [using/filename ][,  ///
    rts(string) ort(string) stage(#)  ///
	trace saving(filename) ]
```


其中，  

- `ivars` 表示投入变量  
- `ovars` 表示产出变量  
- `rts(string)` 可选择不同规模报酬的相应模型：默认值是 `rts(crs)` ，即规模报酬不变， `rts(vrs)` 、 `rts(drs)` 和 `rts(nirs)` 分别表示规模报酬可变，规模报酬递减和规模报酬不增长
- `ort(string)` 指定方向：默认值是 `ort(i)` ，表示面向投入的 DEA 模型； `ort(o)` 表示面向产出的 DEA 模型
- `stage(#)` 默认值是 `stage(2)` ，即两阶段 DEA 模型；`stage(1)` 为单阶段 DEA 模型
- `trace` 允许所有序列显示在结果窗口中，并保存在 “ dea.log ” 文件中 

注意：需要导入决策单元变量 **dmu** 。

#### 2.1.2 dea 的使用方法

举例：

```stata
*-导入例子所用的数据
local address https://gitee.com/Stata002/StataSX2018/raw/master/2019C/%E5%BD%AD%E8%AF%97%E6%95%8F-%E4%B8%AD%E5%A4%A7/data_dea.csv
local field dmu;area;employee;sales;profit
local name = 111
copy "`address'?code=1`name'&fields=`field'\\`name'.csv" s`name'.csv, replace
insheet using "`address'?code=1`name'&fields=`field'\\`name'.csv", clear

*-采用dea命令
dea area employee = sales profit
```

其中， **area** **employee** 为 csv 表中的投入变量，**sales** **profit** 为产出变量；**data_dea.csv** 数据仅用来举例，大家可以导入自己实际需要处理的数据。

#### 2.1.3 dea 的结果解读

```
options: RTS(CRS) ORT(IN) STAGE(2)
CRS-INPUT Oriented DEA Efficiency Results:
                        ref: ref: ref:     ref:     ref:  islack:   islack:  oslack:  oslack:
       rank     theta     A    B    C        D        E     area  employee    sales   profit
dmu:A     5   .888889     0    0    0        0  .666667        0   1.55556        0  .666667
dmu:B     1         1     0    1    0        0        0        0         0        0        0
dmu:C     4   .888889     0    0    0  .333333  .333333        0   .333333        0        0
dmu:D     1         1     0    0    0        1        0        0         0        0        0
dmu:E     1         1     0    0    0        0        1        0         0        0        0
```

由输出的结果可知，B 、D 、E 公司为 DMU 有效，A 、C 公司为 DMU 低效率。对于公司 A，减少 1.556 单位 的 **employee** 投入与 0.667 单位的 **profit** 产出将使得生产更有效率。对于公司 C ，减少 0.333 单位的 **employee** 投入将使得生产更有效率。


### 2.2 malmq 命令

#### 2.2.1 malmq 命令语法

```stata
malmq ivars = ovars [if] [in] [using/filename] [, ///
      ort(string) period(string) ///
	  trace saving(filename) ]
```


其中，  

- `ivars` 表示投入变量
- `ovars` 表示产出变量
- `ort(string)` 指定方向：默认值是 `ort(i)` ，表示面向投入的 DEA 模型；`ort(o)`表示面向产出的 DEA 模型
- `period(string)` 指定时间
- `trace` 允许所有序列显示在结果窗口中，并保存在 “ malmq.log ” 文件中  

注意：决策单元变量 (**dmu**) 需要是字符型


#### 2.2.2 malmq 的使用方法

举例：

```stata    
*-导入例子所用的数据
 local address https://gitee.com/Stata002/StataSX2018/raw/master/2019C/%E5%BD%AD%E8%AF%97%E6%95%8F-%E4%B8%AD%E5%A4%A7/data_mpi.csv
 local field dmu;year;i_x;o_q
 local name = 222
 copy "`address'?code=1`name'&fields=`field'\\`name'.csv" s`name'.csv, replace
 insheet using "`address'?code=1`name'&fields=`field'\\`name'.csv", clear
 
*-使用malmq命令
 malmq i_x = o_q,ort(i) period(year) 
```

其中， **i_x** 为 csv 表中的投入变量，**o_q** 为产出变量，时间间隔为一年；**data_mpi.csv** 数据仅用来举例，大家可以导入自己实际需要处理的数据。

#### 2.2.3 malmq 的结果解读

```
Cross CRS-DEA Result:
          from     thru        t       t1
dmu:A        1        2       .5     .375
dmu:B        1        2      .75     .375
dmu:C        1        2  1.33333      .75
dmu:D        1        2       .6       .6
dmu:E        1        2        1     .625
dmu:A        2        3     .375     .375
dmu:B        2        3    .5625    .5625
dmu:C        2        3        1        1
dmu:D        2        3      .45      .45
dmu:E        2        3      .75      .75

Malmquist efficiency INPUT Oriented DEA Results:

     +--------------------------------+
     | year   dmu   CRS_eff   VRS_eff |
     |--------------------------------|
  1. |    1     A        .5         1 |
  2. |    1     B        .5      .625 |
  3. |    1     C         1         1 |
  4. |    1     D        .8        .9 |
  5. |    1     E   .833333         1 |
     |--------------------------------|
  6. |    2     A      .375         1 |
  7. |    2     B     .5625   .666667 |
  8. |    2     C         1         1 |
  9. |    2     D       .45   .533333 |
 10. |    2     E       .75         1 |
     |--------------------------------|
 11. |    3     A      .375         1 |
 12. |    3     B     .5625   .666667 |
 13. |    3     C         1         1 |
 14. |    3     D       .45   .533333 |
 15. |    3     E       .75         1 |
     +--------------------------------+

Malmquist productvity index INPUT Oriented DEA Results:

     +--------------------------------------------------------------+
     | period   dmu     tfpch   effch    techch      pech      sech |
     |--------------------------------------------------------------|
  1. |    1~2     A         1     .75   1.33333         1       .75 |
  2. |    1~2     B       1.5   1.125   1.33333   1.06667   1.05469 |
  3. |    1~2     C   1.33333       1   1.33333         1         1 |
  4. |    1~2     D       .75   .5625   1.33333   .592593   .949219 |
  5. |    1~2     E       1.2      .9   1.33333         1        .9 |
     |--------------------------------------------------------------|
  6. |    2~3     A         1       1         1         1         1 |
  7. |    2~3     B         1       1         1         1         1 |
  8. |    2~3     C         1       1         1         1         1 |
  9. |    2~3     D         1       1         1         1         1 |
 10. |    2~3     E         1       1         1         1         1 |
     +--------------------------------------------------------------+
```

由以上的输出结果我们可以得到各时期的 CRS 效率与 VRS 效率，以及每个阶段的全要素生产率变化 (tfpch) ，相对于 CRS 技术的技术效率变化 (effch)，技术变革 (techch)，相对于VRS技术的纯技术效率变化 (pech) 和规模效率变化 (sech) 这五个指标。


### 文献来源

> [1]Yong-bae Ji, and Choonjo Lee. (2010) Data Envelopment Analysis. Stata Journal, 10(2): 267–280. [[PDF]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000207)  
> [2]Kyong-Rock Lee, and Byung-Ihn Leem. (2011) Malmquist Productivity Analysis Index using DEA frontier in Stata. The Stata Journal.[[PDF]](https://gitee.com/Stata002/StataSX2018/raw/f36f42ca3f2e4f1c21842e8d6f8e20d1189405ef/2019C/%E5%BD%AD%E8%AF%97%E6%95%8F-%E4%B8%AD%E5%A4%A7/Malmquist_Productivity_Index_using_DEA_frontier_in_Stata.pdf) 