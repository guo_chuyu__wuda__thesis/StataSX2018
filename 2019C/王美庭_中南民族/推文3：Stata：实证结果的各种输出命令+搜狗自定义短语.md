# Stata: 实证结果的各种输出命令 + 搜狗自定义短语

> 作者：王美庭 (中南民族大学经济学院)  



## 1. 本文目的
很多时候，我们往往在做着很多重复性的输入，如一遍又一遍的在 Stata 中输入 `sysuse auto.dta, clear`，有什么方法能让我们快速的输入这些经常要用到的语句？连享会在文章 [「Stata+搜狗=？=效率！（搜狗自定义短语）」](https://www.jianshu.com/p/4c27c66757a8) 中阐述了如何使用搜狗自定义短语来解决这个问题。

同时，连享会的文章[「Stata：实证结果的各种输出命令，孰优孰劣？」](https://gitee.com/Stata002/StataSX2018/blob/master/2019C/%E7%8E%8B%E7%BE%8E%E5%BA%AD_%E4%B8%AD%E5%8D%97%E6%B0%91%E6%97%8F/%E6%8E%A8%E6%96%872%EF%BC%9AStata%EF%BC%9A%E5%AE%9E%E8%AF%81%E7%BB%93%E6%9E%9C%E7%9A%84%E5%90%84%E7%A7%8D%E8%BE%93%E5%87%BA%E5%91%BD%E4%BB%A4%EF%BC%8C%E5%AD%B0%E4%BC%98%E5%AD%B0%E5%8A%A3%EF%BC%9F.md)阐述了 Stata 各种常用的实证结果输出命令（含 **Word** 表格输出和 **LaTeX** 代码输出），并对它们的优劣进行了评价。

本文将综合以上两个方面，**目的在于**：把文章[「Stata：实证结果的各种输出命令，孰优孰劣？」](https://gitee.com/Stata002/StataSX2018/blob/master/2019C/%E7%8E%8B%E7%BE%8E%E5%BA%AD_%E4%B8%AD%E5%8D%97%E6%B0%91%E6%97%8F/%E6%8E%A8%E6%96%872%EF%BC%9AStata%EF%BC%9A%E5%AE%9E%E8%AF%81%E7%BB%93%E6%9E%9C%E7%9A%84%E5%90%84%E7%A7%8D%E8%BE%93%E5%87%BA%E5%91%BD%E4%BB%A4%EF%BC%8C%E5%AD%B0%E4%BC%98%E5%AD%B0%E5%8A%A3%EF%BC%9F.md)所涉及的所有命令变成搜狗自定义短语，供大家进行套用，以提高大家的工作效率。

另外，大家使用该方法在快速的键出 Stata 各种实证结果输出命令的同时，**还能根据命令主体后面的注释，知道该命令的特点和存在的问题**，从而能帮大家更快的选择合适自己的命令。


## 2. 实际效果演示
- **用 asdoc 输出描述性统计 Word 表格**
![关键字：asdocsum](https://images.gitee.com/uploads/images/2019/0921/182425_385ef2eb_1985121.gif)

- **用 esttab 输出分组 T 均值检验 Word 表格**
![关键字：esttabttest](https://images.gitee.com/uploads/images/2019/0921/182425_fbbf8f25_1985121.gif)


- **用 corr2docx 输出相关系数矩阵 Word 表格**
![关键字：corrdocx](https://images.gitee.com/uploads/images/2019/0921/182425_0eac28c1_1985121.gif)


- **用 outreg2 输出回归结果 Word 表格**
![关键字：outregreg](https://upload-images.jianshu.io/upload_images/12638186-2f3c239abe4d296b.gif?imageMogr2/auto-orient/strip)


- **用 esttab 输出所有以上表格至一个 Word 文件里**
![关键字：esttab](https://upload-images.jianshu.io/upload_images/12638186-59cd6531d39fdc46.gif?imageMogr2/auto-orient/strip)

- **用 esttab 输出所有以上表格至一个 .tex 文件里**
![关键字：esttabl](https://upload-images.jianshu.io/upload_images/12638186-248c4ee199a67118.gif?imageMogr2/auto-orient/strip)



## 3. 各种自定义短语关键字及其含义
最后一章的附件中，包含了所有搜狗自定义短语。为了让大家能快速的使用，下面用表格的形式列出其中所有的自定义短语关键字及其含义。

| 关键字         	| 所用命令                                         	| 输出表格类型                     	|
|----------------	|--------------------------------------------------	|----------------------------------	|
| `asdocsum`     	| `asdoc sum ...`                                  	| 描述性统计 **Word** 输出         	|
| `sumdocx`      	| `sum2docx ...`                                   	| 描述性统计 **Word** 输出         	|
| `outregsum`    	| `outreg2 using Myfile, sum(detail)...`           	| 描述性统计 **Word** 输出         	|
| `esttabsum`    	| `(estpost summarize ...)`和`(esttab ...)`        	| 描述性统计 **Word** 输出         	|
| `outregsuml`   	| `outreg2 using Myfile, sum(detail)...`           	| 描述性统计 **LaTeX** 输出        	|
| `esttabsuml`   	| `(estpost summarize ...)`和`(esttab ...)`        	| 描述性统计 **LaTeX** 输出        	|
| `asdocttest`   	| `asdoc ttest ...`                                	| 分组 T 均值检验 **Word** 输出    	|
| `tdocx`        	| `t2docx ...`                                     	| 分组 T 均值检验 **Word** 输出    	|
| `logoutttest`  	| `logout, save(Myfile) word replace: ttable2 ...` 	| 分组 T 均值检验 **Word** 输出    	|
| `esttabttest`  	| `(estpost ttest ...)`和`(esttab ...)`            	| 分组 T 均值检验 **Word** 输出    	|
| `logoutttestl` 	| `logout, save(Myfile) tex replace: ttable2 ...`  	| 分组 T 均值检验 **LaTeX** 输出   	|
| `esttabttestl` 	| `(estpost ttest ...)`和`(esttab ...)`            	| 分组 T 均值检验 **LaTeX** 输出   	|
| `asdoccorr`    	| `asdoc cor ...`                                  	| 相关系数矩阵 **Word** 输出       	|
| `corrdocx`     	| `corr2docx ...`                                  	| 相关系数矩阵 **Word** 输出       	|
| `logoutcorr`   	| `logout, save(Myfile) word replace : pwcorr ...` 	| 相关系数矩阵 **Word** 输出       	|
| `esttabcorr`   	| `(estpost correlate ...)`和`(esttab ...)`        	| 相关系数矩阵 **Word** 输出       	|
| `logoutcorrl`  	| `logout, save(Myfile) tex replace : pwcorr ...`  	| 相关系数矩阵 **LaTeX** 输出      	|
| `esttabcorrl`  	| `(estpost correlate ...)`和`(esttab ...)`        	| 相关系数矩阵 **LaTeX** 输出      	|
| `asdocreg`     	| `asdoc reg ...`                                  	| 回归结果 **Word** 输出           	|
| `regdocx`      	| `reg2docx ...`                                   	| 回归结果 **Word** 输出           	|
| `outregreg`    	| `(reg ...)`和`(outreg2 using Myfile ...)`        	| 回归结果 **Word** 输出           	|
| `esttabreg`    	| `(reg ...)`和`(esttab ...)`                      	| 回归结果 **Word** 输出           	|
| `outregregl`   	| `(reg ...)`和`(outreg2 using Myfile ...)`        	| 回归结果 **LaTeX** 输出          	|
| `esttabregl`   	| `(reg ...)`和`(esttab ...)`                      	| 回归结果 **LaTeX** 输出          	|
| `esttab`       	| -                                                	| 导出所有表格至一个 **Word** 文件 	|
| `esttabl`      	| -                                                	| 导出所有表格至一个 **.tex** 文件 	|

> 注意：搜狗的快捷短语的关键字**只能是大小写英文字母**，不能有空格、数字、下划线等其他字符。


## 4. 相关附件
[所有搜狗自定义短语.do](https://gitee.com/Stata002/StataSX2018/raw/master/2019C/%E7%8E%8B%E7%BE%8E%E5%BA%AD_%E4%B8%AD%E5%8D%97%E6%B0%91%E6%97%8F/%E6%8E%A8%E6%96%873%EF%BC%9AStata:%20%E5%AE%9E%E8%AF%81%E7%BB%93%E6%9E%9C%E7%9A%84%E5%90%84%E7%A7%8D%E8%BE%93%E5%87%BA%E5%91%BD%E4%BB%A4+%E6%90%9C%E7%8B%97%E8%87%AA%E5%AE%9A%E4%B9%89%E7%9F%AD%E8%AF%AD-%E9%99%84%E4%BB%B6/%E6%89%80%E6%9C%89%E6%90%9C%E7%8B%97%E8%87%AA%E5%AE%9A%E4%B9%89%E7%9F%AD%E8%AF%AD.do)

> 搜狗短语设定方法参见： [「Stata+搜狗=？=效率！（搜狗自定义短语）」](https://www.jianshu.com/p/4c27c66757a8)


&emsp;


&emsp;


```stata
asdocsum,1=
*-----------------描述性统计 Word 输出(by asdoc)-------------------------------
local varlist "wage age race married grade collgrad south union occupation"
asdoc sum `varlist', save(Myfile.doc) replace ///
	stat(N mean sd min p50 max)  dec(3) ///
	title(asdoc_Table: Descriptive statistics)
/*
1. 在将结果导入到 word 的途中，Stata 界面也能看到相应的结果。
2. 该命令不支持中文。
3. 若有字符串变量，命令会报错。
*/

sumdocx,1=
*-----------------描述性统计 Word 输出(by sum2docx)-------------------------------
local varlist "wage age race married grade collgrad south union occupation"
sum2docx `varlist' using Myfile.docx,replace ///
	stats(N mean(%9.2f) sd(%9.3f) min(%9.2f) median(%9.2f) max(%9.2f)) ///
	title(sum2docx_Table: Descriptive statistics)
/*
1. 在将结果导入到 word 的途中，Stata 界面不能看到相应的结果。
2. 该命令支持中文。
3. 若有字符串变量，命令会报错。
4. 能分别设置每个统计量的小数点位数。
*/

outregsum,1=
*-----------------描述性统计 Word 输出(by outreg2)-------------------------------
local varlist "wage age race married grade collgrad south union occupation"
outreg2 using Myfile, sum(detail) replace word eqkeep(N mean sd min p50 max) ///
	fmt(f) keep(`varlist') sortvar(wage age grade) ///
	title(outreg2_Table: Descriptive statistics)
/*
1. 在将结果导入到 word 的途中，Stata 界面能看到相应的结果。
2. 该命令不支持中文。
3. 若有字符串变量，命令会在窗口说明什么变量是字符型，并在报告列表中自动剔除该变量。
4. 支持变量排序。
*/

esttabsum,1=
*-----------------描述性统计 Word 输出(by esttab)-------------------------------
local varlist "wage age race married grade collgrad south union occupation"
estpost summarize `varlist', detail
esttab using Myfile.rtf, ///
	cells("count mean(fmt(2)) sd(fmt(2)) min(fmt(2)) p50(fmt(2)) max(fmt(2))") ///
	noobs compress replace title(esttab_Table: Descriptive statistics)
/*
1. 在将结果导入到 word 的途中，Stata 界面能看到相应的结果。
2. 该命令不支持中文(有些教程说可以对中文进行 gb18030 转码后导入，但本文试验不成功)。
3. 若有字符串变量，命令仍会直接运行，在输出列表中字符型变量名会写上去，但后面的统计量为空白。
4. 能分别设置每个统计量的小数点位数。
*/

asdocttest,1=
*-----------------分组 T 均值检验 Word 输出(by asdoc)---------------------------
local common_exp "save(Myfile.doc) by(south) stat(obs mean p)"
asdoc ttest wage, `common_exp' replace title(asdoc_Table: T_test by group)
asdoc ttest age, `common_exp' rowappend 
asdoc ttest race, `common_exp' rowappend
asdoc ttest married, `common_exp' rowappend
asdoc ttest grade, `common_exp' rowappend
asdoc ttest collgrad, `common_exp' rowappend
asdoc ttest union, `common_exp' rowappend
/*
1. 每次只能检验一个变量, 当然它可以通过 rowappend 进行叠加，但这样麻烦不少。
2. 内容不完整，本身没有也无法添加 MeanDiff 一列。
*/

tdocx,1=
*-----------------分组 T 均值检验 Word 输出(by t2docx)--------------------------
local varlist "wage age race married grade collgrad union"
t2docx `varlist' using Myfile.docx,replace ///
	not by(south) title(t2docx_Table: T_test by group)
/*
1. 表格报告的内容剔除了所输入变量中包含缺漏值的观测值，且该操作无法被禁止。
2. 支持中文。
3. 有 MeanDiff 一列，并默认以"* p < 0.1 ** p < 0.05 *** p < 0.01"的方式标注星号。
*/

logoutttest,1=
*-----------------分组 T 均值检验 Word 输出(by logout)--------------------------
! taskkill /F /IM WINWORD.EXE /T
local varlist "wage age race married grade collgrad union"
logout, save(Myfile) word replace: ttable2 `varlist', by(south)
/*
1. 能一次性分组 T 均值检验所有变量
2. 有 MeanDiff 一列，并默认以"* p < 0.1 ** p < 0.05 *** p < 0.01"的方式标注星号
3. 该命令不能书写表格标题。
*/

esttabttest,1=
*-----------------分组 T 均值检验 Word 输出(by esttab)--------------------------
local varlist "wage age race married grade collgrad union"
estpost ttest `varlist', by(south)
esttab using Myfile.rtf, ///
	cells("N_1 mu_1(fmt(3)) N_2 mu_2(fmt(3)) b(star fmt(3))") starlevels(* 0.10 ** 0.05 *** 0.01) ///
	noobs compress replace title(esttab_Table: T_test by group) 
/*
1. 能一次性分组 T 均值检验所有变量。
2. 有 MeanDiff 一列，可设置以"* p < 0.1 ** p < 0.05 *** p < 0.01"的方式标注星号
3. 能分别设置每一列的小数点位数。
*/

asdoccorr,1=
*-----------------相关系数矩阵 Word 输出(by asdoc)------------------------------
local varlist "wage age race married grade collgrad"
asdoc cor `varlist', save(Myfile.doc) replace nonum dec(3) ///
	title(asdoc_Table: correlation coefficient matrix)
/*
1. 无法使用 asdoc pwcorr 命令，而简易的 asdoc cor 命令不能报告 p 值(显著性)，不能标星号。
*/

corrdocx,1=
*-----------------相关系数矩阵 Word 输出(by corr2docx)--------------------------
local varlist "wage age race married grade collgrad"
corr2docx `varlist' using Myfile.docx, replace spearman(ignore) ///
	star title(corr2docx_Table: correlation coefficient matrix)
/*
1. 支持中文
2. 不能报告 p 值，但能以"* p < 0.1 ** p < 0.05 *** p < 0.01"的方式标注星号
*/

logoutcorr,1=
*-----------------相关系数矩阵 Word 输出(by logout)------------------------------
! taskkill /F /IM WINWORD.EXE /T
local varlist "wage age race married grade collgrad"
logout, save(Myfile) word replace : pwcorr `varlist', ///
	star(0.05) listwise
/*
1. 能报告 p 值，能标注星号(只能设置一个显著性水平)。
2. 表格会有串行的问题，需要后期自己手动调整。
3. 不能设置表格标题。
*/	

esttabcorr,1=
*-----------------相关系数矩阵 Word 输出(by esttab)------------------------------
local varlist "wage age race married grade collgrad"
estpost correlate `varlist', matrix listwise
esttab using Myfile.rtf, ///
	unstack not noobs compress nogaps replace star(* 0.1 ** 0.05 *** 0.01) ///
	b(%8.3f) p(%8.3f) title(esttab_Table: correlation coefficient matrix)
/*
1. 能报告 p 值，可设置以"* p < 0.1 ** p < 0.05 *** p < 0.01"的方式标注星号。
*/

asdocreg,1=
*-----------------回归结果 Word 输出(by asdoc)------------------------------
asdoc reg wage age married occupation, save(Myfile.doc) nest replace ///
	cnames(OLS-1) rep(se) add(race, no)
asdoc reg wage age married collgrad occupation, save(Myfile.doc) nest append ///
	cnames(OLS-2) add(race, no)
asdoc reg wage age married collgrad occupation race_num*, save(Myfile.doc) nest append ///
	add(race, yes) cnames(OLS-3) dec(3) drop(occupation race_num*) ///
	stat(r2_a, F, rmse, rss) title(asdoc_Table: regression result)
/*
1. 可添加不同列的列名。
2. 没有诸如 indicate("race=race_num*") 的选项，需用 add(race, yes) 代替。
3. 不能对变量进行排序。
4. 表格布局与一般的文献不同，且不能用命令调整。
*/

regdocx,1=
*-----------------回归结果 Word 输出(by reg2docx)------------------------------
reg wage age married occupation
est store m1
reg wage age married collgrad occupation
est store m2
reg wage age married collgrad occupation race_num*
est store m3
reg2docx m1 m2 m3 using Myfile.docx, replace indicate("race=race_num*") ///
	b(%9.2f) se(%7.2f) scalars(r2(%9.3f) r2_a(%9.2f) N) ///
	drop(occupation) order(married) ///
	title(reg2docx_Table: regression result) ///
	mtitles("OLS-1" "OLS-2" "OLS-3") 
/*
1. 可添加不同列的列名
2. 有诸如 indicate("race=race_num*") 的选项。
3. 命令运行之后会将所有已经储存的回归结果清除，导致后面无法再继续使用前面的回归结果。
4. 支持中文。
*/

outregreg,1=
*-----------------回归结果 Word 输出(by outreg2)------------------------------
local subexp "nor2 noobs e(r2 r2_a F N) fmt(f) bdec(2) sdec(2) drop(occupation race_num*) sortvar(married)"
reg wage age married occupation
outreg2 using Myfile, word replace title(outreg2_Table: regression result) ///
	ctitle(OLS-1) `subexp' addtext(race, no)
	
reg wage age married collgrad occupation
outreg2 using Myfile, word append ctitle(OLS-2) `subexp' addtext(race, no)

reg wage age married collgrad occupation race_num*
outreg2 using Myfile, word append ctitle(OLS-3) `subexp' addtext(race, yes)
/*
1. 可添加不同列的列名。
2. 没有诸如 indicate("race=race_num*") 的选项，需用 addtext(race, yes) 代替。
*/

esttabreg,1=
*-----------------回归结果 Word 输出(by esttab)------------------------------
reg wage age married occupation
est store m1
reg wage age married collgrad occupation
est store m2
reg wage age married collgrad occupation race_num*
est store m3
esttab m1 m2 m3 using Myfile.rtf, ///
	replace star( * 0.10 ** 0.05 *** 0.01 ) nogaps compress ///
	order(married) drop(occupation) b(%20.3f) se(%7.2f)  ///
	r2(%9.3f) ar2 aic bic obslast scalars(F)  ///
	indicate("race=race_num*") mtitles("OLS-1" "OLS-2" "OLS-3") ///
	title(esttab_Table: regression result) 
/*
1. 可添加不同列的列名。
2. 有诸如 indicate("race=race_num*") 的选项。
3. 表格形式最符合一般文献报告表格的格式。
*/

outregsuml,1=
*------------------描述性统计 LaTeX 输出(by outreg2)---------------------------
local varlist "wage age race married grade collgrad south union occupation"
outreg2 using Myfile, sum(detail) replace tex eqkeep(N mean sd min p50 max) ///
	fmt(f) keep(`varlist') sortvar(wage age grade) ///
	title(outreg2_Table: Descriptive statistics)
/*
默认：输出一个完整的可以编译的但格式较为简易 .tex 文件，表格默认居左对齐。
outreg2 的 tex() 的专有选项：
1. fragment：只输出表格本身。
2. pretty：在默认的基础上增加了更为丰富的格式，并将表格居中对齐。
3. landscape：横置页面。
*/

logoutttestl,1=
*-----------------分组 T 均值检验 LaTeX 输出(by logout)-------------------------
local varlist "wage age race married grade collgrad union"
logout, save(Myfile) tex replace: ttable2 `varlist', by(south)
/*
1. LaTeX 输出不能增加表格标题。
2. logout 的 tex 没有选项，只有默认形式。
*/

logoutcorrl,1=
*-----------------相关系数矩阵 LaTeX 输出(by logout)-------------------------
local varlist "wage age race married grade collgrad"
logout, save(Myfile) tex replace : pwcorr `varlist', ///
	star(0.05) listwise
/*
1. 表格会有串行的问题，需要后期自己手动调整。
2. LaTeX 输出不能增加表格标题。
3. logout 的 tex 没有选项，只有默认形式。
*/

outregregl,1=
*-----------------回归结果 LaTeX 输出(by outreg2)-------------------------
local subexp "tex nor2 noobs e(r2 r2_a F N) fmt(f) bdec(2) sdec(2) drop(occupation race_num*) sortvar(married)"
reg wage age married occupation
outreg2 using Myfile, replace title(outreg2_Table: regression result) ///
	ctitle(OLS-1) `subexp' addtext(race, no)
reg wage age married collgrad occupation
outreg2 using Myfile, append ctitle(OLS-2) `subexp' addtext(race, no)
reg wage age married collgrad occupation race_num*
outreg2 using Myfile, append ctitle(OLS-3) `subexp' addtext(race, yes)
/*
默认：输出一个完整的可以编译的但格式较为简易 .tex 文件，表格默认居左对齐。
outreg2 的 tex() 的专有选项：
1. fragment：只输出表格本身。
2. pretty：在默认的基础上增加了更为丰富的格式，并将表格居中对齐。
3. landscape：横置页面。
*/

esttabsuml,1=
*------------------描述性统计 LaTeX 输出(by esttab)---------------------------
local varlist "wage age race married grade collgrad south union occupation"
estpost summarize `varlist', detail
esttab using Myfile.tex, ///
	cells("count mean(fmt(2)) sd(fmt(2)) min(fmt(2)) p50(fmt(2)) max(fmt(2))") ///
	noobs compress replace title(esttab_Table: Descriptive statistics) ///
	booktabs  page(array, makecell) alignment(cccccc) width(\hsize)
/*
esttab 的 LaTeX 输出的专有选项：
1. booktabs: 用 booktabs 宏包输出表格(三线表格)。
2. page[(packages)]: 创建完成的 LaTeX 文档以及添加括号里的宏包
3. 如果写了 booktabs 选项，则 page[(packages)] 将自动添加\usepackage{booktabs}。
4. alignment(cccccc)：定义从第二列开始的列对齐方式(默认居中)。
5. width(\hsize)：可以使得表格宽度为延伸至页面宽度
6. fragment：不输出表头表尾，只输出表格本身内容，其不能与 page[(packages)] 选项共存。
*/

esttabttestl,1=
*-----------------分组 T 均值检验 LaTeX 输出(by esttab)-------------------------
local varlist "wage age race married grade collgrad union"
estpost ttest `varlist', by(south)
esttab using Myfile.tex, ///
	cells("N_1 mu_1(fmt(3)) N_2 mu_2(fmt(3)) b(star fmt(3))") starlevels(* 0.10 ** 0.05 *** 0.01) ///
	noobs compress replace title(esttab_Table: T_test by group) ///
	booktabs  page width(\hsize)
/*
esttab 的 LaTeX 输出的专有选项：
1. booktabs: 用 booktabs 宏包输出表格(三线表格)。
2. page[(packages)]: 创建完成的 LaTeX 文档以及添加括号里的宏包
3. 如果写了 booktabs 选项，则 page[(packages)] 将自动添加\usepackage{booktabs}。
4. alignment(cccccc)：定义从第二列开始的列对齐方式(默认居中)。
5. width(\hsize)：可以使得表格宽度为延伸至页面宽度
6. fragment：不输出表头表尾，只输出表格本身内容，其不能与 page[(packages)] 选项共存。
*/

esttabcorrl,1=
*-----------------相关系数矩阵 LaTeX 输出(by esttab)-------------------------
local varlist "wage age race married grade collgrad"
estpost correlate `varlist', matrix listwise
esttab using Myfile.tex, ///
	unstack not noobs compress nogaps append star(* 0.1 ** 0.05 *** 0.01) ///
	b(%8.3f) p(%8.3f) title(esttab_Table: correlation coefficient matrix) ///
	booktabs  page width(\hsize)
/*
esttab 的 LaTeX 输出的专有选项：
1. booktabs: 用 booktabs 宏包输出表格(三线表格)。
2. page[(packages)]: 创建完成的 LaTeX 文档以及添加括号里的宏包
3. 如果写了 booktabs 选项，则 page[(packages)] 将自动添加\usepackage{booktabs}。
4. alignment(cccccc)：定义从第二列开始的列对齐方式(默认居中)。
5. width(\hsize)：可以使得表格宽度为延伸至页面宽度
6. fragment：不输出表头表尾，只输出表格本身内容，其不能与 page[(packages)] 选项共存。
*/

esttabregl,1=
*-----------------回归结果 LaTeX 输出(by esttab)-------------------------
reg wage age married occupation
est store m1
reg wage age married collgrad occupation
est store m2
reg wage age married collgrad occupation race_num*
est store m3
esttab m1 m2 m3 using Myfile.tex, ///
	append star( * 0.10 ** 0.05 *** 0.01 ) nogaps compress ///
	order(married) drop(occupation) b(%20.3f) se(%7.2f)  ///
	r2(%9.3f) ar2 aic bic obslast scalars(F)  ///
	indicate("race=race_num*") mtitles("OLS-1" "OLS-2" "OLS-3") ///
	title(esttab_Table: regression result) booktabs  page width(\hsize)
/*
esttab 的 LaTeX 输出的专有选项：
1. booktabs: 用 booktabs 宏包输出表格(三线表格)。
2. page[(packages)]: 创建完成的 LaTeX 文档以及添加括号里的宏包
3. 如果写了 booktabs 选项，则 page[(packages)] 将自动添加\usepackage{booktabs}。
4. alignment(cccccc)：定义从第二列开始的列对齐方式(默认居中)。
5. width(\hsize)：可以使得表格宽度为延伸至页面宽度
6. fragment：不输出表头表尾，只输出表格本身内容，其不能与 page[(packages)] 选项共存。
*/


esttab,1=
*--------------------四张表输出至一个 Word 文件里(by esttab)------------------
! taskkill /F /IM WINWORD.EXE /T
*描述性统计输出
local varlist "wage age race married grade collgrad south union occupation"
estpost summarize `varlist', detail
esttab using Myfile.rtf, ///
	cells("count mean(fmt(2)) sd(fmt(2)) min(fmt(2)) p50(fmt(2)) max(fmt(2))") ///
	noobs compress replace title(esttab_Table: Descriptive statistics)
/*
1. 在将结果导入到 word 的途中，Stata 界面能看到相应的结果(下同)。
2. 该命令不支持中文(有些教程说可以对中文进行 gb18030 转码后导入，但本文试验不成功)。
3. 若有字符串变量，命令仍会直接运行，在输出列表中字符型变量名会写上去，但后面的统计量为空白。
4. 能分别设置每个统计量的小数点位数。
*/

*分组 T 均值检验输出
local varlist "wage age race married grade collgrad union"
estpost ttest `varlist', by(south)
esttab using Myfile.rtf, ///
	cells("N_1 mu_1(fmt(3)) N_2 mu_2(fmt(3)) b(star fmt(3))") starlevels(* 0.10 ** 0.05 *** 0.01) ///
	noobs compress append title(esttab_Table: T_test by group) 
/*
1. 能一次性分组 T 均值检验所有变量。
2. 有 MeanDiff 一列，可设置以"* p < 0.1 ** p < 0.05 *** p < 0.01"的方式标注星号
3. 能分别设置每一列的小数点位数。
*/

*相关系数矩阵输出
local varlist "wage age race married grade collgrad"
estpost correlate `varlist', matrix listwise
esttab using Myfile.rtf, ///
	unstack not noobs compress append nogaps star(* 0.1 ** 0.05 *** 0.01) ///
	b(%8.3f) p(%8.3f) title(esttab_Table: correlation coefficient matrix)
/*
1. 能报告 p 值，可设置以"* p < 0.1 ** p < 0.05 *** p < 0.01"的方式标注星号。
*/

*回归结果输出
reg wage age married occupation
est store m1
reg wage age married collgrad occupation
est store m2
reg wage age married collgrad occupation race_num*
est store m3
esttab m1 m2 m3 using Myfile.rtf, ///
	compress append star( * 0.10 ** 0.05 *** 0.01 ) nogaps ///
	order(married) drop(occupation) b(%20.3f) se(%7.2f)  ///
	r2(%9.3f) ar2 aic bic obslast scalars(F)  ///
	indicate("race=race_num*") mtitles("OLS-1" "OLS-2" "OLS-3") ///
	title(esttab_Table: regression result) 
/*
1. 可添加不同列的列名。
2. 有诸如 indicate("race=race_num*") 的选项。
3. 表格形式最符合一般文献报告表格的格式。
*/


esttabl,1=
*----------------------四张表输出至一个 tex 文件中(by esttab)------------------
*描述性统计输出
local varlist "wage age race married grade collgrad south union occupation"
estpost summarize `varlist', detail
esttab using Myfile.tex, ///
	cells("count mean(fmt(2)) sd(fmt(2)) min(fmt(2)) p50(fmt(2)) max(fmt(2))") ///
	noobs compress replace title(esttab_Table: Descriptive statistics) ///
	booktabs  page(array, makecell) alignment(cccccc) width(\hsize)
/*
esttab 的 LaTeX 输出的专有选项：
1. booktabs: 用 booktabs 宏包输出表格(三线表格)。
2. page[(packages)]: 创建完成的 LaTeX 文档以及添加括号里的宏包
3. 如果写了 booktabs 选项，则 page[(packages)] 将自动添加\usepackage{booktabs}。
4. alignment(cccccc)：定义从第二列开始的列对齐方式(默认居中)。
5. width(\hsize)：可以使得表格宽度为延伸至页面宽度
6. fragment：不输出表头表尾，只输出表格本身内容，其不能与 page[(packages)] 选项共存。
*/


*分组 T 均值检验输出
local varlist "wage age race married grade collgrad union"
estpost ttest `varlist', by(south)
esttab using Myfile.tex, ///
	cells("N_1 mu_1(fmt(3)) N_2 mu_2(fmt(3)) b(star fmt(3))") starlevels(* 0.10 ** 0.05 *** 0.01) ///
	noobs compress append title(esttab_Table: T_test by group) ///
	booktabs  page width(\hsize)
/*
1. 这里的 page 选项可以将本部分附着在先前的 document 环境中，从而形成一个整体。
*/


*相关系数矩阵输出
local varlist "wage age race married grade collgrad"
estpost correlate `varlist', matrix listwise
esttab using Myfile.tex, ///
	unstack not noobs compress append nogaps star(* 0.1 ** 0.05 *** 0.01) ///
	b(%8.3f) p(%8.3f) title(esttab_Table: correlation coefficient matrix) ///
	booktabs  page width(\hsize)


*回归结果输出
reg wage age married occupation
est store m1
reg wage age married collgrad occupation
est store m2
reg wage age married collgrad occupation race_num*
est store m3
esttab m1 m2 m3 using Myfile.tex, ///
	append star( * 0.10 ** 0.05 *** 0.01 ) nogaps compress ///
	order(married) drop(occupation) b(%20.3f) se(%7.2f)  ///
	r2(%9.3f) ar2 aic bic obslast scalars(F)  ///
	indicate("race=race_num*") mtitles("OLS-1" "OLS-2" "OLS-3") ///
	title(esttab_Table: regression result) booktabs  page width(\hsize)
*小结
/*
1. 这里所有的表格都可以生成标题，也没有串行的问题。
2. 与 out_log 不一样，esttab 能形成一个整体，所有的表格都能完美的 append 在一起，
并且可以设置生成导言区和正文区的 document 环境。所以以上命令生成的 .tex 文件可以
直接拿去编译，并生成所有表格，一步到位。之后我们再统一做一些微调就可以放在 LaTeX
正文中了。
*/
```