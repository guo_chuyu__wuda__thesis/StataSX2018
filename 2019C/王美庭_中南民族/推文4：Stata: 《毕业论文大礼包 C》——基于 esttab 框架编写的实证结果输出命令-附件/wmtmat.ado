* Feather: output matrix to Stata interface, Word and LaTeX
* Author: Meiting Wang, Master, School of Economics, South-Central University for Nationalities
* Email: 2017110097@mail.scuec.edu.cn
* Created on Oct 26th, 2019
* need ssc's command "findname"

program define wmtmat
version 15.1

syntax name(id="a matrix name") [using/] [, ///
	replace append FMT(string) ROWFMT(string) COLFMT(string) ///
	TItle(string) Alignment(string)]

*--------设置默认格式------------
local default_fmt "%11.3f"
local default_la_fmt "%11.3fc"


*---------程序不合规时的报错-----------
if ("`replace'`append'"!="")&("`using'"=="") {
	dis "{error:replace or append can't appear when you don't need to output result to a file.}"
	exit
}

if ("`replace'"!="")&("`append'"!="") {
	dis "{error:replace and append cannot appear at the same time.}"
	exit
}

if ("`fmt'"!="")&("`rowfmt'"!="") {
	dis "{error:fmt and rowfmt can't appear at the same time.}"
	exit
}
if ("`fmt'"!="")&("`colfmt'"!="") {
	dis "{error:fmt and colfmt can't appear at the same time.}"
	exit
}
if ("`rowfmt'"!="")&("`colfmt'"!="") {
	dis "{error:rowfmt and colfmt can't appear at the same time.}"
	exit
}

if "`fmt'" != "" {
	local fmt_num: word count `fmt'
	if `fmt_num' != 1 {
		dis "{error:fmt needs to be a single word.}"
		exit
	}
}
if "`rowfmt'" != "" {
	local rowfmt_num: word count `rowfmt'
	if `rowfmt_num' != rowsof(`namelist') {
		dis "{error:the number of words in rowfmt() can't match the number of rows in the matrix `namelist'.}"
		exit
	}
}
if "`colfmt'" != "" {
	local colfmt_num: word count `colfmt'
	if `colfmt_num' != colsof(`namelist') {
		dis "{error:the number of words in colfmt() can't match the number of cols in the matrix `namelist'.}"
		exit
	}
}

if (~ustrregexm("`using'",".tex"))&("`alignment'"!="") { 
	dis "{error:alignment can only be used in the LaTeX output.}"
	exit
}


*---------前期语句处理----------
*普通选项语句的处理
if "`using'" != "" {
	local us_ing "using `using'"
}
if "`title'" == "" {
	local title "Matrix `namelist'"
}

*fmt系列语句构建
if "`fmt'" != "" {
	local st_fmt "fmt(`fmt')"
	local st_fmt_la "fmt(`fmt')"
}
else if "`rowfmt'" != "" {
	local st_fmt `"fmt("`rowfmt'")"'
	local st_fmt_la `"fmt("`rowfmt'")"'
}
else if "`colfmt'" != "" {
	local st_fmt "fmt(`colfmt')"
	local st_fmt_la "fmt(`colfmt')"
}
else {
	local st_fmt "fmt(`default_fmt')"
	local st_fmt_la "fmt(`default_la_fmt')"
} //默认值

*构建esttab中alignment()和page()内部的语句(LaTeX输出专属)
if "`alignment'" == "math" {
	local page "array"
	local alignment "*{`=colsof(`namelist')'}{>{$}c<{$}}"
	
}
else {
	local page "array,dcolumn"
	local alignment "*{`=colsof(`namelist')'}{D{.}{.}{-1}}"
}


*-----------------主程序---------------
esttab matrix(`namelist', `st_fmt'), compress ///
	nomtitles title(`title')  //Stata 界面显示
if ustrregexm("`us_ing'",".rtf") {
	esttab matrix(`namelist', `st_fmt') `us_ing', compress `replace'`append' ///
		nomtitles title(`title')  //Word 显示
}
if ustrregexm("`us_ing'",".tex") {
	local col_names: colnames `namelist'
	tokenize "`col_names'"
	local i = 1
	local col_new_names ""
	while "``i''" != "" {
		local col_new_names "`col_new_names'\multicolumn{1}{c}{``i''} "
		local `i' "" //置空`i'
		local i = `i' + 1
	}
	mat colnames `namelist' = `col_new_names'
	esttab matrix(`namelist', `st_fmt_la') `us_ing', compress `replace'`append' ///
		nomtitles title(`title')  booktabs width(\hsize) page(`page') ///
		alignment(`alignment') 
	mat colnames A = `col_names'
} //LaTeX 显示
end
