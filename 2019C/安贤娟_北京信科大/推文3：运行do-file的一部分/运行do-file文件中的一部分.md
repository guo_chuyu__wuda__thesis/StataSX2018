作者：安贤娟（北京信息科技大学）

「**Source：**[Running sections of do-files.](https://acarril.github.io/posts/run-dofile-section)」

### **运行 do-file 的一部分**


如今 do-files 越来越大、越来越复杂，而有时只是想运行代码的一部分。这些年来，我广泛使用我所称的“ do-switches”，这是一个用来打开或关闭代码某些部分的便利设备。

## 1. 问题
我们都经历过这样一个过程：

1. 错误！r（xxx）

然后——

1.修正一小部分代码

2.在大段的 do-file 中选择一段代码并运行它

（重复上述步骤）

## 2.进入 do-switch
思路非常简单:如果您想运行代码的某个部分，只需将该部分的 local 设置为 1（否则设置为 0 或其他值）。然后，用条件块将要打开或关闭的代码段括起来。

这样，如果你只处理“ B ”部分，那么你可以将除“ B ”之外的所有开关设置为 0，然后运行整个 do-file，而无需手动选择其中的一部分。

```stata
local A     0
local B     0
local merge 0

if `A' == 1 {
  * Import and save A
}

if `B' == 1 {
  * Import and save B
}

if `merge' == 1 {
  * Merge A with B and save
}
```

## 3.实例展示
在这个简单的例子中，只选择处理导入的“ A ”，将其设置为 1，“ B ”和“ marge ”开关设置为 0，然后运行全部的 do-file。

```stata
sysuse "nlsw88.dta"
 global y "wage"       
global x "hours tenure married collgrad"   
local A     1
local B     0
local merge 0

if `A' == 1 {
    reg $y $x          
}

if `B' == 1 {
    reg $y $x i.race i.industry
}
if `merge' == 1 {
    reg $y $x           
    reg $y $x i.race i.industry
}
```

**运行结果展示**

结果中只显示了打开开关的“ A ”部分，而“ B ”和“ merge ”部分均没有显示。

```stata
if `A' == 1 {
.     reg $y $x          

      Source |       SS           df       MS      Number of obs   =     2,227
-------------+----------------------------------   F(4, 2222)      =     67.82
       Model |  8053.67746         4  2013.41937   Prob > F        =    0.0000
    Residual |  65967.1331     2,222  29.6881787   R-squared       =    0.1088
-------------+----------------------------------   Adj R-squared   =    0.1072
       Total |  74020.8106     2,226  33.2528349   Root MSE        =    5.4487

------------------------------------------------------------------------------
        wage |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
       hours |   .0616148   .0113148     5.45   0.000     .0394262    .0838034
      tenure |   .1439428   .0212953     6.76   0.000      .102182    .1857037
     married |   -.273562   .2436884    -1.12   0.262    -.7514427    .2043187
    collgrad |   3.330943   .2730949    12.20   0.000     2.795395    3.866491
       _cons |   4.029129   .4833735     8.34   0.000     3.081218     4.97704
------------------------------------------------------------------------------
. }

. if `B' == 1 {
.     reg $y $x i.race i.industry
. }

. if `merge' == 1 {
.     reg $y $x           
.     reg $y $x i.race i.industry
. }
```

 _**其他有关 do-file 的介绍与应用可以详见推文**_ 「[Stata 中 Do-file 编辑器的使用](https://www.jianshu.com/p/567947f53955)」