## **面板数据模型**

常见的数据形式有时间序列数据( Time series data )，截面数据( Cross-sectional data )和面板数据( Panel data )。
从维度来看，时间序列数据和截面数据均为一维。面板数据可以看做为时间序列与截面混合数据，因此它是二维数据。数据形式如下：

![](https://images.gitee.com/uploads/images/2018/1108/100208_80983544_2269686.png "Snipaste_2018-11-08_10-01-10.png")


世界是复杂的，所表现出来的行为特征也是复杂的，我们需要面板数据。例如，欲研究影响企业利润的决定因素，我们
认为企业规模 (截面维度)和技术进步(时间维度)是两个重要的因素。截面数据仅能研究企业规模对企业利润的影响程度，时间序列数据
仅能研究技术进步对企业利润的影响，而面板数据同时考虑了截面和时间两个维度(^^从哪个维度看都好看)，可以同时研究企业规模和
技术进步对企业利润的影响。

正因为面板数据所具有的独特优势，许多模型从截面数据扩展到面板数据框架下。通过利用 `findit panel data`，可以
发现目前Stata已有许多相关面板数据模型命令，包括(不限于)：

&#8195;    `xtreg` :普通面板数据模型，包括固定效应与随机效应

&#8195;    `xtabond/xtdpdsys/xtabond2/xtdpdqml/xtlsdvc`：动态面板数据模型

&#8195;    `spxtregress/xsmle`: 空间面板数据模型

&#8195;    `xthreg`:面板门限模型

&#8195;    `xtqreg/qregpd/xtrifreg`: 面板分位数模型

&#8195;    `xtunitroot`: 面板单位根检验

&#8195;    `xtcointtest/ xtpedroni/xtwest`: 面板协整检验

&#8195;    `sfpanel`: 面板随机前沿模型

&#8195;     `xtpmg/xtmg`：非平稳异质面板模型

本文主要就普通静态面板数据模型进行介绍，包括模型形式设定、模型分类与选择及 Stata 程序实现等。

### **一. 模型形式设定**

面板数据模型同时包含了截面和时间两个维度，设$i$ ($i$=1, $\cdots$, $N$)表示截面(个体)，$t$ ($t$=1, $\cdots$, $T$)表示
时间，设定如下线性模型：
$$ 
y_{it} = \alpha_{i} + \lambda_{t} + \mathbf{x_{it}}\beta + \epsilon_{it} 
$$

其中：$y_{it}$为$N\times 1$因变量, $\mathbf{x_{it}}$为$N\times k$自变量，$\epsilon_{it}$为模型误差项，$\beta$是待估计参数，
表示$\mathbf{x_{it}}$对$y_{it}$的边际影响。$\alpha_{i}$表示个体效应，表示那些不随时间改变的影响因素，如个人的消费习惯、企业
文化和经营风格等；\lambda_{t}表示时间效应，用于控制随时间改变因素的影响(时间虚拟变量包括时间趋势项，时间趋势主要用于
控制技术进步)，如广告的投放(往往通过电视或广播，我们可以认为在特定的年份所有个体所接受的广告投放量相同)。而这些因素在多数情况
下都是无法直接观测或难以量化而无法进入我们的模型，在截面分析中者往往会引起遗漏变量的问题。面板数据模型的主要用途之一就在于
处理这些不可观测的个体效应或时间效应。当对所有的$i$, $\alpha_{i}$均相等时，模型退化为混合数据模型 (Pooled OLS)，可直接用`reg`
命令进行参数估计。

根据个体数$N$和时期数$T$的大小，通常可以将面板数据分为**宏观面板**和**微观面板**：宏观面板一般为大$T$小$N$，微观面板一般为小$T$大$N$。
依据$N$、$T$大小不同，所采用的参数估计方法也不尽相同。

### **二. 模型分类与选择**

面板数据模型可以分为固定效应( Fixed effect model )和随机效应模型( Random effect model )。当 $\alpha_{i}$和$\mathbf{x_{it}}$相关，
即corr($\alpha_{i}$,$\mathbf{x_{it}}$) $\neq$ 0，则该模型为固定效应模型；反之为随机效应模型。两种模型的差异主要反映
在对“个体效应”的处理上。固定效应模型假设个体效应在组内是固定不变的，个体间的差异反映在每个个体都有一个特定的截距项上； 随机效应模型则假设所有的个体具有相同的截距项， 个体间的差异是随机的，这些差异主要反应在随机干扰项的设定上。基于此，
一种常见的观点认为， 当我们的样本来自一个较小的母体时，我们应该使用固定效应模型，而当样本来自一个很大的母体时， 应当采用随机效应模型。

然而，在具体的实例应用中，大母体和小母体并没有一个严格的界限，我们并不能明确地区分我们的样本来自一个较大母体还是较小的母体。因此有些学者认为，区分固定效应模型和随机效应模型应当通过检验使用二者的假设条件是否满足。下面我们讨论混合数据模型、固定效应模型和随机效应模型的选择。

#### 2.1、固定效应的检验
固定效应的检验本质即检验个体间截距项的差异是否显著，即$\alpha_{1}$=$\alpha_{2}$=$\cdots$=$\alpha_{N}$=0。根据假设检验原理，设定如下原假设
$$
H_{0}:\alpha_{1}=\alpha_{2}=\cdots=\alpha_{N-1}=0
$$
若结果拒绝原假设，则表明个体间截距项存在显著差异，模型中需要考虑固定效应。反之，混合 OLS 模型更为合适。通常可以利用$F$统计量来检验
上述假设是否成立：

$$
F=\frac{(R_{u}^2-R_{r}^2)/(N-1)}{(1-R_{u}^2)/(NT-N-K)}\sim F(N-1,NT-N-K)
$$
其中：$R_{u}^2$为固定效应模型的拟合优度系数(不受约束模型)，$R_{r}^2$为混合数据模型的拟合优度系数(受约束模型)；$N$,$T$分别为截面与时期数；$K$为解释变量个数。若原假设被拒绝，则说明个体效应显著，固定效应模型比混合数据模型更优。同理，可以构造相似的$F$统计量检验时期效应是否显著。

#### 2.2、随机效应的检验
Breusch和Pagan (1980)提出了基于面板随机效应模型残差的**LM**统计量，构造如下原假设来检验随机效应：
$$
H_{0}: \sigma_{\alpha}^2=0; H_{1}: \sigma_{\alpha}^2\neq0; 
$$
相应的检验统计量**LM**为：
$$
LM=\frac{NT}{2(T-1)}[\frac{\sum_{i=1}^{N} [\sum_{t=1}^{T}{e_{it}}]^2}{\sum_{i=1}^{N} [\sum_{t=1}^{T}{e_{it}^2}]}]^2
$$
在原假设下，该统计量服从自由度为1的卡方分布。若拒绝原假设则表明存在随机效应。

#### 2.3、固定效应还是随机效应？

通过检验说明个体效应($\alpha_{i}$)需要被纳入到模型中后，应该将$\alpha_{i}$看成随机干扰项的一部分(随机效应模型)还是待估计参数
(固定效应模型)，下面介绍一些基本方法。

##### (1) **Hausman** 检验

从基本定义出发，可以通过通过检验个体效应$\alpha_{i}$与其它解释变量是否相关作为进行固定效应和随机效应模型筛选的依据。此时，我们可以采用 **Hausman** 检验。其基本思想是:在$\alpha_{i}$和其他解释变量不相关假定下，采用组内变换法估计固定效应模型和采用GLS法随机效应模型得到的参数估计都是无偏且一致的，只是前者不具有效性。若原假设不成立，则固定效应模型的参数估计仍然是一致的，但随机效应模型不一致。因此，在原假设下，二者的参数估计应该不会有显著的差异， 可以基于二者参数估计的差异构造统计检验量。

假设$\beta_{within}$为固定效应模型的组合估计，$\beta_{GLS}$为随机效应模型的**GLS**估计。在原假设成立下，有
$$
cov(\beta_{GLS},\beta_{GLS}-\beta_{within})=0                  
$$
根据方差公式
$$
Var(\beta_{within}-\beta_{GLS})=Var(\beta_{within}) + Var(\beta_{GLS})-2cov(\beta_{within},\beta_{GLS})
$$
又因为$Var(\beta_{GLS})=cov(\beta_{within},\beta_{GLS})$
因此有
$$
Var(\beta_{within}-\beta_{GLS})=Var(\beta_{within}) - Var(\beta_{GLS})=\Psi
$$
**Hausman** 检验基于如下 **Wald** 统计量
$$
W=(\beta_{within}-\beta_{GLS})'\hat\Psi^{-1}(\beta_{within}-\beta_{GLS})\sim\chi^2(K-1)
$$
若拒绝原假设，表明个体效应$\alpha_{i}$和解释变量相关，此时随机效应模型的结果不一致，应选择固定效应模型。

##### (2) **Mundlak’s (1978)**  方法

在原假设成立情况下，估计量的有效性假设(存在最小渐近方差)是运用**Hausman**检验的前提条件。然而，当误差项存在异方差或者序列相关时，这个条件往往不能够被满足。即使在这个条件满足情况下，该方法也可能存在小样本问题。为了解决这一问题，**Stata**提供了两个命令：第一，利用**subset**实现一般性的**Hausman**检验；第二，利用稳健版本的**Hausman**检验方法 `rhausman` ( Boris Kaiser )，该方法采用**bootstrap**技术解决
不满足异方差和序列相关假设问题。 

本部分介绍另外一种方法，即 ** Mundlak’s (1978)** 提出的一种检验方法。与传统的 **Hausman**检验不同，该方法在误差项不满足同方差和序列
不相关情况下也是有效的。
设定如下线性模型：
$$ 
y_{it} = \alpha_{i} + \mathbf{x_{it}}\beta + \epsilon_{it} 
$$
**Mundlak** 方法的思想为检验$\alpha_{i}$和解释变量$\mathbf{x_{it}}$是否存在相关。因此，建立如下关系式：


$\alpha_{i} = \overline x_{i}\theta+\upsilon_{i}$


$E(\alpha_{i}|x_{i})=\overline x_{i}\theta$

其中：$\overline x_{i}$是$\mathbf{x_{it}}$的组内平均，$\upsilon_{i}$为时不变的，且与自变量不相关的。

要保证$\alpha_{i}$和解释变量$\mathbf{x_{it}}$不存在相关，只需$\theta$=0。
根据以上式子，可以转化为检验如下方程的系数
$$ 
y_{it} = \mathbf{x_{it}}\beta  + \overline x_{i}\theta+\upsilon_{i} + \epsilon_{it} 
$$
因此，只需要回归这个方程，并检验$H_{0}:\theta=0$是否成立。若拒绝原假设，则$\alpha_{i}$和解释变量$\mathbf{x_{it}}$存在相关，应选择固定效应模型。

### **三. 模型实现**

本部分以 Kleiber 和 Zeileis (2008)的**Grunfeld.dta**数据集为例，说明运用面板数据模型的一般步骤。

#### 3.1. 读取数据与面板数据设定

```
 webuse grunfeld,clear //利用webuse从网络读取数据grunfeld.dta，webuse可以看成website + use
 list in 1/10          // 显示该数据集的前10行
 
```
![](https://images.gitee.com/uploads/images/2018/1109/202702_0499f9a1_2269686.png "Snipaste_list.png")
 
```
xtset company year,yearly //设置面板数据格式，利用 Stata 中`xt`开头的命令，必须用该命令进行设置。yearly表示年度数据，详细参考 `help xtset`
```
![](https://images.gitee.com/uploads/images/2018/1109/203029_0ea44298_2269686.png "xtset.png")

#### 3.2. 模型检验与模型选择

以**invest**为因变量，**mvalue kstock**为自变量，建立如下模型：
$$
invest_{it} = \alpha_{i} + \lambda_{t} + \beta_{1}mvalue_{it} + \beta_{2}kstock_{it}+\epsilon_{it} 
$$
其中：$\beta_{1}$和$\beta_{2}$位待估系数。

利用**Stata**中 `xtreg` 可以方便实现面板固定效应模型与面板随机效应模型的估计：

```
xtreg invest mvalue kstock,fe //fe表示固定效应;若同时包括时期虚拟变量，xtreg invest mvalue kstock i.year,fe，利用 testparm 检验                                   
```
![](https://images.gitee.com/uploads/images/2018/1109/204424_deaa6938_2269686.png "Fstat.png")


```
xtreg invest mvalue kstock,re //re表示随机效应
```

![](https://images.gitee.com/uploads/images/2018/1109/205258_af7849fb_2269686.png "RE.png")

```
xttest0  //检验随机效应是否显著，需要运行随机效应模型后使用
```

![](https://images.gitee.com/uploads/images/2018/1109/205316_e8175e83_2269686.png "BP.png")


上述结果说明了有必要考虑个体效应和随机效应，接下来利用`hausman` 命令进行固定效应模型和随机效应模型的选择，主要步骤为：

 . **步骤一**：估计固定效应模型，存储估计结果；
 
 . **步骤二**：估计随机效应模型，存储估计结果;
 
 . **步骤三**：进行**Hausman**检验；
 
 
利用`hausman` 命令之前，有必要对其语法进行说明：

```
                         hausman name-consistent [name-efficient] [, options]
```

![](https://images.gitee.com/uploads/images/2018/1109/210616_2a1f684e_2269686.png "Hausman.png")

接下来进行**hausman**检验，

```
xtreg invest mvalue kstock,fe
est store fe_result
xtreg invest mvalue kstock,re
est store re_result
hausman fe_result re_result
```

![](https://images.gitee.com/uploads/images/2018/1109/211333_c42bcd4e_2269686.png "Hausman_results.png")


前文已经说明，当模型误差项存在序列相关或异方差时，此时经典的`hausman`检验不在适用，下面我们进行序列相关和异方差检验。

**序列相关检验**

先进行序列相关检验，在固定效应模型时可以利用命令`xtserial`，原假设为不存在序列相关。

```
xtserial invest mvalue kstock

```
![](https://images.gitee.com/uploads/images/2018/1109/212126_896e959b_2269686.png "xtserial.png")


同样地，在随机效应时可以利用命令`xttest1`，原假设为不存在序列相关。

![](https://images.gitee.com/uploads/images/2018/1109/230805_df4c8b5d_2269686.png "RE2.png")


**异方差检验**

Greene (2000, p598) 提出一种修正的Wald统计量检验异方差，与标准的Wald统计量、LR和LM统计量不同，修正Wald检验同样适用于模型残差不服从
正态分布情况下。值得一提的是，在大$N$小$T$情况下，该方法的检验功效较低。该检验的原假设为同方差。

```
xtreg invest mvalue kstock,fe
xttest3
```
![](https://images.gitee.com/uploads/images/2018/1109/233031_ac40aa7a_2269686.png "xttest3.png")


**稳健hausman检验**
由于存在序列相关和异方差，经典的`hausman`命令不再适用，下面使用稳健版的`hausman`检验命令`rhausman`进行检验。


```
xtreg invest mvalue kstock,fe
est store fe_result
xtreg invest mvalue kstock,re
est store re_result
rhausman fe_result re_result,reps(200) cluster
```

![](https://images.gitee.com/uploads/images/2018/1109/233840_ac015d76_2269686.png "rhausman.png")

**Mundlak’s (1978)法**

根据上文所述原理，可通过如下三个步骤实现该方法：

**第一**：计算解释变量$ \mathbf{x_{it}}=(mvalue_{it},kstock_{it})$均值
local xlist "mvalue kstock" 
foreach f of local xlist{
bysort company: egen mean_`f' = mean(`f')
}

**第二步**:估计包含均值的回归方程： 
$$
y_{it} = \mathbf{x_{it}}\beta  + \overline x_{i}\theta+\upsilon_{i} + \epsilon_{it} 
$$

```
xtreg invest mvalue kstock mean_mvalue mean_kstock,re vce(robust)
est store Mundlak_result
```

**第三步**:利用`test`进行假设检验

```
test mean_value mean_kstock
```

结果如下

![](https://images.gitee.com/uploads/images/2018/1110/063156_69004f9e_2269686.png "M.png")

从上述三种检验结果可以发现，利用经典的`hausman` 和稳健版`hausman`均显示应该选择随机效应模型，而利用**Mundlak**结果显示选择固定效应模型。

除了序列相关和异方差检验之外，截面相依检验也尤为重要。在固定效应模型中，可以利用命令`xttest2`进行检验，该方法是基于似不相关回归(SUR)进行
估计，所以一般要求截面数$N$比时期数$T$小；在随机效应模型中利用`xtcsd`进行检验，当然该命令也适用于固定效应模型。

**截面相依检验**

```
qui xtreg invest mvalue kstock, fe
xttest2
```
![](https://images.gitee.com/uploads/images/2018/1110/071411_a8dc02ba_2269686.png "xttest2.png")


```
 qui xtreg invest mvalue kstock, re
 xtcsd, pesaran
```
![](https://images.gitee.com/uploads/images/2018/1110/071743_d528e39c_2269686.png "xtcsd.png")


当误差项存在序列相关，异方差或截面相依时，依据形式不同，可以利用不同的方法和命令进行估计，详细可以参考 Hoechle (2007)。

![](https://images.gitee.com/uploads/images/2018/1110/072339_c6432107_2269686.png "Hoechle2007.png")

#### **几点说明**

1.  `vce(robust)` 和` vce(cluster)`: 前者适用于异方差且观测值之间独立情况(heteroscedasticity-consistent standard errors)；后者
适用于异方差且允许观测值组内相关。例如`cluster(group)` 的含义是：假设干扰项在 group 之间不相关，而在 group 内部存在相关性。
若 group 代表行业类别，则表示行业间的公司所面临的随机干扰不相关，而行业内部不同公司间的干扰项存在相关性，或者是说，行业内的公司受到了一些共同的干扰因素。

2.  固定效应模型与随机效应模型选择，学者们存在不同的观点。一些学者检验利用严格的统计检验选择，有些学者认为应该根据实际分析的需要进行选择，比如主要变量为不随时变的，那则必须采用随机效应模型。

3. 面板固定效应模型的估计除了可利用`xtreg,fe`进行估计外，也可以利用`areg`或者`reg` + **dummy variables**进行估计，注意这些方法的差异。
4. 上文中涉及到的一些命令，如`xttest0`, `xttest1`, `xttest2`, `xttest3`, `xtserial`, `xtcsd`, `rhausman`等需要下载安装。

#### **参考文献**

1. 钟经樊和连玉君.计量分析与 STATA 应用,2010.
2. Hoechle D. Robust standard errors for panel regressions with cross–sectional dependence[J]. Stata Journal, 2007, 7(3):281-312.
3. Breusch T S, Pagan A R. The Lagrange Multiplier Test and its Applications to Model Specification in Econometrics[J]. Review of     
  Economic Studies, 1980, 47(1):239-253.
4. Mundlak, Y. On the pooling of time series and cross section data. Econometrica, 1978, 46:69-85.
5. Greene, W. 2000. Econometric Analysis.  Upper Saddle River, NJ: Prentice--Hall. 
6. How can the standard errors with the vce(cluster clustvar) option be smaller than those without the vce(cluster clustvar) option? 
   https://www.stata.com/support/faqs/statistics/standard-errors-and-vce-cluster-option/
7. https://blog.stata.com/2015/10/29/fixed-effects-or-random-effects-the-mundlak-approach/
8. Kleiber C, Zeileis A (2008). Applied Econometrics with R. Springer-Verlag, New York. ISBN978-0-387-77316-2, URL 
   https://cran.r-project.org/package=AER.