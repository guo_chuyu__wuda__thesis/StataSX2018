 


*=====================
*-文本分析和数据处理
*=====================

   
  global path "`c(sysdir_personal)'Net_course_C\C_text"
  cd $path
  cap mkdir data
  cap mkdir refs
  cap mkdir out
  cap mkdir adofiles  
  global D    "$path\data"      //范例数据
  global R    "$path\refs"      //参考文献
  global out  "$path\out"       //图形和表格结果
  adopath +   "$path\adofiles"  //添加外部命令所在路径  
  cd $D
  
  set scheme s2color
  
  
  
  help temdir //indicate the directory Stata is using for a temporary directory
  
  help approval //Importing presidential approval poll results, SJ12-3 
  approval, president(32/44) 
  
  
  adoedit ssccount  // 从ssc下载stata用户自编命令的点击次数数据
  //主要是看这个命令如何下载网络数据，以及输出错误信息的。
  
  
  
*-学一些 DOS 命令
  view browse "https://www.sohu.com/a/214956564_697896" //Stata中五条特别好用的DOS命令  
  
  
*-文件路径的操作和访问

  ref to   C_text.do  

  help rcd  //遍历并列示当前工作路径下的所有文件夹
  
  help dirlist //将当前路径下指定类型的文件名写入一个全局暂元,不支持中文
  *-Stata 范例：
    cd "`c(sysdir_personal)'"
	dirlist *.do
	ret list 
  
  help c  // fastcd
  
  help grep //搜索当前数据文件中的关键词
  
  help lookfor
  
  help clickout //produces a list of clickable files for your clicking pleasure, 很好用
  
*-文件搜索和查找

  help doparser //look for dta-files used in do-files 
  
  help filesearch
  
  
*-字符串的分解和切割

  help tokenize
  
  help vtokenize //to split a variable into its tokens
  help vgettoken //Pull the First Token from Each Observation of a Variable
  help getoken

  
  help linewrap  //Split a long string into shorter strings.

*-数据处理

  *-清理或替换变量\变量标签中的特殊字符
    help cleanchars // To replace specific characters or strings in variable names and/or variable labels and/or string variable value

	
*-文件处理\文本替换
  
    help convert_top_lines  //to handle leading lines of text file, 适于GTA数据
  
  * 批量替换 txt 文档中的字符或符号(换行符, Tab符号等)
   help filefilter // Convert text or binary patterns in a file
   help freplace  // 批量替换特定类型的文件中的特定字符(相对于对filefilter进行循环)
   
   help moss // to find multiple occurrences of substrings

   
   help subinfile // 文本替换, 清除空行, 保留特定行，支持正则表达式

   
   help appendfile //纵向合并多个 txt 文档，可以把多个保存变量标签的 txt 文档合并起来
   
   help changeeol //Convert end-of-line characters of text file
   
  *-
    
	help txttool   //Utilities for text analysis in Stata, SJ14-4
	
	help screening //SJ10-1
  
  
  *-文本表格转化为变量
  
  help tex2col  //Split Text into Columns,适于将 PDF 贴进来的表格转成变量
 
  *-Example I
    clear
    input str60 (data)
        "Chaco Hamedo 16,6 25,1 21,5 881,5 58 73 66"
        "Chaco Seco 16,2 24,3 21,3 736,1 60 79 71"
        "Valles Centrales 14,7 18,3 16,9 721,4 50 74 62"
        "Valles del Sur 12,2 18,1 16 351,3 40 70 53"
    end
    tex2col, data(data) col(7) dpcomma  // dpcomma: 把逗号转换成句点
    list

  
    clear
    input str60 (data)
        "Argentina 2011 18.7%"
        "Bolivia 2011 0.4%"
        "Brasil 2011 3.6%"
        "Chile 2011 1.7%"
        "Colombia 2011 5.2%"
        "Costa Rica 2010 8.0%"
        "Ecuador 2011 3.1%"
        "El Salvador 2010 0.3%"
        "Honduras 2011 3.0%"
        "Mexico 2010 3.1%"
        "Panam 2011 0.6%"
        "Paraguay 2010 0.9%"
        "Per 2011 24.1%"
        "Uruguay 2011 4.3%"
        "Venezuela 2011 10.3%"
    end
    tex2col, data(data) col(2) ignore(%)
    list

*-文字对比
  help strgroup  //Match strings based on their Levenshtein edit distance.	

  help countmatch
  
  help fndmtch2
  
  help fndmtch
  
  matchit from http://fmwww.bc.edu/RePEc/bocode/m
    'MATCHIT': module to match two datasets based on similar text patterns /
    matchit is a tool to join observations from two datasets based / on string
    variables which do not necessarily need to be exactly / the same. It
    performs many different string-based matching / techniques, allowing for a

moss from http://fmwww.bc.edu/RePEc/bocode/m
    'MOSS': module to find multiple occurrences of substrings / moss finds
    occurrences of substrings matching a pattern in a / given string variable.
    Depending on what is sought and what is / found, variables are created
    giving the count of occurrences / (always); the positions of occurrences

	
*-导出导入部分文件 txt
   help chewfile
	
   help dataex //Generate a properly formatted data example for Statalist	

   

*-网页爬虫

  help copy  //下载网页数据或表单

  *-从网页下载历史数据
    help fetchyahooquotes  // 雅虎财经
	
  *-下载网上的压缩包文件，解压后整理成stata数据文件
    adoedit fetchyahooquotes  // 查看 unzipfile 那一行
    help unzipfile 
   
SJ-13-3 dm0070. . . . . . . . . . . . . . .  Financial portfolio selection
      . . . . . . . . . . . . . . . . . . . . . . . . . . . .  M. F. Dicle
        (help fetchcomponents, fetchportfolio, fetchyahoooptions
        if installed)
        Q3/13   SJ 13(3):603--617
        fits a multifactor capital asset pricing model for components
        of the Dow Jones Composite Index using data from Yahoo! Finance
	
	
	
*-多个文本的对比

  help tex_equal   // compare the contents of file1 with file2, 
                   // and is extremely useful when dealing with many files to compare

				   
*=======================
*-文件的读写			
*=======================

*- Word 文档(rft格式) 
  help rtfutil  //Utilities for writing Rich Text Format (RTF) files				   
		
		
  help tfinsert //insert text into an open text file 

  help intext // 将文本读入为字符变量，不损失空格 Read text files into string variables in the memory (without losing blanks)			   
		
  help post		
-----------------------------------------------------------------------------------------------------
package dm0082 from http://www.stata-journal.com/software/sj15-3
-----------------------------------------------------------------------------------------------------

TITLE
      SJ15-3 dm0082. Record linkage using Stata:...

DESCRIPTION/AUTHOR(S)
      Record linkage using Stata: Preprocessing,
        linking, and reviewing utilities
      by Nada Wasi, Survey Research Center, Institute
           for Social Research, University of
           Michigan
         Aaron Flaaen, Division of Research and
           Statistics, Federal Reserve Board of
           Governors
      Support:  nwasi@umich.edu,
                aaron.b.flaaen@frb.gov
      After installation, type help stnd_compname,
        stnd_address, reclink2, and clrevmatch

		help stnd_compname
		
INSTALLATION FILES                                  (click here to install)
      dm0082/stnd_compname.ado
      dm0082/stnd_compname.sthlp
      dm0082/stnd_address.ado
      dm0082/stnd_address.sthlp
      dm0082/reclink2.ado
      dm0082/reclink2.sthlp
      dm0082/clrevmatch.ado
      dm0082/clrevmatch.sthlp
      dm0082/agg_acronym.ado
      dm0082/agg_acronym.sthlp
      dm0082/parsing_add_secondary.ado
      dm0082/parsing_add_secondary.sthlp
      dm0082/parsing_entitytype.ado
      dm0082/parsing_entitytype.sthlp
      dm0082/parsing_namefield.ado
      dm0082/parsing_namefield.sthlp
      dm0082/parsing_pobox.ado
      dm0082/parsing_pobox.sthlp
      dm0082/stnd_commonwrd_all.ado
      dm0082/stnd_commonwrd_all.sthlp
      dm0082/stnd_commonwrd_name.ado
      dm0082/stnd_commonwrd_name.sthlp
      dm0082/stnd_entitytype.ado
      dm0082/stnd_entitytype.sthlp
      dm0082/stnd_nesw.ado
      dm0082/stnd_nesw.sthlp
      dm0082/stnd_numbers.ado
      dm0082/stnd_numbers.sthlp
      dm0082/stnd_secondaryadd.ado
      dm0082/stnd_secondaryadd.sthlp
      dm0082/stnd_smallwords.ado
      dm0082/stnd_smallwords.sthlp
      dm0082/stnd_specialchar.ado
      dm0082/stnd_specialchar.sthlp
      dm0082/stnd_streettype.ado
      dm0082/stnd_streettype.sthlp

ANCILLARY FILES                                     (click here to get)
      dm0082/P10_namecomp_patterns.csv
      dm0082/P21_spchar_namespecialcases.csv
      dm0082/P22_spchar_remove.csv
      dm0082/P23_spchar_rplcwithspace.csv
      dm0082/P30_std_entity.csv
      dm0082/P40_std_commonwrd_name.csv
      dm0082/P50_std_commonwrd_all.csv
      dm0082/P60_std_numbers.csv
      dm0082/P70_std_nesw.csv
      dm0082/P81_std_smallwords_all.csv
      dm0082/P82_std_smallwords_address.csv
      dm0082/P90_entity_patterns.csv
      dm0082/P110_std_streettypes.csv
      dm0082/P120_pobox_patterns.csv
      dm0082/P131_std_secondaryadd.csv
      dm0082/P132_secondaryadd_patterns.csv
      dm0082/respondent_employers.dta
      dm0082/firm_dataset.dta
      dm0082/example_sj_stnd.do
      dm0082/example_sj_reclink2.do
      dm0082/example_sj_clrevmatch.do
-----------------------------------------------------------------------------------------------------
(click here to return to the previous screen)
				  