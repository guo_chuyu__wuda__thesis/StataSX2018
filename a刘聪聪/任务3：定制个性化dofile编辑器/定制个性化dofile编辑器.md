> version 1：`2017/11/8 22:27`
> version 2: `2019/4/30 23:08`

## 推文目的   
- 介绍如何设定 Stata dofile 编辑器的`配色方案`，以便呈现语法高亮，光标高亮等功能，提高 dofile 编辑器的舒适性。
- 一些经典的配色方案可以参考 [highlightjs经典配色方案](https://highlightjs.org/static/demo/)。其中，`rainbow`, `VS 2015`，`Xcode`，`Tomorrow系列`，`GoogleCode` 等风格都不错。

## 参考资料

目前似乎没有找到专门介绍这个话题的推文。但我找到了一些介绍其他软件或程序编辑器的配色方案。参见如下文章：
- [让代码看起来更舒服](https://www.cnblogs.com/xiaoshatian/archive/2009/11/20/1606440.html)
- [为革命保护视力 --- 给 Visual Studio 换颜色 - stg609 - 博客园](https://www.cnblogs.com/stg609/p/3723968.html)，这一篇写的很细致，也提供了颜色代码方案。采用颜色代码可以准确控制配色方案。
- [这个配色方案也不错](https://bbs.csdn.net/topics/391940967)
- [一些 Matlab 中的配色方案 - 黑色背景](https://www.cnblogs.com/mat-wu/p/7419746.html)
- [灰色系列配色方案](https://www.bbsmax.com/A/kPzOO7kezx/)


&emsp;


## Stata 中配色方案(亦称为“个人设置 (Preference)”) 的设定方式


---
**特别说明：**`2017/11/8 23:01` 这一小节我写好了框架，里面有些具体条目的介绍还没有写，你可以补充进去。     

### 通用设置

1. 在 Stata 的任何一个窗口中都可以通过 **右击鼠标** 来设定`字体属性`；
1. 在 Stata 结果窗口中，**右击鼠标** &rarr; **Preference** 可以进一步设定`结果窗口(Results)`、`文件浏览器(Viewer)`、`数据编辑器(Data Editor)`等窗口的属性，主要涉及**字体是否加粗**、**颜色**、**窗口背景色**等。
1. 上述设定，以及如下设定，都可以保存到一个模板中。Stata 支持多种模板，可以让你在早、中、晚使用不同的模板，既能调节心情，又能保护视力。
**很重要：** 模板的保存方法为如下。在设置了某些特征后，可以在 Stata 主界面依次点击 **Edit** &rarr;  **Preference** &rarr; **save preference set**，自行定义模板名称，然后 **确认** 即可。若想调用某个模板，方法同上，只是上述最后一步中的 **save preference set** 改为 **load preference set** 即可。
1. 打开 dofile 编辑器 (快捷键为：Ctrl+9)，依次点击 **Edit** &rarr; **Preferences**，会弹出如下 `Do-file Editor Preferences`&rarr; `通用属性 (General)` 设定的对话框：  
  ![输入图片说明](https://gitee.com/uploads/images/2017/1108/225137_d13341fc_1522177.png "屏幕截图.png")   
   - **Display** 下拉菜单中的条目含义如下：  
     - xxx 语法高亮；
     - xxx cccccc；
     - （依次介绍即可）
1. Do-file 编辑器配色方案主要是通过调整 **`Colors`** 对话框中的参数来实现的：   
![输入图片说明](https://gitee.com/uploads/images/2017/1108/225601_8150b7d8_1522177.png "屏幕截图.png")
- xxxxx依次介绍每个条目的含义——可以按照这种方式，截一个图，对应出每个元素的英文名称，用户可以自行定义颜色搭配方案。**注意：** 我这里只是随便举了个例子，正式写推文时，可以自建一个 dofile，举个小例子，里面包含的棉铃要包含图中右半部分涉及的各种元素。微信和 QQ 的截图效果一般，看看有没有更好的截图+标注软件，可以做到更好的复杂标注效果。
![输入图片说明](https://gitee.com/uploads/images/2017/1118/151522_d5656355_1522177.png "屏幕截图.png")


### 选择合适的字体

在 Stata dofile 编辑器中，目前体验最好的是 **Courier New** 字体，主要是因为这种字体等宽呈现。

## 主要任务：
  1. 总体上介绍设定思路和基本方法；先通过图片和 GIF 展示几种我们用起来比较舒服的配色方案；
  1. 介绍 3-5 种比较好用的配色方案，提供每个元素的颜色代码；  
    (1) 介绍时，重点介绍一种配色方案的具体设定方法，其他几种则只提供配色方案(即颜色设定方式);  
    (2) 设定好配色方案后，可以按如下方式保存之：在 Stata 主界面中，依次点击 **Edit** &rarr; **Preference** &rarr; **Save Preference Set**，填入容易记忆和区分的名字即可。在不同的配色方案之间切换时，可以在 Stata 主界面中，依次点击  **Edit** &rarr; **Preference** &rarr; **Load Preference Set**，继而选择你中意的配色方案名称。


## 主要困难：   
  无法通过命令方式设定 Stata dofile 编辑器的模式 (已经通过 Stata 公司工程师确认)，只能通过手动方式设定，颜色部分只能查询颜色代码来设定 (如 RGB)，以免出现色差。因此，在推文中，需要提供几种模板对应的颜色设定代码。我们可以采用这种方式分享我们的配色方案：[给出颜色代码的配色方案](https://www.jb51.net/Dreamweaver/133752.html)

## 附录：一些配色方案

### 黑色背景配色方案

![黑色背景-Son of Obsidian](https://gitee.com/uploads/images/2019/0501/001337_cf6561d9_1522177.png)

![image.png](https://gitee.com/uploads/images/2019/0501/001337_448b5250_1522177.png)

