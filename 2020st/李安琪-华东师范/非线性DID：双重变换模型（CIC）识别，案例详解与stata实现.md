

&emsp;

> **作者**： (大学)    
> **E-Mail:**     

  


## 1.背景介绍
## 2.CIC模型的识别与推断

> 从《Identification and Inference in Nonlinear Difference-in-Differences Models》一文解释非线性DID由来

## 3.CIC模型案例详解
### 3.1 CIC命令安装
### 3.2 论文实例演示
## 4.选择CIC模型使用的建议
## 5.延伸：分位数DID简介


## 参考文献
- 邓国营, 徐舒, 赵绍阳. 环境治理的经济价值:基于CIC方法的测度[J]. 世界经济, 2012,(009):143-160.
- Susan Athey, & Guido W. Imbens. (2005). Identification and inference in nonlinear difference-in-difference models. David K. Levine.
- Borah B J , Burns M E , Shah N D . Assessing the impact of high deductible health plans on health-care utilization and cost: a changes-in-changes approach[J]. Health Economics, 2011, 20(9):1025-1042.
- Melly, B., & Santangelo, G. (2015). The changes-in-changes model with covariates. Universität Bern, Bern.
- 胡日东, 林明裕. 双重差分方法的研究动态及其在公共政策评估中的应用[J]. 财经智库, 2018 (3):84-111.
- Garlick, R. (2012). Academic Peer Effects with Different Group Assignment Policies. U. of Michigan mimeo.
- Callaway, B., Li, T., & Oka, T. (2018). Quantile treatment effects in difference in differences models under dependence restrictions and with only two time periods. Journal of Econometrics, 206(2), 395-413.