

> 模板供参考。建议用 VScode 写推文，成文后贴入此处。不要直接在码云上写，容易丢失。



&emsp;

> **作者：** xxx (xxx大学)      
> **E-mail:**     

> **Source:** [原文作者, 标题](原文链接)

&emsp;
  

## 1. 简介

我用 Stata 已经 15 年了 (数字和英文字符两侧要保留一个空格)。

数学公式如下：

$$y_{it} = x_{it}\beta + \varepsilon_{it}$$

其中，$y_{it}$ 未被解释变量。

此模型可以用 Stata 中的 `regress` 或 `xtreg` 命令进行估计，例如：

```stata
. sysuse "auto.dta", clear
. reg price wei len 
```


## 2. xxxx

### 2.1 xxx

### 2.2 xxx

#### aaaa

#### bbbb

## 3. xxx

后续章节可以自行添加

## 4. 参考资料

**Note：** 关键文献请通过 Google 学术 或 百度学术搜索全文，并按如下格式附上链接。
如果无法找到原文，可以采用 `[[PDF]](https://sci-hub.tw/doi)` 格式提供原文链接 (如何一篇有 DOI 的论文都可以通过 DOI 前面添加 `https://sci-hub.tw/` 网址获得其原文链接)。

- Dumitrescu, E.-I., and C. Hurlin. 2012. Testing for Granger non-causality in heteroge­ neous panels. _Economic Modelling_ 29: 1450-1460. [[PDF]](http://www.univ-orleans.fr/deg/masters/ESA/CH/Causality_20111.pdf)
- Hadri, K. 2000. Testing for stationarity in heterogeneous panel data. _Econometrics Journal_ 3: 148-161. [[PDF]](https://sci-hub.tw/10.1111/1368-423X.00043)  
- Dumitrescu, E.-I., and C. Hurlin. 2012. Testing for Granger non-causality in heteroge­ neous panels. _Economic Modelling_ 29: 1450-1460. [[PDF]](http://www.univ-orleans.fr/deg/masters/ESA/CH/Causality_20111.pdf)