> `2018/11/17 23:56`

- **任务：** 翻译 [Stata Blogs >> Understanding the generalized method of moments (GMM): A simple example](https://blog.stata.com/2015/12/03/understanding-the-generalized-method-of-moments-gmm-a-simple-example/)

- **期望完成时间：** `2019/4/27`







>写在前面:本文是与 StataCorp 高级计量经济学家恩里克•平松 (Enrique Pinzon) 共同撰写的。

**Source：**[Understanding the generalized method of moments (GMM): A simple example](https://blog.stata.com/2015/12/03/understanding-the-generalized-method-of-moments-gmm-a-simple-example/)

## GMM in Stata

- **1.GMM 模型介绍**

广义矩估计 (GMM) 是一种构造估计量的方法，类似于极大似然法 (ML) 。GMM 使用随机变量特定矩的假设，而不是对整个分布的假设，这些假设被称为矩条件。这使得 GMM 比 ML 更稳健，但同时也会丢失一定的效率。

GMM 是矩估计（MM）的推广。在恰好识别情况下，目标函数的最小值等于 0 ，GMM 估计量与 MM 估计量等价；然而在过度识别情况下，MM 不再适用，GMM 可以有效地组合矩条件，使 GMM 比 MM 更有效。

GMM 建立在期望值和样本平均值的基础上。矩条件是用真实矩指定模型参数的期望值。

自由度为 d 的 χ2 随机变量的均值为 d，其方差为 2d，因此两个矩条件公式如下：
$$ E[Y-d]=0 $$   
$$ E[(Y-d)^2-2d]=0 $$   

样本矩条件为：
$$
1 / N \sum_{i=1}^{N}\left(y_{i}-\hat{d}\right)=0
$$   (1)
$$
1 / N \sum_{i=1}^{N}\left[\left(y_{i}-\hat{d}\right)^{2}-2 \hat{d}\right]=0
$$  （2）

我们可以使用样本矩条件 (1) 或样本矩条件 (2) 来估计 d。当我们同时使用 (1) 和 (2) 时，有两个样本矩条件，只有一个参数，所以我们不能解这个方程组。GMM寻找最接近于求解加权样本矩条件的参数。

Uniform weights 和 optimal weights 是加权样本矩条件的两种方法。Uniform weights 使用单位矩阵对矩条件进行加权。optimal weights 使用矩条件协方差的逆矩阵来加权。


- **2.Stata 实现**

我们首先生成一个样本量为 500 的样本.

```stata
drop _all
set obs 500
set seed 12345
generate double y=rchi2(1)
mean y
```

```stata

Mean estimation                   Number of obs   =        500

--------------------------------------------------------------
             |       Mean   Std. Err.     [95% Conf. Interval]
-------------+------------------------------------------------
           y |   .9107644   .0548647      .8029702    1.018559
--------------------------------------------------------------
```
使用 GMM 利用样本矩条件(1)估计参数。参数 d 包含在大括号{}中。我们指定 onestep 选项是因为参数的数量与矩条件的数量相同，也就是说准确地标识了估计值，此时各样本矩条件均可精确求解。
```stata
gmm (y-{d}),instruments( ) onestep  //方程1
```

```stata
Step 1
Iteration 0:   GMM criterion Q(b) =  .82949186  
Iteration 1:   GMM criterion Q(b) =  3.608e-33  
Iteration 2:   GMM criterion Q(b) =  3.608e-33  (backed up)

note: model is exactly identified

GMM estimation 

Number of parameters =   1
Number of moments    =   1
Initial weight matrix: Unadjusted                 Number of obs   =        500

------------------------------------------------------------------------------
             |               Robust
             |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
          /d |   .9107644   .0548098    16.62   0.000     .8033392     1.01819
------------------------------------------------------------------------------
Instruments for equation 1: _cons`
```

然后，使用 GMM 通过样本矩条件(2)估计参数。

```stata
gmm ((y-{d})^2-2*{d}),instruments( ) onestep  //方程2
```

```stata
Step 1
Iteration 0:   GMM criterion Q(b) =  5.4361161  
Iteration 1:   GMM criterion Q(b) =  .02909692  
Iteration 2:   GMM criterion Q(b) =  .00004009  
Iteration 3:   GMM criterion Q(b) =  5.714e-11  
Iteration 4:   GMM criterion Q(b) =  1.172e-22  

note: model is exactly identified

GMM estimation 

Number of parameters =   1
Number of moments    =   1
Initial weight matrix: Unadjusted                 Number of obs   =        500

------------------------------------------------------------------------------
             |               Robust
             |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
          /d |   .7620814   .1156756     6.59   0.000     .5353613    .9888015
------------------------------------------------------------------------------
Instruments for equation 1: _cons
```

比较得到的两个估计值结果，可以发现方程（1）得到的结果比方程（2）更接近真实值，方程（2）的标准误差更大，表明方程 (1) 提供了一个更有效的估计量。

现在，我们使用 GMM 通过 uniform weights 来估计参数。

```stata
matrix I=I(2)
gmm (y-{d})  ((y-{d})^2-2*{d}),instruments( ) winitial(I) onestep 
```
(y-{d}) 表示第一个样本矩条件, ((y-{d})^2-2*{d}) 表示第二个样本矩条件。选项 winitial(I) 和 onestep 表示基于初始权重矩阵计算估计量。
```stata

Step 1
Iteration 0:   GMM criterion Q(b) =   6.265608  
Iteration 1:   GMM criterion Q(b) =  .05343812  
Iteration 2:   GMM criterion Q(b) =  .01852592  
Iteration 3:   GMM criterion Q(b) =   .0185221  
Iteration 4:   GMM criterion Q(b) =   .0185221  

GMM estimation 

Number of parameters =   1
Number of moments    =   2
Initial weight matrix: user                       Number of obs   =        500

------------------------------------------------------------------------------
             |               Robust
             |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
          /d |   .7864099   .1050692     7.48   0.000     .5804781    .9923418
------------------------------------------------------------------------------
Instruments for equation 1: _cons
Instruments for equation 2: _cons
```

最后，我们使用 GMM 通过 two-step optimal weights 估计参数。在 first-step 得到的一致估计量的基础下重新赋予新的权重。

```stata
gmm (y-{d})  ((y-{d})^2-2*{d}),instruments( ) winitial(I) 
```
```stata
Step 1
Iteration 0:   GMM criterion Q(b) =   6.265608  
Iteration 1:   GMM criterion Q(b) =  .05343812  
Iteration 2:   GMM criterion Q(b) =  .01852592  
Iteration 3:   GMM criterion Q(b) =   .0185221  
Iteration 4:   GMM criterion Q(b) =   .0185221  

Step 2
Iteration 0:   GMM criterion Q(b) =  .02888076  
Iteration 1:   GMM criterion Q(b) =  .00547223  
Iteration 2:   GMM criterion Q(b) =  .00546176  
Iteration 3:   GMM criterion Q(b) =  .00546175  

GMM estimation 

Number of parameters =   1
Number of moments    =   2
Initial weight matrix: user                       Number of obs   =        500
GMM weight matrix:     Robust

------------------------------------------------------------------------------
             |               Robust
             |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
          /d |   .9566219   .0493218    19.40   0.000     .8599529    1.053291
------------------------------------------------------------------------------
Instruments for equation 1: _cons
Instruments for equation 2: _cons
```

可以发现，上述得到的四个估计量都是一致的。


- **3.基于蒙特卡罗模拟的效率比较**

下面我们运行一个蒙特卡罗模拟来比较它们的相对效率。

```stata
clear all
 set seed 12345
 matrix I = I(2)
 postfile sim  d_a d_v d_ml d_gmm d_gmme using efcomp, replace
forvalues i = 1/2000 {
      quietly drop _all
      quietly set obs 500
       quietly generate double y = rchi2(1)
   
     quietly mean y 
     local d_a         =  _b[y]
  
    quietly gmm ( (y-{d=`d_a'})^2 - 2*{d}) , instruments( )  ///
      winitial(unadjusted) onestep conv_maxiter(200) 
     if e(converged)==1 {
            local d_v = _b[d:_cons]
     }
    else {
              local d_v = .
     }

    quietly mlexp (ln(chi2den({d=`d_a'},y)))
     if e(converged)==1 {
             local d_ml  =  _b[d:_cons]
      }
     else {
            local d_ml  = .
      }
  
     quietly gmm ( y - {d=`d_a'}) ( (y-{d})^2 - 2*{d}) , instruments( )  ///
        winitial(I) onestep conv_maxiter(200) 
    if e(converged)==1 {
            local d_gmm = _b[d:_cons]
    }
     else {
             local d_gmm = .
     }
 
     quietly gmm ( y - {d=`d_a'}) ( (y-{d})^2 - 2*{d}) , instruments( )  ///
      winitial(unadjusted, independent) conv_maxiter(200) 
     if e(converged)==1 {
            local d_gmme = _b[d:_cons]
     }
    else {
             local d_gmme = .
    }
 
    post sim (`d_a') (`d_v') (`d_ml') (`d_gmm') (`d_gmme') 
 
}
 postclose sim
 use efcomp, clear 
summarize
```

```stata

    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
         d_a |      2,000     1.00017    .0625367   .7792076    1.22256
         d_v |      1,996    1.003621    .1732559   .5623049   2.281469
        d_ml |      2,000    1.002876    .0395273   .8701175   1.120148
       d_gmm |      2,000    .9984172    .1415176   .5947328   1.589704
      d_gmme |      2,000    1.006765    .0540633   .8224731   1.188156

```
结果表明，最大似然估计量标准误差最小，是最有效的（d_ml，std.dev.0.0395），其次是 optimal GMM 估计量（d_gmme，std.dev.0.0541），其次是样本平均值（d_a，std.dev.0.0625），最后是 uniformly-weighted 的 GMM 估计量（d_gmm，std.dev.0.1415），最后是样本方差矩条件（d_v，std.dev.0.1732）。

基于样本方差矩条件的估计量在 2000 次模拟中有 4 次没有收敛；这就是为什么当其他估计量有 2000 次观测时，D_v 上只有 1996 次观测的原因。即使我们使用样本平均值作为非线性规划求解的起始值，也会出现这些收敛失败。

为了更好地了解这些估计量的分布，我们绘制了它们估计量的密度图。


```stata
use efcomp
local N = _N
kdensity d_a,     n(`N') generate(x_a    den_a)    nograph
kdensity d_v,     n(`N') generate(x_v    den_v)    nograph
kdensity d_ml,    n(`N') generate(x_ml   den_ml)   nograph
kdensity d_gmm,   n(`N') generate(x_gmm  den_gmm)  nograph
kdensity d_gmme,  n(`N') generate(x_gmme den_gmme) nograph
twoway (line den_a x_a,       lpattern(solid))        ///
       (line den_v x_v,       lpattern(dash))         ///
       (line den_ml x_ml,     lpattern(dot))          ///
       (line den_gmm x_gmm,   lpattern(dash_dot))     ///
       (line den_gmme x_gmme, lpattern(shordash))

```
![输入图片说明](https://gitee.com/uploads/images/2019/0423/171123_8381f759_4769669.png "5b970ba96530c719cbe02473af509b9.png")

密度图说明了不同估计量的效率排名。 uniformly weighted GMM 估计比样本平均值效率低，因为它对样本平均值施加的权重与基于样本方差的低效率估计值施加的权重相同。

在有效和无效估计量上放置相等权重的 GMM 估计量比在无效估计量上放置较少权重的 GMM 估计量效率低得多。下面，我们将展示 optimal GMM 估计的权重矩阵。

```stata
 matlist e(W), border(rows)
```
```stata
-------------------------------------
             | 1         | 2         
             |     _cons |     _cons 
-------------+-----------+-----------
1            |           |           
       _cons |  1.621476 |           
-------------+-----------+-----------
2            |           |           
       _cons | -.2610053 |  .0707775 
-------------------------------------
```

在每个过度识别的情况下，GMM 估计量使用两个样本矩条件的加权平均值来估计平均值。第一个样本矩条件是样本平均值，第二个矩条件是样本方差。蒙特卡罗模拟结果表明，样本方差比样本平均值提供的平均值估计量的效率要低得多。



