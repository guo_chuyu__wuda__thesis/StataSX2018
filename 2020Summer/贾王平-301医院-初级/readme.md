
&emsp; 

### 贾王平 的项目主页

Stata 连享会高级成员

&emsp;

---


### 推文写作流程
- **S1：** 在 VScode 或 Typora 中写推文初稿 (建议两个同时用)。
- **S2：** 完成推文后务必贴入 [mdnice.com](https://mdnice.com/) 测试一下文中的数学公式和图片是否能正常显示
- **S3：** 将测试完的推文 Markdown 文档贴入你的码云主页下 (此时格式错乱，数学公式无法正常显示等都可以忽略)
> Note: 写作过程中，有任何问题，都可以在微信群里讨论。

请务必认真阅读【[推文写作手册](https://gitee.com/Stata002/StataSX2018/blob/master/2020TE/readme.md)】以及 PDF 版 (我会在微信群中分享) 中的相关要求。 

&emsp; 

### 资料和工具

1. **连享会图书馆：** [计量教材在线浏览](https://quqi.gblhgk.com/s/880197/wqZT9wv6IKpd4Wh1)  || [Stata Journal 在线浏览](https://quqi.gblhgk.com/s/880197/Uv7IUeASWJnvyjUd) || [SJ单篇PDF](https://www.lianxh.cn/news/12ffe67d8d8fb.html) 

2. **常用工具和链接：** [连玉君的链接](https://www.lianxh.cn/news/9e917d856a654.html) || [五分钟 Markdown](https://gitee.com/arlionn/md) 

3. **连享会成员工具箱：** 
- 下载链接：
  - 链接：https://pan.baidu.com/s/1tfXziwFDeSdyboLdpBLq8g 
  - 提取码：qsvf
- 工具简介
  - **VScode**: Markdown 编辑器，很好用，我们用这个编辑器写推文
  - **mathpix**: 快速图片转公式软件，亦可以整页识别 PDF 文档。[Mathpix介绍](https://blog.csdn.net/qq_34243930/article/details/89158366)
  - **Snipaste**: 截图-贴图软件，配合 PicGo 使用。[官网介绍](https://zh.snipaste.com/)；[Snipaste简介](https://baijiahao.baidu.com/s?id=1654041827952660862&wfr=spider&for=pc)
  - **PicGo**: 图片上传到 lianxh.cn 图床的图片管理软件。[PicGo简介](https://www.jianshu.com/p/9d91355e8418)；[PicGo+Typora](https://blog.csdn.net/bruce_6/article/details/104821531)。图床使用参见我在群里分享的【连享会成员-PicGo图床设定和Markdown写作建议.pdf】。
  - **Typora**: Markdown 编辑器 (将 Word, 网页文档贴入该编辑器，可以自动转换为 Markdown 文档) + Pandoc (可以把 Markdown 转换为 Word，PDF 文档，效果甚佳)。Note: Typora 可以自己去官网下载最新版： 
     - [Typora](https://www.typora.io/) &rarr; [使用说明](https://sspai.com/post/54912)
     - [VS Code](https://code.visualstudio.com/) &rarr; [教程1](https://www.cnblogs.com/clwydjgs/p/10078065.html)；[教程2](https://www.cnblogs.com/qianguyihao/archive/2019/04/18/10732375.html) 



&emsp;


### 1. 必读必读 
- 写作之前请务必先查看 [「Wiki」](https://gitee.com/Stata002/StataSX2018/wikis/Home) 页面中的相关建议和要求
  - 重要参考资料1：[Stata Journal 论文合集](https://gitee.com/Stata002/StataSX2018/wikis/%E5%90%84%E6%9C%9F%20Stata%20Journal%20%E8%AE%BA%E6%96%87.md?sort_id=1447682)
  - 推文格式请参阅 [「Stata连享会推文格式要求」](https://gitee.com/Stata002/StataSX2018/wikis/00_Stata%E8%BF%9E%E4%BA%AB%E4%BC%9A%E6%8E%A8%E6%96%87%E6%A0%BC%E5%BC%8F%E8%A6%81%E6%B1%82.md?sort_id=953475)
- 已经认领的题目请到 [[_News]](https://gitee.com/Stata002/StataSX2018/blob/master/_News.md) 页面点击 【编辑】按钮进行登记；完成推文后，请在认领条目前标记 【OK】字样。

&emsp;
---

### 2. 推文选题
选题有三个来源：
- **来源1：** StataSX2018 >> [【Wiki - 002_备选专题】](https://gitee.com/Stata002/StataSX2018/wikis/Home)
- **来源2（推荐）：** 你也可以自行提出选题(最好是你正在学习或研究的内容，一举两得)，我们做进一步讨论，以便确认是否合适。
- **重要说明：** 自 2019/5/9 起，选题主要源于 StataSX2018 >> [【Wiki - 002_备选专题】](https://gitee.com/Stata002/StataSX2018/wikis/Home)。请将选定的题目拖入【===已认领===】文件夹，推文完成后拖入【~~~已完成~~~】文件夹。


#### 2.1 参考项目
如下几位的推文项目主页都做的非常好，供大家参考：
- 参考项目1：[Stata连享会-游万海](https://gitee.com/Stata002/StataSX2018/tree/master/%E6%B8%B8%E4%B8%87%E6%B5%B7)
- 参考项目2：[Stata连享会-许梦洁](https://gitee.com/Stata002/StataSX2018/tree/master/%E8%AE%B8%E6%A2%A6%E6%B4%81)
- 参考项目3：[Stata连享会-胡雨霄](https://gitee.com/Stata002/StataSX2018/tree/master/2019B/%E8%83%A1%E9%9B%A8%E9%9C%84)

#### 2.2 特别说明    
- 选定主题后，请微信通知我确认一下，自拟提纲开始写作 (如有疑问可以随时联系我讨论)
- 每篇推文是一个项目，每个项目下新建一个以 `.md` 为后缀的 Markdown 文档，用于推文的撰写；如有需要，可以新建子文件夹用于存放数据，程序等附件资料。

---

### 3. 推文写作格式要求

> **特别注意：** 除非万不得已，尽量不要用图片形式展示 Stata 结果。推文中的 Stata 结果尽量采用结果窗口中呈现的文本信息来呈现，也就是采用代码块来呈现估计结果。这样就不会过度依赖图床，图床一旦出问题，就会导致推文中的所有图片都无法正常显示。另外，使用文本块显示结果可以获得更好的跨平台展示效果。

1. **署名**： 请在推文首行填入你的真实姓名 (单位信息)，格式为 `> 张三丰 (xxx大学)，邮箱`
1. **标题和层级：** 推文的各个小节需要顺序编号，一级标题用 **「## 1. 一级标题」** (说明：`##` 后要添加一个空格)，二级标题用 **「### 1.1 二级标题」**，最多用到三级标题，即「#### 1.1.1 三级标题」。如果后续还需细分，可以用 **条目(item)** 或独占一行的粗体文字代替。这主要是因为，多数 Markdown 编辑器的一级标题字号都太大了。
1. **段落：** 段落之间要空一行，这样网页上的显示效果比较好；每个段落不要太长 (最好不好超过 200 字，4 行以内比较好)，否则网页或手机阅读时会比较累；
1. **中英文混排**：英文字符和数字两侧要空一格，否则中英文混排时字符间距过小；例如：「**我用Stata已经15年了(Cox,2019)，但Arlion(2018)说他刚用了14年。**」要修改为：「**我用 Stata 已经 15 年了 (Cox, 2019)，但 Arlion (2018) 说他刚用了 14 年。**」。注意，文中所有圆括号都要在半角模式下输入，`(` 左侧和 `)` 右侧各添加一个空格。
1. **Stata 相关**
  - Stata 要写为 「Stata」(首字母大写，无需加粗)，不要写成 「stata」或「STATA」。
  - 变量名用粗体 (如 \*\*varname\*\* &rarr; **varname**)；
  - Stata 命令用高亮显示 (如 \`regress\` &rarr; `regress`)；
  - 多行 Stata 命令和结果展示使用 **代码块样式**，即使用首尾 **\`\`\`** 包围，首行为 「\`\`\`stata」，尾行为「\`\`\`」。
  - **特别注意：** 除非万不得已，尽量不要用图片形式展示 Stata 结果。推文中的 Stata 结果尽量采用结果窗口中呈现的文本信息来呈现，也就是采用代码块来呈现估计结果。这样就不会过度依赖图床，图床一旦出问题，就会导致推文中的所有图片都无法正常显示。另外，使用文本块显示结果可以获得更好的跨平台展示效果。
  - 把如下语句加入你的 **profile.do** 文档中，以便保证结果显示格式比较规整：
  ```stata
   set cformat  %4.3f  //回归结果中系数的显示格式
   set pformat  %4.3f  //回归结果中 p 值的显示格式      
   set sformat  %4.2f  //回归结果中 se值的显示格式  
  ```
5. **数学公式：** (1) 单行公式：用 **\$\$**math**\$\$** 包围的单行数学公式上下各空一行，以保证公式和正文之间的段落间距合理。(2) 行内公式：可以使用 **\$**math**\$** 包围。为了保证在知乎，简书和码云中都能正常显示公式，请把 $y=x\beta$ 写成 `$y=x\beta$`，而不要写成 `$ y=x\beta $` (内侧多加了两个空格)。&emsp; **惊喜：** 无论是网页还是 PDF 中的数学公式，都可以使用 mathpix (https://mathpix.com/) 软件扫描后识别成 LaTeX 数学公式，非常快捷。参见 [神器-数学公式识别工具 mathpix](https://www.jianshu.com/p/1f0506163694)。本页面顶端的「连享会成员工具包」中提供该软件的下载。
6. **图片**：(1) 务必使用 lianxh.cn 的图床，我随后在微信群里发一份「连享会成员-PicGo图床设定和Markdown写作建议.pdf」文档，里面有图床设置说明。(2) 图片的命令规则：**作者姓名_推文关键词_Figxx.png/jpg**，例如：**连玉君_面板数据一文读懂_Fig01.png**。由于我们所有人的图片都放置在同一个图床，若图片名称过于简单，会导致图传里面有很多重名的文件，引用的时候就会出错。 
1. **版权和引用：** 推文若为英文翻译稿，请务必在文首注明来源(独占一行)。Markdown 格式为：「\**Source：**\[原英文标题](网址)」，显示效果为「**Source：**[原英文标题](网址)」。文中若有从别处复制粘贴而来的内容，要标明出处。
1. **底部文本：** 推文底部的文字介绍无需添加，在修改完后会由总编统一添加。



 