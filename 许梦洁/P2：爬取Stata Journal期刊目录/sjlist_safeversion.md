[![连享会-Python爬虫与文本分析现场班 (2019年5月17-19日，太原)](http://upload-images.jianshu.io/upload_images/7692714-fe9847853cf613fe.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "连享会-Python爬虫与文本分析现场班 (2019年5月17-19日，太原)")](https://gitee.com/arlionn/Course/blob/master/Python_text.md)

[![连享会-空间计量现场班 (2019年6月27-30日，西安)](http://upload-images.jianshu.io/upload_images/7692714-26cfc907a456a153.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "连享会-空间计量现场班 (2019年6月27-30日，西安)")](https://gitee.com/arlionn/Course/blob/master/Spatial.md)


&emsp;

&emsp;

> 作者：许梦洁 (中山大学)      
>     &emsp;
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)

- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)
[![点击此处-查看完整推文列表](https://upload-images.jianshu.io/upload_images/7692714-8b1fb0b5068487af.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "连享会(公众号: StataChina)推文列表")](https://gitee.com/arlionn/Course/blob/master/README.md)


 

> **导言：** 
> &emsp;
> **什么是 Stata Journal ？** Stata Journal ([SJ](https://www.stata.com/support/faqs/resources/stata-journal-faq/)) 是 Stata 公司主办的期刊，聚集了全球最优秀、最勤奋的 Stata 用户，分享计量和统计方法的最新进展。其行文风格简洁明了，辅以 Stata 范例来解读复杂的计量模型背后的原理。多数 Stata Journal 上的论文都提供了新近发展的计量方法的 Stata 实现程序，是追踪和学习前沿方法的绝佳读物。
> &emsp;
> **我们从哪里爬取 SJ 论文？** Stata Journal 由 [SAGE](https://journals.sagepub.com/toc/stja/14/1) 出版。我们从 [SAGE](https://journals.sagepub.com/toc/stja/14/1) 网站爬取了各期 Stata Journal 的目录和 PDF 原文链接，以方便各位实时浏览和下载。
> &emsp;
> **哪些 SJ 论文可以免费浏览？** 2001-2015 年各期论文都可以免费在线浏览；2016 年以后的论文只有购买了 [SAGE](https://journals.sagepub.com/toc/stja/14/1) 数据库版权的学校才可以浏览。由于 SAGE 已经提供了每篇论文的 DOI (如 `https://doi.org/10.1177/1536867X1601600109`)，因此，可以在 [-这里-](https://sci-hub.tw) 贴入 DOI 号，浏览原文。
> &emsp;
> **为何缺少 SJ 1-2,1-3 和 1-4 ？** Stata Journal 的前身是 Stata Technical Bulletin ([STB](https://www.stata.com/support/faqs/resources/stata-technical-bulletin-faq/#what))，始创于 1991 年 5 月，第一期为 STB-1。STB 虽然简洁明了，但以短文 (类似于通讯稿或随笔) 为主。为此，从 2001 年 11 月开始，由 Stata Journal 取而代之，第一期为 SJ 1-1。SJ 为季刊，SJ 3-1 表示 2003 年第一期。
>  &emsp;
> **如何获取 SJ 论文中的程序和数据？** 只需在 Stata 命令窗口中输入 `net sj 14-2` 即可呈现 SJ 2014 年 第 2 期所有论文的的程序和数据，点击链接后即可按指引下载。更多详情参见 [[Stata: 外部命令的搜索、安装与使用]](https://www.jianshu.com/p/9b8ecf8f332e)。

**特别声明：** 这些论文仅限于学术交流，请勿用于商业用途。

&emsp;

[toc]

---

## 2001
### SJ 1-1
- Patrick Royston, 2001, Flexible Parametric Alternatives to the Cox Model, and more, Stata Journal, 1(1): 1–28. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0100100101)
- William Gould, 2001, Statistical Software Certification, Stata Journal, 1(1): 29–50. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0100100102)
- J. Scott Long,  Jeremy Freese, 2001, Predicted Probabilities for Count Models, Stata Journal, 1(1): 51–57. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0100100103)
- A. P. Mander, 2001, Haplotype Analysis in Population-based Association Studies, Stata Journal, 1(1): 58–75. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0100100104)
- Allen McDowell, 2001, From the Help Desk, Stata Journal, 1(1): 76–85. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0100100105)
- Nicholas J. Cox, 2001, Speaking Stata: How to Repeat Yourself without going Mad, Stata Journal, 1(1): 86–97. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0100100106)
- Roger Newson, 2001, Review of Generalized Linear Models and Extensions by Hardin and Hilbe, Stata Journal, 1(1): 98–100. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0100100107)
- Christopher F. Baum, 2001, Residual Diagnostics for Cross-section Time Series Regression Models, Stata Journal, 1(1): 101–104. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0100100108)
- Patrick Royston, 2001, Sort a List of Items, Stata Journal, 1(1): 105–106. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0100100109)
- Philippe Van Kerm,  Stephen P. Jenkins, 2001, Generalized Lorenz Curves and Related Graphs: An Update for Stata 7, Stata Journal, 1(1): 107–112. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0100100110)
 
## 2002
### SJ 2-1
 - Sophia Rabe-Hesketh,  Anders Skrondal,  Andrew Pickles, 2002, Reliable Estimation of Generalized Linear Mixed Models using Adaptive Quadrature, Stata Journal, 2(1): 1–21.[[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200101)
 - Roberto G. Gutierrez, 2002, Parametric Frailty and Shared Frailty Survival Models, Stata Journal, 2(1): 22–44. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200102)
 - Roger Newson, 2002, Parameters behind “Nonparametric” Statistics: Kendall's tau, Somers’ D and Median Differences, Stata Journal, 2(1): 45–64. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200103)
 - A. P. Mander, 2002, Analysis of Quantitative Traits using Regression and Log-linear Modeling when Phase is unknown, Stata Journal, 2(1): 65–70. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200104)
 - Allen McDowell, 2002, From the Help Desk: Transfer Functions, Stata Journal, 2(1): 71–85. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200105)
 - Nicholas J. Cox, 2002, Speaking Stata: How to Move Step By: Step, Stata Journal, 2(1): 86–102. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200106)
 - John Hendrickx, 2002, Review of Regression Models for Categorical Dependent Variables Using Stata by Long and Freese, Stata Journal, 2(1): 103–105. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200107)
 
### SJ 2-2
 - A. H. Feiveson, 2002, Power by Simulation, Stata Journal, 2(2): 107–124. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200201)
 - David M. Drukker, 2002, Bootstrapping a Conditional Moments Test for Normality after Tobit Estimation, Stata Journal, 2(2): 125–139. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200202)
 - Ian R. White,  Sarah Walker,  Abdel Babiker, 2002, strbee: Randomization-based Efficacy Estimator, Stata Journal, 2(2): 140–150. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200203)
 - Patrick Royston,  Abdel Babiker, 2002, A Menu-driven Facility for Complex Sample Size Calculation in Randomized Controlled Trials with a Survival or a Binary Outcome, Stata Journal, 2(2): 151–163. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200204)
 - Jonathan A. C. Sterne,  Kate Tilling, 2002, G-estimation of Causal Effects, Allowing for Time-varying Confounding, Stata Journal, 2(2): 164–182. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200205)
 - Thomas J. Steichen,  Nicholas J. Cox, 2002, A Note on the Concordance Correlation Coefficient, Stata Journal, 2(2): 183–189. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200206)
 - Allen McDowell,  Jeff Pitblado, 2002, From the Help Desk: It's all about the Sampling, Stata Journal, 2(2): 190–201. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200207)
 - Nicholas J. Cox, 2002, Speaking Stata: How to Face Lists with Fortitude, Stata Journal, 2(2): 202–222. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200208)
 - Jeremy Freese, 2002, Review of Statistics with Stata (Updated for Version 7), Stata Journal, 2(2): 223–225. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200209)
 
### SJ 2-3
 - Florian Heiss, 2002, Structural Choice Analysis with Nested Logit Models, Stata Journal, 2(3): 227–252. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200301)
 - James W. Hardin, 2002, The Robust Variance Estimator for Two-stage Models, Stata Journal, 2(3): 253–266. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200302)
 - Michael E. Reichenheim,  Antônio Ponce de Leon, 2002, Estimation of Sensitivity and Specificity Arising from Validity Studies with Incomplete Designs, Stata Journal, 2(3): 267–279. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200303)
 - Mario A. Cleves, 2002, Comparative Assessment of Three Common Algorithms for Estimating the Variance of the Area under the Nonparametric Receiver Operating Characteristic Curve, Stata Journal, 2(3): 280–289. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200304)
 - Ronán M. Conroy, 2002, Choosing an Appropriate Real-Life Measure of Effect Size: The Case of a Continuous Predictor and a Binary Outcome, Stata Journal, 2(3): 290–295. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200305)
 - Jeremy Freese, 2002, Least Likely Observations in Regression Models for Categorical Outcomes, Stata Journal, 2(3): 296–300. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200306)
 - Mario A. Cleves, 2002, From the Help Desk: Comparing Areas under Receiver Operating Characteristic Curves from Two or more Probit or Logit Models, Stata Journal, 2(3): 301–313. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200307)
 - Nicholas J. Cox, 2002, Speaking Stata: On Numbers and Strings, Stata Journal, 2(3): 314–329. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200308)
 
### SJ 2-4
 - David W. Hosmer,  Patrick Royston, 2002, Using Aalen's Linear Hazards Model to Investigate Time-varying Effects in the Proportional Hazards Regression Model, Stata Journal, 2(4): 331–350. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200401)
 - Michael E. Reichenheim, 2002, Two-graph Receiver Operating Characteristic, Stata Journal, 2(4): 351–357. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200402)
 - Sascha O. Becker,  Andrea Ichino, 2002, Estimation of Average Treatment Effects Based on Propensity Scores, Stata Journal, 2(4): 358–377. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200403)
 - Weihua Guan,  Roberto G. Gutierrez, 2002, Programmable GLM: Two User-defined Links, Stata Journal, 2(4): 378–390. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200404)
 - Matthias Schonlau, 2002, The Clustergram: A Graph for Visualizing Hierarchical and Nonhierarchical Cluster Analyses, Stata Journal, 2(4): 391–402. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200405)
 - Brian P. Poi, 2002, From the Help Desk: Demand System Estimation, Stata Journal, 2(4): 403–410. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200406)
 - Nicholas J. Cox, 2002, Speaking Stata: On Getting Functions to do the Work, Stata Journal, 2(4): 411–427. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200407)
 - David W. Hosmer, 2002, Review of an Introduction to Survival Analysis Using Stata, Stata Journal, 2(4): 428–431. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0200200408)
 
 
## 2003
### SJ 3-1
 - Christopher F. Baum,  Mark E. Schaffer,  Steven Stillman, 2003, Instrumental Variables and GMM: Estimation and Testing, Stata Journal, 3(1): 1–31. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300101)
 - Germán Rodríguez,  Irma Elo, 2003, Intra-class Correlation in Random-effects Models for Binary Data, Stata Journal, 3(1): 32–46. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300102)
 - Catherine L. Saunders,  D. Timothy Bishop,  Jennifer H. Barrett, 2003, Sample Size Calculations for Main Effects and Interactions in Case–control Studies using Stata's nchi2 and npnchi2 Functions, Stata Journal, 3(1): 47–56. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300103)
 - Marcelo J. Moreira,  Brian P. Poi, 2003, Implementing Tests with Correct Size in the Simultaneous Equations Model, Stata Journal, 3(1): 57–70. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300104)
 - Weihua Guan, 2003, From the Help Desk: Bootstrapped Standard Errors, Stata Journal, 3(1): 71–80. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300105)
 - Nicholas J. Cox,  Ulrich Kohler, 2003, Speaking Stata: On Structure and Shape: The Case of Multiple Responses, Stata Journal, 3(1): 81–99. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300106)
 - John McGready, 2003, Review of a Short Introduction to Stata for Biostatistics by Hills and De Stavola, Stata Journal, 3(1): 100–104. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300107)
 
### SJ 3-2
 - H. Joseph Newton,  Nicholas J. Cox, 2003, The Stata Journal so Far: Editors’ Report, Stata Journal, 3(2): 105–108. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300201)
 - Roger Newson, The ALSPAC Study Team, 2003, Multiple—test Procedures and Smile Plots, Stata Journal, 3(2): 109–132. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300202)
 - Isaías H. Salgado-Ugarte,  Marco A. Pérez-Hernández, 2003, Exploring the Use of Variable Bandwidth Kernel Density Estimators, Stata Journal, 3(2): 133–147. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300203)
 - Philippe Van Kerm, 2003, Adaptive Kernel Density Estimation, Stata Journal, 3(2): 148–156. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300204)
 - Omar M. G. Keshk, 2003, CDSIMEQ: A Program to Implement Two-stage Probit Least Squares, Stata Journal, 3(2): 157–167. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300205)
 - David M. Drukker, 2003, Testing for Serial Correlation in Linear Panel-data Models, Stata Journal, 3(2): 168–177. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300206)
 - Allen McDowell, 2003, From the Help Desk: Hurdle Models, Stata Journal, 3(2): 178–184. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300207)
 - Nicholas J. Cox, 2003, Speaking Stata: Problems with Lists, Stata Journal, 3(2): 185–202. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300208)
 - Joanne M. Garrett, 2003, Review of Statistical Modeling for Biomedical Researchers by Dupont, Stata Journal, 3(2): 203–207. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300209)
 - Steven Stillman, 2003, Review of Generalized Estimating Equations by Hardin and Hilbe, Stata Journal, 3(2): 208–210. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300210)
 
 
### SJ 3-3
 - Susan M. Hailpern,  Paul F. Visintainer, 2003, Odds Ratios and Logistic Regression: Further Examples of their use and Interpretation, Stata Journal, 3(3): 213–225. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300301)
 - John B. Carlin,  Ning Li,  Philip Greenwood,  Carolyn Coffey, 2003, Tools for Analyzing Multiple Imputed Datasets, Stata Journal, 3(3): 226–244. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300302)
 - Roger Newson, 2003, Confidence Intervals and p-values for Delivery to the End User, Stata Journal, 3(3): 245–269. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300303)
 - Nigel Smeeton,  Nicholas J. Cox, 2003, Do-it-yourself Shuffling and the Number of Runs under Randomness, Stata Journal, 3(3): 270–277. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300304)
 - Lorenzo Cappellari,  Stephen P. Jenkins, 2003, Multivariate Probit Regression using Simulated Maximum Likelihood, Stata Journal, 3(3): 278–294. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300305)
 - Svend Juul, 2003, Lean Mainstream Schemes for Stata 8 Graphics, Stata Journal, 3(3): 295–301. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300306)
 - Brian P. Poi, 2003, From the Help Desk: Swamy's Random-coefficients Model, Stata Journal, 3(3): 302–308. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300307)
 - Nicholas J. Cox, 2003, Speaking Stata: Problems with Tables, Part I, Stata Journal, 3(3): 309–324. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0300300308)
 
 
### SJ 3-4
 - H. Joseph Newton,  Nicholas J. Cox, 2003, A Special Issue of the Stata Journal, Stata Journal, 3(4): 327–327. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400300401)
 - H. Joseph Newton,  Nicholas J. Cox, 2003, Introducing Stata Tips, Stata Journal, 3(4): 328–328. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400300402)
 - James W. Hardin,  Raymond J. Carroll, 2003, Measurement Error, GLMs, and Notational Conventions, Stata Journal, 3(4): 329–341. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400300403)
 - James W. Hardin,  Raymond J. Carroll, 2003, Variance Estimation for the Instrumental Variables Approach to Measurement Error in Generalized Linear Models, Stata Journal, 3(4): 342–350. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400300404)
 - James W. Hardin,  Henrik Schmiediche,  Raymond J. Carroll, 2003, Instrumental Variables, Bootstrapping, and Generalized Linear Models, Stata Journal, 3(4): 351–360. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400300405)
 - James W. Hardin,  Henrik Schmiediche,  Raymond J. Carroll, 2003, The Regression-calibration Method for Fitting Generalized Linear Models with Additive Measurement Error, Stata Journal, 3(4): 361–372. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400300406)
 - James W. Hardin,  Henrik Schmiediche,  Raymond J. Carroll, 2003, The Simulation Extrapolation Method for Fitting Generalized Linear Models with Additive Measurement Error, Stata Journal, 3(4): 373–385. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400300407)
 - Sophia Rabe-Hesketh,  Anders Skrondal,  Andrew Pickles, 2003, Maximum Likelihood Estimation of Generalized Linear Models with Covariate Measurement Error, Stata Journal, 3(4): 386–411. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400300408)
 - Roberto G. Gutierrez,  Jean Marie Linhart,  Jeffrey S. Pitblado, 2003, From the Help Desk: Local Polynomial Regression and Stata Plugins, Stata Journal, 3(4): 412–419. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400300409)
 - Nicholas J. Cox, 2003, Speaking Stata: Problems with Tables, Part II, Stata Journal, 3(4): 420–439. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400300410)
 - Stephen P. Jenkins, 2003, Review of Maximum Likelihood Estimation with Stata by Gould, Pitblado, and Sribney, Stata Journal, 3(4): 440–444. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400300411)
 - Roger Newson, 2003, Stata Tip 1: The Eform() Option of Regress, Stata Journal, 3(4): 445–445. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400300412)
 - Nicholas J. Cox, 2003, Stata Tip 2: Building with Floors and Ceilings, Stata Journal, 3(4): 446–447. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400300413)
 - William Gould, 2003, Stata Tip 3: How to be Assertive, Stata Journal, 3(4): 448–448. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400300414)
 
 
## 2004
### SJ 4-1
 - Justin Fenty, 2004, Analyzing Distances, Stata Journal, 4(1): 1–26. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0100400101)
 - Mark B. Stewart, 2004, Semi-nonparametric Estimation of Extended Ordered Probit Models, Stata Journal, 4(1): 27–39. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0100400102)
 - Alfonso Miranda, 2004, FIML Estimation of an Endogenous Switching Model for Count Data, Stata Journal, 4(1): 40–49. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0100400103)
 - Suzanna Vidmar,  John Carlin,  Kylie Hesketh,  Tim Cole, 2004, Standardizing Anthropometric Measures in Children and Adolescents with New Functions for Egen, Stata Journal, 4(1): 50–55. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0100400104)
 - Jean Marie Linhart,  Jeffrey S. Pitblado,  James Hassell, 2004, From the Help Desk: Kaplan–Meier Plots with Stsatrisk, Stata Journal, 4(1): 56–65. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0100400105)
 - Nicholas J. Cox, 2004, Speaking Stata: Graphing Distributions, Stata Journal, 4(1): 66–88. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0100400106)
 - Laurent Audigé, 2004, Review of Veterinary Epidemiologic Research by Dohoo, Martin, and Stryhn, Stata Journal, 4(1): 89–92. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0100400107)
 - Philip Ryan, 2004, Stata Tip 4: Using Display as an Online Calculator, Stata Journal, 4(1): 93–93. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0100400108)
 - Roger Newson, 2004, Stata Tip 5: Ensuring Programs Preserve Dataset Sort Order, Stata Journal, 4(1): 94–94. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0100400109)
 - Nicholas J. Cox, 2004, Stata Tip 6: Inserting Awkward Characters in the Plot, Stata Journal, 4(1): 95–96. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0100400110)
 
 - ——, 2004, Flexible Parametric Alternatives to the Cox Model: Update, Stata Journal, 4(1): 98–101. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0100400112)
 
### SJ 4-2
 - Vincenzo Coviello,  May Boggess, 2004, Cumulative Incidence Estimation in the Presence of Competing Risks, Stata Journal, 4(2): 103–112. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400201)
 - Amil Petrin,  Brian P. Poi,  James Levinsohn, 2004, Production Function Estimation in Stata using Inputs to Control for Unobservables, Stata Journal, 4(2): 113–123. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400202)
 - Thomas J. Steichen, 2004, Submenu and Dialogs for Meta-analysis Commands, Stata Journal, 4(2): 124–126. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400203)
 - Jonathan A. C. Sterne,  Roger M. Harbord, 2004, Funnel Plots in Meta-analysis, Stata Journal, 4(2): 127–141. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400204)
 - David A. Harrison,  Anthony R. Brady, 2004, Sample Size and Power Calculations using the Noncentral t-distribution, Stata Journal, 4(2): 142–153. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400205)
 - Edward C. Norton,  Hua Wang,  Chunrong Ai, 2004, Computing Interaction Effects and Standard Errors in Logit and Probit Models, Stata Journal, 4(2): 154–167. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400206)
 - Carlo V. Fiorio, 2004, Confidence Intervals for Kernel Density Estimation, Stata Journal, 4(2): 168–179. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400207)
 - Allen McDowell, 2004, From the Help Desk: Polynomial Distributed Lag Models, Stata Journal, 4(2): 180–189. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400208)
 - Nicholas J. Cox, 2004, Speaking Stata: Graphing Categorical and Compositional Data, Stata Journal, 4(2): 190–215. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400209)
 - Richard Williams, 2004, Review of Statistics with Stata (Updated for Version 8) by Hamilton, Stata Journal, 4(2): 216–219. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400210)
 - Shannon Driver,  Patrick Royston, 2004, Stata Tip 7: Copying and Pasting under Windows, Stata Journal, 4(2): 220–220. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400211)
 - Ben Jann, 2004, Stata Tip 8: Splitting Time-span Records with Categorical Time-varying Covariates, Stata Journal, 4(2): 221–222. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400212)
 - Nicholas J. Cox, 2004, Stata Tip 9: Following Special Sequences, Stata Journal, 4(2): 223–223. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400213)
 
 
### SJ 4-3
 - Patrick Royston, 2004, Multiple Imputation of Missing Values, Stata Journal, 4(3): 227–241. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400301)
 - Anders Alexandersson, 2004, Graphing Confidence Ellipses: An Update of Ellip for Stata 8, Stata Journal, 4(3): 242–256. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400302)
 - Lois G. Kim,  Ian R. White, 2004, Compliance-adjusted Intervention Effects in Survival Data, Stata Journal, 4(3): 257–264. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400303)
 - Paulo Guimarães, 2004, Understanding the Multinomial-Poisson Transformation, Stata Journal, 4(3): 265–273. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400304)
 - Peter Cummings,  Barbara McKnight, 2004, Analysis of Matched Cohort Data, Stata Journal, 4(3): 274–281. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400305)
 - Michael Lokshin,  Zurab Sajaia, 2004, Maximum Likelihood Estimation of Endogenous Switching Regression Models, Stata Journal, 4(3): 282–289. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400306)
 - Alberto Abadie,  David Drukker,  Jane Leber Herr,  Guido W. Imbens, 2004, Implementing Matching Estimators for Average Treatment Effects in Stata, Stata Journal, 4(3): 290–311. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400307)
 - Brian P. Poi, 2004, From the Help Desk: Some Bootstrapping Techniques, Stata Journal, 4(3): 312–328. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400308)
 - Nicholas J. Cox, 2004, Speaking Stata: Graphing Agreement and Disagreement, Stata Journal, 4(3): 329–349. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400309)
 - Nicholas Winter, 2004, Review of a Handbook of Statistical Analyses Using Stata by Rabe-Hesketh and Everitt, Stata Journal, 4(3): 350–353. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400310)
 - Philip Ryan,  Nicholas Winter, 2004, Stata Tip 10: Fine Control of Axis Title Positions, Stata Journal, 4(3): 354–355. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400311)
 - Patrick Royston, 2004, Stata Tip 11: The Nolog Option with Maximum-likelihood Modeling Commands, Stata Journal, 4(3): 356–356. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400312)
 - Nicholas J. Cox, 2004, Stata Tip 12: Tuning the Plot Region Aspect Ratio, Stata Journal, 4(3): 357–358. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400313)
 
 
### SJ 4-4
 - Maurizio Pisati, 2004, Simple Thematic Mapping, Stata Journal, 4(4): 361–378. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400401)
 - Roger Newson, 2004, Generalized Power Calculations for Generalized Linear Models and more, Stata Journal, 4(4): 379–401. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400402)
 - Zoe Fewell,  Miguel A. Hernán,  Frederick Wolfe,  Kate Tilling,  Hyon Choi,  Jonathan A. C. Sterne, 2004, Controlling for Time-dependent Confounding using Marginal Structural Models, Stata Journal, 4(4): 402–420. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400403)
 - Michael E. Reichenheim, 2004, Confidence Intervals for the Kappa Statistic, Stata Journal, 4(4): 421–428. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400404)
 - Matteo Bottai,  Nicola Orsini, 2004, Confidence Intervals for the Variance Component of Random-effects Linear Models, Stata Journal, 4(4): 429–435. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400405)
 - Bear F. Braumoeller, 2004, Boolean Logit and Probit in Stata, Stata Journal, 4(4): 436–441. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400406)
 - Allen McDowell, 2004, From the Help Desk: Seemingly Unrelated Regression with Unbalanced Equations, Stata Journal, 4(4): 442–448. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400407)
 - Nicholas J. Cox, 2004, Speaking Stata: Graphing Model Diagnostics, Stata Journal, 4(4): 449–475. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400408)
 - Ulrich Kohler, 2004, Review of a Visual Guide to Stata Graphics by Mitchell, Stata Journal, 4(4): 476–479. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400409)
 - Nicholas J. Cox, 2004, Review of Statistical Evaluation of Measurement Errors by Dunn, Stata Journal, 4(4): 480–483. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400410)
 - Roger Newson, 2004, Stata Tip 13: Generate and Replace use the Current Sort Order, Stata Journal, 4(4): 484–485. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400411)
 - Kenneth Higbee, 2004, Stata Tip 14: Using Value Labels in Expressions, Stata Journal, 4(4): 486–487. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400412)
 - Nicholas J. Cox, 2004, Stata Tip 15: Function Graphs on the Fly, Stata Journal, 4(4): 488–489. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400413)
 - ——, 2004, Cumulative Index for SJ1-1–SJ4-4, Stata Journal, 4(4): 492–498. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400400415)
 
## 2005
### SJ 5-1
 - Nicholas J. Cox, 2005, A Special 20th Stata Anniversary Issue of the Stata Journal, Stata Journal, 5(1): 1–1. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500101)
 - H. Joseph Newton, 2005, A Brief History of Stata on its 20th Anniversary, Stata Journal, 5(1): 2–18. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500102)
 - Sean Becketti, 2005, A Conversation with William Gould, Stata Journal, 5(1): 19–31. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500103)
 - Lawrence Hamilton, 2005, In at the Creation, Stata Journal, 5(1): 32–34. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500104)
 - Tony Lachenbruch, 2005, A Short History of Statistics with Stata, Stata Journal, 5(1): 35–37. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500105)
 - Joseph M. Hilbe, 2005, Memories of Stata, Stata Journal, 5(1): 38–38. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500106)
 - J. Theodore Anagnoson, 2005, The Birth of the Bulletin, Stata Journal, 5(1): 39–40. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500107)
 - Patrick Royston, 2005, The History of StataQuest, Stata Journal, 5(1): 41–42. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500108)
 - Christopher F. Baum, 2005, Stata at 20: A Personal View, Stata Journal, 5(1): 43–45. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500109)
 - Michael N. Mitchell,  Xiao Chen, 2005, Stata: The Language of Choice for Time-Series Analysis?, Stata Journal, 5(1): 46–63. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500110)
 - Ian Watson, 2005, Visualizing Main Effects and Interactions for Binary Logit Models, Stata Journal, 5(1): 64–82. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500111)
 - Ben Jann, 2005, Further Processing of Estimation Results: Basic Programming with Matrices, Stata Journal, 5(1): 83–91. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500112)
 - Friederike M.-S. Barthel,  Patrick Royston,  Abdel Babiker, 2005, Tabulation of Multiple Responses, Stata Journal, 5(1): 92–122. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500113)
 - Roger Newson, 2005, A Menu-driven Facility for Complex Sample Size Calculation in Randomized Controlled Trials with a Survival or a Binary Outcome: Update, Stata Journal, 5(1): 123–129. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500114)
 - Ulrich Kohler, 2005, Review of Generalized Latent Variable Modeling by Skrondal and Rabe-Hesketh, Stata Journal, 5(1): 130–133. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500115)
 - Nicholas J. Cox, 2005, Stata Tip 16: Using Input to Generate Variables, Stata Journal, 5(1): 134–134. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500116)
 - Shannon Driver, 2005, Stata Tip 17: Filling in the Gaps, Stata Journal, 5(1): 135–136. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500117)
 - ——, 2005, Stata Tip 18: Making Keys Functional, Stata Journal, 5(1): 137–138. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500118)
 
 
### SJ 5-2
 - Mario A. Cleves, 2005, Exploratory Analysis of Single Nucleotide Polymorphism (SNP) for Quantitative Traits, Stata Journal, 5(2): 141–153. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500201)
 - Jeroen Weesie, 2005, Value Label Utilities: Labeldup and Labelrename, Stata Journal, 5(2): 154–161. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500202)
 - Jeroen Weesie, 2005, Multilingual Datasets, Stata Journal, 5(2): 162–187. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500203)
 - Patrick Royston, 2005, Multiple Imputation of Missing Values: Update, Stata Journal, 5(2): 188–201. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500204)
 - J. Lloyd Blackwell, III, 2005, Estimation and Testing of Fixed-effect Panel-data Systems, Stata Journal, 5(2): 202–207. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500205)
 - Ulrich Kohler,  Magdalena Luniak, 2005, Data Inspection using Biplots, Stata Journal, 5(2): 208–223. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500206)
 - Daniel Müller, 2005, Stata in Space: Econometric Analysis of Spatially Explicit Raster Data, Stata Journal, 5(2): 224–238. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500207)
 - Emma Slaymaker, 2005, Using the File Command to Produce Formatted Output for other Applications, Stata Journal, 5(2): 239–247. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500208)
 - Susan M. Hailpern, 2005, Teaching Statistics to Physicians using Stata, Stata Journal, 5(2): 248–258. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500209)
 - Nicholas J. Cox, 2005, Speaking Stata: Density Probability Plots, Stata Journal, 5(2): 259–273. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500210)
 - Stanley Lemeshow,  Melvin L. Moeschberger, 2005, Review of Regression Methods in Biostatistics: Linear, Logistic, Survival, and Repeated Measures Models by Vittinghoff, Glidden, Shiboski, and McCulloch, Stata Journal, 5(2): 274–278. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500211)
 - Patrick Royston, 2005, Stata Tip 19: A Way to Leaner, Faster Graphs, Stata Journal, 5(2): 279–279. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500212)
 - David A. Harrison, 2005, Stata Tip 20: Generating Histogram Bin Variables, Stata Journal, 5(2): 280–281. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500213)
 - Nicholas J. Cox, 2005, Stata Tip 21: The Arrows of Outrageous Fortune, Stata Journal, 5(2): 282–284. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500214)
 
 
### SJ 5-3
 - H. Joseph Newton,  Nicholas J. Cox, 2005, Editorial Announcements, Stata Journal, 5(3): 287–287. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500301)
 - Ben Jann, 2005, Making Regression Tables from Stored Estimates, Stata Journal, 5(3): 288–308. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500302)
 - Tamás Bartus, 2005, Estimation of Marginal Effects using Margeff, Stata Journal, 5(3): 309–329. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500303)
 - Matthias Schonlau, 2005, Boosted Regression (Boosting): An Introductory Tutorial and a Stata Plugin, Stata Journal, 5(3): 330–354. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500304)
 - Nicola Orsini,  Debora Rizzuto,  Nicola Nante, 2005, Introduction to Game-theory Calculations, Stata Journal, 5(3): 355–370. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500305)
 - William D. Dupont,  W. Dale Plummer, Jr., 2005, Using Density-distribution Sunflower Plots to Explore Bivariate Relationships in Dense Data, Stata Journal, 5(3): 371–384. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500306)
 - Paulo Guimarães, 2005, A Simple Approach to Fit the Beta-binomial Model, Stata Journal, 5(3): 385–394. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500307)
 - Ronán M. Conroy, 2005, Stings in the Tails: Detecting and Dealing with Censored Data, Stata Journal, 5(3): 395–404. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500308)
 - Patrick Royston,  Nicholas J. Cox, 2005, A Multivariable Scatterplot Smoother, Stata Journal, 5(3): 405–412. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500309)
 - David Kantor,  Nicholas J. Cox, 2005, Depending on Conditions: A Tutorial on the Cond() Function, Stata Journal, 5(3): 413–420. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500310)
 - William Gould, 2005, Mata Matters: Translating Fortran, Stata Journal, 5(3): 421–441. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500311)
 - Nicholas J. Cox, 2005, Speaking Stata: The Protean Quantile Plot, Stata Journal, 5(3): 442–460. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500312)
 - Rino Bellocco, 2005, Review of Statistics for Epidemiology by Jewell, Stata Journal, 5(3): 461–464. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500313)
 - Philip Ryan, 2005, Stata Tip 22: Variable Name Abbreviation, Stata Journal, 5(3): 465–466. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500314)
 - Nicholas J. G. Winter, 2005, Stata Tip 23: Regaining Control over Axis Ranges, Stata Journal, 5(3): 467–468. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500315)
 - Nicholas J. Cox, 2005, Stata Tip 24: Axis Labels on Two or more Levels, Stata Journal, 5(3): 469–469. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500316)
 
 
### SJ 5-4
 - Giovanni S. F. Bruno, 2005, Estimation and Inference in Dynamic Unbalanced Panel-data Models with a Small Number of Individuals, Stata Journal, 5(4): 473–500. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500401)
 - Anirban Basu, 2005, Extended Generalized Linear Models: Simultaneous Estimation of Flexible Link and Variance Functions, Stata Journal, 5(4): 501–516. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500402)
 - James Cui, 2005, Buckley–James Method for Analyzing Censored Data, with an Application to a Cardiovascular Disease and an HIV/AIDS Study, Stata Journal, 5(4): 517–526. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500403)
 - Patrick Royston, 2005, Multiple Imputation of Missing Values: Update of Ice, Stata Journal, 5(4): 527–536. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500404)
 - Jun Xu,  J. Scott Long, 2005, Confidence Intervals for Predicted Outcomes in Regression Models for Categorical Outcomes, Stata Journal, 5(4): 537–559. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500405)
 - Nicholas J. Cox, 2005, Suggestions on Stata Programming Style, Stata Journal, 5(4): 560–566. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500406)
 - William Gould, 2005, Mata Matters: Using Views onto the Data, Stata Journal, 5(4): 567–573. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500407)
 - Nicholas J. Cox, 2005, Speaking Stata: Smoothing in Various Directions, Stata Journal, 5(4): 574–593. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500408)
 - L. Philip Schumm, 2005, Review of Data Analysis Using Stata by Kohler and Kreuter, Stata Journal, 5(4): 594–600. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500409)
 - Ulrich Kohler,  Christian Brzinsky-Fay, 2005, Stata Tip 25: Sequence Index Plots, Stata Journal, 5(4): 601–602. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500410)
 - Michael S. Hanson, 2005, Stata Tip 26: Maximizing Compatibility between Macintosh and Windows, Stata Journal, 5(4): 603–603. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500411)
 - Nicholas J. Cox, 2005, Stata Tip 27: Classifying Data Points on Scatter Plots, Stata Journal, 5(4): 604–606. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500412)
 
 
## 2006
### SJ 6-1
 - Yulia V. Marchenko, 2006, Estimating Variance Components in Stata, Stata Journal, 6(1): 1–21. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600101)
 - Rosa Gini,  Jacopo Pasquini, 2006, Automatic Generation of Documents, Stata Journal, 6(1): 22–39. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600102)
 - Nicola Orsini,  Rino Bellocco,  Sander Greenland, 2006, Generalized Least Squares for Trend Estimation of Summarized Dose–response Data, Stata Journal, 6(1): 40–57. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600103)
 - Richard Williams, 2006, Generalized Ordered Logit/Partial Proportional Odds Models for Ordinal Dependent Variables, Stata Journal, 6(1): 58–82. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600104)
 - Patrick Royston, 2006, Explained Variation for Survival Models, Stata Journal, 6(1): 83–96. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600105)
 - Kellie J. Archer,  Stanley Lemeshow, 2006, Goodness-of-fit Test for a Logistic Regression Model Fitted using Survey Sample Data, Stata Journal, 6(1): 97–105. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600106)
 - Alejandro López-Feldman, 2006, Decomposing Inequality and Obtaining Marginal Effects, Stata Journal, 6(1): 106–111. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600107)
 - William Gould, 2006, Mata Matters: Creating New Variables—sounds Boring, isn't, Stata Journal, 6(1): 112–123. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600108)
 - Nicholas J. Cox, 2006, Speaking Stata: Time of Day, Stata Journal, 6(1): 124–137. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600109)
 - Rory Wolfe, 2006, Review of Multilevel and Longitudinal Modeling Using Stata by Rabe-Hesketh and Skrondal, Stata Journal, 6(1): 138–143. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600110)
 - L. Philip Schumm, 2006, Stata Tip 28: Precise Control of Dataset Sort Order, Stata Journal, 6(1): 144–146. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600111)
 - Charles H. Franklin, 2006, Stata Tip 29: For All Times and All Places, Stata Journal, 6(1): 147–148. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600112)
 - Nicholas J. Cox, 2006, Stata Tip 30: May the Source be with you, Stata Journal, 6(1): 149–150. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600113)
 
 
### SJ 6-2
 - David M. Drukker, 2006, Maximum Simulated Likelihood: Introduction to a Special Issue, Stata Journal, 6(2): 153–155. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600201)
 - Lorenzo Cappellari,  Stephen P. Jenkins, 2006, Calculation of Multivariate Normal Probabilities by Simulation, with Applications to Maximum Simulated Likelihood Estimation, Stata Journal, 6(2): 156–189. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600202)
 - Richard Gates, 2006, A Mata Geweke–Hajivassiliou–Keane Multivariate Normal Simulator, Stata Journal, 6(2): 190–213. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600203)
 - David M. Drukker,  Richard Gates, 2006, Generating Halton Sequences using Mata, Stata Journal, 6(2): 214–228. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600204)
 - Peter Haan,  Arne Uhlendorff, 2006, Estimation of Multinomial Logit Models with Unobserved Heterogeneity using Maximum Simulated Likelihood, Stata Journal, 6(2): 229–245. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600205)
 - Partha Deb,  Pravin K. Trivedi, 2006, Maximum Simulated Likelihood Estimation of a Negative Binomial Regression Model with Multinomial Endogenous Treatment, Stata Journal, 6(2): 246–255. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600206)
 - Mark Stewart, 2006, Maximum Simulated Likelihood Estimation of Random-Effects Dynamic Probit Models with Autocorrelated Errors, Stata Journal, 6(2): 256–272. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600207)
 - Richard Williams, 2006, Review of Regression Models for Categorical Dependent Variables Using Stata, Second Edition, by Long and Freese, Stata Journal, 6(2): 273–278. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600208)
 - Gueorgui I. Kolev, 2006, Stata Tip 31: Scalar or Variable? The Problem of Ambiguous Names, Stata Journal, 6(2): 279–280. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600209)
 - Stephen P. Jenkins, 2006, Stata Tip 32: Do not Stop, Stata Journal, 6(2): 281–281. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600210)
 - Nicholas J. Cox, 2006, Stata Tip 33: Sweet Sixteen: Hexadecimal Formats and Precision Problems, Stata Journal, 6(2): 282–283. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600211)
 
 
### SJ 6-3
 - Alfonso Miranda,  Sophia Rabe-Hesketh, 2006, Maximum Likelihood Estimation of Endogenous Switching and Sample Selection Models for Binary, Ordinal, and Count Variables, Stata Journal, 6(3): 285–308. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600301)
 - Roger Newson, 2006, Confidence Intervals for Rank Statistics: Somers’ D and Extensions, Stata Journal, 6(3): 309–334. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600302)
 - Anna Mikusheva,  Brian P. Poi, 2006, Tests and Confidence Sets with Correct Size when Instruments are Potentially Weak, Stata Journal, 6(3): 335–347. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600303)
 - Friederike Maria-Sophie Barthel,  Patrick Royston, 2006, Graphical Representation of Interactions, Stata Journal, 6(3): 348–363. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600304)
 - Brian P. Poi, 2006, Jackknife Instrumental Variables Estimation in Stata, Stata Journal, 6(3): 364–376. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600305)
 - Michael Lokshin, 2006, Difference-Based Semiparametric Estimation of Partial Linear Regression Models, Stata Journal, 6(3): 377–383. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600306)
 - David M. Drukker, 2006, Importing Federal Reserve Economic Data, Stata Journal, 6(3): 384–386. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600307)
 - William Gould, 2006, Mata Matters: Interactive use, Stata Journal, 6(3): 387–396. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600308)
 - Nicholas J. Cox, 2006, Speaking Stata: Graphs for all Seasons, Stata Journal, 6(3): 397–419. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600309)
 - Michael Mulcahy, 2006, Review of a Gentle Introduction to Stata by Acock, Stata Journal, 6(3): 420–424. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600310)
 - David A. Harrison, 2006, Stata Tip 34: Tabulation by Listing, Stata Journal, 6(3): 425–427. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600311)
 - William Gould, 2006, Stata Tip 35: Detecting Whether Data have Changed, Stata Journal, 6(3): 428–429. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600312)
 - Nicholas J. Cox, 2006, Stata Tip 36: Which Observations?, Stata Journal, 6(3): 430–432. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600313)
 
 
### SJ 6-4
 - Christian Brzinsky-Fay,  Ulrich Kohler,  Magdalena Luniak, 2006, Sequence Analysis with Stata, Stata Journal, 6(4): 435–460. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600401)
 - Martyn Andrews,  Thorsten Schank,  Richard Upward, 2006, Practical Fixed-Effects Estimation Methods for the Three-Way Error-Components Model, Stata Journal, 6(4): 461–481. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600402)
 - Rafael E. De Hoyos,  Vasilis Sarafidis, 2006, Testing for Cross-Sectional Dependence in Panel-Data Models, Stata Journal, 6(4): 482–496. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600403)
 - Roger Newson, 2006, Confidence Intervals for Rank Statistics: Percentile Slopes, Differences, and Ratios, Stata Journal, 6(4): 497–520. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600404)
 - Arne Risa Hole, 2006, Calculating Murphy–Topel Variance Estimates in Stata: A Simplified Procedure, Stata Journal, 6(4): 521–529. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600405)
 - John Thompson,  Tom Palmer,  Santiago Moreno, 2006, Bayesian Analysis in Stata with WinBUGS, Stata Journal, 6(4): 530–549. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600406)
 - William Gould, 2006, Mata Matters: Precision, Stata Journal, 6(4): 550–560. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600407)
 - Nicholas J. Cox, 2006, Speaking Stata: In Praise of Trigonometric Predictors, Stata Journal, 6(4): 561–579. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600408)
 - John Carlin, 2006, Review of An Introduction to Stata for Health Researchers by Juul, Stata Journal, 6(4): 580–583. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600409)
 - Scott W. Allard, 2006, Review of a Stata Companion to Political Analysis by Pollock, Stata Journal, 6(4): 584–587. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600410)
 - Christopher F. Baum, 2006, Stata Tip 37: And the Last shall be First, Stata Journal, 6(4): 588–589. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600411)
 - Christopher F. Baum, 2006, Stata Tip 38: Testing for Groupwise Heteroskedasticity, Stata Journal, 6(4): 590–592. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600412)
 - Nicholas J. Cox, 2006, Stata Tip 39: In a List or Out? In a Range or Out?, Stata Journal, 6(4): 593–595. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600413)
 - Nicholas J. Cox, 2006, Stata Tip 36: Which Observations? Erratum, Stata Journal, 6(4): 596–596. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0600600414)
 
 
## 2007
### SJ 7-1
 - Frauke Kreuter,  Richard Valliant, 2007, A Survey on Survey Statistics: What is Done and Can be Done in Stata, Stata Journal, 7(1): 1–21. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700101)
 - Jean-Benoit Hardouin, 2007, Rasch Analysis: Estimation and Tests with Raschtest, Stata Journal, 7(1): 22–44. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700102)
 - Patrick Royston,  Willi Sauerbrei, 2007, Multivariable Modeling with Cubic Regression Splines: A Principled Approach, Stata Journal, 7(1): 45–70. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700103)
 - Sascha O. Becker,  Marco Caliendo, 2007, Sensitivity Analysis for Average Treatment Effects, Stata Journal, 7(1): 71–83. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700104)
 - Pierpaolo Vittorini,  Stefano Necozione,  Ferdinando di Orio, 2007, Stata and the WeeW Information System, Stata Journal, 7(1): 84–97. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700105)
 - John Eng, 2007, File Filtering in Stata: Handling Complex Data Formats and Navigating Log Files Efficiently, Stata Journal, 7(1): 98–105. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700106)
 - William Gould, 2007, Mata Matters: Subscripting, Stata Journal, 7(1): 106–116. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700107)
 - Nicholas J. Cox, 2007, Speaking Stata: Making it Count, Stata Journal, 7(1): 117–130. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700108)
 - Austin Nichols, 2007, Review of An Introduction to Modern Econometrics Using Stata by Baum, Stata Journal, 7(1): 131–136. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700109)
 - Christopher F. Baum, 2007, Stata Tip 40: Taking Care of Business, Stata Journal, 7(1): 137–139. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700110)
 - David A. Harrison, 2007, Stata Tip 41: Monitoring Loop Iterations, Stata Journal, 7(1): 140–140. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700111)
 - James Cui, 2007, Stata Tip 42: The Overlay Problem: Offset for Clarity, Stata Journal, 7(1): 141–142. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700112)
 - Nicholas J. Cox, 2007, Stata Tip 43: Remainders, Selections, Sequences, Extractions: Uses of the Modulus, Stata Journal, 7(1): 143–145. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700113)
 
### SJ 7-2
 - Justine Shults,  Sarah J. Ratcliffe,  Mary Leonard, 2007, Improved Generalized Estimating Equation Analysis via xtqls for Quasi–Least Squares in Stata, Stata Journal, 7(2): 147–166. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700201)
 - Richard Chiburis,  Michael Lokshin, 2007, Maximum Likelihood and Two-Step Estimation of an Ordered-Probit Selection Model, Stata Journal, 7(2): 167–182. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700202)
 - Zhiqiang Wang, 2007, Two Postestimation Commands for Assessing Confounding Effects in Epidemiological Studies, Stata Journal, 7(2): 183–196. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700203)
 - Edward F. Blackburne, III,  Mark W. Frank, 2007, Estimation of Nonstationary Heterogeneous Panels, Stata Journal, 7(2): 197–208. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700204)
 - James Cui, 2007, QIC Program and Model Selection in GEE Analyses, Stata Journal, 7(2): 209–220. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700205)
 - Maarten L. Buis, 2007, Predict and Adjust with Logistic Regression, Stata Journal, 7(2): 221–226. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700206)
 - Ben Jann, 2007, Making Regression Tables Simplified, Stata Journal, 7(2): 227–244. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700207)
 - William D. Dupont, 2007, Review of a Handbook of Statistical Analyses Using Stata, Fourth Edition, by Rabe-Hesketh and Everitt, Stata Journal, 7(2): 245–248. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700208)
 - Nicholas J. Cox, 2007, Speaking Stata: Identifying Spells, Stata Journal, 7(2): 249–265. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700209)
 - Ben Jann, 2007, Stata Tip 44: Get a Handle on your Sample, Stata Journal, 7(2): 266–267. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700210)
 - Christopher F. Baum,  Nicholas J. Cox, 2007, Stata Tip 45: Getting those Data into Shape, Stata Journal, 7(2): 268–271. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700211)
 - Richard Williams, 2007, Stata Tip 46: Step We Gaily, on We Go, Stata Journal, 7(2): 272–274. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700212)
 - Nicholas J. Cox, 2007, Stata Tip 47: Quantile–Quantile Plots without Programming, Stata Journal, 7(2): 275–279. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700213)
 
 
### SJ 7-3
 - Daniel Hoechle, 2007, Robust Standard Errors for Panel Regressions with Cross-Sectional Dependence, Stata Journal, 7(3): 281–312. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700301)
 - Xiaohui Zheng,  Sophia Rabe-Hesketh, 2007, Estimating Parameters of Dichotomous and Ordinal Item Response Models with Gllamm, Stata Journal, 7(3): 313–333. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700302)
 - Tommaso Nannicini, 2007, Simulation-Based Sensitivity Analysis for Matching Estimators, Stata Journal, 7(3): 334–350. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700303)
 - Paul C. Lambert, 2007, Modeling of the Cure Fraction in Survival Studies, Stata Journal, 7(3): 351–375. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700304)
 - Patrick Royston, 2007, Profile Likelihood for Estimation and Confidence Intervals, Stata Journal, 7(3): 376–387. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700305)
 - Arne Risa Hole, 2007, Fitting Mixed Logit Models by Using Maximum Simulated Likelihood, Stata Journal, 7(3): 388–401. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700306)
 - Johannes Kaiser, 2007, An Exact and a Monte Carlo Proposal to the Fisher–Pitman Permutation Tests for Paired Replicates and for Independent Samples, Stata Journal, 7(3): 402–412. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700307)
 - Nicholas J. Cox, 2007, Speaking Stata: Turning over a New Leaf, Stata Journal, 7(3): 413–433. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700308)
 - Maarten L. Buis, 2007, Stata Tip 48: Discrete Uses for Uniform(), Stata Journal, 7(3): 434–435. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700309)
 - Scott Merryman, 2007, Stata Tip 49: Range Frame Plots, Stata Journal, 7(3): 436–437. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700310)
 - Nicholas J. Cox, 2007, Stata Tip 50: Efficient Use of Summarize, Stata Journal, 7(3): 438–439. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700311)
 - Nicholas J. Cox, 2007, Stata Tip 51: Events in Intervals, Stata Journal, 7(3): 440–443. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0700700312)
 
 
### SJ 7-4
 - Patrick Royston, 2007, Multiple Imputation of Missing Values: Further Update of Ice, with an Emphasis on Interval Censoring, Stata Journal, 7(4): 445–464. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800700401)
 - Christopher F. Baum,  Mark E. Schaffer,  Steven Stillman, 2007, Enhanced Routines for Instrumental Variables/Generalized Method of Moments Estimation and Testing, Stata Journal, 7(4): 465–506. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800700402)
 - Austin Nichols, 2007, Causal Inference with Observational Data, Stata Journal, 7(4): 507–541. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800700403)
 - Henrik Støvring, 2007, A Generic Function Evaluator Implemented in Mata, Stata Journal, 7(4): 542–555. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800700404)
 - William Gould, 2007, Mata Matters: Structures, Stata Journal, 7(4): 556–570. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800700405)
 - Nicholas J. Cox, 2007, Speaking Stata: Counting Groups, Especially Panels, Stata Journal, 7(4): 571–581. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800700406)
 - Nicholas J. Cox, 2007, Stata Tip 52: Generating Composite Categorical Variables, Stata Journal, 7(4): 582–583. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800700407)
 - Maarten L. Buis, 2007, Stata Tip 53: Where did My P-Values Go?, Stata Journal, 7(4): 584–586. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800700408)
 - Philippe Van Kerm, 2007, Stata Tip 54: Post your Results, Stata Journal, 7(4): 587–589. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800700409)
 - Nicholas J. Cox, 2007, Stata Tip 55: Better Axis Labeling for Time Points and Time Intervals, Stata Journal, 7(4): 590–592. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800700410)
 
 
## 2008
### SJ 8-1
 - H. Joseph Newton,  Nicholas J. Cox, 2008, Editorial Announcements, Stata Journal, 8(1): 1–2. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800101)
 - Ross J. Harris,  Jonathan J. Deeks,  Douglas G. Altman,  Michael J. Bradburn,  Roger M. Harbord,  Jonathan A. C. Sterne, 2008, Metan: Fixed- and Random-Effects Meta-Analysis, Stata Journal, 8(1): 3–28. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800102)
 - Nicola Orsini,  Rino Bellocco,  Matteo Bottai,  Alicja Wolk,  Sander Greenland, 2008, A Tool for Deterministic and Probabilistic Sensitivity Analysis of Epidemiologic Studies, Stata Journal, 8(1): 29–48. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800103)
 - John B. Carlin,  John C. Galati,  Patrick Royston, 2008, A New Framework for Managing and Analyzing Multiply Imputed Data in Stata, Stata Journal, 8(1): 49–67. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800104)
 - Walter Sosa-Escudero,  Anil K. Bera, 2008, Tests for Unbalanced Error-Components Models under Local Misspecification, Stata Journal, 8(1): 68–78. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800105)
 - Kyle C. Longest,  Stephen Vaisey, 2008, Fuzzy: A Program for Performing Qualitative Comparative Analyses (QCA) in Stata, Stata Journal, 8(1): 79–104. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800106)
 - Nicholas J. Cox, 2008, Speaking Stata: Spineplots and their Kin, Stata Journal, 8(1): 105–121. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800107)
 - Stephen P. Jenkins, 2008, Review of Applied Health Economics by Jones, Rice, Bago d'Uva, and Balia, Stata Journal, 8(1): 122–128. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800108)
 - Frank Kalter, 2008, Review of Event History Analysis with Stata by Blossfeld, Golsch, and Rohwer, Stata Journal, 8(1): 129–133. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800109)
 - Rosa Gini, 2008, Stata Tip 56: Writing Parameterized Text Files, Stata Journal, 8(1): 134–136. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800110)
 - Bill Gould, 2008, Stata Tip 57: How to Reinstall Stata, Stata Journal, 8(1): 137–138. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800111)
 - Brian P. Poi, 2008, Stata Tip 58: Nl is not Just for Nonlinear Models, Stata Journal, 8(1): 139–141. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800112)
 - Nicholas J. Cox, 2008, Stata Tip 59: Plotting on Any Transformed Scale, Stata Journal, 8(1): 142–145. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800113)
 
 
### SJ 8-2
 - Ben Jann, 2008, Multinomial Goodness-of-Fit: Large-Sample Tests with Survey Design Correction and Exact Tests for Small Samples, Stata Journal, 8(2): 147–169. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800201)
 - Thomas Cornelissen, 2008, The Stata Command Felsdvreg to Fit a Linear Model with Two High-Dimensional Fixed Effects, Stata Journal, 8(2): 170–189. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800202)
 - Giuseppe De Luca, 2008, SNP and SML Estimation of Univariate and Bivariate Binary-Choice Models, Stata Journal, 8(2): 190–220. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800203)
 - Mahmut Yasar,  Rafal Raciborski,  Brian Poi, 2008, Production Function Estimation in Stata Using the Olley and Pakes Method, Stata Journal, 8(2): 221–231. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800204)
 - Damiaan Persyn,  Joakim Westerlund, 2008, Error-Correction–Based Cointegration Tests for Panel Data, Stata Journal, 8(2): 232–241. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800205)
 - Tom M. Palmer,  Alex J. Sutton,  Jaime L. Peters,  Santiago G. Moreno, 2008, Contour-Enhanced Funnel Plots for Meta-Analysis, Stata Journal, 8(2): 242–254. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800206)
 - Jean Marie Linhart, 2008, Mata Matters: Overflow, Underflow and the IEEE Floating-Point Format, Stata Journal, 8(2): 255–268. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800207)
 - Nicholas J. Cox, 2008, Speaking Stata: Between Tables and Graphs, Stata Journal, 8(2): 269–289. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800208)
 - Alan R. Riley, 2008, Stata Tip 60: Making Fast and Easy Changes to Files with Filefilter, Stata Journal, 8(2): 290–292. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800209)
 - Nicholas J. Cox, 2008, Stata Tip 61: Decimal Commas in Results Output and Data Input, Stata Journal, 8(2): 293–294. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800210)
 - Nicholas J. Cox,  Natasha L. M. Barlow, 2008, Stata Tip 62: Plotting on Reversed Scales, Stata Journal, 8(2): 295–298. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800211)
 - Christopher F. Baum, 2008, Stata Tip 63: Modeling Proportions, Stata Journal, 8(2): 299–303. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800212)
 
### SJ 8-3
 - Yulia V. Marchenko,  Raymond J. Carroll,  Danyu Y. Lin,  Christopher I. Amos,  Roberto G. Gutierrez, 2008, Semiparametric Analysis of Case–control Genetic Data in the Presence of Environmental Factors, Stata Journal, 8(3): 305–333. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800301)
 - Richard Emsley,  Mark Lunt,  Andrew Pickles,  Graham Dunn, 2008, Implementing Double-robust Estimators of Causal Effects, Stata Journal, 8(3): 334–353. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800302)
 - Michela Bia,  Alessandra Mattei, 2008, A Stata Package for the Estimation of the Dose-response Function through Adjustment for the Generalized Propensity Score, Stata Journal, 8(3): 354–373. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800303)
 - Michael Lokshin,  Zurab Sajaia, 2008, Creating Print-ready Tables in Stata, Stata Journal, 8(3): 374–389. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800304)
 - Rafal Raciborski, 2008, kountry: A Stata Utility for Merging Cross-country Data from Multiple Sources, Stata Journal, 8(3): 390–400. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800305)
 - William Gould, 2008, Mata Matters: Macros, Stata Journal, 8(3): 401–412. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800306)
 - Nicholas J. Cox, 2008, Speaking Stata: Correlation with Confidence, or Fisher's z revisited, Stata Journal, 8(3): 413–439. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800307)
 - Scott Merryman, 2008, Review of a Visual Guide to Stata Graphics, Second Edition by Michael N. Mitchell, Stata Journal, 8(3): 440–443. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800308)
 - Jeph Herrin,  Eva Poen, 2008, Stata tip 64: Cleaning up User-entered String Variables, Stata Journal, 8(3): 444–445. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800309)
 - Nicholas J. Cox, 2008, Stata tip 65: Beware the Backstabbing Backslash, Stata Journal, 8(3): 446–447. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800310)
 - Martin Weiss, 2008, Stata tip 66: Ds—A Hidden Gem, Stata Journal, 8(3): 448–449. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800311)
 - Nicholas J. Cox, 2008, Stata Tip 67: J() Now has Greater Replicating Powers, Stata Journal, 8(3): 450–451. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800312)
 
 
### SJ 8-4
 - Ben Jann, 2008, The Blinder–Oaxaca Decomposition for Linear Regression Models, Stata Journal, 8(4): 453–479. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800401)
 - Mathias Sinning,  Markus Hahn,  Thomas K. Bauer, 2008, The Blinder–Oaxaca Decomposition for Nonlinear Regression Models, Stata Journal, 8(4): 480–492. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800402)
 - Roger M. Harbord,  Julian P. T. Higgins, 2008, Meta-Regression in Stata, Stata Journal, 8(4): 493–519. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800403)
 - Brady T. West,  Patricia Berglund,  Steven G. Heeringa, 2008, A Closer Examination of Subpopulation Analysis of Complex-Sample Survey Data, Stata Journal, 8(4): 520–531. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800404)
 - Austin Nichols, 2008, Erratum and Discussion of Propensity-Score Reweighting, Stata Journal, 8(4): 532–539. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800405)
 - Ward Vanlaar, 2008, A Shortcut through Long Loops: An Illustration of Two Alternatives to Looping over Observations, Stata Journal, 8(4): 540–553. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800406)
 - Brian P. Poi, 2008, Demand-System Estimation: Update, Stata Journal, 8(4): 554–556. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800407)
 - Nicholas J. Cox,  Gary M. Longton, 2008, Speaking Stata: Distinct Observations, Stata Journal, 8(4): 557–568. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800408)
 - Antoine Terracol, 2008, Review of Stata Par La Pratique: Statistiques, Graphiques et éléments De Programmation, by Éric Cahuzac and Christophe Bontemps, Stata Journal, 8(4): 569–573. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800409)
 - Antoine Terracol, 2008, Stata Par la Pratique: Statistiques, Graphiques et éléments de Programmation par Éric Cahuzac et Christophe Bontemps, Stata Journal, 8(4): 574–578. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800410)
 - Nicholas J. Horton, 2008, Review of Multilevel and Longitudinal Modeling Using Stata, Second Edition, by Sophia Rabe-Hesketh and Anders Skrondal, Stata Journal, 8(4): 579–582. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800411)
 - Alan R. Riley, 2008, Stata Tip 69: Producing Log Files Based on Successful Interactive Commands, Stata Journal, 8(4): 583–585. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800412)
 - Nicholas J. Cox, 2008, Stata Tip 70: Beware the Evaluating Equal Sign, Stata Journal, 8(4): 586–587. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800413)
 - Nicholas J. Cox, 2008, Stata Tip 71: The Problem of Split Identity, or How to Group Dyads, Stata Journal, 8(4): 588–591. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800414)
 - Kevin Crow, 2008, Stata Tip 72: Using the Graph Recorder to Create a Pseudograph Scheme, Stata Journal, 8(4): 592–593. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800415)
 
 
## 2009
### SJ 9-1
 - Margaret S. Pepe,  Gary Longton,  Holly Janes, 2009, Estimation and Comparison of Receiver Operating Characteristic Curves, Stata Journal, 9(1): 1–16. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900101)
 - Holly Janes,  Gary Longton,  Margaret S. Pepe, 2009, Accommodating Covariates in Receiver Operating Characteristic Analysis, Stata Journal, 9(1): 17–39. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900102)
 - Ian R. White, 2009, Multivariate Random-effects Meta-analysis, Stata Journal, 9(1): 40–56. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900103)
 - Ian R. White,  Julian P. T. Higgins, 2009, Meta-analysis with Missing Data, Stata Journal, 9(1): 57–69. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900104)
 - Johannes Kaiser,  Michael G. Lacy, 2009, A General-purpose Method for Two-group Randomization Tests, Stata Journal, 9(1): 70–85. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900105)
 - David Roodman, 2009, How to do Xtabond2: An Introduction to Difference and System GMM in Stata, Stata Journal, 9(1): 86–136. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900106)
 - Nicholas J. Cox, 2009, Speaking Stata: Rowwise, Stata Journal, 9(1): 137–157. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900107)
 - Alan C. Acock, 2009, Review of the Workflow of Data Analysis Using Stata, by J. Scott Long, Stata Journal, 9(1): 158–160. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900108)
 - Christopher F. Baum,  Teresa Linz, 2009, Evaluating Concavity for Production and Cost Functions, Stata Journal, 9(1): 161–165. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900109)
 - Christopher F. Baum, 2009, Stata Tip 73: Append with Care!, Stata Journal, 9(1): 166–168. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900110)
 - Roberto G. Gutierrez,  Peter A. Lachenbruch, 2009, Stata Tip 74: Firstonly, a New Option for Tab2, Stata Journal, 9(1): 169–170. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900111)
 - Kevin Crow, 2009, Stata Tip 75: Setting up Stata for a Presentation, Stata Journal, 9(1): 171–172. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900112)
 
 
### SJ 9-2
 - Peter Cummings, 2009, Methods for Estimating Adjusted Risk Ratios, Stata Journal, 9(2): 175–196. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900201)
 - Roger M. Harbord,  Ross J. Harris,  Jonathan A. C. Sterne, 2009, Updated Tests for Small-study Effects in Meta-analyses, Stata Journal, 9(2): 197–210. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900202)
 - Roger M. Harbord,  Penny Whiting, 2009, Metandi: Meta-analysis of Diagnostic Accuracy Using Hierarchical Logistic Regression, Stata Journal, 9(2): 211–229. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900203)
 - Patrick Royston,  Willi Sauerbrei, 2009, Two Techniques for Investigating Interactions between Treatment and Continuous Covariates in Clinical Trials, Stata Journal, 9(2): 230–251. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900204)
 - Patrick Royston,  John B. Carlin,  Ian R. White, 2009, Multiple Imputation of Missing Values: New Features for Mim, Stata Journal, 9(2): 252–264. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900205)
 - Paul C. Lambert,  Patrick Royston, 2009, Further Development of Flexible Parametric Models for Survival Analysis, Stata Journal, 9(2): 265–290. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900206)
 - Alexis Dinno, 2009, Implementing Horn's Parallel Analysis for Principal Component Analysis and Factor Analysis, Stata Journal, 9(2): 291–298. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900207)
 - Mark Chatfield,  Adrian Mander, 2009, The Skillings–Mack Test (Friedman Test when There are Missing Data), Stata Journal, 9(2): 299–305. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900208)
 - Nicholas J. Cox, 2009, Speaking Stata: I. J. Good and Quasi-Bayes Smoothing of Categorical Frequencies, Stata Journal, 9(2): 306–314. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900209)
 - Andrew Gelman, 2009, A statistician's perspective on “Mostly Harmless Econometrics: An Empiricist's Companion”, by Joshua D. Angrist and Jörn-Steffen Pischke, Stata Journal, 9(2): 315–320. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900210)
 - Nicholas J. Cox, 2009, Stata Tip 76: Separating Seasonal Time Series, Stata Journal, 9(2): 321–326. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900211)
 
 
### SJ 9-3
 - Stanislav Kolenikov, 2009, Confirmatory Factor Analysis using Confa, Stata Journal, 9(3): 329–373. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900301)
 - Rafal Raciborski, 2009, Graphical Representation of Multivariate Data using Chernoff Faces, Stata Journal, 9(3): 374–387. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900302)
 - Yulia V. Marchenko,  Jerome P. Reiter, 2009, Improved Degrees of Freedom for Multivariate Significance Tests Obtained from Multiply Imputed, Small-Sample Data, Stata Journal, 9(3): 388–397. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900303)
 - Keith Finlay,  Leandro M. Magnusson, 2009, Implementing Weak-Instrument Robust Tests for a General Class of Instrumental-Variables Models, Stata Journal, 9(3): 398–421. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900304)
 - Domenico Depalo, 2009, A Seasonal Unit-Root Test with Stata, Stata Journal, 9(3): 422–438. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900305)
 - Vincenzo Verardi,  Christophe Croux, 2009, Robust Regression in Stata, Stata Journal, 9(3): 439–453. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900306)
 - Sebastian J. Goerg,  Johannes Kaiser, 2009, Nonparametric Testing of Distributions—the Epps–Singleton Two-Sample Test using the Empirical Characteristic Function, Stata Journal, 9(3): 454–465. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900307)
 - Patrick Royston, 2009, Multiple Imputation of Missing Values: Further Update of Ice, with an Emphasis on Categorical Variables, Stata Journal, 9(3): 466–477. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900308)
 - Nicholas J. Cox, 2009, Speaking Stata: Creating and Varying Box Plots, Stata Journal, 9(3): 478–496. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900309)
 - Jeph Herrin, 2009, Stata Tip 77: (Re)Using Macros in Multiple do-Files, Stata Journal, 9(3): 497–498. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900310)
 - Nicholas J. Cox, 2009, Stata Tip 78: Going Gray Gracefully: Highlighting Subsets and Downplaying Substrates, Stata Journal, 9(3): 499–503. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900311)
 - Nicholas J. Cox, 2009, Stata Tip 79: Optional Arguments to Options, Stata Journal, 9(3): 504–504. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900312)
 
### SJ 9-4
 - Friederike M.-S. Barthel,  Patrick Royston,  Mahesh K. B. Parmar, 2009, A Menu-Driven Facility for Sample-Size Calculation in Novel Multiarm, Multistage Randomized Controlled Trials with a Time-to-Event Outcome, Stata Journal, 9(4): 505–523. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900401)
 - Matthew Blackwell,  Stefano Iacus,  Gary King,  Giuseppe Porro, 2009, Cem: Coarsened Exact Matching in Stata, Stata Journal, 9(4): 524–546. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900402)
 - Patrick Royston,  Willi Sauerbrei, 2009, Bootstrap Assessment of the Stability of Multivariable Models, Stata Journal, 9(4): 547–570. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900403)
 - Thomas Cornelißen,  Katja Sonderhof, 2009, Partial Effects in Probit and Logit Models with a Triple Dummy-Variable Interaction Term, Stata Journal, 9(4): 571–583. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900404)
 - William J. Burke, 2009, Fitting and Interpreting Cragg's Tobit Alternative using Stata, Stata Journal, 9(4): 584–592. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900405)
 - Ivan Žežula, 2009, Implementation of a New Solution to the Multivariate Behrens-Fisher Problem, Stata Journal, 9(4): 593–598. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900406)
 - William Gould, 2009, Mata Matters: File Processing, Stata Journal, 9(4): 599–620. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900407)
 - Nicholas J. Cox, 2009, Speaking Stata: Paired, Parallel, or Profile Plots for Changes, Correlations, and Other Comparisons, Stata Journal, 9(4): 621–639. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900408)
 - Martin Weiss, 2009, Stata Tip 80: Constructing a Group Variable with Specified Group Sizes, Stata Journal, 9(4): 640–642. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900409)
 - Maarten L. Buis,  Martin Weiss, 2009, Stata Tip 81: A Table of Graphs, Stata Journal, 9(4): 643–647. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900410)
 - Nicholas J. Cox, 2009, Stata Tip 82: Grounds for Grids on Graphs, Stata Journal, 9(4): 648–651. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0900900411)
 
 
## 2010
### SJ 10-1
 - H. Joseph Newton,  Nicholas J. Cox, 2010, Editorial Announcements, Stata Journal, 10(1): 1–2. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000101)
 - Nicholas J. Cox, 2010, A Conversation with Kit Baum, Stata Journal, 10(1): 3–8. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000102)
 - Teresa Timberlake,  Nicholas J. Cox, 2010, Ana Isabel Palma Carlos Timberlake (1943–2009), Stata Journal, 10(1): 9–10. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000103)
 - Maarten L. Buis, 2010, Direct and Indirect Effects in a Logit Model, Stata Journal, 10(1): 11–29. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000104)
 - P. Wilner Jeanty, 2010, Using the World Development Indicators Database for Statistical Analysis in Stata, Stata Journal, 10(1): 30–45. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000105)
 - Ben Jann,  J. Scott Long, 2010, Tabulating SPost Results Using Estout and Esttab, Stata Journal, 10(1): 46–60. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000106)
 - Milena Falcaro,  Andrew Pickles, 2010, Riskplot: A Graphical aid to Investigate the Effect of Multiple Categorical Risk Factors, Stata Journal, 10(1): 61–68. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000107)
 - Charles Lindsey,  Simon Sheather, 2010, Power Transformation via Multivariate Box–Cox, Stata Journal, 10(1): 69–81. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000108)
 - Kata Mihaly,  Daniel F. McCaffrey,  J. R. Lockwood,  Tim R. Sass, 2010, Centering and Reference Groups for Estimates of Fixed Effects: Modifications to Felsdvreg, Stata Journal, 10(1): 82–103. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000109)
 - Joseph M. Hilbe, 2010, Creating Synthetic Discrete-response Regression Models, Stata Journal, 10(1): 104–124. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000110)
 - William Gould, 2010, Mata Matters: Stata in Mata, Stata Journal, 10(1): 125–142. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000111)
 - Nicholas J. Cox, 2010, Speaking Stata: The Statsby Strategy, Stata Journal, 10(1): 143–151. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000112)
 - Devra L. Golbe, 2010, Stata Tip 83: Merging Multilingual Datasets, Stata Journal, 10(1): 152–156. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000113)
 - Nicholas J. Cox, 2010, Stata Tip 84: Summing Missings, Stata Journal, 10(1): 157–159. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000114)
 - Nicholas J. Cox, 2010, Stata Tip 85: Looping over Nonintegers, Stata Journal, 10(1): 160–163. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000115)
 
 
### SJ 10-2
 - Stanislav Kolenikov, 2010, Resampling Variance Estimation for Complex Survey Data, Stata Journal, 10(2): 165–199. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000201)
 - Charles Lindsey,  Simon Sheather, 2010, Optimal Power Transformation via Inverse Response Plots, Stata Journal, 10(2): 200–214. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000202)
 - Charles Lindsey,  Simon Sheather, 2010, Model Fit Assessment via Marginal Model Plots, Stata Journal, 10(2): 215–225. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000203)
 - Nikos Pantazis,  Giota Touloumi, 2010, Analyzing Longitudinal Data in the Presence of Informative Dropout: The Jmre1 Command, Stata Journal, 10(2): 226–251. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000204)
 - Juan Muro,  Cristina Suáarez,  María del Mar Zamora, 2010, Computing Murphy-Topel-corrected Variances in a Heckprobit Model with Endogeneity, Stata Journal, 10(2): 252–258. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000205)
 - Vincenzo Verardi,  Catherine Dehon, 2010, Multivariate Outlier Detection in Stata, Stata Journal, 10(2): 259–266. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000206)
 - Yong-bae Ji,  Choonjoo Lee, 2010, Data Envelopment Analysis, Stata Journal, 10(2): 267–280. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000207)
 - Nicholas J. Cox, 2010, Speaking Stata: Finding Variables, Stata Journal, 10(2): 281–296. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000208)
 - William D. Dupont, 2010, Review of Multivariable Model-building: A Pragmatic Approach to Regression Analysis Based on Fractional Polynomials for Modeling Continuous Variables, by Royston and Sauerbrei, Stata Journal, 10(2): 297–302. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000209)
 - Bill Rising, 2010, Stata Tip 86: The Missing() Function, Stata Journal, 10(2): 303–304. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000210)
 - Maarten L. Buis, 2010, Stata Tip 87: Interpretation of Interactions in Nonlinear Models, Stata Journal, 10(2): 305–308. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000211)
 - Christopher F. Baum, 2010, Stata Tip 88: Efficiently Evaluating Elasticities with the Margins Command, Stata Journal, 10(2): 309–312. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000212)
 
 
### SJ 10-3
 - Martin Wittenberg, 2010, An Introduction to Maximum Entropy and Minimum Cross-entropy Estimation Using Stata, Stata Journal, 10(3): 315–330. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000301)
 - Sylvain Weber, 2010, Bacon: An Effective way to Detect Outliers in Multivariate Data Using Stata (and Mata), Stata Journal, 10(3): 331–338. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000302)
 - Roger B. Newson, 2010, Comparing the Predictive Powers of Survival Models Using Harrell's C or Somers’ D, Stata Journal, 10(3): 339–358. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000303)
 - J. Charles Huber, Jr., 2010, Using Stata with PHASE and Haploview: Commands for Importing and Exporting Data, Stata Journal, 10(3): 359–368. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000304)
 - Ian R. White, 2010, Simsum: Analyses of Simulation Studies Including Monte Carlo Error, Stata Journal, 10(3): 369–385. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000305)
 - Patrick Royston,  Friederike M.-S. Barthel, 2010, Projection of Power and Events in Clinical Trials with a Time-to-event Outcome, Stata Journal, 10(3): 386–394. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000306)
 - Evangelos Kontopantelis,  David Reeves, 2010, Metaan: Random-effects Meta-analysis, Stata Journal, 10(3): 395–407. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000307)
 - Erik T. Parner,  Per K. Andersen, 2010, Regression Analysis of Censored Data Using Pseudo-observations, Stata Journal, 10(3): 408–422. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000308)
 - Markus Fröolich,  Blaise Melly, 2010, Estimation of Quantile Treatment Effects with Stata, Stata Journal, 10(3): 423–457. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000309)
 - Federico Belotti,  Domenico Depalo, 2010, Translation from Narrative Text to Standard Codes Variables with Stata, Stata Journal, 10(3): 458–481. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000310)
 - Nicholas J. Cox, 2010, Speaking Stata: The Limits of Sample Skewness and Kurtosis, Stata Journal, 10(3): 482–495. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000311)
 - Peter A. Lachenbruch, 2010, Stata Tip 89: Estimating Means and Percentiles following Multiple Imputation, Stata Journal, 10(3): 496–499. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000312)
 - Martin Weiss, 2010, Stata Tip 90: Displaying Partial Results, Stata Journal, 10(3): 500–502. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000313)
 - Nicholas J. Cox, 2010, Stata Tip 91: Putting Unabbreviated Varlists into Local Macros, Stata Journal, 10(3): 503–504. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000314)
 
 
### SJ 10-4
 - Yulia V. Marchenko,  Marc G. Genton, 2010, A Suite of Commands for Fitting the Skew-normal and Skew-t models, Stata Journal, 10(4): 507–539. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101000401)
 - Richard Williams, 2010, Fitting Heterogeneous Choice Models with Oglm, Stata Journal, 10(4): 540–567. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101000402)
 - Roger B. Newson, 2010, Frequentist Q-values for Multiple-test Procedures, Stata Journal, 10(4): 568–584. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101000403)
 - Eric Neumayer,  Thomas Plümper, 2010, Making Spatial Analysis Operational: Commands for Generating Spatial-effect Variables in Monadic and Dyadic Data, Stata Journal, 10(4): 585–605. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101000404)
 - Mark J. Rutherford,  Paul C. Lambert,  John R. Thompson, 2010, Age–period–cohort Modeling, Stata Journal, 10(4): 606–627. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101000405)
 - Paulo Guimarães,  Pedro Portugal, 2010, A Simple Feasible Procedure to fit Models with High-dimensional Fixed Effects, Stata Journal, 10(4): 628–649. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101000406)
 - Charles Lindsey,  Simon Sheather, 2010, Variable Selection in Linear Regression, Stata Journal, 10(4): 650–669. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101000407)
 - Nicholas J. Cox, 2010, Speaking Stata: Graphing Subsets, Stata Journal, 10(4): 670–681. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101000408)
 - Nicholas J. Cox, 2010, Stata Tip 68: Week Assumptions, Stata Journal, 10(4): 682–685. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101000409)
 - Lars Ängquist, 2010, Stata Tip 92: Manual Implementation of Permutations and Bootstraps, Stata Journal, 10(4): 686–688. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101000410)
 - Vince Wiggins, 2010, Stata Tip 93: Handling Multiple y Axes on Twoway Graphs, Stata Journal, 10(4): 689–690. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101000411)
 
 
## 2011
### SJ 11-1
 - Nicola Orsini,  Sander Greenland, 2011, A Procedure to Tabulate and Plot Results after Flexible Modeling of a Quantitative Covariate, Stata Journal, 11(1): 1–29. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100101)
 - Jean-Benoit Hardouin,  Angélique Bonnaud-Antignac,  Véronique Sébille, 2011, Nonparametric Item Response Theory Using Stata, Stata Journal, 11(1): 30–51. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100102)
 - Rense Corten, 2011, Visualization of Social Networks in Stata Using Multidimensional Scaling, Stata Journal, 11(1): 52–63. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100103)
 - Matthew Cefalu, 2011, Pointwise Confidence Intervals for the Covariate-adjusted Survivor Function in the Cox Model, Stata Journal, 11(1): 64–81. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100104)
 - Helmut Farbmacher, 2011, Estimation of Hurdle Models for Overdispersed Count Data, Stata Journal, 11(1): 82–94. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100105)
 - Rafal Raciborski, 2011, Right-censored Poisson Regression Model, Stata Journal, 11(1): 95–105. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100106)
 - Adam Ozimek,  Daniel Miles, 2011, Stata Utilities for Geocoding and Generating Travel Time and Travel Distance Information, Stata Journal, 11(1): 106–119. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100107)
 - Juan Manuel Ramos-Goñi,  Oliver Rivero-Arias, 2011, Eq5d: A command to Calculate Index Values for the EQ-5D Quality-of-life Instrument, Stata Journal, 11(1): 120–125. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100108)
 - Nicholas J. Cox, 2011, Speaking Stata: MMXI and all That: Handling Roman Numerals within Stata, Stata Journal, 11(1): 126–142. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100109)
 - Theresa Boswell,  Roberto G. Gutierrez, 2011, Stata Tip 94: Manipulation of Prediction Parameters for Parametric Survival Regression Models, Stata Journal, 11(1): 143–144. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100110)
 - Nicholas J. Horton, 2011, Stata Tip 95: Estimation of Error Covariances in a Linear Model, Stata Journal, 11(1): 145–148. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100111)
 - Nicholas J. Cox, 2011, Stata Tip 96: Cube Roots, Stata Journal, 11(1): 149–154. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100112)
 
 
### SJ 11-2
 - Christopher F. Baum, 2011, Richard Sperling (1961–2011), Stata Journal, 11(2): 157–158. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100201)
 - David Roodman, 2011, Fitting Fully Observed Recursive Mixed-process Models with cmp, Stata Journal, 11(2): 159–206. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100202)
 - J. M. C. Santos Silva,  Silvana Tenreyro, 2011, Poisson: Some Convergence Issues, Stata Journal, 11(2): 207–212. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100203)
 - Giuseppe De Luca,  Valeria Perotti, 2011, Estimation of Ordered Response Models with Sample Selection, Stata Journal, 11(2): 213–239. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100204)
 - Cornelia U. Kunz,  Meinhard Kieser, 2011, Simon's Minimax and Optimal and Jung's Admissible Two-stage Designs with or without Curtailment, Stata Journal, 11(2): 240–254. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100205)
 - Ian R. White, 2011, Multivariate Random-effects Meta-regression: Updates to Mvmeta, Stata Journal, 11(2): 255–270. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100206)
 - Pietro Tebaldi,  Marco Bonetti,  Marcello Pagano, 2011, M Statistic Commands: Interpoint Distance Distribution Analysis, Stata Journal, 11(2): 271–289. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100207)
 - Peter Cummings, 2011, Estimating Adjusted Risk Ratios for Matched and Unmatched Data: An Update, Stata Journal, 11(2): 290–298. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100208)
 - Katarina Lukacsy, 2011, Generating Random Samples from User-defined Distributions, Stata Journal, 11(2): 299–304. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100209)
 - Nicholas J. Cox, 2011, Speaking Stata: Compared with …, Stata Journal, 11(2): 305–314. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100210)
 - Maarten L. Buis, 2011, Stata Tip 97: Getting at ρ's and σ's, Stata Journal, 11(2): 315–317. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100211)
 - Nicholas J. Cox, 2011, Stata Tip 98: Counting Substrings within Strings, Stata Journal, 11(2): 318–320. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100212)
 - Clyde Schechter, 2011, Stata Tip 99: Taking Extra Care with Encode, Stata Journal, 11(2): 321–322. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100213)
 - William Gould,  Nicholas J. Cox, 2011, Stata Tip 100: Mata and the Case of the Missing Macros, Stata Journal, 11(2): 323–324. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100214)
 
 
### SJ 11-3
 - Nicola Orsini,  Matteo Bottai, 2011, Logistic Quantile Regression in Stata, Stata Journal, 11(3): 327–344. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100301)
 - Tom M. Palmer,  Roland R. Ramsahai,  Vanessa Didelez,  Nuala A. Sheehan, 2011, Nonparametric Bounds for the Causal Effect in a Binary Instrumental-Variable Model, Stata Journal, 11(3): 345–367. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100302)
 - Michael Lokshin,  Zurab Sajaia, 2011, Impact of Interventions on Discrete Outcomes: Maximum Likelihood Estimation of the Binary Choice Models with Binary Endogenous Regressors, Stata Journal, 11(3): 368–385. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100303)
 - Maria Paola Caria,  Maria Rosaria Galanti,  Rino Bellocco,  Nicholas J. Horton, 2011, The Impact of Different Sources of Body Mass Index Assessment on Smoking Onset: An Application of Multiple-Source Information Models, Stata Journal, 11(3): 386–402. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100304)
 - David A. Wagstaff,  Ofer Harel, 2011, A Closer Examination of Three Small-Sample Approximations to the Multiple-Imputation Degrees of Freedom, Stata Journal, 11(3): 403–419. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100305)
 - Ulrich Kohler,  Kristian Bernt Karlson,  Anders Holm, 2011, Comparing Coefficients of Nested Nonlinear Probability Models, Stata Journal, 11(3): 420–438. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100306)
 - Aedín Doris,  Donal O'Neill,  Olive Sweetman, 2011, GMM Estimation of the Covariance Structure of Longitudinal Data on Earnings, Stata Journal, 11(3): 439–459. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100307)
 - Nicholas J. Cox, 2011, Speaking Stata: Fun and Fluency with Functions, Stata Journal, 11(3): 460–471. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100308)
 - Nicholas J. Cox, 2011, Stata Tip 101: Previous but Different, Stata Journal, 11(3): 472–473. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100309)
 - Nicholas J. Cox, 2011, Stata Tip 102: Highlighting Specific Bars, Stata Journal, 11(3): 474–477. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101100310)
 
 
### SJ 11-4
 - Rhian M. Daniel,  Bianca L. De Stavola,  Simon N. Cousens, 2011, Gformula: Estimating Causal Effects in the Presence of Time-Varying Confounding or Mediation using the G-Computation Formula, Stata Journal, 11(4): 479–517. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201100401)
 - Giuseppe De Luca,  Jan R. Magnus, 2011, Bayesian Model Averaging and Weighted-Average Least Squares: Equivariance, Stability, and Numerical Issues, Stata Journal, 11(4): 518–544. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201100402)
 - Graham K. Brown,  Thanos Mergoupis, 2011, Treatment Interactions with Nonexperimental Data in Stata, Stata Journal, 11(4): 545–555. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201100403)
 - Daniel A. Powers,  Hirotoshi Yoshioka,  Myeong-Su Yun, 2011, Mvdcmp: Multivariate Decomposition for Nonlinear Response Models, Stata Journal, 11(4): 556–576. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201100404)
 - Laron K. Williams,  Guy D. Whitten, 2011, Dynamic Simulations of Autoregressive Relationships, Stata Journal, 11(4): 577–588. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201100405)
 - P. Wilner Jeanty, 2011, Managing the U.S. Census 2000 and World Development Indicators Databases for Statistical Analysis in Stata, Stata Journal, 11(4): 589–604. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201100406)
 - Raymond Hicks,  Dustin Tingley, 2011, Causal Mediation Analysis, Stata Journal, 11(4): 605–619. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201100407)
 - Mehmet F. Dicle,  John Levendis, 2011, Importing Financial Data, Stata Journal, 11(4): 620–626. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201100408)
 - Ulrich Kohler,  Stephanie Eckman, 2011, Stata tip 103: Expressing confidence with gradations, Stata Journal, 11(4): 627–631. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201100409)
 - Nicholas J. Cox, 2011, Stata Tip 104: Added Text and Title Options, Stata Journal, 11(4): 632–633. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201100410)
 
 
## 2012
### SJ 12-1
 - H. Joseph Newton,  Nicholas J. Cox, 2012, Announcement of the Stata Journal Editors’ Prize 2012, Stata Journal, 12(1): 1–2. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200101)
 - John Luke Gallup, 2012, A New System for Formatting Estimation Tables, Stata Journal, 12(1): 3–28. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200102)
 - Stanislav Kolenikov, 2012, Scrambled Halton Sequences in Mata, Stata Journal, 12(1): 29–44. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200103)
 - Peter D. Sasieni, 2012, Age–Period–Cohort Models in Stata, Stata Journal, 12(1): 45–60. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200104)
 - Markus Eberhardt, 2012, Estimating Panel Time-Series Models with Heterogeneous Slopes, Stata Journal, 12(1): 61–71. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200105)
 - Matthias Schonlau,  Elisabeth Liebau, 2012, Respondent-Driven Sampling, Stata Journal, 12(1): 72–93. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200106)
 - Hirotaka Miura, 2012, Stata Graph Library for Network Analysis, Stata Journal, 12(1): 94–129. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200107)
 - Anders Gorst-Rasmussen, 2012, tt: Treelet Transform with Stata, Stata Journal, 12(1): 130–146. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200108)
 - Nicholas J. Cox, 2012, Speaking Stata: Output to Order, Stata Journal, 12(1): 147–158. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200109)
 - Steven J. Samuels,  Nicholas J. Cox, 2012, Stata Tip 105: Daily Dates with Missing Days, Stata Journal, 12(1): 159–161. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200110)
 - Maarten L. Buis, 2012, Stata Tip 106: With or without Reference, Stata Journal, 12(1): 162–164. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200111)
 - Maarten L. Buis, 2012, Stata Tip 107: The Baseline is Now Reported, Stata Journal, 12(1): 165–166. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200112)
 
 
### SJ 12-2
 - Rodolphe Desbordes,  Vincenzo Verardi, 2012, A Robust Instrumental-Variables Estimator, Stata Journal, 12(2): 169–181. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200201)
 - Ronán M. Conroy, 2012, What Hypotheses do “Nonparametric” Two-Group Tests Actually Test?, Stata Journal, 12(2): 182–190. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200202)
 - Roger B. Newson, 2012, From Resultssets to Resultstables in Stata, Stata Journal, 12(2): 191–213. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200203)
 - Qunyong Wang,  Na Wu, 2012, Menu-Driven X-12-ARIMA Seasonal Adjustment in Stata, Stata Journal, 12(2): 214–241. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200204)
 - Michael G. Farnworth, 2012, Faster Estimation of a Discrete-Time Proportional Hazards Model with Gamma Frailty, Stata Journal, 12(2): 242–256. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200205)
 - Tao Xiao,  G. A. Whitmore,  Xin He,  Mei-Ling Ting Lee, 2012, Threshold Regression for Time-to-Event Analysis: The Stthreg Package, Stata Journal, 12(2): 257–283. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200206)
 - Daniele Pacifico, 2012, Fitting Nonparametric Mixed Logit Models via Expectation-Maximization Algorithm, Stata Journal, 12(2): 284–298. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200207)
 - Vincenzo Verardi,  Alice McCathie, 2012, The S-Estimator of Multivariate Location and Scatter in Stata, Stata Journal, 12(2): 299–307. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200208)
 - Richard Williams, 2012, Using the Margins Command to Estimate and Interpret Adjusted Predictions and Marginal Effects, Stata Journal, 12(2): 308–331. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200209)
 - Nicholas J. Cox, 2012, Speaking Stata: Transforming the Time Axis, Stata Journal, 12(2): 332–341. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200210)
 - Maarten L. Buis, 2012, Stata Tip 108: On Adding and Constraining, Stata Journal, 12(2): 342–344. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200211)
 - Peter A. Lachenbruch, 2012, Stata Tip 109: How to Combine Variables with Missing Values, Stata Journal, 12(2): 345–346. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200212)
 - Anna Makles, 2012, Stata Tip 110: How to Get the Optimal K-Means Cluster Solution, Stata Journal, 12(2): 347–351. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200213)
 
 
### SJ 12-3
 - Wesley Eddings,  Yulia Marchenko, 2012, Diagnostics for Multiple Imputation in Stata, Stata Journal, 12(3): 353–367. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200301)
 - Michal Brzezinski, 2012, The Chen–Shapiro Test for Normality, Stata Journal, 12(3): 368–374. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200302)
 - Ulrich Kohler,  Janina Zeh, 2012, Apportionment Methods, Stata Journal, 12(3): 375–392. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200303)
 - Ingvild Almås,  Tarjei Havnes,  Magne Mogstad, 2012, Adjusting for Age Effects in Cross-Sectional Distributions, Stata Journal, 12(3): 393–405. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200304)
 - Daniel F. McCaffrey,  J. R. Lockwood,  Kata Mihaly,  Tim R. Sass, 2012, A Review of Stata Commands for Fixed-Effects Estimation in Normal Linear Models, Stata Journal, 12(3): 406–432. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200305)
 - Brian P. Poi, 2012, Easy Demand-System Estimation with Quaids, Stata Journal, 12(3): 433–446. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200306)
 - Morten W. Fagerland,  David W. Hosmer, 2012, A Generalized Hosmer–Lemeshow Goodness-of-Fit Test for Multinomial Logistic Regression Models, Stata Journal, 12(3): 447–453. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200307)
 - Mehmet F. Dicle,  Betul Dicle, 2012, Importing Presidential Approval Poll Results, Stata Journal, 12(3): 454–460. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200308)
 - Harald Tauchmann, 2012, Partial Frontier Efficiency Analysis, Stata Journal, 12(3): 461–478. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200309)
 - Roger B. Newson, 2012, Sensible Parameters for Univariate and Multivariate Splines, Stata Journal, 12(3): 479–504. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200310)
 - Morten W. Fagerland, 2012, Exact and Mid-p Confidence Intervals for the Odds Ratio, Stata Journal, 12(3): 505–514. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200311)
 - Qunyong Wang,  Na Wu, 2012, Long-Run Covariance and its Applications in Cointegration Regression, Stata Journal, 12(3): 515–542. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200312)
 - Philippe Van Kerm, 2012, Kernel-Smoothed Cumulative Distribution Function Estimation with Akdensity, Stata Journal, 12(3): 543–548. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200313)
 - Nicholas J. Cox, 2012, Speaking Stata: Axis Practice, or What Goes Where on a Graph, Stata Journal, 12(3): 549–561. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200314)
 - Alan C. Acock, 2012, Review of Interpreting and Visualizing Regression Models Using Stata by Michael N. Mitchell, Stata Journal, 12(3): 562–564. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200315)
 - Nicholas J. Cox, 2012, Stata Tip 111: More on Working with Weeks, Stata Journal, 12(3): 565–569. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200316)
 
 
### SJ 12-4
 - H. Joseph Newton,  Nicholas J. Cox, 2012, The Stata Journal Editors’ Prize 2012: David Roodman, Stata Journal, 12(4): 571–574. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200401)
 - Valentino Dardanoni,  Giuseppe De Luca,  Salvatore Modica,  Franco Peracchi, 2012, A Generalized Missing-Indicator Approach to Regression with Imputed Covariates, Stata Journal, 12(4): 575–604. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200402)
 - Michael J. Crowther,  Dean Langan,  Alex J. Sutton, 2012, Graphical Augmentations to the Funnel Plot to Assess the Impact of a New Study on an Existing Meta-Analysis, Stata Journal, 12(4): 605–622. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200403)
 - Therese M.-L. Andersson,  Paul C. Lambert, 2012, Fitting and Modeling Cure in Population-Based Cancer Studies within the Framework of Flexible Parametric Survival Models, Stata Journal, 12(4): 623–638. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200404)
 - Patrick Royston, 2012, Tools to Simulate Realistic Censored Survival-Time Distributions, Stata Journal, 12(4): 639–654. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200405)
 - John Luke Gallup, 2012, A Programmer's Command to Build Formatted Statistical Tables, Stata Journal, 12(4): 655–673. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200406)
 - Michael J. Crowther,  Paul C. Lambert, 2012, Simulating Complex Survival Data, Stata Journal, 12(4): 674–687. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200407)
 - Dario Consonni,  Enzo Coviello,  Carlotta Buzzoni,  Carolina Mensi, 2012, A Command to Calculate Age-Standardized Rates with Efficient Interval Estimation, Stata Journal, 12(4): 688–701. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200408)
 - Llorenç Quintó,  Sergi Sanz,  Elisa De Lazzari,  John J. Aponte, 2012, HTML Output in Stata, Stata Journal, 12(4): 702–717. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200409)
 - Brady T. West,  Sean Esteban McCabe, 2012, Incorporating Complex Sample Design Effects when Only Final Survey Weights are Available, Stata Journal, 12(4): 718–725. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200410)
 - Vincenzo Verardi,  Nicolas Debarsy, 2012, Robinson's Square Root of N Consistent Semiparametric Regression Estimator in Stata, Stata Journal, 12(4): 726–735. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200411)
 - Tammy Harris,  Zhao Yang,  James W. Hardin, 2012, Modeling Underdispersed Count Data with Generalized Poisson Regression, Stata Journal, 12(4): 736–747. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200412)
 - Nicholas J. Cox, 2012, Speaking Stata: Matrices as Look-Up Tables, Stata Journal, 12(4): 748–758. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200413)
 - Maarten L. Buis, 2012, Stata Tip 112: Where did My P-Values Go? (Part 2), Stata Journal, 12(4): 759–760. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200414)
 - Nicholas J. Cox, 2012, Stata Tip 113: Changing a Variable's Format: What it Does and Does Not Mean, Stata Journal, 12(4): 761–764. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200415)
 - Nicholas J. Cox, 2012, Stata Tip 111: More on Working with Weeks, Erratum, Stata Journal, 12(4): 765–765. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200416)
 
 
## 2013
### SJ 13-1
 - H. Joseph Newton,  Nicholas J. Cox, 2013, Announcement of the Stata Journal Editors’ Prize 2013, Stata Journal, 13(1): 1–2. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300101)
 - Theresa Wimberley,  Erik Parner,  Henrik Støvring, 2013, Stata as a Numerical Tool for Scientific Thought Experiments: A Tutorial with Worked Examples, Stata Journal, 13(1): 3–20. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300102)
 - Richard Hooper, 2013, Versatile Sample-Size Calculation using Simulation, Stata Journal, 13(1): 21–38. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300103)
 - Giovanni L. Lo Magno, 2013, Sar: Automatic Generation of Statistical Reports using Stata and Microsoft Word for Windows, Stata Journal, 13(1): 39–64. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300104)
 - Reinhard Schunck, 2013, Within and between Estimates in Random-Effects Models: Advantages and Drawbacks of Correlated Random Effects and Hybrid Models, Stata Journal, 13(1): 65–76. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300105)
 - Branko Miladinovic,  Iztok Hozo,  Benjamin Djulbegovic, 2013, Trial Sequential Boundaries for Cumulative Meta-Analyses, Stata Journal, 13(1): 77–91. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300106)
 - Valerio Filoso, 2013, Regression Anatomy, Revealed, Stata Journal, 13(1): 92–106. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300107)
 - Adam Okulicz-Kozaryn, 2013, kmlmap: A Stata Command for Producing Google's Keyhole Markup Language, Stata Journal, 13(1): 107–113. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300108)
 - Karla Hemming,  Jen Marsh, 2013, A Menu-Driven Facility for Sample-Size Calculations in Cluster Randomized Controlled Trials, Stata Journal, 13(1): 114–135. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300109)
 - Mehmet F. Dicle,  John Levendis, 2013, Estimating Geweke's (1982) Measure of Instantaneous Feedback, Stata Journal, 13(1): 136–140. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300110)
 - Edmond S.-W. Ng,  Richard Grieve,  James R. Carpenter, 2013, Two-Stage Nonparametric Bootstrap Sampling with Shrinkage Correction for Clustered Data, Stata Journal, 13(1): 141–164. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300111)
 - Michael J. Crowther,  Keith R. Abrams,  Paul C. Lambert, 2013, Joint Modeling of Longitudinal and Survival Data, Stata Journal, 13(1): 165–184. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300112)
 - Nicola Orsini,  Rino Bellocco,  Arvid Sjölander, 2013, Doubly Robust Estimation in Generalized Linear Models, Stata Journal, 13(1): 185–205. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300113)
 - L. Philip Schumm, 2013, Review of Data Analysis Using Stata, Third Edition, by Kohler and Kreuter, Stata Journal, 13(1): 206–211. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300114)
 - Nicola Orsini, 2013, Review of Flexible Parametric Survival Analysis Using Stata: Beyond the Cox Model by Patrick Royston and Paul C. Lambert, Stata Journal, 13(1): 212–216. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300115)
 - Nicholas J. Cox, 2013, Stata Tip 114: Expand Paired Dates to Pairs of Dates, Stata Journal, 13(1): 217–219. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300116)
 
 
### SJ 13-2
 - David M. Drukker,  Ingmar R. Prucha,  Rafal Raciborski, 2013, Maximum Likelihood and Generalized Spatial Two-Stage Least-Squares Estimators for a Spatial-Autoregressive Model with Spatial-Autoregressive Disturbances, Stata Journal, 13(2): 221–241. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300201)
 - David M. Drukker,  Hua Peng,  Ingmar R. Prucha,  Rafal Raciborski, 2013, Creating and Managing Spatial-Weighting Matrices with the Spmat Command, Stata Journal, 13(2): 242–286. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300202)
 - David M. Drukker,  Ingmar R. Prucha,  Rafal Raciborski, 2013, A Command for Estimating Spatial-Autoregressive Models with Spatial-Autoregressive Disturbances and Additional Endogenous Variables, Stata Journal, 13(2): 287–301. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300203)
 - Matteo Bottai,  Nicola Orsini, 2013, A Command for Laplace Regression, Stata Journal, 13(2): 302–314. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300204)
 - Betul Dicle,  John Levendis,  Mehmet F. Dicle, 2013, Importing U.S. Exchange Rate Data from the Federal Reserve and Standardizing Country Names across Datasets, Stata Journal, 13(2): 315–322. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300205)
 - Daniel E. Cook,  Kelli R. Ryckman,  Jeffrey C. Murray, 2013, Generating Manhattan Plots in Stata, Stata Journal, 13(2): 323–328. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300206)
 - François Libois,  Vincenzo Verardi, 2013, Semiparametric Fixed-Effects Estimator, Stata Journal, 13(2): 329–336. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300207)
 - Tammy Harris,  James W. Hardin, 2013, Exact Wilcoxon Signed-Rank and Wilcoxon Mann–Whitney Ranksum Tests, Stata Journal, 13(2): 337–343. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300208)
 - Sally R. Hinchliffe,  Paul C. Lambert, 2013, Extending the Flexible Parametric Survival Model for Competing Risks, Stata Journal, 13(2): 344–355. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300209)
 - Rino Bellocco,  Sara Algeri, 2013, Goodness-of-Fit Tests for Categorical Data, Stata Journal, 13(2): 356–365. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300210)
 - Suzanna I. Vidmar,  Tim J. Cole,  Huiqi Pan, 2013, Standardizing Anthropometric Measures in Children and Adolescents with Functions for Egen: Update, Stata Journal, 13(2): 366–378. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300211)
 - Roger B. Newson, 2013, Bonferroni and Holm Approximations for Šidák and Holland–Copenhaver q-Values, Stata Journal, 13(2): 379–381. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300212)
 - Yuanyuan Gu,  Arne Risa Hole,  Stephanie Knox, 2013, Fitting the Generalized Multinomial Logit Model in Stata, Stata Journal, 13(2): 382–397. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300213)
 - Nicholas J. Cox, 2013, Speaking Stata: Creating and Varying Box Plots: Correction, Stata Journal, 13(2): 398–400. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300214)
 - Michael Herrmann, 2013, Stata Tip 115: How to properly Estimate the Multinomial Probit Model with Heteroskedastic Errors, Stata Journal, 13(2): 401–405. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300215)
 
 
### SJ 13-3
 - Matias D. Cattaneo,  David M. Drukker,  Ashley D. Holland, 2013, Estimation of Multivalued Treatment Effects under Conditional Independence, Stata Journal, 13(3): 407–450. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300301)
 - Michael J. Crowther,  Sally R. Hinchliffe,  Alison Donald,  Alex J. Sutton, 2013, Simulation-Based Sample-Size Calculation for Designing New Clinical Trials and Diagnostic Test Accuracy Studies to Update an Existing Meta-Analysis, Stata Journal, 13(3): 451–473. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300302)
 - Juan Manuel Ramos-Gonñi,  Oliver Rivero-Arias,  Helen Dakin, 2013, Response Mapping to Translate Health Outcomes into the Generic Health-Related Quality-of-Life Instrument EQ-5D: Introducing the mrs2eq and oks2eq Commands, Stata Journal, 13(3): 474–491. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300303)
 - Edward C. Norton,  Morgen M. Miller,  Lawrence C. Kleinman, 2013, Computing Adjusted Risk Ratios and Risk Differences in Stata, Stata Journal, 13(3): 492–509. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300304)
 - Patrick Royston, 2013, Marginscontplot: Plotting the Marginal Effects of Continuous Predictors, Stata Journal, 13(3): 510–527. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300305)
 - Andrés Riquelme,  Daniel Berkowitz,  Mehmet Caner, 2013, Valid Tests when Instrumental Variables do not Perfectly Satisfy the Exclusion Restriction, Stata Journal, 13(3): 528–546. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300306)
 - Takuya Hasebe, 2013, Copula-Based Maximum-Likelihood Estimation of Sample-Selection Models, Stata Journal, 13(3): 547–573. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300307)
 - Evangelos Kontopantelis,  David Reeves, 2013, A Short Guide and a Forest Plot Command (Ipdforest) for One-Stage Meta-Analysis, Stata Journal, 13(3): 574–587. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300308)
 - Jason R. Blevins,  Shakeeb Khan, 2013, Distribution-Free Estimation of Heteroskedastic Binary Response Models in Stata, Stata Journal, 13(3): 588–602. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300309)
 - Mehmet F. Dicle, 2013, Financial Portfolio Selection using the Multifactor Capital Asset Pricing Model and Imported Options Data, Stata Journal, 13(3): 603–617. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300310)
 - Marc Sangnier, 2013, Allocation of Ordered Exclusive Choices, Stata Journal, 13(3): 618–624. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300311)
 - Daniele Pacifico,  Hong il Yoo, 2013, Lclogit: A Stata Command for Fitting Latent-Class Conditional Logit Models via the Expectation-Maximization Algorithm, Stata Journal, 13(3): 625–639. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300312)
 - Nicholas J. Cox, 2013, Speaking Stata: Trimming to Taste, Stata Journal, 13(3): 640–666. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300313)
 
 
### SJ 13-4
 - H. Joseph Newton,  Nicholas J. Cox, 2013, The Stata Journal Editors’ Prize 2013: Erik Thorlund Parner and per Kragh Andersen, Stata Journal, 13(4): 669–671. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300401)
 - Roger B. Newson, 2013, Attributable and Unattributable Risks and Fractions and other Scenario Comparisons, Stata Journal, 13(4): 672–698. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300402)
 - P. Wilner Jeanty, 2013, Dealing with Identifier Variables in Data Management and Analysis, Stata Journal, 13(4): 699–718. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300403)
 - Federico Belotti,  Silvio Daidone,  Giuseppe Ilardi,  Vincenzo Atella, 2013, Stochastic Frontier Analysis using Stata, Stata Journal, 13(4): 719–758. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300404)
 - Sally R. Hinchliffe,  David A. Scott,  Paul C. Lambert, 2013, Flexible Parametric Illness-Death Models, Stata Journal, 13(4): 759–775. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300405)
 - Bruno García, 2013, Implementation of a Double-Hurdle Model, Stata Journal, 13(4): 776–794. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300406)
 - Cheng Li, 2013, Little's Test of Missing Completely at Random, Stata Journal, 13(4): 795–809. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300407)
 - Bruce A. Desmarais,  Jeffrey J. Harden, 2013, Testing for Zero Inflation in Count Models: Bias Correction for the Vuong Test, Stata Journal, 13(4): 810–835. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300408)
 - Zachary L. Flynn,  Leandro M. Magnusson, 2013, Parametric Inference using Structural Break Tests, Stata Journal, 13(4): 836–861. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300409)
 - Patrick Royston, 2013, Cmpute: A Tool to Generate or Replace a Variable, Stata Journal, 13(4): 862–866. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300410)
 - Christian H. Salas Pauliac, 2013, Group2: Generating the Finest Partition that is Coarser than Two Given Partitions, Stata Journal, 13(4): 867–875. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300411)
 - Paulo Guimarães, 2013, A Score Test for Group Comparisons in Single-Index Models, Stata Journal, 13(4): 876–883. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1301300412)
 
 
## 2014
### SJ 14-1
 - H. Joseph Newton,  Nicholas J. Cox, 2014, Announcement of the Stata Journal Editors’ Prize 2014, Stata Journal, 14(1): 1–2. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400101)
 - H. Joseph Newton,  Nicholas J. Cox, 2014, Editorial Announcement, Stata Journal, 14(1): 3–3. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400102)
 - Daniele Pacifico, 2014, Sreweight: A Stata Command to Reweight Survey Data to External Totals, Stata Journal, 14(1): 4–21. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400103)
 - Stanislav Kolenikov, 2014, Calibrating Survey Data using Iterative Proportional Fitting (Raking), Stata Journal, 14(1): 22–59. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400104)
 - Shawn Bauldry, 2014, Miivfind: A Command for Identifying Model-Implied Instrumental Variables for Structural Equation Models in Stata, Stata Journal, 14(1): 60–75. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400105)
 - Branko Miladinovic,  Anna Chaimani,  Iztok Hozo,  Benjamin Djulbegovic, 2014, Indirect Treatment Comparison, Stata Journal, 14(1): 76–86. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400106)
 - Isabelle Clerc-Urmès,  Michel Grzebyk,  Guy Hédelin, 2014, Net Survival Estimation with Stns, Stata Journal, 14(1): 87–102. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400107)
 - Rafael J. A. Cámara, 2014, Reports and other PDF Documents, Stata Journal, 14(1): 103–118. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400108)
 - Tom M. Palmer,  Corrie M. Macdonald-Wallis,  Debbie A. Lawlor,  Kate Tilling, 2014, Estimating Adjusted Associations between Random Effects from Multilevel Models: The Reffadjust Package, Stata Journal, 14(1): 119–140. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400109)
 - Barbara Guardabascio,  Marco Ventura, 2014, Estimating the Dose–Response Function through a Generalized Linear Model Approach, Stata Journal, 14(1): 141–158. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400110)
 - Evridiki Batistatou,  Chris Roberts,  Steve Roberts, 2014, Sample Size and Power Calculations for Trials and Quasi-Experimental Studies with Clustering, Stata Journal, 14(1): 159–175. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400111)
 - Michael Hills,  Bernard Rachet,  Milena Falcaro, 2014, Strel2: A Command for Estimating Excess Hazard and Relative Survival in Large Population-Based Studies, Stata Journal, 14(1): 176–190. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400112)
 - Scott Brave,  Thomas Walstrum, 2014, Estimating Marginal Treatment Effects using Parametric and Semiparametric Methods, Stata Journal, 14(1): 191–217. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400113)
 - Maarten L. Buis, 2014, Stata Tip 116: Where did My P-Values Go? (Part 3), Stata Journal, 14(1): 218–220. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400114)
 - Lars Ängquist, 2014, Stata Tip 117: Graph Combine—Combining Graphs, Stata Journal, 14(1): 221–225. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400115)
 - Carsten Sauer, 2014, Stata Tip 118: Orthogonalizing Powered and Product Terms using Residual Centering, Stata Journal, 14(1): 226–229. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400116)
 - Nicholas J. Cox, 2014, Stata Tip 119: Expanding Datasets for Graphical Ends, Stata Journal, 14(1): 230–235. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400117)
 
### SJ 14-2
 - Joerg Luedicke,  Alberto Bernacchia, 2014, Self-Consistent Density Estimation, Stata Journal, 14(2): 237–258. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400201)
 - Alexander Plum, 2014, Simulated Multivariate Random-Effects Probit Models for Unbalanced Panels, Stata Journal, 14(2): 259–279. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400202)
 - James W. Hardin,  Joseph M. Hilbe, 2014, Regression Models for Count Data Based on the Negative Binomial(p) Distribution, Stata Journal, 14(2): 280–291. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400203)
 - James W. Hardin,  Joseph M. Hilbe, 2014, Estimation and Testing of Binomial and Beta-Binomial Regression Models with and without Zero Inflation, Stata Journal, 14(2): 292–303. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400204)
 - Albert Vexler,  Hovig Tanajian,  Alan D. Hutson, 2014, Density-Based Empirical Likelihood Procedures for Testing Symmetry of Data Distributions and K-Sample Comparisons, Stata Journal, 14(2): 304–328. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400205)
 - Patrick Royston, 2014, A Smooth Covariate Rank Transformation for Use in Regression Models with a Sigmoid Dose–Response Function, Stata Journal, 14(2): 329–341. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400206)
 - Sara Ayllón, 2014, From Stata to aML, Stata Journal, 14(2): 342–362. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400207)
 - Karla Hemming,  Alan Girling, 2014, A Menu-Driven Facility for Power and Detectable-Difference Calculations in Stepped-Wedge Cluster-Randomized Trials, Stata Journal, 14(2): 363–380. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400208)
 - Xuan Zhang,  Chuntao Li, 2014, Importing Chinese Historical Stock Market Quotations from NetEase, Stata Journal, 14(2): 381–388. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400209)
 - Christopher L. Aberson, 2014, Power Analyses for Detecting Effects for Multiple Coefficients in Regression, Stata Journal, 14(2): 389–397. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400210)
 - Muhammad Rashid Ansari,  Chiara Mussida,  Francesco Pastore, 2014, Note on Lilien and Modified Lilien Index, Stata Journal, 14(2): 398–406. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400211)
 - Bryan M. Fellman, 2014, Stata Command for Calculating Adverse Event and Efficacy Stopping Boundaries for Phase II Single-Arm Trials, Stata Journal, 14(2): 407–417. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400212)
 - Catherine Welch,  Jonathan Bartlett,  Irene Petersen, 2014, Application of Multiple Imputation using the Two-Fold Fully Conditional Specification Algorithm in Longitudinal Clinical Data, Stata Journal, 14(2): 418–431. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400213)
 - Nicholas J. Cox, 2014, Speaking Stata: Self and Others, Stata Journal, 14(2): 432–444. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400214)
 - Dimitriy V. Masterov, 2014, Review of Introduction to Time Series Using Stata by Sean Becketti, Stata Journal, 14(2): 445–448. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400215)
 - Maarten L. Buis, 2014, Stata Tip 120: Certifying Subroutines, Stata Journal, 14(2): 449–450. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400216)
 
 
### SJ 14-3
 - Giovanni Cerulli, 2014, Ivtreatreg: A Command for Fitting Binary Treatment Models with Heterogeneous Response to Treatment and Unobservable Selection, Stata Journal, 14(3): 453–480. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400301)
 - Valerie K. Bostwick,  Douglas G. Steigerwald, 2014, Obtaining Critical Values for Test of Markov Regime Switching, Stata Journal, 14(3): 481–498. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400302)
 - Bryan M. Fellman,  Joe Ensor, 2014, A Command for Significance and Power to Test for the Existence of a Unique Most Probable Category, Stata Journal, 14(3): 499–510. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400303)
 - Jonas Björnerstedt,  Frank Verboven, 2014, Merger Simulation with Nested Logit Demand, Stata Journal, 14(3): 511–540. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400304)
 - Giovanni Cerulli, 2014, Treatrew: A User-Written Command for Estimating Average Treatment Effects by Reweighting on the Propensity Score, Stata Journal, 14(3): 541–561. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400305)
 - Tammy Harris,  Joseph M. Hilbe,  James W. Hardin, 2014, Modeling Count Data with Generalized Distributions, Stata Journal, 14(3): 562–579. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400306)
 - Michela Bia,  Carlos A. Flores,  Alfonso Flores-Lagunes,  Alessandra Mattei, 2014, A Stata Package for the Application of Semiparametric Estimators of Dose–Response Functions, Stata Journal, 14(3): 580–604. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400307)
 - Michela Bia,  Philippe Van Kerm, 2014, Space-Filling Location Selection, Stata Journal, 14(3): 605–622. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400308)
 - Matthew J. Baker, 2014, Adaptive Markov Chain Monte Carlo Sampling and Estimation in Mata, Stata Journal, 14(3): 623–661. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400309)
 - Alberto A. Gaggero, 2014, Csvconvert: A Simple Command to Gather Comma-Separated Value Files into Stata, Stata Journal, 14(3): 662–669. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400310)
 - Ian McCarthy,  Daniel Millimet,  Rusty Tchernis, 2014, The Bmte Command: Methods for the Estimation of Treatment Effects when Exclusion Restrictions are Unavailable, Stata Journal, 14(3): 670–683. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400311)
 - Timothy Neal, 2014, Panel Cointegration Analysis with Xtpedroni, Stata Journal, 14(3): 684–692. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400312)
 - Raymond Hicks, 2014, Stata and Dropbox, Stata Journal, 14(3): 693–696. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400313)
 - Ariel Linden, 2014, Review of An Introduction to Stata for Health Researchers, Fourth Edition, by Juul and Frydenberg, Stata Journal, 14(3): 697–700. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400314)
 
 
### SJ 14-4
 - H. Joseph Newton,  Nicholas J. Cox, 2014, The Stata Journal Editors’ Prize 2014: Roger Newson, Stata Journal, 14(4): 703–707. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400401)
 - Ben Jann, 2014, Plotting Regression Coefficients and other Estimates, Stata Journal, 14(4): 708–737. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400402)
 - Patrick Royston, 2014, Tools for Checking Calibration of a Cox Model in External Validation: Approach Based on Individual Event Probabilities, Stata Journal, 14(4): 738–755. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400403)
 - Tamás Bartus,  David Roodman, 2014, Estimation of Multiprocess Survival Models with cmp, Stata Journal, 14(4): 756–777. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400404)
 - Christoph Engel,  Peter G. Moffatt, 2014, Dhreg, Xtdhreg, and Bootdhreg: Commands to Implement Double-Hurdle Regression, Stata Journal, 14(4): 778–797. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400405)
 - Miguel Manjón,  Oscar Martínez, 2014, The Chi-Squared Goodness-of-Fit Test for Count-Data Models, Stata Journal, 14(4): 798–816. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400406)
 - Unislawa Williams,  Sean P. Williams, 2014, Txttool: Utilities for Text Analysis in Stata, Stata Journal, 14(4): 817–829. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400407)
 - Florian Wendelspiess Chávez Juárez,  Isidro Soloaga, 2014, Iop: Estimating Ex-Ante Inequality of Opportunity, Stata Journal, 14(4): 830–846. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400408)
 - Klaus Pforr, 2014, Femlogit—Implementation of the Multinomial Logit Model with Fixed Effects, Stata Journal, 14(4): 847–862. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400409)
 - Kathryn M. Aloisio,  Nadia Micali,  Sonja A. Swanson,  Alison Field,  Nicholas J. Horton, 2014, Analysis of Partially Observed Clustered Data using Generalized Estimating Equations and Multiple Imputation, Stata Journal, 14(4): 863–883. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400410)
 - Harald Tauchmann, 2014, Lee (2009) Treatment-Effect Bounds for Nonrandom Sample Selection, Stata Journal, 14(4): 884–894. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400411)
 - Damian Clarke, 2014, General-to-Specific Modeling in Stata, Stata Journal, 14(4): 895–908. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400412)
 - Sebastian Calonico,  Matias D. Cattaneo,  Rocío Titiunik, 2014, Robust Data-Driven Inference in the Regression-Discontinuity Design, Stata Journal, 14(4): 909–946. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400413)
 - Morten W. Fagerland, 2014, Adjcatlogit, Ccrlogit, and Ucrlogit: Fitting Ordinal Logistic Regression Models, Stata Journal, 14(4): 947–964. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400414)
 - Joseph D. Wolfe,  Shawn Bauldry, 2014, Collecting and Organizing Stata Graphs, Stata Journal, 14(4): 965–974. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400415)
 - Nicholas J. Cox, 2014, Speaking Stata: Design Plots for Graphical Summary of a Response Given Factors, Stata Journal, 14(4): 975–990. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400416)
 - Nicholas J. Cox, 2014, Stata tip 121: Box plots side by side, Stata Journal, 14(4): 991–996. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1401400417)
 
 
## 2015
### SJ 15-1
 - H. Joseph Newton,  Nicholas J. Cox, 2015, Announcement of the Stata Journal Editors’ Prize 2015, Stata Journal, 15(1): 1–2. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500101)
 - Federico Belotti,  Partha Deb,  Willard G. Manning,  Edward C. Norton, 2015, Twopm: Two-Part Models, Stata Journal, 15(1): 3–20. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500102)
 - Victor Chernozhukov,  Wooyoung Kim,  Sokbae Lee,  Adam M. Rosen, 2015, Implementing Intersection Bounds in Stata, Stata Journal, 15(1): 21–44. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500103)
 - Giovanni L. Lo Magno, 2015, More Power through Symbolic Computation: Extending Stata by using the Maxima Computer algebra system, Stata Journal, 15(1): 45–76. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500104)
 - Wouter Gelade,  Vincenzo Verardi,  Catherine Vermandele, 2015, Time-Efficient Algorithms for Robust Estimators of Location, Scale, Symmetry, and Tail heaviness, Stata Journal, 15(1): 77–94. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500105)
 - Sunbok Lee, 2015, Generating Univariate and Multivariate Nonnormal Data, Stata Journal, 15(1): 95–109. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500106)
 - Bryan M. Fellman,  Ying Yuan, 2015, Bayesian Optimal Interval Design for Phase I Oncology Clinical Trials, Stata Journal, 15(1): 110–120. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500107)
 - Qunyong Wang, 2015, Fixed-Effect Panel Threshold Model using Stata, Stata Journal, 15(1): 121–134. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500108)
 - Ying Xu,  Yin Bun Cheung, 2015, Frailty Models and Frailty-Mixture Models for Recurrent Event Times, Stata Journal, 15(1): 135–154. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500109)
 - Hannes Kröger, 2015, Newspell: Easy Management of Complex Spell Data, Stata Journal, 15(1): 155–172. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500110)
 - Enzo Coviello,  Karri Seppä,  Paul W. Dickman,  Arun Pokhrel, 2015, Estimating Net Survival using a Life-Table Approach, Stata Journal, 15(1): 173–185. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500111)
 - Paul W. Dickman,  Enzo Coviello, 2015, Estimating and Modeling Relative Survival, Stata Journal, 15(1): 186–215. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500112)
 - Carolin E. Pflueger,  Su Wang, 2015, A Robust Test for Weak Instruments in Stata, Stata Journal, 15(1): 216–225. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500113)
 - James W. Hardin,  Joseph M. Hilbe, 2015, Regression Models for Count Data from Truncated Distributions, Stata Journal, 15(1): 226–246. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500114)
 - Chiara Criscuolo,  Peter N. Gal,  Carlo Menon, 2015, Dynemp: A Routine for Distributed Microdata Analysis of Business Dynamics, Stata Journal, 15(1): 247–274. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500115)
 - Patrick Royston, 2015, Tools for Checking Calibration of a Cox Model in External Validation: Prediction of Population-Averaged Survival Curves Based on Risk Groups, Stata Journal, 15(1): 275–291. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500116)
 - Alexis Dinno, 2015, Nonparametric Pairwise Multiple Comparisons in Independent Groups using Dunn's Test, Stata Journal, 15(1): 292–300. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500117)
 - Minxing Chen, 2015, Generating Nonnegatively Correlated Binary Random Variates, Stata Journal, 15(1): 301–308. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500118)
 - Richard Williams, 2015, Review of Alan Acock's Discovering Structural Equation Modeling Using Stata, Revised Edition, Stata Journal, 15(1): 309–315. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500119)
 - Ben Jann, 2015, Stata Tip 122: Variable Bar Widths in Two-Way Graphs, Stata Journal, 15(1): 316–318. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500120)
 
### SJ 15-2
 - Pablo Gluzmann,  Demian Panigo, 2015, Global Search Regression: A New Automatic Model-selection Technique for Cross-section, Time-series, and Panel-data Regressions, Stata Journal, 15(2): 325–349. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500201)
 - Daniel J. Bratton,  Babak Choodari-Oskooei,  Patrick Royston, 2015, A Menu-driven Facility for Sample-size Calculation in Multiarm, Multistage Randomized Controlled Trials with Time-to-event Outcomes: Update, Stata Journal, 15(2): 350–368. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500202)
 - David J. Fisher, 2015, Two-stage Individual Participant Data Meta-analysis and Generalized Forest Plots, Stata Journal, 15(2): 369–396. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500203)
 - Anirban Basu, 2015, Person-centered Treatment (PeT) Effects: Individualized Treatment Effects Using Instrumental Variables, Stata Journal, 15(2): 397–410. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500204)
 - Ian McCarthy,  Daniel L. Millimet,  Manan Roy, 2015, Bounding Treatment Effects: A Command for the Partial Identification of the Average Treatment Effect with Endogenous and Misreported Treatment Assignment, Stata Journal, 15(2): 411–436. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500205)
 - Jonathan W. Bartlett,  Tim P. Morris, 2015, Multiple Imputation of Covariates by Substantive-model Compatible Fully Conditional Specification, Stata Journal, 15(2): 437–456. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500206)
 - Tammy H. Cummings,  James W. Hardin,  Alexander C. McLain,  James R. Hussey,  Kevin J. Bennett,  Gina M. Wingood, 2015, Modeling Heaped Count Data, Stata Journal, 15(2): 457–479. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500207)
 - Ariel Linden, 2015, Conducting Interrupted Time-series Analysis for Single- and Multiple-group Comparisons, Stata Journal, 15(2): 480–500. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500208)
 - Jonathan Shaw, 2015, Top 10 Stata “Gotchas”, Stata Journal, 15(2): 501–511. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500209)
 - Paul Corral,  Mungo Terbish, 2015, Generalized Maximum Entropy Estimation of Discrete Choice Models, Stata Journal, 15(2): 512–522. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500210)
 - Tim S. L. Brophy,  Reza Che Daniels,  Sibongile Musundwa, 2015, Gpsbound: A Command for Importing and Verifying Geographical Information from a User-provided Shapefile, Stata Journal, 15(2): 523–536. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500211)
 - Alexander Zlotnik,  Víctor Abraira, 2015, A General-purpose Nomogram Generator for Predictive Logistic Regression Models, Stata Journal, 15(2): 537–546. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500212)
 - Marco Savegnago, 2015, Transition Matrix for a Bivariate Normal Distribution in Stata, Stata Journal, 15(2): 547–553. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500213)
 - Sébastien Lecocq,  Jean-Marc Robin, 2015, Estimating Almost-ideal Demand Systems with Endogenous Regressors, Stata Journal, 15(2): 554–573. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500214)
 - Nicholas J. Cox, 2015, Speaking Stata: Species of Origin, Stata Journal, 15(2): 574–587. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500215)
 - Tim Collier, 2015, Review of Alan Acock's a Gentle Introduction to Stata, Fourth Edition, Stata Journal, 15(2): 588–593. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500216)
 - Martin Rune Hansen, 2015, Graphlog: Creating Log Files with Embedded Graphics, Stata Journal, 15(2): 594–596. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500217)
 - Maarten L. Buis, 2015, Stata Tip 124: Passing Temporary Variables to Subprograms, Stata Journal, 15(2): 597–598. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500218)
 - Jessica Kasza, 2015, Stata Tip 125: Binned Residual Plots for Assessing the Fit of Regression Models for Binary Outcomes, Stata Journal, 15(2): 599–604. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500219)
 
 
### SJ 15-3
 - Mark D. Chatfield, 2015, Precombine: A Command to Examine n ≥ 2 Datasets before Combining, Stata Journal, 15(3): 607–626. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500301)
 - Christopher L. Skeels,  Larry W. Taylor, 2015, Prediction in Linear Index Models with Endogenous Regressors, Stata Journal, 15(3): 627–644. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500302)
 - Tom M. Palmer,  Jonathan A. C. Sterne, 2015, Fitting Fixed- and Random-effects Meta-analysis Models Using Structural Equation Modeling with the Sem and Gsem Commands, Stata Journal, 15(3): 645–671. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500303)
 - Nada Wasi,  Aaron Flaaen, 2015, Record Linkage Using Stata: Preprocessing, Linking, and Reviewing Utilities, Stata Journal, 15(3): 672–697. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500304)
 - Shuai Chen,  Jennifer Rolfes,  Hongwei Zhao, 2015, Estimation of Mean Health Care Costs and Incremental Cost-effectiveness Ratios with Possibly Censored Data, Stata Journal, 15(3): 698–711. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500305)
 - Andrea Discacciati,  Nicola Orsini,  Sander Greenland, 2015, Approximate Bayesian Logistic Regression via Penalized Likelihood by Data Augmentation, Stata Journal, 15(3): 712–736. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500306)
 - Mónica Hernández Alava,  Allan Wailoo, 2015, Fitting Adjusted Limited Dependent Variable Mixture Models to EQ-5D, Stata Journal, 15(3): 737–750. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500307)
 - Ben Jann, 2015, A Note on Adding Objects to an Existing Twoway Graph, Stata Journal, 15(3): 751–755. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500308)
 - Christian A. Gregory, 2015, Estimating Treatment Effects for Ordered Outcomes Using Maximum Simulated Likelihood, Stata Journal, 15(3): 756–774. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500309)
 - Lauren J. Scott,  Chris A. Rogers, 2015, Creating Summary Tables Using the Sumtable Command, Stata Journal, 15(3): 775–783. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500310)
 - Ignacio López-de-Ullibarri, 2015, Bandwidth Selection in Kernel Distribution Function Estimation, Stata Journal, 15(3): 784–795. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500311)
 - Ricardo Mora,  Iliana Reggio, 2015, Didq: A Command for Treatment-effect Estimation under Alternative Assumptions, Stata Journal, 15(3): 796–808. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500312)
 - Morten Overgaard,  Per K. Andersen,  Erik T. Parner, 2015, Regression Analysis of Censored Data Using Pseudo-observations: An Update, Stata Journal, 15(3): 809–821. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500313)
 - Javier Alejo,  Antonio Galvao,  Gabriel Montes-Rojas,  Walter Sosa-Escudero, 2015, Tests for Normality in Linear Panel-data Models, Stata Journal, 15(3): 822–832. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500314)
 - W. Scott Comulada, 2015, Model Specification and Bootstrapping for Multiply Imputed Data: An Application to Count Models for the Frequency of Alcohol Use, Stata Journal, 15(3): 833–844. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500315)
 - John Voorheis, 2015, Mqtime: A Stata Tool for Calculating Travel Time and Distance Using Mapquest web Services, Stata Journal, 15(3): 845–853. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500316)
 - David W. Vincent, 2015, The Berry–Levinsohn–Pakes Estimator of the Random-coefficients Logit Demand Model, Stata Journal, 15(3): 854–880. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500317)
 - Fernando Rios-Avila, 2015, Feasible Fitting of Linear Models with N Fixed Effects, Stata Journal, 15(3): 881–898. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500318)
 
 
### SJ 15-4
 - H. Joseph Newton,  Nicholas J. Cox, 2015, The Stata Journal Editors’ Prize 2015: Richard Williams, Stata Journal, 15(4): 901–904. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500401)
 - Anna Chaimani,  Georgia Salanti, 2015, Visualizing Assumptions and Results in Network Meta-analysis: The Network Graphs Package, Stata Journal, 15(4): 905–950. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500402)
 - Ian R. White, 2015, Network Meta-analysis, Stata Journal, 15(4): 951–985. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500403)
 - Ignace De Vos,  Gerdie Everaert,  Ilse Ruyssen, 2015, Bootstrap-based Bias Correction and Inference for Dynamic Panels with Fixed Effects, Stata Journal, 15(4): 986–1018. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500404)
 - Giovanni Cerulli, 2015, ctreatreg: Command for Fitting Dose–response Models under Exogenous and Endogenous Treatment, Stata Journal, 15(4): 1019–1045. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500405)
 - Charles Lindsey,  Simon Sheather, 2015, Best Subsets Variable Selection in Nonnormal Regression Models, Stata Journal, 15(4): 1046–1059. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500406)
 - Caroline Bascoul-Mollevi,  Florence Castan,  David Azria,  Sophie Gourgou-Bourgade, 2015, EORTC QLQ-C30 Descriptive Analysis with the qlqc30 Command, Stata Journal, 15(4): 1060–1074. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500407)
 - Maria Elena Bontempi,  Irene Mammi, 2015, Implementing a Strategy to Reduce the Instrument Count in Panel GMM, Stata Journal, 15(4): 1075–1097. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500408)
 - Patrick Royston, 2015, Estimating the Treatment Effect in a Clinical Trial Using Difference in Restricted Mean Survival Time, Stata Journal, 15(4): 1098–1117. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500409)
 - Modesto Escobar, 2015, Studying Coincidences with Network Analysis and Other Multivariate Tools, Stata Journal, 15(4): 1118–1156. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500410)
 - Hüseyin Tastan, 2015, Testing for Spectral Granger Causality, Stata Journal, 15(4): 1157–1166. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500411)
 - Seth T. Lirette, 2015, Complete Automation of a Participant Characteristics Table, Stata Journal, 15(4): 1167–1173. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500412)
 - Nicholas J. Cox, 2015, Speaking Stata: A Set of Utilities for Managing Missing Values, Stata Journal, 15(4): 1174–1185. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500413)
 
 
 ### SJ 16-4
 - H. Joseph Newton,  Nicholas J. Cox, 2016, The Stata Journal Editors’ Prize 2016: Patrick Royston, Stata Journal, 16(4): 815–825. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1601600401)
 - David M. Drukker, 2016, A Generalized Regression-adjustment Estimator for Average Treatment Effects from Panel Data, Stata Journal, 16(4): 826–836. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1601600402)
 - Ben Jann, 2016, Estimating Lorenz and Concentration Curves, Stata Journal, 16(4): 837–866. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1601600403)
 - Constantin Ruhe, 2016, Estimating Survival Functions after Stcox with Time-varying Coefficients, Stata Journal, 16(4): 867–879. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1601600404)
 - Odile Sauzet,  Maren Kleine, 2016, Distributional Estimates for the Comparison of Proportions of a Dichotomized Continuous Outcome, Stata Journal, 16(4): 880–899. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1601600405)
 - Miguel Manjón,  Juan Mañez, 2016, Production Function Estimation in Stata Using the Ackerberg–Caves–Frazer Method, Stata Journal, 16(4): 900–916. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1601600406)
 - Nick Guenther,  Matthias Schonlau, 2016, Support Vector Machines, Stata Journal, 16(4): 917–937. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1601600407)
 - E. F. Haghish, 2016, Rethinking Literate Programming in Statistics, Stata Journal, 16(4): 938–963. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1601600408)
 - E. F. Haghish, 2016, Markdoc: Literate Programming in Stata, Stata Journal, 16(4): 964–988. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1601600409)
 - Hannah Bower,  Michael J. Crowther,  Paul C. Lambert, 2016, Strcs: A Command for Fitting Flexible Parametric Survival Models on the Log-hazard Scale, Stata Journal, 16(4): 989–1012. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1601600410)
 - Sebastian Kripfganz, 2016, Quasi–maximum Likelihood Estimation of Linear Dynamic Short-T panel-data Models, Stata Journal, 16(4): 1013–1038. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1601600411)
 - Javier Alejo,  Anil Bera,  Gabriel Montes-Rojas,  Antonio Galvao,  Zhijie Xiao, 2016, Tests for Normality Based on the Quantile-mean Covariance, Stata Journal, 16(4): 1039–1057. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1601600412)
 - Nicholas J. Cox, 2016, Speaking Stata: Letter Values as Selected Quantiles, Stata Journal, 16(4): 1058–1071. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1601600413)
  - ——, 2016, Software Updates, Stata Journal, 16(4): 1072–1073. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1601600414)
 

 ## 2017

 ### SJ 17-1
 - H. Joseph Newton,  Nicholas J. Cox, 2017, Announcement of the Stata Journal Editors’ Prize 2017, Stata Journal, 17(1): 1–2. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700101)
 - Ben Jann, 2017, Creating HTML or Markdown Documents from within Stata using Webdoc, Stata Journal, 17(1): 3–38. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700102)
 - Mustafa U. Karakaplan, 2017, Fitting Endogenous Stochastic Frontier Models in Stata, Stata Journal, 17(1): 39–55. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700103)
 - Donald W. K. Andrews,  Wooyoung Kim,  Xiaoxia Shi, 2017, Commands for Testing Conditional Moment Inequalities and Equalities, Stata Journal, 17(1): 56–72. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700104)
 - Ariel Linden, 2017, A Comprehensive set of Postestimation Measures to Enrich Interrupted Time-series Analysis, Stata Journal, 17(1): 73–88. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700105)
 - Reinhard Schunck,  Francisco Perales, 2017, Within- and Between-cluster Effects in Generalized Linear Mixed Models: A Discussion of Approaches and the Xthybrid command, Stata Journal, 17(1): 89–115. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700106)
 - Timothy Erickson,  Robert Parham,  Toni M. Whited, 2017, Fitting the Errors-in-variables Model Using High-order Cumulants and Moments, Stata Journal, 17(1): 116–129. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700107)
 - Seth T. Lirette,  Samantha R. Seals,  Chad Blackshear,  Warren May, 2017, Capturing a Stata Dataset and Releasing it into REDcap, Stata Journal, 17(1): 130–138. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700108)
 - Federico Belotti,  Gordon Hughes,  Andrea Piano Mortari, 2017, Spatial Panel-data Models Using Stata, Stata Journal, 17(1): 139–180. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700109)
 - Paul C. Lambert, 2017, The Estimation and Modeling of Cause-specific Cumulative Incidence Functions Using Time-dependent Weights, Stata Journal, 17(1): 181–207. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700110)
 - Patrick Taffé,  Mingkai Peng,  Vicki Stagg,  Tyler Williamson, 2017, Biasplot: A Package to Effective Plots to Assess Bias and Precision in Method Comparison Studies, Stata Journal, 17(1): 208–221. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700111)
 - Martina Menon,  Federico Perali,  Nicola Tommasi, 2017, Estimation of Unit Values in Household Expenditure Surveys without Quantity Information, Stata Journal, 17(1): 222–239. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700112)
 - Paul Corral,  Daniel Kuehn,  Ermengarde Jabir, 2017, Generalized Maximum Entropy Estimation of Linear Models, Stata Journal, 17(1): 240–249. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700113)
  - ——, 2017, Software Updates, Stata Journal, 17(1): 250–250. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700114)
 

 ### SJ 17-2
 - James Hardin, 2017, Joseph M. Hilbe (1944–2017), Stata Journal, 17(2): 251–252. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700201)
 - Eric J. Daza,  Michael G. Hudgens,  Amy H. Herring, 2017, Estimating Inverse-probability Weights for Longitudinal Data with Dropout or Truncation: The Xtrccipw Command, Stata Journal, 17(2): 253–278. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700202)
 - Demetris Christodoulou, 2017, Heuristic Criteria for Selecting an Optimal Aspect Ratio in a Two-variable Line Plot, Stata Journal, 17(2): 279–313. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700203)
 - Demetris Christodoulou,  Vasilis Sarafidis, 2017, Regression Clustering for Panel-data Models with Fixed Effects, Stata Journal, 17(2): 314–329. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700204)
 - Robert L. Grant,  Bob Carpenter,  Daniel C. Furr,  Andrew Gelman, 2017, Introducing the StataStan Interface for Fast, Complex Bayesian Modeling Using Stan, Stata Journal, 17(2): 330–342. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700205)
 - Robert L. Grant,  Daniel C. Furr,  Bob Carpenter,  Andrew Gelman, 2017, Fitting Bayesian item response models in Stata and Stan, Stata Journal, 17(2): 343–357. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700206)
 - Andrea Discacciati,  Matteo Bottai, 2017, Instantaneous Geometric Rates via Generalized Linear Models, Stata Journal, 17(2): 358–371. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700207)
 - Sebastian Calonico,  Matias D. Cattaneo,  Max H. Farrell,  Rocío Titiunik, 2017, Rdrobust: Software for Regression-discontinuity Designs, Stata Journal, 17(2): 372–404. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700208)
 - Patrick Royston, 2017, A Combined Test for a Generalized Treatment Effect in Clinical Trials with a Time-to-event Outcome, Stata Journal, 17(2): 405–421. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700209)
 - Giovanni Cerulli, 2017, Estimating Responsiveness Scores Using Rscore, Stata Journal, 17(2): 422–441. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700210)
 - Tamás Bartus, 2017, Multilevel Multiprocess Modeling with Gsem, Stata Journal, 17(2): 442–461. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700211)
 - Sarwar Islam Mozumder,  Mark J. Rutherford,  Paul C. Lambert, 2017, A Flexible Parametric Competing-risks Model Using a Direct Likelihood Approach for the Cause-specific Cumulative Incidence Function, Stata Journal, 17(2): 462–489. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700212)
 - Jinjing Li, 2017, Rate Decomposition for Aggregate Data Using Das Gupta's Method, Stata Journal, 17(2): 490–502. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700213)
 - Eva Lorenz,  Sabine Gabrysch, 2017, Covariate-constrained Randomization Routine for Achieving Baseline Balance in Cluster-randomized Trials, Stata Journal, 17(2): 503–510. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700214)
 - Roger B. Newson, 2017, Stata Tip 127: Use Capture Noisily Groups, Stata Journal, 17(2): 511–514. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700215)
  - ——, 2017, Software Updates, Stata Journal, 17(2): 515–516. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700216)
 

 ### SJ 17-3
 - Mario Cruz-Gonzalez,  Iván Fernández-Val,  Martin Weidner, 2017, Bias Corrections for Probit and Logit Models with Two-way Fixed Effects, Stata Journal, 17(3): 517–545. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700301)
 - Brendan Halpin, 2017, SADI: Sequence Analysis Tools for Stata, Stata Journal, 17(3): 546–572. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700302)
 - Rachael A. Hughes,  Michael G. Kenward,  Jonathan A. C. Sterne,  Kate Tilling, 2017, Analyzing Repeated Measurements while Accounting for Derivative Tracking, Varying Within-subject Variance, and Autocorrelation: The Xtmixediou Command, Stata Journal, 17(3): 573–599. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700303)
 - Germán Rodríguez, 2017, Literate Data Analysis with Stata and Markdown, Stata Journal, 17(3): 600–618. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700304)
 - Patrick Royston, 2017, Model Selection for Univariable Fractional Polynomials, Stata Journal, 17(3): 619–629. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700305)
 - Simon Heß, 2017, Randomization Inference with Stata: A Guide and Software, Stata Journal, 17(3): 630–651. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700306)
 - Alvaro Carril, 2017, Dealing with Misfits in Random Treatment Assignment, Stata Journal, 17(3): 652–667. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700307)
 - Morten W. Fagerland,  David W. Hosmer, 2017, How to Test for Goodness of Fit in Ordinal Logistic Regression Models, Stata Journal, 17(3): 668–686. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700308)
 - Daniele Pacifico,  Felix Poege, 2017, Estimating Measures of Multidimensional Poverty with Stata, Stata Journal, 17(3): 687–703. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700309)
 - Jesús Otero,  Jeremy Smith, 2017, Response Surface Models for OLS and GLS Detrending-based Unit-root Tests in Nonlinear ESTAR Models, Stata Journal, 17(3): 704–722. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700310)
 - Charles F. Manski,  Max Tabord-Meehan, 2017, Evaluating the Maximum MSE of Mean Estimators with Missing Data, Stata Journal, 17(3): 723–735. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700311)
 - Mehmet F. Dicle,  John D. Levendis, 2017, Technical Financial Analysis Tools for Stata, Stata Journal, 17(3): 736–747. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700312)
 - Daniel Bischof, 2017, New Graphic Schemes for Stata: Plotplain and Plottig, Stata Journal, 17(3): 748–759. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700313)
 - Nicholas J. Cox, 2017, Speaking Stata: Tables as Lists: The Groups Command, Stata Journal, 17(3): 760–773. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700314)
 - Luca J. Uberti, 2017, Stata Tip 128: Marginal Effects in Log-transformed Models: A Trade Application, Stata Journal, 17(3): 774–778. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700315)
  - ——, 2017, Software Updates, Stata Journal, 17(3): 779–779. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1701700316)
 

 ### SJ 17-4
 - H. Joseph Newton,  Nicholas J. Cox, 2017, The Stata Journal Editors’ Prize 2017: Ben Jann, Stata Journal, 17(4): 781–785. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801700401)
 - Yinghui Wei,  Patrick Royston, 2017, Reconstructing Time-to-event Data from Published Kaplan–Meier Curves, Stata Journal, 17(4): 786–802. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801700402)
 - Giovanni Cerulli, 2017, Identification and Estimation of Treatment Effects in the Presence of (Correlated) Neighborhood Interactions: Model and Stata Implementation via Ntreatreg, Stata Journal, 17(4): 803–833. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801700403)
 - Sebastian Galiani,  Brian Quistorff, 2017, The Synth_Runner Package: Utilities to Automate Synthetic Control Estimation Using Synth, Stata Journal, 17(4): 834–849. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801700404)
 - Barbara Rossi,  Matthieu Soupre, 2017, Implementing Tests for Forecast Evaluation in the Presence of Instabilities, Stata Journal, 17(4): 850–865. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801700405)
 - Matthias Schonlau,  Nick Guenther,  Ilia Sucholutsky, 2017, Text Mining with n-gram Variables, Stata Journal, 17(4): 866–881. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801700406)
 - Kerui Du, 2017, Econometric Convergence Test and Club Clustering Using Stata, Stata Journal, 17(4): 882–900. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801700407)
 - Guangwei Zhu,  Zaichao Du,  Juan Carlos Escanciano, 2017, Automatic Portmanteau Tests with Applications to Market Risk Management, Stata Journal, 17(4): 901–915. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801700408)
 - Joseph V. Terza, 2017, Two-stage Residual Inclusion Estimation: A Practitioners Guide to Stata Implementation, Stata Journal, 17(4): 916–938. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801700409)
 - Joseph V. Terza, 2017, Causal Effect Estimation and Inference Using Stata, Stata Journal, 17(4): 939–961. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801700410)
 - Sylvain Weber,  Martin Péclat, 2017, A Simple Command to Calculate Travel Distance and Travel Time, Stata Journal, 17(4): 962–971. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801700411)
 - Luciano Lopez,  Sylvain Weber, 2017, Testing for Granger Causality in Panel Data, Stata Journal, 17(4): 972–984. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801700412)
 - Jesús Otero,  Christopher F. Baum, 2017, Response Surface Models for the Elliott, Rothenberg, and Stock Unit-root Test, Stata Journal, 17(4): 985–1002. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801700413)
 - Giovanni Nattino,  Stanley Lemeshow,  Gary Phillips,  Stefano Finazzi,  Guido Bertolini, 2017, Assessing the Calibration of Dichotomous Outcome Models with the Calibration Belt, Stata Journal, 17(4): 1003–1014. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801700414)
 - Mattia Cattaneo,  Paolo Malighetti,  Daniele Spinelli, 2017, Estimating Receiver Operative Characteristic Curves for Time-dependent Outcomes: The Stroccurve Package, Stata Journal, 17(4): 1015–1023. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801700415)
  - ——, 2017, Software Updates, Stata Journal, 17(4): 1024–1024. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801700416)
 

 ## 2018

 ### SJ 18-1
 - H. Joseph Newton,  Nicholas J. Cox, 2018, Announcement of the Stata Journal Editors’ Prize 2018, Stata Journal, 18(1): 1–2. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800101)
 - Patrick Royston, 2018, Power and sample-size analysis for the Royston–Parmar combined test in clinical trials with a time-to-event outcome, Stata Journal, 18(1): 3–21. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800102)
 - Jesús Otero,  Christopher F. Baum, 2018, Unit-root Tests Based on Forward and Reverse Dickey–Fuller Regressions, Stata Journal, 18(1): 22–28. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800103)
 - Bastien Perrot,  Emmanuelle Bataille,  Jean-Benoit Hardouin, 2018, validscale: A Command to Validate Measurement Scales, Stata Journal, 18(1): 29–50. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800104)
 - Laura A. Gray,  Mónica Hernández Alava, 2018, A Command for Fitting Mixture Regression Models for Bounded Dependent Variables Using the Beta Distribution, Stata Journal, 18(1): 51–75. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800105)
 - Jesse Wursten, 2018, Testing for Serial Correlation in Fixed-effects Panel Models, Stata Journal, 18(1): 76–100. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800106)
 - Carlo Schwarz, 2018, Ldagibbs: A Command for Topic Modeling in Stata Using Latent Dirichlet Allocation, Stata Journal, 18(1): 101–117. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800107)
 - Martin Eckhoff Andresen, 2018, Exploring Marginal Treatment Effects: Flexible Estimation Using Stata, Stata Journal, 18(1): 118–158. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800108)
 - Oscar Barriga Cabanillas,  Jeffrey D. Michler,  Aleksandr Michuda,  Emilia Tjernström, 2018, Fitting and Interpreting Correlated Random-coefficient Models Using Stata, Stata Journal, 18(1): 159–173. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800109)
 - Jonathan A. Cook,  Ashish Rajbhandari, 2018, Heckroccurve: ROC Curves for Selected Samples, Stata Journal, 18(1): 174–183. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800110)
 - Helmut Herwartz,  Simone Maxand,  Fabian H. C. Raters,  Yabibal M. Walle, 2018, Panel Unit-root Tests for Heteroskedastic Panels, Stata Journal, 18(1): 184–196. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800111)
 - Matthew S. Gillman, 2018, Some Commands to Help Produce Rich Text Files from Stata, Stata Journal, 18(1): 197–205. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800112)
 - Fernando Rios-Avila,  Gustavo Canavire-Bacarreza, 2018, Standard-error Correction in Two-stage Optimization Models: A Quasi–maximum Likelihood Estimation Approach, Stata Journal, 18(1): 206–222. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800113)
 - Daniel Gallacher,  Felix Achana, 2018, Assessing the Health Economic Agreement of Different Data Sources, Stata Journal, 18(1): 223–233. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800114)
 - Matias D. Cattaneo,  Michael Jansson,  Xinwei Ma, 2018, Manipulation Testing Based on Density Discontinuity, Stata Journal, 18(1): 234–261. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800115)
 - Nicholas J. Cox, 2018, Speaking Stata: Logarithmic Binning and Labeling, Stata Journal, 18(1): 262–286. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800116)
 - Alexander Koplenig, 2018, Stata Tip 129: Efficiently Processing Textual Data with Stata's New Unicode Features, Stata Journal, 18(1): 287–289. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800117)
  - ——, 2018, Software Updates, Stata Journal, 18(1): 290–291. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800118)
 

 ### SJ 18-2
 - Richard Williams,  Paul D. Allison,  Enrique Moral-Benito, 2018, Linear Dynamic Panel-data Estimation Using Maximum Likelihood and Structural Equation Modeling, Stata Journal, 18(2): 293–326. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800201)
 - Susan Donath, 2018, Baselinetable: A Command for Creating one- and Two-way Tables of Summary Statistics, Stata Journal, 18(2): 327–344. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800202)
 - Marshall A. Taylor, 2018, Simulating the Central Limit Theorem, Stata Journal, 18(2): 345–356. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800203)
 - John A. Gallis,  Fan Li,  Hengshi Yu,  Elizabeth L. Turner, 2018, Cvcrand and Cptest: Commands for Efficient Design and Analysis of Cluster Randomized Trials Using Constrained Randomization and Permutation Tests, Stata Journal, 18(2): 357–378. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800204)
 - Mehmet F. Dicle,  Betul Dicle, 2018, Content Analysis: Frequency Distribution of Words, Stata Journal, 18(2): 379–386. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800205)
 - Christiaan H. Righolt,  Salaheddin M. Mahmud, 2018, Attrition Diagrams for Clinical Trials and Meta-analyses in Stata, Stata Journal, 18(2): 387–394. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800206)
 - Mónica Hernández-Alava,  Stephen Pudney, 2018, Eq5Dmap: A Command for Mapping between EQ-5D-3L and EQ-5D-5L, Stata Journal, 18(2): 395–415. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800207)
 - Michael J. Grayling,  James M. S. Wason,  Adrian P. Mander, 2018, Group Sequential Clinical Trial Designs for Normally Distributed Outcome Variables, Stata Journal, 18(2): 416–431. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800208)
 - Noori Akhtar-Danesh, 2018, Qfactor: A Command for Q-methodology Analysis, Stata Journal, 18(2): 432–446. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800209)
 - Chang Hyung Lee,  Douglas G. Steigerwald, 2018, Inference for Clustered Data, Stata Journal, 18(2): 447–460. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800210)
 - Fausto Pacicco,  Luigi Vena,  Andrea Venegoni, 2018, Event Study Estimations Using Stata: The Estudy Command, Stata Journal, 18(2): 461–476. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800211)
 - Ying Xu,  Yin Bun Cheung, 2018, Frailty Models and Frailty-mixture Models for Recurrent Event Times: Update, Stata Journal, 18(2): 477–484. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800212)
 - Ariel Linden, 2018, Review of Tenko Raykov and George Marcoulides's a Course in Item Response Theory and Modeling with Stata, Stata Journal, 18(2): 485–488. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800213)
  - ——, 2018, Software Updates, Stata Journal, 18(2): 489–489. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800214)
 

 ### SJ 18-3
 - Ben Jann, 2018, Customizing Stata Graphs made Easy (Part 1), Stata Journal, 18(3): 491–502. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800301)
 - Mark D. Chatfield, 2018, Graphing Each Individual's Data over Time, Stata Journal, 18(3): 503–516. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800302)
 - Vincenzo Verardi,  Catherine Vermandele, 2018, Univariate and Multivariate Outlier Identification for Skewed or Heavy-Tailed Distributions, Stata Journal, 18(3): 517–532. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800303)
 - Shawna K. Metzger,  Benjamin T. Jones, 2018, Mstatecox: A Package for Simulating Transition Probabilities from Semiparametric Multistate Survival Models, Stata Journal, 18(3): 533–563. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800304)
 - Jordy Meekes,  Wolter H. J. Hassink, 2018, Flowbca: A Flow-Based Cluster Algorithm in Stata, Stata Journal, 18(3): 564–584. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800305)
 - Jan Ditzen, 2018, Estimating Dynamic Common-Correlated Effects in Stata, Stata Journal, 18(3): 585–617. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800306)
 - Gabriele Rovigatti,  Vincenzo Mollisi, 2018, Theory and Practice of Total-Factor Productivity Estimation: The Control Function Approach using Stata, Stata Journal, 18(3): 618–662. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800307)
 - Damian Clarke,  Benjamín Matta, 2018, Practical Considerations for Questionable IVs, Stata Journal, 18(3): 663–691. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800308)
 - Long Hong,  Guido Alfani,  Chiara Gigliarano,  Marco Bonetti, 2018, Giniinc: A Stata Package for Measuring Inequality from Incomplete Income and Survival Data, Stata Journal, 18(3): 692–715. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800309)
 - Anna Chaimani,  Dimitris Mavridis,  Georgia Salanti,  Julian P. T. Higgins,  Ian R. White, 2018, Allowing for Informative Missingness in Aggregate Data Meta-Analysis with Continuous or Binary Outcomes: Extensions to Metamiss, Stata Journal, 18(3): 716–740. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800310)
 - Nicholas J. Cox, 2018, Speaking Stata: From Rounding to Binning, Stata Journal, 18(3): 741–754. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800311)
 - Nicholas J. Cox, 2018, Stata Tip 130: 106610 and All That: Date Variables that Need to be Fixed, Stata Journal, 18(3): 755–757. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800312)
  - ——, 2018, Software Updates, Stata Journal, 18(3): 758–759. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800313)
 

 ### SJ 18-4
 - H. Joseph Newton,  Nicholas J. Cox, 2018, The Stata Journal Editors’ Prize 2018: Federico Belotti, Stata Journal, 18(4): 761–764. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800401)
 - Ben Jann, 2018, Color Palettes for Stata Graphics, Stata Journal, 18(4): 765–785. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800402)
 - Ben Jann, 2018, Customizing Stata Graphs Made Easy (Part 2), Stata Journal, 18(4): 786–802. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800403)
 - Liyang Sun, 2018, Implementing Valid Two-Step Identification-Robust Confidence Sets for Linear Instrumental-Variables Models, Stata Journal, 18(4): 803–825. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800404)
 - Michael J. Grayling,  Adrian P. Mander, 2018, Calculations Involving the Multivariate Normal and Multivariate t Distributions with and without Truncation, Stata Journal, 18(4): 826–843. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800405)
 - Raffaele Grotti,  Giorgio Cutuli, 2018, Xtpdyn: A Community-Contributed Command for Fitting Dynamic Random-Effects Probit Models with Unobserved Heterogeneity, Stata Journal, 18(4): 844–862. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800406)
 - Sébastien Fontenay, 2018, sdmxuse: Command to Import Data from Statistical Agencies using the SDMX Standard, Stata Journal, 18(4): 863–870. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800407)
 - Daniel Klein, 2018, Implementing a General Framework for Assessing Interrater Agreement in Stata, Stata Journal, 18(4): 871–901. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800408)
 - Soren Jordan,  Andrew Q. Philips, 2018, Cointegration Testing and Dynamic Simulations of Autoregressive Distributed Lag Models, Stata Journal, 18(4): 902–923. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800409)
 - Shawn Bauldry,  Jun Xu,  Andrew S. Fullerton, 2018, Gencrm: A New Command for Generalized Continuation-Ratio Models, Stata Journal, 18(4): 924–936. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800410)
 - Denis Chetverikov,  Dongwoo Kim,  Daniel Wilhelm, 2018, Nonparametric Instrumental-Variable Estimation, Stata Journal, 18(4): 937–950. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800411)
 - Xiaoqing Ye,  Yixiao Sun, 2018, Heteroskedasticity- and Autocorrelation-robust F and t Tests in Stata, Stata Journal, 18(4): 951–980. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800412)
 - Nicholas J. Cox,  Clyde B. Schechter, 2018, Speaking Stata: Seven Steps for Vexatious String Variables, Stata Journal, 18(4): 981–994. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800413)
 - Patrick Royston, 2018, Power and Sample-Size Analysis for the Royston–Parmar Combined Test in Clinical Trials with a Time-to-Event Outcome: Correction and Program Update, Stata Journal, 18(4): 995–996. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800414)
  - ——, 2018, Software Updates, Stata Journal, 18(4): 997–997. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1801800415)
 

 ## 2019

 ### SJ 19-1
 - H. Joseph Newton,  Nicholas J. Cox, 2019, Change of publisher: The Stata Journal is now published by SAGE Publishing, Stata Journal, 19(1): 1–1. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X19830874)
 - H. Joseph Newton,  Nicholas J. Cox, 2019, Announcement of the Stata Journal Editors’ Prize 2019, Stata Journal, 19(1): 2–3. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X19830876)
 - David Roodman,  Morten Ørregaard Nielsen,  James G. MacKinnon,  Matthew D. Webb, 2019, Fast and wild: Bootstrap inference in Stata using boottest, Stata Journal, 19(1): 4–60. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X19830877)
 - E. F. Haghish, 2019, Seamless interactive language interfacing between R and Stata, Stata Journal, 19(1): 61–82. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X19830891)
 - E. F. Haghish, 2019, On the importance of syntax coloring for teaching statistics, Stata Journal, 19(1): 83–86. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X19830892)
 - Alfonso Sánchez-Peñalver, 2019, Estimation methods in the presence of corner solutions, Stata Journal, 19(1): 87–111. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X19830893)
 - Maciej Jakubowski,  Artur Pokropek, 2019, piaactools: A program for data analysis with PIAAC data, Stata Journal, 19(1): 112–128. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X19830909)
 - Carlo Schwarz, 2019, lsemantica: A command for text similarity based on latent semantic analysis, Stata Journal, 19(1): 129–142. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X19830910)
 - Stanislav Kolenikov, 2019, Updates to the ipfraking ecosystem, Stata Journal, 19(1): 143–184. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X19830912)
 - Constantin Ruhe, 2019, Bootstrap pointwise confidence intervals for covariate-adjusted survivor functions in the Cox model, Stata Journal, 19(1): 185–199. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X19830915)
 - Mehmet F. Dicle, 2019, Candle charts for financial technical analysis, Stata Journal, 19(1): 200–209. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X19830918)
 - Matias D. Cattaneo,  Rocío Titiunik,  Gonzalo Vazquez-Bare, 2019, Power calculations for regression-discontinuity designs, Stata Journal, 19(1): 210–245. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X19830919)
 - Nicholas J. Cox,  Clyde B. Schechter, 2019, Speaking Stata: How best to generate indicator or dummy variables, Stata Journal, 19(1): 246–259. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X19830921)
  - ——, 2019, Software Updates, Stata Journal, 19(1): 260–260. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X19833285)
  - ——, 2019, Announcements, Stata Journal, 19(1): 261–261. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X19830922)
  - ——, 2019, Stata Conference advertisement: Journal announcement from StataCorp, Stata Journal, 19(1): NP1–NP1. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X19830933)
  - ——, 2019, The Stata Journal back issues: Journal announcement from StataCorp, Stata Journal, 19(1): NP2–NP2. [[pdf]](https://journals.sagepub.com/doi/pdf/10.1177/1536867X19830934)

&emsp;


>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表](https://www.jianshu.com/p/de82fdc2c18a) 
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

[![点击此处-查看完整推文列表](https://gitee.com/uploads/images/2019/0507/152341_fc66acd2_2272981.png "连享会(公众号: StataChina)推文列表")](https://gitee.com/arlionn/Course/blob/master/README.md)



---
![欢迎加入Stata连享会(公众号: StataChina)](https://gitee.com/uploads/images/2019/0507/094510_d0df7a52_1522177.jpeg "扫码关注 Stata 连享会")

