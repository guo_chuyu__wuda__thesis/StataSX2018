// 1.可以用外部命令提取 ssc install parmest

// 2.不需要外部命令的提取
clear
set obs 1000
gen y = rnormal()
gen x = rnormal()
gen z = rnormal()
gen du1 = (runiform()<0.5)
gen du2 = (runiform()<0.5)

reg y x z


mat A = r(table)'
mat list A

//local cnames: colnames r(table)
//di "`cnames'"


drop _all
// svmat A,names(col) //提取rowsname的话需要用svmat2
svmat2 A ,names(col) rnames(var)
keep if t !=.