//ssc install cntrade ,replace

clear all
cntrade 1,index
keep date
format date %tdCCYYNNDD
sum date
sum date,format

bcal create SH1A0001,replace from(date) purpose(1A0001SH for start to end)/*
*/  personal   maxgap(30) range(19901219 20200520) dateformat(ymd)  //generate(bdate)

/*

  Business calendar SH1A0001 (format %tbSH1A0001):

    purpose:  1A0001SH for start to end

      range:  19dec1990  20may2020
                 11310      22055   in %td units
                     0       7189   in %tbSH1A0001 units

     center:  19dec1990
                 11310              in %td units
                     0              in %tbSH1A0001 units

    omitted:     3,556              days
                   120.9            approx. days/year

   included:     7,190              days
                   244.4            approx. days/year

  Notes:

    business calendar file c:\ado\personal/SH1A0001.stbcal saved

    date not in %td format
*/
format date %tdCCYYNNDD
tsset date
/*
        time variable:  date, 19901219 to 20200520, but with gaps
                delta:  1 day

*/

gen mydate = bofd("SH1A0001",date)
format mydate %tbSH1A0001:CCYYNNDD
tsset mydate
/*
        time variable:  mydate, 19901219 to 20200520
                delta:  1 day
*/

sum date mydate
/*
 
    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
        date |      7,190    16651.73     3117.84      11310      22055
      mydate |      7,190      3594.5    2075.719          0       7189
*/

sum date mydate,format
/*
    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
        date |      7,190   20050803     3117.84  19901219  20200520
      mydate |      7,190   20050804    2075.719  19901219  20200520
*/


//将mydate转换为通常的date
gen regulardate =  dofb(mydate, "SH1A0001")
format regulardate %td
sum date mydate regulardate
/*
    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
        date |      7,190    16651.73     3117.84      11310      22055
      mydate |      7,190      3594.5    2075.719          0       7189
 regulardate |      7,190    16651.73     3117.84      11310      22055
*/

//新格式下提取年份 周几等方法
gen year = year(dofb(mydate, "SH1A0001"))
generate dow = dow(dofb(mydate, "SH1A0001"))










/*So What? */

//下载数据
forvalues i = 1/5{
	tempfile file`i'
	cntrade `i'
	save `file`i''	
}

//合并面板
use `file1',clear
forvalues i = 2/5{
	append using `file`i''
}

gen id = stkcd
xtset id date
/*
       panel variable:  stkcd (unbalanced)
        time variable:  date, 1991-01-02 to 2020-05-20, but with gaps
                delta:  1 day
*/

//format date %tdCCYYNNDD
gen mydate = bofd("SH1A0001",date)
gen regulardate =  dofb(mydate, "SH1A0001")
order id date mydate regulardate

xtset id date
/*
       panel variable:  id (unbalanced)
        time variable:  date, 1991-01-02 to 2020-05-20, but with gaps
                delta:  1 day
*/
xtset id mydate
/*
       panel variable:  id (unbalanced)
        time variable:  mydate, 9 to 7189, but with gaps
                delta:  1 unit

*/
format mydate %tbSH1A0001:CCYYNNDD
format regulardate %tdCCYYNNDD
format date %tdCCYYNNDD

xtset id mydate
/*
       panel variable:  id (unbalanced)
        time variable:  mydate, 19910102 to 20200520, but with gaps
                delta:  1 day

*/
sum date mydate regulardate
/*
    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
        date |     29,382    16382.91    3113.653      11324      22055
      mydate |     29,362    3418.357    2071.691          9       7189
 regulardate |     29,362    16385.96     3112.51      11324      22055

*/
sum date mydate regulardate,format
/*
    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
        date |     29,382   20041107    3113.653  19910102  20200520
      mydate |     29,362   20041112    2071.691  19910102  20200520
 regulardate |     29,362   20041110     3112.51  19910102  20200520

*/


gen year  = year(date)
gen year1 = year(mydate) // 新的格式下应该用这个命令 gen year = year(dofb(mydate, "SH1A0001"))
gen year2 = year(dofb(mydate, "SH1A0001"))
gen year3 = year(regulardate) //或者将时间转为一般的再来

sum year year1 year2 year3
/*
    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
        year |     29,382    2004.346    8.526179       1991       2020
       year1 |     29,362    1968.861     5.67685       1960       1979 //注意！！
       year2 |     29,362    2004.354     8.52312       1991       2020
       year3 |     29,362    2004.354     8.52312       1991       2020
*/

sum date mydate regulardate if year == 2018
/*

    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
        date |        957    21366.27    104.9268      21186      21546
      mydate |        957    6732.918    70.19156       6613       6855
 regulardate |        957    21366.27    104.9268      21186      21546

*/

sum date mydate regulardate if year == 2018,format
/*
    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
        date |        957   20180701    104.9268  20180102  20181228
      mydate |        957   20180702    70.19156  20180102  20181228
 regulardate |        957   20180701    104.9268  20180102  20181228

*/
preserve
	keep if year == 2018
	xtset id date
	sum date
	xtbalance ,range(21186 21546)
	count
	/*
		0
	*/
	// lost all sample because date is not continous
restore
preserve
	keep if year == 2018
	xtset id mydate
	xtbalance ,range(6613 6855)
	count
	/*
		729
	*/
restore
xtset id date
reg clsprc l.clsprc if year == 2018
est store olsdate
gen use1 = (e(sample) != 0)

xtset id mydate
reg clsprc l.clsprc if year == 2018
est store olsmydate
gen use2 = (e(sample) != 0)

esttab olsdate olsmydate,se ar2 star(* 0.1 ** 0.05 *** 0.01)
//note the difference of Observation Number!!!!
/*

--------------------------------------------
                      (1)             (2)   
                   clsprc          clsprc   
--------------------------------------------
L.clsprc            0.999***        0.998***
                (0.00162)       (0.00150)   

_cons             0.00223          0.0110   
                 (0.0291)        (0.0269)   
--------------------------------------------
N                     756             956   
adj. R-sq           0.998           0.998   
--------------------------------------------
Standard errors in parentheses
* p<0.1, ** p<0.05, *** p<0.01

*/



//为什么我们实证的样本量不同？
gen dow = dow(date)

tab dow if use1 == 1
/*
	
        dow |      Freq.     Percent        Cum.
------------+-----------------------------------
          2 |        181       23.94       23.94
          3 |        193       25.53       49.47
          4 |        189       25.00       74.47
          5 |        193       25.53      100.00
------------+-----------------------------------
      Total |        756      100.00

*/


tab dow if use2 == 1
/*

        dow |      Freq.     Percent        Cum.
------------+-----------------------------------
          1 |        180       18.83       18.83
          2 |        193       20.19       39.02
          3 |        197       20.61       59.62
          4 |        193       20.19       79.81
          5 |        193       20.19      100.00
------------+-----------------------------------
      Total |        956      100.00

*/


// 除了周一的，还有不同是因为什么？

list date if use1 ==0 & use2 ==1 & dow !=1

/*

       +----------+
       |     date |
       |----------|
 6373. | 20180102 |  //一、元旦：1月1日放假，与周末连休
 6405. | 20180222 |  //二、春节：2月15日至21日放假调休，共7天。2月11日（星期日）、2月24日（星期六）上班。
 6450. | 20180502 |	 //四、劳动节：4月29日至5月1日放假调休，共3天。4月28日（星期六）上班。
 6483. | 20180619 |  //五、端午节：6月18日放假，与周末连休。
 6552. | 20180925 |  //六、中秋节：9月24日放假，与周末连休。
       |----------|
	   ............
	   
	   	/*
		国务院办公厅关于2018年部分节假日安排的通知国办发明电〔2017〕12号
		http://www.gov.cn/zhengce/content/2017-11/30/content_5243579.htm
		*/
*/
//因为有假日，所以时间不连续，那么，如果含有滞后一阶回归的话，那么前一日放假今日开市，那么第一个回归并未纳入当日样本

/*一个问题：sum date mydate regulardate 时候你的样本量为什么不一样？*/

sum date mydate regulardate
/*
    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
        date |     29,382    16382.91    3113.653      11324      22055
      mydate |     29,362    3418.357    2071.691          9       7189
 regulardate |     29,362    16385.96     3112.51      11324      22055

*/

tab date if date !=. & mydate == .
/*

    Trading |
       Date |      Freq.     Percent        Cum.
------------+-----------------------------------
   19911228 |          3       15.00       15.00
   19920201 |          2       10.00       25.00
   19920202 |          3       15.00       40.00
   19920503 |          3       15.00       55.00
   19921004 |          3       15.00       70.00
   19930501 |          3       15.00       85.00
   19930502 |          3       15.00      100.00
------------+-----------------------------------
      Total |         20      100.00
*/

list date if date !=. & mydate == .

preserve
	duplicates drop date,force
	gen num_date = date
	sort date
	list num_date if date !=. & mydate == .
	/*
		  +----------+
		  | num_date |
		  |----------|
	 242. |    11684 |
	 267. |    11719 |
	 268. |    11720 |
	 328. |    11811 |
	 437. |    11965 |
		  |----------|
	 579. |    12174 |
	 580. |    12175 |
		  +----------+
	*/
restore


preserve
	cntrade 1,index
	foreach i of numlist 11684 11719 11720 11811 11965 12174 12175{
		di "`i' 样本量" 
		count if date == `i'
	} 
restore
//因为我的日历是根据第一个演示里面的上证指数做的！！！
//而那个数据集里面没有这几个时期的数据，所以做的日历是认为这几个时期是假日。。。


//可以根据现有数据集来做新的 Business calendar file 
preserve
	duplicates drop date,force
	bcal create SH1A0001,replace from(date) purpose(1A0001SH for start to end)/*
*/  personal   maxgap(30) range(19901219 20200520) dateformat(ymd)  //generate(bdate)
restore

replace mydate = bofd("SH1A0001",date)
replace regulardate =  dofb(mydate, "SH1A0001")

sum date mydate regulardate
/*
    Variable |        Obs        Mean    Std. Dev.       Min        Max
-------------+---------------------------------------------------------
        date |     29,382    16382.91    3113.653      11324      22055
      mydate |     29,382    3386.131    2069.908          1       7157
 regulardate |     29,382    16382.91    3113.653      11324      22055
*/


