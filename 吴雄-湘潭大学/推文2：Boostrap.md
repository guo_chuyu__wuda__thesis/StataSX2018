
> 任务：翻译 [HOW DO I WRITE MY OWN BOOTSTRAP PROGRAM? ](https://stats.idre.ucla.edu/stata/faq/how-do-i-write-my-own-bootstrap-program/)

注意事项：
1. 认真研读你的项目主页中的格式要求：
 **Note：** 写作之前请务必先查看 [「Wiki」](https://gitee.com/Stata002/StataSX2018/wikis/Home) 页面中的相关建议和要求，尤其是 [「Stata连享会推文格式要求」](https://gitee.com/Stata002/StataSX2018/wikis/00_Stata%E8%BF%9E%E4%BA%AB%E4%BC%9A%E6%8E%A8%E6%96%87%E6%A0%BC%E5%BC%8F%E8%A6%81%E6%B1%82.md?sort_id=953475)
2. 由于是翻译稿，请注明出处；

【第一部分有待添加】
【翻译部分需修改】
【后面的hsb2数据链接有问题，故使用第一个例子中的链接】

## 1 bootstrap 简介


## 2 编写 bootstrap程序

Stata 的 bootstrap 前缀命令非常方便，它不仅与估计命令（例如 OLS 回归和 logistic 回归）还与非估计命令（比如 summarize ）无缝衔接。bootstrap 命令可以给感兴趣的统计量自动执行自抽样过程，并计算相关的统计指标（例如偏差和置信区间）。然而尽管这个命令非常方便，但在某些情况下通过自抽样获得的统计量却不能通过该命令实现。对于这些情况，您需要编写自己的自抽样程序。

本篇 Stata FAQ 将演示如何编写自己的 bootstrap 程序。在第一个例子中，我们将 bootstrap 命令的结果与自己编写的 bootstrap 程序的结果进行对比。一般来说, 这应该可以展示编写 bootstrap 程序并不会太难。接下来便是一个示例, 其中您想要 bootstrap 获得的统计量在 bootstrap 命令中不起作用, 因此需要您编写自己的 bootstrap 程序。

## 2.1 例一

在本例子中，我们通过使用 bootstrap 命令并且用我们自己编写的 bootstrap 程序来复制结果。我们使用 High School and Beyond 这个数据集，性别（ female ）、数学分数（ math ）、写作分数( write )和社会经济地位( ses )对阅读分数( read )进行回归，并通过 bootstrap 获得标准误( rmse )。对于 bootstrap 我们重复 100 次并且指定随机种子数，从而可以重复结果。

```
. use http://statistics.ats.ucla.edu/stat/stata/notes/hsb2 ,clear
(highschool and beyond (200 cases))

. regress read female math write ses

      Source |       SS           df       MS      Number of obs   =       200
-------------+----------------------------------   F(4, 195)       =     52.58
       Model |  10854.9318         4  2713.73294   Prob > F        =    0.0000
    Residual |  10064.4882       195  51.6127602   R-squared       =    0.5189
-------------+----------------------------------   Adj R-squared   =    0.5090
       Total |    20919.42       199  105.122714   Root MSE        =    7.1842

------------------------------------------------------------------------------
        read |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
      female |  -2.450171   1.101524    -2.22   0.027    -4.622602   -.2777409
        math |   .4565641   .0721114     6.33   0.000     .3143457    .5987825
       write |   .3793564   .0732728     5.18   0.000     .2348475    .5238653
         ses |   1.301982   .7400719     1.76   0.080    -.1575905    2.761555
       _cons |   6.833418   3.279371     2.08   0.038     .3658287    13.30101
------------------------------------------------------------------------------
```
```
. bootstrap rmse=e(rmse), reps( 100 ) seed( 12345 ): regress read female math write ses
(running regress on estimation sample)

Bootstrap replications (100)
----+--- 1 ---+--- 2 ---+--- 3 ---+--- 4 ---+--- 5 
..................................................    50
..................................................   100

Linear regression                               Number of obs     =        200
                                                Replications      =        100

      command:  regress read female math write ses
         rmse:  e(rmse)

------------------------------------------------------------------------------
             |   Observed   Bootstrap                         Normal-based
             |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
        rmse |   7.184202   .3210754    22.38   0.000     6.554906    7.813498
------------------------------------------------------------------------------
```
```

. estat bootstrap, all

Linear regression                               Number of obs     =        200
                                                Replications      =        100

      command:  regress read female math write ses
         rmse:  e(rmse)

------------------------------------------------------------------------------
             |    Observed               Bootstrap
             |       Coef.       Bias    Std. Err.  [95% Conf. Interval]
-------------+----------------------------------------------------------------
        rmse |   7.1842021  -.0092243   .32107538    6.554906   7.813498   (N)
             |                                       6.619958   7.831612   (P)
             |                                       6.626499   7.854373  (BC)
------------------------------------------------------------------------------
(N)    normal confidence interval
(P)    percentile confidence interval
(BC)   bias-corrected confidence interval

```


编写 bootstrap 程序需要四步：
- 在第一步中，我们进行估计并将结果存储在一个矩阵中，这个矩阵我们定义为 **observe** 。此外，我们还应当注意使用的观测值数量，当我们在概述 bootstrap 的结果时会用到这个信息。
- 之后，我们编写一个程序，我们定义为  **mybot** ，这个程序通过重复抽样的方法对数据进行采样并返回需要的统计量。在这一步中，我们首先利用 **preserve**命令保存数据，然后用 **bsample** 命令进行自抽样，其中 **bsample** 命令对内存中的数据进行重复抽样，这是 bootstrap 必不可少的部分。通过在 bootstrap 样本中对我们的模型进行回归，并使用 **return scalar** 命令输出需要的统计信息。请注意，当使用 **program define myboot** 定义程序时，我们特别指定 **rclass** ，如果没有指定该项，将不会输出 bootstrap 统计量。 编写的**mybot** 程序以 **restore** 结尾，该命令会将数据返回到 bootstrap 之前的原始状态。
- 在第三步中，我们使用将 **simulate** 的前缀命令与 **mybot** 程序结合使用。我们在此步骤中指定与上一个例子中的相同的种子数和重复次数。
- 最后，我们使用 **bstat** 命令来概述结果，其中矩阵 **observe** 和样本大小分别放在 **stat()** 和 **n()** 选项中。
```
. use http://statistics.ats.ucla.edu/stat/stata/notes/hsb2, clear
(highschool and beyond (200 cases))

. *Step 1
. quietly regress read female math write ses

. matrix observe = e(rmse)

. *Step 2
. capture program drop myboot
. program define myboot, rclass
  1.  preserve 
  2.   bsample
  3.   regress read female math write ses
  4.   return scalar rmse = e(rmse)
  5.  restore
  6. end

. *Step 3
. simulate rmse=r(rmse), reps(100) seed(12345): myboot

      command:  myboot
         rmse:  r(rmse)

Simulations (100)
----+--- 1 ---+--- 2 ---+--- 3 ---+--- 4 ---+--- 5 
..................................................    50
..................................................   100

```

```
. *Step 4
. bstat, stat(observe) n(200)

Bootstrap results                               Number of obs     =        200
                                                Replications      =        100

------------------------------------------------------------------------------
             |   Observed   Bootstrap                         Normal-based
             |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
        rmse |   7.184202   .3210754    22.38   0.000     6.554906    7.813498
------------------------------------------------------------------------------
```

```
. estat bootstrap, all


Bootstrap results                               Number of obs     =        200
                                                Replications      =        100

------------------------------------------------------------------------------
             |    Observed               Bootstrap
             |       Coef.       Bias    Std. Err.  [95% Conf. Interval]
-------------+----------------------------------------------------------------
        rmse |   7.1842021  -.0092243   .32107538    6.554906   7.813498   (N)
             |                                       6.619958   7.831612   (P)
             |                                       6.626499   7.854373  (BC)
------------------------------------------------------------------------------
(N)    normal confidence interval
(P)    percentile confidence interval
(BC)   bias-corrected confidence interval
```


第四步的结果与上文使用 **bootstrap** 命令得到的结果一样。

## 2.2 例二
在本例中，自带的 **bootstrap** 命令并不与我们想要 bootsrap 的统计量相适应，因此我们需要编写一个 bootstrap 程序。bootstrap 命令不能与所有情况都适应是因为要 **bootstrap** 的那个统计量并没有被 **analysis** 命令所存储。而要想查看哪些统计量信息被存储，在“analysis”命令之后使用 **ereturn list** 或 **return list** 命令即可。**ereturn list** 和 **return list** 命令的区别在于“analysis”命令是否是估计命令。
假设我们想要 bootstrap 的统计量是 方差膨胀因子（**vif**），获取这个统计量需要运行回归后使用 **estat vif** 。然而在此情况下， bootstrap 的统计量 是从后估计命令中得到，而并不能直接从 **regress** 中获得，因此不被 bootstrap 命令所容纳。因此，我们必须编写自己的 bootstrap 程序来获得 vif 的bootstrap估计。

```
. use http://statistics.ats.ucla.edu/stat/stata/notes/hsb2, clear
(highschool and beyond (200 cases))

. *Step 1
. quietly regress read female math write ses

. estat vif

    Variable |       VIF       1/VIF  
-------------+----------------------
       write |      1.86    0.537690
        math |      1.76    0.568278
      female |      1.17    0.857692
         ses |      1.11    0.902671
-------------+----------------------
    Mean VIF |      1.47

```

```
. return list

scalars:
              r(vif_4) =  1.107823014259338
              r(vif_3) =  1.165920257568359
              r(vif_2) =  1.759701371192932
              r(vif_1) =  1.859809398651123

macros:
             r(name_4) : "ses"
             r(name_3) : "female"
             r(name_2) : "math"
             r(name_1) : "write"

. matrix vif = ( r(vif_4), r(vif_3), r(vif_2), r(vif_1))

. matrix list vif

vif[1,4]
           c1         c2         c3         c4
r1   1.107823  1.1659203  1.7597014  1.8598094

```

```
. *Step 2
. capture program drop myboot2
. program define myboot2, rclass
  1.  preserve 
  2.   bsample
  3.     regress read female math write ses
  4.     estat vif
  5.     return scalar vif_4 = r(vif_4)
  6.     return scalar vif_3 = r(vif_3)
  7.     return scalar vif_2 = r(vif_2)
  8.     return scalar vif_1 = r(vif_1)
  9.  restore
 10. end
 
. *Step 3
. simulate vif_4=r(vif_4) vif_3=r(vif_3) vif_2=r(vif_2) vif_1=r(vif_1), ///
> reps(100) seed(12345): myboot2

      command:  myboot2
        vif_4:  r(vif_4)
        vif_3:  r(vif_3)
        vif_2:  r(vif_2)
        vif_1:  r(vif_1)

Simulations (100)
----+--- 1 ---+--- 2 ---+--- 3 ---+--- 4 ---+--- 5 
..................................................    50
..................................................   100
```

```
. bstat, stat(vif) n(200)


Bootstrap results                               Number of obs     =        200
                                                Replications      =        100

------------------------------------------------------------------------------
             |   Observed   Bootstrap                         Normal-based
             |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
       vif_4 |   1.107823   .0339884    32.59   0.000     1.041207    1.174439
       vif_3 |    1.16592   .0632162    18.44   0.000     1.042019    1.289822
       vif_2 |   1.759701   .1651981    10.65   0.000     1.435919    2.083484
       vif_1 |   1.859809   .1760854    10.56   0.000     1.514688     2.20493
------------------------------------------------------------------------------
```

```
. estat bootstrap, all


Bootstrap results                               Number of obs     =        200
                                                Replications      =        100

------------------------------------------------------------------------------
             |    Observed               Bootstrap
             |       Coef.       Bias    Std. Err.  [95% Conf. Interval]
-------------+----------------------------------------------------------------
       vif_4 |    1.107823   .0064181   .03398842    1.041207   1.174439   (N)
             |                                       1.052385   1.184641   (P)
             |                                         1.0514   1.181458  (BC)
       vif_3 |   1.1659203   .0369962   .06321623    1.042019   1.289822   (N)
             |                                       1.100754    1.36875   (P)
             |                                       1.089861    1.26431  (BC)
       vif_2 |   1.7597014    .027213    .1651981    1.435919   2.083484   (N)
             |                                       1.505501   2.123814   (P)
             |                                       1.504108   2.071636  (BC)
       vif_1 |   1.8598094   .0473518   .17608536    1.514688    2.20493   (N)
             |                                       1.596556   2.376883   (P)
             |                                       1.581683    2.28026  (BC)
------------------------------------------------------------------------------
(N)    normal confidence interval
(P)    percentile confidence interval
(BC)   bias-corrected confidence interval

```
