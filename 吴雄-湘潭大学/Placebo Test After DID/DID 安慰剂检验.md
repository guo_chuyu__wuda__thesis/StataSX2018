```
clear all
set obs 1000
gen id = _n
gen D = (runiform()<0.5)
expand 2
bysort id :gen T = _n - 1
gen y = 1 + 2*D + 0.5*T + 1.5*T*D +rnormal()
gen treat = T * D
save DID_data.dta,replace


timer clear 1
timer on 1
cap program drop did_placebo
program define did_placebo,rclass
	tempvar D2 treat2
	gen `D2' = (runiform()<0.1)
	bysort id : replace `D2'= `D2'[1]
	gen `treat2' = `D2' * T
	reg y `D2' T `treat2'
	
	scalar b_ = _b[`treat2']
	return scalar b_ = b_
	
	/* T_statistics
    如果你要提取其他值：
	scalar b_ = _b[`treat2']
	scalar df_r_ = e(df_r)
	scalar se_ = _se[`treat2']
	scalar t_ = b_ / se_
	scalar p_ = 2*ttail(df_r_,abs(t_))
	scalar min95_ = b_ - invttail(df_r_,0.025)*se_
	scalar max95_ = b_ + invttail(df_r_,0.025)*se_ 
	
	return scalar b_ = b_
	return scalar df_r_ = df_r_
	return scalar se_ = se_
	return scalar t_ = t_
	return scalar p_ = p_
	return scalar min95_ = min95_
	return scalar max95_ = max95_
	*/
	
	
	/* Z_statistics
    对于probit等z统计量：
		scalar b_ = _b[`treat2']
		scalar df_r_ = e(df_r)
		scalar se_ = _se[`treat2']
		scalar z_ = b_ / se_
		scalar p_ = 2*normal(-abs(z_))
		scalar min95_ = b_ - invnormal(0.975)*se_
		scalar max95_ = b_ + invnormal(0.975)*se_
		
		return scalar b_ = b_
		return scalar df_r_ = df_r_
		return scalar se_ = se_
		retrun scalar z_ = z_
		return scalar p_ = p_
		return scalar min95_ = min95_
		return scalar max95_ = max95_
	*/

	
end

simulate treatment = r(b_),seed(34) reps(200): did_placebo

timer off 1
timer list 
dpplot treatment ,ms(oh) xline(0,lc(black*0.5) lp(dash)) xlabel(-1(0.1)1)
```