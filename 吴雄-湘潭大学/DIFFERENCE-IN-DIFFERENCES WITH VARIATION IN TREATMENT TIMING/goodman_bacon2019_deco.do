//goodman_bacon 2019 figure2
clear all
set obs 3
gen id = _n
//1k 2j 3u
gen     y_1 = 5  if id == 3
replace y_1 = 10 if id == 2
replace y_1 = 19 if id == 1
expand 100
bysort id : gen t = _n
gen did = 0
replace did = 1 if t >= 34 & id == 1
replace did = 1 if t >= 85 & id == 2

gen y = y_1 + 0.1*t
replace y = y + 10*did if id ==1
replace y = y + 15*did if id ==2  

replace y = y //+ rnormal()

twoway (sc y t if id ==1 ,c(l) ms(t)    mc(black) lc(black)) ///
	   (sc y t if id ==2 ,c(l) ms(oh)   mc(black) lc(black)) ///
	   (sc y t if id ==3 ,c(l) ms(none)  lc(gray)  lwidth(thick) ytitle("Units of y") ylabel(,nogrid) xline(34) graphregion(fcolor(white)) xlabel(34 85) legend(off) xtitle("Time"))
graph save bacon2019.gph,replace

twoway (sc y t if id ==1 ,c(l) ms(t)    mc(black)   lc(black)) ///
	   (sc y t if id ==2 ,c(l) ms(oh)   mc(gray%10) lc(gray%10)) ///
	   (sc y t if id ==3 ,c(l) ms(none) title("A. Early Group vs. Untreated Group") lc(gray)    lwidth(thick) ytitle("Units of y") ylabel(,nogrid) xline(34) graphregion(fcolor(white)) xlabel(34 85) legend(off) xtitle("Time")) 
graph save A1.gph,replace
  
twoway (sc y t if id ==1 ,c(l) ms(t)    mc(gray%10) lc(gray%10)) ///
	   (sc y t if id ==2 ,c(l) ms(oh)   mc(black)   lc(black)) ///
	   (sc y t if id ==3 ,c(l) ms(none) title("B. Late Group vs. Untreated Group") lc(gray)    lwidth(thick)  ytitle("Units of y") ylabel(,nogrid) xline(85) graphregion(fcolor(white)) xlabel(34 85) legend(off) xtitle("Time")) 
graph save A2.gph,replace

twoway (sc y t if id ==1 & t < 85 ,c(l) ms(t)    mc(black)   lc(black)) ///
	   (sc y t if id ==2 & t < 85 ,c(l) ms(oh)   mc(black)   lc(black)) ///
	   (sc y t if id ==3 ,c(l) ms(none) title("C. Early Group vs. Late Group, before t*") lc(gray%10)    lwidth(thick)  ytitle("Units of y") ylabel(,nogrid) xline(34 85) graphregion(fcolor(white)) xlabel(34 85) legend(off) xtitle("Time")) 	
graph save A3.gph,replace

twoway (sc y t if id ==1 & t > 34,c(l) ms(t)    mc(black)   lc(black)) ///
	   (sc y t if id ==2 & t > 34,c(l) ms(oh)   mc(black)   lc(black)) ///
	   (sc y t if id ==3 ,c(l) ms(none) title("D. Late Group vs. Early Group, after t*k") lc(gray%10)    lwidth(thick)  ytitle("Units of y") ylabel(,nogrid) xline(34 85) graphregion(fcolor(white)) xlabel(34 85) legend(off) xtitle("Time"))    
graph save A4.gph,replace

graph combine A1.gph A2.gph A3.gph A4.gph
	   
	   

bysort id :sum did

{
	//Frisch-Waugh(1933) 
	bysort id: egen y_bar_i = mean(y) 
	bysort t: egen y_bar_t = mean(y) 
	egen y_bar_bar = mean(y) 

	gen y_hat = y - y_bar_i - (y_bar_t - y_bar_bar) 
	   
	bysort id: egen Did_bar_i = mean(did) 
	bysort t : egen Did_bar_t = mean(did) 
	egen Did_bar_bar = mean(did) 
	gen Did_hat = did - Did_bar_i - (Did_bar_t - Did_bar_bar)
}
sum did if id ==1
scalar bar_d_k = r(mean)

sum did if id ==2
scalar bar_d_l = r(mean)

sum Did_hat
scalar hat_var_Did_hat = r(Var)

scalar n_u = 1/3
scalar n_k = 1/3
scalar n_l = 1/3

scalar s_ku  = n_k*n_u*bar_d_k*(1-bar_d_k) / hat_var_Did_hat
scalar s_lu  = n_l*n_u*bar_d_l*(1-bar_d_l) / hat_var_Did_hat 
scalar s_kl  = n_k*n_l*(bar_d_k-bar_d_l)*(1-(bar_d_k-bar_d_l)) / hat_var_Did_hat 
scalar mu_kl = (1-bar_d_k) / (1-(bar_d_k-bar_d_l))

di s_ku + s_lu + s_kl*mu_kl + s_kl * (1-mu_kl)

reg y did i.t i.id
scalar beta_did = _b[did]
reg y_hat Did_hat
scalar  beta_did_hat = _b[Did_hat]

reg y did i.t i.id if id != 2
scalar beta_did_ku = _b[did]

reg y did i.t i.id if id != 1
scalar beta_did_lu = _b[did]

reg y did i.t i.id if id != 3 & t <  85
scalar beta_did_klk = _b[did] 

reg y did i.t i.id if id != 3 & t >= 34
scalar beta_did_kll = _b[did]  

di s_ku*beta_did_ku + s_lu*beta_did_lu +s_kl*(mu_kl*beta_did_klk+(1-mu_kl)*beta_did_kll)
di beta_did
di s_ku + s_lu + s_kl

di s_ku " , " s_lu " , " s_kl*mu_kl " , " s_kl*(1-mu_kl)
//精度？


di "Did_hat : " beta_did_hat
di "did     : " beta_did 
