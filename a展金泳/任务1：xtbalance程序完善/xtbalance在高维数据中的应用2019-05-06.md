&emsp;

> 作者：展金泳 (对外经济贸易大学)      
>     
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn)

&emsp;

- Stata连享会 [精彩推文1](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文2](https://github.com/arlionn/stata/blob/master/README.md)

![](https://upload-images.jianshu.io/upload_images/7692714-8b1fb0b5068487af.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

  

与一般的数据不同，企业层面的贸易数据往往是高维的，例如我国海关数据库中的进出口数据一般为4维的：企业A在时间B出口（或进口）产品C到（从）国家D。在处理贸易数据的过程中笔者发现很多处理面板数据（相当于二维）的命令纷纷失效，有鉴于此，笔者希望能够对以往面板数据中的常用命令进行改造，以扩大其适用的范围。在本文中，笔者就通过对连玉君老师所编写的xtbalance进行完善（可能并不精确，暂时明名为balance），以使其能够完成高维数据中数据的应用。

* 目录
* 1.balance简介
* 2.balance在连续时间中的应用
	* 2.1 生成数据
	* 2.2 不使用balance的处理流程
	* 2.3 使用balance的处理流程
* 3.balance在非连续时间中的应用
	* 3.1 生成数据
	* 3.2 不使用balance的处理流程
	* 3.3 使用balance的处理流程
* 4.balance与xtbalance的区别与联系


### 1.balance 命令的使用

balance并不是stata内置的命令，需要安装才能进行使用。目前主要有以下安装途径：（1）通过码云（后期完成后设成超链接）下载balance.ado文件；（2）`ssc install balance`（后期完成后设定）。

`balance` 的语法格式: ` balance, [ var(varlist) ]  range(numlist)`

* balance后有两个option选项，其中range为必填选项，var为选填项，如果var留空，在使用该命令之前需要设置你的面板数据，你可以通过 `xtset panelvar timevar` 或 `tsset  panelvar timevar` 将数据设定成面板数据格式。

*  `range(numlist)`括号里为时间范围。如果为连续时间，可以设置为`balance, range(2001 2008)`，表示将前面设定的面板数据转换为2001-2008年八年间的平行面板；如果为非连续时间，可以设置为`balance, range(2001 2003, 2005, 2007 2010)`表示将前面设定的面板数据转换为2001-2003（三年），2005（一年）， 2007-2010（四年）共8年的平行面板。

* `var(varlist)`括号里为变量列表，最少需要填写两个变量，其中最后一个变量为时间变量，与range(numlist)中时间范围对应的`timevar` 。

* 设定案例：`balance, var(firm year) range(2001 2008)`表示保留2001-2008年8年间企业均存在出口行为的样本，前面不需要用 `xtset` 或 `tsset`设定面板，其等同于`xtset firm year` 加 `balance, range(2001 2008)`。`balance, var(firm product year) range(2001 2008)`表示保留2001-2008年8年间企业-产品组均存在出口行为的样本。`balance, var(firm product year) range(2001 2003, 2007)`表示保留2001-2003与2007年四年间企业-产品对均存在出口行为的样本观测值。

* 改进：（1）不需要专门设定面板；（2）兼容了对非连续时间的处理；（3）可以处理高维数据的转换；（4）兼容了以往`xtbalance`的设定。

### 2.balance 的流程

在本部分笔者将伪造一份贸易数据来解析`balance`命令对数据处理的过程。

#### 2.1 生成数据

我们首先生成一份贸易数据，100家国内公司（`firm`）在2001-2010年10年间（`year`）出口10种产品（`product`）到10个国家（`country`）共10万观测值的连续时间、高维、平行数据。

```Stata 
. clear
. set  obs 100000
. gen firm =int(_n/1000.001)+1
. //100家企业的编码为从1-100，分别表示第1家企业到第100家企业。
. bysort firm: gen  product = int(_n/100.001)+100001
. //海关数据库中产品代码由2、4、6、8等位次，在学术研究中大多用6位产品海关代码，因此伪造数据中产品的取值为100001-100010共10种产品。
. bysort firm product: gen country = int(_n/10.001)+1
. //10个进口国的编码为从1-10，分别表示第1个进口国到第10个进口国。
. bysort firm product country: gen year = _n+2000
. //时间的取值为2001-2010，取值2001表示2001年的出口。
. egen id=group(firm product country)
. xtset id year

. /*     panel variable:  id (strongly balanced)
        time variable:  year, 2001 to 2010
                delta:  1 unit
. */
```
鉴于firm product coutry year的4维数据无法直接设定面板，在数据生成中对firm product coutry进行分组配对（`egen id=group(firm product country)`），生成的id变量取值范围为1-10000，每个id对应10年，这样便可以对id和year进行面板设定。
接下来，利用随机函数将平行数据转换成非平行数据，为了保证操作的可重复性在数据删除的过程中设定了种子，每条观测值被删除的概率为5%。

```Stata 
. dis _N
. set seed 123456
. //设定种子以使得操作可重复
. drop if runiform() <0.05
. dis _N
. xtset id year

. /*     panel variable:  id (unbalanced)
        time variable:  year, 2001 to 2010
                delta:  1 unit
. */
```
利用随机函数进行删除后，样本观测值由100000减少成95104个，为了观察观测值删除后每个id的样本数，用`tab id if id<11`列出来了前10个id的样本情况。如果是平行面板则每个id应该有2010-2001+1=10条记录。


```Stata 
. tab id if id<11

 group(firm |
    product |
   country) |      Freq.     Percent        Cum.
------------+-----------------------------------
          1 |         10       10.42       10.42
          2 |         10       10.42       20.83
          3 |          9        9.38       30.21
          4 |          9        9.38       39.58
          5 |         10       10.42       50.00
          6 |          9        9.38       59.37
          7 |         10       10.42       69.79
          8 |          9        9.38       79.17
          9 |         10       10.42       89.58
         10 |         10       10.42      100.00
------------+-----------------------------------
      Total |         96      100.00

```

#### 2.2 不使用balance的处理流程

设定案例：`balance, var(firm product year) range(2001 2008)`表示保留2001-2008年8年间企业-产品对均存在出口行为的样本。

第一步，删除不在样本范围内的样本。
```Stata 
drop if year<2001 |  if year>2008
```
第二步，筛选出在设定范围（`2001-2008`）内均存在出口的企业-产品组名单。
```Stata 
preserve
duplicates drop firm product year, force
bysort firm product: keep if _N==8
duplicates drop firm product, force
keep firm product
gen lin=1
save lin.dta, replace
restore
```
第三步，将筛选出来的企业匹配到原数据中。
```Stata 
merge m:1 firm product using lin.dta, nogen
keep if lin==1
drop lin
erase lin.dta
dis _N
```

#### 2.3 使用balance的处理流程
```Stata 
balance, var(firm product year) range(2001 2008)
//后期balance编写完成后再汇报结果
```
### 3.balance在非连续时间中的应用
* 演示案例：`balance, var(firm product year) range(2001 2003, 2007)`
* 保留2001-2003与2007年四年间企业-产品对均存在出口行为的样本观测值。

#### 3.1 生成数据
本部分继续用前文所构造的贸易数据来进行模拟，数据生成的方式见`2.1`小节。
#### 3.2 不使用balance的处理流程
第一步，重新构造连续性的时间，将非连续时间转换为连续性时间，并删除时间范围外的样本。
```Stata 
gen lin=.
replace lin=1 if year==2001
replace lin=2 if year==2002
replace lin=3 if year==2003
replace lin=4 if year==2007
drop if lin==.
```
第二步，筛选出在设定范围（`2001 2003, 2007`）内均存在出口行为的企业-产品名单。
```Stata 
preserve
duplicates drop firm product lin, force
bysort firm product: keep if _N==4
duplicates drop firm product, force
keep firm product
gen lin2=1
save lin.dta, replace
restore
```
第三步，将筛选出来的企业匹配到原数据中。
```Stata 
merge m:1 firm product using lin.dta, nogen
keep if lin2==1
drop lin lin2
erase lin.dta
dis _N
```
#### 3.3 使用balance的处理流程
```Stata 
balance, var(firm product year) range(2001 2003, 2007)
//后期balance编写完成后再汇报结果
```
### 4.balance与xtbalance的联系与区别
* 联系：balance与xtbalance的基础上进行完善，核心的处理思路一脉相承。
* 区别：balance使得xtbalance摆脱了面板数据的限制，扩大其使用的范围。


### 5. 后记 (连玉君)

在写作这篇推文过程中，我与吴雄多次沟通，受到了不少启发。

后续我们会将文中提及的 `xtbalance` 的局限一一解决掉，以便大家能够更为便捷地进行非平行面板 &rarr; 平行面板的转换。

计划添加的功能如下(欢迎各位补充，反馈至：arlionn@163.com )：
- 添加 `delta()` 选项：以便处理**有固定间隔的平衡面板数据**；
- 添加 `gen(newvarname)` 选项：以便对平行面板构成的观察值进行标记；
- 添加 `force` 选项：由程序自动对用户的数据进行处理；
- ……

&emsp;

---


>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精彩推文1](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文2](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表1](https://www.jianshu.com/p/de82fdc2c18a) 
- [Stata连享会推文列表2](https://gitee.com/arlionn/jianshu/blob/master/README.md)
- Stata连享会 [精彩推文1](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文2](https://github.com/arlionn/stata/blob/master/README.md)

![](https://upload-images.jianshu.io/upload_images/7692714-8b1fb0b5068487af.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)



---
![欢迎加入Stata连享会(公众号: StataChina)](http://upload-images.jianshu.io/upload_images/7692714-c317947be074a605.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")