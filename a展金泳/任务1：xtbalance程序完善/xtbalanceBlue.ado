*! Given By Yujun Lian (arlionn@163.com), and Xiong Wu
*! First Version: 
/*
Name： Wu Xiong  
邮箱：mr.wuxiong@outlook.com 
单位：Business School of Xiangtan University, Xiangtan 411105,
China
*/

capture program drop xtbalanceBlue
program  define xtbalanceBlue
version 8.0
syntax , Range(numlist min=2 max=2 int ascending) [Miss(varlist)]

qui capture tsset
capture confirm e `r(panelvar)'
if ( _rc != 0 ) {
  dis as error "You must {help tsset} your data before using {cmd:xtbalance},see help {help xtbalance}."
  exit
}


tempvar  missv  /*删除 varlist 中的缺漏值*/
egen `missv' = rmiss(`miss')
qui count if `missv' != 0
qui drop if `missv' != 0

/*
version 15.0
如果version是15.0则为r(N_drop)'
if "`miss'" != "" & `r(N_drop)' != 0 {
	display _n in g "(" in y `r(N_drop)' in g " observations deleted due to missing) "
}
*/

if "`miss'" != "" & `r(N)' != 0 {
	display _n in g "(" in y `r(N)' in g " observations deleted due to missing) "
}


qui tsset
local id "`r(panelvar)'"
local t "`r(timevar)'" 

gettoken byear oyear : range

qui count if (`t'<`byear') | (`t'>`oyear')
if `r(N)' != 0 {
   dis _n in g "(" in y `r(N)' in g " observations deleted due to out of range) "
}
cap drop if (`t'<`byear') | (`t'>`oyear')  /*减少搜索量*/
 
 
qui sum `t', meanonly  /*判断用户输入的区间是否超出了样本的时间区间*/ 
local rmin = r(min)
local rmax = r(max)
if `byear'<r(min){
	dis in g "#1" in r " in option " ///
        in g "range(#1,#2)", in r "i.e., " ///
		in g `byear', in r "must be greater than "  ///
		in g "`rmin'," in r " the smallest year in sample."
  exit
}
else if `oyear'>r(max){
  dis in g "#2" in r " in option " ///
	  in g "range(#1,#2)", in r "i.e., " ///
	  in g `oyear', in r "must be less than "  ///
	  in g "`rmax'," in r " the largest year in sample."
  exit
}


qui tab `t'
local r2 = r(r)
tempvar r1_temp
bysort `id': gen `r1_temp' = _N

qui count if `r1_temp'!=`r2'
if `r(N)' != 0 {
   dis _n in g "(" in y `r(N)' in g " observations deleted due to discontinues) "
}

qui keep if `r1_temp'==`r2'
qui xtset

end
