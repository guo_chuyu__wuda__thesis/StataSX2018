## 参考资料

- [Getting those standard errors correct in small sample cluster studies](https://blogs.worldbank.org/impactevaluations/annals-of-good-ie-practice-getting-those-standard-errors-correct-in-small-sample-clustered-studies)
- [Doug Miller’s Stata code page ](http://faculty.econ.ucdavis.edu/faculty/dlmiller/statafiles/)，提供了多维聚类调整标准误的程序和数据，以及 Code for “Robust inference with Multi-way clustering”
- `help clustse` Andrew Menger, 2015\. "**[CLUSTSE: Stata module to estimate the statistical significance of parameters when the data is clustered with a small number of clusters](https://ideas.repec.org/c/boc/bocode/s457989.html)**," [Statistical Software Components](https://ideas.repec.org/s/boc/bocode.html) S457989, Boston College Department of Economics, revised 04 Aug 2017.
- [Cameron 2011 PPT](https://www.stata.com/meeting/mexico11/materials/cameron.pdf)， 可以截取不少公式

&emsp;


---

根据 以及 Cameron and Miller (2015)，Abadie et al. (2017) 的论文，写一篇推文，介绍实证分析中的聚类标准误问题。

- **主要参考文献：**
  - Abadie, A., S. Athey, G. W. Imbens, J. Wooldridge, 2017, When should you adjust standard errors for clustering?, Working Paper. [[PDF]](http://siepr.stanford.edu/sites/default/files/publications/17-030.pdf). [[PPT]](https://www.ifs.org.uk/uploads/cemmap/resources%20(slides)/Guido%20Imbens%20-%20slides%2015.04.16.pdf) 提供了一些比较精炼的结论。[[LaTeX文件]](https://arxiv.org/e-print/1710.02926) 下载后，用记事本打开即可查看公式的 LaTeX 代码。
  - Cameron, C. A., D. L. Miller, 2015, A practitioner’s guide to cluster-robust inference, Journal of Human Resources, 50 (2): 317-372.
  - Wooldridge, Jeff, 2011, CLUSTER SAMPLES AND CLUSTERING, [[PPT]](http://www.eief.it/files/2011/10/slides_5_cluster.pdf) 提供了一些 Stata 范例和 dofiles。并对二维聚类 SE 进行了讨论。
  - MacKinnon, J. G., 2019, How cluster-robust inference is changing applied econometrics, working Paper, [[PDF]](https://www.econ.queensu.ca/sites/econ.queensu.ca/files/files/qed_wp_1413.pdf).  他强烈推荐 Roodman 写的 `boottest` 命令。

- **推文的目的和定位：** 
目的是为了对大家的实证分析有一些实操性的指导建议。因此，要从这些论文中抽取结论性和实操性的论述出来。至于背后的原理，可以借助一些实例进行解释，但无需提供完整的证明过程。有兴趣的读者可以自行阅读原文。

- **推文思路和结构：**
  - 可以参考这篇 Blog 文章的思路 [When should you cluster standard errors? New wisdom from the econometrics oracle](https://blogs.worldbank.org/impactevaluations/when-should-you-cluster-standard-errors-new-wisdom-econometrics-oracle) 来写。
  - 还有这一篇，列举了一些实操中的主要建议：[Is the file drawer too large? Standard Errors in Stata Strike Back](https://www.fionaburlig.com/blog/2016/8/16/is-the-file-drawer-too-large)，作者也提供了相应的 Stata codes。
- **相关资料**
  -  [Stata 连享会: 聚类调整标准误笔记](https://www.jianshu.com/p/a096f3e3e821)
  - [Miguel Sarzosa, 2012, Introduction to Robust and Clustered Standard Errors](http://econweb.umd.edu/~sarzosa/teach/2/Disc2_Cluster_handout.pdf) 对 SE 和 Robust SE 讲的很很清楚，里面的图形也可以借鉴。

### 重要建议
- If nested (e.g., classroom and school district), you should cluster at the
highest level of aggregation (See Cameron, et al., 2015, JHR)
- If not nested (e.g., time and space), you can:
  - Include fixed-eects in one dimension and cluster in the other one.
  - Multi-way clustering extension (see Cameron, Gelbach and Miller, 2006)

> Source: 
The Sampling Design reason for clustering
Consider running a simple Mincer earnings regression of the form:

> Log(wages) = b0 + b1\*years of schooling + b2\*experience + b3\*experience^2 + u

You present this model, and are deciding whether to cluster the standard errors. 
- Referee 1 tells you “the wage residual is likely to be correlated within local labor markets, so you should cluster your standard errors by state or village.”. 
- But referee 2 argues “The wage residual is likely to be correlated for people working in the same industry, so you should cluster your standard errors by industry”, and 
- referee 3 argues that “the wage residual is likely to be correlated by age cohort, so you should cluster your standard errors by cohort”. What should you do?

&emsp;

&emsp;

---

### Abadie et al. (2017) 文章的核心观点

We develop a new perspective on clustering adjustments to standard errors. We argue that there are two potential motivations for such adjustments, one based on clustered sampling, and one based on clustered assignment. Although when researchers look for formal justification for clustering, they typically rely on clustered sampling justifications, we argue that clustered assignment is more commonly the setting of interest. This leads to new conclusions about when to adjust standard errors for clustering, and at what level to do the adjustment.

The practical implications from the results in this paper are as follows. The researcher should assess whether the sampling process is clustered or not, and whether the assignment mechanism is clustered. If the answer to both is no, one should not adjust the standard errors for clustering, irrespective of whether such an adjustment would change the standard errors. 

We show that the standard Liang-Zeger cluster adjustment is conservative, and further, we derive an estimator for the correct variance that can be used if there is variation in treatment assignment within clusters and the fraction of clusters that is observed is known. This analysis extends to the case where fixed effects are included in the regression at the level of a cluster, with the provision that if there is no heterogeneity in the treatment effects, one need not adjust standard errors for clustering once fixed effects are included.


### 二维聚类调整标准误
see Thompson (2011, JFE)


When estimating finance panel regressions, it is common practice to adjust standard errors for correlation either across firms or across time. These procedures are valid only if the residuals are correlated either across time or across firms, but not across both. This note shows that it is very easy to calculate standard errors that are robust to simultaneous correlation along two dimensions, such as firms and time. The covariance estimator is equal to the estimator that clusters by firm, plus the the estimator that clusters by time, minus the usual heteroskedasticity-robust OLS covariance matrix. Any statistical package with a clustering command can be used to easily calculate these standard errors.

### 多维聚类调整标准误

### 参考文献
- Cameron, A. Colin, Gelbach, Jonah, and Miller, Douglas, 2006, Robust Inference with Multi-way Clustering, NBER Technical Working Paper No. 327.
- Newey, Whitney, and Kenneth West, 1987, A Simple, Positive Semi-Definite, Heteroskedasticity and Autocorrelation Consistent Covariance Matrix, Econometrica 55, 703-708.
- Petersen, Mitchell, 2009, Estimating Standard Errors in Finance Panel Data Sets: Comparing Approaches, Review of Financial Studies 22, 435-480.
- Thompson, S. B., 2011, Simple formulas for standard errors that cluster by both firm and time, Journal of Financial Economics, 99 (1): 1-10.
- White, Hal, 1984, A Heteroskedasticity-Consistent Covariance Matrix Estimator and a Direct Test of Heteroskedasticity, Econometrica 48, 817-838.
### 参考资料


&emsp;

## FAQ
#### Q1：加入了省份固定效应后是否还需要在省级层面进行聚类调整？
这里讲的并不是很清楚，我们还需要再找一些资料，把这个问题说清楚
- Source：[Adding fixed effects](https://blogs.worldbank.org/impactevaluations/when-should-you-cluster-standard-errors-new-wisdom-econometrics-oracle)
What if we sample at the level of cities, but then add city fixed effects to our Mincer regression. Or we randomize at the city level, but add city fixed effects. Do we still need to cluster at the city level? 
The authors note that there is a lot of confusion about using clustering with fixed effects. The general rule is that you still need to cluster if either the sampling or assignment to treatment was clustered. However, the authors show that cluster adjustments will only make an adjustment with fixed effects if there is heterogeneity in treatment effects.

#### Q2：一维聚类调整还是二维聚类调整 ?

Thompson (2011, JFE) 认为 

Is there a downside to double-clustering the standard errors? Should we always adjust standard errors to handle persistent common shocks? In fact, it is not always best to use the “most robust” standard error formula. The various standard error formulas are estimates of true, unknown standard errors. In this section we will point out that the more robust standard error formulas tend to have less bias, but more variance. The lower bias improves the performance of test statistics. But the increased variance often leads us to find statistical significance even when it does not exist (e.g. we erroneously reject a true null hypothesis).




#### Q3：

#### Q4：

