> **任务：** 对分组回归得到特定统计量这一问题的各类命令和方法进行汇总。该文的定位是一个汇总性质的文章，不需要写太长。
可以在微信中搜索关键词，把相关的推文链接都汇总起来，每个命令给出几个典型的例子 (以命令为主) 即可。



参见：[regress market model for a lot of variables and save the betas](https://www.statalist.org/forums/forum/general-stata-discussion/general/1474564-regress-market-model-for-a-lot-of-variables-and-save-the-betas)

- `help statsby` //
- `help runby` //
- `help asreg` //

`asreg` 或许是最佳方式了。

对于 `runby` 和 `asreg`， 我们的推文都有介绍

- [Stata：runby - 一切皆可分组计算！](https://mp.weixin.qq.com/s/MHNqBcwJk19Su2QNXN3JMw)
- [盈余管理、过度投资怎么算？分组回归获取残差](https://mp.weixin.qq.com/s/J2rRp7N4QGIC09h4lenH7A)