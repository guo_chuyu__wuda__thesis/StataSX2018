### 任务及说明
- **任务：** 翻译  [The Stata Blog » Monte Carlo simulations using Stata](https://blog.stata.com/2015/10/06/monte-carlo-simulations-using-stata/)

- **说明：**
  - 开头部分要介绍一下 Monte Carlo 模拟的基本思想，以及 Stata 中一些基本命令，比如 `help random_number_functions`, `help simulate`
  - 要自己亲自做一遍，把完整的 Stata codes 附于文末，参见 [许梦洁 » 协整还是伪回归](https://gitee.com/Stata002/StataSX2018/blob/master/%E8%AE%B8%E6%A2%A6%E6%B4%81/%E9%A1%B9%E7%9B%AE2:%20Stata%E5%8D%8F%E6%95%B4%E5%88%86%E6%9E%90/%E5%8D%8F%E6%95%B4%E8%BF%98%E6%98%AF%E4%BC%AA%E5%9B%9E%E5%BD%92.md) 的处理方式。

  
  
