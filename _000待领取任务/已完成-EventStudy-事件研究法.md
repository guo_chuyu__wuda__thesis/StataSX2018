> 目的：介绍事件研究法的基本思路和操作流程，提供一些可供参考的资料。

> 按如下提纲撰写：

## 事件研究法概览(该题目下可以酌情加入细分二级或三级标题)  
  - 将 [事件研究法概览 - eventstudymetrics.com](https://eventstudymetrics.com/index.php/event-studies/) 
中的内容翻译成中文，作为事件研究法的基本介绍部分。

## 事件研究法参考资料 (该题目下可以酌情加入细分二级或三级标题) 
 
  - 将 [Stata连享会 - Stata - Wikis](https://gitee.com/arlionn/stata/wikis/%E4%BA%8B%E4%BB%B6%E7%A0%94%E7%A9%B6%E6%B3%95%E6%A6%82%E8%A7%88?parent=%E4%BA%8B%E4%BB%B6%E7%A0%94%E7%A9%B6%E6%B3%95) 中的内容嵌入进来。  

- 注意事项：若有数学公式，可以使用图片方式插入，亦可以从网上查资料，看看有没有其他的更好的方法。