> **任务：** 介绍一些支持模糊合并或匹配的命令。可以选择一个重点介绍，也可以在一篇推文中对多个命令进行对比介绍。我比较倾向于前者。

目前来看，主要是 `matchit` 命令。

### 命令列表

- `help iematch` //Matching base observations towards target observations using on a single continous variable.
- `help matchit` //Matches two columns or two datasets based on similar text patterns
- `help freqindex` //Generates an index of terms with their frequencies based on the current dataset
- `help strgroup` //to match strings based on their Levenshtein edit distance