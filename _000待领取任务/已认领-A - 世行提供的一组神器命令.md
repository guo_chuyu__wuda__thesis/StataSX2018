> 目的：写一篇推文，介绍 World Bank 在 `GitHub` 中提供的一组神奇的命令。
- 项目网址：https://github.com/worldbank/stata/tree/master/docs

**写作提纲：**

### Wold Bank Stata 项目简介
写作说明： 可以直接摘抄后翻译即可，适当加一些自己的评述

### 典型命令概览

写作说明：重点介绍其中的几个命令，配图说明: `sumStats`， `timeLines`， `manskiBounds`

#### `sumStats` 命令

#### `timeLines` 命令

#### `manskiBounds` 命令


### 安装命令和使用方法

写作说明：这些命令的下载和使用方法(重点)。主要是这些命令无法在 stata 内部使用 `findit` 命令搜索到，也无法用 `ssc install` 命令下载。
- 重点看 **Installing Commands** 这一小节，要亲测后再写；

`以下是我写的一部分，你可以继续：`

上述命令都需要使用 World Bank 提供的 命令从其 `GitHub` 仓库中下载，具体使用方法如下：
- 首先，安装 `wb_git_install` 命令。具体操作：在 Stata 命令窗口中输入如下命令，敲回车，
```
net install "https://raw.githubusercontent.com/worldbank/stata/master/wb_git_install/wb_git_install.pkg"
```



