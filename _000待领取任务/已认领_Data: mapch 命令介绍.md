> **任务：** 介绍 `mapch` 命令及其用法，可以重点参考 SJ 8(4):540--553 文中的说明。以往各期 Stata Journal 可以到 [「这里」](https://gitee.com/Stata002/StataSX2018/tree/master/_001_StataJournal) 下载。


- `help mapch` //  SJ 8(4):540--553
```stata
Example 1:

        begin           end
        A               B
        B               C
        G               H
        C               D
        X               Y
        H               I
        Z               Z1
        Z1              Z2
        X2              X3
        Z2              Z3

mapch will map the chains and create the database mapping as follows:

        begin           end             recent  date            NoOfEvents
        A               B               D               1               3
        B               C               D               2               3
        C               D               D               3               3
        G               H               I               1               2
        H               I               I               2               2
        X               Y               Y               .               1
        X2              X3              X3              .               1
        Z               Z1              Z3              1               3
        Z1              Z2              Z3              2               3
        Z2              Z3              Z3              3               3


Example 2:

        begin           end             time
        A               B               17004
        B               C               17203
        G               H               15000
        C               D               18999
        X               Y               17034
        H               I               16000
        Z               Z1              14333
        Z1              Z2              14334
        X2              X3              15001
        Z2              Z3              14335

mapch will map the chains and create the database mapping as follows:

        begin           end             time            recent  NoOfEvents
        A               B               17004           D               3
        B               C               17203           D               3
        C               D               18999           D               3
        G               H               15000           I               2
        H               I               16000           I               2
        X               Y               17034           Y               1
        X2              X3              15001           X3              1
        Z               Z1              14333           Z3              3
        Z1              Z2              14334           Z3              3
        Z2              Z3              14335           Z3              3


```