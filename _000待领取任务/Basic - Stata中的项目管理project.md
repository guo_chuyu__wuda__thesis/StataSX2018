- 目的：介绍外部命令 `project`
- 下载方法： net describe project, from(http://fmwww.bc.edu/RePEc/bocode/p)

- 另一个更为强大的命令是 `workingdir`，输入`findit workingdir` 可以查到该命令包并下载，也可以直接在命令窗口中输入：`net get workingdir.pkg`
- 详细说明文件参见： https://gitee.com/Stata002/Hello/attach_files