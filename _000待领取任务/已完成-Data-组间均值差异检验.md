> **任务：**韩少真认领 介绍几个进行组间均值差异检验的命令。
> **要点：**
- 最好列出一张表，分析各个命令的适用场景和优劣；
- 看一些期刊文献，看此类表格最常用的呈现模式是什么样的，然后使用如下命令实现，给出具体的范例。
- 可以使用 Stata 官方范例数据 `sysuse "auto.dta", clear` 或 `sysuse "nlsw88.dta", clear`

### 命令列表
- `help ttable2` //
- `help ttable3` //
- `help orth_out` //虽然选项复杂一些，但输出结果更为丰富
- `help balancetable` //功能很强大，可以输出到 LaTeX 或 Excel 文档中
- `help covbal` //Covariate balance statistics，适合 DID 或 PSM 分析中的平行假设检验