## 简介
本次内容拟包括渐进 DID (时变双重差分法)的实现，以及其与Event Study Approach的结合，还有如何解决ESA中Pretrend的问题

## 具体内容
- 渐进 DID 的具体函数设定以及与 Standard DID 的异同
   - 渐进 DID 的设定
   - 冲击影响到所有个体，只不过时间不同，即 I 型
   - 具有没有受到冲击影响的个体，即存在一直在 Control Group 个体，为 II 型

- 渐进 DID 与 Event Study Approach Approach的结合
   - I型的 ESA 的实现，以 Big Bad Banks 为例
   - II型的 ESA 的实现，以 Stata 命令 ddid 为例

- 消除Event Study Approach中Pretrend
  - 目前还需要探索，未形成思路，可参考引文中 NBER working paper, 也是 AER 的forthcoming paper

## 实现内容和过程
   - 未完待续.....
---


## 参考文献(相关资料)
- Beck, T., Levine, R., & Levkov, A. (2010). Big bad banks? The winners and losers from bank deregulation in the United States. The Journal of Finance, 65(5), 1637-1667.
- Autor, D. H. (2003). Outsourcing at will: The contribution of unjust dismissal doctrine to the growth of employment outsourcing. Journal of labor economics, 21(1), 1-42.
- [DID- 多期DID图示](https://gitee.com/Stata002/StataSX2018/blob/master/_000%E5%BE%85%E9%A2%86%E5%8F%96%E4%BB%BB%E5%8A%A1/%E5%B7%B2%E5%AE%8C%E6%88%90-DID-%20%E5%A4%9A%E6%9C%9FDID%E5%9B%BE%E7%A4%BA.md)
- [mostly-harmless-replication/05 Fixed Effects - DD and Panel Data/05 Fixed Effects](https://github.com/vikjam/mostly-harmless-replication/blob/master/05%20Fixed%20Effects%2C%20DD%20and%20Panel%20Data/05%20Fixed%20Effects%2C%20DD%20and%20Panel%20Data.md)
- [DID - 多期DID详解](https://gitee.com/Stata002/StataSX2018/blob/master/_000%E5%BE%85%E9%A2%86%E5%8F%96%E4%BB%BB%E5%8A%A1/%E5%B7%B2%E8%AE%A4%E9%A2%86-DID%20-%20%E5%A4%9A%E6%9C%9FDID%E8%AF%A6%E8%A7%A3.md)
- [DID与边际效应分析](https://gitee.com/Stata002/StataSX2018/blob/master/_000%E5%BE%85%E9%A2%86%E5%8F%96%E4%BB%BB%E5%8A%A1/%E5%B7%B2%E5%AE%8C%E6%88%90-DID-DID%E4%B8%8E%E8%BE%B9%E9%99%85%E6%95%88%E5%BA%94%E5%88%86%E6%9E%90.md)
- [ddid命令介绍](https://gitee.com/Stata002/StataSX2018/blob/master/_000%E5%BE%85%E9%A2%86%E5%8F%96%E4%BB%BB%E5%8A%A1/DID-ddid-%E5%A4%9A%E6%9C%9F%20DID.md)
- [事件研究法及stata实现](https://gitee.com/arlionn/stata/wikis/%E4%BA%8B%E4%BB%B6%E7%A0%94%E7%A9%B6%E6%B3%95%E5%8F%8Astata%E5%AE%9E%E7%8E%B0?sort_id=94192)
- Freyaldenhoven, S., Hansen, C., & Shapiro, J. M. (2018). Pre-event trends in the panel event-study design (No. w24565). National Bureau of Economic Research.
- Xiaohuan Lan, Tractor vs. Animal: Rural Reforms and Technology Adoption in China  (with Shuo Chen), submitted
