/// 20190804 16:04 对于渐进DID，拟准备先做一个simulation，然后replicate Big Bad Banks 这篇经典
/// 2019080 23：30 想到了一个加入个体和时间效应的模拟方法，记录如下

///这部分为所有个体都接受了干预，并分为冲击大小是否发生变化两种情况

///Simulations

///第一种设定，所有个体最终都接受了干预,三次强度一样，此处除对ESA变量设定敏感外，暂无其他问题

///设定60个观测值，设定随机数种子

```. clear all
. set obs 60 
. set seed 10101
. gen id =_n 

/// 每一个数值的数量扩大11倍，再减去前六十个观测值，即60*11-60 = 600，为60个体10年的面板数据

. expand 11
. drop in 1/60
. count 

///以id分组生成时间标识

 . bys id: gen time = _n+1999
. xtset id time 

///生成协变量以及个体和时间效应

 . gen x1 = rnormal(1,7)
. gen x2 = rnormal(2,5)

. sort time id
. by time: gen ind = _n
. sort id time

. by id: gen T = _n
. gen y = 0 

///生成处理变量,此时D为Dit,设定1-20在2004年接受冲击，21-40为2006年，41-60为2008年

 . gen D = 0
. gen birth_date = 0
forvalues i = 1/20{
	replace D = 1 if id  == `i' & time >= 2004
	replace birth_date = 2004 if id == `i'
}

forvalues i = 21/40{
	replace D = 1 if id  == `i' & time >= 2006
	replace birth_date = 2006 if id == `i'
}

forvalues i = 41/60{
	replace D = 1 if id  == `i' & time >= 2008
	replace birth_date = 2008 if id == `i'
} 

///Y的生成

 bysort id: gen y0 = 10  + 5 * x1 + 3 * x2 + T + ind  + rnormal()

bysort id: gen y1 = 10  + 5 * x1 + 3 * x2 + T + ind  + 10 + rnormal() if time >= 2004 & id >= 1 & id <= 20

bysort id: replace y1 = 10  + 5 * x1 + 3 * x2 + T + ind  + 10 + rnormal() if time >= 2006 & id >= 21 & id <= 40

bysort id: replace y1 = 10  + 5 * x1 + 3 * x2 + T + ind  + 10 + rnormal() if time >= 2008 & id >= 41 & id <= 60

bysort id: replace y1 = 10  + 5 * x1 + 3 * x2 + T + ind  + rnormal() if y1 == .

replace y = y0 + D * (y1 - y0) 

///两种消除x1,x2后表示y时间趋势的方法

 binscatter y time, by(D) line(connect) controls(x1 x2)
xtreg y x1 x2, fe r
predict e, residual 
binscatter e time, by(D) line(connect) 

///多种回归形式，待添加...

 xtreg y D x1 x2 i.time, fe r 

///ESA的实现和图示法,由于年份差值不同，应该取值到最早接受干预的那一组的最长时间差，在本模拟中就是2000-2004，即-4

 gen event = time - birth_date
replace event = -4 if event <= -4
///replace event = 2 if event >= 2
tab event, gen(eventt)
drop eventt1

xtreg y eventt* x1 x2 i.time, r fe

coefplot, ///
   keep(event*)  ///
   coeflabels(eventt2 = "-3"   ///
   eventt3 = "-2"           ///
   eventt4 = "-1"           ///
   eventt5 = "0"           ///
   eventt6  = "1"             ///
   eventt7  = "2"              ///
   eventt8  = "3"              ///
   eventt9  = "4"              ///
   eventt10 = "5")            ///
   vertical                             ///
   yline(0)                             ///
   ytitle("Y")                 ///
   xtitle("Time passage relative to year of adoption of implied contract exception") ///
   addplot(line @b @at)                 ///
   ciopts(recast(rcap))                 ///
   ///rescale(100)                         ///
   scheme(s1mono)
```

-------------------------------------------------
-------------------------------------------------
--------------------------------------------------


///第二种设定，所有的个体都接受的干预,但是其接收的干预强度随时间发生变化 
///设定60个观测值，设定随机数种子

```. clear all
. set obs 60 
. set seed 10101
. gen id =_n 

/// 每一个数值的数量扩大11倍，再减去前六十个观测值，即60*11-60 = 600，为60个体10年的面板数据
 . expand 11
. drop in 1/60
. count 

///以id分组生成时间标识

 . bys id: gen time = _n+1999
. xtset id time 

///生成协变量以及个体和时间效应

 . gen x1 = rnormal(1,7)
. gen x2 = rnormal(2,5)

. sort time id
. by time: gen ind = _n
. sort id time

. by id: gen T = _n
. gen y = 0 

///生成处理变量,此时D为Dit,设定1-20在2004年接受冲击，21-40为2006年，41-60个体一直不接受干预

 . gen D = 0
. gen birth_date = 0

forvalues i = 1/20{
	replace D = 1 if id  == `i' & time >= 2004
	replace birth_date = 2004 if id == `i'
}

forvalues i = 21/40{
	replace D = 1 if id  == `i' & time >= 2006
	replace birth_date = 2006 if id == `i'
}

forvalues i = 41/60{
	replace D = 1 if id  == `i' & time >= 2008
	replace birth_date = 2008 if id == `i'
} 

///Y的生成

 bysort id: gen y0 = 10  + 5 * x1 + 3 * x2 + T + ind + rnormal()

bysort id: gen y1 = 10  + 5 * x1 + 3 * x2 + T + ind  + (time - birth + 1 ) * 3 + rnormal() if time >= 2004 & id >= 1 & id <= 20

bysort id: replace y1 = 10  + 5 * x1 + 3 * x2 +  T + ind  + (time - birth + 1 ) * 3  + rnormal() if time >= 2006 & id >= 21 & id <= 40

bysort id: replace y1 = 10  + 5 * x1 + 3 * x2 +  T + ind  + (time - birth + 1 ) * 3  + rnormal() if time >= 2008 & id >= 41 & id <= 60

bysort id: replace y1 = 10  + 5 * x1 + 3 * x2 +  T + ind  + rnormal() if y1 == .

replace y = y0 + D * (y1 - y0) 


///两种消除x1,x2后表示y时间趋势的方法

 binscatter y time, by(D) line(connect) controls(x1 x2)
xtreg y x1 x2, fe r
predict e, residual 
binscatter e time, by(D) line(connect) 

///多种回归形式，待添加...,估计出的因果效应可能被时间趋势吸收了一部分
///原本应该为（3 + 6 + 9 +12 + 18），吸收后变成(-2 + 0 + 2 + 4 + 6 + 8),因此估计系数应该为18/6=3附近

 xtreg y D x1 x2 i.time, fe r 

///ESA的实现和图示法

 gen event = time - birth_date
replace event = -4 if event <= -4
///replace event = 4 if event >= 4
tab event, gen(eventt)
drop eventt4

xtreg y eventt* x1 x2 i.time, r fe

coefplot, ///
   keep(event*)  ///
   coeflabels(eventt2 = "-3"   ///
   eventt3 = "-2"           ///
   eventt4 = "-1"           ///
   eventt5 = "0"           ///
   eventt6  = "1"             ///
   eventt7  = "2"              ///
   eventt8  = "3"              ///
   eventt9  = "4"              ///
   eventt10 = "5")            ///
   vertical                             ///
   yline(0)                             ///
   ytitle("Y")                 ///
   xtitle("Time passage relative to year of adoption of implied contract exception") ///
   addplot(line @b @at)                 ///
   ciopts(recast(rcap))                 ///
   ///rescale(100)                         ///
   scheme(s1mono)
```
