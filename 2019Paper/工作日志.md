#### 选题情况 

> 请各位选定题目后登记到此页面下，并在自己的主页中新建一个文件夹，用于存放推文 Markdown 文档和相关资料。


1. 谭睿鹏：
    - A. RDD：Interpreting regression discontinuity designs with multiple cutoffs;  
    - B. RDD：Regression discontinuity designs using covariates
    - `2019/9/2 23:10` ~~即将完成~~
2. 修雪
    - A. `nestreg` 命令介绍
3. 徐萌萌
    - A. 连享会数据集分享
4. 郭飞
    - A. Leszczensky, l., and Woldbring. T, 2019, How to Deal With Reverse Causality Using Panel Data? Recommendations for Researchers Based on a Simulation Study. Sociological Methods and Research
    - ~~8.31日前赶一篇论文，9月份会加紧推文进度~~ (`2019/9/2 23:11`)
5. 杨咏文
    - A. 字符变量的处理 (已完成初稿，`2019/9/2 23:11`)
6. 亢延锟
    - A. RDD 的最新进展 (大概 9.10 日前可以完成，`2019/9/2 23:11`)
7. 李鑫：
    - A. 断点回归中 forcing variable 在断点两侧的不受人为的操纵的检验。(**已完成**，`2019.8.27`)
    - B. 对于 fuzzy RD，使用参数估计和非参估计的方法分别估计 ATE，对比两种方法的优劣。
    - C. 对于 `rdrobust` 命令下，解释传统的ATE估计量和进行偏差修正后的估计值，以及稳健标准误情况。
8. 王昆仑：
    - A. Simulations of Standard DID and Time-varying DID
    - B. Replicating  <Beck, T., Levine, R., & Levkov, A. (2010). Big bad banks? The winners and losers from bank deregulation in the United States. The Journal of Finance, 65(5), 1637-1667.> 
    - C. Replicating  <Ahlfeldt, G. M., 2018, Weights to address non-parallel trends in panel difference-in-differences models, CESifo Economic Studies, 64 (2): 216-24>   
9. 晏发发：(**已认领四个任务**，9月10日以后可以集中开工)
    - A. RDD, Do female politicians empower women to vote or run for office? A regression discontinuity approach; 
    - B. RDD, Distributional tests for regression discontinuity: Theory and empirical examples; 
    - C. RDD 断点回归-拐点回归-的组合检验 
    - D. 复制 AER 的一篇论文，FE，Dynamic panel data
10. 占云:
    - A. 面板中如何合理控制不可观测的异质性特征
11. 朱磊
    - A. 新命令介绍：textfind  