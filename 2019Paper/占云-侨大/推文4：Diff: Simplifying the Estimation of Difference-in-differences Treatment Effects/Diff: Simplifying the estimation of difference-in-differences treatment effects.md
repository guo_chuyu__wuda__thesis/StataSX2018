
> 作者：占云 (华侨大学)  
> E-mail: 953185016@qq.com     


在实证研究中，双重差分 ( DID ) 模型是评估某项政策效果的常用方法。本次推文将结合 `diff` 命令对使用 DID 模型估计处理效应进行介绍。

## 1. DID 模型

在单期DID 模型中，设定如下：

$$
\begin{aligned}
DID&=\left\{E\left(Y_{i t=1}|D_{i t=1}=1, Z_{i}=1\right)- E\left(Y_{i t=1}|D_{i t=1}=1, Z_{i}=0\right)\right\}\\&-\left\{E\left(Y_{i t=0}|D_{i t=0}=1, Z_{i}=1\right)- E\left(Y_{i t=0}|D_{i t=0}=0,Z_{i}=0\right)\right\}
\end{aligned} \quad(1)
$$ 

其中，

- $ Z_{i} $ 表示个体 $ i $ 是否进行某项处理，即 1 表示处理，0表示未处理；
- $ D_{i t} $ 为时期虚拟变量，即 $D_{i t=0}$ 表示基期，$D_{i t=0}$ 表示下一期；
- $ y_{i t=0} $ 表示基期的结果变量；
- $ y_{i t=1} $ 表示下一期的结果变量。

为了估计 (1) 式中的期望值，我们使用的回归估计模型如下：

$$
y_{it}=\beta_{0}+D_{t}\beta_{1} +Z_{i}\beta_{2}+D_{t}·Z_{i}\beta_{3}+ \varepsilon_{it}\quad (2)
$$

其中，
- $\hat{\beta}_{0}$ 为基期控制组的均值；
- $\hat{\beta}_{0}+\hat{\beta}_{2}$  为基期处理组的均值；
- $\hat{\beta}_{2}$ 为基期处理组和控制组之间的均值差异；
- $\hat{\beta}_{0}+\hat{\beta}_{1}$  为下一期控制组的均值；
- $\hat{\beta}_{0}+\hat{\beta}_{1}+\hat{\beta}_{2}+\hat{\beta}_{3}$ 为下一期处理组的均值；
- $\hat{\beta}_{2}+\hat{\beta}_{3}$  为下一期处理组和控制组之间的均值差异；
- $\hat{\beta}_{3}$ 则为 DID 估计量。

## 2. Diff 命令

`diff` 命令可以将单期 DID 与控制协变量、匹配方法、平衡检验等结合进行分析。同时，`diff` 能够在输出结果中提供估计系数及其交互项，进而对 DID 的估计进行简化。

### 2.1 语法

`diff` 命令的基本格式为

```
diff outcome_var [if] [in] [weight] ,[ options]
```
outcome_var 为结果变量，即估计模型中的 $ y_{i t} $。

### 2.2 选项

- `period(varname)` 用来指定时期虚拟变量 ( 即估计模型中的$ D_{t} $)；
- `treated(varname)` 用来指定处理变量 ( 即估计模型中的$ Z_{i} $)；
- `cov(varlist)` 用来指定协变量；
- `robust` 表示汇报稳健标准误；
- `report` 表示汇报对协变量系数的估计结果；
- `test` 表示检验在基期时，各变量在实验组和控制组的均值是否相等；
- `kernel` 用于估计核匹配 DID；
- `qdid(quantile) ` 用于估计 10%分位数 至 90%分位数的 DID 处理效应。

## 3. Diff 在 Stata 中的应用

我们以 Card and Krueger (1994) 数据集为例说明 `diff` 命令的用法。该数据研究的是新泽西州最低工资的增加对快餐业行业就业水平的影响。

```
use "http://fmwww.bc.edu/repec/bocode/c/CardKrueger1994.dta" //(Dataset from Card&Krueger (1994))
describe  // 对数据集进行描述

-----------------------------------------------
Contains data from http://fmwww.bc.edu/repec/bocode/c/CardKrueger1994.dta
  obs:           820                          Dataset from Card&Krueger
                                                (1994)
 vars:             8                          27 May 2011 20:36
 size:        12,300                          
----------------------------------------------------------------------------
              storage   display    value
variable name   type    format     label      variable label
----------------------------------------------------------------------------
id              int     %8.0g                 Store ID
t               byte    %8.0g                 Feb. 1992 = 0; Nov. 1992 = 1
treated         long    %8.0g      treated    New Jersey = 1; Pennsylvania =
                                                0
fte             float   %9.0g                 Output: Full Time Employment
bk              byte    %8.0g                 Burger King == 1
kfc             byte    %8.0g                 Kentuky Fried Chiken == 1
roys            byte    %8.0g                 Roy Rogers == 1
wendys          byte    %8.0g                 Wendy's == 1
----------------------------------------------------------------------------
Sorted by: id  t
```
数据中有 820 个观测值，结果变量为 `fte` ( Full Time Employment )，时期虚拟变量为 `t` ( 若时间为 1992 年 2 月，t = 0；若时间为 1992 年 11 月，t = 1 ) ，处理变量为 `treated` ( 若在新泽西州，treated = 1；若在宾夕法尼亚州，treated = 0 ) 。

### 3.1 不含协变量的单期 DID

使用 `diff` 估计单期 DID 处理效应估计需要三个变量：结果变量、处理变量和时期虚拟变量。在没有控制协变量的情况下，命令运行如下：

```
diff fte, treated(treated) period(t)

----------------------------------------------------
DIFFERENCE-IN-DIFFERENCES ESTIMATION RESULTS
Number of observations in the DIFF-IN-DIFF: 801
            Before         After    
   Control: 78             77          155
   Treated: 326            320         646
            404            397
--------------------------------------------------------
 Outcome var.   | fte     | S. Err. |   |t|   |  P>|t|
----------------+---------+---------+---------+---------
Before          |         |         |         | 
   Control      | 19.949  |         |         | 
   Treated      | 17.065  |         |         | 
   Diff (T-C)   | -2.884  | 1.135   | -2.54   | 0.011**
After           |         |         |         | 
   Control      | 17.542  |         |         | 
   Treated      | 17.573  |         |         | 
   Diff (T-C)   | 0.030   | 1.143   | 0.03    | 0.979
                |         |         |         | 
Diff-in-Diff    | 2.914   | 1.611   | 1.81    | 0.071*
--------------------------------------------------------
R-square:    0.01
* Means and Standard Errors are estimated by linear regression
**Inference: *** p<0.01; ** p<0.05; * p<0.1
```

DID 处理效应估计值为 2.914，且在10% 的水平上显著。然而，标准误的参数估计可能存在偏差。 因此，我们可以通过添加选项 `bs` 来计算自助标准误。

```
diff fte, treated(treated) period(t) bs 

----------------------------------------------------
DIFFERENCE-IN-DIFFERENCES ESTIMATION RESULTS
Number of observations in the DIFF-IN-DIFF: 801
            Before         After    
   Control: 78             77          155
   Treated: 326            320         646
            404            397
Bootstrapped Standard Errors

--------------------------------------------------------
 Outcome var.   | fte     | S. Err. |   |t|   |  P>|t|
----------------+---------+---------+---------+---------
Before          |         |         |         | 
   Control      | 19.949  |         |         | 
   Treated      | 17.065  |         |         | 
   Diff (T-C)   | -2.884  | 1.330   | -2.17   | 0.030**
After           |         |         |         | 
   Control      | 17.542  |         |         | 
   Treated      | 17.573  |         |         | 
   Diff (T-C)   | 0.030   | 1.165   | 0.03    | 0.979
                |         |         |         | 
Diff-in-Diff    | 2.914   | 1.694   | 1.72    | 0.085*
--------------------------------------------------------
R-square:    0.01
* Means and Standard Errors are estimated by linear regression
**Inference: *** p<0.01; ** p<0.05; * p<0.1
```

### 3.2 含协变量的单期 DID

`diff` 可以使用选项 `cov(varlist)` 来控制协变量，并通过添加选项 `report` 来报告协变量的系数。

```
diff fte, treated(treated) period(t) cov(bk kfc roys) report bs

-------------------------------------------------------------------
DIFFERENCE-IN-DIFFERENCES ESTIMATION RESULTS
Number of observations in the DIFF-IN-DIFF: 801
            Before         After    
   Control: 78             77          155
   Treated: 326            320         646
            404            397
Report - Covariates and coefficients:
-------------------------------------------------------------------
 Variable(s)         |   Coeff.   | Std. Err. |    z    |  P>|z|
---------------------+------------+-----------+---------+----------
bk                   | 0.917      | 1.091     | 0.841   | 0.401
kfc                  | -9.205     | 0.892     | -10.323 | 0.000
roys                 | -0.897     | 0.986     | -0.909  | 0.363
-------------------------------------------------------------------
Bootstrapped Standard Errors

--------------------------------------------------------
 Outcome var.   | fte     | S. Err. |   |t|   |  P>|t|
----------------+---------+---------+---------+---------
Before          |         |         |         | 
   Control      | 21.161  |         |         | 
   Treated      | 18.837  |         |         | 
   Diff (T-C)   | -2.324  | 1.255   | -1.85   | 0.064*
After           |         |         |         | 
   Control      | 18.758  |         |         | 
   Treated      | 19.369  |         |         | 
   Diff (T-C)   | 0.611   | 1.042   | 0.59    | 0.557
                |         |         |         | 
Diff-in-Diff    | 2.935   | 1.593   | 1.84    | 0.065*
--------------------------------------------------------
R-square:    0.19
* Means and Standard Errors are estimated by linear regression
**Inference: *** p<0.01; ** p<0.05; * p<0.1
```

### 3.3 核匹配 DID 处理效应

我们在 3.2 节中提到可以使用`diff` 命令选项 `cov(varlist)` 来控制协变量，而这些协变量可以用于匹配控制组和处理组个体。在此基础上，可以进一步添加 `kernel` 选项进行核匹配 DID 处理效应估计。

```
diff fte, treated(treated) period(t) cov(bk kfc roys) kernel id(id) report bs

------------------------------------------------------------------------------
KERNEL PROPENSITY SCORE MATCHING DIFFERENCE-IN-DIFFERENCES
    Report - Propensity score estimation with probit command
    Atention: _pscore is estimated at baseline

Iteration 0:   log likelihood = -198.21978
Iteration 1:   log likelihood =  -196.7657
Iteration 2:   log likelihood =  -196.7636

Probit regression                                 Number of obs   =        404
                                                  LR chi2(3)      =       2.91
                                                  Prob > chi2     =     0.4053
Log likelihood =  -196.7636                       Pseudo R2       =     0.0073

------------------------------------------------------------------------------
     treated |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
          bk |   .1812529   .2090916     0.87   0.386    -.2285591    .5910649
         kfc |   .3888298    .246799     1.58   0.115    -.0948873    .8725469
        roys |   .2997977   .2318227     1.29   0.196    -.1545664    .7541618
       _cons |   .6476036   .1777446     3.64   0.000     .2992305    .9959767
------------------------------------------------------------------------------
    Matching iterations...
.........................................................................................................................................................
> .......................................................................................................................................................
> ......................
Bootstrapping...
..................................................


DIFFERENCE-IN-DIFFERENCES ESTIMATION RESULTS
Number of observations in the DIFF-IN-DIFF: 801
            Before         After    
   Control: 78             77          155
   Treated: 326            320         646
            404            397
Bootstrapped Standard Errors

--------------------------------------------------------
 Outcome var.   | fte     | S. Err. |   |t|   |  P>|t|
----------------+---------+---------+---------+---------
Before          |         |         |         | 
   Control      | 20.040  |         |         | 
   Treated      | 17.065  |         |         | 
   Diff (T-C)   | -2.975  | 1.433   | -2.08   | 0.038**
After           |         |         |         | 
   Control      | 17.449  |         |         | 
   Treated      | 17.573  |         |         | 
   Diff (T-C)   | 0.124   | 0.986   | 0.13    | 0.900
                |         |         |         | 
Diff-in-Diff    | 3.099   | 1.446   | 2.14    | 0.032**
--------------------------------------------------------
R-square:       .
* Means and Standard Errors are estimated by linear regression
**Inference: *** p<0.01; ** p<0.05; * p<0.1
```

结果包含两个部分，第一阶段为倾向得分核匹配估计结果，第二阶段为 DID 估计结果，基于核匹配的 DID 估计值为 3.099，且在 5% 的水平显著。

### 3.4 分位数 DID ( QDID )

相较于一般的线性 (均值) 回归模型，分位数回归模型条件更为宽泛，可以描述结果变量的全局特征。比如，如果我们想知道最低工资的提高对全职雇员人数较少或较高的餐馆的影响是否更强，此时采用 QDID 就是合适的。我们可以在`diff` 中添加 `qdid(quantile)` 选项进行估计。

例如，对全职雇员人数处于中位数 ( 50% 分位数 ) 的餐馆的处理效应估计如下：

```
diff fte, t(treated) p(t) qdid(0.50)
DIFFERENCE-IN-DIFFERENCES ESTIMATION RESULTS

----------------------------------------------------
Number of observations in the DIFF-IN-DIFF: 801
            Before         After    
   Control: 78             77          155
   Treated: 326            320         646
            404            397
--------------------------------------------------------
 Outcome var.   | fte     | S. Err. |   |t|   |  P>|t|
----------------+---------+---------+---------+---------
Before          |         |         |         | 
   Control      | 17.000  |         |         | 
   Treated      | 16.000  |         |         | 
   Diff (T-C)   | -1.000  | 1.415   | -0.71   | 0.480
After           |         |         |         | 
   Control      | 16.000  |         |         | 
   Treated      | 17.500  |         |         | 
   Diff (T-C)   | 1.500   | 1.412   | 1.06    | 0.288
                |         |         |         | 
Diff-in-Diff    | 2.500   | 1.999   | 1.25    | 0.211
--------------------------------------------------------
R-square:    0.00
* Values are estimated at the .5 quantile
**Inference: *** p<0.01; ** p<0.05; * p<0.1
```

可知处理效应估计值为 2.5，而 t 值为 1.25，不具有显著性。

进一步地，我们对 25% 分位数，50% 分位数和 75% 分位数的 DID 处理效应分别进行估计进行对比：

```
diff fte, t(treated) p(t) qdid(0.25)
  est store m25
diff fte, t(treated) p(t) qdid(0.50)
  est store m50
diff fte, t(treated) p(t) qdid(0.75)
  est store m75

local m "m25 m50 m75"
    esttab `m', mtitle(`m') nogap ///
    b(%10.2f) t(%3.2f) s(r2 r2_a N) ///
    star(* 0.1 ** 0.05 *** 0.01)

------------------------------------------------------------
                      (1)             (2)             (3)   
                      m25             m50             m75   
------------------------------------------------------------
t                   -1.00           -1.00           -2.50   
                  (-0.49)         (-0.56)         (-1.62)   
treated             -1.50           -1.00           -4.50***
                  (-0.95)         (-0.71)         (-3.71)   
_diff                1.50            2.50            4.50***
                   (0.65)          (1.25)          (2.62)   
_cons               12.50***        17.00***        25.00***
                   (8.86)         (13.41)         (23.02)   
------------------------------------------------------------
r2                                                          
r2_a                                                        
N                  801.00          801.00          801.00   
------------------------------------------------------------
t statistics in parentheses
* p<0.1, ** p<0.05, *** p<0.01
```

通过对比可以发现，只有在 75% 分位水平的 DID 处理效应具有显著影响。也就是说，最低工资的提高对全职雇员人数较高的餐馆具有正向影响，而对中下水平的餐馆则没有影响。

## 4. 平衡性检验

最后，我们通过在 `diff` 命令中添加 `test` 选项，对基期各个协变量在实验组和控制组的均值是否相等来考察数据的平衡性。

```
diff fte, t(treated) p(t) cov(bk kfc roys wendys) test

------------------------------------------------------------------------------------------
TWO-SAMPLE T TEST

Number of observations (baseline): 404
            Before         After    
   Control: 78             -           78
   Treated: 326            -           326
            404            -

t-test at period = 0:
----------------------------------------------------------------------------------------------
 Variable(s)         |   Mean Control   | Mean Treated |    Diff.   |   |t|   |  Pr(|T|>|t|)
---------------------+------------------+--------------+------------+---------+---------------
fte                  | 19.949           | 17.065       | -2.884     |  2.44   | 0.0150**
bk                   | 0.436            | 0.408        | -0.028     |  0.45   | 0.6538
kfc                  | 0.154            | 0.209        | 0.055      |  1.09   | 0.2769
roys                 | 0.218            | 0.252        | 0.034      |  0.62   | 0.5368
wendys               | 0.192            | 0.132        | -0.060     |  1.37   | 0.1726
----------------------------------------------------------------------------------------------
*** p<0.01; ** p<0.05; * p<0.1
```

可以看出，各个协变量的 t 检验结果接受 " 处理组与控制组无系统差异 " 的原假设，即处理组和控制组各个协变量均值相等，数据能够得到较好地平衡。

