> 作者：占云 (华侨大学)  
> E-mail: 953185016@qq.com     
  
> **Source：** David M. Drukker, 2016, A Generalized Regression-adjustment Estimator for Average Treatment Effects from Panel Data, Stata Journal, 16(4): 826–836. [[pdf]](https://sci-hub.tw/10.1177/1536867X1601600402)


## 1. 引言
实证研究中，当影响处理的随机效应与影响潜在结果的随机效应相关联时，**平均处理效应 (ATE)** 采用 **简单回归估计量 (SRA)** 会产生不一致的结果。 通常人们将随机效应仅视为导致估计标准误 (SEs) 的问题，因此没有意识到此问题的重要性。本推文将介绍在面板数据能够得到 ATE 的无偏估计量 —— **广义回归调整估计量 (GRA)** 来解决这个问题。

## 2. 模型设定

面板数据中，处理随机效应 probit 模型为：

$$
\scriptsize   τ_{it} = \begin{cases}
                         1,\ & if\ z_{it} \gamma^{'}+ \alpha_{i}+\xi_{it}>0\\
                         0,\ & if\ otherwise \\
                           \end{cases}\quad (1)
$$

其中，

- $z_{it}$ 是处理协变量。
- $\gamma$ 是 $z_{it}$ 的系数。
- $\alpha_{i}$ 是影响处理的个体层面随机效应。
- $\xi_{it}$ 是影响处理的特定冲击。


潜在结果的线性模型为：

$$
y_{0,i t} = x_{i t}\beta_{0}+\eta_{i }+\epsilon_{0,i t}\quad (2)
$$
$$
y_{1,i t} = x_{i t}\beta_{1}+\eta_{i }+\epsilon_{1,i t}\quad (3)
$$

其中，
- $y_{0,i t}$ 是个体$i$ 在 $t$ 时刻不进行处理的潜在结果。
- $y_{1,i t}$ 是个体 $i$ 在 $t$ 时刻进行处理的潜在结果。
- $x_{it}$ 是结果协变量。
- $\beta_{0}$ 是个体 $i$ 不进行处理时 $x_{i t}$ 的系数。
- $\beta_{1}$ 是个体 $i$ 进行处理时 $x_{i t}$ 的系数。
- $\eta_{i }$ 是个体层面潜在结果随机效应。
- $\epsilon_{i t}$ 是随时间变化的特定冲击。

由此可得 $t$ 时刻的平均处理效应 $ATE_{t}$ 为：

$$
\begin{aligned}
ATE_{t} &=E\left(y_{1,i t}-y_{0,i t}\right) \\
&=E\left(y_{1,i t}\right)-E\left( y_{0,i t}\right)\\
&=\beta_{1}-\beta_{0}
\end{aligned} \quad (4)
$$ 

**简单回归估计量  (SRA)** 使用广义最小二乘估计量来估计每个$\widehat\beta_\widetilde{τ}$来估计$ATE_{t}$，仅仅使用观测值 $τ_{it}$ = $\widetilde{τ}$，忽略了样本选择产生的内生性问题。有偏的$\beta_\widetilde{τ}$ 参数使得均值预测也是有偏的，因而估计的均值差异也是有偏的。

例如，当 $\alpha_{i}$ 和 $\eta_{i }$ 正相关时，具有更大随机效应的个体更可能在$y_{1}$ 组中被观测而具有高于平均的 $y_{1}$ 值，因而使得 SRA 估计量高于真实的 ATE。

那么，如何才能得到 ATE 的无偏估计量呢？

**广义回归调整估计量 (GRA)** 基于广义结构方程模型 (GESM) ，使用最大似然估计得到$\widehat\beta_{0}$ 和 $\widehat\beta_{1}$，考虑了处理随机效应和潜在结果随机效应的相关性和样本选择的内生性偏误。此时，使用无偏的 $\beta_\widetilde{τ}$ 参数估计可以得到无偏的均值预测，因而估计的均值差异即 ATE 也是无偏的。




## 3. Stata 操作
## 3.1 SRA 估计处理

首先，生成一份模拟数据如下：

```
    set seed 12345671
    local N  2000
	local T  3
	set obs `N'
	generate id = _n
	matrix C = (1, .7 \ .7, 1)
	drawnorm ut ud , cov(C)
	expand `T'

	sort id
	by id: generate t = _n

	xtset id t

	generate c  = rnormal() 
	generate x1 = rnormal() + .5*c + 1
	generate x2 = rnormal() + .5*c + 1
	generate x3 = rnormal() + .5*c + 1

	generate e0 = rnormal()
	generate e1 = rnormal()
	generate e2 = rnormal()

	local b00 = .1
	local b01 = .2
	local b02 = .5

	local b10 = .1
	local b11 = .5
	local b12 = .6

	generate d  = (.1 + .1*x1 + .2*x2 + .3*x3 + ut + e2 > 0)
	generate y0 = `b00' + `b01'*x1 + `b02'*x2 + ud + e0
	generate y1 = `b10' + `b11'*x1 + `b12'*x2 + ud + e1
	generate y  = d*y1 + (1-d)*y0
```

从模拟数据可知真实的 $E\left(y_{0,i t}\right)$ 和 $E\left(y_{1,i t}\right)$分别为：

$$
\begin{aligned}
E\left(y_{0,i t}\right) &=b00+ b01+b02\\&=0.1+0.2+0.5\\&=0.8\end{aligned} \quad (5)
$$

$$
\begin{aligned}
E\left(y_{1,i t}\right) &=b10+ b11+b12\\
&=0.1+0.5+0.6\\
&=1.2
\end{aligned} \quad (6)
$$

$$
\begin{aligned}
ATE_{t} &=E\left(y_{1,i t}\right)-E\left( y_{0,i t}\right)\\&=1.2-0.8\\&=0.4 \end{aligned}\quad (7)
$$ 

接下来，进行回归和均值估计：

```
quietly xtreg y x1 x2 if d==1, re
predict double yh1, xb
quietly xtreg y x1 x2 if d==0, re
predict double yh0, xb
mean yh1 yh0, over(t) coeflegend

------------------------------------------------------------------------------
Mean estimation                   Number of obs   =      6,000

            1: t = 1
            2: t = 2
            3: t = 3

------------------------------------------------------------------------------
        Over |       Mean  Legend
-------------+----------------------------------------------------------------
yh1          |
           1 |    1.39922  _b[yh1:1]
           2 |   1.400209  _b[yh1:2]
           3 |   1.382231  _b[yh1:3]
-------------+----------------------------------------------------------------
yh0          |
           1 |   .3668965  _b[yh0:1]
           2 |   .3844787  _b[yh0:2]
           3 |   .3635779  _b[yh0:3]
------------------------------------------------------------------------------
```

由此，ATE 估计结果为：

```
display "ATE at time 1 is " _b[yh1:1] - _b[yh0:1]
display "ATE at time 2 is " _b[yh1:2] - _b[yh0:2]
display "ATE at time 3 is " _b[yh1:3] - _b[yh0:3]

----------------------------------------------------------
ATE at time 1 is 1.0323234
ATE at time 2 is 1.0157304
ATE at time 3 is 1.0186535
----------------------------------------------------------
```

从报告结果可知，ATE 的估计值分别为1.03、1.02和1.02，远高于真实值0.4，可知 SRA 估计的 ATE 是有偏的。


## 3.2 GRA 估计处理

同样地，我们使用和 SRA 相同的模拟数据进行 GRA 估计处理。

首先，生成结果变量 `y1_obs` 和 `y0_obs`，其中包含 $y_{1,i t}$ 和 $y_{0,i t}$ 的观测值以及缺失值。

```
generate y1_obs = y if d==1 //(1,910 missing values generated)
generate y0_obs = y if d==0 //(4,090 missing values generated)
```

然后，使用 `gsem` 命令进行 GESM 参数估计，并基于该参数使用  `margins` 命令估计预期潜在结果的均值。

```
quietly gsem (y1_obs <- x1 x2 U1[id]) ///
     (y0_obs <- x1 x2 U1[id])  ///
     (d  <- x1 x2 x3 U[id]@1, probit),  ///
     cov(U1[id]*U[id]) vce(robust)

margins, predict(outcome(y1_obs) marginal) ///
	predict(outcome(y0_obs) marginal)  ///
	vce(unconditional) post over(t) ///
	coeflegend

------------------------------------------------------------------------------
Predictive margins                              Number of obs     =      6,000

over         : t
1._predict   : Marginal predicted mean (y1_obs), predict(outcome(y1_obs) marginal)
2._predict   : Marginal predicted mean (y0_obs), predict(outcome(y0_obs) marginal)

                                 (Std. Err. adjusted for 2,000 clusters in id)
------------------------------------------------------------------------------
             |     Margin  Legend
-------------+----------------------------------------------------------------
  _predict#t |
        1 1  |   1.227004  _b[1bn._predict#1bn.t]
        1 2  |   1.228521  _b[1bn._predict#2.t]
        1 3  |    1.20972  _b[1bn._predict#3.t]
        2 1  |   .8174044  _b[2._predict#1bn.t]
        2 2  |   .8352209  _b[2._predict#2.t]
        2 3  |   .8130332  _b[2._predict#3.t]
------------------------------------------------------------------------------
```

最后，用 `nlcom` 命令进行均值差异即 ATE 估计。

```
nlcom ( _b[1bn._predict#1bn.t] - _b[2._predict#1bn.t] )  ///
      ( _b[1bn._predict#2.t]   - _b[2._predict#2.t] ) 	 ///
      ( _b[1bn._predict#3.t]   - _b[2._predict#3.t] ),   ///
      noheader

------------------------------------------------------------------------------
             |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
       _nl_1 |      .4096   .0450441     9.09   0.000     .3213153    .4978848
       _nl_2 |   .3932996   .0451034     8.72   0.000     .3048985    .4817007
       _nl_3 |   .3966868   .0452209     8.77   0.000     .3080555    .4853182
------------------------------------------------------------------------------
```

从报告结果可得，ATE 的估计值分别为 0.4、0.39 和 0.4，接近真实值 0.4，可知 GRA 估计的 ATE 是无偏的。

综上所述，当影响处理的随机效应与影响处理特定潜在结果的随机效应相关联时，使用 SRA 估计 ATE 会产生不一致的结果，而 GRA 能够得到一致结果，因此应该在实证分析中使用。

