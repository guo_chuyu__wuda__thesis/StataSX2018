# RD平滑性检验

### 1.背景

断点回归（RD）已经成为当前微观计量经济学分析政策效果的重要手段，而样本的随机分配被认为是该项工作的黄金律。因为只有样本在临界值（cut-point）的领域范围内是随机分布的，那么模型估计在临界值处的平均因果效应(ATE)才是政策实施对于处理组和控制组之间的差异的无偏估计值。但在现实经验研究中，样本的随机分配难以得到满足，出现样本分配机制人为干预现象，例如，学校开展对于期末考试分数低于某一值的学生进行强制的暑期培训，如果学生们提前知道这个分数值（如60分），那么原本考试成绩在这个分数值附近的学生就会加倍努力而通过考试，避免参加暑期培训项目，所以通过考试的学生数量会在断点（60分）右侧有一个数量上的上升。因此评价参加暑期培训是否能够提高学生的考试成绩的平均因果效应（ATE）存在偏差。

下面我们通过模拟数据的形式来展现上述样本在临界值处的两种现象。

![figure5](6E2D8420828548339EAC0DB368D9061A)
在figure1(a)中，我们通过模拟数据发现，当样本分配不存在人为干预，即学生事前并不知道低于60分将参加学校组织的暑期培训班项目，那么在驱动变量（考试分数）的直方图中不存在明显的样本数量在临界值（60分）处任意一侧样本数量的出现巨大的波动。

在figure1(b)中，我们通过人为改变样本数，模拟样本的认为干预现象，即学生事前知道低
于60分将必须参加学校组织的暑期培训项目，那么驱动变量（考试分数）的直方图中在临界值（60分）右侧将出现较高的学生数，而在临界值（60分）左侧的学生数大幅度下降，出现明显的跳跃现象。

### 2.检验工具
因此，在断点回归中，检验样本是否随机分配，即驱动变量密度函数的连续性，将有助于判断政策效应估计的有效性。下面，我们通过使用David S. Lee(2007)关于参议院选举的样本数据，分析三种不同的工具检验样本是否存在人为干预的现象，其中，该数据中民主党获胜的票数差margin为驱动变量，临界值为margin=0。

#### 2.1使用histogram（直方图）命令

使用histogram命令绘制驱动变量的直方图，观测断点处的频数变动和断点两侧的变动情况。  
- 优点：其操作简单、直观。  
- 缺点：直方图在不同组间距中的样本数量的不同，使得很难直观上观测到连续性。

```
*----------使用直方图显示驱动变量的连续性figure2------
    use "rdrobust_senate.dta", clear  //使用David S.Lee(2007)参议院选举的数据
    #d ;
 	histogram margin, 
	   lcolor(brown) fcolor(gs16) 
	   title("Senate_selection")  
	   xtitle("margin") 
	   note("figure2");
	#d cr
 )
```
![figure6](A64FE15BEC4440FE88F5B23F990A7500)

从figure2中可以看出，驱动变量（民主党获胜票数差）在临界值（margin=0）处两侧没有明显的数量波动，表明驱动变量密度函数在临界值处是连续函数。

#### 2.2使用McGrary方法

 DCdensity命令是由McCrary (2006)提出来用于检验驱动变量在临界值处是否连续，即样本在临界值处是否存在人为干预现象。主要方法如下：
-  （1）生成驱动变量的密度直方图，确保临界值不会被直方图所覆盖；
-  （2）在临界值左右两侧，分别进行局部的线性回归。其中因变量是组内频数，自变量是组间距的中间值；
-  （3）检验临界值两侧的拟合值的差的对数值是否显著不为零。

优点：
- 该命令相对于绘制直方图，绘制出较为平滑的核密度曲线图以及置信区间，能够直观的现实驱动变量在断点处的连续性。

缺点： 
- （1）该命令的缺点在于该命令所使用的核密度函数的带宽(bandwith)并不是使用交互验证或者是插入法确定的最优带宽选择，而是由其编写的默认公式所确定，因此在整体断点回归中使用不同带宽无法有效地证明其驱动变量的连续性。
- （2）如果存在某种干预模式使得高于临界值点和低于临界值点处的样本数量相同，那么该命令无法有效识别该类驱动变量的跳跃。
- （3）图形无法进行注释修饰。

McGrary(2006)驱动变量密度函数连续检验的基本命令为DCdensity, 其基本语法格式如下：

DCdensity varlist [if] [in] [weight] [, options]

其中，主要选项如下

     varlist:驱动变量
    breakpoint(): 驱动变量临界值处
    b()：指定特定带宽值
    generate():生成新的参数变量
    graphname(): 保存图形

举例

```
*----------使用McGray(2006)命令figure3----------------
    DCdensity margin, breakpoint(0) gen(Xj Yj r0 fhat se_fhat)
    
    return list
    
```
```
Discontinuity estimate (log difference in height): -.100745626
                                                   (.117145041)

scalars:
          r(bandwidth) =  25.84938346120713
            r(binsize) =  1.841330210610218
                 r(se) =  .117145040900551
              r(theta) =  -.1007456257891786

```
![figure3](3E308BC6197C4C2ABE3E9D352F8C8624)

从figure3中可以看到，尽管在临界值两侧的密度函数存在跳跃，但置信区间在此处重叠，表明在临界值两侧的驱动变量（民主党获胜票数差）密度函数是连续函数。并且临界值处估计的对数样本数差[r(theta)]不显著，证明不存驱动变量（民主党获胜票数差）的人为干预。

#### 2.3使用rdcont命令。

rdcont命令能够有效地克服由于断点处局部小样本的因素所导致对于密度函数连续性检验的影响。rdcont通过构造一个g阶的统计量来实现检验，其易于实现，较其他方法使用能够在更弱的条件下渐近有效，在比其渐近有效性所需的条件更强的条件下显示出有效样本。

优点：
- （1）操作简单，对于样本数量没有限制。
- （2）rdcont命令在估计过程中，并不涉及核密度函数，局部多项式，偏差修正，密度函数最优带宽的选择等问题。
     
rdcont命令的检验驱动变量的连续性的基本命令格式如下：

rdcont running_var [if] [in], [options]

其中：

     running_var: 驱动变量
     alpha(): 指定用于计算最佳带宽的临界值
     threshold():指定测试的临界值
     qband():指定特定的带宽值

举例

```
*----------使用rdcont命令table1----------------------
    rdcont margin, threshold(0)
```
```
RDD non-randomized approximate sign test
Running variable: margin
Cutoff c =       0 | Left of c  Right of c            Number of obs =       1390
-------------------+----------------------            q             =         94
     Number of obs |       640         750
Eff. number of obs |        46          48
 Eff. neighborhood |    -1.743       1.824
-------------------+----------------------
           p-value |     0.918

. 
end of do-file

```


原假设H_0:驱动变量密度函数在临界值处是连续函数。
如上表所示，断点回归在临界值处的样本非随机检验的p-value为0.918，不能拒绝原假设，证明驱动变量（民主党获胜票数差）密度函数在临界值处是连续函数，即不存在人为干预。

### 3.结语

这篇推文主要介绍了如何在RDD实证分析中进行平滑性检验，主要介绍了三种不同的检验方法。三种方法各有利弊，因此，在实际操作中分别使用进而实现相互验证。

以上所涉及的全部代码

```
*-------驱动变量不连续图形figure1--------
*----生成模拟数据---
	clear
	set obs 5000
	set seed 123
	gen z = rnormal() * 0.5       
    save simu1.dta, replace
*----生成干预数据----   
	gen z0 = z if (z>=-0.15)&(z<0)
    gen z1 = z if (z>=0)&(z<0.15)    
	gen z2 = z
	replace z2 = . if z2 == z0
	stack z2 z1, into(z3) clear
	drop if z3 == .                            
    merge 1:1 _n using simu1.dta, nogenerate      //stack命令会将z变量删除，该命令将z变量贴加进来
	drop if z3 == .	  
	 #d ;
	 twoway (histogram z ,
	        frequency lcolor(blue) fcolor(gs12)) 
			(histogram z3, 
			frequency lcolor(pink) fcolor(none)),
			xline(0, lpattern(dash) lcolor(green) lwidth(*1.5))
			xtitle("Score")
	        xlabel(-2 "40" -1 "50" 0 "60" 1 "80" 2 "90")
			legend(label(1 "no manipulation at the cut-point (fig.1a)")
			       label(2 "manipulation at the cut-point (fig.1b)") stack)
			note("figure1");
	  #d cr
*----------使用直方图显示驱动变量的连续性figure2------
    use "rdrobust_senate.dta", clear
    #d ;
 	histogram margin, 
	    lcolor(brown) fcolor(gs16) 
	    title("Senate_selection")  
     	xtitle("margin") 
    	note("figure2");
    #d cr
	
*----------使用McGray(2006)命令figure3----------------
    DCdensity margin, breakpoint(0) gen(Xj Yj r0 fhat se_fhat)
    return list
	
*----------使用rdcont命令table1----------------------
    rdcont margin, threshold(0)

```




